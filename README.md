# hied-harmony-frontend


## Getting Started

### Project Setup
Once you clone or download project go into you folder

>now copy **.env.example** file to **.env** file and change the value of below variables as per your need

PORT=5000                                                           # port


REACT_APP_SERVER_URL=http://103.21.53.11:3060/v1/api/               # API end point


REACT_APP_CLIENT_SERVER_URL=http://192.168.1.10:5000/api/        # API end point for external api


REACT_APP_STRIPE_PUBLISH_KEY= 'pk_update_live_key'               # Stripe publish key

REACT_APP_CLIENT_SERVER_IMAGEPATH=http://192.168.1.10:5000/        # Imahe path end point for external api




### Installing
```
> **npm install** (this will install all dependent libraries)
> **npm start** (this will run your project locally)
```

### Build
```
> **npm run build** (this will create build)

```