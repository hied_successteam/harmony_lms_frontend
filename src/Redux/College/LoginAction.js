import {STUDENT_LOGIN_SUCCESS, STUDENT_LOGIN_UNSUCCESS,
   STUDENT_LOGIN_FAILED, 
   ADMIN_LOGIN_SUCCESS,
   ADMIN_LOGIN_UNSUCCESS,
   ADMIN_LOGIN_FAILED,
   USER_LOGOUT
  } from "./LoginType"
import {login, Sociallogin, Adminlogin} from '../../Actions/Student/register'

export const studentLogin = (email, password, userType) =>{
  return(dispatch, getState)=>{
    login({
      email, password, userType
    }).then(res => {
      if(res.data.success == true){
        dispatch({type: STUDENT_LOGIN_SUCCESS, data: res.data});
      }else{
        dispatch({type: STUDENT_LOGIN_UNSUCCESS, data: res.data});
      }
    }).catch(err => {
        dispatch({type: STUDENT_LOGIN_FAILED, data: {success: false, message: err && err.response && err.response.data && err.response.data.message}})
    });
  }  
}

export const actionLogout = (data) => async (dispatch) => {
  const request = data;
  dispatch({ type: USER_LOGOUT, payload: request });
  return request;
};

export const adminLogin = (email, password, userType) =>{
  return(dispatch, getState)=>{
    Adminlogin({
      email, password
    }).then(res => {
      if(res.data.success == true){
        dispatch({type: ADMIN_LOGIN_SUCCESS, data: res.data});
      }else{
        dispatch({type: ADMIN_LOGIN_UNSUCCESS, data: res.data});
      }
    }).catch(err => {
        dispatch({type: ADMIN_LOGIN_FAILED, data: {success: false, message: err && err.response && err.response.data && err.response.data.message}})
    });
  }
  
}



export const studentSocialLogin = (request) =>{
  return(dispatch, getState)=>{
    Sociallogin(request).then(res => {
      if(res.data.success == true){
        dispatch({type: STUDENT_LOGIN_SUCCESS, data: res.data});
      }else{
        dispatch({type: STUDENT_LOGIN_UNSUCCESS, data: res.data});
      }
    }).catch(err => {
        dispatch({type: STUDENT_LOGIN_FAILED, data: {success: false, message: err && err.response && err.response.data && err.response.data.message}})
    });
  }
  
}