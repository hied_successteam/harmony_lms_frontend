import {
  STUDENT_LOGIN_SUCCESS, STUDENT_LOGIN_UNSUCCESS, STUDENT_LOGIN_FAILED,
  ADMIN_LOGIN_SUCCESS,
  ADMIN_LOGIN_UNSUCCESS,
  ADMIN_LOGIN_FAILED,
  USER_LOGOUT

} from "./LoginType"

import { clearLocalStorage} from '../../Lib/Utils'

const initialState = {
  status: "",
  required_data: ""
}

const LoginReducer = (state = initialState, action) => {

  if (state == null || state == undefined || action == null) {
    return { ...state }
  }

  switch (action.type) {

    case STUDENT_LOGIN_SUCCESS:
      return {
        ...state,
        required_data: action.data
      }
    case STUDENT_LOGIN_UNSUCCESS:
      return {
        ...state,
        required_data: action.data
      }
    case STUDENT_LOGIN_FAILED:
      return {
        ...state,
        required_data: action.data
      }

    case ADMIN_LOGIN_SUCCESS:
      return {
        ...state,
        required_data: action.data
      }
    case ADMIN_LOGIN_UNSUCCESS:
      return {
        ...state,
        required_data: action.data
      }
    case ADMIN_LOGIN_FAILED:
      return {
        ...state,
        required_data: action.data
      }
    case USER_LOGOUT:
      state.required_data = [];
      clearLocalStorage();
      return { ...state };

    default: return { ...state };
  }

  //return {...state}

}

export default LoginReducer;