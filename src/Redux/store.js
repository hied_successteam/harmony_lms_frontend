// import Reducer from "./College";
import { createStore, applyMiddleware } from "redux";
import ReduxThunk from 'redux-thunk'
import LoginReducer from './College/LoginReducer'

const store = createStore(LoginReducer, applyMiddleware(ReduxThunk));


export default store;