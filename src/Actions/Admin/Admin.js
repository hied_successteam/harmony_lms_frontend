import Request from '../../Lib/AdminApi';
import getRequest from '../../Lib/AdminGetAPi'

export const studentList = (params) => {
    const api_url = 'admin/student?'+params;    
    return getRequest(api_url);
};

export const exportStudentList = () => {
    const api_url = 'admin/student/download';    
    return getRequest(api_url);
};

//admin/college
export const collegeList = (params) => {
    const api_url = 'admin/college?'+params;    
    return getRequest(api_url);
};

export const studentListView = (params) => {
    const api_url = 'admin/student/view?userId='+params;  
    return getRequest(api_url);
};


export const transactionList = (params) => {
    // const api_url = 'admin/student'+params;
    const api_url = 'admin/transaction?'+params;    
    return getRequest(api_url);
};

export const collegeViewProfile = (params) => {
    // const api_url = 'admin/student'+params;
    const api_url = 'admin/college/view?userId='+params;    
    return getRequest(api_url);
};

export const logout = (params) => {
    const api_url = 'admin/logout';  
    return Request(api_url,params);
};

