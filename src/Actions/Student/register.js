import Request from '../../Lib/Api';
import getRequest from '../../Lib/getApi'
import DeleteRequest from '../../Lib/deleteApi'

export const register = (params) => {
    const api_url = 'auth/register';  
    return Request(api_url,params);
};

export const forgotPassword = (params) => {
    const api_url = 'auth/forgot-password';  
    return Request(api_url,params);
};

export const resetPassword = (params) => {
    const api_url = 'auth/reset-password';  
    return Request(api_url,params);
};


// const HarmonyApiService = (apiKeyName, data) => {
//     const api_url = 'auth/registration';  
//     return Request(apiKeyName, data);

// };

export const login = (params) => {
    const api_url = 'auth/login';  
    return Request(api_url,params);
};

export const Sociallogin = (params) => {
    const api_url = 'auth/social-signup';  
    return Request(api_url,params);
};

export const countryList = () => {
    const api_url = 'country';  
    return getRequest(api_url);
};

export const InstituteList = () => {
    const api_url = '/institution';  
    return getRequest(api_url);  
};

export const stateList = (params) => {
    const api_url = 'state?countryId='+params;  
    return getRequest(api_url);
};

export const cityList = (params) => {
    const api_url = 'city?stateId='+params;  
    return getRequest(api_url);
};

//save student profile
export const profile = (params,id) => {    
    //const api_url = 'student/'+id+'/profile';   
    const api_url = 'student/profile';   
    return Request(api_url,params);
};

//save student purchase plan
export const purchasePlan = (params) => {    
    //const api_url = 'student/'+id+'/profile';   
    const api_url = 'student/purchase-plan';   
    return Request(api_url,params);
};

export const purchaseServices = (params) => {  
    const api_url = 'student/purchase-service';   
    return Request(api_url,params);
};

//get student profile
export const getprofile = (id) => {    
    const api_url = 'student/profile';        
    return getRequest(api_url);
};

//save college profile
export const collegeProfile = (params,id) => {    
    //const api_url = 'student/'+id+'/profile';   
    const api_url = 'college/profile';   
    return Request(api_url,params);
};

//get college profile
export const getCollegeProfile = () => {    
    const api_url = 'college/profile';        
    return getRequest(api_url);
};


export const logout = (params) => {
    const api_url = 'auth/logout';  
    return Request(api_url,params);
};

export const major = (params) => {
    const api_url = 'major';    
    return Request(api_url,params);
};

export const getPlan = () => {    
    const api_url = 'plan';        
    return getRequest(api_url);
};

export const getfeature = () => {    
    const api_url = 'feature';        
    return getRequest(api_url);
};

//Save College compare list
export const saveCollegeComparison = (params) => {
    const api_url = 'student/college-comparison';    
    return Request(api_url,params);
};

//College compare from student dashboard
export const getCollegeComparison = () => {    
    const api_url = 'student/college-comparison';        
    return getRequest(api_url);
};


//Get student list for college dashboard
export const getCollegeDashboard = (params) => {    
    const api_url = 'college/dashboard?offset='+params.offset+'&limit='+params.limit+'&location='+params.location+'&term='+params.term+'&year='+params.year+'&bachelor='+params.bachelor+'&studyfield='+params.studyfield+'&major='+params.major;        
    return getRequest(api_url);
};

//View student profile by ID
export const getStudentViewProgile = (params) => {    
    const api_url = 'college/student/view?userId='+params;        
    return getRequest(api_url);
};

//college dashboard ( student favourite )
export const studentFavourite = (params) => {
    const api_url = 'favourite';  
    return Request(api_url,params);
};

//college dashboard ( student favourite list )
export const getStudentFavourite = (params) => {    
    const api_url = 'favourite?userId='+params;        
    return getRequest(api_url);
};

/// admin ///
export const Adminlogin = (params) => {
    const api_url = 'admin/login';  
    return Request(api_url,params);
};

//get students compare list by IDS
export const getStudentList = (params) => {    
    const api_url = 'college/student-profile?userId='+params;        
    return getRequest(api_url);
};

//Save student compare list
export const saveStudentComparison = (params) => {
    const api_url = 'college/student-comparison';    
    return Request(api_url,params);
};

//Student compare from college dashboard
export const getStudentComparison = () => {    
    const api_url = 'college/student-comparison';        
    return getRequest(api_url);
};

//delete comparison from compare list
export const deleteComparisonList = (params) => {    
    const api_url = 'feature/comparision?id='+params;        
    return DeleteRequest(api_url);
};


/////////////////////////////////////////////////

////////////   client server API        ////////

/////////////////////////////////////////////////

// college listing 
export const dashboard = (params) => {
    const api_url = 'institutions/external';  
    return Request(api_url,params, "client");
};

// Get college by UNITID 
export const getCollegeListByID = (params) => {
    const api_url = 'institutions/external/UNITID/'+params;  
    return getRequest(api_url, "client");
};

// Get favourite colleges by UNITID 
export const getFavouriteColleges = (params) => {
    const api_url = 'institutions/favourites';  
    return Request(api_url,params, "client");
};

// Get Partner college list
export const getPartnerCollegeList = (params) => {
    const api_url = 'institutions/partnercollege';  
    return Request(api_url,params, "client");
};

// Get Filter data for college list
export const collegeFinterData = () => {
    const api_url = 'institutions/external/filter';  
    return getRequest(api_url, "client");
};

//API for college profile compatibility
// export const collegeProfileCompatibility = (params) => {
//     const api_url = 'institutions/externalprofilematch';  
//     return Request(api_url,params, "client");
// }; 

//API for student profile compatibility
// export const studentProfileCompatibility = (params) => {
//     const api_url = 'institutions/externalinstituteprofilematch';  
//     return Request(api_url,params, "client");
// }; 

//API for student admission probability
export const studentAdmissionProbability = (params) => {
    const api_url = 'institutions/externalinstitute';  
    return Request(api_url,params, "client");
}; 