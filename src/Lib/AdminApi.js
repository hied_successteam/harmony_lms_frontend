import axios from 'axios';
//import { SERVER_URL } from './Constant';
import CONSTANTS from './Constants';
import { getLocalStorage } from './Utils';



const AdminApi = (api_url,params, status) => {

    var instance = '' ;

    if(status == "client"){
        //client server api
         instance = axios.create({
            baseURL: CONSTANTS.CLIENT_SERVER_URL  
        });
    }else{
        //server api
         instance = axios.create({
            baseURL: CONSTANTS.SERVER_URL  
        });
    }
    

    const headers = {
        'Content-Type': 'application/json',
        'Authorization': (getLocalStorage('admin') && status !== "client") ? "Bearer " + getLocalStorage('admin').accessToken : 'JWT fefege...'
        
      }    
    return instance.post(api_url,params, {headers: headers});
}


export default AdminApi;