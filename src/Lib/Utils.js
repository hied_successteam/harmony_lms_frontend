//import _ from 'lodash';
import Cryptr from 'cryptr';
//import moment from 'moment';
import CONSTANTS from './Constants';

var cryptlib = require('cryptlib'),
    iv = 'F@$%^*GD$*(*#!12', //16 bytes = 128 bit
    key = cryptlib.getHashSha256(CONSTANTS.ENCRYPTION_KEY, 32); //32 bytes = 256 bits

//let azureThumbURL = `${CONSTANTS.azureBlobURI}/${CONSTANTS.azureThumbContainer}`;
const cryptr = new Cryptr(CONSTANTS.CRYPTER_KEY);
//const userChannel = new BroadcastChannel('userNew_channel');

export const encrypt = (text) => {
    let cryptText = cryptlib.encrypt(text, key, iv);
    return cryptText.replace(/\//g, '_harmony_');
};

export const decrypt = (text) => {
    let decrptText = text.replace(/_harmony_/g, '/');
    return cryptlib.decrypt(decrptText, key, iv);
};

//used to encryption of localstorage data
export const encryptedData = (data) => {
    return cryptr.encrypt(data);
};

//used to decrypt localstorage data
export const decryptedData = (data) => {
    return cryptr.decrypt(data);
  };
  

// To set local storage data
export const setLocalStorage = (key, value) => {       
    value = JSON.stringify(value);
    //const encodedData = encryptedData(value);  
    //localStorage.setItem(key, encodedData);
    localStorage.setItem(key, value);
};

// To get local storage data
export const getLocalStorage = (key) => {
    if (key) {
      let data = localStorage.getItem(key);
      if (data) {
        //data = JSON.parse(decryptedData(data));
        data = JSON.parse(data);        
        return data;
      }
    }
    return null;
  };


  export const clearLocalStorage = (cb) => {    
    localStorage.clear();
    if (cb) cb();
  };

  //array to string 
  export const arrToStr = (arr) => {
    if (arr.length>0) {
      let newAr = arr.join(', ');
      return newAr;      
    }else{
      return '-'
    }    
  };
