import React from 'react';
import { Spinner } from 'react-bootstrap';

const Loader = (props) => {
  return <Spinner animation="border" variant="primary" className={ props.classes  } />;
};

export default Loader;
