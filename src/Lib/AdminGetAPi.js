import axios from 'axios';
//import { SERVER_URL } from './Constant';
import CONSTANTS from './Constants';
import { getLocalStorage } from './Utils';

const instance = axios.create({
    baseURL: CONSTANTS.SERVER_URL
});


const AdminGetAPi = (api_url) => {

    const headers = {
        'Content-Type': 'application/json',
        'Authorization': getLocalStorage('admin') ? "Bearer " + getLocalStorage('admin').accessToken : 'JWT fefege...'       
    }    

    return instance.get(api_url, { headers: headers });
}


export default AdminGetAPi;