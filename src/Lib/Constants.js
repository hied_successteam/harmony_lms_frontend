const CONSTANTS = {
    IV_LENGTH: 16,
    ENCRYPTION_KEY: 'sd5b75nb7577#^%$%*&G#CGF*&%@#%*&',
    CRYPTER_KEY:
      '0xffffffff,0xffffffff,0xffffffff,0xffffffff,0xffffffff,0xfffffff8', 
  
    OriginUrl: window.location.origin,  

    //server server URL
    SERVER_URL: process.env.REACT_APP_SERVER_URL  ,
    //SERVER_URL:'https://testcareerapi.mindwavetech.com/v1/api/',

    //client server url
    CLIENT_SERVER_URL: process.env.REACT_APP_CLIENT_SERVER_URL  ,


    //client server image path
    CLIENT_SERVER_IMAGEPATH: process.env.REACT_APP_CLIENT_SERVER_IMAGEPATH  ,

    ROLES: {
        STUDENT: 'student',
        COLLEGE: 'college',        
      },

      GENDER: {
        "2": 'Male',
        "3": 'Female',     
        "Non-Binary": "Non-Binary",
        "Prefer not to say": "Prefer not to say" 
      },
    
    ROLE_DESCRIPTION: [
      { roleId: 1, role: 'student', title: 'Student' },
      { roleId: 2, role: 'college', title: 'College' },      
    ],   
    
    EFCIPLEV: {
      "101": 'Education',
      "201": 'Engineering',     
      "301": "Biological Sciences/Life Sciences",
      "401": "Mathematics" ,
      "501": "Physical Sciences",
      "601": "Business Management and Administrative Services",
      "716": "Law",
      "816": "Dentistry",
      "916": "Medicine",
    }, 
   

    STRIPE_PUBLISH_KEY: process.env.REACT_APP_STRIPE_PUBLISH_KEY ,   
    
    //STRIPE_PUBLISH_KEY: "pk_test_BX1dy9UdoNqfxgcCc4wtaYzE00UXgBkmf2" ,     
    

    //Publish Key : pk_test_BX1dy9UdoNqfxgcCc4wtaYzE00UXgBkmf2
//Secret Key : sk_test_55C7FYH75dS29WWJ0UQdmRvi00YgUZjI8L
    
    // SOCKET: {
    //   URL: 'http://103.21.53.11:3001',     
    // },   
    
  };
    
  export default CONSTANTS;
  