import Validator from "validator";
import isEmpty from "lodash/isEmpty";
import React from "react";
import { toLower } from "lodash";

export default function validateInput(firstname, lastname, email, mobile, country, state, zipCode, password, confPassword, dialCountryName, dialCountryShortName) {
  let errors = {};

  let alphaOnly = "^[a-zA-Z .']*$";
  //let alphaNumeric = "^[a-zA-Z0-9']*$";

  if (Validator.isEmpty(firstname)) {
    errors.firstname = (
      <span style={{ color: "red", fontSize: 13 }}>First name is required.</span>
    );
  } else if (!Validator.matches(firstname, alphaOnly)) {
    errors.firstname = (
      <span style={{ color: "red", fontSize: 13 }}>First name can only contain alphabets.</span>
    );
  }

  if (Validator.isEmpty(lastname)) {
    errors.lastname = (
      <span style={{ color: "red", fontSize: 13 }}>Last name is required.</span>
    );
  }else if (!Validator.matches(lastname, alphaOnly)) {
    errors.lastname = (
      <span style={{ color: "red", fontSize: 13 }}>Last name can only contain alphabets.</span>
    );
  }

  if (Validator.isEmpty(email)) {
    errors.email = (
      <span style={{ color: "red", fontSize: 13 }}>Email is required.</span>
    );
  } else {
    if (!Validator.isEmail(email)) {
      errors.email = (
        <span style={{ color: "red", fontSize: 13 }}>Email is invalid.</span>
      );
    // }else if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)){
    }else if(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{1,}))$/.test(email)){   
    }else{
      errors.email = (
        <span style={{ color: "red", fontSize: 13 }}>Email is invalid.</span>
      );
    }
  }
  if (Validator.isEmpty(mobile)) {
    errors.mobile = (
      <span style={{ color: "red", fontSize: 13 }}>Mobile number is required.</span>
    );
  } else {
    if (mobile.length < 10 || mobile.length > 10) {
      errors.mobile = (
        <span style={{ color: "red", fontSize: 13 }}>Mobile number must be 10 digit.</span>
      );
    }
  }

  if (Validator.isEmpty(country)) {
    errors.country = (
      <span style={{ color: "red", fontSize: 13 }}>Country is required.</span>
    );
  }
  if (Validator.isEmpty(state)) {
    errors.state = (
      <span style={{ color: "red", fontSize: 13 }}>State is required.</span>
    );
  }
  if (Validator.isEmpty(zipCode)) {
    errors.zipCode = (
      <span style={{ color: "red", fontSize: 13 }}>ZipCode is required.</span>
    );
  }
  if (Validator.isEmpty(password)) {
    errors.password = (
      <span style={{ color: "red", fontSize: 13 }}>Password is required.</span>
    );
  }

  if (!Validator.isStrongPassword(password)) {
    errors.password = (
      <span style={{ color: "red", fontSize: 13 }}>Password should contain min 8 characters & min 1 Uppercase, Lowercase, Number & Special character.</span>
    );
  }



  if (Validator.isEmpty(confPassword)) {
    errors.confPassword = (
      <span style={{ color: "red", fontSize: 13 }}>Confirm password is required.</span>
    );
  }

  if (!Validator.isEmpty(password) && !Validator.isEmpty(confPassword)) {
    if (password !== confPassword) {
      errors.confPassword = (
        <span style={{ color: "red", fontSize: 13 }}>Password and Confirm password does not match.</span>
      );
    }
  }

  if (!Validator.isEmpty(country) && !Validator.isEmpty(mobile)) {  

    if (toLower(dialCountryName) !== toLower(dialCountryShortName)) {
      errors.mobile = (
        <span style={{ color: "red", fontSize: 13 }}>Country's Contact Number should be same as Country</span>
      );
    }
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
}
