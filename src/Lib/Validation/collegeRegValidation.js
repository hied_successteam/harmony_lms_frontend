import Validator from "validator";
import isEmpty from "lodash/isEmpty";

export default function validateInput(data) {
    let errors = {};


      if (Validator.isEmpty(data.instituteId.toString())) {
        errors.instituteId = "Please select college.";
      }



    if (Validator.isEmpty(data.userName)) {
        errors.userName = "Full name is required.";
    }

    if (data.instituteId == 0) {

        if (Validator.isEmpty(data.instituteName)) {
            errors.instituteName = "College name is required.";
        }
    }else{
        if (Validator.isEmpty(data.instituteName)) {
            errors.instituteName = "College name is required.";
        }
        // if (Validator.isEmpty(data.instituteAddress)) {
        //     errors.instituteAddress = "College address is required.";
        // }
    }


    if (Validator.isEmpty(data.email)) {
        errors.email = "Email is required.";
    } else {
        if (!Validator.isEmail(data.email)) {
            errors.email = "Email is invalid.";

        // }else if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(data.email)){
        }else if(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{1,}))$/.test(data.email)){           
        }else{
            errors.email = "Email is invalid.";
        }
    }

    if (Validator.isEmpty(data.mobile)) {
        errors.mobile = "Contact number is required.";
    }else
    {
        if (data.mobile.length < 10) {
            errors.mobile = "Contact number must be 10 digit.";
        }
    }

    if (Validator.isEmpty(data.password)) {
        errors.password = "Password is required.";
    } else {
        if (!Validator.isStrongPassword(data.password)) {
            errors.password = "Password should contain min 8 characters & min 1 Uppercase, Lowercase, Number & Special character.";

        }
    }

    if (Validator.isEmpty(data.confPassword)) {
        errors.confPassword = "Confirm password is required.";
    }

    if (!Validator.isEmpty(data.password) && !Validator.isEmpty(data.confPassword)) {
        if (data.password !== data.confPassword) {
            errors.confPassword = "Password and Confirm password does not match.";
        }
    }

    return {
        errors,
        isValid: isEmpty(errors)
    };
}
