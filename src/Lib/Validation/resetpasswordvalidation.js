import Validator from "validator";
import isEmpty from "lodash/isEmpty";

export default function validateInput(data) {
    let errors = {};   

    if (Validator.isEmpty(data.password)) {
        errors.password = "Password is required.";
    } else {
        if (!Validator.isStrongPassword(data.password)) {
            errors.password = "Password should contain min 8 characters & min 1 Uppercase, Lowercase, Number & Special character.";

        }
    }

    if (Validator.isEmpty(data.confPassword)) {
        errors.confPassword = "Confirm password is required.";
    }

    if (!Validator.isEmpty(data.password) && !Validator.isEmpty(data.confPassword)) {
        if (data.password !== data.confPassword) {
            errors.confPassword = "Password and Confirm password does not match.";
        }
    }

    return {
        errors,
        isValid: isEmpty(errors)
    };
}
