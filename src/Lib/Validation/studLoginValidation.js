import Validator from "validator";
import isEmpty from "lodash/isEmpty";
import React from "react";

export default function validateInput(email, password) {
  let errors = {};

  if (Validator.isEmpty(email)) {
    errors.email = (
      <span style={{ color: "red", fontSize: 13 }}>Email is required.</span>
    );
  }else{
    if (!Validator.isEmail(email)) {
      errors.email = (
        <span style={{ color: "red", fontSize: 13 }}>Email is invalid.</span>
      );
    // }else if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)){
    }else if(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{1,}))$/.test(email)){   
    }else{
      errors.email = (
        <span style={{ color: "red", fontSize: 13 }}>Email is invalid.</span>
      );
    }

  }
  
  if (Validator.isEmpty(password)) {
    errors.password = (
      <span style={{ color: "red", fontSize: 13 }}>Password is required.</span>
    );
  }


  
  return {
    errors,
    isValid: isEmpty(errors)
  };
}
