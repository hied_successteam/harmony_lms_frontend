import axios from 'axios';
//import { SERVER_URL } from './Constant';
import CONSTANTS from './Constants';
import { getLocalStorage } from './Utils';


const DeleteRequest = (api_url, status) => {

    var instance = '' ;

    if(status == "client"){
        //client server api
         instance = axios.create({
            baseURL: CONSTANTS.CLIENT_SERVER_URL  
        });
    }else{
        //server api
         instance = axios.create({
            baseURL: CONSTANTS.SERVER_URL  
        });
    }
 
    const headers = {
        'Content-Type': 'application/json',
        'Authorization': (getLocalStorage('user') && status !== "client") ? "Bearer " + getLocalStorage('user').accessToken : 'JWT fefege...'    
    }    

    return instance.delete(api_url, { headers: headers });
}


export default DeleteRequest;