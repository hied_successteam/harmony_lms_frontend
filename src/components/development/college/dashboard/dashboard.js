import React, { useEffect, useState } from 'react';
import { Col, Row, Container, Image, Form, Dropdown, Accordion, Card, Button, Modal } from 'react-bootstrap';
import Logoadmin from '../../../../assets/images/svg/logoadmin.svg';
import SearchIcon from '../../../../assets/images/svg/MagnifyingGlass.svg';
import bellIcon from '../../../../assets/images/svg/Bell.svg';
import Bookmark from '../../../../assets/images/svg/BookmarkSimple.svg'; 
import hearts from '../../../../assets/images/svg/hearts.svg';
import heartfill from '../../../../assets/images/svg/heartfill.svg';
import Settings from '../../../../assets/images/svg/GearSix.svg';
import Downloads from '../../../../assets/images/svg/downloads.svg';
import MapPin from '../../../../assets/images/svg/MapPin.svg';
import Hand from '../../../../assets/images/svg/Hand.svg';
import Money from '../../../../assets/images/svg/Money.svg';
import DotsThreeVertical from '../../../../assets/images/svg/DotsThreeVertical.svg';
import UserOne from '../../../../assets/images/teamtop.png';
import logos from '../../../../assets/images/logos.png';
import UserThree from '../../../../assets/images/user3.png';
import Scale from '../../../../assets/images/Scale.png';
import Scaletwo from '../../../../assets/images/Scaletwo.png';
import TipModal from '../../../development/TipModal/TipModal'
import { RangeSlider } from 'rsuite';
import 'rsuite/dist/styles/rsuite-default.css';
import Loader from '../../../../Lib/LoaderNew';
import { setLocalStorage, getLocalStorage, clearLocalStorage, arrToStr } from '../../../../Lib/Utils';
import { studentFavourite, getStudentFavourite, getCollegeDashboard, logout, getCollegeListByID, getCollegeProfile, studentAdmissionProbability } from '../../../../Actions/Student/register';

import { AcademicInfo } from '../../student/wizard/WizardQuestion'
import { major } from '../../../../Actions/Student/register';
import Pagination from "react-bootstrap-4-pagination";
import CONSTANTS from '../../../../Lib/Constants';
import ReactSpeedometer from "react-d3-speedometer"



// // Step 2 - Include the react-fusioncharts component
// import ReactFC from "react-fusioncharts";

// // Step 3 - Include the fusioncharts library
// import FusionCharts from "fusioncharts";

// // Step 4 - Include the chart type
// import Column2D from "fusioncharts/fusioncharts.charts";

// // Step 5 - Include the theme as fusion
// import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";

// // Step 6 - Adding the chart and theme as dependency to the core fusioncharts
// ReactFC.fcRoot(FusionCharts, Column2D, FusionTheme); 


const Dashboard = (props) => {


  //setOffset(props.location.page_data.offset)
      //setnumOfRecord(props.location.page_data.limit)
      //setLocationFilter(props.location.page_data.location)
      //setTermFilter(props.location.page_data.term)
      //setYearFilter(props.location.page_data.year)
      //setStudyFieldFilter(props.location.page_data.studyfield)
      // setMajorFilter(props.location.page_data.major)
      // setDegreeFilter(props.location.page_data.bachelor)
      // setLimit(parseInt(props.location.page_data.limit))
      // setCurrentPage(parseInt(props.location.page_data.offset) + 1)


  const [dashboardData, setDashboardData] = useState('');
  const [loader, setLoader] = useState(false);
  const [userData, setUserData] = useState(false);
  const [spicificMajor, setSpicificMajor] = useState('');
  const [locationFilter, setLocationFilter] = useState(props.location && props.location.page_data && props.location.page_data.location ? props.location.page_data.location : '');
  const [collegeImage, setCollegeImage] = useState('');
  const [termFilter, setTermFilter] = useState(props.location && props.location.page_data && props.location.page_data.term ? props.location.page_data.term : '');
  const [yearFilter, setYearFilter] = useState(props.location && props.location.page_data && props.location.page_data.year ? props.location.page_data.year : '');
  const [degreeFilter, setDegreeFilter] = useState(props.location && props.location.page_data && props.location.page_data.bachelor ? props.location.page_data.bachelor : '');
  const [studyFieldFilter, setStudyFieldFilter] = useState(props.location && props.location.page_data && props.location.page_data.studyfield ? props.location.page_data.studyfield : '');
  const [majorFilter, setMajorFilter] = useState(props.location && props.location.page_data && props.location.page_data.major ? props.location.page_data.major : '');
  const [offset, setOffset] = useState(props.location && props.location.page_data && props.location.page_data.offset ? props.location.page_data.offset : 0);
  const [limit, setLimit] = useState(props.location && props.location.page_data && props.location.page_data.limit ? props.location.page_data.limit : 10);
  const [numOfRecord, setnumOfRecord] = useState(props.location && props.location.page_data && props.location.page_data.limit ? props.location.page_data.limit : 10);
  const [totalPages, setTotalPages] = useState(1);
  const [currentPage, setCurrentPage] = useState(props.location && props.location.page_data && props.location.page_data.offset ? parseInt(props.location.page_data.offset) + 1 : 1);
  const [moreToggle, setMoreToggle] = useState([]); 
  const [selectStudents, setSelectStudents] = useState([]);
  const limitArr = [10, 20, 30, 40, 50];
  const [tipsData, setTipsData] = useState(false);
  const [show, setShow] = useState(false);
  const [localFavourite, setLocalFavourite] = useState('');
  const [allStudents, setAllStudents] = useState(0);
  const [uppDown, setUppDown] = useState(false);
  const [admissionProbabilityData, setAdmissionProbabilityData] = useState([]);
  const [profileCompatibilityData, setProfileCompatibilityData] = useState([]);


  const handleClose = () => setShow(false);


  const handleShow = (tipsData) => {
    setTipsData(tipsData);
    setShow(true);
  }

  // Pagination config
  let paginationConfig = {
    totalPages: totalPages ? totalPages : 1,
    currentPage: currentPage ? currentPage : 1,
    showMax: totalPages === currentPage ? 2 : 5,
    borderColor: currentPage === 1 || totalPages === currentPage ? "#f8f9fa": "#604BE8",
    prevText: currentPage === 1 ? "" : "<",
    nextText: totalPages === currentPage ? "" : ">" ,
    // // size: "lg",
    threeDots: true,
    prevNext: true,
    onClick: function (page) {
      window.scrollTo(0, 0);
      setCurrentPage(page)
      window.scrollTo(0, 0);
      setOffset(page - 1)
      setLimit(numOfRecord)
      dashboardLinsting({
        offset: page - 1,
        limit: parseInt(numOfRecord),
        location: locationFilter,
        term: termFilter,
        year: yearFilter,
        bachelor: degreeFilter,
        studyfield: studyFieldFilter,
        major: majorFilter
      })
    }
  };

  //pagination change function
  const paginationOnChange = (evnt) => {
    window.scrollTo(0, 0);
    setCurrentPage(1)
    setnumOfRecord(evnt.target.value)
    setCurrentPage(1)
    setOffset(0)
    setLimit(parseInt(evnt.target.value))
    dashboardLinsting({
      offset: 0,
      limit: parseInt(evnt.target.value),
      location: locationFilter,
      term: termFilter,
      year: yearFilter,
      bachelor: degreeFilter,
      studyfield: studyFieldFilter,
      major: majorFilter
    })
  }


  // get student list responses
  const dashboardLinsting = (params, sts) => {
    
    if(sts){}else{
      window.scrollTo(0, 0);
      setLoader(true)
      setDashboardData([])
      setAllStudents(0)
    }
    var user = getLocalStorage('user')
    setUserData(user)
    getCollegeDashboard(params).then(res => {
      if (res && res.data && res.data.data && res.data.data.rows) {
        if(sts){}else{
          setAllStudents(res.data.data.total)
        }


        setDashboardData(res.data.data.rows)

        let datas = res.data.data.rows
        let IDS = datas.map((d_item) => {return d_item._id} ) 
        

        if(res.data.data.total <= 10 ){
          setTotalPages(1)
        }else{
          if(Number.isInteger(res.data.data.total / params.limit)){
            setTotalPages(parseInt(res.data.data.total / params.limit))
          }else{
            setTotalPages(parseInt(res.data.data.total / params.limit) + 1)
          }
        }

        getCollegeProfile().then(res => {
          if(res && res.data && res.data.success && res.data.data){
            var payload = {
              studentIds: IDS.toString(),
              UNITID: user.instituteId,
              // instituteReg: res.data.data.instituteReg,
              // aboutTheInstitution: res.data.data.aboutTheInstitution,
              // admissions: res.data.data.admissions,
              // academicsAndExtracurriculars: res.data.data.academicsAndExtracurriculars,
              // campusInfrastructure: res.data.data.campusInfrastructure,
              // placement: res.data.data.placement
            }

             
            //External API call for admission probability
            studentAdmissionProbability(payload).then(resp =>{
              if(resp && resp.data && resp.data.status == "success"){
                setAdmissionProbabilityData(resp.data.data.students)

                // External API call for profile compatibility
                // studentProfileCompatibility(payload).then(response =>{ 
                //   if(resp && resp.data && resp.data.status == "success"){
                //     setProfileCompatibilityData(resp.data.data.institutions)

                    //external API call for college image
                    if (user.instituteType && user.instituteType == 'iped' && user.instituteId) {
                      getCollegeListByID(user.instituteId).then(responseData => {
                        if (responseData && responseData.data && responseData.data.data && responseData.data.data.institutions && responseData.data.data.institutions.IMAGEPATH) {
                          setLoader(false)
                          user.instituteImage = ''
                          setLocalStorage('user', user)
                          setCollegeImage(responseData.data.data.institutions.IMAGEPATH)
                        } else {
                          setLoader(false)
                          setCollegeImage('')
                        }
                      }).catch(err => {setLoader(false)});
                    } else {
                      setCollegeImage('')
                      setLoader(false)
                    }
                //   }else {
                //     setLoader(false)
                //   }
                // }).catch(err => {setLoader(false)})
              }else {
                setLoader(false)
              }
            }).catch(err => {setLoader(false)})
          }else {
            setLoader(false)
          }
        }).catch(err => {setLoader(false)});

      } else {
        setLoader(false)
      }
    }).catch(err => {
      setLoader(false)
    });
  }

  const editProfile = (change) => {
    var data = getLocalStorage("user");
    data.isProfileCompleted = true;
    setLocalStorage("user", data); 
    props.history.push({ pathname: "/college/wizard" });
  };


  //logout API call
  const logoutCall = () => {
    setLoader(true)
    var user = getLocalStorage('user');
    if (user && user.id) {
      logout({
        userId: user.id
      }).then(res => {
        setLoader(false)
        if (res.data.success == true) {
          setLoader(false)
          localStorage.clear();
          clearLocalStorage();
          props.history.push({ pathname: '/college/login' });
        } else {
          setLoader(false)
          localStorage.clear();
          clearLocalStorage();
          props.history.push({ pathname: '/college/login' });
        }
      }).catch(err => {
        setLoader(false)
        localStorage.clear();
        clearLocalStorage();
        props.history.push({ pathname: '/college/login' });
      });
    } else {
      setLoader(false)
      localStorage.clear();
      clearLocalStorage();
      props.history.push({ pathname: '/college/login' });
    }

  }

  const viewMyProfile = () => {
    props.history.push({ pathname: '/college/myProfile' });
  }


  const studentDetailPage = (item) => {
    setLocalStorage('stdDetail', item)
    var params = {
      offset,
      limit: parseInt(numOfRecord),
      location: locationFilter,
      term: termFilter,
      year: yearFilter,
      bachelor: degreeFilter,
      studyfield: studyFieldFilter,
      major: majorFilter,
    }
    props.history.push({ pathname: '/college/studentDetail', studentData: item, page_data: params });
  }

  const getMajorFilter = (value) => {
    if (value) {
      let data = { "fieldOfStudy": value }
      major(data).then(({ data: { success, data, message } }) => {
        if (success !== true) {
          return;
        } else {
          setSpicificMajor(data.rows);
        }
      }).catch((error) => {
        setSpicificMajor([]);
      });
    } else {
      setSpicificMajor([]);
    }

  }

  const filterValueSet = (e, status) => {
    setCurrentPage(1)
    setOffset(0)
    if (status === '1') setLocationFilter(e.target.value)
    if (status === '2') setTermFilter(e.target.value)
    if (status === '6') setYearFilter(e.target.value)
    if (status === '3') setDegreeFilter(e.target.value)
    if (status === '4') {
      setMajorFilter("")
      setStudyFieldFilter(e.target.value)
      getMajorFilter(e.target.value)
    }
    if (status === '5') setMajorFilter(e.target.value)
  }

  const onResetFilter = () => {
    setLocationFilter('')
    setTermFilter('')
    setYearFilter('')
    setDegreeFilter('')
    setStudyFieldFilter('')
    setMajorFilter('')
  }

  // useEffect(() =>{
  //   if(props && props.location && props.location.page_data){
  //     setOffset(props.location.page_data.offset)
  //     setnumOfRecord(props.location.page_data.limit)
  //     setLocationFilter(props.location.page_data.location)
  //     setTermFilter(props.location.page_data.term)
  //     setYearFilter(props.location.page_data.year)
  //     setStudyFieldFilter(props.location.page_data.studyfield)
  //     setMajorFilter(props.location.page_data.major)
  //     setDegreeFilter(props.location.page_data.bachelor)
  //     setLimit(parseInt(props.location.page_data.limit))
  //     setCurrentPage(parseInt(props.location.page_data.offset) + 1)
  //   }else{
  //     setOffset(0)
  //     setnumOfRecord(10)
  //     setLocationFilter('')
  //     setTermFilter('')
  //     setYearFilter('')
  //     setStudyFieldFilter('')
  //     setMajorFilter('')
  //     setDegreeFilter('')
  //     setLimit(10)
  //     setCurrentPage(1)
  //   }
  // },[props])


  useEffect(() => {
    var user = getLocalStorage('user');
    if(user.favourite){
      setLocalFavourite(user.favourite)
    }else{
      user.favourite = []
      setLocalStorage('user', user)
    }
    
    var params = {
      offset: offset,
      limit: parseInt(numOfRecord),
      location: locationFilter,
      term: termFilter,
      year: yearFilter,
      bachelor: degreeFilter,
      studyfield: studyFieldFilter,
      major: majorFilter,
    }
    
    dashboardLinsting(params)
  }, [locationFilter, termFilter, yearFilter, degreeFilter, studyFieldFilter, majorFilter])


  const favoriteApiCall = (id, type) => {
    var data = getLocalStorage('user')
    //setLoader(true)
    studentFavourite({ favouriteId: id, type }).then(res => {
      if (res.data.success == true) {
        // if (type === true) {
        //   if (data.favourite) {
        //     data.favourite.push(id)
        //   } else {
        //     data.favourite = [id]
        //   }
        //   setLocalFavourite(data.favourite)
        //   setLocalStorage('user', data);
        // } else {
        //   let ArryVale = data.favourite;
        //   var indexB = ArryVale.indexOf(id);
        //   if (indexB > -1) {
        //     ArryVale.splice(indexB, 1);
        //     data.favourite = [...ArryVale]
        //     setLocalFavourite(data.favourite)
        //     setLocalStorage('user', data);
        //   }
        // }


        var params = {
          offset,
          limit: parseInt(numOfRecord),
          location: locationFilter,
          term: termFilter,
          year: yearFilter,
          bachelor: degreeFilter,
          studyfield: studyFieldFilter,
          major: majorFilter,
        }
        dashboardLinsting(params, true)
      } else {
        setLoader(false)
      }
    }).catch(err => { setLoader(true) })
  }

  const goTOFavouritePage = () => {
    props.history.push({ pathname: '/college/favouriteStudent' });
  }


  const manageFavouriteStatus = (id) => {
    var userFavourite = localFavourite
    if (userFavourite && userFavourite.length) {
      var response = userFavourite.find(element => element == id)
      if (response) {
        return true
      } else {
        return false
      }
    } else {
      return false
    }
  }

  //Manage more toggle option
  const moreToggleOption = (value) => {    

    let ArryVale = moreToggle;
    var indexB = ArryVale.indexOf(value);
    if (indexB > -1) {
      ArryVale.splice(indexB, 1);
      setMoreToggle([...ArryVale]);      
    } else {
      ArryVale.push(value);
      setMoreToggle([...ArryVale]);      
    }       

  }

  const onCompare = () => {
    if (selectStudents && selectStudents.length) {
      if (selectStudents.length <= 4 && selectStudents.length >= 2) {
        setLocalStorage('compareData', selectStudents)
        props.history.push({ pathname: '/college/comparison', data: selectStudents });
      } else {
        handleShow("Less than 2 and More than 4 students can't compare at a time")
      }
    }
  }

  //select check box
  const oncheckBox = (value) => {    
    let ArryVal = selectStudents;
    var indexB = ArryVal.indexOf(value);
    if (indexB > -1) {
      ArryVal.splice(indexB, 1);
      setSelectStudents([...ArryVal]);      
    } else {
      ArryVal.push(value);
      setSelectStudents([...ArryVal]);      
    }       
  }

  const accordianClick = () =>{
    uppDown ? setUppDown(false) : setUppDown(true)
  }

  // const mangeProfileMatch = (id) => {
  //   var isFound = false;
  //   profileCompatibilityData &&
  //     profileCompatibilityData.length > 0 &&
  //     profileCompatibilityData.map((item) => {
  //       if (item.studentId === id) {
  //         isFound = item.PROFMATCH;
  //       }
  //     });
  //   if (isFound) {
  //     return Math.round(isFound);
  //   }
  // };

  const mangeAdminProbability = (id, status) => {
    var isFound = false;
    admissionProbabilityData &&
      admissionProbabilityData.length > 0 &&
      admissionProbabilityData.map((item) => {
        if (item.STUDENTID === id) {
          if(status == "profile_Match"){
            isFound = item.PROF_MATCH;
          }
          if(status == "admission_prob"){
            isFound = item.ADM_PROB;
          }

        }
      });
    if (isFound) {
      return Math.round(isFound);
    }
  };



  return (
    <div className="collegeAdminUi">
      <Container fluid> 
        <Row>
          <Col md={8} className="topHeaderBox bgCol25 pl-0 pr-0">
            <div className="pl-4 pr-4 pt-4">
              <Row>
                <Col md={4}>
                  <Image src={Logoadmin} alt="HiEd Success" title="Welecome to HiEd Success" className="logos" />
                  <span className="fs20 col24 fw700 ml-1">HiEd Harmony</span>
                </Col>
                <Col md={5}>
                  {/* <div className="searchBox d-flex justify-content-center">
                    <Form.Group className="position-relative w-100">
                      <Form.Control type="text" placeholder="Search..." className="searchType1 shadow3 w-100" />
                      <Image src={SearchIcon} alt="Search" className="pointer searchOne" />
                    </Form.Group>
                  </div> */}
                </Col>
                <Col md={3}>
                  <div className="notifications d-flex justify-content-end">
                    <div className="bookMark ml-4">
                      <span className="d-inline-block bgCol1 br50 text-center">
                        <Image src={hearts} alt="Search" className="pointer" onClick={() => goTOFavouritePage()} />
                      </span>
                    </div>
                  </div>
                </Col>
              </Row>
              <div className="middileAccordian mt-4">
                <Accordion >
                  <Card className="shadow4">
                    <Card.Header onClick={() => accordianClick()}>
                      <Accordion.Toggle variant="link" eventKey="0">
                        Filters <span className="ml-4 col8" onClick={() => onResetFilter()}>Reset</span>
                         <i className= {`fa fa-chevron-down arrowA ${uppDown? "rotateArrow": ""} `} aria-hidden="true"></i>
                      </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="0">
                      <Card.Body className="bgCol3">
                        <div>
                          <Row>
                            <Col md={3}>
                              <Form.Group controlId="exampleOne" className="mb-2">
                                <Form.Label className="fw600 col2 mb-1">
                                  <span className="mr-3">Location</span>
                                </Form.Label>
                                <Form.Control as="select" className="selectTyp1 select2 pointer"
                                  value={locationFilter || ''}
                                  onChange={(e) => filterValueSet(e, '1')}>
                                  <option value="">Select</option>
                                  <option value="In state">In state</option>
                                  <option value="International">International</option>
                                </Form.Control>
                              </Form.Group>
                            </Col>
                            <Col md={3}>
                              <Form.Group controlId="exampleOne" className="mb-2">
                                <Form.Label className="fw600 col2 mb-1">
                                  <span className="mr-3">Term</span>
                                </Form.Label>
                                <Form.Control as="select" className="selectTyp1 select2 pointer"
                                  value={termFilter || ''}
                                  onChange={(e) => filterValueSet(e, '2')}>
                                  <option value="">Select</option>
                                  {
                                    AcademicInfo && AcademicInfo.map((f_item, index) =>
                                      f_item.code == "SEMESTER" && f_item.questions[0] && f_item.questions[0].options.length > 0 &&
                                      f_item.questions[0].options.map((f_item_1, index_1) => (
                                        <option key={index + index_1} value={f_item_1.value}>{f_item_1.value}</option>
                                      ))
                                    )
                                  }
                                </Form.Control>
                              </Form.Group>
                            </Col>
                            <Col md={3}>
                              <Form.Group controlId="exampleOne" className="mb-2">
                                <Form.Label className="fw600 col2 mb-1">
                                  <span className="mr-3">Batch/Year</span>
                                </Form.Label>
                                <Form.Control as="select" className="selectTyp1 select2 pointer"
                                  value={yearFilter || ''}
                                  onChange={(e) => filterValueSet(e, '6')}>
                                  <option value="">Select</option>
                                  {
                                    AcademicInfo && AcademicInfo.map((f_item, index) =>
                                      f_item.code == "SEMESTER" && f_item.questions[1] && f_item.questions[1].options.length > 0 &&
                                      f_item.questions[1].options.map((f_item_1, index_1) => (
                                        <option key={index + index_1} value={f_item_1.value}>{f_item_1.value}</option>
                                      ))
                                    )
                                  }
                                </Form.Control>
                              </Form.Group>
                            </Col>
                            <Col md={3}>
                              <Form.Group controlId="exampleOne" className="mb-2">
                                <Form.Label className="fw600 col2 mb-1">
                                  <span className="mr-3">Degree applying for</span>
                                </Form.Label>
                                <Form.Control as="select" className="selectTyp1 select2 pointer"
                                  value={degreeFilter || ''}
                                  onChange={(e) => filterValueSet(e, '3')}>
                                  <option value="">Select</option>
                                  <option value="Associates">Associates</option>
                                  <option value="Bachelor’s Degree (BS/BA)">Bachelor’s Degree (BS/BA)</option>
                                  <option value="Bachelor (BA/BS)">Bachelor (BA/BS)</option>
                                  <option value="Diploma">Diploma</option>
                                  <option value="Doctorate / PhD / EdD">Doctorate / PhD / EdD</option>
                                  <option value="Integrated Programs">Integrated Programs</option>
                                  <option value="Masters (General)">Masters (General)</option>
                                  <option value="Masters (Science)">Masters (Science)</option>
                                  <option value="Masters (Management)">Masters (Management)</option>
                                  <option value="Occupational Associates">Occupational Associates</option>
                                  <option value="Post Doctoral">Post Doctoral</option>
                                  <option value="Post Graduate Certificates">Post Graduate Certificates</option>
                                  <option value="Professional Certificate">Professional Certificate</option>
                                  <option value="Summer Internships">Summer Internships</option>
                                  <option value="Transfer Degree">Transfer Degree</option>
                                  <option value="Vocational Training">Vocational Training</option>
                                  {/* {
                                    AcademicInfo && AcademicInfo.map((f_item, index) =>
                                      f_item.code == "HIGHDEGEDU" && f_item.options.length > 0 &&
                                      f_item.options.map((f_item_1, index_1) => (
                                        <option key={index + index_1} value={f_item_1.value}>{f_item_1.value}</option>
                                      ))
                                    )
                                  } */}
                                </Form.Control>
                              </Form.Group>
                            </Col>
                            <Col md={3}>
                              <Form.Group controlId="exampleOne" className="mb-2">
                                <Form.Label className="fw600 col2 mb-1">
                                  <span className="mr-3">Field of Study</span>
                                </Form.Label>
                                <Form.Control as="select" className="selectTyp1 select2 "
                                  value={studyFieldFilter || ''}
                                  onChange={(e) => filterValueSet(e, '4')}>
                                  <option value="">Select</option>
                                  {
                                    AcademicInfo && AcademicInfo.map((f_item, index) =>
                                      f_item.code == "STUDYFIELD" && f_item.options.length > 0 &&
                                      f_item.options.map((f_item_1, index_1) => (
                                        <option key={index + index_1} value={f_item_1.value}>{f_item_1.value}</option>
                                      ))
                                    )
                                  }
                                </Form.Control>
                              </Form.Group>
                            </Col>
                            <Col md={3}>
                              <Form.Group controlId="exampleOne" className="mb-2">
                                <Form.Label className="fw600 col2 mb-1">
                                  <span className="mr-3">Major</span>
                                </Form.Label>
                                <Form.Control as="select" className="selectTyp1 select2 pointer"
                                  value={majorFilter || ''}
                                  onChange={(e) => filterValueSet(e, '5')}>
                                  <option value="">Select</option>
                                  {
                                    spicificMajor && spicificMajor.length > 0 &&
                                    spicificMajor.map((f_item, index) =>
                                      <option key={index} value={f_item.name}>{f_item.name}</option>
                                    )
                                  }
                                </Form.Control>
                              </Form.Group>
                            </Col>

                          </Row>

                        </div>
                      </Card.Body>
                    </Accordion.Collapse>
                  </Card>
                </Accordion>
                <div className="mt-4 pt-2">
                  <Row>
                    <Col md={6}>
                      <div className="col2 fs22 fw500">Candidates Results ({allStudents && allStudents})</div>
                      <div className="col5 fs14 fw400">Eligible students matching your criteria</div>
                    </Col>
                    <Col md={6}>
                      <div className="d-flex flex-wrap justify-content-end">
                        {/* <div className="mr-1 fs14 fw600 col2 pt-1">Sort by:
                        </div> */}
                        {/* <div className="mr-3">
                          <Dropdown>
                            <Dropdown.Toggle id="dropdown-basic" className="DropdownType1 col2 fs14 fw400 pt-0 pb-0">
                              Admission Probability <i className="fa fa-chevron-down ml-1 col2" aria-hidden="true"></i>
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                              <Dropdown.Item >Profile</Dropdown.Item>
                              <Dropdown.Item href="#">Logout</Dropdown.Item>
                            </Dropdown.Menu>
                          </Dropdown>
                        </div> */}
                        {/* <div className="mr-3">
                          <Image src={Downloads} alt="Icon" className="pointer " />
                        </div> */}
                        <div>
                          <Button
                            type="button"
                            disabled={selectStudents && selectStudents.length > 0 ? false : true}
                            className={`btnType5`}
                            onClick={() => onCompare()}>Compare</Button>
                        </div>
                      </div>
                    </Col>
                  </Row>
                </div>



                <div className="mt-4">
                  {
                    dashboardData && dashboardData.length > 0 ?
                    dashboardData.map((item, index) => {
                      return <React.Fragment key={index}>
                        <Row >
                          <Col md={6}>
                            <div className="d-flex">     
                              <input type="checkbox" 
                                checked={selectStudents &&
                                    selectStudents.length &&
                                    selectStudents.indexOf(item._id) > -1 ? true : false} 
                                    onChange={() => oncheckBox(item._id)}  
                                    className="checkboxTyp5"
                              />
                              {/* <Form.Group 
                              controlId={item._id} 
                              className="checkboxTyp1 checkTy1">
                                <Form.Check  
                                  type="checkbox"
                                  className={`pointer ${
                                    selectStudents &&
                                    selectStudents.length &&
                                    selectStudents.indexOf(item._id) > -1
                                      ? "form-checkSelect"
                                      : ""
                                  }  `}
                                  onChange={() => oncheckBox(item._id)}
                                  label=""
                                />
                              </Form.Group> */}
                              
                              <div className='overflow-h'>     
                                <div className="mb-2">    
                                  <div className="mb-2 col2 mt-10 text-capitalize fw600 fs20">{item.name && item.name}</div>
                                  <div className="d-flex flex-wrap">
                                    <div className="mr-3 col5 fs14 fw500 d-flex align-items-center">
                                      {item.city && item.city + ', '}
                                      {item.state && item.state + ', '}
                                      {item.country && item.country}
                                    </div>
                                    {/* <div className="mr-3 col5 fs14 fw500 d-flex align-items-center">
                                      <Image src={Hand} alt="Icon" className="pointer mr-1" />
                                      40 Hours
                                    </div>
                                    <div className="col5 fs14 fw500 d-flex align-items-center">
                                      <Image src={Money} alt="Icon" className="pointer mr-1" />
                                      Need Financing
                                    </div> */}
                                  </div>
                                </div>
                                <div className="col29 fw500 fs14 mb-1">Applying for: 
                                  <span className="actives text-uppercase fw500 br40 col2 fs12 mr-2 mb-2">
                                    {} {item.edudegnext}
                                  </span>
                                </div>
                                <div className="col29 fw500 fs14 mb-1">{item.fieldOfInterest && "Field of Interest: "}</div>
                                {
                                  item.fieldOfInterest &&
                                  <div className="d-flex flex-wrap mb-4">
                                    <div className="tagTitle actives text-uppercase fw500 br40 col2 fs12 mr-2 mb-2">
                                      {item.fieldOfInterest}
                                    </div>
                                  </div>
                                }             
                                <div className="d-flex flex-wrap">                                                         
                                  <div className="mr-3">  
                                    {
                                      item.favourite ?
                                        <span className="d-inline-block bgCol1 grayCol wishListOne br50 text-center">
                                          <Image src={hearts} alt="Search" className="pointer" onClick={() => favoriteApiCall(item._id, false)} /> 
                                        </span>
                                      :
                                        <span className="d-inline-block grayCol wishListOne br50 text-center">
                                        <Image src={hearts} alt="Search" className="pointer" onClick={() => favoriteApiCall(item._id, true)} /> 
                                        </span>
                                    }

                                  </div>
                                  <div className="mr-4">
                                    <Button type="button" className="btnType5" onClick={() => studentDetailPage(item)}>View Details</Button>
                                  </div>
                                  <div>
                                    <Dropdown>
                                      <Dropdown.Toggle id="dropdown-basic2"
                                        onClick={() => moreToggleOption(item._id)}
                                        className="DropdownType1 col2 fs14 fw600 pt-0 pb-0">
                                        More
                                        <i className="fa fa-caret-down col2 ml-1" aria-hidden="true"></i>
                                      </Dropdown.Toggle>
                                    </Dropdown>
                                  </div>
                                  {/* <div>
                                    <Dropdown>
                                      <Dropdown.Toggle id="dropdown-basic2" className="DropdownType1 col2 fs14 fw600 pt-0 pb-0">
                                        More
                                        <i className="fa fa-caret-down col2 ml-1" aria-hidden="true"></i>
                                      </Dropdown.Toggle>
                                      <Dropdown.Menu>
                                        <Dropdown.Item href="#">Profile</Dropdown.Item>
                                        <Dropdown.Item href="#">Setting</Dropdown.Item>
                                        <Dropdown.Item href="#">Logout</Dropdown.Item>
                                      </Dropdown.Menu>
                                    </Dropdown>
                                  </div> */}
                                </div>
                              </div>
                            </div>
                          </Col>
                          <Col md={6} className="row min15">
                            <Col md={6} className="speedMeter pr0">
                              <ReactSpeedometer
                                width={190}          
                                maxheight={100}
                                height={115}
                                needleHeightRatio={0.7}
                                value={mangeAdminProbability(item._id, "admission_prob")}
                                maxValue={100}
                                customSegmentStops={[0, 33, 66, 100]}
                                segmentColors={['#FE5E54', '#FFDB1B', '#89D667']}
                                currentValueText={'${value}%'}
                                segments={3}
                                maxSegmentLabels={0}
                                ringWidth={10}
                                needleTransitionDuration={33}
                                needleTransition="easeElastic"
                                needleColor={'#a7ff83'}
                                textColor={'#000000'}
                              /> 
                              <span className="titleGraph">Admission Probability</span>  
                            </Col>

                            <Col md={6} className="speedMeter pr0"> 

                               <ReactSpeedometer
                                width={190}
                                height={115}                                            
                                maxheight={100}
                                needleHeightRatio={0.7}
                                value={mangeAdminProbability(item._id, "profile_Match")}
                                maxValue={100}
                                customSegmentStops={[0, 33, 66, 100]}
                                segmentColors={['#FE5E54', '#FFDB1B', '#89D667']}
                                currentValueText={'${value}%'}
                                segments={3}
                                maxSegmentLabels={0}
                                ringWidth={10}
                                needleTransitionDuration={33}
                                needleTransition="easeElastic"
                                needleColor={'#a7ff83'}
                                textColor={'#000000'}
                              />   
                              <span className="titleGraph">Profile Compatibility</span> 
                            </Col>

                          </Col>                          
                         { moreToggle && moreToggle.length > 0 && moreToggle.indexOf(item._id) > -1 ?
                          <React.Fragment>
                            <Col md={1}></Col>
                            <Col md={11}>
                              <div className="popularType1">
                                <ul className="d-flex flex-wrap pl-1 w-100">
                                  <li>Term: {item.term && item.term}</li>
                                  <li>Entrances Taken/Appearing: { }
                                    {/* {
                                      item.entranceappeared && item.entranceappeared.length > 0 && item.entranceappeared.map((entr) =>{
                                        return entr
                                      })
                                    } */}
                                     {item.entranceappeared && item.entranceappeared.length > 0 ? arrToStr(item.entranceappeared) :'-'}
                                  </li>
                                  <li>Highest degree: {item.bachelor && item.bachelor}</li>
                                </ul>
                                {

                                  <ul className="d-flex flex-wrap pl-1 w-100">
                                    <li className="flex1"><span className="d-block">Major:</span>
                                      {
                                        item.major && item.major.length > 0 && item.major.map((fields) => {
                                          return <div className="tagTitle mt-1 actives text-uppercase fw500 br40 col2 fs14 mr-2 mb-1 pointer d-inline-block">
                                            {fields}
                                          </div>
                                        })
                                      }</li>
                                  </ul>
                                }
                              </div>
                            </Col>
                          </React.Fragment>
                          : '' }
                        </Row>
                        <hr className="borderOne" />
                      </React.Fragment>
                    })
                    :
                    <div className="noStudent">Record not Found.</div>
                  }

                </div>



              </div>
            </div>


            {
              dashboardData && dashboardData.length > 0 && allStudents > 10 &&
              <div className="d-flex pl-4 pr-4 mt-5 mb-5 justify-content-between">
                <div className="d-flex align-items-center">
                  <div className="mr-2">Show</div>
                  <Form.Control
                    as="select"
                    className="selectTyp1 selectNumber1"
                    onChange={(e) => paginationOnChange(e)}
                    value={numOfRecord}
                  >
                    {limitArr.map((cVal, cIndx) => {
                      return <option key={cIndx} value={cVal}>{cVal}</option>
                    })}
                  </Form.Control>
                </div>
                <div className="paginationType4">
                  <Pagination {...paginationConfig} />
                </div>
              </div>
            } 
            

          </Col>
          <Col md={4} className="topHeaderBox bgCol28 pl-0 pr-0">
            <div className="rightSidebar pl-4 pr-4 pt-4">
              <div className="powerBtn d-flex justify-content-end">
                {/* <div className="mr-4">
                  <Dropdown>
                    <Dropdown.Toggle id="dropdown-basic" className="DropdownType1">
                      <Image src={bellIcon} alt="Search" className="pointer setingOne mt-1" />
                      <span className="fs10 d-inline-block bgCol27 br50 text-center">2</span>
                    </Dropdown.Toggle>
                  </Dropdown>
                </div>
                <Image src={Settings} alt="Search" className="pointer setingOne mr-5" /> */}
                <div>
                  <Dropdown>
                    <Dropdown.Toggle id="dropdown-basic" className="DropdownType1 col2 fs14 fw600">
                      {/* <Image src={UserThree} alt="Search" className="pointer user1 mr-1" /> */}
                      {userData && userData.firstName}
                      <i className="fa fa-chevron-down ml-1 col8" aria-hidden="true"></i>
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                      <Dropdown.Item onClick={() => editProfile()}>
                        Edit Profile
                      </Dropdown.Item>
                      <Dropdown.Item onClick={() => viewMyProfile()}>View Profile</Dropdown.Item>
                      <Dropdown.Item onClick={() => logoutCall()}>Logout</Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
              </div>
              <div className="mt-5 text-center mb-3">
                {
                  collegeImage ?
                    <Image src={CONSTANTS.CLIENT_SERVER_IMAGEPATH + collegeImage} alt="User" className="br20 mhSet" />
                    :
                    <Image src={'https://edulytics.hiedsuccess.com/logo.png'} alt="User" className="br20 mhSet" />
                }
              </div>
              <div className="text-center mb-4 pb-2">
                <div className="col2 fs22 fw600 mb-2">{userData && userData.instituteName}</div>
                <div className="col29 fw500">
                  {userData && userData.state && userData.state + ', '}
                  United States
                </div>
              </div>
              {/* <div className="boxLayout bgCol30 br4 p-3 mt-3">
                <Row>
                  <Col md={3}>
                    <div className="d-flex justify-content-center align-items-center">
                      <div className="text-center">
                        <div className="col3 fw700 fs24">40%</div>
                        <div className="col3 fs12 fw500">Done</div>
                      </div>
                    </div>
                  </Col>
                  <Col md={9}>
                    <div className="col3 fw400 fs18">A 100% complete profile gets you free access to the first 50 students*</div>
                  </Col>
                </Row>
              </div> */}
            </div>



          </Col>


        </Row>
      </Container>


      <Modal show={loader} centered className="loaderModal"><Loader /></Modal>
      
      <TipModal tipsData={tipsData} title={"Info"} show={show} handleClose={handleClose}></TipModal>

    </div>
  )
}
export default Dashboard;




