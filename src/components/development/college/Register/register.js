import React, { useState } from 'react';
import { Col, Row, Container, Image, Form, Button } from 'react-bootstrap';
import axios from 'axios';
import IntlTelInput from 'react-intl-tel-input';
import Loader from "react-js-loader";
import 'react-intl-tel-input/dist/main.css';
import AsyncSelect from 'react-select/async';
import Logos from '../../../../assets/images/logos.png';
import { register } from '../../../../Actions/Student/register'
import validateInput from '../../../../Lib/Validation/collegeRegValidation'
import CONSTANTS from '../../../../Lib/Constants';
import TipModal from '../../../development/TipModal/TipModal'

const CollegeRegister = (props) => {  

  const [instituteId, setInstituteId] = useState('');
  const [instituteName, setInstituteName] = useState('');
  const [instituteAddress, setinstituteAddress] = useState('');
  const [userName, setUserName] = useState('');
  const [email, setEmail] = useState('');
  const [mobile, setMobile] = useState('');
  const [countryCode, setCountryCode] = useState('');
  const [password, setPassword] = useState('');
  const [confPassword, setConfPassword] = useState('');
  const [errors, setErrors] = useState({});
  const [check, setCheck] = useState(false);
  const [success, setSuccess] = useState(false);
  const [message, setMessage] = useState('');
  const [loading, setLoading] = useState(false);
  const [isOtherCollege, setIsOtherCollege] = useState(false);

  const [inputValue, setValue] = useState('');
  const [selectedValue, setSelectedValue] = useState('');
  const [openOptions, setOpenOptions] = useState(null);
  const [tipsData, setTipsData] = useState(false);
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);

  const handleShow = (tipsData) => {
    setTipsData(tipsData);
    setShow(true);
  }


  //get country list
  // useEffect(() => {
  //      InstituteList().then(res => {
  //           setInstitueData(res.data.data)

  //      }).catch(err => {
  //           console.log(err)
  //      });
  // }, []);


  //go to login in page
  const goToLogin = () => {
    props.history.push({ pathname: 'Login' });
  }

  const formatPhoneNumberOutput = (a, b, c) => {
    //props.history.push({ pathname: 'Login' }); 
    let num = /^[0-9\b]+$/;
    if (b == '' || num.test(b)) {
      if (b.length < 11) {
        setMobile(b);
        setCountryCode(c.dialCode);
      }
    }
  }

  const formatPhoneNumberOutputFlag = (b, c) => {
    //props.history.push({ pathname: 'Login' }); 
    let num = /^[0-9\b]+$/;
    if (b == '' || num.test(b)) {
      if (b.length < 11) {
        setMobile(b);
        setCountryCode(c.dialCode);
      }
    }
  }


  // const handleOnChange = (e) => {
  //   if (e.target.name == 'instituteId') {
  //     const myArr = e.target.value.split("^");
  //     setInstituteId(myArr[0]);
  //     //setInstituteIdString(e.target.value);
  //     if (myArr[1]) {
  //       setInstituteName(myArr[1])
  //     } else {
  //       setInstituteName('');
  //     }
  //     //instituteName
  //   }
  // };

  const conditionCheck = (e) => {
    setCheck(check ? false : true)
  }

  const selectOtherCollege = (e) =>{
    if(isOtherCollege){
      setIsOtherCollege(false)
      setInstituteName('');
      setInstituteId('');
      setSelectedValue('')
    }else{
      setIsOtherCollege(true)
      setInstituteName('');
      setInstituteId(0);
    }
  }

  const isValid = () => {

    let data = {
      instituteId,
      instituteName,
      instituteAddress,
      userName,
      email,
      mobile,
      countryCode,
      password,
      confPassword
    };

    const { errors, isValid } = validateInput(data);
    if (!isValid) {
      setErrors(errors)
    }
    return isValid;
  }


  //submit register data
  const registerSubmit = () => {
    if (isValid()) {
      setErrors({})
      setLoading(true)

      let data = {
        instituteId: isOtherCollege == true ? 0 : instituteId,
        collegeType: isOtherCollege == true ? "other" : "iped",
        instituteType: isOtherCollege == true ? "other" : "iped",
        instituteName,
        instituteAddress,
        userName,
        email,
        mobileNumber: mobile,
        countryCode,
        password,
        confirmPassword: password,
        roleId: 2,
        userType: "college"
      };


      register(data).then(({ data: { success, data, message } }) => {

        setMessage(message);
        setSuccess(true)
        setLoading(false)
        setTimeout(() => {
          setSuccess(false)
        }, 30000);

        if (success !== true) {
          //setMessage(message);                                                
          return;
        } else {
          // setMessage(registermsg); 
          setInstituteId('');
          //setInstituteIdString('');
          setInstituteName('');
          setinstituteAddress('');
          setUserName('');
          setEmail('');
          setMobile('');
          setPassword('');
          setConfPassword('');
          setErrors({});
          setCheck(false);

          props.history.push({ pathname: 'success' });

        }
      }).catch((err) => { 
          setLoading(true) 
        });

    }
  }

  //go to landing page
  const goToLanding = () => {
    props.history.push({ pathname: '/landing/dashboard' });
  }

  // handle input change event
  const handleInputChange = value => {
    setValue(value);
  };

  // handle selection
  const handleChange = value => {
    setSelectedValue(value);
    setInstituteName(value.INSTNM);
    setInstituteId(value.UNITID);
  }

  // load options using API call
  const loadOptions = (inputValue) => {
    if (inputValue && inputValue.length >= 3) {
      //const searchUrlNew = `http://192.168.1.10:5000/api/institutions/${inputValue}`;
      const searchUrl = `http://192.168.1.10:5000/api/institutions/${inputValue}`;
      return axios.get(searchUrl).then((res) => {
        setOpenOptions(res.data.data.institutions)
        return res.data.data.institutions;
      });

    } else {
      return [];
    }
  };



  return (
    <div className="bgBanner d-flex align-items-center justify-content-center w-100">
      <Container>
        <Col lg={10} md={10} className="m-auto">
          <div className="studentBox text-center bgCol3 shadow1 br10 registerUsers">
            <div>
              <Image src={Logos} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" onClick={() => goToLanding()} />
              <div className="studentOne mt-4 text-left">
                <div className="fs22 fw600 col2 mb-1 text-center">Sign Up</div>
                <div className="fw500 col5 mb-4 text-center">Create your account</div>
                {
                  success &&
                  <div className="fw500 col5 mb-3 text-center mb-4 pb-1" style={{ color: 'red' }}>{message}</div>
                }
                <Form className="formLayoutUi">
                  <Row>

                    {
                      isOtherCollege == false ?
                    
                      <Col md={6}>
                        <Form.Group controlId="exampleForm.ControlSelect1">
                          <AsyncSelect
                            noOptionsMessage={() => null}
                            //isMulti
                            cacheOptions
                            defaultOptions={openOptions}
                            value={selectedValue}
                            getOptionLabel={e => e.INSTNM}
                            getOptionValue={e => e.UNITID}
                            loadOptions={inputValue && inputValue.length >= 3 ? loadOptions : ''}
                            onInputChange={handleInputChange}
                            onChange={handleChange}
                            //closeMenuOnSelect={true}
                            placeholder="College Name"
                            className="basic-multi-select selectTyp2"
                            classNamePrefix="select"
                            isSearchable={true}
                          //isLoading = {false}                                                           

                          />
                          {
                            errors.instituteId &&
                            <span className="help-block error-text">
                              <span style={{ color: "red", fontSize: 13 }}>{errors.instituteId}</span>
                            </span>
                          }
                        </Form.Group>
                      </Col>
                      :
                      <Col md={6}>
                        <Form.Group controlId="formBasicEmailA">
                          <Form.Control
                            name="collegename"
                            value={instituteName}
                            type="text"
                            placeholder="College Name"
                            maxLength={35}
                            className="inputType1"
                            onChange={(e) => setInstituteName(e.target.value)}
                          />
                          {
                            errors.instituteName &&
                            <span className="help-block error-text">
                              <span style={{ color: "red", fontSize: 13 }}>{errors.instituteName}</span>
                            </span>
                          }
                        </Form.Group>
                      </Col>
                    }


                    <Col md={6}>
                      <Form.Group controlId="formBasicEmailA">
                        <Form.Control
                          name="userName"
                          value={userName}
                          type="text"
                          placeholder="Full Name"
                          maxLength={35}
                          className="inputType1"
                          onChange={(e) => setUserName(e.target.value)}
                        />
                        {
                          errors.userName &&
                          <span className="help-block error-text">
                            <span style={{ color: "red", fontSize: 13 }}>{errors.userName}</span>
                          </span>
                        }
                      </Form.Group>
                    </Col>

                    <Col md={12}>
                      <Form.Group controlId="_formBasicCheckbox">
                        <Form.Check
                          type="checkbox"
                          checked={isOtherCollege ? true : false}
                          className="checkboxTyp1 mt0"
                          onChange={(e) => selectOtherCollege(e)}
                          label="Other College Name" />
                      </Form.Group>
                    </Col>

                    


                    <Col md={6}>
                      <Form.Group controlId="formBasicEmailG">
                        <Form.Control
                          type="email"
                          placeholder="Email"
                          className="inputType1"
                          value={email}
                          onChange={(e) => setEmail(e.target.value)}
                        />
                        {
                          errors.email &&
                          <span className="help-block error-text">
                            <span style={{ color: "red", fontSize: 13 }}>{errors.email}</span>
                          </span>
                        }
                      </Form.Group>
                    </Col>
                    {/* <Col md={6}>
                        <Form.Group
                              controlId="formBasicEmail"
                              className="flagSelect">
                              <Form.Control
                                  type="text"
                                  maxlength="10"
                                  placeholder="Contact Number"
                                  className="inputType1"
                                  value={mobile}
                                  onChange={(e) =>
                                        e.target.value.match('^[+0-9 ]*$') != null ?
                                            setMobile(e.target.value)
                                            : ''
                                  } />                                                       
                              {
                                  errors.mobile &&
                                  <span className="help-block error-text">
                                        <span style={{ color: "red", fontSize: 13 }}>{errors.mobile}</span>
                                  </span>
                              }
                        </Form.Group>
                      </Col> */}

                    <Col md={6}>
                      <Form.Group controlId="formBasicEmailE" className="mb-4 flagSelect">
                        <IntlTelInput
                          containerClassName="intl-tel-input"
                          inputClassName="form-control inputType1"
                          placeholder="Contact Number"
                          onPhoneNumberChange={(...args) => {
                            formatPhoneNumberOutput(...args);
                          }}
                          onSelectFlag={(...args) => {
                            formatPhoneNumberOutputFlag(...args);
                          }}
                          name="mobile"
                          value={mobile}
                          formatOnInit={true}
                        />
                        {
                          errors.mobile &&
                          <span className="help-block error-text">
                            <span style={{ color: "red", fontSize: 13 }}>{errors.mobile}</span>
                          </span>
                        }
                      </Form.Group>
                    </Col>


                    <Col md={6}>
                      <Form.Group controlId="formBasicEmailF">
                        <Form.Control
                          type="password"
                          placeholder="Password"
                          value={password}
                          onChange={(e) => setPassword(e.target.value)}
                          className="inputType1" />
                        {
                          errors.password &&
                          <span className="help-block error-text">
                            <span style={{ color: "red", fontSize: 13 }}>{errors.password}</span>
                          </span>
                        }
                      </Form.Group>
                    </Col>
                    <Col md={6}>
                      <Form.Group controlId="formBasicEmailGA">
                        <Form.Control
                          type="password"
                          value={confPassword}
                          placeholder="Confirm Password"
                          onChange={(e) => setConfPassword(e.target.value)}
                          className="inputType1" />
                        {
                          errors.confPassword &&
                          <span className="help-block error-text">
                            <span style={{ color: "red", fontSize: 13 }}>{errors.confPassword}</span>
                          </span>
                        }
                      </Form.Group>
                    </Col>
                    <Col md={12}>
                      <Form.Group controlId="formBasicCheckbox">
                        <Form.Check
                          type="checkbox"
                          checked={check ? true : false}  
                          className="checkboxTyp1 termOne"    
                          onChange={(e) => conditionCheck(e)}
                          label="" /> 
                          <span className="left-pad topSet">I agree to the <span className="fw600 pointer" onClick={() => handleShow('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.')}>Terms & Conditions</span></span>  
                      </Form.Group>
                      <div className="mt-4 align-sm-center text-center">
                        <Button
                          disabled={check ? false : true}
                          className="btnType4 fw600 mr-4 mb-3" onClick={() => registerSubmit()}>
                          {
                            loading ?
                              <Loader type="bubble-top" bgColor={"#414180"} size={25} />
                              :
                              "Sign Up"
                          }
                        </Button>
                        <div className="fw500 col5">Already have an account?
                          <span className="col1 fw700 pointer ml-2" onClick={() => goToLogin()}>Sign In</span></div>
                      </div>
                    </Col>
                  </Row>
                </Form>
              </div>
            </div>
          </div>
        </Col>
      </Container>

      <TipModal tipsData= {tipsData} title={"Terms & Conditions"} show= {show} handleClose= {handleClose}></TipModal>

    </div>
  )
}

export default CollegeRegister;

