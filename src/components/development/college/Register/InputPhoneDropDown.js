import React from "react";
import ReactCustomFlagSelect from "react-custom-flag-select";
import "react-custom-flag-select/lib/react-custom-flag-select.min.css";
import { Textbox } from "react-inputs-validation";
import "react-inputs-validation/lib/react-inputs-validation.min.css";
// import { FLAGS } from "./consts";
import ChinaFlag from '../../../../assets/images/svg/chinaflag.png';
import UsFlag from '../../../../assets/images/svg/usflag.png';
// import "./styles.css";
const find = (arr, obj) => {
  const res = [];
  arr.forEach((o) => {
    let match = false;
    Object.keys(obj).forEach((i) => {
      if (obj[i] == o[i]) {
        match = true;
      }
    });
    if (match) {
      res.push(o);
    }
  });
  return res;
};

const FLAG_SELECTOR_OPTION_LIST = [
  {
    id: "1",
    name: "US",
    displayText: "US(1)",
    locale: "en-US",
    flag: UsFlag
  },
  {
    id: "86",
    name: "中国",
    displayText: "中国(86)",
    locale: "zh-CN",
    flag: ChinaFlag
  }
];

const DEFAULT_AREA_CODE = FLAG_SELECTOR_OPTION_LIST[0].id;

class InputPhoneDropDown extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      areaCode: DEFAULT_AREA_CODE,
      phone: "",
      hasPhoneError: true,
      validate: false
    };
    this.submit = this.submit.bind(this);
  }

  handlePhoneChange(res) {
    this.setState({ phone: res });
  }

  toggleValidating(validate) {
    this.setState({ validate });
  }

  submit(e) {
    e.preventDefault();
    this.toggleValidating(true);
    const { hasPhoneError } = this.state;
    if (!hasPhoneError) {
      alert("You are good to go");
    }
  }

  render() {
    const { areaCode, phone, validate } = this.state;
    const currentItem = find(FLAG_SELECTOR_OPTION_LIST, { id: areaCode })[0];
    return (
          <form onSubmit={this.submit}>
            <div style={{ position: "relative" }}>
              <div
                style={{
                  position: "absolute",
                  left: "0",
                  height: "45px"
                }}
              >
                <ReactCustomFlagSelect
                  attributesWrapper={{ id: "areaCodeWrapper", tabIndex: "1" }} //Optional.[Object].Modify wrapper general attributes.
                  attributesButton={{ id: "areaCodeButton" }} //Optional.[Object].Modify button general attributes.
                  attributesInput={{ id: "areaCode", name: "areaCode" }} //Optional.[Object].Modify hidden input general attributes.
                  value={currentItem.id} //Optional.[String].Default: "".
                  disabled={false} //Optional.[Bool].Default: false.
                  showSearch={true} ////Optional.[Bool].Default: false.
                  showArrow={true} //Optional.[Bool].Default: true.
                  animate={true} //Optional.[Bool].Default: false.
                  optionList={FLAG_SELECTOR_OPTION_LIST} //Required.[Array of Object(s)].Default: [].
                  // selectOptionListItemHtml={<div>us</div>} //Optional.[Html].Default: none. The custom select options item html that will display in dropdown list. Use it if you think the default html is ugly.
                  // selectHtml={<div>us</div>} //Optional.[Html].Default: none. The custom html that will display when user choose. Use it if you think the default html is ugly.
                  classNameWrapper="" //Optional.[String].Default: "".
                  classNameContainer="" //Optional.[String].Default: "".
                  classNameOptionListContainer="" //Optional.[String].Default: "".
                  classNameOptionListItem="" //Optional.[String].Default: "".
                  classNameDropdownIconOptionListItem={""} //Optional.[String].Default: "".
                  customStyleWrapper={{}} //Optional.[Object].Default: {}.
                  customStyleContainer={{ border: "none", fontSize: "12px" }} //Optional.[Object].Default: {}.
                  customStyleSelect={{ width: "80px" }} //Optional.[Object].Default: {}.
                  customStyleOptionListItem={{}} //Optional.[Object].Default: {}.
                  customStyleOptionListContainer={{
                    maxHeight: "100px",
                    overflow: "auto",
                    width: "120px"
                  }} //Optional.[Object].Default: {}.
                  onChange={(areaCode) => {
                    this.setState({ areaCode: areaCode }, () => {
                      this.handlePhoneChange(phone);
                    });
                  }}
                  // onBlur={() => {}} //Optional.[Func].Default: none.
                  // onFocus={(e) => {console.log(e)}} //Optional.[Func].Default: none.
                  // onClick={(e) => {console.log(e)}} //Optional.[Func].Default: none.
                />
              </div>
              <Textbox
                attributesInput={{
                  id: "phone",
                  placeholder: "Place your phone here ^-^",
                  type: "text"
                }}
                customStyleWrapper={{ height: "100%" }}
                customStyleContainer={{ height: "100%" }}
                customStyleInput={{
                  paddingTop: "0",
                  paddingBottom: "0",
                  height: "45px",
                  paddingLeft: "90px",
                  paddingRight: "20px"
                }}
                value={phone}
                validate={validate}
                validationCallback={(res) =>
                  this.setState({
                    hasPhoneError: res,
                    validate: false
                  })
                }
                type="text"
                placeholder="Please enter your phone number"
                onChange={(res) => {
                  this.handlePhoneChange(res);
                }}
                onBlur={() => {}}
                validationOption={{
                  check: true,
                  required: true,
                  customFunc: (phone) => {
                    if (
                   true //   validator.isMobilePhone(`${phone}`, currentItem.locale)
                    ) {
                      return true;
                    } else {
                      return `Invalid phone format for ${currentItem.locale}`;
                    }
                  }
                }}
              />
            </div>
            <div /> 
            <input type="submit" style={{ display: "none" }} />
          </form>
    );
  }
}

export default InputPhoneDropDown;