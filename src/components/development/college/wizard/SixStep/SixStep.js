import React, { useState, useEffect } from 'react';
import { Col, Row, Button} from 'react-bootstrap';
import 'rsuite/dist/styles/rsuite-default.css';
import FormInput from '../FormInput'
import { placement } from '../WizardQuestion'
import Loader from "react-js-loader";
import Validator from "validator";
import { getLocalStorage } from '../../../../../Lib/Utils';


const SixStep = (props) => {

  const [state, setState] = useState({});
  const [errors, setErrors] = useState({});
  const [urlMsg, setUrlMsg] = useState({});
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() =>{
    setErrors({})
  },[props])


  const onchangeInput = (event, code) => {
    if (code) {
      setState({ ...state, [code]: event });
    } else {
      const { target: { name, value } } = event;
      setState({ ...state, [name]: value });
    }
  };

  const onKeyDownInput = event => {
    if (event && event.target && event.target.name) {
      const { target: { name, value } } = event;

      if (event.keyCode == 13) {
        setState({ ...state, [name]: '' });

        var arr = name.split('_');
        let Newname = arr[0];

        if (state[Newname]) {
          let ArryVale = [...state[Newname]];
          var indexB = ArryVale.indexOf(value);
          if (indexB > -1) {
            ArryVale.splice(indexB, 1);
            setState({ ...state, [name]: '', [Newname]: [...ArryVale] });
          } else {
            ArryVale.push(value);
            setState({ ...state, [name]: '', [Newname]: [...ArryVale] });
          }
        } else {
          setState({ ...state, [name]: '', [Newname]: [value] });
        }
      }
    } else {
      //setState({ ...state, DOB: event });
    }
  };

  //select check box
  const oncheckBox = (name, value) => {
    if (state[name]) {
      let ArryVale = state[name];
      var indexB = ArryVale.indexOf(value);
      if (indexB > -1) {
        ArryVale.splice(indexB, 1);
        if(ArryVale && ArryVale.length <= 0){
          delete state[name]
          setState({ ...state});
        }else{
          setState({ ...state, [name]: [...ArryVale] });
        }
      } else {
        ArryVale.push(value);
        setState({ ...state, [name]: [...ArryVale] });
      }
    } else {
      setState({ ...state, [name]: [value] });
    }
  }



  useEffect(() => {
    if (props.data && Object.keys(props.data).length !== 0) {
      setState({
        ...state,
        ...props.data
      });
    }
  }, [props.data]);


  const stepSubmit = () => {
    setIsLoading(true)
    let errors = {};
    let valid = true;
    setUrlMsg('')
    for (var m = 0; m < placement.length; m++) {

      if (placement[m].isRequired) {
        let codevalue = state[placement[m].code];
        if (!codevalue || codevalue == 'undefined' || codevalue == '') {
          valid = false;
          errors[placement[m].code] = true;
        }

        let childArray = placement[m].options;
        childArray && childArray.map((data_item) =>{
          if(state["INSTITUTEPARTNERSHIP"] && state["INSTITUTEPARTNERSHIP"] == "Yes"){
            data_item.questions && data_item.questions.map((item_1) =>{
              if(item_1.code == "INSTITUTION_PARTNERED"){
                let arrayValue = state[item_1.code];
                if (!arrayValue || arrayValue == 'undefined' || arrayValue == '') {
                  valid = false;
                  errors[item_1.code] = true;
                }
              } 
            })
          }
        })
      }else{
        let ParentArray = placement[m].options;
        ParentArray && ParentArray.map((data_item) =>{
          if(state["DISCOVERHIED"] && state["DISCOVERHIED"] == "HiEd Student Success Reps"){
            data_item.questions && data_item.questions.map((item_1) =>{
              if(item_1.code == "WHOWEREREFERRED"){
                let arrayValue = state[item_1.code];
                if (!arrayValue || arrayValue == 'undefined' || arrayValue == '') {
                  valid = false;
                  errors[item_1.code] = true;
                }
              } 
            })
          }
          if(state["DISCOVERHIED"] && state["DISCOVERHIED"] == "Others (please specify)"){
            data_item.questions && data_item.questions.map((item_1) =>{
              if(item_1.code == "OTHERDISCOVERHIED"){
                let arrayValue = state[item_1.code];
                if (!arrayValue || arrayValue == 'undefined' || arrayValue == '') {
                  valid = false;
                  errors[item_1.code] = true;
                } 
              }
            })
          }
        })
      }

      placement[m] && (placement[m].code == "SOCIAL_LINK" || placement[m].code == "OFFICIALSOCIALMEDIASELECT") && placement[m].isLogical &&
      placement[m].questions && placement[m].questions.map((item, i) =>{
        if(state && state[item.code]){

          var regex = /(http|https):(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;

          if(state[item.code] && Validator.isURL(state[item.code])){
            if (state[item.code].match(regex)) {
            }else{
              valid = false;
              errors[item.code] = true;
              setUrlMsg('Valid URL is required (ex.- https://hiedharmony.com).')
            }
          }else{
            valid = false;
              errors[item.code] = true;
              setUrlMsg('Valid URL is required (ex.- https://hiedharmony.com).')
          }
          
        }
      })

      placement[m].options && placement[m].options.map((item_1, i) =>{
        item_1.questions && item_1.questions.map((item, i) =>{
          if(state && state[item.code] && state[placement[m].code] == item.text){
  
            var regex = /(http|https):(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
  
            if(state[item.code] && Validator.isURL(state[item.code])){
              if (state[item.code].match(regex)) {
              }else{
                valid = false;
                errors[item.code] = true;
                setUrlMsg('Valid URL is required (ex.- https://hiedharmony.com).')
              }
            }else{
              valid = false;
                errors[item.code] = true;
                setUrlMsg('Valid URL is required (ex.- https://hiedharmony.com).')
            }
            
          }
        })
      })
      
    }


    if (valid) {
      props.stepSubmitSix(state, 6);
      setTimeout(function () {
        setIsLoading(false)
      // }.bind(this), 2000);
    }, 2000);
      setErrors({});
    }
    else {
      setIsLoading(false)
      setErrors({ ...errors });
    }

  }



  return (


    <div className="rightBox pt-5 pb-5 pl-4">

      <Col lg={10} md={12}>
        <div className="rightLayout">
          <div className="d-flex flex-wrap justify-content-between mb-1">
            <div className="stepTitle fw500 fs30 col4 position-relative mb-4 pb-1">Placement</div>
            {
              getLocalStorage('user') && getLocalStorage('user').isProfileCompleted &&
              <div className="col2 fw500 fs18 pointer" onClick={() => props.stepSelect('final')}>Go To Dashboard</div>
            }
          </div>
          <Row className="stepForm7">

          {
              placement && placement.map((data, index) =>
                  index < 7 &&
                  data.section == 'placement' ? (
                    <div key={index + 'i'}>
                      <Col md={12} key={index + 'i'}>
                        <div className="col4 fw600  fs18 mb-2">
                          <span className="numericType1">{index + 1}.</span>
                          {data.text}
                          {data.isRequired ? <sup className="starQ">*</sup> : ''}
                        </div>
                      </Col>
                      <Col key={index + 'j'} lg={'12'} md={'12'}>
                        <FormInput
                          inputData={data}
                          listOptions={data.options}
                          onchangeInput={onchangeInput}
                          onKeyDownInput={onKeyDownInput}
                          oncheckBox={oncheckBox}
                          setData={state}
                          error={errors[data.code] ? true : false}
                          errorNew={errors}
                          errorNested={errors}
                          errorMessage={'This field is required.'}
                          OuterIndex={index}
                          cssSteps={true}  
                        />
                      </Col>

                      {/* logical questions */}
                      {
                        data.options && data.options.map((option_1, index_2) => (
                          option_1.isLogical == true &&
                          option_1.questions && option_1.questions.map((que, index_3) => (
                            state[data.code] && state[data.code].indexOf(option_1.value) > -1 &&
                            <div key={index_3}>
                              <Col md={12} key={index_3 + 'i'}>
                                <div className="col4 fw600  fs18 mb-2">
                                  <span className="numericType1">{index + 1}.{index_3 + 1} </span>
                                  {que.text}
                                  {que.isRequired ? <sup className="starQ">*</sup> : ''}
                                </div>
                              </Col>

                              <Col key={index_3 + 'j'} lg={'12'} md={'12'}>
                                <FormInput
                                  inputData={que}
                                  listOptions={que.options ? que.options : que}
                                  onchangeInput={onchangeInput}
                                  onKeyDownInput={onKeyDownInput}
                                  oncheckBox={oncheckBox}
                                  setData={state}
                                  error={errors[que.code] ? true : false}
                                  errorNew={errors}
                                  errorNested={errors}
                                  errorMessage={'This field is required.'}
                                  OuterIndex={index}
                                />
                              </Col>
                            </div>
                          ))

                        ))
                      }
                    </div>
                  ) : ''
                
              )
            }













            <Col md={12}>
              <div className="hrBorder mb-4 mt-1"></div>
              <div className="fs20 fw600 col2 mb-3">Final Steps</div>
            </Col>
            {
              placement && placement.map((data, index) =>
                
                  index >= 7 &&
                  data.section == 'placement' ? (
                    <div key={index + 'i'}>
                      <Col md={12} key={index + 'i'}>
                        <div className="col4 fw600  fs18 mb-2">
                          <span className="numericType1">{index + 1}.</span>
                          {data.text}
                          {data.isRequired ? <sup className="starQ">*</sup> : ''}
                        </div>
                      </Col>
                      <Col key={index + 'j'} lg={'12'} md={'12'}>
                        
                        <FormInput
                          inputData={data}
                          listOptions={data.options}
                          onchangeInput={onchangeInput}
                          onKeyDownInput={onKeyDownInput}
                          oncheckBox={oncheckBox}
                          setData={state}
                          error={errors[data.code] ? true : false}
                          errorNew={errors}
                          errorNested={errors}
                          errorMessage={'This field is required.'}
                          OuterIndex={index}
                        />
                      </Col>
                      {
                        //code for logical questions other
                        <div>
                            {
                              data.options && data.options.map((option_1, index_2) => (
                                option_1.isLogical == true &&
                                option_1.questions && option_1.questions.map((que, index_3) => (
                                  state[data.code] && state[data.code].indexOf(option_1.value) > -1 &&
                                  <div key={index_3}>
                                    <Col md={12} key={index_3 + 'i'}>
                                      <div className="col4 fw600  fs18 mb-2">
                                        {/* <span className="numericType1">{index + 1}.{index_2 + 1} </span> */}
                                        {que.text ? que.text : ''}
                                        {que.isRequired ? <sup className="starQ">*</sup> : ''}
                                      </div>
                                    </Col>

                                    <Col key={index_3 + 'j'} lg={'12'} md={'12'}>
                                      <FormInput
                                        inputData={que}
                                        listOptions={que.options ? que.options : que}
                                        onchangeInput={onchangeInput}
                                        onKeyDownInput={onKeyDownInput}
                                        oncheckBox={oncheckBox}
                                        setData={state}
                                        error={errors[que.code] ? true : false}
                                        errorNew={errors}
                                        errorNested={errors}
                                        errorMessage={urlMsg ? urlMsg : 'This field is required.'}
                                        OuterIndex={index}
                                      />
                                    </Col>
                                  </div>
                                ))

                              ))
                            }



                            {
                              data.isLogical == true &&
                              data.questions && data.questions.map((question, index_4) => (
                                <div key={index_4}>
                                  <Col md={12} key={index_4 + 'i'}>
                                    <div className="col4 fw600  fs18 mb-2">
                                      <span className="numericType1">{index + 1}.{index_4 + 1}</span>
                                      {question.text}
                                      {question.isRequired ? <sup className="starQ">*</sup> : ''}
                                    </div>
                                  </Col>

                                  <Col key={index_4 + 'j'} lg={'12'} md={'12'}>
                                    <FormInput
                                      inputData={question}
                                      listOptions={question}
                                      onchangeInput={onchangeInput}
                                      onKeyDownInput={onKeyDownInput}
                                      oncheckBox={oncheckBox}
                                      setData={state}
                                      error={errors[question.code] ? true : false}
                                      errorNew={errors}
                                      errorNested={errors}
                                      errorMessage={urlMsg ? urlMsg : 'This field is required.'}
                                      OuterIndex={index_4}
                                    />
                                  </Col>
                                </div>
                            ))
                            }
                          {/* </Row> */}
                        </div>
                      }



                    </div>
                  ) : ''
                
              )
            }



          </Row>
        </div>
        <div className="footerBotton">
          <div className="hrBorder pt-3"></div>
          <div className="text-right d-flex flex-wrap justify-content-between  mt-3">
            <Button type="button" className="btnType7"
              onClick={() => props.stepSelect('fivth')}>
              Previous
            </Button>
            <Button type="button" className="btnType2"
              onClick={() => stepSubmit()}>
              {
                isLoading ?
                  <Loader type="bubble-top" bgColor={"#414180"} size={25} />
                  :
                  "Submit"
              }
            </Button>
          </div>
        </div>

      </Col>
    </div>





  )
}

export default SixStep;




