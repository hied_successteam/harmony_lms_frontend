import React, { useState, useEffect } from 'react';
import { Col, Row, Button } from 'react-bootstrap';
import 'rsuite/dist/styles/rsuite-default.css';
import FormInput from '../FormInput'
import { campusInfrastructure } from '../WizardQuestion'
import { getLocalStorage } from '../../../../../Lib/Utils';


const FiveStep = (props) => {

  const [state, setState] = useState({});
  const [errors, setErrors] = useState({});

  useEffect(() =>{
    setErrors({})
  },[props])



  const onchangeInput = (event, code) => {
    if(code){
      setState({ ...state, [code]: event });
    }else{
      const { target: { name, value } } = event;
      setState({ ...state, [name]: value });
    }
  };

  const onKeyDownInput = event => {
    if (event && event.target && event.target.name) {
      const { target: { name, value } } = event;

      if (event.keyCode == 13) {
        setState({ ...state, [name]: '' });

        var arr = name.split('_');
        let Newname = arr[0];

        if (state[Newname]) {
          let ArryVale = [...state[Newname]];
          var indexB = ArryVale.indexOf(value);
          if (indexB > -1) {
            ArryVale.splice(indexB, 1);
            setState({ ...state, [name]: '', [Newname]: [...ArryVale] });
          } else {
            ArryVale.push(value);
            setState({ ...state, [name]: '', [Newname]: [...ArryVale] });
          }
        } else {
          setState({ ...state, [name]: '', [Newname]: [value] });
        }
      }
    } else {
      //setState({ ...state, DOB: event });
    }
  };

  //select check box
  const oncheckBox = (name, value) => {
    if (state[name]) {
      let ArryVale = state[name];
      var indexB = ArryVale.indexOf(value);
      if (indexB > -1) {
        ArryVale.splice(indexB, 1);
        if(ArryVale && ArryVale.length <= 0){
          delete state[name]
          setState({ ...state});
        }else{
          setState({ ...state, [name]: [...ArryVale] });
        }
      } else {
        ArryVale.push(value);
        setState({ ...state, [name]: [...ArryVale] });
      }
    } else {
      setState({ ...state, [name]: [value] });
    }
  }


  useEffect(() => {

    if (props.data && Object.keys(props.data).length !== 0) {

      setState({
        ...state,
        ...props.data
      });
    }
  }, [props.data]);


  const stepSubmit = () => {
    let errors = {};
    let valid = true;
    for (var m = 0; m < campusInfrastructure.length; m++) {

      if (campusInfrastructure[m].isRequired) {
        let codevalue = state[campusInfrastructure[m].code];
        if (!codevalue || codevalue == 'undefined' || codevalue == '') {
          valid = false;
          errors[campusInfrastructure[m].code] = true;
        }
        let queArray = campusInfrastructure[m].options
        queArray && queArray.map((item, i) =>{
          if(campusInfrastructure[m].code == "CAMPUSSECURITY" && item.questions && item.questions.length && state && state["CAMPUSSECURITY"] && state["CAMPUSSECURITY"].indexOf("Others") > -1){
            var NestedArr = item.questions
            for (var i = 0; i < NestedArr.length; i++) {
              if (NestedArr[i].isRequired) {
                let codevalueNew = state[NestedArr[i].code];
                if (!codevalueNew || codevalueNew == 'undefined' || codevalueNew == '') {
                  valid = false;
                  errors[NestedArr[i].code] = true;
                }
              }
            }
          }
        })
      }else{
        let queArray = campusInfrastructure[m].options
        queArray && queArray.map((item, i) =>{
          if(campusInfrastructure[m].code == "COMPUTERFACILITIES" && item.questions && item.questions.length && state && state["COMPUTERFACILITIES"] && state["COMPUTERFACILITIES"].indexOf("Others") > -1){
            var NestedArr = item.questions
            for (var i = 0; i < NestedArr.length; i++) {
              if (NestedArr[i].isRequired) {
                let codevalueNew = state[NestedArr[i].code];
                if (!codevalueNew || codevalueNew == 'undefined' || codevalueNew == '') {
                  valid = false;
                  errors[NestedArr[i].code] = true;
                }
              }
            }
          }
        })        
      }
    }

    if (valid) {
      props.stepSubmitFive(state, 5);
      setErrors({});
    }
    else {
      setErrors({ ...errors });
    }

  }



  return (


    <div className="rightBox pt-5 pb-5 pl-4">

      <Col lg={10} md={12}>
        <div className="rightLayout">
          <div className="d-flex flex-wrap justify-content-between mb-1">
            <div className="stepTitle fw500 fs30 col4 position-relative mb-4 pb-1">Campus Infrastructure</div>
            {
              getLocalStorage('user') && getLocalStorage('user').isProfileCompleted &&
              <div className="col2 fw500 fs18 pointer" onClick={() => props.stepSelect('sixth')}>Skip</div>
            }
          </div>
          <Row className="stepForm7">
            {
              campusInfrastructure && campusInfrastructure.map((data, index) =>
                data.section == 'campusInfrastructure' ? (
                  <div key={index + 'i'}>
                    <Col md={12} key={index + 'i'}>
                      <div className="col4 fw600  fs18 mb-2">
                        <span className="numericType1">{index + 1}.</span>
                        {data.text}
                        {data.isRequired ? <sup className="starQ">*</sup> : ''}
                      </div>
                    </Col>
                    <Col key={index + 'j'} lg={'12'} md={'12'}>
                      <FormInput
                        inputData={data}
                        listOptions={data.options}
                        onchangeInput={onchangeInput}
                        onKeyDownInput={onKeyDownInput}
                        oncheckBox={oncheckBox}
                        setData={state}
                        error={errors[data.code] ? true : false}
                        errorNew={errors}
                        errorNested={errors}
                        errorMessage={'This field is required.'}
                        OuterIndex={index}
                      />
                    </Col>

                    {/* for Others option text */}
                    {
                      
                      data.options  &&
                      data.options.map((option, index_1) =>(
                        <div key={index_1}>
                          {
                            option.questions && option.questions.map((question, index_2) =>(
                              state && state[data.code] && state[data.code].indexOf(question.text) > -1 &&
                            <div key={index_2}>
                                <Col md={12} key={index_2 + 'i'}>
                                  <div className="col4 fw600  fs18 mb-2">
                                    <span className="numericType1">{index + 1}.{index_2 + 1}</span>
                                    {question.text}
                                    {question.isRequired ? <sup className="starQ">*</sup> : ''}
                                  </div>
                                </Col>
                                <Col key={index_2 + 'j'} lg={'12'} md={'12'}>
                                  <FormInput
                                    inputData={question}
                                    listOptions={question}
                                    onchangeInput={onchangeInput}
                                    onKeyDownInput={onKeyDownInput}
                                    oncheckBox={oncheckBox}
                                    setData={state}
                                    error={errors[question.code] ? true : false}
                                    errorNew={errors}
                                    errorNested={errors}
                                    errorMessage={'This field is required.'}
                                    OuterIndex={index_2}
                                  />
                                </Col>
                            </div>
                            ))
                          }
                        </div>
                      ))
                    }

                  </div>
                ) : ''
              )
            }



          </Row>
        </div>
        <div className="footerBotton">
          <div className="hrBorder pt-3"></div>
          <div className="text-right d-flex flex-wrap justify-content-between  mt-3">
            <Button type="button" className="btnType7"
              onClick={() => props.stepSelect('fourth')}>
              Previous
            </Button>
            <Button type="button" className="btnType2"
              onClick={() => stepSubmit()}>
              Save & Next
            </Button>
          </div>
        </div>

      </Col>
    </div>





  )
}

export default FiveStep;




