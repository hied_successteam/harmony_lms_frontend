export const instituteRegistration = [
    {
        "section": "instituteReg",
        "text": "Institution Name",
        "code": "INSTITUTENAME",
        "options": [],
        "maxLength": 80,
        "placeholder": "Institution Name",
        "type": "text",
        "stream": "engineering",
        "isRequired": true
    },
    {
        "section": "instituteReg",
        "text": "Campus location (State)",
        "code": "STATE",
        "maxLength": 35,
        "options": [],
        "placeholder": "State",
        "type": "dropDown",
        "stream": "engineering",
        "isRequired": true,
    },
    {
        "section": "instituteReg",
        "text": "College Officer Name",
        "code": "COLLOFFICERNAME",
        "maxLength": 35,
        "options": [],
        "placeholder": "College Officer name",
        "type": "text",
        "stream": "engineering",
        "isRequired": true,
    },
    {
        "section": "instituteReg",
        "text": "Department/Unit",
        "code": "DEPARTMENTUNIT",
        "maxLength": 35,
        "options": [],
        "placeholder": "Department/Unit",
        "type": "text",
        "stream": "engineering",
        "isRequired": true,
    },
    {
        "section": "instituteReg",
        "text": "Job Title",
        "code": "TITLE",
        "options": [],
        "placeholder": "Title",
        "maxLength": 35,
        "type": "text",
        "stream": "engineering",
        "isRequired": true,
    },
    {
        "section": "instituteReg",
        "text": "Email",
        "code": "EMAIL",
        "options": [],
        "placeholder": "Email ID",
        "type": "email",
        "stream": "engineering",
        "isRequired": true,
        "disabled": true,
    },
    {
        "section": "instituteReg",
        "text": "Contact Number",
        "code": "CONTCTNUM",
        "options": [],
        "placeholder": "Contact Number",
        "type": "number",
        "stream": "engineering",
        "isRequired": true,
        "disabled": true,

    }
]

export const aboutTheInstitution = [
    {
        "section": "aboutTheInstitution",
        "isRequired": true,
        "type": "checkbox",
        "stream": "engineering",
        "text": "What makes your institution stand out? (Select all that apply)",
        "code": "CURRPROGCOLL",
        "placeholder": "",
        "options": [
            {
                "text": "Academic & career support services",
                "value": "Academic & career support services"
            },
            {
                "text": "Affordable fees",
                "value": "Affordable fees"
            },
            {
                "text": "Clubs & activities",
                "value": "Clubs & activities"
            },
            {
                "text": "Expert faculty",
                "value": "Expert faculty"
            },
            {
                "text": "Flexible curriculum",
                "value": "Flexible curriculum"
            },
            {
                "text": "Health & wellness facilities",
                "value": "Health & wellness facilities"
            },
            {
                "text": "Higher acceptance",
                "value": "Higher acceptance"
            },
            {
                "text": "Higher grad rates",
                "value": "Higher grad rates"
            },
            {
                "text": "International student friendly",
                "value": "International student friendly"
            },
            {
                "text": "Internship opp.",
                "value": "Internship opp."
            },
            {
                "text": "Research opp.",
                "value": "Research opp."
            },
            {
                "text": "Great job prospects",
                "value": "Great job prospects"
            },
            {
                "text": "Strong corporate collaboration",
                "value": "Strong corporate collaboration"
            },
            {
                "text": "Location & setting",
                "value": "Location & setting"
            },
            {
                "text": "Low student to faculty ratio",
                "value": "Low student to faculty ratio"
            },
            {
                "text": "Social scene",
                "value": "Social scene"
            },
            {
                "text": "State-of-art labs & classrooms",
                "value": "State-of-art labs & classrooms"
            },
            {
                "text": "Strong alumni network",
                "value": "Strong alumni network"
            },
            {
                "text": "Student diversity",
                "value": "Student diversity"
            },
            {
                "text": "TOEFL optional for certain countries",
                "value": "TOEFL optional for certain countries"
            },
            {
                "text": "Optional admission entrances",
                "value": "Optional admission entrances"
            },
            {
                "text": "Optional essay req.",
                "value": "Optional essay req."
            },
            {
                "text": "CPT on day 1",
                "value": "CPT on day 1"
            },
            {
                "text": "Work opportunity at campus",
                "value": "Work opportunity at campus"
            },
            {
                "text": "Musical and Broadway shows",
                "value": "Musical and Broadway shows"
            },
            {
                "text": "Athletics or Intramural Sports/Outdoors",
                "value": "Athletics or Intramural Sports/Outdoors"
            },
            {
                "text": "Prayer room on campus",
                "value": "Prayer room on campus"
            }

        ]
    },
    {
        "section": "aboutTheInstitution",
        "text": "What are the challenges faced by the institution while selecting a student?",
        "code": "CHALLENGESFACESINSTI",
        "options": [],
        "placeholder": "For example, skills gap, international student recruitment",
        "type": "textArea",
        "stream": "engineering"
    },
    {
        "section": "aboutTheInstitution",
        "text": "Are there any specific goals for increasing student enrollment in the next 2 years? If yes, share as many strategic goals as possible including goals so we can align qualifying candidates.",
        "code": "INSTITUTEPROSPECTOFSTUDENTS",
        "options": [],
        "placeholder": "For example, 10% increase in women enrollment for STEM programs, 100% scholarship availability for them etc.",
        "type": "textArea",
        "stream": "engineering"
    },
    {
        "section": "aboutTheInstitution",
        "isRequired": true,
        "isLogical": true,
        "type": "checkbox",
        "stream": "engineering",
        "text": "Which of the following financial aids and scholarships are offered by the institution? (Select all that apply)",
        "code": "FINANCIALAIDS",
        "placeholder": "",
        "options": [
            {
                "text": "Federal work study",
                "value": "Federal work study"
            },
            {
                "text": "Need-based scholarships",
                "value": "Need-based scholarships",
                "isLogical": true,
                "questions": [
                    {
                        "section": "aboutTheInstitution",
                        "text": " How much is the set aside need-based scholarship% amount kept for international students?",
                        "code": "NEEDBASEDSCOLARSHIP",
                        "placeholder": "",
                        "type": "slider",
                        "stream": "engineering",
                        "isRequired": true,
                        "hasQuestions": true,
                        "isLogical": true,
                        "questions": [
                            {
                                "text": "",
                                "code": "NEEDBASEDSCOLARSHIP",
                                "minRange": 0,
                                "maxRange": 100,
                                "interval": 25
                            },
                        ],
                    },
                ]
            },
            {
                "text": "Merit-based scholarships",
                "value": "Merit-based scholarships",
                "isLogical": true,
                "questions": [
                    {
                        "section": "aboutTheInstitution",
                        "text": " How much is the set aside merit-based scholarship% amount kept for international students?",
                        "code": "MERITBASEDSCOLARSHIP",
                        "placeholder": "",
                        "type": "slider",
                        "stream": "engineering",
                        "isRequired": true,
                        "hasQuestions": true,
                        "isLogical": true,
                        "questions": [
                            {
                                "text": "",
                                "code": "MERITBASEDSCOLARSHIP",
                                "minRange": 0,
                                "maxRange": 100,
                                "interval": 25
                            },
                        ],
                    },
                ]
            },
            {
                "text": "Federal student aid",
                "value": "Federal student aid"
            },
            {
                "text": "Bursaries",
                "value": "Bursaries"
            },
            {
                "text": "Student Grants (Pell / Federal / State)",
                "value": "Student Grants (Pell / Federal / State)"
            },
            {
                "text": "Subsidized student loans",
                "value": "Subsidized student loans"
            },
            {
                "text": "Aid for military families",
                "value": "Aid for military families"
            },
            {
                "text": "Unsubsidized student loans",
                "value": "Unsubsidized student loans"
            },
            {
                "text": "Others",
                "value": "Others",
                "isLogical": true,
                "isRequired": true,
                "questions": [
                    {
                        "section": "academicsAndExtracurriculars",
                        "text": "Others",
                        "isRequired": true,
                        "code": "FINANCIALAIDS_OTHEROPTION",
                        "placeholder": "",
                        "type": "text",
                        "stream": "engineering",
                    },
                ]
            }

        ]
    },
    {
        "section": "aboutTheInstitution",
        "text": "Please answer the following questions with YES or NO",
        "code": "",
        "isLogical": true,
        "isRequired": true,
        "questions": [
            {
                text: "Does the institution have a co-op program?",
                code: "COOPPROGRAM",
                type: "categorical",
                isRequired: true,
                isLogical: true,
                options: [
                    {
                        text: "Yes",
                        value: "Yes",
                        itemValue: 1
                    },
                    {
                        text: "No",
                        value: "No",
                        itemValue: 2
                    }
                ]
            },
            {
                text: "Does institution provide extra credits to candidates with community or volunteering experience?",
                code: "CANDIDATEVOLUNTRCREDIT  ",
                type: "categorical",
                isRequired: true,
                isLogical: true,
                options: [
                    {
                        text: "Yes",
                        value: "Yes",
                        itemValue: 1
                    },
                    {
                        text: "No",
                        value: "No",
                        itemValue: 2
                    }
                ]
            },
            {
                text: <p>Does the institution offer institutional scholarships for out-of-state/international students from 1<sup>st</sup> Semester?</p>,
                code: "PROSPECTIVESTUDENTSCHOLAR",
                type: "categorical",
                isRequired: true,
                isLogical: true,
                options: [
                    {
                        text: "Yes",
                        value: "Yes",
                        itemValue: 1
                    },
                    {
                        text: "No",
                        value: "No",
                        itemValue: 2
                    }
                ]
            }, 
            {
                text: "Do students get the opportunity to work on a capstone project?",
                code: "CAPSTONEPROJECTOPPORTUNITY",
                type: "categorical",
                isRequired: true,
                isLogical: true,
                options: [
                    {
                        text: "Yes",
                        value: "Yes",
                        itemValue: 1
                    },
                    {
                        text: "No",
                        value: "No",
                        itemValue: 2
                    }
                ]
            },  
            {
                text: "Does the institution provide summer camps / skill building programs for international students?",
                code: "INSTPROVIDESUMMERCAMP",
                type: "categorical",
                isRequired: true,
                isLogical: true,
                options: [
                    {
                        text: "Yes",
                        value: "Yes",
                        itemValue: 1
                    },
                    {
                        text: "No",
                        value: "No",
                        itemValue: 2
                    }
                ]
            },
        ],
        "placeholder": "",
        "type": "matrix",
        "stream": "engineering"
    },
    {
        "section": "aboutTheInstitution",
        "text": "How would you rate the college on the following factors?",
        "code": "",
        "placeholder": "",
        "type": "matrix",
        "stream": "engineering",
        "isLogical": true,
        "isRequired": true,
        "questions": [
            {
                "text": "Student demographics",
                "code": "STUDENTDEMOGRAPHI",
                "type": "categorical",
                "isRequired": true,
                "isLogical": true,
                "options": [
                    {
                        "text": "OK",
                        "value": "OK",
                        "itemValue": 1
                    },
                    {
                        "text": "Fairly good",
                        "value": "Fairly good",
                        "itemValue": 2
                    },
                    {
                        "text": "Meets most expectations",
                        "value": "Meets most expectations",
                        "itemValue": 3
                    },
                    {
                        "text": "Great overall",
                        "value": "Great overall",
                        "itemValue": 4
                    },
                    {
                        "text": "Slightly exceeds expectations",
                        "value": "Slightly exceeds expectations",
                        "itemValue": 5
                    },
                    {
                        "text": "Exceptional",
                        "value": "Exceptional",
                        "itemValue": 6
                    },
                    {
                        "text": "World class",
                        "value": "World class",
                        "itemValue": 7
                    }
                ]
            },
            {
                "text": "Quality of education",
                "code": "QUALITYEDUCATION",
                "type": "categorical",
                "isRequired": true,
                "isLogical": true,
                "options": [
                    {
                        "text": "OK",
                        "value": "OK",
                        "itemValue": 1
                    },
                    {
                        "text": "Fairly good",
                        "value": "Fairly good",
                        "itemValue": 2
                    },
                    {
                        "text": "Meets most expectations",
                        "value": "Meets most expectations",
                        "itemValue": 3
                    },
                    {
                        "text": "Great overall",
                        "value": "Great overall",
                        "itemValue": 4
                    },
                    {
                        "text": "Slightly exceeds expectations",
                        "value": "Slightly exceeds expectations",
                        "itemValue": 5
                    },
                    {
                        "text": "Exceptional",
                        "value": "Exceptional",
                        "itemValue": 6
                    },
                    {
                        "text": "World class",
                        "value": "World class",
                        "itemValue": 7
                    }
                ]
            },
            {
                "text": "College location",
                "code": "COLLEGELOCATION",
                "type": "categorical",
                "isRequired": true,
                "isLogical": true,
                "options": [
                    {
                        "text": "OK",
                        "value": "OK",
                        "itemValue": 1
                    },
                    {
                        "text": "Fairly good",
                        "value": "Fairly good",
                        "itemValue": 2
                    },
                    {
                        "text": "Meets most expectations",
                        "value": "Meets most expectations",
                        "itemValue": 3
                    },
                    {
                        "text": "Great overall",
                        "value": "Great overall",
                        "itemValue": 4
                    },
                    {
                        "text": "Slightly exceeds expectations",
                        "value": "Slightly exceeds expectations",
                        "itemValue": 5
                    },
                    {
                        "text": "Exceptional",
                        "value": "Exceptional",
                        "itemValue": 6
                    },
                    {
                        "text": "World class",
                        "value": "World class",
                        "itemValue": 7
                    }
                ]
            },
            {
                "text": "Climate and conditions",
                "code": "CLIMATECONDITION",
                "type": "categorical",
                "isRequired": true,
                "isLogical": true,
                "options": [
                    {
                        "text": "OK",
                        "value": "OK",
                        "itemValue": 1
                    },
                    {
                        "text": "Fairly good",
                        "value": "Fairly good",
                        "itemValue": 2
                    },
                    {
                        "text": "Meets most expectations",
                        "value": "Meets most expectations",
                        "itemValue": 3
                    },
                    {
                        "text": "Great overall",
                        "value": "Great overall",
                        "itemValue": 4
                    },
                    {
                        "text": "Slightly exceeds expectations",
                        "value": "Slightly exceeds expectations",
                        "itemValue": 5
                    },
                    {
                        "text": "Exceptional",
                        "value": "Exceptional",
                        "itemValue": 6
                    },
                    {
                        "text": "World class",
                        "value": "World class",
                        "itemValue": 7
                    }
                ]
            },
            {
                "text": "Job-readiness and employment",
                "code": "JOBREADINESS",
                "type": "categorical",
                "isRequired": true,
                "isLogical": true,
                "options": [
                    {
                        "text": "OK",
                        "value": "OK",
                        "itemValue": 1
                    },
                    {
                        "text": "Fairly good",
                        "value": "Fairly good",
                        "itemValue": 2
                    },
                    {
                        "text": "Meets most expectations",
                        "value": "Meets most expectations",
                        "itemValue": 3
                    },
                    {
                        "text": "Great overall",
                        "value": "Great overall",
                        "itemValue": 4
                    },
                    {
                        "text": "Slightly exceeds expectations",
                        "value": "Slightly exceeds expectations",
                        "itemValue": 5
                    },
                    {
                        "text": "Exceptional",
                        "value": "Exceptional",
                        "itemValue": 6
                    },
                    {
                        "text": "World class",
                        "value": "World class",
                        "itemValue": 7
                    }
                ]
            },
            {
                "text": "Financial affordability",
                "code": "FINANCIALAFFORAD",
                "type": "categorical",
                "isRequired": true,
                "isLogical": true,
                "options": [
                    {
                        "text": "OK",
                        "value": "OK",
                        "itemValue": 1
                    },
                    {
                        "text": "Fairly good",
                        "value": "Fairly good",
                        "itemValue": 2
                    },
                    {
                        "text": "Meets most expectations",
                        "value": "Meets most expectations",
                        "itemValue": 3
                    },
                    {
                        "text": "Great overall",
                        "value": "Great overall",
                        "itemValue": 4
                    },
                    {
                        "text": "Slightly exceeds expectations",
                        "value": "Slightly exceeds expectations",
                        "itemValue": 5
                    },
                    {
                        "text": "Exceptional",
                        "value": "Exceptional",
                        "itemValue": 6
                    },
                    {
                        "text": "World class",
                        "value": "World class",
                        "itemValue": 7
                    }
                ]
            },
        ],
    },
    {
        "section": "aboutTheInstitution",
        "text": "Does the Institution have different course/major specific fees?",
        "code": "DEFFERENTCOURSESPECIFIC",
        "placeholder": "",
        "type": "radio",
        "stream": "engineering",
        "isLogical": true,
        "isRequired": true,
        "options": [
            {
                "text": "Yes",
                "value": "Yes",
                "isLogical": true,
                "isRequired": true,
                "questions":[
                    {
                        "section": "aboutTheInstitution",
                        "text": "What is the course specific fee for MS or Graduate programs offered?",
                        "code": "GRADUATEPROGRAMOFFERED",
                        "placeholder": "",
                        "type": "number",
                        "stream": "engineering",
                        "isRequired": true,
                    },
                ]
            },
            {
                "text": "No",
                "value": "No"
            }
        ],
    },
    {
        "section": "aboutTheInstitution",
        "isRequired": true,
        "type": "dropDown",
        "stream": "engineering",
        "text": "Does the institution follow a specialized mission? (Only if applicable)",
        "code": "SPECIALMISSION",
        "placeholder": "",
        "options": [
            {
                "text": "Catholic",
                "value": "Catholic"
            },
            {
                "text": "Protestant",
                "value": "Protestant"
            },
            {
                "text": "Historically Black College or University",
                "value": "Historically Black College or University"
            },
            {
                "text": "Jewish",
                "value": "Jewish"
            },
            {
                "text": "Muslim",
                "value": "Muslim"
            },
            {
                "text": "Other Religious Affiliation",
                "value": "Other Religious Affiliation"
            },
            {
                "text": "Single-Sex: Men",
                "value": "Single-Sex: Men"
            },
            {
                "text": "Single-Sex: Women",
                "value": "Single-Sex: Women"
            },
            {
                "text": "Tribal College",
                "value": "Tribal College"
            },
            {
                "text": "None",
                "value": "None"
            },
        ]
    },
    {
        "section": "aboutTheInstitution",
        "text": "Does the institution offer integrated programs for Business Analytics/ Data Science?",
        "code": "OFFERINTEGRATEDPROGRAM",
        "placeholder": "",
        "type": "radio",
        "stream": "engineering",
        "isLogical": true,
        "isRequired": true,
        "options": [
            {
                "text": "Yes",
                "value": "Yes",
                "isLogical": true,
                "isRequired": true,
                "questions":[
                    {
                        "section": "aboutTheInstitution",
                        "text": "Please share website link offering details on programs offered and the cost of attending these programs?",
                        "code": "OFFERWEBLINK",
                        "placeholder": "",
                        "type": "text",
                        "stream": "engineering",
                        "isRequired": true,
                    },
                ]
            },
            {
                "text": "No",
                "value": "No"
            }
        ],
    },
    {
        "section": "aboutTheInstitution",
        "text": "Does the institution have an active alumni network?",
        "code": "ALUMNINETWORK",
        "placeholder": "",
        "type": "radio",
        "stream": "engineering",
        "isLogical": true,
        "isRequired": true,
        "options": [
            {
                "text": "Yes",
                "value": "Yes",
                "isLogical": true,
                "isRequired": true,
                "questions":[
                    {
                        "section": "aboutTheInstitution",
                        "text": "Please share the website/social media link to access the alumni network?",
                        "code": "ALUMNIWEBLINK",
                        "placeholder": "",
                        "type": "text",
                        "stream": "engineering",
                        "isRequired": true
                    },
                ]
            },
            {
                "text": "No",
                "value": "No"
            }
        ],
    },
    {
        "section": "aboutTheInstitution",
        "text": "Does the institution offer Athletic/Sports based scholarships for prospective students?",
        "code": "SCHOLARSHIPPROSPECT",
        "placeholder": "",
        "type": "radio",
        "stream": "engineering",
        "isRequired": true,
        "isLogical": true,
        "options": [
            {
                "text": "Yes",
                "value": "Yes",
                "isLogical": true,
                "isRequired": true,
                "questions":[
                    {
                        "section": "aboutTheInstitution",
                        "text": " How much is the set aside scholarship% amount kept for international students?",
                        "code": "SCOLARSHIPAPPLICATION",
                        "placeholder": "",
                        "type": "slider",
                        "stream": "engineering",
                        "isRequired": true,
                        "hasQuestions": true,
                        "isLogical": true,
                        "questions": [
                            {
                                "text": "",
                                "code": "SCOLARSHIPAPPLICATION",
                                "minRange": 0,
                                "maxRange": 100,
                                "interval": 25
                            },
                        ],
                    },
                    {
                        "section": "aboutTheInstitution",
                        "text": " Are scholarships available from semester 1?",
                        "code": "SCOLARSHIPAVAILABLE",
                        "placeholder": "",
                        "type": "radio",
                        "stream": "engineering",
                        "isRequired": true,
                        "options": [
                            {
                                "text": "Yes",
                                "value": "Yes"
                            },
                            {
                                "text": "No",
                                "value": "No"
                            }
                        ],
                    },
                ]
            },
            {
                "text": "No",
                "value": "No"
            }
        ],
    },   
]

export const admissions = [
    {
        "section": "admissions",
        "isRequired": true,
        "isLogical": true,
        "text": "Which terms can international and out-of-state students apply for? (Select all that apply for the next academic year)",
        "code": "TERMSSTUDENTAPPLYFOR",
        "placeholder": "",
        "type": "checkbox",
        "stream": "engineering",
        "options": [
            {
                "text": "Summer",
                "value": "Summer",
                "isLogical": true,
                "questions": [
                    {
                        "isRequired": true,
                        "isLogical": true,
                        "text": "Summer application deadline",
                        "options": [

                            {
                                "text": "Start Date",
                                "code": "SUMMER_STARTDATE",
                                "type": "date",
                                "placeholder": "Start Date (DD/MM)",
                                "isRequired": true,
                            },
                            {
                                "text": "End Date",
                                "code": "SUMMER_ENDDATE",
                                "type": "date",
                                "placeholder": "End Date (DD/MM)",
                                "isRequired": true,
                            }
                        ]
                    }
                ]
            },
            {
                "text": "Spring",
                "value": "Spring",
                "isLogical": true,
                "questions": [
                    {
                        "isRequired": true,
                        "isLogical": true,
                        "text": "Spring application deadline",
                        "options": [

                            {
                                "text": "Start Date",
                                "code": "SPRING_STARTDATE",
                                "type": "date",
                                "placeholder": "Start Date (DD/MM)",
                                "isRequired": true,
                            },
                            {
                                "text": "End Date",
                                "code": "SPRING_ENDDATE",
                                "type": "date",
                                "placeholder": "End Date (DD/MM)",
                                "isRequired": true,
                            }
                        ]
                    }
                ]
            },
            {
                "text": "Fall",
                "value": "Fall",
                "isLogical": true,
                "questions": [
                    {
                        "isRequired": true,
                        "isLogical": true,
                        "text": "Fall application deadline",
                        "options": [
                            {
                                "text": "Start Date",
                                "code": "FALL_STARTDATE",
                                "type": "date",
                                "placeholder": "Start Date (DD/MM)",
                                "isRequired": true,
                            },
                            {
                                "text": "End Date",
                                "code": "FALL_ENDDATE",
                                "type": "date",
                                "placeholder": "End Date (DD/MM)",
                                "isRequired": true,
                            }
                        ]
                    }
                ]
            },

        ]
    },
    {
        "section": "admissions",
        "isRequired": true,
        "isLogical": true,
        "text": "Which of the following admission options are available? (Select all that apply)",
        "code": "ADMISSIONFALLTERM",
        "placeholder": "",
        "type": "checkbox",
        "stream": "engineering",
        "options": [
            {
                "text": "Early action",
                "value": "Early action",
                "isLogical": true,
                "questions": [
                    {
                        "isRequired": true,
                        "isLogical": true,
                        "text": "Early Action - application deadline",
                        "options": [
                            {
                                "text": "Start Date",
                                "code": "EARLYACTION_STARTDATE",
                                "type": "date",
                                "placeholder": "Start Date (DD/MM)",
                                "isRequired": true,
                            },
                            {
                                "text": "End Date",
                                "code": "EARLYACTION_ENDDATE",
                                "type": "date",
                                "placeholder": "End Date (DD/MM)",
                                "isRequired": true,
                            }
                        ]
                    }
                ]

            },
            {
                "text": "Early decision",
                "value": "Early decision",
                "isLogical": true,
                "questions": [
                    {
                        "isRequired": true,
                        "isLogical": true,
                        "text": "Early Decision - application deadline",
                        "options": [
                            {
                                "text": "Start Date",
                                "code": "EARLYDECISION_STARTDATE",
                                "type": "date",
                                "placeholder": "Start Date (DD/MM)",
                                "isRequired": true,
                            },
                            {
                                "text": "End Date",
                                "code": "EARLYDECISION_ENDDATE",
                                "type": "date",
                                "placeholder": "End Date (DD/MM)",
                                "isRequired": true,
                            }
                        ]
                    }
                ]
            },
            {
                "text": "Rolling admissions",
                "value": "Rolling admissions",
            },
            {
                "text": "Regular Admissions Only",
                "value": "Regular Admissions Only"
            },
        ]
    },
    {
        "section": "admissions",
        "isRequired": true,
        "isLogical": true,
        "text": "What degree/certifications does the institution offer? (Select all that apply)",
        "code": "DEGREECERTIFICATE_INST",
        "options": [
            {
                "text": "Professional Certificates",
                "value": "Professional Certificates",
            },
            {
                "text": "Diploma",
                "value": "Diploma"
            },
            {
                "text": "Associates",
                "value": "Associates",
            },
            {
                "text": "Bachelors",
                "value": "Bachelors",
            },
            {
                "text": "Masters (Science)",
                "value": "Masters (Science)",
            },
            {
                "text": "Masters (General)",
                "value": "Masters (General)",
            },
            {
                "text": "Masters (Management)",
                "value": "Masters (Management)",
            },
            {
                "text": "Post Graduate Certificates",
                "value": "Post Graduate Certificates",
            },
            {
                "text": "Doctorate / PhD / EdD",
                "value": "Doctorate / PhD / EdD",
            },
            {
                "text": "Post-Doctoral",
                "value": "Post-Doctoral",
            },
            {
                "text": "Vocational Training",
                "value": "Vocational Training",
            },
            {
                "text": "Summer Internships",
                "value": "Summer Internships",
            },
            {
                "text": "Bridge for Bachelors",
                "value": "Bridge for Bachelors",
            },
            {
                "text": "Bridge for Masters",
                "value": "Bridge for Masters",
            },           
            
        ],
        "placeholder": "",
        "type": "checkbox",
        "stream": "engineering"
    },
    {
        "section": "admissions",
        "isRequired": true,
        "isLogical": true,
        "text": "For which of these degree/certifications are you targeting student enrollment using the Harmony platform? (Please select as per your current admission requirement)",
        "code": "DEGREECERTIFICATE",
        "options": [
            {
                "text": "Professional Certificates",
                "value": "Professional Certificates"
            },
            {
                "text": "Diploma",
                "value": "Diploma"
            },
            {
                "text": "Associates",
                "value": "Associates",
                "heading": "Which entrance exam scores are considered by the institution to apply for Associates (Select all that apply)",
                "code": "DEGREECERTIFICATE_ASSOCIATE",
                "type": "checkbox",
                "isLogical": true,
                "questions": [
                    {
                        "text": "TOEFL",
                        "value": "TOEFL",
                        "heading": "What is the institution’s minimum TOEFL score requirement?",
                        "isRequired": true,
                        "isLogical": true,
                        "isCalculate": true,
                        "questions": [
                            {
                                "text": "Reading (0-30)",
                                "code": "ASSO_TOEFL_READING",
                                "placeholder": "Reading",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number"
                            },
                            {
                                "text": "Listening (0-30)",
                                "code": "ASSO_TOEFL_LISTENING",
                                "placeholder": "Listening",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number"
                            },
                            {
                                "text": "Speaking (0-30)",
                                "code": "ASSO_TOEFL_SPEAKING",
                                "placeholder": "Speaking",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number"
                            },
                            {
                                "text": "Writing (0-30)",
                                "code": "ASSO_TOEFL_WRITING",
                                "placeholder": "Writing",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number"
                            },
                            {
                                "text": "Total (0-120)",
                                "code": "ASSO_TOEFL_TOTAL",
                                "placeholder": "Total",
                                "isRequired": true,
                                "minValue": 0,
                                "maxValue": 120,
                                "type": "number"
                            }
                        ]
                    },
                    {
                        "text": "SAT",
                        "value": "SAT",
                        "type": "checkbox",
                        "heading": "What is the institution’s minimum SAT score requirement?",
                        "isLogical": true,
                        "isRequired": true,
                        "isCalculate": true,
                        "questions": [
                            {
                                "text": "Evidence based Reading & Writing (200-800)",
                                "code": "ASSO_SAT_EBRW",
                                "placeholder": "EBRW",
                                "minValue": 200,
                                "maxValue": 800,
                                "type": "number"
                            },
                            {
                                "text": "Math Score (200-800)",
                                "code": "ASSO_SAT_MATH",
                                "placeholder": "Math",
                                "minValue": 200,
                                "maxValue": 800,
                                "type": "number"
                            },
                            {
                                "text": "Total Score (400-1600)",
                                "code": "ASSO_SAT_TOTAL",
                                "placeholder": "Total",
                                "isRequired": true,
                                "minValue": 400,
                                "maxValue": 1600,
                                "type": "number"
                            }
                        ]
                    },
                    {
                        "text": "ACT",
                        "value": "ACT",
                        "type": "checkbox",
                        "heading": "What is the institution’s minimum ACT score requirement?",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                            {
                                "text": "English (1-36)",
                                "code": "ASSO_ACT_ENGLISH",
                                "placeholder": "English",
                                "minValue": 1,
                                "maxValue": 36,
                                "type": "number"
                            },
                            {
                                "text": "Reading (1-36)",
                                "code": "ASSO_ACT_READING",
                                "placeholder": "Reading",
                                "minValue": 1,
                                "maxValue": 36,
                                "type": "number"
                            },
                            {
                                "text": "Math (1-36)",
                                "code": "ASSO_ACT_MATH",
                                "placeholder": "Math",
                                "minValue": 1,
                                "maxValue": 36,
                                "type": "number"
                            },
                            {
                                "text": "Science  (1-36)",
                                "code": "ASSO_ACT_SCIENCE",
                                "placeholder": "Science",
                                "minValue": 1,
                                "maxValue": 36,
                                "type": "number"
                            },
                            {
                                "text": "Composite Score (1-36)",
                                "code": "ASSO_ACT_COMPOSITESCORE",
                                "placeholder": "Score",
                                "isRequired": true,
                                "minValue": 1,
                                "maxValue": 36,
                                "type": "number"
                            }
                        ]
                    },
                    {
                        "text": "IELTS",
                        "value": "IELTS",
                        "type": "checkbox",
                        "heading": "What is the institution’s minimum IELTS score requirement?",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                            {
                                "text": "Listening (1-9)",
                                "code": "ASSO_IELTS_LISTENING",
                                "placeholder": "Listening",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float"
                            },
                            {
                                "text": "Reading (1-9)",
                                "code": "ASSO_IELTS_READING",
                                "placeholder": "Reading",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float"
                            },
                            {
                                "text": "Writing (1-9)",
                                "code": "ASSO_IELTS_WRITING",
                                "placeholder": "Writing",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float"
                            },
                            {
                                "text": "Speaking (1-9)",
                                "code": "ASSO_IELTS_SPEAKING",
                                "placeholder": "Speaking",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float"
                            },
                            {
                                "text": "Band Score (1-9)",
                                "code": "ASSO_IELTS_BANDSCORE",
                                "placeholder": "Score",
                                "isRequired": true,
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float"
                            }
                        ]
                    },
                    {
                        "text": "PSAT",
                        "value": "PSAT",
                        "type": "checkbox",
                        "heading": "What is the institution’s minimum PSAT score requirement?",
                        "isLogical": true,
                        "isRequired": true,
                        "isCalculate": true,
                        "questions": [
                            {
                                "text": "Total Score (320-1520)",
                                "code": "ASSO_PSAT_NMSQT_TOTAL",
                                "placeholder": "Total",
                                "isRequired": true,
                                "minValue": 320,
                                "maxValue": 1520,
                                "type": "number"
                            }
                        ]
                    },
                    {
                        "text": "Not Applicable",
                        "value": "Not Applicable",
                        "type": "checkbox",
                        "isRequired": true,
                        "heading": "",
                    }
                ]
            },
            {
                "text": "Bachelors",
                "value": "Bachelors",
                "heading": "Which entrance exam scores are considered by the institution to apply for Bachelors (Select all that apply)",
                "code": "DEGREECERTIFICATE_BACHELORS",
                "type": "checkbox",
                "isLogical": true,
                "questions": [
                    {
                        "text": "TOEFL",
                        "value": "TOEFL",
                        "type": "checkbox",
                        "heading": "What is the institution’s minimum TOEFL score requirement?",
                        "isRequired": true,
                        "isLogical": true,
                        "isCalculate": true,
                        "questions": [
                            {
                                "text": "Reading (0-30)",
                                "code": "BACHE_TOEFL_READING",
                                "placeholder": "Reading",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number"
                            },
                            {
                                "text": "Listening (0-30)",
                                "code": "BACHE_TOEFL_LISTENING",
                                "placeholder": "Listening",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number"
                            },
                            {
                                "text": "Speaking (0-30)",
                                "code": "BACHE_TOEFL_SPEAKING",
                                "placeholder": "Speaking",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number"
                            },
                            {
                                "text": "Writing (0-30)",
                                "code": "BACHE_TOEFL_WRITING",
                                "placeholder": "Writing",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number"
                            },
                            {
                                "text": "Total (0-120)",
                                "code": "BACHE_TOEFL_TOTAL",
                                "placeholder": "Total",
                                "isRequired": true,
                                "minValue": 0,
                                "maxValue": 120,
                                "type": "number"
                            }
                        ]
                    },
                    {
                        "text": "SAT",
                        "value": "SAT",
                        "type": "checkbox",
                        "heading": "What is the institution’s minimum SAT score requirement?",
                        "isLogical": true,
                        "isRequired": true,
                        "isCalculate": true,
                        "questions": [
                            {
                                "text": "Evidence based Reading & Writing (200-800)",
                                "code": "BACHE_SAT_EBRW",
                                "placeholder": "EBRW",
                                "minValue": 200,
                                "maxValue": 800,
                                "type": "number"
                            },
                            {
                                "text": "Math Score (200-800)",
                                "code": "BACHE_SAT_MATH",
                                "placeholder": "Math",
                                "minValue": 200,
                                "maxValue": 800,
                                "type": "number"
                            },
                            {
                                "text": "Total Score (400-1600)",
                                "code": "BACHE_SAT_TOTAL",
                                "placeholder": "Total",
                                "isRequired": true,
                                "minValue": 400,
                                "maxValue": 1600,
                                "type": "number"
                            }
                        ]
                    },
                    {
                        "text": "ACT",
                        "value": "ACT",
                        "type": "checkbox",
                        "heading": "What is the institution’s minimum ACT score requirement?",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                            {
                                "text": "English (1-36)",
                                "code": "BACHE_ACT_ENGLISH",
                                "placeholder": "English",
                                "minValue": 1,
                                "maxValue": 36,
                                "type": "number"
                            },
                            {
                                "text": "Reading (1-36)",
                                "code": "BACHE_ACT_READING",
                                "placeholder": "Reading",
                                "minValue": 1,
                                "maxValue": 36,
                                "type": "number"
                            },
                            {
                                "text": "Math (1-36)",
                                "code": "BACHE_ACT_MATH",
                                "placeholder": "Math",
                                "minValue": 1,
                                "maxValue": 36,
                                "type": "number"
                            },
                            {
                                "text": "Science  (1-36)",
                                "code": "BACHE_ACT_SCIENCE",
                                "placeholder": "Science",
                                "minValue": 1,
                                "maxValue": 36,
                                "type": "number"
                            },
                            {
                                "text": "Composite Score (1-36)",
                                "code": "BACHE_ACT_COMPOSITESCORE",
                                "placeholder": "Score",
                                "isRequired": true,
                                "minValue": 1,
                                "maxValue": 36,
                                "type": "number"
                            }
                        ]
                    },
                    {
                        "text": "IELTS",
                        "value": "IELTS",
                        "type": "checkbox",
                        "heading": "What is the institution’s minimum IELTS score requirement?",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                            {
                                "text": "Listening (1-9)",
                                "code": "BACHE_IELTS_LISTENING",
                                "placeholder": "Listening",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float"
                            },
                            {
                                "text": "Reading (1-9)",
                                "code": "BACHE_IELTS_READING",
                                "placeholder": "Reading",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float"
                            },
                            {
                                "text": "Writing (1-9)",
                                "code": "BACHE_IELTS_WRITING",
                                "placeholder": "Writing",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float"
                            },
                            {
                                "text": "Speaking (1-9)",
                                "code": "BACHE_IELTS_SPEAKING",
                                "placeholder": "Speaking",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float"
                            },
                            {
                                "text": "Band Score (1-9)",
                                "code": "BACHE_IELTS_BANDSCORE",
                                "placeholder": "Score",
                                "isRequired": true,
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float"
                            }
                        ]
                    },
                    {
                        "text": "PSAT",
                        "value": "PSAT",
                        "type": "checkbox",
                        "heading": "What is the institution’s minimum PSAT score requirement?",
                        "isLogical": true,
                        "isRequired": true,
                        "isCalculate": true,
                        "questions": [
                            {
                                "text": "Total Score (320-1520)",
                                "code": "BACHE_PSAT_NMSQT_TOTAL",
                                "placeholder": "Total",
                                "isRequired": true,
                                "minValue": 320,
                                "maxValue": 1520,
                                "type": "number"
                            }
                        ]
                    },
                    {
                        "text": "Not Applicable",
                        "value": "Not Applicable",
                        "isRequired": true,
                        "heading": "",
                    }
                ]
            },
            {
                "text": "Masters (Science)",
                "value": "Masters (Science)",
                "heading": "Which entrance exam scores are considered by the institution to apply for Masters (Science) (Select all that apply)",
                "code": "DEGREECERTIFICATE_MASTERS",
                "type": "checkbox",
                "isLogical": true,
                "isRequired": true,
                "questions": [
                    {
                        "text": "GRE",
                        "value": "GRE",
                        "heading": "What is the institution’s minimum GRE score requirement?",
                        "isLogical": true,
                        "isRequired": true,
                        "isCalculate": true,
                        "questions": [
                            {
                                "text": "Quantitative (130-170)",
                                "code": "MASTER_GRE_QUANTITATIVE",
                                "placeholder": "Quantitative",
                                "minValue": 130,
                                "maxValue": 170,
                                "type": "number",
                            },
                            {
                                "text": "Verbal (130-170)",
                                "code": "MASTER_GRE_VERBAL",
                                "placeholder": "Verbal",
                                "minValue": 130,
                                "maxValue": 170,
                                "type": "number",
                            },
                            {
                                "text": "Analytical Writing AWA  (0-6)",
                                "code": "MASTER_GRE_ANALYTICAL",
                                "placeholder": "Analytical",
                                "minValue": 0,
                                "maxValue": 6,
                                "type": "float",
                            },
                            {
                                "text": "Total Score (260-340)",
                                "code": "MASTER_GRE_TOTAL",
                                "placeholder": "Total",
                                "isRequired": true,
                                "minValue": 260,
                                "maxValue": 340,
                                "type": "number",
                            }
                        ]
                    },
                    {
                        "text": "TOEFL",
                        "value": "TOEFL",
                        "heading": "What is the institution’s minimum TOEFL score requirement?",
                        "isLogical": true,
                        "isRequired": true,
                        "isCalculate": true,
                        "questions": [
                            {
                                "text": "Reading (0-30)",
                                "code": "MASTER_TOEFL_READING",
                                "placeholder": "Reading",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number",
                            },
                            {
                                "text": "Listening (0-30)",
                                "code": "MASTER_TOEFL_LISTENING",
                                "placeholder": "Listening",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number",
                            },
                            {
                                "text": "Speaking (0-30)",
                                "code": "MASTER_TOEFL_SPEAKING",
                                "placeholder": "Speaking",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number",
                            },
                            {
                                "text": "Writing (0-30)",
                                "code": "MASTER_TOEFL_WRITING",
                                "placeholder": "Writing",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number",
                            },
                            {
                                "text": "Total (0-120)",
                                "code": "MASTER_TOEFL_TOTAL",
                                "isRequired": true,
                                "placeholder": "Total",
                                "minValue": 0,
                                "maxValue": 120,
                                "type": "number",
                            }
                        ]
                    },
                    {
                        "text": "IELTS",
                        "value": "IELTS",
                        "heading": "What is the institution’s minimum IELTS score requirement?",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                            {
                                "text": "Listening (1-9)",
                                "code": "MASTER_IELTS_LISTENING",
                                "placeholder": "Listening",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float",
                            },
                            {
                                "text": "Reading (1-9)",
                                "code": "MASTER_IELTS_READING",
                                "placeholder": "Reading",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float",
                            },
                            {
                                "text": "Writing (1-9)",
                                "code": "MASTER_IELTS_WRITING",
                                "placeholder": "Writing",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float",
                            },
                            {
                                "text": "Speaking (1-9)",
                                "code": "MASTER_IELTS_SPEAKING",
                                "placeholder": "Speaking",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float",
                            },
                            {
                                "text": "Band Score (1-9)",
                                "code": "MASTER_IELTS_BANDSCORE",
                                "placeholder": "Score",
                                "isRequired": true,
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float",
                            }
                        ]
                    },
                    {
                        "text": "LSAT",
                        "value": "LSAT",
                        "heading": "What is the institution’s minimum LSAT score requirement?",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                            {
                                "text": "LSAT Score (120-180)",
                                "code": "MASTER_LSAT_LSATSCORE",
                                "placeholder": "LSAT",
                                "isRequired": true,
                                "minValue": 120,
                                "maxValue": 180,
                                "type": "number",
                            }
                        ]
                    },
                    {
                        "text": "GMAT",
                        "value": "GMAT",
                        "heading": "What is the institution’s minimum GMAT score requirement?",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                            {
                                "text": "Quantitative (0-60)",
                                "code": "MASTER_GMAT_QUANTITATIVE",
                                "placeholder": "Quantitative",
                                "minValue": 0,
                                "maxValue": 60,
                                "type": "number",
                            },
                            {
                                "text": "Verbal (0-60)",
                                "code": "MASTER_GMAT_VERBAL",
                                "placeholder": "Verbal",
                                "minValue": 0,
                                "maxValue": 60,
                                "type": "number",
                            },
                            {
                                "text": "Total (200-800)",
                                "code": "MASTER_GMAT_Total",
                                "placeholder": "Total",
                                "isRequired": true,
                                "minValue": 200,
                                "maxValue": 800,
                                "type": "number",
                            }
                        ]
                    },
                    {
                        "text": "Not Applicable",
                        "value": "Not Applicable",
                        "isRequired": true,
                        "heading": "",
                    }

                ]
            },
            {
                "text": "Masters (General)",
                "value": "Masters (General)",
                "heading": "Which entrance exam scores are considered by the institution to apply for Masters (General) (Select all that apply)",
                "code": "DEGREECERTIFICATE_MASTERS_GENERAL",
                "type": "checkbox",
                "isLogical": true,
                "isRequired": true,
                "questions": [
                    {
                        "text": "TOEFL",
                        "value": "TOEFL",
                        "heading": "What is the institution’s minimum TOEFL score requirement?",
                        "isLogical": true,
                        "isRequired": true,
                        "isCalculate": true,
                        "questions": [
                            {
                                "text": "Reading (0-30)",
                                "code": "MASTERS_GENERAL_TOEFL_READING",
                                "placeholder": "Reading",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number",
                            },
                            {
                                "text": "Listening (0-30)",
                                "code": "MASTERS_GENERAL_TOEFL_LISTENING",
                                "placeholder": "Listening",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number",
                            },
                            {
                                "text": "Speaking (0-30)",
                                "code": "MASTERS_GENERAL_TOEFL_SPEAKING",
                                "placeholder": "Speaking",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number",
                            },
                            {
                                "text": "Writing (0-30)",
                                "code": "MASTERS_GENERAL_TOEFL_WRITING",
                                "placeholder": "Writing",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number",
                            },
                            {
                                "text": "Total (0-120)",
                                "code": "MASTERS_GENERAL_TOEFL_TOTAL",
                                "isRequired": true,
                                "placeholder": "Total",
                                "minValue": 0,
                                "maxValue": 120,
                                "type": "number",
                            }
                        ]
                    },
                    {
                        "text": "IELTS",
                        "value": "IELTS",
                        "heading": "What is the institution’s minimum IELTS score requirement?",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                            {
                                "text": "Listening (1-9)",
                                "code": "MASTERS_GENERAL_IELTS_LISTENING",
                                "placeholder": "Listening",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float",
                            },
                            {
                                "text": "Reading (1-9)",
                                "code": "MASTERS_GENERAL_IELTS_READING",
                                "placeholder": "Reading",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float",
                            },
                            {
                                "text": "Writing (1-9)",
                                "code": "MASTERS_GENERAL_IELTS_WRITING",
                                "placeholder": "Writing",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float",
                            },
                            {
                                "text": "Speaking (1-9)",
                                "code": "MASTERS_GENERAL_IELTS_SPEAKING",
                                "placeholder": "Speaking",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float",
                            },
                            {
                                "text": "Band Score (1-9)",
                                "code": "MASTERS_GENERAL_IELTS_BANDSCORE",
                                "placeholder": "Score",
                                "isRequired": true,
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float",
                            }
                        ]
                    },
                    {
                        "text": "Not Applicable",
                        "value": "Not Applicable",
                        "isRequired": true,
                        "heading": "",
                    }
                ]
            },
            {
                "text": "Masters (Management)",
                "value": "Masters (Management)",
                "heading": "Which entrance exam scores are considered by the institution to apply for Masters (Management) (Select all that apply)",
                "code": "DEGREECERTIFICATE_MASTERS_MANAGEMENT",
                "type": "checkbox",
                "isLogical": true,
                "isRequired": true,
                "questions": [
                    {
                        "text": "GRE",
                        "value": "GRE",
                        "heading": "What is the institution’s minimum GRE score requirement?",
                        "isLogical": true,
                        "isRequired": true,
                        "isCalculate": true,
                        "questions": [
                            {
                                "text": "Quantitative (130-170)",
                                "code": "MASTER_MGMT_GRE_QUANTITATIVE",
                                "placeholder": "Quantitative",
                                "minValue": 130,
                                "maxValue": 170,
                                "type": "number",
                            },
                            {
                                "text": "Verbal (130-170)",
                                "code": "MASTER_MGMT_GRE_VERBAL",
                                "placeholder": "Verbal",
                                "minValue": 130,
                                "maxValue": 170,
                                "type": "number",
                            },
                            {
                                "text": "Analytical Writing AWA  (0-6)",
                                "code": "MASTER_MGMT_GRE_ANALYTICAL",
                                "placeholder": "Analytical",
                                "minValue": 0,
                                "maxValue": 6,
                                "type": "float",
                            },
                            {
                                "text": "Total Score (260-340)",
                                "code": "MASTER_MGMT_GRE_TOTAL",
                                "placeholder": "Total",
                                "isRequired": true,
                                "minValue": 260,
                                "maxValue": 340,
                                "type": "number",
                            }
                        ]
                    },
                    {
                        "text": "TOEFL",
                        "value": "TOEFL",
                        "heading": "What is the institution’s minimum TOEFL score requirement?",
                        "isLogical": true,
                        "isRequired": true,
                        "isCalculate": true,
                        "questions": [
                            {
                                "text": "Reading (0-30)",
                                "code": "MASTER_MGMT_TOEFL_READING",
                                "placeholder": "Reading",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number",
                            },
                            {
                                "text": "Listening (0-30)",
                                "code": "MASTER_MGMT_TOEFL_LISTENING",
                                "placeholder": "Listening",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number",
                            },
                            {
                                "text": "Speaking (0-30)",
                                "code": "MASTER_MGMT_TOEFL_SPEAKING",
                                "placeholder": "Speaking",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number",
                            },
                            {
                                "text": "Writing (0-30)",
                                "code": "MASTER_MGMT_TOEFL_WRITING",
                                "placeholder": "Writing",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number",
                            },
                            {
                                "text": "Total (0-120)",
                                "code": "MASTER_MGMT_TOEFL_TOTAL",
                                "isRequired": true,
                                "placeholder": "Total",
                                "minValue": 0,
                                "maxValue": 120,
                                "type": "number",
                            }
                        ]
                    },
                    {
                        "text": "IELTS",
                        "value": "IELTS",
                        "heading": "What is the institution’s minimum IELTS score requirement?",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                            {
                                "text": "Listening (1-9)",
                                "code": "MASTER_MGMT_IELTS_LISTENING",
                                "placeholder": "Listening",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float",
                            },
                            {
                                "text": "Reading (1-9)",
                                "code": "MASTER_MGMT_IELTS_READING",
                                "placeholder": "Reading",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float",
                            },
                            {
                                "text": "Writing (1-9)",
                                "code": "MASTER_MGMT_IELTS_WRITING",
                                "placeholder": "Writing",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float",
                            },
                            {
                                "text": "Speaking (1-9)",
                                "code": "MASTER_MGMT_IELTS_SPEAKING",
                                "placeholder": "Speaking",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float",
                            },
                            {
                                "text": "Band Score (1-9)",
                                "code": "MASTER_MGMT_IELTS_BANDSCORE",
                                "placeholder": "Score",
                                "isRequired": true,
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float",
                            }
                        ]
                    },
                    {
                        "text": "LSAT",
                        "value": "LSAT",
                        "heading": "What is the institution’s minimum LSAT score requirement?",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                            {
                                "text": "LSAT Score (120-180)",
                                "code": "MASTER_MGMT_LSAT_LSATSCORE",
                                "placeholder": "LSAT",
                                "isRequired": true,
                                "minValue": 120,
                                "maxValue": 180,
                                "type": "number",
                            }
                        ]
                    },
                    {
                        "text": "GMAT",
                        "value": "GMAT",
                        "heading": "What is the institution’s minimum GMAT score requirement?",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                            {
                                "text": "Quantitative (0-60)",
                                "code": "MASTER_MGMT_GMAT_QUANTITATIVE",
                                "placeholder": "Quantitative",
                                "minValue": 0,
                                "maxValue": 60,
                                "type": "number",
                            },
                            {
                                "text": "Verbal (0-60)",
                                "code": "MASTER_MGMT_GMAT_VERBAL",
                                "placeholder": "Verbal",
                                "minValue": 0,
                                "maxValue": 60,
                                "type": "number",
                            },
                            {
                                "text": "Total (200-800)",
                                "code": "MASTER_MGMT_GMAT_Total",
                                "placeholder": "Total",
                                "isRequired": true,
                                "minValue": 200,
                                "maxValue": 800,
                                "type": "number",
                            }
                        ]
                    },
                    {
                        "text": "Not Applicable",
                        "value": "Not Applicable",
                        "isRequired": true,
                        "heading": "",
                    }

                ]
            },
            {
                "text": "Post Graduate Certificates",
                "value": "Post Graduate Certificates",
                "heading": "Which entrance exam scores are considered by the institution to apply for Post Graduate Certificates (Select all that apply)",
                "code": "DEGREECERTIFICATE_POST_GRADUATE",
                "type": "checkbox",
                "isLogical": true,
                "isRequired": true,
                "questions": [
                    {
                        "text": "GRE",
                        "value": "GRE",
                        "heading": "What is the institution’s minimum GRE score requirement?",
                        "isLogical": true,
                        "isRequired": true,
                        "isCalculate": true,
                        "questions": [
                            {
                                "text": "Quantitative (130-170)",
                                "code": "POST_GRAD_GRE_QUANTITATIVE",
                                "placeholder": "Quantitative",
                                "minValue": 130,
                                "maxValue": 170,
                                "type": "number",
                            },
                            {
                                "text": "Verbal (130-170)",
                                "code": "POST_GRAD_GRE_VERBAL",
                                "placeholder": "Verbal",
                                "minValue": 130,
                                "maxValue": 170,
                                "type": "number",
                            },
                            {
                                "text": "Analytical Writing AWA  (0-6)",
                                "code": "POST_GRAD_GRE_ANALYTICAL",
                                "placeholder": "Analytical",
                                "minValue": 0,
                                "maxValue": 6,
                                "type": "float",
                            },
                            {
                                "text": "Total Score (260-340)",
                                "code": "POST_GRAD_GRE_TOTAL",
                                "placeholder": "Total",
                                "isRequired": true,
                                "minValue": 260,
                                "maxValue": 340,
                                "type": "number",
                            }
                        ]
                    },
                    {
                        "text": "TOEFL",
                        "value": "TOEFL",
                        "heading": "What is the institution’s minimum TOEFL score requirement?",
                        "isLogical": true,
                        "isRequired": true,
                        "isCalculate": true,
                        "questions": [
                            {
                                "text": "Reading (0-30)",
                                "code": "POST_GRAD_TOEFL_READING",
                                "placeholder": "Reading",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number",
                            },
                            {
                                "text": "Listening (0-30)",
                                "code": "POST_GRAD_TOEFL_LISTENING",
                                "placeholder": "Listening",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number",
                            },
                            {
                                "text": "Speaking (0-30)",
                                "code": "POST_GRAD_TOEFL_SPEAKING",
                                "placeholder": "Speaking",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number",
                            },
                            {
                                "text": "Writing (0-30)",
                                "code": "POST_GRAD_TOEFL_WRITING",
                                "placeholder": "Writing",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number",
                            },
                            {
                                "text": "Total (0-120)",
                                "code": "POST_GRAD_TOEFL_TOTAL",
                                "isRequired": true,
                                "placeholder": "Total",
                                "minValue": 0,
                                "maxValue": 120,
                                "type": "number",
                            }
                        ]
                    },
                    {
                        "text": "IELTS",
                        "value": "IELTS",
                        "heading": "What is the institution’s minimum IELTS score requirement?",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                            {
                                "text": "Listening (1-9)",
                                "code": "POST_GRAD_IELTS_LISTENING",
                                "placeholder": "Listening",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float",
                            },
                            {
                                "text": "Reading (1-9)",
                                "code": "POST_GRAD_IELTS_READING",
                                "placeholder": "Reading",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float",
                            },
                            {
                                "text": "Writing (1-9)",
                                "code": "POST_GRAD_IELTS_WRITING",
                                "placeholder": "Writing",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float",
                            },
                            {
                                "text": "Speaking (1-9)",
                                "code": "POST_GRAD_IELTS_SPEAKING",
                                "placeholder": "Speaking",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float",
                            },
                            {
                                "text": "Band Score (1-9)",
                                "code": "POST_GRAD_IELTS_BANDSCORE",
                                "placeholder": "Score",
                                "isRequired": true,
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float",
                            }
                        ]
                    },
                    {
                        "text": "LSAT",
                        "value": "LSAT",
                        "heading": "What is the institution’s minimum LSAT score requirement?",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                            {
                                "text": "LSAT Score (120-180)",
                                "code": "POST_GRAD_LSAT_LSATSCORE",
                                "placeholder": "LSAT",
                                "isRequired": true,
                                "minValue": 120,
                                "maxValue": 180,
                                "type": "number",
                            }
                        ]
                    },
                    {
                        "text": "GMAT",
                        "value": "GMAT",
                        "heading": "What is the institution’s minimum GMAT score requirement?",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                            {
                                "text": "Quantitative (0-60)",
                                "code": "POST_GRAD_GMAT_QUANTITATIVE",
                                "placeholder": "Quantitative",
                                "minValue": 0,
                                "maxValue": 60,
                                "type": "number",
                            },
                            {
                                "text": "Verbal (0-60)",
                                "code": "POST_GRAD_GMAT_VERBAL",
                                "placeholder": "Verbal",
                                "minValue": 0,
                                "maxValue": 60,
                                "type": "number",
                            },
                            {
                                "text": "Total (200-800)",
                                "code": "POST_GRAD_GMAT_Total",
                                "placeholder": "Total",
                                "isRequired": true,
                                "minValue": 200,
                                "maxValue": 800,
                                "type": "number",
                            }
                        ]
                    },
                    {
                        "text": "Not Applicable",
                        "value": "Not Applicable",
                        "isRequired": true,
                        "heading": "",
                    }

                ]
            },
            {
                "text": "Doctorate / PhD / EdD",
                "value": "Doctorate / PhD / EdD",
                "heading": "Which entrance exam scores are considered by the institution to apply for Doctorate / PhD / EdD (Select all that apply)",
                "code": "DEGREECERTIFICATE_DOCTORATE",
                "type": "checkbox",
                "isLogical": true,
                "isRequired": true,
                "questions": [
                    {
                        "text": "TOEFL",
                        "value": "TOEFL",
                        "heading": "What is the institution’s minimum TOEFL score requirement?",
                        "isLogical": true,
                        "isRequired": true,
                        "isCalculate": true,
                        "questions": [
                            {
                                "text": "Reading (0-30)",
                                "code": "DOCTORATE_TOEFL_READING",
                                "placeholder": "Reading",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number",
                            },
                            {
                                "text": "Listening (0-30)",
                                "code": "DOCTORATE_TOEFL_LISTENING",
                                "placeholder": "Listening",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number",
                            },
                            {
                                "text": "Speaking (0-30)",
                                "code": "DOCTORATE_TOEFL_SPEAKING",
                                "placeholder": "Speaking",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number",
                            },
                            {
                                "text": "Writing (0-30)",
                                "code": "DOCTORATE_TOEFL_WRITING",
                                "placeholder": "Writing",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number",
                            },
                            {
                                "text": "Total (0-120)",
                                "code": "DOCTORATE_TOEFL_TOTAL",
                                "isRequired": true,
                                "placeholder": "Total",
                                "minValue": 0,
                                "maxValue": 120,
                                "type": "number",
                            }
                        ]
                    },
                    {
                        "text": "IELTS",
                        "value": "IELTS",
                        "heading": "What is the institution’s minimum IELTS score requirement?",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                            {
                                "text": "Listening (1-9)",
                                "code": "DOCTORATE_IELTS_LISTENING",
                                "placeholder": "Listening",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float",
                            },
                            {
                                "text": "Reading (1-9)",
                                "code": "DOCTORATE_IELTS_READING",
                                "placeholder": "Reading",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float",
                            },
                            {
                                "text": "Writing (1-9)",
                                "code": "DOCTORATE_IELTS_WRITING",
                                "placeholder": "Writing",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float",
                            },
                            {
                                "text": "Speaking (1-9)",
                                "code": "DOCTORATE_IELTS_SPEAKING",
                                "placeholder": "Speaking",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float",
                            },
                            {
                                "text": "Band Score (1-9)",
                                "code": "DOCTORATE_IELTS_BANDSCORE",
                                "placeholder": "Score",
                                "isRequired": true,
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float",
                            }
                        ]
                    },
                    {
                        "text": "Not Applicable",
                        "value": "Not Applicable",
                        "isRequired": true,
                        "heading": "",
                    }
                ]
            },
            {
                "text": "Post-Doctoral",
                "value": "Post-Doctoral",
                "heading": "Which entrance exam scores are considered by the institution to apply for Post-Doctoral (Select all that apply)",
                "code": "DEGREECERTIFICATE_POST_DOCTORATE",
                "type": "checkbox",
                "isLogical": true,
                "isRequired": true,
                "questions": [
                    {
                        "text": "TOEFL",
                        "value": "TOEFL",
                        "heading": "What is the institution’s minimum TOEFL score requirement?",
                        "isLogical": true,
                        "isRequired": true,
                        "isCalculate": true,
                        "questions": [
                            {
                                "text": "Reading (0-30)",
                                "code": "POST_DOCTORAL_TOEFL_READING",
                                "placeholder": "Reading",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number",
                            },
                            {
                                "text": "Listening (0-30)",
                                "code": "POST_DOCTORAL_TOEFL_LISTENING",
                                "placeholder": "Listening",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number",
                            },
                            {
                                "text": "Speaking (0-30)",
                                "code": "POST_DOCTORAL_TOEFL_SPEAKING",
                                "placeholder": "Speaking",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number",
                            },
                            {
                                "text": "Writing (0-30)",
                                "code": "POST_DOCTORAL_TOEFL_WRITING",
                                "placeholder": "Writing",
                                "minValue": 0,
                                "maxValue": 30,
                                "type": "number",
                            },
                            {
                                "text": "Total (0-120)",
                                "code": "POST_DOCTORAL_TOEFL_TOTAL",
                                "isRequired": true,
                                "placeholder": "Total",
                                "minValue": 0,
                                "maxValue": 120,
                                "type": "number",
                            }
                        ]
                    },
                    {
                        "text": "IELTS",
                        "value": "IELTS",
                        "heading": "What is the institution’s minimum IELTS score requirement?",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                            {
                                "text": "Listening (1-9)",
                                "code": "POST_DOCTORAL_IELTS_LISTENING",
                                "placeholder": "Listening",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float",
                            },
                            {
                                "text": "Reading (1-9)",
                                "code": "POST_DOCTORAL_IELTS_READING",
                                "placeholder": "Reading",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float",
                            },
                            {
                                "text": "Writing (1-9)",
                                "code": "POST_DOCTORAL_IELTS_WRITING",
                                "placeholder": "Writing",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float",
                            },
                            {
                                "text": "Speaking (1-9)",
                                "code": "POST_DOCTORAL_IELTS_SPEAKING",
                                "placeholder": "Speaking",
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float",
                            },
                            {
                                "text": "Band Score (1-9)",
                                "code": "POST_DOCTORAL_IELTS_BANDSCORE",
                                "placeholder": "Score",
                                "isRequired": true,
                                "minValue": 1,
                                "maxValue": 9,
                                "type": "float",
                            }
                        ]
                    },
                    {
                        "text": "Not Applicable",
                        "value": "Not Applicable",
                        "isRequired": true,
                        "heading": "",
                    }
                ]
            },
            {
                "text": "Vocational Training",
                "value": "Vocational Training",
                "heading": "Which entrance exam scores are considered by the institution to apply for Vocational Training (Select all that apply)",
                "code": "VOCATIONAL_TRAINING",
                "type": "checkbox",
                "isLogical": true,
                "isRequired": true,
                "questions":[
                    {
                        "section": "aboutTheInstitution",
                        "text": "Please share website link offering details on programs offered and the cost of attending Vocational Training programs?",
                        "code": "COSTOFATTENDINGLINK_VOCATIONAL",
                        "placeholder": "",
                        "type": "text",
                        "stream": "engineering",
                        "isRequired": true,
                    },
                ]
            },
            {
                "text": "Summer Internships",
                "value": "Summer Internships",
                "heading": "Which entrance exam scores are considered by the institution to apply for Summer Internships (Select all that apply)",
                "code": "SUMMER_INTERNSHIP",
                "type": "checkbox",
                "isLogical": true,
                "isRequired": true,
                "questions":[
                    {
                        "section": "aboutTheInstitution",
                        "text": "Please share website link offering details on programs offered and the cost of attending Summer Internships programs?",
                        "code": "COSTOFATTENDINGLINK_SUMMER",
                        "placeholder": "",
                        "type": "text",
                        "stream": "engineering",
                        "isRequired": true,
                    },
                ]
            },
            {
                "text": "Bridge for Bachelors",
                "value": "Bridge for Bachelors",
                "heading": "Which entrance exam scores are considered by the institution to apply for Bridge for Bachelors (Select all that apply)",
                "code": "BRIDGE_FOR_BACHELORS",
                "type": "checkbox",
                "isLogical": true,
                "isRequired": true,
                "questions":[
                    {
                        "section": "aboutTheInstitution",
                        "text": "Please share website link offering details on programs offered and the cost of attending Bridge for Bachelors programs?",
                        "code": "COSTOFATTENDINGLINK_BRIDGEFORBACHELORS",
                        "placeholder": "",
                        "type": "text",
                        "stream": "engineering",
                        "isRequired": true,
                    },
                ]
            },
            {
                "text": "Bridge for Masters",
                "value": "Bridge for Masters",
                "heading": "Which entrance exam scores are considered by the institution to apply for Bridge for Masters (Select all that apply)",
                "code": "BRIDGE_FOR_MASTERS",
                "type": "checkbox",
                "isLogical": true,
                "isRequired": true,
                "questions":[
                    {
                        "section": "aboutTheInstitution",
                        "text": "Please share website link offering details on programs offered and the cost of attending Bridge for Masters programs?",
                        "code": "COSTOFATTENDINGLINK_BRIDGEFORMASTERS",
                        "placeholder": "",
                        "type": "text",
                        "stream": "engineering",
                        "isRequired": true,
                    },
                ]
            },
            
        ],
        "placeholder": "",
        "type": "checkbox",
        "stream": "engineering"
    },
    {
        "section": "admissions",
        "isRequired": true,
        "text": "For undergraduate degrees, what is the institution’s min. HIGHSCHOOL unweighted GPA requirement? (Out of 4)",
        "code": "UNWEIGHTGPA_SCHOOL",
        "options": [
            {
                "text": "3.875+",
                "value": "3.875+"
            },
            {
                "text": "3.75+",
                "value": "3.75+"
            },
            {
                "text": "3.625+",
                "value": "3.625+"
            },
            {
                "text": "3.5+",
                "value": "3.5+"
            },
            {
                "text": "3.375+",
                "value": "3.375+"
            },
            {
                "text": "3.25+",
                "value": "3.25+"
            },
            {
                "text": "3.0+",
                "value": "3.0+"
            },
            {
                "text": "Less than 3",
                "value": "Less than 3"
            },
        ],
        "placeholder": "",
        "type": "dropDown",
        "stream": "engineering"
    },
    {
        "section": "admissions",
        "text": "For undergraduate degrees, what is the institution’s min. HIGHSCHOOL weighted GPA requirement? (Out of 5)",
        "code": "WEIGHTGPA_SCHOOL",
        "isRequired": true,
        "options": [
            {
                "text": "4.875+",
                "value": "4.875+"
            },
            {
                "text": "4.75+",
                "value": "4.75+"
            },
            {
                "text": "4.625+",
                "value": "4.625+"
            },
            {
                "text": "4.5+",
                "value": "4.5+"
            },
            {
                "text": "4.375+",
                "value": "4.375+"
            },
            {
                "text": "4.25+",
                "value": "4.25+"
            },
            {
                "text": "4.0+",
                "value": "4.0+"
            },
            {
                "text": "3.875+",
                "value": "3.875+"
            },
            {
                "text": "3.75+",
                "value": "3.75+"
            },
            {
                "text": "3.625+",
                "value": "3.625+"
            },
            {
                "text": "3.5+",
                "value": "3.5+"
            },
            {
                "text": "3.375+",
                "value": "3.375+"
            },
            {
                "text": "3.25+",
                "value": "3.25+"
            },
            {
                "text": "3.0+",
                "value": "3.0+"
            },
            {
                "text": "Less than 3",
                "value": "Less than 3"
            },
        ],
        "placeholder": "",
        "type": "dropDown",
        "stream": "engineering"
    },
    {
        "section": "admissions",
        "isRequired": true,
        "text": "For graduate degrees, what is the institution’s min. COLLEGE unweighted GPA requirement? (Out of 4)",
        "code": "UNWEIGHTGPA_COLLEGE",
        "options": [
            {
                "text": "3.875+",
                "value": "3.875+"
            },
            {
                "text": "3.75+",
                "value": "3.75+"
            },
            {
                "text": "3.625+",
                "value": "3.625+"
            },
            {
                "text": "3.5+",
                "value": "3.5+"
            },
            {
                "text": "3.375+",
                "value": "3.375+"
            },
            {
                "text": "3.25+",
                "value": "3.25+"
            },
            {
                "text": "3.0+",
                "value": "3.0+"
            },
            {
                "text": "Less than 3",
                "value": "Less than 3"
            },
        ],
        "placeholder": "",
        "type": "dropDown",
        "stream": "engineering"
    },
    {
        "section": "admissions",
        "text": "For graduate degrees, what is the institution’s min. COLLEGE weighted GPA requirement? (Out of 5)",
        "code": "WEIGHTGPA_COLLEGE",
        "isRequired": true,
        "options": [
            {
                "text": "4.875+",
                "value": "4.875+"
            },
            {
                "text": "4.75+",
                "value": "4.75+"
            },
            {
                "text": "4.625+",
                "value": "4.625+"
            },
            {
                "text": "4.5+",
                "value": "4.5+"
            },
            {
                "text": "4.375+",
                "value": "4.375+"
            },
            {
                "text": "4.25+",
                "value": "4.25+"
            },
            {
                "text": "4.0+",
                "value": "4.0+"
            },
            {
                "text": "3.875+",
                "value": "3.875+"
            },
            {
                "text": "3.75+",
                "value": "3.75+"
            },
            {
                "text": "3.625+",
                "value": "3.625+"
            },
            {
                "text": "3.5+",
                "value": "3.5+"
            },
            {
                "text": "3.375+",
                "value": "3.375+"
            },
            {
                "text": "3.25+",
                "value": "3.25+"
            },
            {
                "text": "3.0+",
                "value": "3.0+"
            },
            {
                "text": "Less than 3",
                "value": "Less than 3"
            },
        ],
        "placeholder": "",
        "type": "dropDown",
        "stream": "engineering"
    },
    {
        "section": "admissions",
        "text": "Are there any admission requirements that you would like to share other than the ones mentioned above?",
        //"maxLength": 35,
        "code": "ADMISSIONREQUIREMENTS",
        "options": [],
        //"placeholder": "Eg- MS in data science, Ph.D in Artificial intelligence",
        "type": "textArea",
        "stream": "engineering",
    },
    {
        "section": "admissions",
        "text": "Has the institution added any new majors in the past 1-2 years? (Answer only if applicable) ",
        "maxLength": 35,
        "code": "INSTITUTEADDEDNEWMAJORS",
        "options": [],
        "placeholder": "Eg- MS in data science, Ph.D in Artificial intelligence",
        "type": "text",
        "stream": "engineering",
    },
    {
        "section": "admissions",
        "text": "How important are the following criteria for a student’s admission ? (On a scale of 1 - 10)",
        "code": "STUDENTADMISSIONRATE",
        "isRequired": true,
        "hasQuestions": true,
        "isLogical": true,
        "questions": [
            {
                text: "GPA or Aggregate marks",
                code: "AGGREGATEGPA",
                "minRange": 1,
                "maxRange": 10,
                "interval": 1
            },
            {
                text: "English proficiency (For Intl. students only)",
                code: "ENGLISHPROFICIENCY",
                "minRange": 1,
                "maxRange": 10,
                "interval": 1
            },
            {
                text: "Test scores (SAT/ACT, GRE/GMAT etc.)",
                code: "TESTSCORESSAT",
                "minRange": 1,
                "maxRange": 10,
                "interval": 1
            },
            {
                text: "Financial eligibility (Ability to pay fees, Income level)",
                code: "FINANCIALELIGIBILITY",
                "minRange": 1,
                "maxRange": 10,
                "interval": 1
            },
            {
                text: "Statement of Purpose (Essay)",
                code: "STATEOFPURPOSE",
                "minRange": 1,
                "maxRange": 10,
                "interval": 1
            },
            {
                text: "Letters of Recommendation (LOR)",
                code: "LETTEROFRECOMM",
                "minRange": 1,
                "maxRange": 10,
                "interval": 1
            },
            {
                text: "Professional/Volunteering experience",
                code: "PROFESSIONALVOLUNEXPERIENCE",
                "minRange": 1,
                "maxRange": 10,
                "interval": 1
            },
            {
                text: "Extracurriculars",
                code: "EXTRACURRICULAR",
                "minRange": 1,
                "maxRange": 10,
                "interval": 1
            },
            {
                text: "Overall personality",
                code: "OVERALLPERSONALITY",
                "minRange": 1,
                "maxRange": 10,
                "interval": 1
            },

        ],
        "placeholder": "",
        "type": "slider",
        "stream": "engineering"
    }
]

export const academicsAndExtracurriculars = [
    {
        "section": "academicsAndExtracurriculars",
        "text": "Does the institution offer any integrated programs? (Select options if applicable)",
        "code": "INTEGRATEDPROGRAMS",
        "isLogical": true,
        "options": [
            {
                "text": "Bachelor's + Master's (BS/MS)",
                "value": "Bachelor's + Master's (BS/MS)",
            },
            {
                "text": "Bachelor's - Business + Master's - Business (BBA/MBA)",
                "value": "Bachelor's - Business + Master's - Business (BBA/MBA)",
            },
            {
                "text": "Master's + Master's - Business (M.S./M.B.A)",
                "value": "Master's + Master's - Business (M.S./M.B.A)",
            },
            {
                "text": "Master's + Doctorate (MS + PhD)",
                "value": "Master's + Doctorate (MS + PhD)",
            },
            {
                "text": "Master's + STEM (MS + STEM)",
                "value": "Master's + STEM (MS + STEM)",
            },
            {
                "text": "Others",
                "value": "Others",
                "isLogical": true,
                "isRequired": true,
                "questions": [
                    {
                        "section": "academicsAndExtracurriculars",
                        "text": "Others",
                        "isRequired": true,
                        "code": "INTEGRATEDPROGRAMS_OTHEROPTION",
                        "placeholder": "",
                        "type": "text",
                        "stream": "engineering",
                    },
                ]
            },
        ],
        "placeholder": "",
        "type": "checkbox",
        "stream": "engineering",
    },
    {
        "section": "academicsAndExtracurriculars",
        "text": "Which mode of education does the institution follow? (Select all that apply)",
        "code": "MODEOFEDUCATION",
        "isLogical": true,
        "isRequired": true,
        "options": [
            {
                "text": "Online",
                "value": "Online",
                "isLogical": true,
                "isRequired": true,
                "questions" : [
                    {
                        "text" : "Which systems facilitate remote learning to the students (Select all that apply)",
                        "code" : "ONLINE_REMOTELEARNING",
                        "type": "checkbox",
                        "isRequired": true,
                        "isLogical": true,
                        "options": [
                            {
                                "text" : "Learning management systems",
                                "value" : "Learning management systems"
                            },
                            {
                                "text" : "Collaboration tools",
                                "value" : "Collaboration tools"
                            },
                            {
                                "text" : "Productivity tools",
                                "value" : "Productivity tools"
                            },
                            {
                                "text" : "Digital simulations",
                                "value" : "Digital simulations"
                            },
                            {
                                "text" : "Assistive technology",
                                "value" : "Assistive technology"
                            },
                            {
                                "text" : "Others",
                                "value" : "Others",
                                "isLogical": true,
                                "isRequired": true,
                                "questions": [
                                    {
                                        "section": "academicsAndExtracurriculars",
                                        "text": "Others",
                                        "code": "ONLINE_REMOTELEARNING_OTHEROPTION",
                                        "placeholder": "",
                                        "isRequired": true,
                                        "type": "text",
                                        "stream": "engineering",
                                    },
                                ]
                            }
                         ],
                    }
                ]
              
            },
            {
                "text": "Hybrid",
                "value": "Hybrid",
            },
            {
                "text": "In Person",
                "value": "In Person"
            },
        ],
        "type": "checkbox",
        "stream": "engineering",
        "placeholder": "",
        "isRequired": true
    },
    {
        "section": "academicsAndExtracurriculars",
        "text": "Which recreational activities or student clubs are offered by the institution? (Select all that apply)",
        "code": "RECREATIONALACTIVITIES",
        "isLogical": true,
        "options": [
            {
                "text": "Academic (math, literature, language, history)",
                "value": "Academic (math, literature, language, history)"
            },
            {
                "text": "Theater & the arts (drama, film, performing arts)",
                "value": "Theater & the arts (drama, film, performing arts)"
            },
            {
                "text": "Athletics, sports & recreation",
                "value": "Athletics, sports & recreation"
            },
            {
                "text": "Business & entrepreneurship",
                "value": "Business & entrepreneurship"
            },
            {
                "text": "International (country, multicultural)",
                "value": "International (country, multicultural)",
                "isLogical": true,
                "questions": [
                  {
                    "isRequired": true,
                    "text": "Which of the following country clubs are there on campus?",
                    "code": "MULTICULTURALCOUNTRY",
                    // "type": "dropDown",
                    "type": "checkbox",
                    "options": [
                        {
                            "text": "Nigeria",
                            "value": "Nigeria"
                        },
                        {
                            "text": "India",
                            "value": "India"
                        },
                        {
                            "text": "Iran",
                            "value": "Iran"
                        },
                        {
                            "text": "Iraq",
                            "value": "Iraq"
                        },
                        {
                            "text": "Singapore",
                            "value": "Singapore"
                        }
                    ]
                  }
                ]
            },
            {
                "text": "Music & dance (orchestras, choirs, and music)",
                "value": "Music & dance (orchestras, choirs, and music)"
            },
            {
                "text": "Political & government (student senate)",
                "value": "Political & government (student senate)"
            },
            {
                "text": "Health & counseling",
                "value": "Health & counseling"
            },
            {
                "text": "Special interest (culinary, games, travel)",
                "value": "Special interest (culinary, games, travel)"
            },
            {
                "text": "Media & publication",
                "value": "Media & publication"
            },
            {
                "text": "Science & engineering",
                "value": "Science & engineering"
            },
            {
                "text": "Professional development",
                "value": "Professional development"
            },
            {
                "text": "Community service & social justice",
                "value": "Community service & social justice"
            },
            {
                "text": "Religious & spiritual (philosophy, greek life)",
                "value": "Religious & spiritual (philosophy, greek life)"
            },
            {
                "text": "Other",
                "value": "Other",
                "isLogical": true,
                "isRequired": true,
                "questions": [
                    {
                        "section": "academicsAndExtracurriculars",
                        "text": "Other",
                        "code": "RECREATIONALACTIVITIES_OTHEROPTION",
                        "placeholder": "",
                        "isRequired": true,
                        "type": "text",
                        "stream": "engineering",
                    },
                ]
            },
        ],
        "type": "checkbox",
        "stream": "engineering",
        "placeholder": "",
        "isRequired": true
    },
    {
        "section": "academicsAndExtracurriculars",
        "text": "Which of the following intramural sports are offered by the institution? (Select all that apply)",
        "code": "INTRAMURALSPORTS",
        "isLogical": true,
        "options": [
            {
                "text": "Archery ",
                "value": "Archery "
            },
            {
                "text": "Golf",
                "value": "Golf"
            },
            {
                "text": "Softball",
                "value": "Softball"
            },
            {
                "text": "Badminton",
                "value": "Badminton"
            },
            {
                "text": "Gymnastics",
                "value": "Gymnastics"
            },
            {
                "text": "Squash",
                "value": "Squash"
            },
            {
                "text": "Baseball",
                "value": "Baseball"
            },
            {
                "text": "Ice hockey",
                "value": "Ice hockey"
            },
            {
                "text": "Swimming",
                "value": "Swimming"
            },
            {
                "text": "Basketball",
                "value": "Basketball"
            },
            {
                "text": "Lacrosse",
                "value": "Lacrosse"
            },
            {
                "text": "Tennis",
                "value": "Tennis"
            },
            {
                "text": "Cross-country",
                "value": "Cross-country"
            },
            {
                "text": "Martial arts",
                "value": "Martial arts"
            },
            {
                "text": "Track & field",
                "value": "Track & field"
            },
            {
                "text": "Cycling",
                "value": "Cycling"
            },
            {
                "text": "Cricket",
                "value": "Cricket"
            },
            {
                "text": "Rowing",
                "value": "Rowing"
            },
            {
                "text": "Volleyball ",
                "value": "Volleyball "
            },
            {
                "text": "Fencing",
                "value": "Fencing"
            },
            {
                "text": "Rugby",
                "value": "Rugby"
            },
            {
                "text": "Wrestling",
                "value": "Wrestling"
            },
            {
                "text": "Field hockey",
                "value": "Field hockey"
            },
            {
                "text": "Skiing",
                "value": "Skiing"
            },
            {
                "text": "Soccer",
                "value": "Soccer"
            },
            {
                "text": "Football",
                "value": "Football"
            },
            {
                "text": "Others",
                "value": "Others",
                "isLogical": true,
                "isRequired": true,
                "questions": [
                    {
                        "section": "academicsAndExtracurriculars",
                        "text": "Others",
                        "code": "INTRAMURALSPORTS_OTHEROPTION",
                        "placeholder": "",
                        "isRequired": true,
                        "type": "text",
                        "stream": "engineering",
                    },
                ]
            }
        ],
        "type": "checkbox",
        "stream": "engineering",
        "placeholder": "",
        "isRequired": true
    },
]

export const campusInfrastructure = [
    {
        "section": "campusInfrastructure",
        "text": "Which modes of transportation are most convenient for students? (Select all that apply)",
        "code": "STUDENTTRAVELFACILITY",
        "options": [
            {
                "text": "Public transport",
                "value": "Public transport"
            },
            {
                "text": "Bike Rentals",
                "value": "Bike Rentals"
            },
            {
                "text": "Ride-sharing (using internet)",
                "value": "Ride-sharing (using internet)"
            },
            {
                "text": "Campus shuttle services",
                "value": "Campus shuttle services"
            },
            {
                "text": "Free Carpooling",
                "value": "Free Carpooling"
            }
        ],
        "type": "checkbox",
        "stream": "engineering",
        "placeholder": "",
    },
    {
        "section": "campusInfrastructure",
        "text": "Which of the following state-of-art computer facilities are available on campus? (Select all that apply)",
        "code": "COMPUTERFACILITIES",
        "options": [
            {
                "text": "VR/AR lab",
                "value": "VR/AR lab"
            },
            {
                "text": "Robotics lab",
                "value": "Robotics lab"
            },
            {
                "text": "High-performance computers",
                "value": "High-performance computers"
            },
            {
                "text": "Latest OS & software",
                "value": "Latest OS & software"
            },
            {
                "text": "Cloud service access",
                "value": "Cloud service access"
            },
            {
                "text": "Others",
                "value": "Others",
                "isLogical": true,
                "isRequired": true,
                "questions": [
                    {
                        "section": "academicsAndExtracurriculars",
                        "text": "Others",
                        "isRequired": true,
                        "code": "COMPUTERFACILITIES_OTHEROPTION",
                        "placeholder": "",
                        "type": "text",
                        "stream": "engineering",
                    },
                ]
            },
        ],
        "type": "checkbox",
        "stream": "engineering",
        "isLogical": true,
        "placeholder": "",
    },
    {
        "section": "campusInfrastructure",
        "text": "How would you rate the WiFi connectivity on campus?",
        "code": "WIFICONNECTIVITY",
        "hasQuestions": true,
        "isLogical": true,
        "questions": [
            {
                "text": "Speed",
                "code": "SPEED",
                "minRange": 1,
                "maxRange": 5,
                "interval": 1
            },
            {
                "text": "Coverage",
                "code": "COVERAGE",
                "minRange": 1,
                "maxRange": 5,
                "interval": 1
            }
        ],
        "type": "slider",
        "stream": "engineering",
        "placeholder": "",
    },
    {
        "section": "campusInfrastructure",
        "text": "Which of the following types of living accommodations is available at the institute? (Select all that apply)",
        "code": "LIVINGACCOMODATION",
        "options": [
            {
                "text": "Freshmen (1st year)",
                "value": "Freshmen (1st year)"
            },
            {
                "text": "Sophomore (2nd year)",
                "value": "Sophomore (2nd year)"
            },
            {
                "text": "Junior (3rd Year)",
                "value": "Junior (3rd Year)"
            },
            {
                "text": "Senior (4th Year)",
                "value": "Senior (4th Year)"
            },
            {
                "text": "Graduate students",
                "value": "Graduate students"
            },
            {
                "text": "Families (Graduates / PhD)",
                "value": "Families (Graduates / PhD)"
            },
            {
                "text": "No Restrictions",
                "value": "No Restrictions"
            },

        ],
        "type": "checkbox",
        "stream": "engineering",
        "placeholder": "",
        "isRequired": true
    },
    {
        "section": "campusInfrastructure",
        "text": "Which of the following campus security measures are in place? (Select all that apply)",
        "code": "CAMPUSSECURITY",
        "options": [
            {
                "text": "24-hour foot and vehicle patrols",
                "value": "24-hour foot and vehicle patrols"
            },
            {
                "text": "Controlled access to buildings (dormitory, campus, etc.)",
                "value": "Controlled access to buildings (dormitory, campus, etc.)"
            },
            {
                "text": "Late night transport/safe escort service",
                "value": "Late night transport/safe escort service"
            },
            {
                "text": "Lit pathways/sidewalks",
                "value": "Lit pathways/sidewalks"
            },
            {
                "text": "Security cameras used to monitor campus",
                "value": "Security cameras used to monitor campus"
            },
            {
                "text": "Emergency notification system",
                "value": "Emergency notification system"
            },
            {
                "text": "24-hour emergency helpline",
                "value": "24-hour emergency helpline"
            },
            {
                "text": "Require faculty, staff & students to wear picture IDs",
                "value": "Require faculty, staff & students to wear picture IDs"
            },
            {
                "text": "Others",
                "value": "Others",
                "isLogical": true,
                "isRequired": true,
                "questions": [
                    {
                        "section": "academicsAndExtracurriculars",
                        "text": "Others",
                        "isRequired": true,
                        "code": "CAMPUSSECURITY_OTHEROPTION",
                        "placeholder": "",
                        "type": "text",
                        "stream": "engineering",
                    },
                ]
            },

        ],
        "type": "checkbox",
        "stream": "engineering",
        "placeholder": "",
        "isLogical": true,
        "isRequired": true
    },
]

export const placement = [
    {
        "section": "placement",
        "isRequired": true,
        "text": "What is the average job placement rate for recent graduates? (Within 6 months of graduation)",
        "code": "JOBPLACEMENTRATE",
        
        "placeholder": "",
        "type": "dropDown",
        "stream": "engineering",
        "options": [
            {
                "text": "Excellent (Greater than 90%)",
                "value": "Excellent (Greater than 90%)"
            },
            {
                "text": "Very High (Greater than 80%)",
                "value": "Very High (Greater than 80%)"
            },
            {
                "text": "High (Greater than 70%)",
                "value": "High (Greater than 70%)"
            },
            {
                "text": "Medium (Greater than 60%)",
                "value": "Medium (Greater than 60%)"
            },
            {
                "text": "Low (Greater than 50%)",
                "value": "Low (Greater than 50%)"
            },
            {
                "text": "Very Low (Greater than 40%)",
                "value": "Very Low (Greater than 40%)"
            },
            {
                "text": "Less than 40 %",
                "value": "Less than 40 %"
            },
        ],
    },
    {
        "section": "placement",
        "isRequired": true,
        "text": "Does the institution provide employment opportunities to graduating students?",
        "code": "PROVIDE_EMPLOYMENT",
        "placeholder": "",
        "type": "radio",
        "stream": "engineering",
        "options": [
            {
                "text": "Yes",
                "value": "Yes"
            },
            {
                "text": "No",
                "value": "No"
            },
        ],
    },
    {
        "section": "placement",
        "isRequired": true,
        "text": "Does the institution offer job assistance even after the student has graduated?",
        "code": "OFFERJOB_ASSISTANCE",
        "placeholder": "",
        "type": "radio",
        "stream": "engineering",
        "options": [
            {
                "text": "Yes",
                "value": "Yes"
            },
            {
                "text": "No",
                "value": "No"
            },
        ],
    },
    {
        "section": "placement",
        "isRequired": true,
        "text": "Does the institution provide internship opportunities to full-time enrolled students?",
        "code": "INTERNSHIP_OPPROTUNITIES",
        "placeholder": "",
        "type": "radio",
        "stream": "engineering",
        "options": [
            {
                "text": "Yes",
                "value": "Yes"
            },
            {
                "text": "No",
                "value": "No"
            },
        ],
    },
    {
        "section": "placement",
        "text": "Does the institution offer OPT (Optional Practical Training) or CPT (Curricular Practical Training)?",
        "code": "OFFEROPTCPT",
        "placeholder": "",
        "type": "radio",
        "stream": "engineering",
        "isRequired": true,
        "isLogical": true,
        "tips": <p>● Optional Practical Training (OPT) is a period during which undergraduate and graduate students with F-1 status who have completed or have been pursuing their degrees for one academic year are permitted by the United States Citizenship and Immigration Services (USCIS) to work for one year on a student visa towards getting practical training to complement their education.<br />
        ● Curricular Practical Training (CPT) is a program that temporarily allows international students with an F-1 visa to gain practical experience directly related to their major through employment, paid or unpaid internships, or cooperative education.</p>,
        "options": [
            {
                "text": "OPT",
                "value": "OPT"
            },
            {
                "text": "CPT",
                "value": "CPT",
                "isLogical": true,
                "questions": [
                    {
                        "section": "placement",
                        "text": "From when can the student start CPT?",
                        "code": "STUDENTSTART_CPT",
                        "placeholder": "",
                        "isRequired": true,
                        "type": "dropDown",
                        "stream": "engineering",
                        "options": [
                            {
                                "text": "Semester 1",
                                "value": "Semester 1",
                            },
                            {
                                "text": "Semester 2",
                                "value": "Semester 2",
                            },
                            {
                                "text": "Semester 3 or later",
                                "value": "Semester 3 or later",
                            },
                        ],
                    },
                    {
                        "section": "placement",
                        "text": "How many hours can they work on CPT?",
                        "code": "WORKHOURS_CPT",
                        "placeholder": "",
                        "isRequired": true,
                        "type": "dropDown",
                        "stream": "engineering",
                        "options": [
                            {
                                "text": "20",
                                "value": "20",
                            },
                            {
                                "text": "25",
                                "value": "25",
                            },
                            {
                                "text": "30",
                                "value": "30",
                            },
                        ],
                    },
                ]
            },
            {
                "text": "Both",
                "value": "Both",
                "isLogical": true,
                "questions": [
                    {
                        "section": "placement",
                        "text": "From when can the student start CPT?",
                        "code": "STUDENTSTART_CPT",
                        "placeholder": "",
                        "isRequired": true,
                        "type": "dropDown",
                        "stream": "engineering",
                        "options": [
                            {
                                "text": "Semester 1",
                                "value": "Semester 1",
                            },
                            {
                                "text": "Semester 2",
                                "value": "Semester 2",
                            },
                            {
                                "text": "Semester 3 or later",
                                "value": "Semester 3 or later",
                            },
                        ],
                    },
                    {
                        "section": "placement",
                        "text": "How many hours can they work on CPT?",
                        "code": "WORKHOURS_CPT",
                        "placeholder": "",
                        "isRequired": true,
                        "type": "dropDown",
                        "stream": "engineering",
                        "options": [
                            {
                                "text": "20",
                                "value": "20",
                            },
                            {
                                "text": "25",
                                "value": "25",
                            },
                            {
                                "text": "30",
                                "value": "30",
                            },
                        ],
                    },
                ]
            },
            {
                "text": "Not Applicable",
                "value": "Not Applicable"
            },
        ],
    },
    // {
    //     "section": "placement",
    //     "text": "Does the institute have partnerships with certain companies or recruiters for providing training/job opportunities? If yes, which companies have you partnered with? (Type NA if not applicable)",
    //     "code": "INSTITUTEPARTNERSHIP",
    //     "options": [],
    //     "placeholder": "",
    //     "type": "text",
    //     "stream": "engineering",
    //     "isRequired": true
    // },
    
    {
        "section": "placement",
        "text": "Does the institution have partnerships with certain companies or recruiters for providing training/job opportunities?",
        "code": "INSTITUTEPARTNERSHIP",
        "options": [],
        "placeholder": "",
        "type": "radio",
        "stream": "engineering",
        "isRequired": true,
        "isLogical": true,
        "options":[
            {
                "text": "Yes",
                "value": "Yes",
                "isLogical": true,
                "isRequired": true,
                "questions":[
                    {
                        "section": "placement",
                        "text": "Which companies has the institution partnered with?",
                        "code": "INSTITUTION_PARTNERED",
                        "placeholder": "",
                        "type": "text",
                        "isRequired": true,
                        "stream": "engineering",
                    },
                ]
            },
            {
                "text": "No",
                "value": "No"
            }
        ]
    },
    {
        "section": "placement",
        "text": "Mention some of the institution’s notable graduates. ",
        "code": "INSTITUTENOTABLEGRADUATES",
        "options": [],
        "placeholder": "",
        "type": "text",
        "stream": "engineering",
    },
    {
        "section": "placement",
        "text": "Please share the Social Media links for the following: -",
        "code": "SOCIAL_LINK",
        "placeholder": "",
        "type": "",
        "isLogical": true,
        "stream": "engineering",
        "questions": [
            // {
            //     "section": "placement",
            //     "text": "Instagram",
            //     "code": "INSTAGRAM_LINK",
            //     "placeholder": "",
            //     "type": "text",
            //     "stream": "engineering",
            // },
            // {
            //     "section": "placement",
            //     "text": "LinkedIn",
            //     "code": "LINKEDIN_LINK",
            //     "placeholder": "",
            //     "type": "text",
            //     "stream": "engineering",
            // },
            // {
            //     "section": "placement",
            //     "text": "Twitter",
            //     "code": "TWITTER_LINK",
            //     "placeholder": "",
            //     "type": "text",
            //     "stream": "engineering",
            // },
            // {
            //     "section": "placement",
            //     "text": "Facebook",
            //     "code": "FACEBOOK_LINK",
            //     "placeholder": "",
            //     "type": "text",
            //     "stream": "engineering",
            // },
            {
                "section": "placement",
                "text": "Official website",
                "code": "OFFICIALWEB_LINK",
                "placeholder": "",
                "type": "text",
                "stream": "engineering",
            },
        ]
    },
    {
        "section": "placement",
        "text": "Which of the following social media channels are used by the institution for official communication and marketing purposes)?",
        "code": "OFFICIALSOCIALMEDIASELECT",
        "isLogical": true,
        "type": "checkbox",
        "options": [
            {
                "text": "Facebook",
                "value": "Facebook",
                "isLogical": true,
                "isRequired": true,
                "questions":[
                    {
                        "section": "placement",
                        "text": "Facebook",
                        "code": "FACEBOOK_LINK",
                        "placeholder": "Facebook Link",
                        "type": "text",
                        "stream": "engineering",
                    },
                ]
            },
            {
                "text": "Twitter",
                "value": "Twitter",
                "isLogical": true,
                "isRequired": true,
                "questions":[
                    {
                        "section": "placement",
                        "text": "Twitter",
                        "code": "TWITTER_LINK",
                        "placeholder": "Twitter Link",
                        "type": "text",
                        "stream": "engineering",
                    },
                ]
            },
            {
                "text": "YouTube",
                "value": "YouTube",
                "isLogical": true,
                "isRequired": true,
                "questions":[
                    {
                        "section": "placement",
                        "text": "YouTube",
                        "code": "YOUTUBE_LINK",
                        "placeholder": "YouTube Link",
                        "type": "text",
                        "stream": "engineering",
                    },
                ]
            },
            {
                "text": "Instagram",
                "value": "Instagram",
                "isLogical": true,
                "isRequired": true,
                "questions":[
                    {
                        "section": "placement",
                        "text": "Instagram",
                        "code": "INSTAGRAM_LINK",
                        "placeholder": "Instagram Link",
                        "type": "text",
                        "stream": "engineering",
                    },
                ]
            },
            {
                "text": "Pinterest",
                "value": "Pinterest",
                "isLogical": true,
                "isRequired": true,
                "questions":[
                    {
                        "section": "placement",
                        "text": "Pinterest",
                        "code": "PINTEREST_LINK",
                        "placeholder": "Pinterest Link",
                        "type": "text",
                        "stream": "engineering",
                    },
                ]
            },
            {
                "text": "LinkedIn",
                "value": "LinkedIn",
                "isLogical": true,
                "isRequired": true,
                "questions":[
                    {
                        "section": "placement",
                        "text": "LinkedIn",
                        "code": "LINKEDIN_LINK",
                        "placeholder": "LinkedIn Link",
                        "type": "text",
                        "stream": "engineering",
                    },
                ]
            },
            {
                "text": "Reddit",
                "value": "Reddit",
                "isLogical": true,
                "isRequired": true,
                "questions":[
                    {
                        "section": "placement",
                        "text": "Reddit",
                        "code": "REDDIT_LINK",
                        "placeholder": "Reddit Link",
                        "type": "text",
                        "stream": "engineering",
                    },
                ]
            },
            {
                "text": "Snapchat",
                "value": "Snapchat",
                "isLogical": true,
                "isRequired": true,
                "questions":[
                    {
                        "section": "placement",
                        "text": "Snapchat",
                        "code": "SNAPCHAT_LINK",
                        "placeholder": "Snapchat Link",
                        "type": "text",
                        "stream": "engineering",
                    },
                ]
            }
        ],
        "placeholder": "",
        "stream": "engineering"
    },
    {
        "section": "placement",
        "text": "How did you discover HiEd Harmony?",
        "code": "DISCOVERHIED",
        "isLogical": true,
        "options": [
            {
                "text": "Facebook",
                "value": "Facebook"
            },
            {
                "text": "LinkedIn",
                "value": "LinkedIn"
            },
            {
                "text": "Instagram",
                "value": "Instagram"
            },
            {
                "text": "Internet Ad.",
                "value": "Internet Ad."
            },
            {
                "text": "School",
                "value": "School"
            },
            {
                "text": "College",
                "value": "College"
            },
            {
                "text": "Newspaper/Brochure",
                "value": "Newspaper/Brochure"
            },
            {
                "text": "HiEd Student Success Reps",
                "value": "HiEd Student Success Reps",
                "isLogical": true,
                "isRequired": true,
                "questions": [
                    {
                        "text": "Who were you referred by?",
                        "type": "dropDown",
                        "code": "WHOWEREREFERRED",
                        "isRequired": true,
                        "isLogical": true,
                        "options":[
                            {
                                "text": "Preeti Tanwar",
                                "value": "Preeti Tanwar"
                            },
                            {
                                "text": "Sukanya Bhowmik",
                                "value": "Sukanya Bhowmik"
                            },
                            {
                                "text": "Kiran Yerawar",
                                "value": "Kiran Yerawar"
                            },
                            {
                                "text": "Salman Shaikh",
                                "value": "Salman Shaikh"
                            },
                            {
                                "text": "Shwan Muhammed",
                                "value": "Shwan Muhammed"
                            },
                            {
                                "text": "Destiny Ihenacho",
                                "value": "Destiny Ihenacho"
                            },
                            {
                                "text": "Kiza Mauridi",
                                "value": "Kiza Mauridi"
                            },
                            {
                                "text": "Archit Singh",
                                "value": "Archit Singh"
                            },
                            {
                                "text": "Vinod Badoni",
                                "value": "Vinod Badoni"
                            },
                            {
                                "text": "Ajay Sankhla",
                                "value": "Ajay Sankhla"
                            },
                            {
                                "text": "Sreedhar Kodali",
                                "value": "Sreedhar Kodali"
                            },
                            {
                                "text": "Fermata",
                                "value": "Fermata"
                            },
                        ]
                    }
                ]
            },
            {
                "text": "Recommended by friend/colleague",
                "value": "Recommended by friend/colleague"
            },
            {
                "text": "Others (please specify)",
                "value": "Others (please specify)",
                "isLogical": true,
                "questions": [
                    {
                        "text": "Other",
                        "type": "text",
                        "code": "OTHERDISCOVERHIED",
                        "isRequired": true
                    }
                ]
            },
        ],
        "placeholder": "",
        "type": "dropDown",
        "stream": "engineering"
    },
    {
        "section": "placement",
        "isRequired": true,
        "text": "HiEd Harmony portal connects you to 1M+ students globally. Would you like to run your institution’s advertisements our platform?",
        "code": "CONNECTSTUDENTGLOBALLY",
        "placeholder": "",
        "type": "radio",
        "stream": "engineering",
        "options": [
            {
                "text": "Yes",
                "value": "Yes"
            },
            {
                "text": "No",
                "value": "No"
            },
            {
                "text": "Would like to discuss further",
                "value": "Would like to discuss further"
            },
        ],
    },
]



