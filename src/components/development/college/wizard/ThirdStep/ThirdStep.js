import React, { useState, useEffect } from 'react';
import { Col, Row, Button } from 'react-bootstrap';
import 'rsuite/dist/styles/rsuite-default.css';
import FormInput from '../FormInput'
import { admissions } from '../WizardQuestion'
import Validator from "validator";
import { getLocalStorage } from '../../../../../Lib/Utils';
// import { getLocalStorage } from '../../../../../Lib/Utils';


const ThirdStep = (props) => {

  const [state, setState] = useState({});
  const [errors, setErrors] = useState({});
  const [validMsg, setValidMsg] = useState('')
  const [urlMsg, setUrlMsg] = useState('');


  useEffect(() => {
    setErrors({})
  }, [props])



  const onchangeInput = (event, code, type) => {
    if (code) {
      if (type && type == "slider") {
        setState({ ...state, [code]: event });
        errors[[code]] = false
      } else {
        if (event) {
          setState({ ...state, [code]: event.getTime() });
        } else {
          setState({ ...state, [code]: 0 });
        }
      }
    } else {
      const { target: { name, value } } = event;
      errors[[name]] = false


      if (name == "ASSO_IELTS_LISTENING" || name == "ASSO_IELTS_READING" || name == "ASSO_IELTS_WRITING" || name == "ASSO_IELTS_SPEAKING" || name == "ASSO_IELTS_BANDSCORE" ||

        name == "BACHE_IELTS_LISTENING" || name == "BACHE_IELTS_READING" || name == "BACHE_IELTS_WRITING" || name == "BACHE_IELTS_SPEAKING" || name == "BACHE_IELTS_BANDSCORE" ||

        name == "MASTER_IELTS_LISTENING" || name == "MASTER_IELTS_READING" || name == "MASTER_IELTS_WRITING" || name == "MASTER_IELTS_SPEAKING" || name == "MASTER_IELTS_BANDSCORE" ||

        name == "MASTERS_GENERAL_IELTS_LISTENING" || name == "MASTERS_GENERAL_IELTS_READING" || name == "MASTERS_GENERAL_IELTS_WRITING" || name == "MASTERS_GENERAL_IELTS_SPEAKING" || name == "MASTERS_GENERAL_IELTS_BANDSCORE" ||

        name == "MASTER_MGMT_IELTS_LISTENING" || name == "MASTER_MGMT_IELTS_READING" || name == "MASTER_MGMT_IELTS_WRITING" || name == "MASTER_MGMT_IELTS_SPEAKING" || name == "MASTER_MGMT_IELTS_BANDSCORE" ||

        name == "POST_GRAD_IELTS_LISTENING" || name == "POST_GRAD_IELTS_READING" || name == "POST_GRAD_IELTS_WRITING" || name == "POST_GRAD_IELTS_SPEAKING" || name == "POST_GRAD_IELTS_BANDSCORE" ||

        name == "DOCTORATE_IELTS_LISTENING" || name == "DOCTORATE_IELTS_READING" || name == "DOCTORATE_IELTS_WRITING" || name == "DOCTORATE_IELTS_SPEAKING" || name == "DOCTORATE_IELTS_BANDSCORE" ||

        name == "POST_DOCTORAL_IELTS_LISTENING" || name == "POST_DOCTORAL_IELTS_READING" || name == "POST_DOCTORAL_IELTS_WRITING" || name == "POST_DOCTORAL_IELTS_SPEAKING" || name == "POST_DOCTORAL_IELTS_BANDSCORE") {
        var pointing = value.split('.', 2)

        if (pointing[1] && pointing[1].length > 1) {
          return
        } else {
          setState({ ...state, [name]: value });
        }
      } else if (name == "MASTER_GRE_ANALYTICAL" || name == "MASTER_MGMT_GRE_ANALYTICAL" || name == "POST_GRAD_GRE_ANALYTICAL") {
        var pointing = value.split('.', 2)

        if (pointing && pointing.length > 1) {
          if (pointing[1] == '') {
            setState({ ...state, [name]: value });
          } else {
            setState({ ...state, [name]: (Math.round(value / 0.5) * 0.5) });
          }
        } else {
          setState({ ...state, [name]: value });
        }
      } else if (name == "MASTER_GMAT_Total" || name == "MASTER_MGMT_GMAT_Total" || name == "POST_GRAD_GMAT_Total" || name == "ASSO_SAT_EBRW" || name == "ASSO_SAT_MATH" || name == "BACHE_SAT_EBRW" || name == "BACHE_SAT_MATH") {
        if (value.length > 2) {
          setState({ ...state, [name]: (Math.round(value / 10) * 10) });
        } else {
          setState({ ...state, [name]: value });
        }
      } else {

        //Associates TOEFL
        // var reading = name == "ASSO_TOEFL_READING" ? parseFloat(value) : parseFloat(state.ASSO_TOEFL_READING)
        // var writing = name == "ASSO_TOEFL_LISTENING" ? parseFloat(value) : parseFloat(state.ASSO_TOEFL_LISTENING)
        // var listing = name == "ASSO_TOEFL_SPEAKING" ? parseFloat(value) : parseFloat(state.ASSO_TOEFL_SPEAKING)
        // var speaking = name == "ASSO_TOEFL_WRITING" ? parseFloat(value) : parseFloat(state.ASSO_TOEFL_WRITING)
        // var ASSO_TOEFL_TOTAL = reading + writing + listing + speaking


        //Associates SAT
        // var Asso_ebrw = name == "ASSO_SAT_EBRW" ? parseFloat(value) : parseFloat(state.ASSO_SAT_EBRW)
        // var Asso_math = name == "ASSO_SAT_MATH" ? parseFloat(value) : parseFloat(state.ASSO_SAT_MATH)
        // var ASSO_SAT_TOTAL = Asso_ebrw + Asso_math

        //Bachelors TOEFL
        // var Bach_reading = name == "BACHE_TOEFL_READING" ? parseFloat(value) : parseFloat(state.BACHE_TOEFL_READING)
        // var Bach_writing = name == "BACHE_TOEFL_LISTENING" ? parseFloat(value) : parseFloat(state.BACHE_TOEFL_LISTENING)
        // var Bach_listing = name == "BACHE_TOEFL_SPEAKING" ? parseFloat(value) : parseFloat(state.BACHE_TOEFL_SPEAKING)
        // var Bach_speaking = name == "BACHE_TOEFL_WRITING" ? parseFloat(value) : parseFloat(state.BACHE_TOEFL_WRITING)
        // var BACHE_TOEFL_TOTAL = Bach_reading + Bach_writing + Bach_listing + Bach_speaking

        //Bachelors SAT
        // var Bach_ebrw = name == "BACHE_SAT_EBRW" ? parseFloat(value) : parseFloat(state.BACHE_SAT_EBRW)
        // var Bach_math = name == "BACHE_SAT_MATH" ? parseFloat(value) : parseFloat(state.BACHE_SAT_MATH)
        // var BACHE_SAT_TOTAL = Bach_ebrw + Bach_math

        //Masters GRE
        // var Master_quant = name == "MASTER_GRE_QUANTITATIVE" ? parseFloat(value) : parseFloat(state.MASTER_GRE_QUANTITATIVE)
        // var Master_verbal = name == "MASTER_GRE_VERBAL" ? parseFloat(value) : parseFloat(state.MASTER_GRE_VERBAL)
        // var Master_analy = name == "MASTER_GRE_ANALYTICAL" ? parseFloat(value) : parseFloat(state.MASTER_GRE_ANALYTICAL)
        // var MASTER_GRE_TOTAL = Master_quant + Master_verbal + Master_analy

        //Masters TOEFL
        // var Master_reading = name == "MASTER_TOEFL_READING" ? parseFloat(value) : parseFloat(state.MASTER_TOEFL_READING)
        // var Master_writing = name == "MASTER_TOEFL_LISTENING" ? parseFloat(value) : parseFloat(state.MASTER_TOEFL_LISTENING)
        // var Master_listing = name == "MASTER_TOEFL_SPEAKING" ? parseFloat(value) : parseFloat(state.MASTER_TOEFL_SPEAKING)
        // var Master_speaking = name == "MASTER_TOEFL_WRITING" ? parseFloat(value) : parseFloat(state.MASTER_TOEFL_WRITING)
        // var MASTER_TOEFL_TOTAL = Master_reading + Master_writing + Master_listing + Master_speaking

        //Masters GMAT
        // var Master_G_quant = name == "MASTER_GMAT_QUANTITATIVE" ? parseFloat(value) : parseFloat(state.MASTER_GMAT_QUANTITATIVE)
        // var Master_G_verbal = name == "MASTER_GMAT_VERBAL" ? parseFloat(value) : parseFloat(state.MASTER_GMAT_VERBAL)
        // var MASTER_GMAT_Total = Master_G_quant + Master_G_verbal


        //Masters Management GRE
        // var Master_quant = name == "MASTER_MGMT_GRE_QUANTITATIVE" ? parseFloat(value) : parseFloat(state.MASTER_MGMT_GRE_QUANTITATIVE)
        // var Master_verbal = name == "MASTER_MGMT_GRE_VERBAL" ? parseFloat(value) : parseFloat(state.MASTER_MGMT_GRE_VERBAL)
        // var Master_analy = name == "MASTER_MGMT_GRE_ANALYTICAL" ? parseFloat(value) : parseFloat(state.MASTER_MGMT_GRE_ANALYTICAL)
        // var MASTER_MGMT_GRE_TOTAL = Master_quant + Master_verbal + Master_analy

        //Masters Management TOEFL
        // var Master_reading = name == "MASTER_MGMT_TOEFL_READING" ? parseFloat(value) : parseFloat(state.MASTER_MGMT_TOEFL_READING)
        // var Master_writing = name == "MASTER_MGMT_TOEFL_LISTENING" ? parseFloat(value) : parseFloat(state.MASTER_MGMT_TOEFL_LISTENING)
        // var Master_listing = name == "MASTER_MGMT_TOEFL_SPEAKING" ? parseFloat(value) : parseFloat(state.MASTER_MGMT_TOEFL_SPEAKING)
        // var Master_speaking = name == "MASTER_MGMT_TOEFL_WRITING" ? parseFloat(value) : parseFloat(state.MASTER_MGMT_TOEFL_WRITING)
        // var MASTER_MGMT_TOEFL_TOTAL = Master_reading + Master_writing + Master_listing + Master_speaking

        //Masters Management GMAT
        // var Master_G_quant = name == "MASTER_MGMT_GMAT_QUANTITATIVE" ? parseFloat(value) : parseFloat(state.MASTER_MGMT_GMAT_QUANTITATIVE)
        // var Master_G_verbal = name == "MASTER_MGMT_GMAT_VERBAL" ? parseFloat(value) : parseFloat(state.MASTER_MGMT_GMAT_VERBAL)
        // var MASTER_MGMT_GMAT_Total = Master_G_quant + Master_G_verbal

        //Doctorate TOEFL
        // var Doctorate_reading = name == "DOCTORATE_TOEFL_READING" ? parseFloat(value) : parseFloat(state.DOCTORATE_TOEFL_READING)
        // var Doctorate_writing = name == "DOCTORATE_TOEFL_LISTENING" ? parseFloat(value) : parseFloat(state.DOCTORATE_TOEFL_LISTENING)
        // var Doctorate_listing = name == "DOCTORATE_TOEFL_SPEAKING" ? parseFloat(value) : parseFloat(state.DOCTORATE_TOEFL_SPEAKING)
        // var Doctorate_speaking = name == "DOCTORATE_TOEFL_WRITING" ? parseFloat(value) : parseFloat(state.DOCTORATE_TOEFL_WRITING)
        // var DOCTORATE_TOEFL_TOTAL = Doctorate_reading + Doctorate_writing + Doctorate_listing + Doctorate_speaking

        // //Doctorate TOEFL
        // var Doctorate_reading = name == "POST_DOCTORAL_TOEFL_READING" ? parseFloat(value) : parseFloat(state.POST_DOCTORAL_TOEFL_READING)
        // var Doctorate_writing = name == "POST_DOCTORAL_TOEFL_LISTENING" ? parseFloat(value) : parseFloat(state.POST_DOCTORAL_TOEFL_LISTENING)
        // var Doctorate_listing = name == "POST_DOCTORAL_TOEFL_SPEAKING" ? parseFloat(value) : parseFloat(state.POST_DOCTORAL_TOEFL_SPEAKING)
        // var Doctorate_speaking = name == "POST_DOCTORAL_TOEFL_WRITING" ? parseFloat(value) : parseFloat(state.POST_DOCTORAL_TOEFL_WRITING)
        // var POST_DOCTORAL_TOEFL_TOTAL = Doctorate_reading + Doctorate_writing + Doctorate_listing + Doctorate_speaking

        // //Masters (General)
        // var Doctorate_reading = name == "MASTERS_GENERAL_TOEFL_READING" ? parseFloat(value) : parseFloat(state.MASTERS_GENERAL_TOEFL_READING)
        // var Doctorate_writing = name == "MASTERS_GENERAL_TOEFL_LISTENING" ? parseFloat(value) : parseFloat(state.MASTERS_GENERAL_TOEFL_LISTENING)
        // var Doctorate_listing = name == "MASTERS_GENERAL_TOEFL_SPEAKING" ? parseFloat(value) : parseFloat(state.MASTERS_GENERAL_TOEFL_SPEAKING)
        // var Doctorate_speaking = name =="MASTERS_GENERAL_TOEFL_WRITING" ? parseFloat(value) : parseFloat(state.MASTERS_GENERAL_TOEFL_WRITING)
        // var MASTERS_GENERAL_TOEFL_TOTAL = Doctorate_reading + Doctorate_writing + Doctorate_listing + Doctorate_speaking

        setState({ ...state, [name]: value });
      }







    }
  };

  const onKeyDownInput = event => {
    if (event && event.target && event.target.name) {
      const { target: { name, value } } = event;

      if (event.keyCode == 13) {
        setState({ ...state, [name]: '' });

        var arr = name.split('_');
        let Newname = arr[0];

        if (state[Newname]) {
          let ArryVale = [...state[Newname]];
          var indexB = ArryVale.indexOf(value);
          if (indexB > -1) {
            ArryVale.splice(indexB, 1);
            setState({ ...state, [name]: '', [Newname]: [...ArryVale] });
          } else {
            ArryVale.push(value);
            setState({ ...state, [name]: '', [Newname]: [...ArryVale] });
          }
        } else {
          setState({ ...state, [name]: '', [Newname]: [value] });
        }
      }
    } else {
      //setState({ ...state, DOB: event });
    }
  };

  //select check box
  const oncheckBox = (name, value) => {
    if (state[name]) {
      let ArryVale = state[name];
      var indexB = ArryVale.indexOf(value);
      if (indexB > -1) {
        ArryVale.splice(indexB, 1);
        if (ArryVale && ArryVale.length <= 0) {
          delete state[name]
          setState({ ...state });
        } else {
          setState({ ...state, [name]: [...ArryVale] });
        }
      } else {
        ArryVale.push(value);
        setState({ ...state, [name]: [...ArryVale] });
      }
      //////
      if (name == "DEGREECERTIFICATE_INST" && name !== "DEGREECERTIFICATE" && state["DEGREECERTIFICATE"] && state["DEGREECERTIFICATE"].length) {
        let ArryVale = state["DEGREECERTIFICATE"];
        var indexB = ArryVale.indexOf(value);
        if (indexB > -1) {
          ArryVale.splice(indexB, 1);
          if (ArryVale && ArryVale.length <= 0) {
            delete state["DEGREECERTIFICATE"]
            setState({ ...state });
          } else {
            setState({ ...state, DEGREECERTIFICATE: [...ArryVale] });
          }
        }
      }
    } else {
      setState({ ...state, [name]: [value] });
    }

    if (name == "DEGREECERTIFICATE_ASSOCIATE" && state && state["DEGREECERTIFICATE_ASSOCIATE"] && state["DEGREECERTIFICATE_ASSOCIATE"].indexOf("Not Applicable") > -1) {
      setState({ ...state, DEGREECERTIFICATE_ASSOCIATE: ["Not Applicable"] });
    }

    if (name == "DEGREECERTIFICATE_BACHELORS" && state && state["DEGREECERTIFICATE_BACHELORS"] && state["DEGREECERTIFICATE_BACHELORS"].indexOf("Not Applicable") > -1) {
      setState({ ...state, DEGREECERTIFICATE_BACHELORS: ["Not Applicable"] });
    }

    if (name == "DEGREECERTIFICATE_MASTERS" && state && state["DEGREECERTIFICATE_MASTERS"] && state["DEGREECERTIFICATE_MASTERS"].indexOf("Not Applicable") > -1) {
      setState({ ...state, DEGREECERTIFICATE_MASTERS: ["Not Applicable"] });
    }

    if (name == "DEGREECERTIFICATE_DOCTORATE" && state && state["DEGREECERTIFICATE_DOCTORATE"] && state["DEGREECERTIFICATE_DOCTORATE"].indexOf("Not Applicable") > -1) {
      setState({ ...state, DEGREECERTIFICATE_DOCTORATE: ["Not Applicable"] });
    }


    admissions && admissions.map((item_1, index_1) => {
      if ((index_1 == 0 || index_1 == 1 || index_1 == 2 || index_1 == 3) && item_1.options) {
        item_1.options.map((item_2) => {
          if (state && state[item_1.code] && state[item_1.code].indexOf(item_2.value) > -1) {
            item_2.questions && item_2.questions.map((item_3) => {
              if (state && state[item_2.code] && state[item_2.code].indexOf(item_3.value) > -1) {
              } else {
                item_3.questions && item_3.questions.map((item_4) => {
                  if (state && state[item_4.code]) {
                    delete state[item_4.code]
                    setState({ ...state });
                  }
                  if (errors && errors[item_4.code]) {
                    setErrors({ [errors[item_4.code]]: false })
                  }
                })
              }
            })
          } else {
            if (state && state[item_2.code]) {
              delete state[item_2.code]
              setState({ ...state });
            }

            if (index_1 == 0 || index_1 == 1) {
              item_2.questions && item_2.questions.map((item_5) => {
                item_5.options && item_5.options.map((item_6) => {
                  if (state && state[item_6.code]) {
                    delete state[item_6.code]
                    setState({ ...state });
                  }
                })
              })
            }
          }
        })
      }
    })
  }




  useEffect(() => {
    if (props.data && Object.keys(props.data).length != 0) {
      setErrors({})
      setState({
        ...state,
        ...props.data
      });
    }
  }, [props.data]);



  const stepSubmit = () => {
    let errors = {};
    let valid = true;
    setUrlMsg(false)

    for (var m = 0; m < admissions.length; m++) {

      if (admissions[m].isRequired) {
        let codevalue = state[admissions[m].code];
        if (m == 4 || m == 5) {
          if (state["DEGREECERTIFICATE"] && state["DEGREECERTIFICATE"].length && (state["DEGREECERTIFICATE"].indexOf("Associates") > -1 || state["DEGREECERTIFICATE"].indexOf("Bachelors") > -1)) {
            if (admissions[m].code !== "STUDENTADMISSIONRATE" && (!codevalue || codevalue == 'undefined' || codevalue == '')) {
              valid = false;
              errors[admissions[m].code] = true;
            }
          }
        } else if (m == 6 || m == 7) {
          if (state["DEGREECERTIFICATE"] && state["DEGREECERTIFICATE"].length && (state["DEGREECERTIFICATE"].indexOf("Masters (Science)") > -1 || state["DEGREECERTIFICATE"].indexOf("Masters (General)") > -1 || state["DEGREECERTIFICATE"].indexOf("Masters (Management)") > -1 || state["DEGREECERTIFICATE"].indexOf("Post Graduate Certificates") > -1 || state["DEGREECERTIFICATE"].indexOf("Doctorate / PhD / EdD") > -1 || state["DEGREECERTIFICATE"].indexOf("Post-Doctoral") > -1)) {
            if (admissions[m].code !== "STUDENTADMISSIONRATE" && (!codevalue || codevalue == 'undefined' || codevalue == '')) {
              valid = false;
              errors[admissions[m].code] = true;
            }
          }
        } else {
          if (admissions[m].code !== "STUDENTADMISSIONRATE" && (!codevalue || codevalue == 'undefined' || codevalue == '')) {
            valid = false;
            errors[admissions[m].code] = true;
          }
        }


        if ((m == 0 || m == 1 || m == 2) && admissions[m].options) {
          var checkOption = admissions[m].options
          var checkStatus = false
          for (var x = 0; x < checkOption.length; x++) {
            if (state && state[admissions[m].code] && state[admissions[m].code].indexOf(checkOption[x].value) > -1) {
              checkStatus = true
            }
          }
          if ((m == 0 || m == 1 || m == 2 || m == 3) && checkStatus == false && (!codevalueCheck || codevalueCheck == 'undefined' || codevalueCheck == '')) {
            valid = false;
            errors[admissions[m].code] = true;
          }


        }


        // Check child validation
        if (admissions[m].isLogical && admissions[m].options) {
          let Newarry = admissions[m].options;

          for (var n = 0; n < Newarry.length; n++) {
            if (Newarry[n].isLogical && Newarry[n].questions && state[admissions[m].code] && state[admissions[m].code].indexOf(Newarry[n].value) > -1) {

              let newdata = Newarry[n].questions;
              var status = false
              var codevalueCheck
              for (var i = 0; i < newdata.length; i++) {
                if (m == 3 && newdata[i].isRequired) {
                  codevalueCheck = state[newdata[i].value];
                  if (state && state[Newarry[n].code] && state[Newarry[n].code].indexOf(newdata[i].value) > -1) {
                    status = true
                  }
                }

                // if(newdata[i].code == "COSTOFATTENDINGLINK"){
                //   let linkValue = state[newdata[i].code];
                //   console.log('3333333--',linkValue)
                //   if (!linkValue || linkValue == 'undefined' || linkValue == '') {
                //     console.log('3333333',newdata[i])
                //     valid = false;
                //     errors[newdata[i].code] = true;
                //   }
                // }




                if (newdata[i].isLogical && newdata[i].options) {
                  let optiondata = newdata[i].options;
                  for (var j = 0; j < optiondata.length; j++) {
                    if (optiondata[j].isRequired) {
                      let codevalueNew = state[optiondata[j].code];
                      if (!codevalueNew || codevalueNew == 'undefined' || codevalueNew == '') {
                        valid = false;
                        errors[optiondata[j].code] = true;
                      }
                    }
                  }
                }

                if (state && state[admissions[m].code] && state[admissions[m].code].indexOf(Newarry[n].value) > -1 && state[Newarry[n].code] && state[Newarry[n].code].indexOf(newdata[i].value) > -1) {
                  if (newdata[i].isLogical && newdata[i].questions) {
                    let queData = newdata[i].questions;
                    for (var j = 0; j < queData.length; j++) {

                      if (state[queData[j].code]) {
                        if (queData[j].minValue && queData[j].maxValue && state[queData[j].code] && state[queData[j].code] < queData[j].minValue || state[queData[j].code] > queData[j].maxValue) {
                          // let codeValid = state[s_item.code];
                          // if (codeValid) {
                          //   valid = false;
                          //   errors[s_item.code] = true;
                          //   setValidMsg("Valid number required")
                          // }
                          let codevalueNew = state[queData[j].code];
                          if (codevalueNew) {
                            valid = false;
                            errors[queData[j].code] = true;
                            setValidMsg("Valid number required")
                          }
                        }
                      } else if (queData[j].isRequired) {
                        let codevalueNew = state[queData[j].code];
                        if (!codevalueNew || codevalueNew == 'undefined' || codevalueNew == '') {
                          valid = false;
                          errors[queData[j].code] = true;
                        }
                      }



                    }
                  }
                }
              }
              if (m == 3 && status == false && (!codevalueCheck || codevalueCheck == 'undefined' || codevalueCheck == '')) {
                if (Newarry[n].code == "VOCATIONAL_TRAINING") {
                  let linkValue_vack = state["COSTOFATTENDINGLINK_VOCATIONAL"]
                  if (!linkValue_vack || linkValue_vack == 'undefined' || linkValue_vack == '') {
                    valid = false;
                    errors[Newarry[n].code] = true;
                  }
                } else if (Newarry[n].code == "SUMMER_INTERNSHIP") {
                  let linkValue_summ = state["COSTOFATTENDINGLINK_SUMMER"]
                  if (!linkValue_summ || linkValue_summ == 'undefined' || linkValue_summ == '') {
                    valid = false;
                    errors[Newarry[n].code] = true;
                  }
                }
                else if (Newarry[n].code == "BRIDGE_FOR_BACHELORS") {
                  let linkValue_summ = state["COSTOFATTENDINGLINK_BRIDGEFORBACHELORS"]
                  if (!linkValue_summ || linkValue_summ == 'undefined' || linkValue_summ == '') {
                    valid = false;
                    errors[Newarry[n].code] = true;
                  }
                }
                else if (Newarry[n].code == "BRIDGE_FOR_MASTERS") {
                  let linkValue_summ = state["COSTOFATTENDINGLINK_BRIDGEFORMASTERS"]
                  if (!linkValue_summ || linkValue_summ == 'undefined' || linkValue_summ == '') {
                    valid = false;
                    errors[Newarry[n].code] = true;
                  }
                }
                else {
                  valid = false;
                  errors[Newarry[n].code] = true;
                }
              }

              var regex = /(http|https):(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
              if (m == 3 && status == false && state && state["COSTOFATTENDINGLINK_VOCATIONAL"]) {
                if (Validator.isURL(state["COSTOFATTENDINGLINK_VOCATIONAL"])) {
                  if (state["COSTOFATTENDINGLINK_VOCATIONAL"].match(regex)) {
                  } else {
                    valid = false;
                    errors["VOCATIONAL_TRAINING"] = true;
                    setUrlMsg('Valid URL is required (ex.- https://hiedharmony.com).')
                  }
                } else {
                  valid = false;
                  errors["VOCATIONAL_TRAINING"] = true;
                  setUrlMsg('Valid URL is required (ex.- https://hiedharmony.com).')
                }
              }
              if (m == 3 && status == false && state && state["COSTOFATTENDINGLINK_SUMMER"]) {
                if (Validator.isURL(state["COSTOFATTENDINGLINK_SUMMER"])) {
                  if (state["COSTOFATTENDINGLINK_SUMMER"].match(regex)) {
                  } else {
                    valid = false;
                    errors["SUMMER_INTERNSHIP"] = true;
                    setUrlMsg('Valid URL is required (ex.- https://hiedharmony.com).')
                  }
                } else {
                  valid = false;
                  errors["SUMMER_INTERNSHIP"] = true;
                  setUrlMsg('Valid URL is required (ex.- https://hiedharmony.com).')
                }
              }
              if (m == 3 && status == false && state && state["COSTOFATTENDINGLINK_BRIDGEFORBACHELORS"]) {
                if (Validator.isURL(state["COSTOFATTENDINGLINK_BRIDGEFORBACHELORS"])) {
                  if (state["COSTOFATTENDINGLINK_BRIDGEFORBACHELORS"].match(regex)) {
                  } else {
                    valid = false;
                    errors["BRIDGE_FOR_BACHELORS"] = true;
                    setUrlMsg('Valid URL is required (ex.- https://hiedharmony.com).')
                  }
                } else {
                  valid = false;
                  errors["BRIDGE_FOR_BACHELORS"] = true;
                  setUrlMsg('Valid URL is required (ex.- https://hiedharmony.com).')
                }
              }
              if (m == 3 && status == false && state && state["COSTOFATTENDINGLINK_BRIDGEFORMASTERS"]) {
                if (Validator.isURL(state["COSTOFATTENDINGLINK_BRIDGEFORMASTERS"])) {
                  if (state["COSTOFATTENDINGLINK_BRIDGEFORMASTERS"].match(regex)) {
                  } else {
                    valid = false;
                    errors["BRIDGE_FOR_MASTERS"] = true;
                    setUrlMsg('Valid URL is required (ex.- https://hiedharmony.com).')
                  }
                } else {
                  valid = false;
                  errors["BRIDGE_FOR_MASTERS"] = true;
                  setUrlMsg('Valid URL is required (ex.- https://hiedharmony.com).')
                }
              }

            }
          }
        }
      }
    }


    if (valid) {
      props.stepSubmitThird(state, 3);
      setErrors({});
    }
    else {
      setErrors({ ...errors });
    }

  }



  return (


    <div className="rightBox pt-5 pb-5 pl-4">

      <Col lg={10} md={12}>
        <div className="rightLayout">
          <div className="d-flex flex-wrap justify-content-between mb-1">
            <div className="stepTitle fw500 fs30 col4 position-relative mb-4 pb-1">Admissions</div>
            {
              getLocalStorage('user') && getLocalStorage('user').isProfileCompleted &&
              <div className="col2 fw500 fs18 pointer" onClick={() => props.stepSelect('fourth')}>Skip</div>
            }
          </div>
          <Row className="stepForm7">
            {
              admissions && admissions.map((data, index) =>
                data.section == 'admissions' ? (
                  <div key={index + 'i'}>
                    <Col md={12} key={index + 'i'}>
                      {
                        <>
                          {

                            index < 4 ?
                              index == 3 ?
                                state["DEGREECERTIFICATE_INST"] && state["DEGREECERTIFICATE_INST"].length &&
                                <div className="col4 fw600  fs18 mb-2">
                                  <span className="numericType1">3.1</span>
                                  {data.text}
                                  {data.isRequired ? <sup className="starQ">*</sup> : ''}
                                </div>
                                :
                                <div className="col4 fw600  fs18 mb-2">
                                  <span className="numericType1">{index + 1}.</span>
                                  {data.text}
                                  {data.isRequired ? <sup className="starQ">*</sup> : ''}
                                </div>
                              :
                              !state["DEGREECERTIFICATE"] || state["DEGREECERTIFICATE"].length <= 0
                                ?
                                <div className="col4 fw600  fs18 mb-2">
                                  {
                                    (index == 8 || index == 9 || index == 10) &&
                                    <>
                                      <span className="numericType1">{state["DEGREECERTIFICATE_INST"] && state["DEGREECERTIFICATE_INST"].length ? index - 4 : index - 4}.</span>
                                      {data.text}
                                      {data.isRequired ? <sup className="starQ">*</sup> : ''}
                                    </>
                                  }
                                </div>
                                :
                                (state["DEGREECERTIFICATE"] &&
                                  (state["DEGREECERTIFICATE"].indexOf("Associates") > -1 ||
                                    state["DEGREECERTIFICATE"].indexOf("Bachelors") > -1)) ?
                                  <>
                                    {
                                      (state["DEGREECERTIFICATE"].indexOf("Masters (Science)") > -1 ||
                                        state["DEGREECERTIFICATE"].indexOf("Masters (General)") > -1 ||
                                        state["DEGREECERTIFICATE"].indexOf("Masters (Management)") > -1 ||
                                        state["DEGREECERTIFICATE"].indexOf("Post Graduate Certificates") > -1 ||
                                        state["DEGREECERTIFICATE"].indexOf("Doctorate / PhD / EdD") > -1 ||
                                        state["DEGREECERTIFICATE"].indexOf("Post-Doctoral") > -1) ?
                                        <div className="col4 fw600  fs18 mb-2">
                                          <span className="numericType1">{index}.</span>
                                          {data.text}
                                          {data.isRequired ? <sup className="starQ">*</sup> : ''}
                                        </div>
                                        :
                                        <>
                                          {
                                            (index == 4 || index == 5 || index == 8 || index == 9 || index == 10) &&
                                            <div className="col4 fw600  fs18 mb-2">
                                              <span className="numericType1">{(index == 4 || index == 5) ? index : (index == 8 || index == 9 || index == 10) ? index - 2 : ''}.</span>
                                              {data.text}
                                              {data.isRequired ? <sup className="starQ">*</sup> : ''}
                                            </div>

                                          }
                                        </>
                                    }
                                  </>
                                  :
                                  (state["DEGREECERTIFICATE"].indexOf("Masters (Science)") > -1 ||
                                    state["DEGREECERTIFICATE"].indexOf("Masters (General)") > -1 ||
                                    state["DEGREECERTIFICATE"].indexOf("Masters (Management)") > -1 ||
                                    state["DEGREECERTIFICATE"].indexOf("Post Graduate Certificates") > -1 ||
                                    state["DEGREECERTIFICATE"].indexOf("Doctorate / PhD / EdD") > -1 ||
                                    state["DEGREECERTIFICATE"].indexOf("Post-Doctoral") > -1) ?
                                    <>
                                      {
                                        (index == 6 || index == 7 || index == 8 || index == 9 || index == 10) &&
                                        <div className="col4 fw600  fs18 mb-2">
                                          <span className="numericType1">{index - 2}.</span>
                                          {data.text}
                                          {data.isRequired ? <sup className="starQ">*</sup> : ''}
                                        </div>

                                      }
                                    </>
                                    :
                                    <>
                                      {
                                        (index == 8 || index == 9 || index == 10) &&
                                        <div className="col4 fw600  fs18 mb-2">
                                          <span className="numericType1">{index - 4}.</span>
                                          {data.text}
                                          {data.isRequired ? <sup className="starQ">*</sup> : ''}
                                        </div>

                                      }
                                    </>

                          }
                        </>
                      }

                    </Col>
                    <Col key={index + 'j'} lg={'12'} md={'12'}>
                      {/* Question 4 and 5 displayed only when the institution user selects in Q3.1 “Associates” OR “Bachelors”  */}

                      {
                        (index == 4 || index == 5) &&
                          state["DEGREECERTIFICATE"] &&
                          (state["DEGREECERTIFICATE"].indexOf("Associates") > -1 ||
                            state["DEGREECERTIFICATE"].indexOf("Bachelors") > -1)
                          ?
                          <FormInput
                            inputData={data}
                            listOptions={data.options}
                            onchangeInput={onchangeInput}
                            onKeyDownInput={onKeyDownInput}
                            oncheckBox={oncheckBox}
                            setData={state}
                            error={errors[data.code] ? true : false}
                            errorNew={errors}
                            errorNested={errors}
                            errorMessage={'This field is required.'}
                            OuterIndex={index}
                          /> : ''

                      }

                      {/* Question 4 and 5 displayed only when the institution user selects in Q3.1 either “Masters (General)” OR “Masters (Science)” OR “Masters (Management)” OR “Post Graduate Certificates” OR “Doctorate / PhD / EdD” OR “Post-Doctoral”  */}
                      {
                        (index == 6 || index == 7) &&
                          state["DEGREECERTIFICATE"] &&
                          (state["DEGREECERTIFICATE"].indexOf("Masters (Science)") > -1 ||
                            state["DEGREECERTIFICATE"].indexOf("Masters (General)") > -1 ||
                            state["DEGREECERTIFICATE"].indexOf("Masters (Management)") > -1 ||
                            state["DEGREECERTIFICATE"].indexOf("Post Graduate Certificates") > -1 ||
                            state["DEGREECERTIFICATE"].indexOf("Doctorate / PhD / EdD") > -1 ||
                            state["DEGREECERTIFICATE"].indexOf("Post-Doctoral") > -1)
                          ?
                          <FormInput
                            inputData={data}
                            listOptions={data.options}
                            onchangeInput={onchangeInput}
                            onKeyDownInput={onKeyDownInput}
                            oncheckBox={oncheckBox}
                            setData={state}
                            error={errors[data.code] ? true : false}
                            errorNew={errors}
                            errorNested={errors}
                            errorMessage={'This field is required.'}
                            OuterIndex={index}
                          /> : ''
                      }

                      {
                        (index == 4 || index == 5 || index == 6 || index == 7) ? '' :
                          index == 3 ?
                            state["DEGREECERTIFICATE_INST"] && state["DEGREECERTIFICATE_INST"].length &&
                            <FormInput
                              inputData={data}
                              listOptions={data.options}
                              onchangeInput={onchangeInput}
                              onKeyDownInput={onKeyDownInput}
                              oncheckBox={oncheckBox}
                              setData={state}
                              error={errors[data.code] ? true : false}
                              errorNew={errors}
                              errorNested={errors}
                              errorMessage={'This field is required.'}
                              OuterIndex={index}
                              manageOption={true}
                            />
                            :
                            <FormInput
                              inputData={data}
                              listOptions={data.options}
                              onchangeInput={onchangeInput}
                              onKeyDownInput={onKeyDownInput}
                              oncheckBox={oncheckBox}
                              setData={state}
                              error={errors[data.code] ? true : false}
                              errorNew={errors}
                              errorNested={errors}
                              errorMessage={'This field is required.'}
                              OuterIndex={index}
                            />
                      }

                    </Col>


                    {
                      //code for logical questions
                      data.isLogical == true ?
                        data.options && data.options.map((option, index_1) => (
                          <div key={index_1} style={{ marginLeft: 10 }}>
                            {
                              option.isLogical == true && option.questions && option.questions.map((question, index_2) => (

                                <React.Fragment key={index_2}>
                                  {
                                    question.isLogical == true && question.questions && index_2 == 0 &&
                                      state && state[data.code] && state[data.code].indexOf(option.value) > -1 ?
                                      state["DEGREECERTIFICATE_INST"] && state["DEGREECERTIFICATE_INST"].length &&
                                      <div className="col4 fw600  fs18 mb-2">
                                        {option.heading ? option.heading : option.value}
                                        {question.isRequired && option.value ? <sup className="starQ">*</sup> : ''}
                                      </div> : ''
                                  }

                                  {/* --- For logical question 1 and 2  ------- */}
                                  {
                                    question.isLogical == true && question.options &&
                                    state && state[data.code] && state[data.code].indexOf(option.value) > -1 &&
                                    <div key={index_2}>
                                      <div className="col4 fw600  fs18 mb-2" style={{ marginLeft: -27 }}>
                                        {index + 1}.{index_1 + 1} &nbsp; {option.value}{" Application deadline"}
                                        {question.isRequired && option.value ? <sup className="starQ">*</sup> : ''}
                                      </div>
                                      <Row className="examList" style={{ paddingLeft: 5 }}>
                                        {
                                          question.isLogical == true &&
                                          question.options && question.options.map((option_1, index_3) => (
                                            <div key={index_3}>

                                              <Col key={index_3 + 'j'} lg={'12'} md={'12'}>
                                                <FormInput
                                                  inputData={option_1}
                                                  listOptions={option_1}
                                                  onchangeInput={onchangeInput}
                                                  onKeyDownInput={onKeyDownInput}
                                                  oncheckBox={oncheckBox}
                                                  setData={state}
                                                  lowerCode={option_1.code.split("_")[0]}
                                                  error={errors[option_1.code] ? true : false}
                                                  errorNew={errors}
                                                  errorNested={errors}
                                                  errorMessage={'This field is required.'}
                                                  OuterIndex={index}
                                                />
                                              </Col>
                                            </div>

                                          ))
                                        }
                                      </Row>
                                    </div>
                                  }







                                  {/*  --- For logical question 4  ------- */}
                                  {
                                    index == 3 && index_2 == 0 &&
                                    option.isLogical == true && option.questions &&
                                    state && state[data.code] && state[data.code].indexOf(option.value) > -1 &&
                                    <div key={index}>
                                      <Row className="examList" style={{ paddingLeft: 5 }}>
                                        {
                                          <div key={index}>
                                            {
                                              (option.code == "VOCATIONAL_TRAINING"
                                                || option.code == "SUMMER_INTERNSHIP"
                                                || option.code == "BRIDGE_FOR_BACHELORS"
                                                || option.code == "BRIDGE_FOR_MASTERS"
                                              ) ?
                                                <Col key={index + 'j'} lg={'12'} md={'12'}>
                                                  <div className="col4 fw600  fs18 mb-2">
                                                    {/* <span className="numericType1">{index + 1}.1</span> */}
                                                    {option.questions[0].text}
                                                    {option.questions[0].isRequired ? <sup className="starQ">*</sup> : ''}
                                                  </div>
                                                  <FormInput
                                                    inputData={option.questions[0]}
                                                    listOptions={option}
                                                    onchangeInput={onchangeInput}
                                                    onKeyDownInput={onKeyDownInput}
                                                    oncheckBox={oncheckBox}
                                                    setData={state}
                                                    isReqClass={true}
                                                    error={errors[option.code] ? true : false}
                                                    errorNew={errors}
                                                    errorNested={errors}
                                                    errorMessage={urlMsg && errors[option.code] ? urlMsg : 'This field is required.'}
                                                    OuterIndex={index}
                                                  />
                                                </Col>
                                                :
                                                <Col key={index + 'j'} lg={'12'} md={'12'}>
                                                  <FormInput
                                                    inputData={option}
                                                    listOptions={option}
                                                    onchangeInput={onchangeInput}
                                                    onKeyDownInput={onKeyDownInput}
                                                    oncheckBox={oncheckBox}
                                                    setData={state}
                                                    isReqClass={true}
                                                    error={errors[option.code] ? true : false}
                                                    errorNew={errors}
                                                    errorNested={errors}
                                                    errorMessage={'This field is required.'}
                                                    OuterIndex={index}
                                                  />
                                                </Col>
                                            }

                                          </div>
                                        }
                                      </Row>
                                    </div>
                                  }







                                  {/*  --- For logical question 3  ------- */}
                                  {/*  --- when select TOEFL, SAT, ACT and all  ------- */}
                                  {
                                    index == 3 && state["DEGREECERTIFICATE_INST"] && state["DEGREECERTIFICATE_INST"].length &&
                                    question.isLogical == true && question.questions && question.value &&
                                    state && state[data.code] && state[data.code].indexOf(option.value) > -1 &&
                                    state && state[option.code] && state[option.code].indexOf(question.value) > -1 &&

                                    <div key={index_2}>
                                      <div className="col4 fw600  fs18 mb-2">
                                        {question.heading}
                                        {question.isRequired && question.heading ? <sup className="starQ">*</sup> : ''}
                                      </div>
                                      <Row className="examList" style={{ paddingLeft: 5 }}>
                                        {
                                          question.isLogical == true &&
                                          question.questions && question.questions.map((option_2, index_3) => (
                                            <div key={index_3}>
                                              <Col key={index_3 + 'j'} lg={'12'} md={'12'}>
                                                <div className="col2 fw600 mb-3">{option_2.text}</div>
                                                <FormInput
                                                  inputData={option_2}
                                                  listOptions={option_2}
                                                  onchangeInput={onchangeInput}
                                                  onKeyDownInput={onKeyDownInput}
                                                  oncheckBox={oncheckBox}
                                                  setData={state}
                                                  isReqClass={true}
                                                  error={errors[option_2.code] ? true : false}
                                                  errorNew={errors}
                                                  errorNested={errors}
                                                  errorMessage={validMsg ? validMsg : 'This field is required.'}
                                                  OuterIndex={index}
                                                />
                                              </Col>
                                            </div>

                                          ))
                                        }
                                      </Row>
                                    </div>
                                  }
                                </React.Fragment>


                              ))
                            }









                          </div>

                        ))
                        : ''
                    }


                  </div>
                ) : ''
              )
            }



          </Row>
        </div>
        <div className="footerBotton">
          <div className="hrBorder pt-3"></div>
          <div className="text-right d-flex flex-wrap justify-content-between  mt-3">
            <Button type="button" className="btnType7"
              onClick={() => props.stepSelect('second')}>
              Previous
            </Button>
            <Button type="button" className="btnType2"
              onClick={() => stepSubmit()}>
              Save & Next
            </Button>
          </div>
        </div>

      </Col>
    </div>





  )
}

export default ThirdStep;




