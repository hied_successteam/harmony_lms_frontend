import React, { useState, useEffect } from 'react';
import { Col, Row, Button, Form, Table } from 'react-bootstrap';
import 'rsuite/dist/styles/rsuite-default.css';
import FormInput from '../FormInput'
import { aboutTheInstitution } from '../WizardQuestion'
import Validator from "validator";
import { getLocalStorage } from '../../../../../Lib/Utils';


const SecondStep = (props) => {

  const [state, setState] = useState({});
  const [errors, setErrors] = useState({});
  const [urlMsg, setUrlMsg] = useState('');

  useEffect(() =>{
    setErrors({})
  },[props])


  const onchangeInput = (event, code, type) => {
    setUrlMsg('')
    if(code){
      if(type && type == "slider"){
        setState({ ...state, [code]: event });
        errors[[code]] = false
      }else{
        if (event) {
          setState({ ...state, [code]: event.getTime() });
        } else {
            setState({ ...state, [code]: 0 });
        }
      }
    }else{
      let { target: { name, value } } = event;
      setState({ ...state, [name]: value });
      errors[[name]] = false
    }
  };


  //select check box
  const oncheckBox = (name, value) => {
    if (state[name]) {
      let ArryVale = state[name];
      var indexB = ArryVale.indexOf(value);
      if (indexB > -1) {
        ArryVale.splice(indexB, 1);
        if(ArryVale && ArryVale.length <= 0){
          delete state[name]
          setState({ ...state});
        }else{
          setState({ ...state, [name]: [...ArryVale] });
        }
      } else {
        ArryVale.push(value);
        setState({ ...state, [name]: [...ArryVale] });
      }
    } else {
      setState({ ...state, [name]: [value] });
    }
  }



  useEffect(() => {

    setState({
      ...state, STUDENTDEMOGRAPHI: 4, QUALITYEDUCATION: 4, COLLEGELOCATION: 4, CLIMATECONDITION: 4, JOBREADINESS: 4, FINANCIALAFFORAD: 4, SPECIALMISSION: "None"
    });
    if (props.data && Object.keys(props.data).length !== 0) {
      setState({
        ...state,
        ...props.data
      });
    }
  }, [props.data]);



  const stepSubmit = () => {
    let errors = {};
    let valid = true;

    //Check parent validation
    for (var m = 0; m < aboutTheInstitution.length; m++) {
      if (aboutTheInstitution[m].isRequired) {

        let codevalue = state[aboutTheInstitution[m].code];
        if (aboutTheInstitution[m].code !== "" && (!codevalue || codevalue == 'undefined' || codevalue == '')) {
          valid = false;
          errors[aboutTheInstitution[m].code] = true;
        }


        // Check child validation
        if (aboutTheInstitution[m].isLogical) {
          let ChildArr = aboutTheInstitution[m].options;
          if(ChildArr && ChildArr.length){
            for (var n = 0; n < ChildArr.length; n++) {
              if(state && state[aboutTheInstitution[m].code] && state[aboutTheInstitution[m].code].indexOf("Yes") > -1){
                if(ChildArr[n].questions && ChildArr[n].questions.length){
                  var NestedArr = ChildArr[n].questions
                  for (var i = 0; i < NestedArr.length; i++) {
                    if (NestedArr[i].isRequired) {
                      if(NestedArr[i].questions && NestedArr[i].questions.length){
                        let statevalueNew = state[NestedArr[i].questions[0].code];
                        if (!statevalueNew || statevalueNew == 'undefined' || statevalueNew == '') {
                          valid = false;
                          errors[NestedArr[i].questions[0].code] = true;
                        }
                      }else{
                        let codevalueNew = state[NestedArr[i].code];
                        if (!codevalueNew || codevalueNew == 'undefined' || codevalueNew == '' || codevalueNew == 0) {
                          valid = false;
                          errors[NestedArr[i].code] = true;
                        }
                        
                        if(codevalueNew && state && (state["OFFERWEBLINK"] || state["ALUMNIWEBLINK"]) && (NestedArr[i].code == "OFFERWEBLINK" || NestedArr[i].code == "ALUMNIWEBLINK")){

                          var regex = /(http|https):(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
 
                          
                          if(codevalueNew && Validator.isURL(codevalueNew)){
                            if (codevalueNew && codevalueNew.match(regex)) {
                            }else{
                              valid = false;
                              errors[NestedArr[i].code] = true;
                              setUrlMsg('Valid URL is required (ex.- https://hiedharmony.com).')
                            }
                          }else{
                            valid = false;
                            errors[NestedArr[i].code] = true;
                            setUrlMsg('Valid URL is required (ex.- https://hiedharmony.com).')
                          }
                          
                        }
                      }
                    }
                  }
                }
              }else{
                if(ChildArr[n].questions && ChildArr[n].questions.length && state && state[aboutTheInstitution[m].code] && state[aboutTheInstitution[m].code].indexOf(ChildArr[n].text) > -1){
                  var NestedArr = ChildArr[n].questions
                  for (var i = 0; i < NestedArr.length; i++) {
                    if (NestedArr[i].isRequired) {
                      let codevalueNew = state[NestedArr[i].code];
                      if (!codevalueNew || codevalueNew == 'undefined' || codevalueNew == '') {
                        valid = false;
                        errors[NestedArr[i].code] = true;
                      }
                    }
                  }
                }
              }
            }
          }

          let Newarry = aboutTheInstitution[m].questions;
          if(Newarry && Newarry.length){
            for (var j = 0; j < Newarry.length; j++) {
              if (Newarry[j].isLogical && Newarry[j].isRequired) {
                let codevalueNew = state[Newarry[j].code];
                if (!codevalueNew || codevalueNew == 'undefined' || codevalueNew == '') {
                  valid = false;
                  errors[Newarry[j].code] = true;
                }
              }
            }
          }
        }
      }
    }

    if (valid) {
      props.stepSubmitSecond(state, 2);
      setErrors({});
    }
    else {
      setErrors({ ...errors });
    }

  }


  const onKeyDownInput = event => {
    if (event && event.target && event.target.name) {
      const { target: { name, value } } = event;

      if (event.keyCode == 13) {
        setState({ ...state, [name]: '' });
        var arr = name.split('_');
        let Newname = arr[0];
        if (state[Newname]) {
          let ArryVale = [...state[Newname]];
          var indexB = ArryVale.indexOf(value);
          if (indexB > -1) {
            ArryVale.splice(indexB, 1);
            setState({ ...state, [name]: '', [Newname]: [...ArryVale] });
          } else {
            ArryVale.push(value);
            setState({ ...state, [name]: '', [Newname]: [...ArryVale] });
          }
        } else {
          setState({ ...state, [name]: '', [Newname]: [value] });
        }
      }
    } else {
      //setState({ ...state, DOB: event });
    }
  };


  const dotIndex = (code) =>{
    if(state["FINANCIALAIDS"].indexOf("Need-based scholarships") > -1 && state["FINANCIALAIDS"].indexOf("Merit-based scholarships") > -1 && state["FINANCIALAIDS"].indexOf("Others") > -1){
      if(code == "NEEDBASEDSCOLARSHIP") return 1
      if(code == "MERITBASEDSCOLARSHIP") return 2
      if(code == "FINANCIALAIDS_OTHEROPTION") return 3
    }else if(state["FINANCIALAIDS"].indexOf("Need-based scholarships") > -1 && state["FINANCIALAIDS"].indexOf("Merit-based scholarships") > -1){
      if(code == "NEEDBASEDSCOLARSHIP") return 1
      if(code == "MERITBASEDSCOLARSHIP") return 2
    }else if(state["FINANCIALAIDS"].indexOf("Need-based scholarships") > -1  && state["FINANCIALAIDS"].indexOf("Others") > -1){
      if(code == "NEEDBASEDSCOLARSHIP") return 1
      if(code == "FINANCIALAIDS_OTHEROPTION") return 2
    }else if(state["FINANCIALAIDS"].indexOf("Merit-based scholarships") > -1 && state["FINANCIALAIDS"].indexOf("Others") > -1){
      if(code == "MERITBASEDSCOLARSHIP") return 1
      if(code == "FINANCIALAIDS_OTHEROPTION") return 2
    }else if(state["FINANCIALAIDS"].indexOf("Need-based scholarships") > -1){
      return 1
    }else if(state["FINANCIALAIDS"].indexOf("Merit-based scholarships") > -1){
      return 1
    }else if(state["FINANCIALAIDS"].indexOf("Others") > -1){
      return 1
    }
  }




  return (
    <div className="rightBox pt-5 pb-5 pl-4">
      <Col lg={10} md={12}>
        <div className="rightLayout">
          <div className="d-flex flex-wrap justify-content-between mb-1">
            <div className="stepTitle fw500 fs30 col4 position-relative mb-4 pb-1">About the Institution</div>
            {
              getLocalStorage('user') && getLocalStorage('user').isProfileCompleted &&
              <div className="col2 fw500 fs18 pointer" onClick={() => props.stepSelect('third')}>Skip</div>
            }
          </div>


          {/* Uper Questions */}
          <Row className="stepForm7">
            {
              aboutTheInstitution && aboutTheInstitution.map((data, index) =>
                data.section == 'aboutTheInstitution' ? (
                  <div key={index + 'i'}>
                    <Col md={12} key={index + 'i'}>
                      <div className="col4 fw600  fs18 mb-2">
                        <span className="numericType1">{index + 1}.</span>
                        {data.text}
                        {data.isRequired ? <sup className="starQ">*</sup> : ''}
                      </div>
                    </Col>
                    <Col key={index + 'j'} lg={'12'} md={'12'}>
                      <FormInput
                        inputData={data}
                        listOptions={data.options}
                        onchangeInput={onchangeInput}
                        onKeyDownInput={onKeyDownInput}
                        oncheckBox={oncheckBox}
                        setData={state}
                        error={errors[data.code] ? true : false}
                        errorNew={errors}
                        errorNested={errors}
                        errorMessage={'This field is required.'}
                        OuterIndex={index}
                        cssSteps={true}
                      />
                    </Col>



                    {/* 
                    Logical questio for 
                    "If Option YES or NO"
                    */}
                    {
                      data && data.isLogical == true &&
                      
                      data.options && data.options.map((option, index_1) =>(
                        state && state[data.code] && state[data.code].indexOf("Yes") > -1  ?
                        <div key={index_1}>
                          {
                            option.questions && option.questions.map((question, index_2) =>(
                            <div key={index_2}>
                                <Col md={12} key={index_2 + 'i'}>
                                  <div className="col4 fw600  fs18 mb-2">
                                    <span className="numericType1">{index + 1}.{index_2 + 1}</span>
                                    {question.text}
                                    {question.isRequired ? <sup className="starQ">*</sup> : ''}
                                  </div>
                                </Col>
                                <Col key={index_2 + 'j'} lg={'12'} md={'12'}>
                                  <FormInput
                                    inputData={question}
                                    listOptions={question}
                                    onchangeInput={onchangeInput}
                                    onKeyDownInput={onKeyDownInput}
                                    oncheckBox={oncheckBox}
                                    setData={state}
                                    error={errors[question.code] ? true : false}
                                    errorNew={errors}
                                    errorNested={errors}
                                    errorMessage={(question.code == "OFFERWEBLINK" || question.code ==  "ALUMNIWEBLINK") && urlMsg ? urlMsg : 'This field is required.'}
                                    OuterIndex={index_2}
                                  />
                                </Col>
                            </div>
                            ))
                          }
                        </div>

                        :

                        
                        // Logical questio for 
                        // "If Option is Checkbox"
                      
                        <div key={index_1}>
                          {
                            state && state[data.code] && state[data.code].indexOf(option.text) > -1 &&
                            option.questions && option.questions.map((question, index_2) =>(
                            <div key={index_2}>
                                <Col md={12} key={index_2 + 'i'}>
                                  <div className="col4 fw600  fs18 mb-2">
                                    <span className="numericType1">{index + 1}.
                                      {
                                        dotIndex(question.code)
                                      }
                                    </span>
                                    {question.text}
                                    {question.isRequired ? <sup className="starQ">*</sup> : ''}
                                  </div>
                                </Col>
                                <Col key={index_2 + 'j'} lg={'12'} md={'12'}>
                                  <FormInput
                                    inputData={question}
                                    listOptions={question}
                                    onchangeInput={onchangeInput}
                                    onKeyDownInput={onKeyDownInput}
                                    oncheckBox={oncheckBox}
                                    setData={state}
                                    error={errors[question.code] ? true : false}
                                    errorNew={errors}
                                    errorNested={errors}
                                    errorMessage={'This field is required.'}
                                    OuterIndex={index_2}
                                  />
                                </Col>
                            </div>
                            ))
                          }
                        </div>
                      ))

                      

                    }



              





                    


                    



                    {
                      // ----------  Matrix ------------//
                      // ---- Logical Question ---------//

                      data.isLogical == true && data.questions && data.code == "" &&
                        <Table bordered className="tableType2 br10 mb-4">
                          <thead>
                            <tr>
                              <th></th>
                              {
                                data.questions && data.questions.map((option, indexNew) => (
                                  indexNew == 0 &&
                                  option.options && option.options.map((opt, indexNew_1) => (
                                    <th key={indexNew_1}>{opt.value}</th>
                                  ))
                                ))
                              }
                            </tr>
                          </thead>
                          <tbody>

                            {
                              data.questions && data.questions.map((option, indexNew) => (

                                <tr key={indexNew}>
                                  <td style={{ verticalAlign: 'middle' }}>
                                    {option.text}
                                    {
                                      errors[option.code] &&
                                      <span className="help-block error-text">
                                        <span style={{ color: "red", fontSize: 13 }}>
                                          {'This field is required.'}
                                        </span>
                                      </span>
                                    }
                                  </td>
                                  {option && option.options.map((number, index) =>
                                    <td key={index}>
                                      <Form.Group controlId={`formBasicRadio1${option.code}${number.itemValue}`}>
                                        <Form.Check
                                          type="radio"
                                          aria-label={option.text}
                                          className="checkboxTyp3"
                                          id={`formBasicRadio1${option.code}${number.itemValue}`}
                                          name={option.code}
                                          label=""
                                          value={number.itemValue || ''}
                                          checked={true}
                                          checked={state[option.code] == number.itemValue}
                                          onChange={onchangeInput}
                                        />
                                      </Form.Group>
                                    </td>
                                  )}
                                </tr>
                              ))
                            }

                          </tbody>
                        </Table>
                    }


                  </div>
                ) : ''
              )
            }



          </Row>
        </div>
        <div className="footerBotton">
          <div className="hrBorder pt-3"></div>
          <div className="text-right d-flex flex-wrap justify-content-between  mt-3">
            <Button type="button" className="btnType7"
              onClick={() => props.stepSelect('first')}>
              Previous
            </Button>
            <Button type="button" className="btnType2"
              onClick={() => stepSubmit()}>
              Save & Next
            </Button>
          </div>
        </div>
      </Col>
    </div>
  )
}

export default SecondStep;





