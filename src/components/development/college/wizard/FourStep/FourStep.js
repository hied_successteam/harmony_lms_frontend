import React, { useState, useEffect } from 'react';
import { Col, Row, Button, Image } from 'react-bootstrap';
import 'rsuite/dist/styles/rsuite-default.css';
import FormInput from '../FormInput'
import { academicsAndExtracurriculars } from '../WizardQuestion'
import { getLocalStorage } from '../../../../../Lib/Utils';
import TipModal from '../../../TipModal/TipModal'
import infoicon from "../../../../../assets/images/svg/questions.svg";


const FourStep = (props) => {

  const [state, setState] = useState({});
  const [errors, setErrors] = useState({});
  const [tipsData, setTipsData] = useState(false);
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);

  const handleShow = (tipsData) => {
    setTipsData(tipsData);
    setShow(true);
  }

  useEffect(() => {
    setErrors({})
  }, [props])


  const onchangeInput = event => {
    const { target: { name, value } } = event;
    setState({ ...state, [name]: value });
  };

  const onKeyDownInput = event => {
    if (event && event.target && event.target.name) {
      const { target: { name, value } } = event;

      if (event.keyCode == 13) {
        setState({ ...state, [name]: '' });

        var arr = name.split('_');
        let Newname = arr[0];

        if (state[Newname]) {
          let ArryVale = [...state[Newname]];
          var indexB = ArryVale.indexOf(value);
          if (indexB > -1) {
            ArryVale.splice(indexB, 1);
            setState({ ...state, [name]: '', [Newname]: [...ArryVale] });
          } else {
            ArryVale.push(value);
            setState({ ...state, [name]: '', [Newname]: [...ArryVale] });
          }
        } else {
          setState({ ...state, [name]: '', [Newname]: [value] });
        }
      }
    } else {
      //setState({ ...state, DOB: event });
    }
  };

  //select check box
  const oncheckBox = (name, value) => {
    if (state[name]) {
      let ArryVale = state[name];
      var indexB = ArryVale.indexOf(value);
      if (indexB > -1) {
        ArryVale.splice(indexB, 1);
        if (ArryVale && ArryVale.length <= 0) {
          delete state[name]
          setState({ ...state });
        } else {
          setState({ ...state, [name]: [...ArryVale] });
        }
      } else {
        ArryVale.push(value);
        setState({ ...state, [name]: [...ArryVale] });
      }
    } else {
      setState({ ...state, [name]: [value] });
    }
  }


  useEffect(() => {

    if (props.data && Object.keys(props.data).length !== 0) {

      setState({
        ...state,
        ...props.data
      });
    }
  }, [props.data]);




  const stepSubmit = () => {
    let errors = {};
    let valid = true;
    for (var m = 0; m < academicsAndExtracurriculars.length; m++) {

      if (academicsAndExtracurriculars[m].isRequired) {
        let codevalue = state[academicsAndExtracurriculars[m].code];
        if (!codevalue || codevalue == 'undefined' || codevalue == '') {
          valid = false;
          errors[academicsAndExtracurriculars[m].code] = true;
        }


        // Check child validation
        if (academicsAndExtracurriculars[m].isLogical && academicsAndExtracurriculars[m].options) {
          let Newarry = academicsAndExtracurriculars[m].options;
          for (var n = 0; n < Newarry.length; n++) {
            if (Newarry[n].isLogical && Newarry[n].questions && state[academicsAndExtracurriculars[m].code] && state[academicsAndExtracurriculars[m].code].indexOf(Newarry[n].value) > -1) {
              let newdata = Newarry[n].questions;
              for (var i = 0; i < newdata.length; i++) {
                let codevalueNew = state[newdata[i].code];
                if (!codevalueNew || codevalueNew == 'undefined' || codevalueNew == '') {
                  valid = false;
                  errors[newdata[i].code] = true;
                }

                if (newdata[i].options) {
                  let newOptionData = newdata[i].options;
                  for (var j = 0; j < newOptionData.length; j++) {
                    if (newOptionData[j].isRequired && newOptionData[j].questions) {
                      if(state[newdata[i].code] && state[newdata[i].code].indexOf(newOptionData[j].value) > -1 && newOptionData[j].questions && newOptionData[j].questions.length){
                        let optionCodeValue = state[newOptionData[j].questions[0].code];
                        if (!optionCodeValue || optionCodeValue == 'undefined' || optionCodeValue == '') {
                          valid = false;
                          errors[newOptionData[j].questions[0].code] = true;
                        }
                      }
                      
                    }

                  }
                }
              }
            }
          }
        }
      }else{
        if (academicsAndExtracurriculars[m].isLogical && academicsAndExtracurriculars[m].options) {
          let Newarry = academicsAndExtracurriculars[m].options;
          for (var n = 0; n < Newarry.length; n++) {
            if (Newarry[n].isLogical && Newarry[n].questions && state[academicsAndExtracurriculars[m].code] && state[academicsAndExtracurriculars[m].code].indexOf(Newarry[n].value) > -1) {
              let newdata = Newarry[n].questions;
              for (var i = 0; i < newdata.length; i++) {
                let codevalueNew = state[newdata[i].code];               
                if (!codevalueNew || codevalueNew == 'undefined' || codevalueNew == '') {
                  valid = false;
                  errors[newdata[i].code] = true;
                }
              }
            }
          }
        }
      }
    }

    if (valid) {
      props.stepSubmitFour(state, 4);
      setErrors({});
    }
    else {
      setErrors({ ...errors });
    }

  }



  return (


    <div className="rightBox pt-5 pb-5 pl-4">

      <Col lg={10} md={12}>
        <div className="rightLayout">
          <div className="d-flex flex-wrap justify-content-between mb-1">
            <div className="stepTitle fw500 fs30 col4 position-relative mb-4 pb-1">Academics and Extracurriculars</div>
            {
              getLocalStorage('user') && getLocalStorage('user').isProfileCompleted &&
              <div className="col2 fw500 fs18 pointer" onClick={() => props.stepSelect('fivth')}>Skip</div>
            }
          </div>
          <Row className="stepForm7">
            {
              academicsAndExtracurriculars && academicsAndExtracurriculars.map((data, index) =>
                data.section == 'academicsAndExtracurriculars' ? (
                  <div key={index + 'i'}>
                    <Col md={12} key={index + 'i'}>
                      <div className="col4 fw600  fs18 mb-2">
                        <span className="numericType1">{index + 1}.</span>
                        {data.text}
                        {data.isRequired ? <sup className="starQ">*</sup> : ''}
                        {data.tips ? <Image onClick={() => handleShow(data.tips)}
                              src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                      </div>
                    </Col>
                    <Col key={index + 'j'} lg={'12'} md={'12'}>
                      <FormInput
                        inputData={data}
                        listOptions={data.options}
                        onchangeInput={onchangeInput}
                        onKeyDownInput={onKeyDownInput}
                        oncheckBox={oncheckBox}
                        setData={state}
                        error={errors[data.code] ? true : false}
                        errorNew={errors}
                        errorNested={errors}
                        errorMessage={'This field is required.'}
                        OuterIndex={index}
                      />
                    </Col>


                    {
                      //code for logical questions
                      data.isLogical == true ?
                        data.options && data.options.map((option, index_1) => (
                          <div key={index_1}>
                            {


                              <React.Fragment key={index_1}>

                                {/* --- For logical question 3  ------- */}
                                {
                                  option.isLogical == true && option.questions && data.code != "MODEOFEDUCATION" &&
                                  state && state[data.code] && state[data.code].indexOf(option.value) > -1 &&
                                  <div key={index_1}>
                                    {/* {
                                      (option.value !== "Others" && option.value !== "Other" && option.value !== "International (country, multicultural)") &&
                                      <div className="col4 fw600 fs18 mb-2 pl-4">
                                        {option.value}
                                        {option.isRequired && option.value ? <sup className="starQ">*</sup> : ''}
                                      </div>
                                    } */}
                                    {
                                      option.isLogical == true &&
                                      option.questions && option.questions.map((option_1, index_3) => (
                                        <React.Fragment key={index_3 + 'm'}>
                                          <Row key={index_3} className="examList pl-4">
                                            <Col md={12} key={index_3 + 'i'}>
                                              <div className="col4 fw600  fs18 mb-2"> 
                                                <span className="numericType1"> 
                                                {index+1}.
                                                {
                                                  data.code == "RECREATIONALACTIVITIES" &&
                                                  (state[data.code].indexOf("International (country, multicultural)") > -1 && state[data.code].indexOf("Other") > -1 && option_1.code == "RECREATIONALACTIVITIES_OTHEROPTION") ? index_3 + 2 :index_3 + 1
                                                }
                                                </span>
                                                {option_1.text}
                                                {option_1.isRequired && option_1.text ? <sup className="starQ">*</sup> : ''}
                                              </div>
                                            </Col>
                                            <Col key={index_3 + 'j'} lg={'12'} md={'12'}>
                                              <FormInput
                                                inputData={option_1}
                                                listOptions={option_1.options}
                                                onchangeInput={onchangeInput}
                                                onKeyDownInput={onKeyDownInput}
                                                oncheckBox={oncheckBox}
                                                setData={state}
                                                error={errors[option_1.code] ? true : false}
                                                errorNew={errors}
                                                errorNested={errors}
                                                errorMessage={'This field is required.'}
                                                OuterIndex={index}
                                              />
                                            </Col>
                                          </Row>
                                        </React.Fragment>
                                      ))
                                    }
                                  </div>
                                }


                                {
                                  option.isLogical == true && data.code == "MODEOFEDUCATION" &&
                                  state && state[data.code] && (state[data.code].indexOf("Online") > -1 || state[data.code].indexOf("Hybrid") > -1) &&
                                  <div key={index_1}>
                                    {
                                      option.isLogical == true &&
                                      data.options[0] && data.options[0].questions && data.options[0].questions.map((option_1, index_3) => (
                                        <React.Fragment key={index_3 + 'm'}>
                                          <Row key={index_3} className="examList pl-4">
                                            <Col md={12} key={index_3 + 'i'}>
                                              <div className="col4 fw600  fs18 mb-2"> 
                                                <span className="numericType1"> 
                                                {index+1}.
                                                {
                                                  data.code == "RECREATIONALACTIVITIES" &&
                                                  (state[data.code].indexOf("International (country, multicultural)") > -1 && state[data.code].indexOf("Other") > -1 && option_1.code == "RECREATIONALACTIVITIES_OTHEROPTION") ? index_3 + 2 :index_3 + 1
                                                }
                                                </span>
                                                {option_1.text}
                                                {option_1.isRequired && option_1.text ? <sup className="starQ">*</sup> : ''}
                                              </div>
                                            </Col>
                                            <Col key={index_3 + 'j'} lg={'12'} md={'12'}>
                                              <FormInput
                                                inputData={option_1}
                                                listOptions={option_1.options}
                                                onchangeInput={onchangeInput}
                                                onKeyDownInput={onKeyDownInput}
                                                oncheckBox={oncheckBox}
                                                setData={state}
                                                error={errors[option_1.code] ? true : false}
                                                errorNew={errors}
                                                errorNested={errors}
                                                errorMessage={'This field is required.'}
                                                OuterIndex={index}
                                              />
                                            </Col>
                                          </Row>
                                          {
                                            option_1.options && option_1.options.map((option_2, index_4) => (
                                              option_2.isLogical == true && option_2.questions && option_2.questions.length &&
                                              state && state[option_1.code] && state[option_1.code].indexOf(option_2.value) > -1 &&
                                              <Row key={index_3} className="examList pl-4"> 
                                                <Col md={12} key={index_3 + 'i'}>
                                                  <div className="col4 fw600  fs18 mb-2">
                                                    <span className="numericType1"></span>
                                                    {option_2.text}
                                                    {option_2.isRequired && option_2.text ? <sup className="starQ">*</sup> : ''}
                                                  </div>
                                                </Col>

                                                <Col key={index_3 + 'j'} lg={'12'} md={'12'}>
                                                  <FormInput
                                                    inputData={option_2.questions[0]}
                                                    listOptions={option_2.questions}
                                                    onchangeInput={onchangeInput}
                                                    onKeyDownInput={onKeyDownInput}
                                                    oncheckBox={oncheckBox}
                                                    setData={state}
                                                    error={errors[option_2.questions[0].code] ? true : false}
                                                    errorNew={errors}
                                                    errorNested={errors}
                                                    errorMessage={'This field is required.'}
                                                    OuterIndex={index}
                                                  />
                                                </Col>
                                              </Row>
                                            ))
                                          }
                                        </React.Fragment>
                                      ))
                                    }
                                  </div>
                                }


                              </React.Fragment>
                            }
                          </div>
                        ))
                      : ''
                    }


                  </div>
                ) : ''
              )
            }



          </Row>
        </div>
        <div className="footerBotton">
          <div className="hrBorder pt-3"></div>
          <div className="text-right d-flex flex-wrap justify-content-between  mt-3">
            <Button type="button" className="btnType7"
              onClick={() => props.stepSelect('third')}>
              Previous
            </Button>
            <Button type="button" className="btnType2"
              onClick={() => stepSubmit()}>
              Save & Next
            </Button>
          </div>
        </div>

      </Col>

      <TipModal tipsData= {tipsData} title={"Info"} show= {show} handleClose= {handleClose}></TipModal>

    </div>





  )
}

export default FourStep;




