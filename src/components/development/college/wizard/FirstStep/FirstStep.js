import React, { useState, useEffect } from 'react';
import { Col, Row, Button } from 'react-bootstrap';
import 'rsuite/dist/styles/rsuite-default.css';
import FormInput from '../FormInput'
import { instituteRegistration } from '../WizardQuestion'
import { getLocalStorage } from '../../../../../Lib/Utils';
import { stateList } from '../../../../../Actions/Student/register'


const FirstStep = (props) => {

  const [state, setState] = useState({});
  const [errors, setErrors] = useState({});
  const [stateDatas, setStateDatas] = useState({});

  useEffect(() => {
    setErrors({})
  }, [props])


  useEffect(() => {
    var data = getLocalStorage('user');
    setState({
      ...state,
      EMAIL: data.email,
      COLLOFFICERNAME: data.firstName,
      CONTCTNUM: data.mobileNumber,
      INSTITUTENAME: data.instituteName,
      UNITID: data.instituteId,
      OTHERUNITID: data.otherInstituteId ? data.otherInstituteId : '',
      ISOTHER: data.instituteType && data.instituteType == 'other' ? true : false
    });

    stateList(231).then(res => {
      setStateDatas(res.data.data)
    }).catch(err => { });

    // }, []);
  }, []);


  const onchangeInput = event => {
    let { target: { name, value } } = event;
    setState({ ...state, [name]: value });
    errors[[name]] = false
  };


  useEffect(() => {
    if (props.data && Object.keys(props.data).length !== 0) {
      setState({
        ...state,
        ...props.data
      });
    }
  }, [props.data]);


  const stepSubmit = () => {
    let errors = {};
    let valid = true;
    for (var m = 0; m < instituteRegistration.length; m++) {

      if (instituteRegistration[m].isRequired) {

        let codevalue = state[instituteRegistration[m].code];
        if (!codevalue || codevalue == 'undefined' || codevalue == '') {
          valid = false;
          errors[instituteRegistration[m].code] = true;
        }
      }
    }

    if (valid) {
      props.stepSubmit(state, 1);
      setErrors({});
    }
    else {
      setErrors({ ...errors });
    }
  }


  return (
    <div className="rightBox pt-5 pb-5 pl-4">
      <Col lg={10} md={12}>
        <div className="rightLayout">
          <div className="d-flex flex-wrap justify-content-between mb-1">
            <div className="stepTitle fw500 fs30 col4 position-relative mb-4 pb-1">Institute Registration</div>
            {
              (getLocalStorage('user') && getLocalStorage('user').isProfileCompleted) &&
              <div className="col2 fw500 fs18 pointer" onClick={() => props.stepSelect('second')}>Skip</div>
            }
          </div>
          <Row>

            {
              instituteRegistration && instituteRegistration.map((data, index) =>

                <React.Fragment key={index}>
                  <Col md={data.code == 'INSTITUTENAME' ? '12' : '6'} key={index}>
                    <div className="col2 fw600 mb-1">{data.text}
                      {data.isRequired ? <sup className="starQ starQ2">*</sup> : ''}
                    </div>
                    <FormInput
                      key={index}
                      inputData={data}
                      listOptions={stateDatas}
                      onchangeInput={onchangeInput}
                      setData={state}
                      error={errors[data.code] ? true : false}
                      errorMessage={'This field is required.'}
                    />
                  </Col>
                </React.Fragment>

              )
            }

          </Row>
        </div>
        <div className="footerBotton">
          <div className="hrBorder pt-3"></div>
          <div className="text-right mt-3">
            <Button
              type="button"
              className="btnType2"
              onClick={() => stepSubmit()}
            >
              Save & Next
            </Button>
          </div>
        </div>
      </Col>
    </div>





  )
}

export default FirstStep;




