import React from 'react';
import { Form } from 'react-bootstrap';
import 'rsuite/dist/styles/rsuite-default.css';
import "react-datepicker/dist/react-datepicker.css";
import DatePicker from "react-datepicker";
import { Slider } from 'rsuite';

// var datas = ''


const FormInput = (props) => {

  switch (props.inputData.type) {

    case 'text':
      return (
        <Form.Group className="priceBox" controlId={`formBasicName${props.inputData.code}`}>
          <Form.Control
            type="text"
            name={props.inputData.code}
            value={props.setData[props.inputData.code] || ''}
            placeholder={props.inputData.placeholder}
            maxLength={props.inputData.maxLength &&  props.inputData.maxLength ? props.inputData.maxLength : 1000}
            className={`inputType1`}
            disabled={(props.inputData.code == 'INSTITUTENAME' || props.inputData.code ==  'COLLOFFICERNAME') ? true : false}
            onChange={props.onchangeInput}
            onKeyDown={props.onKeyDownInput}
          />

          {
            props.error &&
            <span className="help-block error-text">
              <span style={{ color: "red", fontSize: 13 }}>
                {props.errorMessage}
              </span>
            </span>
          }
        </Form.Group>
      )

    case 'number':
      return (
        <Form.Group className={`priceBox ${props.inputData.code == 'GRADUATEPROGRAMOFFERED' ? 'input-group' : ''} `} controlId={`formBasicName${props.inputData.code}`}>

            {props.inputData.code == 'GRADUATEPROGRAMOFFERED' ?
              <div className="input-group-prepend inputGroup1">
                  <span className="input-group-text" id="basic-addon1ddfd">$</span>
              </div>
              : ''}
          <Form.Control
            type="text"
            maxLength={10}
            name={props.inputData.code}
            value={props.setData[props.inputData.code] || ''}
            placeholder={props.inputData.placeholder}
            className={props.isReqClass ? `inputType2` : `inputType1`}
            onKeyDown={props.onKeyDownInput}
            disabled={props.inputData.code == 'CONTCTNUM' ? true : false}
            onChange={(e) => 
              (e.target.name == "ASSO_IELTS_LISTENING" || e.target.name == "ASSO_IELTS_READING" || e.target.name == "ASSO_IELTS_WRITING" || e.target.name == "ASSO_IELTS_SPEAKING" || e.target.name == "ASSO_IELTS_BANDSCORE" || e.target.name == "BACHE_IELTS_LISTENING" || e.target.name == "BACHE_IELTS_READING" || e.target.name == "BACHE_IELTS_WRITING" || e.target.name == "BACHE_IELTS_SPEAKING" || e.target.name == "BACHE_IELTS_BANDSCORE" || e.target.name == "MASTER_IELTS_LISTENING" || e.target.name == "MASTER_IELTS_READING" || e.target.name == "MASTER_IELTS_WRITING" || e.target.name == "MASTER_IELTS_SPEAKING" || e.target.name == "MASTER_IELTS_BANDSCORE" || e.target.name == "DOCTORATE_IELTS_LISTENING" || e.target.name == "DOCTORATE_IELTS_READING" || e.target.name == "DOCTORATE_IELTS_WRITING" || e.target.name == "DOCTORATE_IELTS_SPEAKING" || e.target.name == "DOCTORATE_IELTS_BANDSCORE") 
              ?
              e.target.value.match('^[.0-9 ]*$') != null ? 
                props.onchangeInput(e)
            : ''
              :
              e.target.value.match('^[0-9 ]*$') != null ? 
                props.onchangeInput(e)
            : ''
          }
          />

          {
            props.error &&
            <span className="help-block error-text">
              <span style={{ color: "red", fontSize: 12 }}>
                {props.errorMessage}
              </span>
            </span>
          }
        </Form.Group>
      )

      case 'float':
        return (
          <Form.Group className={`priceBox ${props.inputData.code == 'GRADUATEPROGRAMOFFERED' ? 'input-group' : ''} `} controlId={`formBasicName${props.inputData.code}`}>
  
              {props.inputData.code == 'GRADUATEPROGRAMOFFERED' ?
                <div className="input-group-prepend inputGroup1">
                    <span className="input-group-text" id="basic-addon1ddfd">$</span>
                </div>
                : ''}
              <Form.Control
                type="text"
                maxLength={10}
                name={props.inputData.code}
                value={props.setData[props.inputData.code] || ''}
                placeholder={props.inputData.placeholder}
                className={props.isReqClass ? `inputType2` : `inputType1`}
                onKeyDown={props.onKeyDownInput}
                disabled={props.inputData.code == 'CONTCTNUM' ? true : false}
                onChange={(e) => 
                  e.target.value.match('^[.0-9 ]*$') != null ? 
                    props.onchangeInput(e)
                : ''
              }
            />
  
            {
              props.error &&
              <span className="help-block error-text">
                <span style={{ color: "red", fontSize: 12 }}>
                  {props.errorMessage}
                </span>
              </span>
            }
          </Form.Group>
      )

    case 'dropDown':
      return (
        <Form.Group >
          <Form.Control as="select"
            className="selectTyp1 pointer"
            name={props.inputData.code}
            value={props.setData[props.inputData.code] || ''}
            onChange={props.onchangeInput}
          >
            <option value="">Select</option>
            {
              props && props.listOptions && props.listOptions.length &&
              props.listOptions.map((item, index) => {
                return <option key={index} title={item.name ? item.name : item.text} value={item.name ? item.name : item.value}>{item.name ? item.name : item.text}</option>
              })
            }
          </Form.Control>
          {
            props.error &&
            <span className="help-block error-text">
              <span style={{ color: "red", fontSize: 13 }}>
                {props.errorMessage}
              </span>
            </span>
          }
        </Form.Group>
      )

    case 'checkbox':
      return (
        <div className="tagType1 d-flex flex-wrap mb-3"> 
          {
            props.inputData.options && !props.manageOption &&
            props.inputData.options.map((item, index) => {
              return <span
                key={index}
                className={`br8 col4 fw500 mr-3 ${props.setData[props.inputData.code] && props.setData[props.inputData.code].indexOf(item.value) > -1 ? 'bgCol35' : ''}  `}
                name={props.inputData.code}
                onClick={() => props.oncheckBox(props.inputData.code, item.value)}
                value={item.value}>{item.value || ''}</span>
            })
          }
          {
            props.inputData.options && props.manageOption == true &&
            props.inputData.options.map((item, index) => {
              if(props.manageOption == true && props.setData["DEGREECERTIFICATE_INST"].indexOf(item.value) > -1){
                return <span
                  key={index}
                  className={`br8 col4 fw500 mr-3 ${props.setData[props.inputData.code] && props.setData[props.inputData.code].indexOf(item.value) > -1 ? 'bgCol35' : ''}  `}
                  name={props.inputData.code}
                  onClick={() => props.oncheckBox(props.inputData.code, item.value)}
                  value={item.value}>{item.value || ''}</span>
              }
            })
          }
          {
            props.OuterIndex == 3 && props.setData["DEGREECERTIFICATE_INST"] && props.setData["DEGREECERTIFICATE_INST"].length &&
            props.inputData.questions &&
            props.inputData.questions.map((item, index) => {
              return <span
                key={index}
                className={`br8 col4 fw500 mr-3 ${props.setData[props.inputData.code] && props.setData[props.inputData.code].indexOf(item.value) > -1 ? 'bgCol35' : ''}  `}
                name={props.inputData.code}
                onClick={() => props.oncheckBox(props.inputData.code, item.value)}
                value={item.value}>{item.value || ''}</span>
            })
          }
          {
            props.error &&
            <span className="help-block error-text">
              <span style={{ color: "red", fontSize: 13 }}>
                {props.errorMessage}
              </span>
            </span>
          }
        </div>
      )


    case 'radio': 
      return (
        <Form.Group className="radioOne">
          {
            props.inputData.options.map((item, index) => {
              return <Form.Check 
                type="radio"
                key={index}
                label={item.text}
                className={props.setData[props.inputData.code] == item.value ? props.cssSteps ? "radioType1 radio2 btm-margin actives" : "radioType1 btm-margin actives" : props.cssSteps ? "radioType1 radio2 btm-margin" : "radioType1 btm-margin"}
                id={props.inputData.code} name={props.inputData.code} value={item.value || ''}
                checked={props.setData[props.inputData.code] == item.text}
                onChange={props.onchangeInput} />
            })
          }
          {
            props.error &&
            <span className="help-block error-text">
              <span style={{ color: "red", fontSize: 13 }}>  
                {props.errorMessage}
              </span>
            </span>
          }  
        </Form.Group>
      )

    case 'email':
      return (
        <Form.Group >
          <Form.Control
            type="text"
            name={props.inputData.code}
            value={props.setData[props.inputData.code] || ''}
            placeholder={props.inputData.placeholder}
            className="inputType1"
            disabled={props.inputData.code == 'EMAIL' ? true : false}
            onChange={props.onchangeInput}
          />
          {
            props.error &&
            <span className="help-block error-text">
              <span style={{ color: "red", fontSize: 13 }}>
                {props.errorMessage}
              </span>
            </span>
          }
        </Form.Group>
      )

    case 'textArea':
      return (
        <>
          <Form.Group className="mb-3">
            <Form.Control
              as="textarea"
              name={props.inputData.code}
              value={props.setData[props.inputData.code] || ''}
              placeholder={props.inputData.placeholder}
              className="inputType1"
              rows={5}
              onChange={props.onchangeInput}
            />
            {
              props.error &&
              <span className="help-block error-text">
                <span style={{ color: "red", fontSize: 13 }}>
                  {props.errorMessage}
                </span>
              </span>
            }
          </Form.Group>
        </>
      )

    case 'date':
      return (
        <Form.Group controlId={`formBasicName${props.inputData.code}`} key={`formBasicName${props.inputData.code}`}>

          <DatePicker
            selected={props.setData[props.inputData.code] || ''}
            name={props.inputData.code}
            value={props.setData[props.inputData.code] || ''}
            className="inputType1 w-100"
            maxDate={props.inputData.code == props.lowerCode+"_STARTDATE" && props.setData && props.setData[props.lowerCode+"_ENDDATE"] ? new Date(props.setData[props.lowerCode+"_ENDDATE"]) : ''}

            minDate={props.inputData.code == props.lowerCode+"_ENDDATE" && props.setData && props.setData[props.lowerCode+"_STARTDATE"] ? new Date(props.setData[props.lowerCode+"_STARTDATE"]) : new Date()}

            placeholderText={props.inputData.placeholder}
            onChange={(e) => props.onchangeInput(e, props.inputData.code)}
            dateFormat="dd/MM"
            autoComplete="off"
          //locale="es"
          />
          {
            props.error &&
            <span className="help-block error-text">
              <span style={{ color: "red", fontSize: 13 }}>
                {props.errorMessage}
              </span>
            </span>
          }
        </Form.Group>
      )

    case 'slider':
      return (
        <>
        {
          props.inputData.questions && props.inputData.questions.map((item, index) =>(
            <Form.Group controlId="formBasicRange" key={index}>
              {
                <Form.Label className="fw600 col2 mb-3">
                  <span className="mr-3">{item.text}</span>
                </Form.Label>
              }
              
              <Slider name={item.code} onChange={(e) => props.onchangeInput(e, item.code, "slider")} defaultValue={2} value={props.setData[item.code] || item.minRange} min={item.minRange} step={item.interval ? item.interval : 1} max={item.maxRange} graduated progress
                renderMark={mark => {
                  return mark;
                }}
              />
              {
                props.error &&
                <span className="help-block error-text">
                  <span style={{ color: "red", fontSize: 13 }}>
                    {props.errorMessage}
                  </span>
                </span>
              }
              <Form.Label className="fw600 col2 mb-3"></Form.Label>
            </Form.Group>
          )) 
        }

        </>

      )
    default:
      return '';
  }


}

export default FormInput;




