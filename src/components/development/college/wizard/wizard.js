import React, { useState, useEffect } from 'react';
import { Col, Row, Image, Nav, Tab } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import 'rsuite/dist/styles/rsuite-default.css';
import footerLogo from '../../../../assets/images/footerlogo.png';
import navComments from '../../../../assets/images/svg/comments.svg';

import FirstStep from './FirstStep/FirstStep';
import SecondStep from './SecondStep/SecondStep';
import ThirdStep from './ThirdStep/ThirdStep';
import FourStep from './FourStep/FourStep';
import FiveStep from './FiveStep/FiveStep';
import SixStep from './SixStep/SixStep';
import { logout } from '../../../../Actions/Student/register';
import { getLocalStorage, clearLocalStorage } from '../../../../Lib/Utils';
import { collegeProfile, getCollegeProfile } from '../../../../Actions/Student/register';
import { actionLogout } from '../../../../Redux/College/LoginAction';

const Wizard = (props) => {

  const [key, setKey] = useState('first');
  const [userData, setUserData] = useState({});
  const [profileData, setProfileData] = useState({});
  const [stage, setStage] = useState(false);
  const [profileStepCompleted, setProfileStepCompleted] = useState(1);
  const [instituteReg, setInstituteReg] = useState({});
  const [aboutTheInstitution, setAboutTheInstitution] = useState({});
  const [admissions, setAdmissions] = useState({});
  const [academicsAndExtracurriculars, setAcademicsAndExtracurriculars] = useState({});
  const [campusInfrastructure, setCampusInfrastructure] = useState({});
  const [placement, setPlacement] = useState({});
  const [isProfileCompleted, setIsProfileCompleted] = useState(false);

  const dispatch = useDispatch();

  useEffect(() => {
    var user = getLocalStorage('user');

    if (user && user.id) {
      setIsProfileCompleted(user.isProfileCompleted ? user.isProfileCompleted : false);
      setUserData(user);
      getCollegeProfile().then(res => {
        setProfileData(res.data.data);
      }).catch(err => {
      });
    }

  }, []);

  useEffect(() => {

    if (profileData && Object.keys(profileData).length != 0) {

      if (profileData.instituteReg && Object.keys(profileData.instituteReg).length != 0) {
        setInstituteReg({ ...profileData.instituteReg });
      }

      if (profileData.aboutTheInstitution && Object.keys(profileData.aboutTheInstitution).length != 0) {
        setAboutTheInstitution({ ...profileData.aboutTheInstitution });
      }
      if (profileData.admissions && Object.keys(profileData.admissions).length != 0) {
        setAdmissions({ ...profileData.admissions });
      }
      if (profileData.academicsAndExtracurriculars && Object.keys(profileData.academicsAndExtracurriculars).length != 0) {
        setAcademicsAndExtracurriculars({ ...profileData.academicsAndExtracurriculars });
      }
      if (profileData.campusInfrastructure && Object.keys(profileData.campusInfrastructure).length != 0) {
        setCampusInfrastructure({ ...profileData.campusInfrastructure });
      }
      if (profileData.placement && Object.keys(profileData.placement).length != 0) {
        setPlacement({ ...profileData.placement });
      }
    }

  }, [profileData]);


  const logoutCall = () => {
    var user = getLocalStorage('user');
    if (user && user.id) {
      logout({
        userId: user.id
      }).then(res => {
        if (res.data.success == true) {
          dispatch(actionLogout('college'));
          localStorage.clear();
          clearLocalStorage();
          props.history.push({ pathname: '/college/login' });
        } else {
          dispatch(actionLogout('college'));
          localStorage.clear();
          clearLocalStorage();
          props.history.push({ pathname: '/college/login' });
        }
      }).catch(err => {
        dispatch(actionLogout('college'));
        localStorage.clear();
        clearLocalStorage();
        props.history.push({ pathname: '/college/login' });
      });
    } else {
      dispatch(actionLogout('college'));
      localStorage.clear();
      clearLocalStorage();
      props.history.push({ pathname: '/college/login' });
    }

  }


  const stepSubmit = (state, steps) => {
    window.scrollTo(0, 0);
    setProfileStepCompleted(steps);
    setInstituteReg({ ...state });
    setStage(true);
    setKey('second')
  }

  const stepSubmitSecond = (state, steps) => {
    window.scrollTo(0, 0);
    setProfileStepCompleted(steps);
    setAboutTheInstitution({ ...state });
    setStage(true);
    setKey('third')
  }

  const stepSubmitThird = (state, steps) => {
    window.scrollTo(0, 0);
    setProfileStepCompleted(steps);
    setAdmissions({ ...state });
    setStage(true);
    setKey('fourth')
  }

  const stepSubmitFour = (state, steps) => {
    window.scrollTo(0, 0);
    setProfileStepCompleted(steps);
    setAcademicsAndExtracurriculars({ ...state });
    setStage(true);
    setKey('fivth')
  }

  const stepSubmitFive = (state, steps) => {
    window.scrollTo(0, 0);
    setProfileStepCompleted(steps);
    setCampusInfrastructure({ ...state });
    setStage(true);
    setKey('sixth')
  }

  const stepSubmitSix = (state, steps) => {
    window.scrollTo(0, 0);
    setProfileStepCompleted(steps);
    setIsProfileCompleted(true);
    setPlacement({ ...state });
    setStage(true);
  }



  useEffect(() => {
    if (stage && stage == true) {
      let data = {
        "profileStepCompleted": profileStepCompleted,
        "isProfileCompleted": isProfileCompleted,
        "profileDetail": {
          "instituteReg": instituteReg,
          "aboutTheInstitution": aboutTheInstitution,
          "admissions": admissions,
          "academicsAndExtracurriculars": academicsAndExtracurriculars,
          "campusInfrastructure": campusInfrastructure,
          "placement": placement,
        }
      }

      collegeProfile(data, userData.id).then(({ data: { success, data, message } }) => {
        setStage(false);
        if (success !== true) {
          //setMessage(message);                                                                    
          return;
        } else {

          if (profileStepCompleted == 6 && isProfileCompleted == true) {
            props.history.push({ pathname: 'successwizard' });
          }
        }
      })
        .catch((error) => {
          setStage(false);
          return;
        });
    }
  }, [stage]);

  const stepSelect = (key) => {
    window.scrollTo(0, 0);
    if (key == 'final') {
      props.history.push({ pathname: 'dashboard' });
    } else {
      setKey(key)
    }
  }

  const manageSteps = (key) => {
    window.scrollTo(0, 0);
    if (key == "first" && Object.keys(instituteReg).length != 0) {
      setKey(key)
    }
    if (key == "second" && Object.keys(aboutTheInstitution).length != 0) {
      setKey(key)
    }
    if (key == "third" && Object.keys(admissions).length != 0) {
      setKey(key)
    }
    if (key == "fourth" && Object.keys(academicsAndExtracurriculars).length != 0) {
      setKey(key)
    }
    if (key == "fivth" && Object.keys(campusInfrastructure).length != 0) {
      setKey(key)
    }

    if (key == "sixth" && Object.keys(placement).length != 0) {
      setKey(key)
    }


  }


  return (
    <div className="wizardLayout">
      <div className="container-fluid pl-0">
        <Tab.Container
          id="uncontrolled-tab-example"
          //defaultActiveKey="fourth"                    
          activeKey={key}
          onSelect={(k) => manageSteps(k)}
        >
          <Row>  
            <Col sm={3}>
              <div className="sidebarBox d-flex align-items-center justify-content-center text-center">
                <Nav variant="pills" className="flex-column innerBox">
                  <Nav.Item onClick={() => logoutCall()}>     
                    <Nav.Link eventKey="profile">
                      <span className="titles text-uppercase fs16 fw600 d-block mt-3 col3">Logout</span>
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item className="pb-1 mb-2">  
                    <Nav.Link eventKey="profile" disabled>
                      <span className="profileTitl1 text-uppercase fs16 fw600 d-block col3">Profile</span>
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item className="pb-1">
                    <Nav.Link eventKey="first" className="listBox">
                      <span className="circleBox text-uppercase d-block m-auto col3 fs24 br50 mb-4 fw600">1</span>
                      <span className="titles text-uppercase fs15 fw600 d-block mt-3">Institute Registration</span>
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item className="pb-1">
                    <Nav.Link eventKey="second" className="listBox">
                      <span className="circleBox text-uppercase d-block m-auto col3 fs24 br50 mb-4 fw600">2</span>
                      <span className="titles text-uppercase fs15 fw600 d-block mt-3">About the Institution</span>
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item className="pb-1">
                    <Nav.Link eventKey="third" className="listBox">
                      <span className="circleBox text-uppercase d-block m-auto col3 fs24 br50 mb-4 fw600">3</span>
                      <span className="titles text-uppercase fs15 fw600 d-block mt-3">Admissions</span>
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item className="pb-1">
                    <Nav.Link eventKey="fourth" className="listBox">
                      <span className="circleBox text-uppercase d-block m-auto col3 fs24 br50 mb-4 fw600">4</span>
                      <span className="titles text-uppercase fs15 fw600 d-block mt-3">
                        Academics and Extracurriculars
                      </span>
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item className="pb-1">
                    <Nav.Link eventKey="fivth" className="listBox">
                      <span className="circleBox text-uppercase d-block m-auto col3 fs24 br50 mb-4 fw600">5</span>
                      <span className="titles text-uppercase fs15 fw600 d-block mt-3">Campus Infrastructure</span>
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item className="pb-1">
                    <Nav.Link eventKey="sixth" className="listBox">
                      <span className="circleBox text-uppercase d-block m-auto col3 fs24 br50 mb-4 fw600">6</span>
                      <span className="titles text-uppercase fs15 fw600 d-block mt-3">Placement</span>
                    </Nav.Link>
                  </Nav.Item>
                </Nav>
                <div className="navBarBottom">
                  <div className="w-100 d-flex">
                    <div>
                      <Image src={navComments} alt="Comments" className="comment1" />  
                    </div>
                    <div>
                      <div className="col3 fs13 fw500 mb-1">If you have any queries, email us at</div>
                      <a className="col1 fs13 fw500 mb-1" href={`mailto:${"info@hiedsuccess.com"}`}>
                        info@hiedsuccess.com   
                      </a>
                    </div>
                  </div>
                </div>
              </div>

            </Col>
            <Col sm={9}>
              <Tab.Content>
                <Tab.Pane eventKey="first" >
                  <FirstStep
                    stepSubmit={stepSubmit}
                    data={profileData ? profileData.instituteReg : {}}
                    stepSelect={stepSelect}
                  />
                </Tab.Pane>

                <Tab.Pane eventKey="second">
                  <SecondStep
                    stepSubmitSecond={stepSubmitSecond}
                    data={profileData ? profileData.aboutTheInstitution : {}}
                    stepSelect={stepSelect}
                  />
                </Tab.Pane>

                <Tab.Pane eventKey="third" >
                  <ThirdStep
                    stepSubmitThird={stepSubmitThird}
                    data={profileData ? profileData.admissions : ''}
                    stepSelect={stepSelect}
                  />
                </Tab.Pane>

                <Tab.Pane eventKey="fourth">
                  <FourStep
                    stepSubmitFour={stepSubmitFour}
                    data={profileData ? profileData.academicsAndExtracurriculars : {}}
                    stepSelect={stepSelect}
                  />
                </Tab.Pane>

                <Tab.Pane eventKey="fivth">
                  <FiveStep
                    stepSubmitFive={stepSubmitFive}
                    data={profileData ? profileData.campusInfrastructure : {}}
                    stepSelect={stepSelect}
                  />
                </Tab.Pane>

                <Tab.Pane eventKey="sixth">
                  <SixStep
                    stepSubmitSix={stepSubmitSix}
                    data={profileData ? profileData.placement : {}}
                    stepSelect={stepSelect}
                  />
                </Tab.Pane>

              </Tab.Content>
            </Col>
          </Row>
        </Tab.Container>
      </div>
      <div className="bottomFooter d-flex align-items-center bgCol43">
        <div className="container-fluid">
          <Row>
            <Col md={12}>
              <div className="d-flex align-items-center">
                <Image src={footerLogo} alt="footer-logo" className="fLogo" />
                <div className="col42 fs13 ml-4">Copyright © {new Date().getFullYear()} HiEd Success | Powered by HiEd Success</div>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  )
}

export default Wizard;




