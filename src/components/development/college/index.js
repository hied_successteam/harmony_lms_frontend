import React from "react";
import Login from "./Login/login";
import Register from "./Register/register";
import Success from "./Register/success";
import Dashboard from "./dashboard/dashboard";
import PublicRoute from '../../../routes/PublicRoute';
import PrivateRoute from '../../../routes/PrivateRoute';
import Wizard from '../college/wizard/wizard'
import successWizard from '../college/wizard/successWizard'
import MyProfile from '../college/myprofile/myprofile';
import StudentDetail from '../college/studentDetail/studentDetail';
import Comparison from '../college/comparison/comparison';
import FavouriteStudentList from '../college/FavouriteStudent/FavouriteStudentList';
import PrivateRouteWizard from '../../../routes/PrivateRouteWizard';
import PrivateRouteDashboard from '../../../routes/PrivateRouteDashboard';
import PrivateRouteClgDashboard from '../../../routes/PrivateRouteClgDashboard';
import PrivateRouteClgWizard from '../../../routes/PrivateRouteClgWizard';


const CollegeModule = ({ match }) => (
  <div>


    <PublicRoute path={`${match.url}/login`} component={Login} />
    <PublicRoute path={`${match.url}/register`} component={Register} />
    <PublicRoute path={`${match.url}/success`} component={Success} /> 


    <PrivateRouteClgDashboard path={`${match.url}/dashboard`} component={Dashboard} />

    {/* <PrivateRouteClgWizard path={`${match.url}/wizard`} component={Wizard} /> */}
    <PrivateRoute path={`${match.url}/wizard`} component={Wizard} />
    <PrivateRouteClgWizard path={`${match.url}/successwizard`} component={successWizard} />
    <PrivateRoute path={`${match.url}/myProfile`} component={MyProfile} />
    <PrivateRoute path={`${match.url}/studentDetail`} component={StudentDetail} />
    <PrivateRoute path={`${match.url}/favouriteStudent`} component={FavouriteStudentList} />
    <PrivateRoute path={`${match.url}/comparison`} component={Comparison} />

    {/* <Route path={`${match.url}/dashboard`} component={Dashboard} />    */}
    {/* <Route path={`${match.url}/notification`} component={Notification} />    */}


  </div>
);

export default CollegeModule;
