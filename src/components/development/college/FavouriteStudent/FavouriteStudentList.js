import React, { useEffect, useState } from 'react';
import { Col, Row, Container, Image, Form, Dropdown, Accordion, Card, Button, Modal } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import Logoadmin from '../../../../assets/images/svg/logoadmin.svg';
import SearchIcon from '../../../../assets/images/svg/MagnifyingGlass.svg';
import bellIcon from '../../../../assets/images/svg/Bell.svg';
import Bookmark from '../../../../assets/images/svg/BookmarkSimple.svg';
import hearts from '../../../../assets/images/svg/hearts.svg';
import heartfill from '../../../../assets/images/svg/heartfill.svg';
import BackOne from '../../../../assets/images/svg/backs.svg';
import Settings from '../../../../assets/images/svg/GearSix.svg';
import Downloads from '../../../../assets/images/svg/downloads.svg';
import MapPin from '../../../../assets/images/svg/MapPin.svg';
import Hand from '../../../../assets/images/svg/Hand.svg';
import Money from '../../../../assets/images/svg/Money.svg';
import DotsThreeVertical from '../../../../assets/images/svg/DotsThreeVertical.svg';
import UserOne from '../../../../assets/images/teamtop.png';
import UserThree from '../../../../assets/images/user3.png';
import Scale from '../../../../assets/images/Scale.png';
import Scaletwo from '../../../../assets/images/Scaletwo.png';
import { RangeSlider } from 'rsuite';
import 'rsuite/dist/styles/rsuite-default.css';
import Loader from '../../../../Lib/LoaderNew';
import { setLocalStorage, getLocalStorage, clearLocalStorage, arrToStr } from '../../../../Lib/Utils';
import { studentFavourite, getStudentFavourite, getCollegeDashboard, logout } from '../../../../Actions/Student/register';
import CONSTANTS from '../../../../Lib/Constants';

import { AcademicInfo } from '../../student/wizard/WizardQuestion'
import { major } from '../../../../Actions/Student/register';
import Pagination from "react-bootstrap-4-pagination";

import { actionLogout } from '../../../../Redux/College/LoginAction';
import ReactSpeedometer from "react-d3-speedometer"



// // Step 2 - Include the react-fusioncharts component
// import ReactFC from "react-fusioncharts";

// // Step 3 - Include the fusioncharts library
// import FusionCharts from "fusioncharts";

// // Step 4 - Include the chart type
// import Column2D from "fusioncharts/fusioncharts.charts";

// // Step 5 - Include the theme as fusion
// import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";

// // Step 6 - Adding the chart and theme as dependency to the core fusioncharts
// ReactFC.fcRoot(FusionCharts, Column2D, FusionTheme); 


const FavouriteStudent = (props) => {


  const [favouriteList, setFavouriteList] = useState('');
  const [loader, setLoader] = useState(false);
  const [userData, setUserData] = useState(false);
  const [moreToggle, setMoreToggle] = useState([]); 
  const dispatch = useDispatch();



  useEffect(() => {
    listApiCall()
  }, [])

  const listApiCall = () => {
    window.scrollTo(0, 0);
    setLoader(true)
    var user = getLocalStorage('user')
    setUserData(user)
    getStudentFavourite(user._id).then(res => {
      setLoader(false)
      if (res && res.data && res.data.data && res.data.data.rows) {
        setFavouriteList(res.data.data.rows)
      } else {
        setFavouriteList([])
      }
    }).catch(err => {
      setLoader(false)
    });
  }


  const logoutCall = () => {
    var user = getLocalStorage('user');
    if (user && user.id) {
      logout({
        userId: user.id
      }).then(res => {
        if (res.data.success == true) {
          dispatch(actionLogout('college'));
          localStorage.clear();
          clearLocalStorage();
          props.history.push({ pathname: '/college/login' });
        } else {
          dispatch(actionLogout('college'));
          localStorage.clear();
          clearLocalStorage();
          props.history.push({ pathname: '/college/login' });
        }
      }).catch(err => {
        dispatch(actionLogout('college'));
        localStorage.clear();
        clearLocalStorage();
        props.history.push({ pathname: '/college/login' });
      });
    } else {
      dispatch(actionLogout('college'));
      localStorage.clear();
      clearLocalStorage();
      props.history.push({ pathname: '/college/login' });
    }
  }

  const studentDetailPage = (item) => {
    setLocalStorage('stdDetail', item)
    props.history.push({ pathname: '/college/studentDetail', studentData: item });
  }

  const unFavoriteApiCall = (id, type) => {
    studentFavourite({ favouriteId: id, type }).then(res => {
      if (res.data.success == true) {

        var data = getLocalStorage('user')
        if (data && data.favourite && data.favourite.length > 0) {
          let ArryVale = data.favourite;
          var indexB = ArryVale.indexOf(id);
          if (indexB > -1) {
            ArryVale.splice(indexB, 1);
            data.favourite = [...ArryVale]
            setLocalStorage('user', data);
          }
        }
        listApiCall()
      }

    }).catch(err => { })
  }

    //Manage more toggle option
    const moreToggleOption = (value) => {    

      let ArryVale = moreToggle;
      var indexB = ArryVale.indexOf(value);
      if (indexB > -1) {
        ArryVale.splice(indexB, 1);
        setMoreToggle([...ArryVale]);      
      } else {
        ArryVale.push(value);
        setMoreToggle([...ArryVale]);      
      }       
  
    }



  return (
    <div className="collegeAdminUi">
      <Container fluid>

        <Row>
          <Col md={8} className="topHeaderBox bgCol25 pl-0 pr-0">
            <div className="pl-4 pr-4 pt-4">
              <Row>
                <Col md={4}>
                  <Image src={Logoadmin} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" />
                  <span className="fs20 col24 fw700 ml-1">HiEd Harmony</span>
                </Col>
                <Col md={5}>
                  {/* <div className="searchBox d-flex justify-content-center">
                    <Form.Group className="position-relative w-100">
                      <Form.Control type="text" placeholder="Search..." className="searchType1 shadow3 w-100" />
                      <Image src={SearchIcon} alt="Search" className="pointer searchOne" />
                    </Form.Group>
                  </div> */}
                </Col>

              </Row>
              <div className="middileAccordian mt-4">

                <div className="col2 fs20 fw500 mt-4 mb-3">
                  <div style={{ width: 100, cursor: 'pointer' }} onClick={() => props.history.goBack()}>
                    <Image src={BackOne} alt="back" className="pointer mr-2 back1" /> Back
                  </div>
                </div>

                <div className="mt-4 pt-2">
                  <Row>
                    <Col md={6}>
                      <div className="col2 fs22 fw500">Favourite Candidates Results ({favouriteList && favouriteList.length})</div>
                      <div className="col5 fs14 fw400">Eligible students matching your criteria</div>
                    </Col>
                    <Col md={6}>
                      <div className="d-flex flex-wrap justify-content-end">
                        {/* <div className="mr-1 fs14 fw600 col2 pt-1">Sort by:
                        </div> */}
                        {/* <div className="mr-3">
                          <Dropdown>
                            <Dropdown.Toggle id="dropdown-basic" className="DropdownType1 col2 fs14 fw400 pt-0 pb-0">
                              Admission Probability <i className="fa fa-chevron-down ml-1 col2" aria-hidden="true"></i>
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                              <Dropdown.Item >Profile</Dropdown.Item>
                              <Dropdown.Item href="#">Logout</Dropdown.Item>
                            </Dropdown.Menu>
                          </Dropdown>
                        </div> */}
                        {/* <div className="mr-3">
                          <Image src={Downloads} alt="Icon" className="pointer " />
                        </div> */}
                        {/* <div>
                          <Button type="button" className="btnType5 opacityActive">Compare</Button>
                        </div> */}
                      </div>
                    </Col>
                  </Row>
                </div>


                <div className="mt-4">
                  {
                    favouriteList && favouriteList.length > 0 &&
                    favouriteList.map((item, index) => {
                      return <React.Fragment key={index}>
                        <Row >
                          <Col md={6}>
                            <div className="d-flex flex-wrap">
                              {/* <Form.Group controlId="formBasicCheckbox" className="formCheckboxs mr-2">
                                <Form.Check type="checkbox" className="pointer checkboxTyp1" label="" />
                              </Form.Group> */}
                              <div>
                                <div className="mb-4">
                                  <div className="mb-2 col2 fw600 fs20">
                                    {item.name && item.name}
                                  </div>
                                  <div className="d-flex flex-wrap">
                                    <div className="mr-3 col5 fs14 fw500 d-flex align-items-center">
                                      {item.city && item.city + ', '}
                                      {item.state && item.state + ', '}
                                      {item.country && item.country}
                                    </div>
                                    {/* <div className="mr-3 col5 fs14 fw500 d-flex align-items-center">
                                      <Image src={Hand} alt="Icon" className="pointer mr-1" />
                                      40 Hours
                                    </div>
                                    <div className="col5 fs14 fw500 d-flex align-items-center">
                                      <Image src={Money} alt="Icon" className="pointer mr-1" />
                                      Need Financing
                                    </div> */}
                                  </div>
                                </div>
                                <div className="col29 fw500 fs14 mb-1">{item.fieldOfInterest && "Field of Interest"}</div>
                                {
                                  item.fieldOfInterest &&
                                  <div className="d-flex flex-wrap mb-4">
                                    <div className="tagTitle actives text-uppercase fw500 br40 col2 fs12 mr-2 mb-2 pointer">
                                      {item.fieldOfInterest}
                                    </div>
                                  </div>
                                }

                                <div className="d-flex flex-wrap">
                                  <div className="mr-3">
                                    <span className="d-inline-block bgCol1 wishListOne br50 text-center">
                                       <Image src={hearts} alt="Search" className="pointer" onClick={() => unFavoriteApiCall(item._id, false)} /> 
                                    </span>
                                    {/* <Button type="button" className="btnType6" onClick={() => unFavoriteApiCall(item._id, false)}>Mark as Unfavorite</Button> */}
                                  </div>
                                  <div className="mr-4">
                                    <Button type="button" className="btnType5" onClick={() => studentDetailPage(item)}>View Details</Button>
                                  </div>
                                  <div>
                                    <Dropdown>
                                      <Dropdown.Toggle id="dropdown-basic2"
                                        onClick={() => moreToggleOption(item._id)}
                                        className="DropdownType1 col2 fs14 fw600 pt-0 pb-0">
                                        More
                                        <i className="fa fa-caret-down col2 ml-1" aria-hidden="true"></i>
                                      </Dropdown.Toggle>
                                    </Dropdown>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </Col>
                          <Col md={6} className="row min15">
                            <Col md={6} className="speedMeter pr0">
                              <ReactSpeedometer
                                width={190}          
                                maxheight={100}
                                height={115}
                                needleHeightRatio={0.7}
                                value={0}
                                maxValue={100}
                                customSegmentStops={[0, 33, 66, 100]}
                                segmentColors={['#FE5E54', '#FFDB1B', '#89D667']}
                                currentValueText={'${value}%'}
                                segments={3}
                                maxSegmentLabels={0}
                                ringWidth={10}
                                needleTransitionDuration={33}
                                needleTransition="easeElastic"
                                needleColor={'#a7ff83'}
                                textColor={'#000000'}
                              /> 
                              <span className="titleGraph">Admission Probability</span>  
                            </Col>

                            <Col md={6} className="speedMeter pr0"> 

                               <ReactSpeedometer
                                width={190}
                                height={115}                                            
                                maxheight={100}
                                needleHeightRatio={0.7}
                                value={0}
                                maxValue={100}
                                customSegmentStops={[0, 33, 66, 100]}
                                segmentColors={['#FE5E54', '#FFDB1B', '#89D667']}
                                currentValueText={'${value}%'}
                                segments={3}
                                maxSegmentLabels={0}
                                ringWidth={10}
                                needleTransitionDuration={33}
                                needleTransition="easeElastic"
                                needleColor={'#a7ff83'}
                                textColor={'#000000'}
                              />   
                              <span className="titleGraph">Profile Compatibility</span> 
                            </Col>

                          </Col>                          
                         { moreToggle && moreToggle.length > 0 && moreToggle.indexOf(item._id) > -1 ?
                          <React.Fragment>
                            <Col md={1}></Col>
                            <Col md={11}>
                              <div className="popularType1">
                                <ul className="d-flex flex-wrap pl-1 w-100">
                                  <li>Term: {item.term}</li>
                                  <li>Entrances Taken/Appearing: { }
                                    {/* {
                                      item.entranceappeared && item.entranceappeared.length > 0 && item.entranceappeared.map((entr) =>{
                                        return entr
                                      })
                                    } */}
                                     {item.entranceappeared && item.entranceappeared.length > 0 ? arrToStr(item.entranceappeared) :'-'}
                                  </li>
                                  <li>Highest degree: {item.bachelor}</li>
                                </ul>
                                {

                                  <ul className="d-flex flex-wrap pl-1 w-100">
                                    {/* <li>Field of Study: {item.fieldOfInterest && item.fieldOfInterest}</li> */}
                                    <li className="flex1"><span className="d-block">Major:</span>
                                      {
                                        item.major && item.major.length > 0 && item.major.map((fields) => {
                                          return <div className="tagTitle mt-1 actives text-uppercase fw500 br40 col2 fs14 mr-2 mb-1 pointer d-inline-block">
                                            {fields}
                                          </div>
                                        })
                                      }</li>
                                  </ul>
                                }
                              </div>
                            </Col>
                          </React.Fragment>
                          : '' }
                        </Row>
                        <hr className="borderOne" />
                      </React.Fragment>
                    })
                  }

                </div>



              </div>
            </div>
          </Col>
          <Col md={4} className="topHeaderBox bgCol28 pl-0 pr-0">
            <div className="rightSidebar pl-4 pr-4 pt-4">
              <div className="powerBtn d-flex justify-content-end">
                {/* <div className="mr-4">
                  <Dropdown>
                    <Dropdown.Toggle id="dropdown-basic" className="DropdownType1">
                      <Image src={bellIcon} alt="Search" className="pointer setingOne mt-1" />
                      <span className="fs10 d-inline-block bgCol27 br50 text-center">2</span>
                    </Dropdown.Toggle>
                  </Dropdown>
                </div>
                <Image src={Settings} alt="Search" className="pointer setingOne mr-5" /> */}
                <div>
                  <Dropdown>
                    <Dropdown.Toggle id="dropdown-basic" className="DropdownType1 col2 fs14 fw600">
                      {/* <Image src={UserThree} alt="Search" className="pointer user1 mr-1" /> */}
                      {userData && userData.firstName}
                      <i className="fa fa-chevron-down ml-1 col8" aria-hidden="true"></i>
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                      <Dropdown.Item onClick={() => logoutCall()}>Logout</Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
              </div>
              <div className="mt-5 text-center mb-3">
                {
                  userData.instituteImage ?
                    <Image src={CONSTANTS.CLIENT_SERVER_IMAGEPATH + userData.instituteImage} alt="User" className="br20 mhSet" />
                    :
                    <Image src={'https://edulytics.hiedsuccess.com/logo.png'} alt="User" className="br20 mhSet" />
                }
              </div>
              <div className="text-center mb-4 pb-2">
                <div className="col2 fs22 fw600 mb-2">{userData && userData.instituteName}</div>
                <div className="col29 fw500">
                  {userData && userData.state && userData.state + ', '}
                  United States
                </div>
              </div>
              {/* <div className="boxLayout bgCol30 br4 p-3 mt-3">
                <Row>
                  <Col md={3}>
                    <div className="d-flex justify-content-center align-items-center">
                      <div className="text-center">
                        <div className="col3 fw700 fs24">40%</div>
                        <div className="col3 fs12 fw500">Done</div>
                      </div>
                    </div>
                  </Col>
                  <Col md={9}>
                    <div className="col3 fw400 fs18">A 100% complete profile gets you free access to the first 50 students*</div>
                  </Col>
                </Row>
              </div> */}
            </div>
          </Col>
        </Row>
      </Container>


      <Modal show={loader} centered className="loaderModal"><Loader /></Modal>

    </div>
  )
}
export default FavouriteStudent;




