import React, { useEffect, useState } from 'react';
import { Col, Row, Container, Image, Form, Dropdown, Accordion, Card, Button, Table } from 'react-bootstrap';
import Logoadmin from '../../../../assets/images/svg/logoadmin.svg';
import SearchIcon from '../../../../assets/images/svg/MagnifyingGlass.svg';
import Bookmark from '../../../../assets/images/svg/BookmarkSimple.svg';
import userTwo from '../../../../assets/images/user2.png';
import StarEmpty from '../../../../assets/images/svg/starempty.svg';
import BellOne from '../../../../assets/images/svg/Bell.svg';
import mapOne from '../../../../assets/images/map1.png';
import { Slider, RangeSlider } from 'rsuite';
import moment from 'moment';
import rocketUp from "../../../../assets/images/svg/rocket-up.svg";
import DownOne from "../../../../assets/images/svg/downOne.svg";
import ProImg from "../../../../assets/images/proImg.png";
import { getLocalStorage, clearLocalStorage, setLocalStorage } from '../../../../Lib/Utils';
import { profile, getCollegeProfile, logout } from '../../../../Actions/Student/register';
import footerLogo from '../../../../assets/images/footerlogo.png';
import BackOne from '../../../../assets/images/svg/backs.svg';
import {
  instituteRegistration,
  aboutTheInstitution,
  admissions,
  academicsAndExtracurriculars,
  campusInfrastructure,
  placement,
} from '../wizard/WizardQuestion';
import MyprofileContent from '../../student/myprofile/MyprofileContent';



import CONSTANTS from '../../../../Lib/Constants';

import MyViewField from './MyViewField';



const CollegeProfileContent = (props) => {

  const [details, setDetails] = useState({});
  const [profileData, setProfileData] = useState({});
  const [userData, setUserData] = useState([]);
  const [totalDefth, setTotalDefth] = useState([1, 2, 3, 4, 5, 6, 7]);
  const [totalDefthPrefren, setTotalDefthPrefren] = useState(['Yes', 'No', 'Preferred but optional']);

  const [instituteRegistrationShow, setinstituteRegistrationShow] = useState(true);
  const [aboutTheInstitutionShow, setAboutTheInstitutionShow] = useState(true);
  const [admissionsShow, setAdmissionsShow] = useState(true);
  const [academicsAndExtracurricularsShow, setAcademicsAndExtracurricularsShow] = useState(true);
  const [campusInfrastructureShow, setCampusInfrastructureShow] = useState(true);
  const [placementShow, setPlacementShow] = useState(true);


  useEffect(() => {
    if (props && props.profileData) {
      setProfileData(props.profileData);
    }
  }, [props.profileData])


  return (

    <div className="middileAccordian mb-5">
      <div className="myProfile">

        {props && props.myprofileHeading == false ?

          <div className="d-flex justify-content-between">
            <div className="fs20 fw500 col2 mb-4 pointer" onClick={() => props.pageBack()}>
              <i className="fs20 mr-2 fa fa-chevron-left" aria-hidden="true"></i> Back
            </div>
            {/* <Button className="btnType3 updateBtn">Update</Button> */}
          </div>
          :
          <div className="fs20 fw500 col2 mb-4">College Profile</div>
        }        

        <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1 pr-3">
          <div>Institute Registration</div>
          <div
            onClick={() => setinstituteRegistrationShow(!instituteRegistrationShow)}
            className="pointer"
          >
            <Image src={DownOne} alt="Icon"
              className={`pointer ${instituteRegistrationShow ? 'rotateArro' : ''}`} />
          </div>
        </div>

        {instituteRegistrationShow &&
          <Row className="mb-4">
            <MyViewField
              QuestionData={instituteRegistration}
              AnswerData={profileData && profileData.instituteReg}
            />
          </Row>
        }

        <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1 pr-3">
          <div>About the Institution</div>
          <div
            onClick={() => setAboutTheInstitutionShow(!aboutTheInstitutionShow)}
            className="pointer"
          >
            <Image src={DownOne} alt="Icon"
              className={`pointer ${aboutTheInstitutionShow ? 'rotateArro' : ''}`}
            />
          </div>
        </div>

        <Row className="mb-4">
          {aboutTheInstitutionShow &&
            <MyViewField
              QuestionData={aboutTheInstitution}
              AnswerData={profileData && profileData.aboutTheInstitution}
            />
          }
        </Row>

        <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1 pr-3">
          <div>Admissions</div>
          <div
            onClick={() => setAdmissionsShow(!admissionsShow)}
            className="pointer"
          >
            <Image src={DownOne}
              alt="Icon"
              className={`pointer ${admissionsShow ? 'rotateArro' : ''}`}
            />
          </div>
        </div>

        <Row className="mb-4">
          {admissionsShow &&
            <MyViewField
              QuestionData={admissions}
              AnswerData={profileData && profileData.admissions}
            />
          }
        </Row>

        <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1 pr-3">
          <div>Academics and Extracurriculars</div>
          <div
            onClick={() => setAcademicsAndExtracurricularsShow(!academicsAndExtracurricularsShow)}
            className="pointer"
          >
            <Image src={DownOne} alt="Icon"
              className={`pointer ${academicsAndExtracurricularsShow ? 'rotateArro' : ''}`}
            />
          </div>
        </div>
        {academicsAndExtracurricularsShow &&
          <Row className="mb-4">
            <MyViewField
              QuestionData={academicsAndExtracurriculars}
              AnswerData={profileData && profileData.academicsAndExtracurriculars}
            />
          </Row>
        }

        <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1 pr-3">
          <div>Campus Infrastructure</div>
          <div
            onClick={() => setCampusInfrastructureShow(!campusInfrastructureShow)}
            className="pointer"
          >
            <Image src={DownOne} alt="Icon"
              className={`pointer ${campusInfrastructureShow ? 'rotateArro' : ''}`}
            />
          </div>
        </div>
        {campusInfrastructureShow &&
          <Row className="mb-4">
            <MyViewField
              QuestionData={campusInfrastructure}
              AnswerData={profileData && profileData.campusInfrastructure}
            />
          </Row>
        }


        <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1 pr-3">
          <div>Placement</div>
          <div
            onClick={() => setPlacementShow(!placementShow)}
            className="pointer"
          >
            <Image src={DownOne} alt="Icon"
              className={`pointer ${placementShow ? 'rotateArro' : ''}`}
            />
          </div>
        </div>
        {placementShow &&
          <Row className="mb-4">
            <MyViewField
              QuestionData={placement}
              AnswerData={profileData && profileData.placement}
            />
          </Row>
        }






        {/* <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1 pr-3">
          <div>Plan</div>
          <div
            onClick={() => setPlanShow(!PlanShow)}
            className="pointer"
          >
            <Image
              src={DownOne}
              alt="Icon"
              className={`pointer ${PlanShow ? 'rotateArro' : ''}`}
            />
          </div>
        </div> */}

        {/* {PlanShow &&
          <Row>
            <Col md={5}>
              <div className="PlanList1 planList2 active w-100 bgCol44 cursor-default p-3">  
                <Row className="align-items-center">
                  <Col md={3}>
                    <div className="text-center">
                      <div className="circleType3">
                        <Image src={profileData && profileData.user && profileData.user.plan && profileData.user.plan.icon} alt="Icon" className="mw30" />
                      </div>
                    </div>
                  </Col>
                  <Col md={6}>
                    <div className="mt-2">
                      <div className="col47 fs30 fw500 mb-1 elippse1">{profileData && profileData.user && profileData.user.plan && profileData.user.plan.name}</div>

                    </div>
                  </Col>
                  <Col md={3} className="text-right">
                    <div className="col30 fw600">${profileData && profileData.user && profileData.user.plan && profileData.user.plan.price}</div>
                  </Col>
                </Row>
              </div>
            </Col>
          </Row>
        } */}
      </div>
    </div>

  )
}
export default CollegeProfileContent;






