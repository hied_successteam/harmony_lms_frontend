import React, { useState, useEffect } from 'react';
import { Col, Row, Image, Button, Form, Nav, Tab, Accordion, Card, Table } from 'react-bootstrap';
import { Slider, RangeSlider } from 'rsuite';
import 'rsuite/dist/styles/rsuite-default.css';
import Select from 'react-select';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import CONSTANTS from '../../../../Lib/Constants';

const MyViewField = (props) => {
  return (
    <>
      {
        props && props.QuestionData && props.QuestionData.map((data, index) =>

          <React.Fragment key={index}>

            {data.type == "matrix" ?

              <Col md={12}>
                <div className="mb-4">
                  <div className="col21 fw500 fs15 mb-2">{data.text}</div>
                  <div className="col2 fw500 fs15">
                    <Table bordered className="tableType2 tablePreferences br10 mb-4">
                      <thead>
                        <tr>
                          <th></th>
                          {
                            data.questions && data.questions.map((option, indexNew) => (
                              indexNew == 0 &&
                              option.options && option.options.map((opt, indexNew_1) => (
                                <th key={indexNew_1}>{opt.value}</th>
                              ))
                            ))
                          }
                        </tr>
                      </thead>
                      <tbody>

                        {data.questions && data.questions.map((ques, index) =>

                          <tr key={index}>
                            <td style={{ verticalAlign: 'middle' }}>
                              {ques.text}

                            </td>
                            {ques && ques.options.map((number, index) =>
                              <td key={index}>
                                <Form.Group controlId={`formBasicRadio1${ques.code}${number.itemValue}`}>

                                  <Form.Check
                                    type="radio"
                                    aria-label={ques.text}
                                    className="checkboxTyp3"
                                    id={`formBasicRadio1${ques.code}${number.itemValue}`}
                                    name={ques.code}
                                    label=""
                                    value={number.itemValue || ''}
                                    checked={true}
                                    checked={props.AnswerData && props.AnswerData[ques.code] == number.itemValue}
                                  />
                                </Form.Group>
                              </td>
                            )}
                          </tr>
                        )}

                      </tbody>
                    </Table>
                  </div>
                </div>
              </Col>
              :
              (data.text == "Please share the Social Media links for the following: -" || data.text == "How would you rate the WiFi connectivity on campus?") ? '' :
                <Col md={6}>
                  <div className="mb-4">
                    <div className="col21 fw500 fs15 mb-2">{data.text}</div>
                    <div className="col2 fw500 fs15">
                      {
                        props && data.code && props.AnswerData && props.AnswerData[data.code] ?
                          data.text == 'Gender' ?
                            CONSTANTS.GENDER[props.AnswerData && props.AnswerData[data.code]]
                            :
                            data.text == 'DOB' ?
                              moment(props.AnswerData && props.AnswerData[data.code]).format('DD/MM/YYYY')
                              :
                              Array.isArray(props.AnswerData && props.AnswerData[data.code]) ?

                                props.AnswerData[data.code].join(", ")
                                :
                                props.AnswerData && props.AnswerData[data.code]
                          :
                          '-'

                      }
                    </div>
                  </div>
                </Col>
            }

            { //Second level
            }
            {
              //code for logical questions
              data.isLogical == true ?
                data.options && data.options.map((option, indexNew) =>
                (
                  option.isLogical == true ?

                    <React.Fragment>
                      {
                        data.code == "DEGREECERTIFICATE" && props.AnswerData && props.AnswerData['DEGREECERTIFICATE'] && props.AnswerData['DEGREECERTIFICATE'].length > 0 && props.AnswerData['DEGREECERTIFICATE'].indexOf(option.text) > -1 ?
                          <Col md={6}>
                            {
                              (option.text != "Vocational Training" && option.text != "Summer Internships" && option.text != "Bridge for Bachelors" && option.text != "Bridge for Masters") &&
                              <div className="mb-4">
                                <div className="col21 fw500 fs15 mb-2">{option.heading}</div>
                                <div className="col21 fw500 fs15 mb-2">{option.text}</div>
                                <div className="col2 fw500 fs15">
                                  {
                                    props.AnswerData && props.AnswerData[option.code] ?
                                      Array.isArray(props.AnswerData && props.AnswerData[option.code]) ?
                                        props.AnswerData[option.code].join(", ")
                                        :
                                        (option.type == "date") && props.AnswerData ?
                                          props.AnswerData && props.AnswerData[option.code] &&
                                          moment(props.AnswerData && props.AnswerData[option.code]).format('MM/YYYY')
                                          :
                                          props.AnswerData && props.AnswerData[option.code]
                                      :
                                      '-'
                                  }
                                </div>
                              </div>
                            }
                            
                            {
                              option && option.questions && props.AnswerData && props.AnswerData[data.code] && props.AnswerData[data.code].length >0 && props.AnswerData[data.code].indexOf(option.text) > -1 && (option.text == "Vocational Training" || option.text == "Summer Internships" || option.text == "Bridge for Bachelors" || option.text == "Bridge for Masters") &&
                              option.questions.map((opt_que, opt_index) =>(
                                <div className="mb-4">
                                  <div className="col21 fw500 fs15 mb-2">{opt_que.text}</div>
                                  <div className="col2 fw500 fs15">
                                    {
                                      props.AnswerData && props.AnswerData[opt_que.code] ?
                                        Array.isArray(props.AnswerData && props.AnswerData[opt_que.code]) ?
                                          props.AnswerData[opt_que.code].join(", ")
                                          :
                                          (opt_que.type == "date") && props.AnswerData ?
                                            props.AnswerData && props.AnswerData[opt_que.code] &&
                                            moment(props.AnswerData && props.AnswerData[opt_que.code]).format('MM/YYYY')
                                            :
                                            props.AnswerData && props.AnswerData[opt_que.code]
                                        :
                                        '-'
                                    }
                                  </div>
                                </div>
                              ))
                            }
                          </Col>
                          :
                          data.code == "MODEOFEDUCATION" && option && option.questions && props.AnswerData && props.AnswerData[data.code] && props.AnswerData[data.code].length > 0 && props.AnswerData[data.code].indexOf(option.text) > -1 && option.text == "Online"  ?
                          <Col md={6}>
                              {
                                option.questions.map((opt_que, opt_index) =>(
                                  <div className="mb-4" key={opt_index}>
                                    <div className="col21 fw500 fs15 mb-2">{opt_que.text}</div>
                                    <div className="col2 fw500 fs15">
                                      {
                                        props.AnswerData && props.AnswerData[opt_que.code] ?
                                          Array.isArray(props.AnswerData && props.AnswerData[opt_que.code]) ?
                                            props.AnswerData[opt_que.code].join(", ")
                                            :
                                            props.AnswerData && props.AnswerData[opt_que.code]
                                          :
                                          '-'
                                      }
                                    </div>
                                  </div>
                                ))
                              }
                            </Col>
                          : ''
                      }
                      {
                        props.AnswerData && (props.AnswerData[data.code] == option.value || (props.AnswerData[data.code] && props.AnswerData[data.code].length > 0 && props.AnswerData[data.code].indexOf(option.value) > -1)) ?
                          option.questions && option.questions.map((question, indexSecond) =>
                            <React.Fragment key={indexSecond}>

                              {
                                data.code == "DEGREECERTIFICATE" && (props.AnswerData[option.code] && props.AnswerData[option.code].length>0 && props.AnswerData[option.code].indexOf(question.value) > -1) ?
                                  <React.Fragment>
                                    {
                                      question.isLogical == true &&
                                      question.questions && question.questions.map((lower_que, lower_ind) => {
                                        return <Col md={6} key={lower_ind}>
                                          <div className="mb-4" key={lower_ind}>
                                            <div className="col21 fw500 fs15 mb-2">{lower_ind == 0 && question.heading}</div>
                                            <div className="col21 fw500 fs15 mb-2">{lower_que.text}</div>
                                            <div className="col2 fw500 fs15">
                                              {
                                                props.AnswerData && props.AnswerData[lower_que.code] ?
                                                  Array.isArray(props.AnswerData && props.AnswerData[lower_que.code]) ?
                                                    props.AnswerData[lower_que.code].join(", ")
                                                    :
                                                    (lower_que.type == "date") && props.AnswerData ?
                                                      props.AnswerData && props.AnswerData[lower_que.code] &&
                                                      moment(props.AnswerData && props.AnswerData[lower_que.code]).format('MM/YYYY')
                                                      :
                                                      props.AnswerData && props.AnswerData[lower_que.code]

                                                  :
                                                  '-'
                                              }
                                            </div>
                                          </div>
                                        </Col>
                                      })

                                    }
                                  </React.Fragment>
                                  :
                                  <>
                                    {
                                       index !== 1 && data.section !== "admissions" ?
                                        <Col md={6}>
                                        {console.log('22222',question)}
                                          <div className="mb-4">
                                            <div className="col21 fw500 fs15 mb-2">{question.text}</div>
                                            <div className="col2 fw500 fs15">
                                              {
                                                props.AnswerData && props.AnswerData[question.code] ?
                                                  Array.isArray(props.AnswerData && props.AnswerData[question.code]) ?
                                                    props.AnswerData[question.code].join(", ")
                                                    :
                                                    (question.type == "date") && props.AnswerData ?
                                                      props.AnswerData && props.AnswerData[question.code] &&
                                                      moment(props.AnswerData && props.AnswerData[question.code]).format('DD/MM')
                                                      :
                                                      props.AnswerData && props.AnswerData[question.code]
                                                  :
                                                  '-'
                                              }
                                            </div>
                                          </div>
                                        </Col>
                                        :
                                        <>
                                          {
                                            data.code !== "DEGREECERTIFICATE" && question.text == "Other" && question.text == "Others" &&
                                            <Col md={6}>
                                              {console.log('33333333',question)}
                                              <div className="mb-4">
                                                <div className="col21 fw500 fs15 mb-2">{question.text}</div>
                                                <div className="col2 fw500 fs15">
                                                  {
                                                    props.AnswerData && props.AnswerData[question.code] ?
                                                      Array.isArray(props.AnswerData && props.AnswerData[question.code]) ?
                                                        props.AnswerData[question.code].join(", ")
                                                        :
                                                        (question.type == "date") && props.AnswerData ?
                                                          props.AnswerData && props.AnswerData[question.code] &&
                                                          moment(props.AnswerData && props.AnswerData[question.code]).format('MM/YYYY')
                                                          :
                                                          props.AnswerData && props.AnswerData[question.code]
                                                      :
                                                      '-'
                                                  }
                                                </div>
                                              </div>
                                            </Col>
                                          }

                                        </>
                                    }
                                  </>
                              }

                             
                                {
                                //Third level
                                question.isLogical == true ?
                                  <>                                       
                                    {
                                      question.options && question.options.map((que, indexThird) =>
                                        <>
                                          {
                                            (index == 0 || index == 1) && data.section == "admissions" &&
                                            <React.Fragment key={indexThird}>
                                              <Col md={6}>
                                                <div className="mb-4">
                                                  <div className="col21 fw500 fs15 mb-2">{question.text}</div>
                                                  <div className="col21 fw500 fs15 mb-2">{que.value} - {que.text}</div>
                                                  <div className="col2 fw500 fs15">
                                                    {
                                                      props.AnswerData && props.AnswerData[que.code] ?
                                                        Array.isArray(props.AnswerData && props.AnswerData[que.code]) ?
                                                          props.AnswerData[que.code].join(", ")
                                                          :
                                                          (que.type == "date") && props.AnswerData ?
                                                            props.AnswerData[que.code] &&
                                                            moment(props.AnswerData[que.code]).format('DD/MM')
                                                            :
                                                            props.AnswerData && props.AnswerData[que.code]
                                                        :
                                                        '-'
                                                    }

                                                  </div>
                                                </div>
                                              </Col>
                                            </React.Fragment>
                                          }

                                          {
                                            props.AnswerData && (props.AnswerData[question.code] == que.value || (props.AnswerData[question.code] && props.AnswerData[question.code].length > 0 && props.AnswerData[question.code].indexOf(que.value) > -1)) ?
                                              que.questions && que.questions.map((quesss, indexFour) =>

                                                <React.Fragment key={indexFour}>
                                                  <Col md={6}>
                                                    <div className="mb-4">
                                                      <div className="col21 fw500 fs15 mb-2">{quesss.code == "ONLINE_REMOTELEARNING_OTHEROPTION" ? '' : que.value + ' -'}  {quesss.text}</div>
                                                      <div className="col2 fw500 fs15">
                                                        {
                                                          props.AnswerData && props.AnswerData[quesss.code] ?
                                                            Array.isArray(props.AnswerData && props.AnswerData[quesss.code]) ?
                                                              props.AnswerData[quesss.code].join(", ")
                                                              :
                                                              (question.type == "date") && props.AnswerData ?
                                                                props.AnswerData && props.AnswerData[quesss.code] &&
                                                                moment(props.AnswerData && props.AnswerData[quesss.code]).format('MM/YYYY')
                                                                :
                                                                props.AnswerData && props.AnswerData[quesss.code]
                                                            :
                                                            '-'
                                                        }

                                                      </div>
                                                    </div>
                                                  </Col>
                                                </React.Fragment>
                                              )
                                              :
                                              ''
                                          }
                                        </>
                                      )

                                    }
                                  </>
                                  :
                                  ''
                              }
                            </React.Fragment>
                          )
                          : ''
                      }
                    </React.Fragment>
                    : ''
                ))
                : ''
            }

            {
              data.isLogical == true && data.type != "matrix" ?
                data.questions && data.questions.map((que, indexNew) => (
                  <Col md={6}>
                    <div className="mb-4">
                      <div className="col21 fw500 fs15 mb-2">{que.code == "SPEED" ? "How would you rate the WiFi connectivity on campus?" : que.code == "OFFICIALWEB_LINK" ? "Please share the Social Media links for the following: -" : ''}</div>
                      <div className="col21 fw500 fs15 mb-2">{que.text}</div>
                      <div className="col2 fw500 fs15">
                        {
                          props.AnswerData && props.AnswerData[que.code] ?
                            Array.isArray(props.AnswerData && props.AnswerData[que.code]) ?
                              props.AnswerData[que.code].join(", ")
                              :
                              (que.type == "date") && props.AnswerData ?
                                props.AnswerData && props.AnswerData[que.code] &&
                                moment(props.AnswerData && props.AnswerData[que.code]).format('MM/YYYY')
                                :
                                props.AnswerData && props.AnswerData[que.code]
                            :
                            '-'
                        }
                      </div>
                    </div>
                  </Col>

                ))
                : ''
            }

          </React.Fragment>


        )
      }
    </>
  )

}


export default MyViewField;