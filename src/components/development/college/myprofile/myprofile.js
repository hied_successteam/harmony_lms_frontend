import React, { useEffect, useState } from 'react';
import { Col, Row, Container, Image, Form, Dropdown, Accordion, Card, Button, Table } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import Logoadmin from '../../../../assets/images/svg/logoadmin.svg';
import SearchIcon from '../../../../assets/images/svg/MagnifyingGlass.svg';
import Bookmark from '../../../../assets/images/svg/BookmarkSimple.svg';
import userTwo from '../../../../assets/images/user2.png';
import StarEmpty from '../../../../assets/images/svg/starempty.svg';
import BellOne from '../../../../assets/images/svg/Bell.svg';
import mapOne from '../../../../assets/images/map1.png';
import { Slider, RangeSlider } from 'rsuite';
import moment from 'moment';
import rocketUp from "../../../../assets/images/svg/rocket-up.svg";
import DownOne from "../../../../assets/images/svg/downOne.svg";
import ProImg from "../../../../assets/images/proImg.png";
import { getLocalStorage, clearLocalStorage, setLocalStorage } from '../../../../Lib/Utils';
import { profile, getCollegeProfile, logout } from '../../../../Actions/Student/register';
import footerLogo from '../../../../assets/images/footerlogo.png';
import BackOne from '../../../../assets/images/svg/backs.svg';
import UserOne from '../../../../assets/images/teamtop.png';
import {
  instituteRegistration,
  aboutTheInstitution,
  admissions,
  academicsAndExtracurriculars,
  campusInfrastructure,
  placement,
} from '../wizard/WizardQuestion';
import CollegeProfileContent from './CollegeProfileContent';
import CONSTANTS from '../../../../Lib/Constants';
import MyViewField from './MyViewField';

import { actionLogout } from '../../../../Redux/College/LoginAction';



const MyProfile = (props) => {

  const [details, setDetails] = useState({});
  const [profileData, setProfileData] = useState({});
  const [userData, setUserData] = useState([]);
  const [totalDefth, setTotalDefth] = useState([1, 2, 3, 4, 5, 6, 7]);
  const [totalDefthPrefren, setTotalDefthPrefren] = useState(['Yes', 'No', 'Preferred but optional']);

  const [basicShow, setBasicShow] = useState(true);
  const [academicShow, setAcademicShow] = useState(true);
  const [collegePrenShow, setCollegePrenShow] = useState(true);
  const [demographicShow, setDemographicShow] = useState(true);
  const [EmploymentShow, setEmploymentShow] = useState(true);
  const [PprefenceShow, setPprefenceShow] = useState(true);
  const [FinPrefShow, setFinPrefShow] = useState(true);
  const [PlanShow, setPlanShow] = useState(true);
  const dispatch = useDispatch();

  useEffect(() => {
    var user = getLocalStorage('user');
    if (user && user.id) {
      setUserData(user);
      getCollegeProfile(user.id).then(res => {
        setProfileData(res.data.data);
        setDetails(res.data.data);
      }).catch(err => {
      });
    }
  }, [])

  const backToDashboard = () => {
    props.history.goBack()
  }


  const logoutCall = () => {
    var user = getLocalStorage('user');
    if (user && user.id) {
      logout({
        userId: user.id
      }).then(res => {
        if (res.data.success == true) {
          dispatch(actionLogout('college'));
          localStorage.clear();
          clearLocalStorage();
          props.history.push({ pathname: '/college/login' });
        } else {
          dispatch(actionLogout('college'));
          localStorage.clear();
          clearLocalStorage();
          props.history.push({ pathname: '/college/login' });
        }
      }).catch(err => {
        dispatch(actionLogout('college'));
        localStorage.clear();
        clearLocalStorage();
        props.history.push({ pathname: '/college/login' });
      });
    } else {
      dispatch(actionLogout('college'));
      localStorage.clear();
      clearLocalStorage();
      props.history.push({ pathname: '/college/login' });
    }

  }


  return (
    <div className="collegeAdminUi">
      <Container fluid>
        <Row>
          <Col md={8} className="topHeaderBox bgCol3 pl-0 pr-0">
            <div className="pl-4 pr-4 pt-4">
              <Row className="mb-4">
                <Col md={4}>
                  <Image src={Logoadmin} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" />
                  <span className="fs20 col24 fw700 ml-1">HiEd Harmony</span>
                </Col>
              </Row>
              <div className="col2 fs20 fw500 mt-4 mb-3">
                <div style={{ width: 100, cursor: 'pointer' }} onClick={() => backToDashboard()}>
                  <Image src={BackOne} alt="back" className="pointer mr-2 back1" /> Back
                </div>
              </div>


              <div className="middileAccordian mb-5">
                <CollegeProfileContent
                  {...props}
                  profileData={profileData}
                />
              </div>

            </div>
          </Col>

          <Col md={4} className="topHeaderBox bgCol28 pl-0 pr-0">
            <div className="rightSidebar pl-4 pr-4 pt-4">
              <div className="powerBtn d-flex justify-content-end">
                {/* <div className="mr-4">
                  <Dropdown>
                    <Dropdown.Toggle id="dropdown-basic" className="DropdownType1">
                      <Image src={bellIcon} alt="Search" className="pointer setingOne mt-1" />
                      <span className="fs10 d-inline-block bgCol27 br50 text-center">2</span>
                    </Dropdown.Toggle>
                  </Dropdown>
                </div>
                <Image src={Settings} alt="Search" className="pointer setingOne mr-5" /> */}
                <div>
                  <Dropdown>
                    <Dropdown.Toggle id="dropdown-basic" className="DropdownType1 col2 fs14 fw600">
                      {/* <Image src={UserThree} alt="Search" className="pointer user1 mr-1" /> */}
                      {userData && userData.firstName}
                      <i className="fa fa-chevron-down ml-1 col8" aria-hidden="true"></i>
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                      {/* <Dropdown.Item onClick={() => viewMyProfile()}>Profile</Dropdown.Item> */}
                      <Dropdown.Item onClick={() => logoutCall()}>Logout</Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
              </div>
              <div className="mt-5 text-center mb-3">
                {
                  userData.instituteImage ?
                    <Image src={CONSTANTS.CLIENT_SERVER_IMAGEPATH + userData.instituteImage} alt="User" className="br20 mhSet" />
                    :
                    <Image src={'https://edulytics.hiedsuccess.com/logo.png'} alt="User" className="br20 mhSet" />
                }
              </div>
              <div className="text-center mb-4 pb-2">
                <div className="col2 fs22 fw600 mb-2">{userData && userData.instituteName}</div>
                <div className="col29 fw500">
                  {userData && userData.state && userData.state + ', '}
                  United States
                </div>
              </div>
              {/* <div className="boxLayout bgCol30 br4 p-3 mt-3">
                <Row>
                  <Col md={3}>
                    <div className="d-flex justify-content-center align-items-center">
                      <div className="text-center">
                        <div className="col3 fw700 fs24">40%</div>
                        <div className="col3 fs12 fw500">Done</div>
                      </div>
                    </div>
                  </Col>
                  <Col md={9}>
                    <div className="col3 fw400 fs18">A 100% complete profile gets you free access to the first 50 students*</div>
                  </Col>
                </Row>
              </div> */}
            </div>
          </Col>
        </Row>
      </Container>
      <div className="bottomFooter d-flex align-items-center bgCol43">
        <div className="container-fluid">
          <Row>
            <Col md={12}>
              <div className="d-flex align-items-center">
                <Image src={footerLogo} alt="footer-logo" className="fLogo" />
                <div className="col42 fs13 ml-4">Copyright © {new Date().getFullYear()} HiEd Success | Powered by HiEd Success</div>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  )
}
export default MyProfile;






