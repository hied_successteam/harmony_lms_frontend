import React, { useState, useEffect } from 'react';
import { Col, Row, Container, Image, Form, Button } from 'react-bootstrap';
import { isChrome, 
  //browserName 
} from "react-device-detect";
import Logos from '../../../../assets/images/logos.png';
import { useSelector, useDispatch } from 'react-redux';
import { studentLogin } from '../../../../Redux/College/LoginAction';
import validateInput from '../../../../Lib/Validation/studLoginValidation'
import { setLocalStorage, clearLocalStorage} from '../../../../Lib/Utils'
import Loader from "react-js-loader";  

const CollegeLogin = (props) => {

  //set local variables in state
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errors, setErrors] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [resultError, setResultError] = useState(false);
  const [message, setMessage] = useState('');
  const [key, setKey] = useState(false);

  useEffect(() => {    
    if(!isChrome)
    {
         alert("For better application compatibility, use Google chrome only.")
    } 
}, [])

  //go to Registration page
  const goToSignup = () => {
    props.history.push({ pathname: 'register' });
  }

  const goToForgot = () => {
    props.history.push({ pathname: '/forgotpassword', userType: 'college' });
  }


  // use Redux to store user data
  const dispatch = useDispatch();
  const response = useSelector(state => state.required_data)

  useEffect(() =>{
    localStorage.clear();
    clearLocalStorage();
    setIsLoading(false)
    if(key){
      if(response && response.success == true){   
        setLocalStorage('user', response.data);
        if(response.data.isProfileCompleted && response.data.isProfileCompleted == true){
          props.history.push({pathname: 'dashboard'});
        }else{
          props.history.push({pathname: 'wizard'});
        }
        // props.history.push({pathname: '/college/wizard'});
      }else{
        if(response && response.success == false){
          setIsLoading(false)
          setResultError(true)
          setMessage(response.message)
          setTimeout(() => {
            setResultError(false)
            setMessage('')
          },4000);
        }
      }
    }
  // }, [response])
  },[key, response, props.history])


  useEffect(() =>{
    localStorage.clear();
    clearLocalStorage();
    setResultError(false)
    setMessage('')
  }, [])




  //call login api to get result
  const loginApiCall = () => {
    localStorage.clear()
    if (isValid()) {
      setIsLoading(true)
      setErrors({})
      dispatch(studentLogin(email, password, 'college')) 

      setTimeout(function () {
        setKey(true)
        setIsLoading(false)
      }.bind(this),2000);
    }
  }

  //check input validation
  const isValid = () => {
    const { errors, isValid } = validateInput(email, password);
    if (!isValid) {
      setErrors(errors)
    }
    return isValid;
  }

  //go to landing page
  const goToLanding = () =>{
    props.history.push({pathname: '/landing/dashboard'});
  }

  const onKeyPress = (e) => {     
    if(e.which == 13) {
      loginApiCall();
    }
  }

  return (
    <div className="bgBanner d-flex align-items-center justify-content-center w-100">
      {/* <div className="topBox bgCol1">
          For better application compatibility, use Google chrome only. 
      </div>  */}
      <Container>
        <Col lg={10} md={10} className="m-auto">
          <div className="studentBox text-center bgCol3 shadow1 br10 registerUsers">
            <div> 
            <Image src={Logos} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" onClick={() => goToLanding()} />
            <div className="studentOne mt-4 text-left">
              <div className="fs22 fw600 col2 mb-1 text-center">Sign In</div>
              <div className="fw500 col5 mb-4 pb-2 text-center">Welcome to your account</div>

              {
                resultError &&
                <div className="fw500 col5 mb-4 pb-2 text-center" style={{ color: 'red' }}>{message}</div>
              }
              <Form className="formLayoutUi">
                <Row>
                  <Col md={6}>
                    <Form.Group controlId="formBasicEmailA">
                      <Form.Control
                        type="email"
                        placeholder="Email" className="inputType1"
                        value={email}
                        onKeyPress={(e) => onKeyPress(e)}
                        onChange={(e) => setEmail(e.target.value)} />
                      {
                        errors.email &&
                        <span className="help-block error-text">{errors.email}</span>
                      }
                    </Form.Group>
                  </Col>
                  <Col md={6}> 
                      <Form.Group controlId="formBasicEmailB" className="mb-2">
                          <Form.Control
                            type="password"
                            placeholder="Password"
                            className="inputType1"
                            value={password}
                            onKeyPress={(e) => onKeyPress(e)}
                            onChange={(e) => setPassword(e.target.value)} />
                          {
                            errors.password &&
                            <span className="help-block error-text">{errors.password}</span>
                          }
                        </Form.Group>
                        <div className="text-right col1 fs18 fw600 mb-4 pb-2 pointer" onClick={() => goToForgot()}>
                          Forgot Password?
                        </div>
                  </Col>
                  <Col md={12}>
                    <div className="text-center align-xs-center">
                      <Button className="btnType4 fw600 mr-4 mb-4" onClick={() => loginApiCall()} > 
                        {
                          isLoading ?
                            <Loader type="bubble-top" bgColor={"#414180"} size={25} />
                            :
                            "Sign In"
                        }
                      </Button>
                      <div className="fw500 col5">Don’t have an account?
                        <span className="col1 fw700 pointer ml-2" onClick={() => goToSignup()}>Sign Up</span></div>
                    </div>
                  </Col>
                </Row>
              </Form>
            </div>
            </div>
          </div>
        </Col>
      </Container>
    </div>
  )
}

export default CollegeLogin;  