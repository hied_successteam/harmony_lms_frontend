import React, { useEffect, useState } from 'react';
import { Col, Row, Container, Image, Form, Dropdown, Accordion, Card, Button, Table } from 'react-bootstrap';
import Logoadmin from '../../../../assets/images/svg/logoadmin.svg';
import SearchIcon from '../../../../assets/images/svg/MagnifyingGlass.svg';
import Bookmark from '../../../../assets/images/svg/BookmarkSimple.svg';
import userTwo from '../../../../assets/images/user2.png';
import StarEmpty from '../../../../assets/images/svg/starempty.svg';
import BellOne from '../../../../assets/images/svg/Bell.svg';
import mapOne from '../../../../assets/images/map1.png';
import { Slider, RangeSlider } from 'rsuite';
import moment from 'moment';
import rocketUp from "../../../../assets/images/svg/rocket-up.svg";
import DownOne from "../../../../assets/images/svg/downOne.svg";
import ProImg from "../../../../assets/images/proImg.png";
import { getLocalStorage, clearLocalStorage, setLocalStorage } from '../../../../Lib/Utils';
import { profile, getprofile, logout } from '../../../../Actions/Student/register';
import footerLogo from '../../../../assets/images/footerlogo.png';
import BackOne from '../../../../assets/images/svg/backs.svg';
import {
  BasicQuestions,
  AcademicInfo,
  CollegePreference,
  DemoGraphicPreference,
  ClimatePreference,
  LocationPreference,
  EmploymentInfo,
  PLACEMENTPREFRENCEPRE,
  FINANCIALINFO,
  FINANCIALPREFRENCE,
} from '../../student/wizard/WizardQuestion';



import CONSTANTS from '../../../../Lib/Constants';

import MyViewField from './MyViewField';



const MyProfile = (props) => {

  const [details, setDetails] = useState({});
  const [profileData, setProfileData] = useState({});
  const [userData, setUserData] = useState([]);
  const [totalDefth, setTotalDefth] = useState([1, 2, 3, 4, 5, 6, 7]);
  const [totalDefthPrefren, setTotalDefthPrefren] = useState(['Yes', 'No', 'Preferred but optional']);

  const [basicShow, setBasicShow] = useState(true);
  const [academicShow, setAcademicShow] = useState(true);
  const [collegePrenShow, setCollegePrenShow] = useState(true);
  const [demographicShow, setDemographicShow] = useState(true);
  const [EmploymentShow, setEmploymentShow] = useState(true);
  const [PprefenceShow, setPprefenceShow] = useState(true);
  const [FinPrefShow, setFinPrefShow] = useState(true);
  const [PlanShow, setPlanShow] = useState(true);

  useEffect(() => {
    window.scrollTo(0, 0);
    var user = getLocalStorage('user');
    var studentDetails = getLocalStorage('stdDetail');
    if(studentDetails){
      setProfileData(studentDetails);
      setDetails(studentDetails);
    }
  }, [])

  const backToDashboard = () =>{
    props.history.goBack()
  }


  const logoutCall = () => {
    var user = getLocalStorage('user');

    if (user && user.id) {
      logout({
        userId: user.id
      }).then(res => {
        if (res.data.success == true) {
          localStorage.clear();
          clearLocalStorage();
          props.history.push({ pathname: '/college/login' });
        }
      }).catch(err => {
      });
    } else {
      props.history.push({ pathname: '/college/login' });
    }

  }


  return (
    <div className="collegeAdminUi">
      <Container fluid>
        <Row>
          <Col md={8} className="topHeaderBox bgCol3 pl-0 pr-0">
            <div className="pl-4 pr-4 pt-4">
            <Row className="mb-4">
                <Col md={4}>
                  <Image src={Logoadmin} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" />
                  <span className="fs20 col24 fw700 ml-1">HiEd Harmony</span>
                </Col>
                {/* <Col md={5}>
                  <div className="searchBox d-flex justify-content-center">
                    <Form.Group className="position-relative w-100">
                      <Form.Control type="text" placeholder="Search..." className="searchType1 shadow3 w-100" />
                      <Image src={SearchIcon} alt="Search" className="pointer searchOne" />
                    </Form.Group>
                  </div>
                </Col> */}
                {/* <Col md={3}>
                  <div className="notifications d-flex justify-content-end">
                    <div className="bookMark ml-4">
                      <span className="d-inline-block mr-3 text-center position-relative">
                        <Image src={BellOne} alt="Bell" className="bell1 pointer" />
                        <span className="bugdeTwo bgCol27 br50 fs10 col3 fw500">2</span>
                      </span>
                      <span className="d-inline-block bgCol1 br50 text-center">
                        <Image src={Bookmark} alt="BookMark" className="pointer" />
                      </span>
                    </div>
                  </div>
                </Col> */}
              </Row>
              <div className="col2 fs20 fw500 mt-4 mb-3">
                <div style={{width: 100, cursor: 'pointer'}} onClick={() => backToDashboard()}>
                <Image src={BackOne} alt="back" className="pointer mr-2 back1"  /> Back
                </div>
              </div>
              <div className="middileAccordian mb-5">
                <div className="myProfile">
                  <div className="fs20 fw500 col2 mb-4">My Profile</div>

                  <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1 pr-3">
                    <div>Basic</div>
                    <div
                      onClick={() => setBasicShow(!basicShow)}
                      className="pointer"
                    >
                      <Image src={DownOne} alt="Icon"
                        className={`pointer ${basicShow ? 'rotateArro' : ''}`} />
                    </div>
                  </div>

                  {basicShow &&
                    <Row className="mb-4">
                      <MyViewField
                        QuestionData={BasicQuestions}
                        AnswerData={profileData && profileData.basic}
                      />
                    </Row>
                  }

                  <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1 pr-3">
                    <div>Academic Info</div>
                    <div
                      onClick={() => setAcademicShow(!academicShow)}
                      className="pointer"
                    >
                      <Image src={DownOne} alt="Icon"
                        className={`pointer ${academicShow ? 'rotateArro' : ''}`}
                      />
                    </div>
                  </div>

                  <Row className="mb-4">
                    {academicShow &&
                      <MyViewField
                        QuestionData={AcademicInfo}
                        AnswerData={profileData && profileData.academicInfo}
                      />
                    }
                  </Row>

                  <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1 pr-3">
                    <div>College Preferences</div>
                    <div
                      onClick={() => setCollegePrenShow(!collegePrenShow)}
                      className="pointer"
                    >
                      <Image src={DownOne}
                        alt="Icon"
                        className={`pointer ${collegePrenShow ? 'rotateArro' : ''}`}
                      />
                    </div>
                  </div>

                  <Row className="mb-4">
                    {collegePrenShow &&
                      <MyViewField
                        QuestionData={CollegePreference}
                        AnswerData={profileData && profileData.collegePreference}
                      />
                    }
                  </Row>

                  <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1 pr-3">
                    <div>Demographic Preferences</div>
                    <div
                      onClick={() => setDemographicShow(!demographicShow)}
                      className="pointer"
                    >
                      <Image src={DownOne} alt="Icon"
                        className={`pointer ${demographicShow ? 'rotateArro' : ''}`}
                      />
                    </div>
                  </div>
                  {demographicShow &&
                    <Row className="mb-4">
                      <MyViewField
                        QuestionData={DemoGraphicPreference}
                        AnswerData={profileData && profileData.demographicPreference}
                      />
                      <MyViewField
                        QuestionData={ClimatePreference}
                        AnswerData={profileData && profileData.demographicPreference}
                      />
                      <MyViewField
                        QuestionData={LocationPreference}
                        AnswerData={profileData && profileData.demographicPreference}
                      />
                    </Row>
                  }

                  <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1 pr-3">
                    <div>Employment Info</div>
                    <div
                      onClick={() => setEmploymentShow(!EmploymentShow)}
                      className="pointer"
                    >
                      <Image src={DownOne} alt="Icon"
                        className={`pointer ${EmploymentShow ? 'rotateArro' : ''}`}
                      />
                    </div>
                  </div>
                  {EmploymentShow &&
                    <Row className="mb-4">
                      <MyViewField
                        QuestionData={EmploymentInfo}
                        AnswerData={profileData && profileData.employmentInfo}
                      />
                    </Row>
                  }

                  <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1 pr-3">
                    <div>Placement Preferences</div>
                    <div
                      onClick={() => setPprefenceShow(!PprefenceShow)}
                      className="pointer"
                    >
                      <Image src={DownOne} alt="Icon"
                        className={`pointer ${PprefenceShow ? 'rotateArro' : ''}`}
                      />
                    </div>
                  </div>
                  {PprefenceShow &&
                    <Row className="mb-4">
                      <MyViewField
                        QuestionData={PLACEMENTPREFRENCEPRE}
                        AnswerData={profileData && profileData.placementPreference}
                        totalDefth={totalDefthPrefren}
                      />
                    </Row>
                  }

                  <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1 pr-3">
                    <div> Financial Preferences</div>
                    <div
                      onClick={() => setFinPrefShow(!FinPrefShow)}
                      className="pointer"
                    >
                      <Image src={DownOne} alt="Icon"
                        className={`pointer ${FinPrefShow ? 'rotateArro' : ''}`}
                      />
                    </div>
                  </div>
                  {FinPrefShow &&
                    <Row className="mb-4">
                      <MyViewField
                        QuestionData={FINANCIALINFO}
                        AnswerData={profileData && profileData.finalPreference}
                        totalDefth={totalDefth}
                      />
                      <MyViewField
                        QuestionData={FINANCIALPREFRENCE}
                        AnswerData={profileData && profileData.finalPreference}
                        totalDefth={totalDefth}
                      />
                    </Row>
                  }


                  <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1 pr-3">
                    <div>Plan</div>
                    <div
                      onClick={() => setPlanShow(!PlanShow)}
                      className="pointer"
                    >
                      <Image
                        src={DownOne}
                        alt="Icon"
                        className={`pointer ${PlanShow ? 'rotateArro' : ''}`}
                      />
                    </div>
                  </div>

                  {PlanShow &&
                    <Row>
                      <Col md={5}>
                        <div className="PlanList1 planList2 active w-100 bgCol44 cursor-default p-3">  
                          <Row className="align-items-center">
                            <Col md={3}>
                              <div className="text-center">
                                <div className="circleType3">
                                  <Image src={profileData && profileData.user && profileData.user.plan && profileData.user.plan.icon} alt="Icon" className="mw30" />
                                </div>
                              </div>
                            </Col>
                            <Col md={6}>
                              <div className="mt-2">
                                <div className="col47 fs30 fw500 mb-1 elippse1">{profileData && profileData.user && profileData.user.plan && profileData.user.plan.name}</div>

                              </div>
                            </Col>
                            <Col md={3} className="text-right">
                              <div className="col30 fw600">${profileData && profileData.user && profileData.user.plan && profileData.user.plan.price}</div>
                            </Col>
                          </Row>
                        </div>
                      </Col>
                    </Row>
                  }
                </div>
              </div>
            </div>
          </Col>

          <Col md={4} className="topHeaderBox bgCol28 pl-0 pr-0">
            <div className="rightSidebar fixedSidebar pl-4 pr-4 pt-4 mb-5">
              <div className="d-flex justify-content-start mb-3">
                <Row className="w-100">
                  {/* <Col md={3} className="pr-0">
                    <Image src={userTwo} alt="User" />
                  </Col> */}
                  <Col md={9}>
                    <div className="col2 fw600 fs20 mt-1 mb-1">{profileData && profileData.user && profileData.user.firstName + " " + profileData.user.lastName}</div>
                    <div className="col5 fw500">{profileData && profileData.user && profileData.user.email}</div>
                    <div className="col5 fw500 mb-3">
                      {profileData && profileData.basic && profileData.basic.CITY && profileData.basic.CITY + ', '}
                      {profileData && profileData.basic && profileData.basic.STATE + ', '} 
                      {profileData && profileData.basic && profileData.basic.COUNTRY}
                    </div>
                  </Col>
                </Row>
                <div>
                  <Dropdown className="DropdownType2">
                    <Dropdown.Toggle id="dropdown-basic" className="DropdownType1 col2 fs14 fw600">
                      <span className="circleType3 d-inline-block br50 text-center">
                        <i className="fa fa-chevron-down col1" aria-hidden="true"></i>
                      </span>
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                      {/* <Dropdown.Item onClick={() => editProfile()}>Edit Profile</Dropdown.Item>
                      <Dropdown.Item href="#">Settings</Dropdown.Item>
                      <Dropdown.Item href="#">Help</Dropdown.Item> */}
                      <Dropdown.Item onClick={() => logoutCall()}>Logout</Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
              </div>

              <div className="profileBox1 bgCol3 p-4 mb-4">
                <Image src={ProImg} alt="Profile" className="w-100 mb-4" />
                <div className="col2 fs20 fw600 mb-4">
                  If you would like to edit your profile, contact us at</div>
                <div className="col8 fs20 fw600">    
                  <a className="textcol8 fs20 fw600 mb-1" href={`mailto:${"info@hiedsuccess.com"}`}>info@hiedsuccess.com</a>
                  </div>
              </div>

            </div>
          </Col>
        </Row>
      </Container>
      <div className="bottomFooter d-flex align-items-center bgCol43">
        <div className="container-fluid">
          <Row>
            <Col md={12}>
              <div className="d-flex align-items-center">
                <Image src={footerLogo} alt="footer-logo" className="fLogo" />
                <div className="col42 fs13 ml-4">Copyright © {new Date().getFullYear()} HiEd Success | Powered by HiEd Success</div>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  )
}
export default MyProfile;  






