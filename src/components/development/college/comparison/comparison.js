import React, { useState, useEffect } from 'react';
import { Col, Row, Container, Image, Form, Dropdown, Accordion, Card, Button, Table, Modal } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import Logoadmin from '../../../../assets/images/svg/logoadmin.svg';
import bellIcon from '../../../../assets/images/svg/Bell.svg';
import Settings from '../../../../assets/images/svg/GearSix.svg';
import UserThree from '../../../../assets/images/user3.png';
import BackOne from '../../../../assets/images/svg/backs.svg';
import AccorIcon from '../../../../assets/images/accoricon.png';
import saveOne from '../../../../assets/images/svg/save1.svg';
import Prints from '../../../../assets/images/svg/prints.svg';
import heartfill from '../../../../assets/images/svg/heartfill.svg';
import hearts from '../../../../assets/images/svg/hearts.svg';
import TrashTwo from '../../../../assets/images/svg/Trash2.svg';
import Trash from '../../../../assets/images/svg/Trash.svg';
import Stars from '../../../../assets/images/svg/stars.svg';
import StarEmpty from '../../../../assets/images/svg/starempty.svg';
import WomanIcon from '../../../../assets/images/svg/woman1.svg';
import BoyIcon from '../../../../assets/images/svg/boy1.svg';
import ProgressOne from '../../../../assets/images/svg/progress1.svg';
import ProgressRed from '../../../../assets/images/svg/progressRed.svg';
import ProgressYellow from '../../../../assets/images/svg/progressYellow.svg';
import { render } from 'react-dom';
import { getLocalStorage, clearLocalStorage, setLocalStorage } from '../../../../Lib/Utils';
import { logout, saveStudentComparison, getStudentComparison, getStudentList, studentFavourite, deleteComparisonList } from '../../../../Actions/Student/register';
import moment from 'moment'
import ReactStars from "react-rating-stars-component";
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import TipModal from '../../../development/TipModal/TipModal'
import { actionLogout } from '../../../../Redux/College/LoginAction';
import Loader from '../../../../Lib/LoaderNew';
import AwardIcon from '../../../../assets/images/awardIcon.png'; 


const Comparison = (props) => {
  const [show, setShow] = useState(false);
  const [listData, setListData] = useState(false);
  const [user, setUser] = useState(false);
  const [compareTitle, setCompareTitle] = useState('');
  const [titleError, setTitleError] = useState(false);
  const [saveSuccessMsg, setSaveSuccessMsg] = useState('');
  const [savedData, setSavedData] = useState('');
  const [compareUpdateID, setCompareUpdateID] = useState('');
  const [localFavourite, setLocalFavourite] = useState('');
  const [loader, setLoader] = useState(false);
  const [deleteMsg, setDeleteMsg] = useState(false);
  const [nameExist, setNameExist] = useState(false);
  const dispatch = useDispatch();


  const handleClose = () => setShow(false);
  const handleShow = () => {
    setTitleError(false)
    setShow(true);
  }

  const [tipsDataDelete, setTipsDataDelete] = useState(false);
  const [showDelete, setShowDelete] = useState(false);

  const handleCloseDelete = () => setShowDelete(false);

  const handleShowDelete = (tipsDataDelete) => {
    setTipsDataDelete(tipsDataDelete);
    setShowDelete(true);
  }

  useEffect(() =>{
    setCompareUpdateID('')
    var user = getLocalStorage('user');
    if(user.favourite){
      setLocalFavourite(user.favourite)
    }else{
      user.favourite = []
      setLocalStorage('user', user)
    }
    getStudList()
    
  },[])

  const getStudList = () =>{
    var user = getLocalStorage('user');
    setUser(user)
    if(props && props.location.data){
      var params = props.location.data.toString()
      setLoader(true)
      getStudentList(params).then(res => {
        if(res && res.data && res.data.success == true){
          setLoader(false)
          setListData(res.data.data.rows) 
        }else{
          setLoader(false)
        }
      }).catch(err =>{
        setLoader(false)
      })
      getComparison()
    }
  }


  const getComparison = (status) =>{
    getStudentComparison().then(res => {
      if(res && res.data.data && res.data.data.rows){
        setSavedData(res.data.data.rows) 
        if(status == "resume"){
          if(res.data.data.rows.length > 0 && res.data.data.rows[0].studentDetail){
            setListData(res.data.data.rows[0].studentDetail)
          }else{
            props.history.goBack()
          }
        }
      }
    }).catch(err => { });
  }


  const saveComparison = () =>{
    var data = []
    listData && listData.map((item) =>{
      data.push(item.user._id)
    })

    if(compareUpdateID){
      var payload = {
        title: compareTitle,
        student: data,
        comparisonId: compareUpdateID
      }
    }else{
      var payload = {
        title: compareTitle,
        student: data
      }
    }

    if(compareTitle && data){
      saveStudentComparison(payload).then(res => {
        setShow(false)
        if(res && res.data.message){
          setSaveSuccessMsg(res.data.message)
          setNameExist(res.data.success)
          getComparison()
          setTimeout(() => {
            setSaveSuccessMsg('')
            setNameExist('')
          },5000);
        } 
      }).catch(err => {   });
    }else{
      setTitleError(true)
    }
    
  }


  useEffect(() =>{ 
    if(props && props.location.data){
      setListData(props.location.data)
    }
  },[props.location && props.location.data])

  const viewMyProfile = () =>{
    props.history.push({ pathname: '/college/myProfile'});
  }

  const logoutCall = () => {    
    var user = getLocalStorage('user');
    if (user && user.id) {
      logout({
        userId: user.id
      }).then(res => {
        if (res.data.success == true) {
          dispatch(actionLogout('college'));
          localStorage.clear();
          clearLocalStorage();
          props.history.push({ pathname: '/college/login' });
        }else
        {
          dispatch(actionLogout('college'));
        }
      }).catch(err => {
        dispatch(actionLogout('college'));
      });
    } else {
      dispatch(actionLogout('college'));
      props.history.push({ pathname: '/college/login' });
    }
  }


  const backToDashboard = () =>{
    props.history.goBack()
  }

  const savedList = (data) =>{
    setCompareUpdateID(data._id)
    // var newArr = []
    // data.college && data.college.map((item_1, index) =>{
    //   JSON.parse(localStorage.getItem("listData")) &&
    //   JSON.parse(localStorage.getItem("listData")).map((item_2) =>{
    //     if(item_1 == item_2.UNITID){
    //       newArr.push(item_2)
    //     }
    //   })
    // })

    // if(newArr && newArr.length){
    //   setListData(data.studentDetail)
    // }

    setListData(data.studentDetail)
  }

  const deleteStudent = (data) =>{
    if(listData.length > 2){
      let ArryVale = listData;
      var indexB = ArryVale.indexOf(data);
      if (indexB > -1) {
        ArryVale.splice(indexB, 1);
        setListData([...ArryVale])
      }
    }else{
      handleShowDelete("List of comparison should be minimum 2 students")
    }
  }


  const favoriteApiCall = (id, type) => {
    setLoader(true)
    studentFavourite({ favouriteId: id, type }).then(res => {
      setLoader(false)
      if (res.data.success == true) {
        var data = getLocalStorage('user')
        if(type == true){
          if (data.favourite) {
            data.favourite.push(id)
          } else {
            data.favourite = [id]
          }
          setLocalFavourite(data.favourite)
          setLocalStorage('user', data);
        }else{
          var data = getLocalStorage('user')
          if (data && data.favourite && data.favourite.length > 0) {
            let ArryVale = data.favourite;
            var indexB = ArryVale.indexOf(id);
            if (indexB > -1) {
              ArryVale.splice(indexB, 1);
              data.favourite = [...ArryVale]
              setLocalFavourite(data.favourite)
              setLocalStorage('user', data);
            }
          }
        }
        //getStudList()
         
      }

    }).catch(err => {setLoader(false) })
  }

  const manageFavouriteStatus = (id) => {
    var userFavourite = localFavourite
    if (userFavourite && userFavourite.length) {
      var response = userFavourite.find(element => element == id)
      if (response) {
        return true
      } else {
        return false
      }
    } else {
      return false
    }
  }

  const deleteList = (id) =>{
    setLoader(true)
    deleteComparisonList(id).then(res =>{ 
      setLoader(false)
      if(res && res.data && res.data.success){
        getComparison("resume")
        setDeleteMsg(res.data.message)
        setTimeout(() => {
          setDeleteMsg('')
        },5000);
      }
    }).catch(err => {setLoader(false)})
  }


  return (
    <div className="collegeAdminUi">
      <Container fluid> 
        <div className="pl-2 pr-2 pt-4">
          <Row>
            <Col md={3}>
              <Image src={Logoadmin} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" />
              <span className="fs20 col24 fw700 ml-1">HiEd Harmony</span>
            </Col>
            <Col md={6}></Col>
            <Col md={3}>
              <div className="powerBtn d-flex justify-content-end">
                {/* <div className="mr-4">
                  <Dropdown>
                    <Dropdown.Toggle id="dropdown-basic" className="DropdownType1">
                      <Image src={bellIcon} alt="Search" className="pointer setingOne mt-1" />
                    </Dropdown.Toggle>
                  </Dropdown>
                </div> */}
                {/* <Image src={Settings} alt="Search" className="pointer setingOne mr-5" /> */}
                <div style={{paddingTop: 6}}>
                  <Dropdown>
                    <Dropdown.Toggle id="dropdown-basic" className="DropdownType1 col2 fs14 fw600">
                      {/* <Image src={UserThree} alt="Search" className="pointer user1 mr-1" /> */}
                      {user.firstName + " " + user.lastName}   <i className="fa fa-chevron-down ml-1 col8" aria-hidden="true"></i>
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                      {/* <Dropdown.Item onClick={() => viewMyProfile()}>Profile</Dropdown.Item> */}
                      {/* <Dropdown.Item href="#">Setting</Dropdown.Item> */}
                      <Dropdown.Item onClick={() => logoutCall()}>Logout</Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
              </div>
            </Col>
          </Row>
          <div className="col2 fs20 fw500 mt-4 mb-3">
            <div style={{width: 100, cursor: 'pointer'}} onClick={() => backToDashboard()}>
            <Image src={BackOne} alt="back" className="pointer mr-2 back1"  /> Back
            </div>
          </div> 
          {
            saveSuccessMsg && 
            <div className="fw500 col5 mb-4 pb-2 text-center" style={nameExist == true ? { color: 'green' } : { color: 'red' }}>{saveSuccessMsg}</div> 
          }
          {
            deleteMsg && 
            <div className="fw500 col5 mb-4 pb-2 text-center" style={{ color: 'green' }}>{deleteMsg}</div> 
          }
          <div className="d-flex flex-wrap justify-content-between" style={{marginTop: 10, marginBottom: 10}}>
            <div>
              <Dropdown className="dropdownBox1">
                <Dropdown.Toggle id="dropdown-basic" className="DropdownType4 col2 fw600 fs20">
                  Saved Comparison <Image src={AccorIcon} alt="back" className="pointer ml-1" />
                </Dropdown.Toggle>
                <Dropdown.Menu className="shadow5">
                  {
                    savedData && savedData.length > 0 ?
                    savedData.map((item, ind) =>{
                      return <Dropdown.Item key={ind}>
                        <Row>
                          <Col md={10} onClick={() => savedList(item)}>
                            <div className="fs22 fw500 col2">{item.title}</div>
                            <div className="fs14 fw400 col5">Created {moment(item.updatedAt).format("MMM Do YY")}</div>
                          </Col>
                          <Col md={2}>
                            <Image src={Trash} alt="Trash" className="pointer grayCol" onClick={() => deleteList(item._id)} />
                          </Col>
                        </Row>
                      </Dropdown.Item>
                    })
                    :
                    <Dropdown.Item >
                      <div className="fs20 fw500 col5">No data found</div>
                    </Dropdown.Item>
                  } 
                </Dropdown.Menu>
              </Dropdown>
            </div>
          
            
            <div className="d-flex">
              <div className="col8 fw600 fs20 mr-4 pointer" onClick={handleShow}>Save Comparison</div>
                {/* <Image src={saveOne} alt="back" className="pointer save1 mr-2" />
                <Image src={Prints} alt="back" className="pointer save1" /> */}
            </div>
          </div>
        </div>
        <div className="bgCol4 comparisonType1 mb-4">
          <Row>
            <Col md={2}></Col>
            <Col md={10} className="pl-5">
              <Row>
                {
                  listData && listData.map((item, index) =>{
                    return <Col md={12 / listData.length} key={index}>          
                      <div className="bgCol39 br10 compareBox1">  
                        <div className="p-3">   
                          <div className="w-100">   
                            <div className="col3 fs15 fw600">{item.basic && item.basic.NAME ? item.basic.NAME : ''}<span className="ml-3"></span></div>
                            <div className="col3 fs14 fw400">
                              {item.basic && item.basic.CITY && item.basic.CITY + ', '}
                              {item.basic && item.basic.STATE && item.basic.STATE + ', '}
                              {item.basic && item.basic.COUNTRY && item.basic.COUNTRY}
                            <span className="ml-3"></span></div>
                          </div>
                        </div>
                        <div className="d-flex align-items-center bgCol1 compareBox2">
                          {
                            item && item.user && item.user._id &&
                            manageFavouriteStatus(item.user._id) ?
                              <Image src={heartfill} alt="Book Mark" className="pointer mr-4" onClick={() => favoriteApiCall(item.user._id, false)}/>
                              :
                              <Image src={hearts} alt="Book Mark" className="pointer mr-4" onClick={() => favoriteApiCall(item.user._id, true)}/>
                          }
                          
                          <Image src={TrashTwo} alt="Trash" className="pointer" onClick={() => deleteStudent(item)}/>
                        </div>
                      </div>
                    </Col>
                  })
                }
              </Row>
            </Col>
          </Row>
        </div>
        <div className="comparisonType4 br10">  
          <Table striped bordered className="tableType1 br10">
            <tbody>
              <tr>
                <td><div className="mb-2 fs18 fw600 col2">FIRST LANGUAGE</div></td>
                {
                  listData && listData.map((item, index) =>{
                    return <td key={index}><div className="fw500 col2 mb-2">{item.academicInfo && item.academicInfo.NATIVELANGUAGE ? item.academicInfo.NATIVELANGUAGE : '-'}</div></td>
                  })
                }
              </tr>
              <tr>
                <td>
                  <div className="mb-2 fs18 fw600 col2">Scores & Ranks</div>
                  <div className="fw500 col2 mb-2">SAT</div>
                  <div className="fw500 col2 mb-2">TOEFL</div>
                  <div className="fw500 col2 mb-2">PSAT/NMSQT</div>
                  <div className="fw500 col2 mb-2">GRE</div>
                  <div className="fw500 col2 mb-2">GMAT</div>
                  <div className="fw500 col2 mb-2">IELTS</div>
                </td>
                {
                  listData && listData.map((item, index) =>{
                    return <React.Fragment key={index}>
                        <td key={index}>
                          <div className="fw500 col2 mb-2" style={{height: 23}}></div>
                          {
                            <div className="fw500 col2 mb-2">{item.academicInfo && item.academicInfo.SAT_TOTAL ? item.academicInfo.SAT_TOTAL : '-'}</div>
                          }
                          {
                            <div className="fw500 col2 mb-2">{item.academicInfo && item.academicInfo.TOEFL_TOTAL ? item.academicInfo.TOEFL_TOTAL : '-'}</div>
                          }
                          {
                            <div className="fw500 col2 mb-2">{item.academicInfo && item.academicInfo.PSAT_NMSQT_TOTAL ? item.academicInfo.PSAT_NMSQT_TOTAL : '-'}</div>
                          }
                          {
                            <div className="fw500 col2 mb-2">{item.academicInfo && item.academicInfo.BACH_GRE_TOTAL ? item.academicInfo.BACH_GRE_TOTAL : '-'}</div>
                          }
                          {
                            <div className="fw500 col2 mb-2">{item.academicInfo && item.academicInfo.BACH_GMAT_Total ? item.academicInfo.BACH_GMAT_Total : '-'}</div>
                          }
                          {
                            <div className="fw500 col2 mb-2">{item.academicInfo && item.academicInfo.IELTS_BANDSCORE ? item.academicInfo.IELTS_BANDSCORE : '-'}</div>
                          }
                        </td>
                      </React.Fragment>
                  })
                }
              </tr>

              <tr>
                <td><div className="mb-2 fs18 fw600 col2">HIGHEST DEGREE COMPLETED</div></td>
                {
                  listData && listData.map((item, index) =>{
                    return <td key={index}><div className="fw500 col2 mb-2">{item.academicInfo && item.academicInfo.HIGHDEGEDU ? item.academicInfo.HIGHDEGEDU : '-'}</div></td>
                  })
                }
              </tr>

              <tr>
                <td><div className="mb-2 fs18 fw600 col2">GRADING SYSTEM</div></td>
                {
                  listData && listData.map((item, index) =>{
                    return <td key={index}><div className="fw500 col2 mb-2">{item.academicInfo && item.academicInfo.GRADINGSYSTEM ? item.academicInfo.GRADINGSYSTEM : '-'}</div></td>
                  })
                }
              </tr>

              <tr>
                <td><div className="mb-2 fs18 fw600 col2">MARKS SCORED</div></td>
                {
                  listData && listData.map((item, index) =>{
                    return <td key={index}><div className="fw500 col2 mb-2">{item.academicInfo && item.academicInfo.MARKSSCORED ? item.academicInfo.MARKSSCORED : '-'}</div></td>
                  })
                }
              </tr>
              
              <tr>
                <td><div className="mb-2 fs18 fw600 col2">Field of Interest</div></td>
                {
                  listData && listData.map((item, index) =>{
                    return <td key={index}><div className="tagTitle actives text-uppercase fw500 br40 col2 fs14 mr-2 mb-2 pointer">{item.academicInfo && item.academicInfo.STUDYFIELD}</div></td>
                  })
                }
              </tr>

              <tr>
                <td><div className="mb-2 fs18 fw600 col2">PREFERRED MAJORS</div></td>
                {
                  listData && listData.map((item, index) =>{
                    return <td key={index}><div className="fw500 col2 mb-2">{item.academicInfo && item.academicInfo.MOJOR_PURSUE ? item.academicInfo.MOJOR_PURSUE : '-'}</div></td>
                  })
                }
              </tr>

              <tr>
                <td><div className="mb-2 fs18 fw600 col2">LETTERS OF RECOMMENDATION #</div></td>
                {
                  listData && listData.map((item, index) =>{
                    return <td key={index}><div className="fw500 col2 mb-2">{item.academicInfo && item.academicInfo.RECOMMENDATION ? item.academicInfo.RECOMMENDATION : '-'}</div></td>
                  })
                }
              </tr>

              <tr>
                <td><div className="mb-2 fs18 fw600 col2">STUDENT WAS RECOMMENDED BY</div></td>
                {
                  listData && listData.map((item, index) =>{
                    return <td key={index}><div className="fw500 col2 mb-2">{item.academicInfo && item.academicInfo.RECOMMENDATION_SELECT ? item.academicInfo.RECOMMENDATION_SELECT : '-'}</div></td>
                  })
                }
              </tr>

              <tr>
                <td><div className="mb-2 fs18 fw600 col2">Sports & Curricular Activities</div></td>
                {
                  listData && listData.map((item, index) =>
                    <td key={index}>
                      <div className="tagType1 d-flex flex-wrap">
                      {
                        item.collegePreference && item.collegePreference.SPORTSFACILITY && item.collegePreference.SPORTSFACILITY.length > 0 &&
                        item.collegePreference.SPORTSFACILITY.map((data_itm, data_index) =>
                          <span key={data_index} className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">{data_itm}</span>
                        )
                      }
                      </div>
                    </td>
                  )
                }
              </tr>
              <tr>
                <td><div className="mb-2 fs18 fw600 col2">International Student Clubs Support</div></td>
                {
                  listData && listData.map((item, index) =>
                    <td key={index}>
                      <div className="fs18 col2 fw500">
                      {
                        item.collegePreference && item.collegePreference.COLLEGECLUBS && item.collegePreference.COLLEGECLUBS.length > 0 &&
                        item.collegePreference.COLLEGECLUBS.map((data_itm, data_index) =>
                          <span key={data_index}>{data_itm}</span>
                        )
                      }
                      </div>
                    </td>
                  )
                }
              </tr>
              <tr>
                <td><div className="mb-2 fs18 fw600 col2">College Preference Tags</div></td>
                {
                  listData && listData.map((item, index) =>
                    <td key={index}>
                      <div className="tagType1 d-flex flex-wrap">
                      {
                        item.collegePreference && item.collegePreference.PRIORITYCOLLEGE && item.collegePreference.PRIORITYCOLLEGE.length > 0 &&
                        item.collegePreference.PRIORITYCOLLEGE.map((data_itm, data_index) =>
                          <span key={data_index} className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">{data_itm}</span>
                        )
                      }
                      </div>
                    </td>
                  )
                }
              </tr>
              <tr>
                <td><div className="mb-2 fs18 fw600 col2">Volunteer Hours</div></td>
                {
                  listData && listData.map((item, index) =>{
                    return <td key={index}>
                      <div className="fw500 col2 mb-2">{item.employmentInfo && item.employmentInfo.HRSCOMMUNITYSERVICE ? item.employmentInfo.HRSCOMMUNITYSERVICE : ''}</div>
                    </td>
                  })
                }
              </tr>
              {/* <tr>
                <td><div className="mb-2 fs18 fw600 col2">Awards</div></td>
                {
                  listData && listData.map((item, index) =>{
                    return <td key={index}>
                      <div className="fw500 col2 mb-2"></div>
                    </td>
                  })
                }
              </tr> */}
              <tr>
                <td><div className="mb-2 fs18 fw600 col2">Need-Based Aid</div></td>
                {
                  listData && listData.map((item, index) =>
                    <td key={index}>
                      <div className="awardType1 mb-4">
                      {
                        item.finalPreference && item.finalPreference.FINANASSIST && item.finalPreference.FINANASSIST.length > 0 &&
                        item.finalPreference.FINANASSIST.map((data_itm, data_index) =>
                          <li><Image src={AwardIcon} alt="Icon" className="awardIcons" />{data_itm}</li>
                        )
                      }
                      </div>
                    </td>
                  )
                }
              </tr>
              <tr>
                <td><div className="mb-2 fs18 fw600 col2">CURRENT EMPLOYMENT STATUS</div></td>
                {
                  listData && listData.map((item, index) =>{
                    return <td key={index}><div className="fw500 col2 mb-2">{item.employmentInfo && item.employmentInfo.EMPSTATUS ? item.employmentInfo.EMPSTATUS : '-'}</div></td>
                  })
                }
              </tr>
              <tr>
                <td><div className="mb-2 fs18 fw600 col2">TOTAL WORK EXPERIENCE</div></td>
                {
                  listData && listData.map((item, index) =>{
                    return <td key={index}><div className="fw500 col2 mb-2">{item.employmentInfo && item.employmentInfo.EMPLOYEDPARTTOTALEXPERIENCE ? item.employmentInfo.EMPLOYEDPARTTOTALEXPERIENCE : '-'}</div></td>
                  })
                }
              </tr>
              <tr>
                <td><div className="mb-2 fs18 fw600 col2">CURRENT ANNUAL INCOME</div></td>
                {
                  listData && listData.map((item, index) =>{
                    return <td key={index}><div className="fw500 col2 mb-2">{item.finalPreference && item.finalPreference.CURRENTINCOME ? item.finalPreference.CURRENTINCOME : '-'}</div></td>
                  })
                }
              </tr>
              <tr>
                <td><div className="mb-2 fs18 fw600 col2">EDUCATIONAL INVESTMENT (ANNUALLY)</div></td>
                {
                  listData && listData.map((item, index) =>{
                    return <td key={index}><div className="fw500 col2 mb-2">{item.finalPreference && item.finalPreference.EDUINVESTMENT ? item.finalPreference.EDUINVESTMENT : '-'}</div></td>
                  })
                }
              </tr>
            
            </tbody>
          </Table>
        </div>

      </Container>

      <Modal show={show} onHide={handleClose} className="modalType1">
        <Modal.Header closeButton>
          <Modal.Title className="col8 fw600 fs22 text-center">Save Comparison</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="p-3">
            <div className="fw500 col5 text-center mb-4">
              Please enter a name below to save your comparison for future reference
            </div>
            <Col md={9} className="m-auto">
              <Form.Group controlId="formBasicAdmissions" className="mb-5">
                <Form.Control type="text" placeholder="Title" className="inputType1" onChange={(e) => setCompareTitle(e.target.value)}/>
              </Form.Group>
              {
                titleError &&
                <span className="help-block error-text" style={{marginTop: -45}}>
                  <span style={{ color: "red", fontSize: 13 }}>Title is required.</span>
                </span>
              }
            </Col>
            <div className="text-center">
              <Button onClick={() => saveComparison()} className="btnType2 compareBtn1 mr-4">
                Save
              </Button>
              <Button onClick={handleClose} className="btnType8">
                Cancel
              </Button>
            </div>
          </div>
        </Modal.Body>
      </Modal>



      <Modal show={loader} centered className="loaderModal"><Loader /></Modal>
      <TipModal tipsData= {tipsDataDelete} title={"Info"} show= {showDelete} handleClose= {handleCloseDelete}></TipModal>
    </div>
  )
}
export default Comparison;

