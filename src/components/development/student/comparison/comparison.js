import React, { useState, useEffect } from 'react';
import { Col, Row, Container, Image, Form, Dropdown, Accordion, Card, Button, Table, Modal } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import Logoadmin from '../../../../assets/images/svg/logoadmin.svg';
import bellIcon from '../../../../assets/images/svg/Bell.svg';
import Settings from '../../../../assets/images/svg/GearSix.svg';
import UserThree from '../../../../assets/images/user3.png';
import BackOne from '../../../../assets/images/svg/backs.svg';
import AccorIcon from '../../../../assets/images/accoricon.png';
import Trash from '../../../../assets/images/svg/Trash.svg';
import saveOne from '../../../../assets/images/svg/save1.svg';
import Prints from '../../../../assets/images/svg/prints.svg';
import BookmarkSimple from '../../../../assets/images/svg/BookmarkSimple.svg';
import TrashTwo from '../../../../assets/images/svg/Trash2.svg';
import hearts from '../../../../assets/images/svg/hearts.svg';
import heartfill from '../../../../assets/images/svg/heartfill.svg';
import Stars from '../../../../assets/images/svg/stars.svg';
import StarEmpty from '../../../../assets/images/svg/starempty.svg';
import WomanIcon from '../../../../assets/images/svg/woman1.svg';
import BoyIcon from '../../../../assets/images/svg/boy1.svg';
import ProgressOne from '../../../../assets/images/svg/progress1.svg';
import ProgressRed from '../../../../assets/images/svg/progressRed.svg';
import ProgressYellow from '../../../../assets/images/svg/progressYellow.svg';
import { render } from 'react-dom';
import { getLocalStorage, clearLocalStorage, setLocalStorage } from '../../../../Lib/Utils';
import { logout, saveCollegeComparison, getCollegeComparison, studentFavourite, deleteComparisonList } from '../../../../Actions/Student/register';
import moment from 'moment'
import ReactStars from "react-rating-stars-component";
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import TipModal from '../../../development/TipModal/TipModal'
import { actionLogout } from '../../../../Redux/College/LoginAction';
import Loader from '../../../../Lib/LoaderNew';
import CONSTANTS from '../../../../Lib/Constants';
import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
import charts from "fusioncharts/fusioncharts.charts";
import widgets from "fusioncharts/fusioncharts.widgets";
import ReactFusioncharts from "react-fusioncharts";
import ReactFC from "react-fusioncharts";
// Step 3 - Include the fusioncharts library
import FusionCharts from "fusioncharts";
// Step 4 - Include the chart type
import Column2D from "fusioncharts/fusioncharts.charts";
// Step 5 - Include the theme as fusion
// import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";s
// Step 6 - Adding the chart and theme as dependency to the core fusioncharts

ReactFC.fcRoot(FusionCharts, Column2D, FusionTheme);

charts(FusionCharts);
widgets(FusionCharts);


const Comparison = (props) => {
  const [show, setShow] = useState(false);
  const [listData, setListData] = useState(false);
  const [user, setUser] = useState(false);
  const [compareTitle, setCompareTitle] = useState('');
  const [titleError, setTitleError] = useState(false);
  const [saveSuccessMsg, setSaveSuccessMsg] = useState('');
  const [savedData, setSavedData] = useState('');
  const [compareUpdateID, setCompareUpdateID] = useState('');
  const [localFavourite, setLocalFavourite] = useState('');
  const [loader, setLoader] = useState(false);
  const [deleteMsg, setDeleteMsg] = useState(false);
  const [nameExist, setNameExist] = useState(false);
  const [ethnicityData, setEthnicityData] = useState([]);
  const dispatch = useDispatch();
  var arrData = []


  const handleClose = () => setShow(false);
  const handleShow = () => {
    setTitleError(false)
    setShow(true);
  }

  const [tipsDataDelete, setTipsDataDelete] = useState(false);
  const [showDelete, setShowDelete] = useState(false);

  const handleCloseDelete = () => setShowDelete(false);

  const handleShowDelete = (tipsDataDelete) => {
    setTipsDataDelete(tipsDataDelete);
    setShowDelete(true);
  }

  useEffect(() =>{
    if(props && props.location.data){
      setListData(props.location.data)
      manageAllGraphs(props.location.data)
    }else{
      var list = getLocalStorage('compareData');
      setListData(list)
      manageAllGraphs(list)
    }
    var user = getLocalStorage('user');
    if(user.favourite){
      setLocalFavourite(user.favourite)
    }else{
      user.favourite = []
      setLocalStorage('user', user)
    }
    setUser(user)
    getComparison()
    
  },[])


  const manageAllGraphs = (graphDetails) => {
    //manage for Ethnicity graph
    graphDetails && graphDetails.map((item) =>{
      if (item && item.EFNRALT && item.EFUNKNT && item.EFAIANT && item.EFASIAT && item.EFBKAAT && item.EFHISPT && item.EFNHPIT && item.EFWHITT && item.EF2MORT) {
        var demoData = [
          { label: "Nonresident alien", value: item.EFNRALT },
          { label: "Race/ethnicity unknown", value: item.EFUNKNT },
          { label: "American Indian or Alaska Native", value: item.EFAIANT },
          { label: "Asian", value: item.EFASIAT },
          { label: "Black or African American", value: item.EFBKAAT },
          { label: "Hispanic", value: item.EFHISPT },
          { label: "Native Hawaiian or Other Pacific Islander", value: item.EFNHPIT },
          { label: "White", value: item.EFWHITT },
          { label: "Two or more races", value: item.EF2MORT },
        ]
        // setEthnicityData(demoData)
      }

      const ethnicityGraph = {
        chart: {
          caption: "",
          subcaption: "",
          baseFontSize: "11",
          showpercentvalues: "1",
          showLabels: "0",
          defaultcenterlabel: "",
          aligncaptionwithcanvas: "0",
          captionpadding: "0",
          //legendNumRows:10,
          showLegend: true,
          decimals: "1",
          plottooltext:
            "Percent value of <b>$label</b> is <b>$percentValue</b>",
          centerlabel: "$value",
          theme: "fusion"
        },
        data: demoData
      };
      arrData.push(ethnicityGraph)
    })
    setEthnicityData(arrData)
    
  }




  const getComparison = () =>{
    getCollegeComparison({}).then(res => {
      if(res && res.data.data && res.data.data.rows){
        setSavedData(res.data.data.rows)
      }
    }).catch(err => { });
  }


  const saveComparison = () =>{
    var data = []
    listData && listData.map((item) =>{
      data.push(item.UNITID)
    })

    if(compareUpdateID){
      var payload = {
        title: compareTitle,
        college: data,
        comparisonId: compareUpdateID
      }
    }else{
      var payload = {
        title: compareTitle,
        college: data
      }
    }

    

    if(compareTitle && data){
      saveCollegeComparison(payload).then(res => {
        setShow(false)
        if(res && res.data.message){
          setSaveSuccessMsg(res.data.message)
          setNameExist(res.data.success)
          getComparison()
          setTimeout(() => {
            setSaveSuccessMsg('')
            setNameExist('')
          },5000);
        } 
      }).catch(err => {   });
    }else{
      setTitleError(true)
    }
    
  }


  useEffect(() =>{ 
    if(props && props.location.data){
      setListData(props.location.data)
    }
  },[props.location && props.location.data])

  const viewMyProfile = () =>{
    props.history.push({ pathname: '/student/myProfile'});
  }

  const logoutCall = () => {    
    var user = getLocalStorage('user');
    if (user && user.id) {
      logout({
        userId: user.id
      }).then(res => {
        if (res.data.success == true) {
          dispatch(actionLogout('college'));
          localStorage.clear();
          clearLocalStorage();
          props.history.push({ pathname: '/student/login' });
        }else
        {
          dispatch(actionLogout('college'));
        }
      }).catch(err => {
        dispatch(actionLogout('college'));
      });
    } else {
      dispatch(actionLogout('college'));
      props.history.push({ pathname: '/student/login' });
    }
  }


  const backToDashboard = () =>{
    props.history.goBack()
  }

  const savedList = (data) =>{
    setCompareUpdateID(data._id)
    var newArr = []
    data.college && data.college.map((item_1, index) =>{
      JSON.parse(localStorage.getItem("listData")) &&
      JSON.parse(localStorage.getItem("listData")).map((item_2) =>{
        if(item_1 == item_2.UNITID){
          newArr.push(item_2)
        }
      })
    })

    if(newArr && newArr.length){
      setListData(newArr)
    }
  }

  const deleteColege = (data) =>{
    if(listData.length > 2){
      var response = listData.filter((item,index) => item.UNITID !== data.UNITID)
      setListData(response)
      setLocalStorage('compareData', response);
    }else{
      handleShowDelete("List of comparison should be minimum 2 colleges")
    }
  }

  const manageFavouriteStatus = (id) => {
    var userFavourite = localFavourite
    if (userFavourite && userFavourite.length) {
      var response = userFavourite.find(element => element == id)
      if (response) {
        return true
      } else {
        return false
      }
    } else {
      return false
    }
  }

  const favoriteApiCall = (id, type) => {
    setLoader(true)
    studentFavourite({ favouriteId: id, type }).then(res => {
      setLoader(false)
      if (res.data.success == true) {
        var data = getLocalStorage('user')
        if(type == true){
          if (data.favourite) {
            data.favourite.push(id)
          } else {
            data.favourite = [id]
          }
          setLocalFavourite(data.favourite)
          setLocalStorage('user', data);
        }else{
          var data = getLocalStorage('user')
          if (data && data.favourite && data.favourite.length > 0) {
            let ArryVale = data.favourite;
            var indexB = ArryVale.indexOf(id);
            if (indexB > -1) {
              ArryVale.splice(indexB, 1);
              data.favourite = [...ArryVale]
              setLocalFavourite(data.favourite)
              setLocalStorage('user', data);
            }
          }
        }
      }

    }).catch(err => { setLoader(false)})
  }

  const deleteList = (id) =>{
    setLoader(true)
    deleteComparisonList(id).then(res =>{ 
      setLoader(false)
      if(res && res.data && res.data.success){
        getComparison()
        setDeleteMsg(res.data.message)
        setTimeout(() => {
          setDeleteMsg('')
        },5000);
      }
    }).catch(err => {setLoader(false)})
  }


  const isInt = (n) =>{
    return Number(n) === n && n % 1 === 0;
  }


  return (
    <div className="collegeAdminUi">
      <Container fluid> 
        <div className="pl-2 pr-2 pt-4">
          <Row>
            <Col md={3}>
              <Image src={Logoadmin} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" />
              <span className="fs20 col24 fw700 ml-1">HiEd Harmony</span>
            </Col>
            <Col md={6}></Col>
            <Col md={3}>
              <div className="powerBtn d-flex justify-content-end">
                {/* <div className="mr-4">
                  <Dropdown>
                    <Dropdown.Toggle id="dropdown-basic" className="DropdownType1">
                      <Image src={bellIcon} alt="Search" className="pointer setingOne mt-1" />
                    </Dropdown.Toggle>
                  </Dropdown>
                </div> */}
                {/* <Image src={Settings} alt="Search" className="pointer setingOne mr-5" /> */}
                <div style={{paddingTop: 6}}>
                  <Dropdown>
                    <Dropdown.Toggle id="dropdown-basic" className="DropdownType1 col2 fs14 fw600">
                      {/* <Image src={UserThree} alt="Search" className="pointer user1 mr-1" /> */}
                      {user.firstName + " " + user.lastName}   <i className="fa fa-chevron-down ml-1 col8" aria-hidden="true"></i>
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                      {/* <Dropdown.Item onClick={() => viewMyProfile()}>Profile</Dropdown.Item> */}
                      {/* <Dropdown.Item href="#">Setting</Dropdown.Item> */}
                      <Dropdown.Item onClick={() => logoutCall()}>Logout</Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
              </div>
            </Col>
          </Row>
          <div className="col2 fs20 fw500 mt-4 mb-3">
            <div style={{width: 100, cursor: 'pointer'}} onClick={() => backToDashboard()}>
            <Image src={BackOne} alt="back" className="pointer mr-2 back1"  /> Back
            </div>
          </div>
          {
            saveSuccessMsg && 
            <div className="fw500 col5 mb-4 pb-2 text-center" style={nameExist == true ? { color: 'green' } : { color: 'red' }}>{saveSuccessMsg}</div> 
          }
          {
            deleteMsg && 
            <div className="fw500 col5 mb-4 pb-2 text-center" style={{ color: 'green' }}>{deleteMsg}</div> 
          }
          <div className="d-flex flex-wrap justify-content-between" style={{marginTop: 10, marginBottom: 10}}>
            <div>
              <Dropdown className="dropdownBox1">
                <Dropdown.Toggle id="dropdown-basic" className="DropdownType4 col2 fw600 fs20">
                  Saved Comparison <Image src={AccorIcon} alt="back" className="pointer ml-1" />
                </Dropdown.Toggle>
                <Dropdown.Menu className="shadow5">
                  {
                    savedData && savedData.length > 0 ?
                    savedData.map((item, ind) =>{
                      return <Dropdown.Item onClick={() => savedList(item)} key={ind}>
                        <Row>
                          <Col md={10} onClick={() => savedList(item)}>
                            <div className="fs22 fw500 col2">{item.title}</div>
                            <div className="fs14 fw400 col5">Created {moment(item.updatedAt).format("MMM Do YY")}</div>
                          </Col>
                          <Col md={2}>
                            <Image src={Trash} alt="Trash" className="pointer grayCol" onClick={() => deleteList(item._id)} />
                          </Col>
                        </Row>
                      </Dropdown.Item>
                    })
                    :
                    <Dropdown.Item >
                      <div className="fs20 fw500 col5">No data found</div>
                    </Dropdown.Item>
                  } 
                </Dropdown.Menu>
              </Dropdown>
            </div>
          
            
            <div className="d-flex">
              <div className="col8 fw600 fs20 mr-4 pointer" onClick={handleShow}>Save Comparison</div>
                {/* <Image src={saveOne} alt="back" className="pointer save1 mr-2" />
                <Image src={Prints} alt="back" className="pointer save1" /> */}
            </div>
          </div>
        </div>
        <div className="bgCol4 comparisonType1 mb-4">
          <Row>
            <Col md={2}></Col>
            <Col md={10} className="pl-5">
              <Row>
                {
                  listData && listData.map((item, index) =>{
                    return <Col md={12 / listData.length} key={index}>          
                      <div className="bgCol39 br10 compareBox1">  
                        <div className="p-3">   
                          <div className="w-100">   
                            <div className="col3 fs15 fw600">{item.INSTNM ? item.INSTNM : ''}<span className="ml-3"></span></div>
                            <div className="col3 fs14 fw400">{item.ADDR + ', ' + item.CITY + ', ' + item.STABBR}<span className="ml-3"></span></div>
                          </div>
                        </div>
                        <div className="d-flex align-items-center bgCol1 compareBox2">
                          {
                            item && item.UNITID &&
                            manageFavouriteStatus(item.UNITID) ?
                              <Image src={heartfill} alt="Book Mark" className="pointer mr-4" onClick={() => favoriteApiCall(item.UNITID, false)}/>
                              :
                              <Image src={hearts} alt="Book Mark" className="pointer mr-4" onClick={() => favoriteApiCall(item.UNITID, true)}/>
                          }
                          <Image src={TrashTwo} alt="Trash" className="pointer" onClick={() => deleteColege(item)}/>
                        </div>
                      </div>
                    </Col>
                  })
                }
              </Row>
            </Col>
          </Row>
        </div>
        <div className="comparisonType4 br10">  
          <Table striped bordered className="tableType1 br10">
            <tbody>
              <tr>
                <td>RATING</td>
                {
                  listData && listData.map((item, index) =>{
                    return <td key={index}>
                        <div className="fw500 col2 mb-2">
                          {item.RATING ? item.RATING : '0'}
                          <span className="ml-2 ratingOne">
                            <ReactStars
                              count={5}
                              size={14}
                              edit={false}
                              isHalf={true}
                              value={item.RATING && parseFloat(item.RATING)}
                              emptyIcon={<i className="far fa-star"></i>}
                              halfIcon={<i className="fa fa-star-half-alt"></i>}
                              fullIcon={<i className="fa fa-star"></i>}
                              activeColor="#ffd700"
                            />
                          </span>
                        </div>
                      </td>
                  })
                }
              </tr>
              <tr>
                <td>COLLEGE TYPE</td>
                {
                  listData && listData.map((item, index) =>{
                    return <td key={index}>{item.CONTROL ? item.CONTROL : "-"}</td>
                  })
                }
              </tr>
              <tr>
                <td>CAMPUS SETTING</td>
                {
                  listData && listData.map((item, index) =>{
                    return <td>{item.LOCALE ? item.LOCALE : '-'}</td>
                  })
                }
              </tr>
              <tr>
                <td>TOTAL ENROLLMENT</td>    
                {
                  listData && listData.map((item, index) =>{
                    // return <td>{parseFloat(parseFloat(item.EFTOTLT_UG) + parseFloat(item.EFTOTLT_G)).toFixed(2)}</td>
                    return <td>{item.EFTOTLT_UG && Math.round(item.EFTOTLT_UG)}</td>
                  })
                }
              </tr>
              <tr>
                <td>GENDER DISTRIBUTION</td>
                {
                  listData && listData.map((item, index) =>{
                    return <td key={index}>
                      <div className="col2 fw500 text-uppercase ratioType1">
                        <Image src={WomanIcon} alt="Icon" />
                        <span className="ml-2 mr-4">{item.ENRLM && item.ENRLW && parseFloat(parseFloat(item.ENRLM) / (parseFloat(item.ENRLM)+parseFloat(item.ENRLW)) * 100).toFixed(2)}%</span>
                        <Image src={BoyIcon} alt="Icon" />
                        <span className="ml-2">{item.ENRLM && item.ENRLW && parseFloat(parseFloat(item.ENRLW) / (parseFloat(item.ENRLM)+parseFloat(item.ENRLW)) * 100).toFixed(2)}%</span>
                      </div>
                    </td>
                  })
                }
              </tr>
              <tr>
                <td>ACCEPTANCE RATE</td>
                {
                  listData && listData.map((item, index) =>{
                    return <td key={index}>
                      <div className="smal-circular">
                        <CircularProgressbar 
                          value={item.ADMSSN_CY && item.APPLCN_CY && parseFloat((parseFloat(item.ADMSSN_CY)/parseFloat(item.APPLCN_CY))*100).toFixed(2)} 
                          styles={buildStyles({
                            textColor: "#000", 
                            pathColor: "#89D667",  
                            trailColor: "#ececff",  
                          })}
                          strokeWidth={10}
                          text={`${parseFloat((parseFloat(item.ADMSSN_CY)/parseFloat(item.APPLCN_CY))*100).toFixed(2)}%`}
                        />
                      </div>
                    </td>
                  })
                }
              </tr>
              <tr>
                <td>GRADUATION RATE</td>
                {
                  listData && listData.map((item, index) =>{
                    return <td key={index}>
                      <div className="smal-circular">
                        <CircularProgressbar 
                          value={item.GRTOTLT_3 && item.GRTOTLT_2 && parseFloat((parseFloat(item.GRTOTLT_3)/parseFloat(item.GRTOTLT_2))*100).toFixed(2)} 
                          styles={buildStyles({
                            textColor: "#000",  
                            pathColor: "#FE5E54",  
                            trailColor: "#ececff",  
                          })}
                          strokeWidth={10} 
                          text={`${parseFloat((parseFloat(item.GRTOTLT_3)/parseFloat(item.GRTOTLT_2))*100).toFixed(2)}%`}
                        />
                      </div>
                    </td>
                  })
                }
              </tr>
              <tr>
                <td>
                  <div className="col2 fw500 mb-2 text-uppercase">ADMISSIONS</div>
                  <div className="fs14 col2 fw500 mb-2">Applicants</div>
                  <div className="fs14 col2 fw500 mb-2">Admissions</div>
                  <div className="fs14 col2 fw500">Enrolled</div>
                </td>
                  {
                    listData && listData.map((item, index) =>{
                      return <td>
                        <div className="col2 fw600 mb-2" style={{height: 20}}></div>
                        <div className="col2 fw600 mb-2">{item.APPLCN_CY && item.APPLCN_CY}</div>
                        <div className="col2 fw600 mb-2">
                          <span>{item.ADMSSN_CY}</span>
                          <span className="ml-4 col8">{item.ADMSSN_CY && item.APPLCN_CY && (parseFloat(parseFloat(item.ADMSSN_CY) / parseFloat(item.APPLCN_CY)).toFixed(2))*100 + "%"}</span>
                        </div>
                        <div className="col2 fw600 mb-2">
                          <span>{item.ENRLT_CY}</span>
                          <span className="ml-4 col8">{item.ENRLT_CY && item.APPLCN_CY && (parseFloat(parseFloat(item.ENRLT_CY) / parseFloat(item.APPLCN_CY)).toFixed(2))*100 + "%"}</span>
                        </div>
                      </td>
                    })
                  }
              </tr>

              <tr>
                <td>
                  <div className="col2 fw500 mb-2 text-uppercase">TUITION & FEES</div>
                  <div className="fs14 col2 fw500 mb-2">Undergraduate Degree</div>
                  <div className="fs14 col2 fw500 mb-2">Graduate Degree</div>
                </td>
                {
                  listData && listData.map((item, index) =>{
                    return <td>
                      <div className="col2 fw600 mb-2" style={{height: 20}}></div>
                      <div className="col2 fw600 mb-2">
                        {item.TUITION3 && item.FEE3 ? isInt(parseFloat(item.TUITION3) + parseFloat(item.FEE3)) ? "$"+parseInt(parseFloat(item.TUITION3) + parseFloat(item.FEE3)) : "$"+parseFloat(parseFloat(item.TUITION3) + parseFloat(item.FEE3)).toFixed(2) : ''}
                      </div>
                      <div className="col2 fw600 mb-2">{item.TUITION7 && item.FEE7 ? isInt(parseFloat(item.TUITION7) + parseFloat(item.FEE7)) ? "$"+ parseInt(parseFloat(item.TUITION7) + parseFloat(item.FEE7)) : "$"+ parseFloat(parseFloat(item.TUITION7) + parseFloat(item.FEE7)).toFixed(2) : ''}</div>
                    </td>
                  })
                }
              </tr>
              
              {/* <tr>
                <td>Test Pre-requisite</td>
                {
                  listData && listData.map((item, index) =>{
                    return <td>Either SAT or ACT</td>
                  })
                }
              </tr> */}
              <tr>
                <td>STUDENT - FACULTY RATIO</td>
                {
                  listData && listData.map((item, index) =>{
                    return <td key={index}>{item.STUFACR && item.STUFACR}</td>
                  })
                }
              </tr>
              <tr>
                <td>Popular Fields (by enrollment)</td>
                {
                  listData && listData.map((item, index) =>(
                    <td key={index}>
                      {
                        item.EFCIPLEV && item.EFCIPLEV.map((p_itm, p_ind) =>(
                          <div className="mb-2" key={p_ind}>
                            <div className="d-flex flex-wrap tagType6">
                              <div className="fs12 col10 bgCol11 fw500 mb-2 mr-2 text-uppercase">{CONSTANTS.EFCIPLEV[p_itm] + "-" + item.EFTOTLT[p_ind]}</div>
                            </div>
                        </div>
                      ))
                      }
                      
                    </td>
                  ))
                }
              </tr>
              <tr>
                <td>Ethnicity</td>
                {
                  listData && listData.map((item, index) =>(
                    <td key={index}>
                      <ReactFusioncharts
                          type="doughnut2d"
                          width="100%"
                          height="500"
                          dataFormat="JSON"
                          dataSource={ethnicityData[index]}
                        />
                      
                    </td>
                  ))
                }
              </tr>
              {/* <tr>
                <td>Level of Institution (4 yr or more / 2 - 4 yr / Less than 2 yr)</td>
                {
                  listData && listData.map((item, index) =>{
                    return <td key={index}>{item.ICLEVEL && item.ICLEVEL}</td>
                  })
                }
              </tr>
              <tr>
                <td>Size of Institution (by Enrollment)</td>
                {
                  listData && listData.map((item, index) =>{
                    return <td key={index}>{item.INSTSIZE && item.INSTSIZE}</td>
                  })
                }
              </tr>
              <tr>
                <td>Undergraduate Enrollment</td>
                {
                  listData && listData.map((item, index) =>{
                    return <td key={index}>{item.FTEUG && item.FTEUG + ', '} {item.FTEGD && item.FTEGD}</td>
                  })
                }
              </tr>
              <tr>
                <td>Cost of Education (In-State/Out-State)</td>
                {
                  listData && listData.map((item, index) =>{
                    return <td key={index}>{item.FTEUG && item.FTEUG + ', '} {item.FTEGD && item.FTEGD}</td>
                  })
                }
              </tr>
              <tr>
                <td>FTFT UG Financial Aid (all types of aid)</td>
                {
                  listData && listData.map((item, index) =>{
                    return <td key={index}>{item.FTEUG && item.FTEUG + ', '} {item.FTEGD && item.FTEGD}</td>
                  })
                }
              </tr> */}
              
            </tbody>
          </Table>
        </div>

      </Container>

      <Modal show={show} onHide={handleClose} className="modalType1">
        <Modal.Header closeButton>
          <Modal.Title className="col8 fw600 fs22 text-center">Save Comparison</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="p-3">
            <div className="fw500 col5 text-center mb-4">
              Please enter a name below to save your comparison for future reference
            </div>
            <Col md={9} className="m-auto">
              <Form.Group controlId="formBasicAdmissions" className="mb-5">
                <Form.Control type="text" placeholder="Title" className="inputType1" onChange={(e) => setCompareTitle(e.target.value)}/>
              </Form.Group>
              {
                titleError &&
                <span className="help-block error-text" style={{marginTop: -45}}>
                  <span style={{ color: "red", fontSize: 13 }}>Title is required.</span>
                </span>
              }
            </Col>
            <div className="text-center">
              <Button onClick={() => saveComparison()} className="btnType2 compareBtn1 mr-4">
                Save
              </Button>
              <Button onClick={handleClose} className="btnType8">
                Cancel
              </Button>
            </div>
          </div>
        </Modal.Body>
      </Modal>



      <Modal show={loader} centered className="loaderModal"><Loader /></Modal>
      <TipModal tipsData= {tipsDataDelete} title={"Info"} show= {showDelete} handleClose= {handleCloseDelete}></TipModal>
    </div>
  )
}
export default Comparison;

