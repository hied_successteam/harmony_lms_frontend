import React, {useState, useEffect} from 'react';
import { Col, Row, Container, Image, Form, Button } from 'react-bootstrap';
import { isChrome,
  //browserName 
} from "react-device-detect";
import Logos from '../../../../assets/images/logos.png';
import Facebook from '../../../../assets/images/svg/facebook.svg';
import Googles from '../../../../assets/images/svg/googles.svg';
//import Linkedin from '../../../../assets/images/svg/linkedin.svg';   
import { useSelector, useDispatch } from 'react-redux';
import { studentLogin, studentSocialLogin } from '../../../../Redux/College/LoginAction';  
import validateInput from '../../../../Lib/Validation/studLoginValidation' 
import Loader from "react-js-loader";
import { setLocalStorage, clearLocalStorage } from '../../../../Lib/Utils'

import FacebookLogin from 'react-facebook-login';
import { GoogleLogin } from 'react-google-login';
//import { LinkedIn } from 'react-linkedin-login-oauth2';

const CollegeLogin = (props) => {

  //set local variables in state
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errors, setErrors] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [resultError, setResultError] = useState(false);
  const [message, setMessage] = useState('');
  const [userType, setUserType] = useState('student');
  const [key, setKey] = useState(false);

  useEffect(() => {
    if(!isChrome)
    {
         alert("For better application compatibility, use Google chrome only.")
    } 
}, [])

  //go to Registration page
  const goToSignup = () =>{
    props.history.push({pathname: 'register'});
  }

  const goToForgot = () => {
    props.history.push({ pathname: '/forgotpassword', userType: userType });
  }

  // use Redux to store user data
  const dispatch = useDispatch();
  const response = useSelector(state => state.required_data)

  useEffect(() =>{ 
    localStorage.clear();
    clearLocalStorage();
    setIsLoading(false)
    if(key){
      // setTimeout(function () {
      //   setIsLoading(false)
      // }.bind(this),2000);

      if(response && response.success == true){
        setLocalStorage('user', response.data);
        if(response.data.isProfileCompleted && response.data.isProfileCompleted == true && response.data.plan){
          props.history.push({pathname: 'dashboard'});
        }else{
          props.history.push({pathname: 'wizard'});
        }
      }else{
        if(response.success == false){
          setIsLoading(false)
          setResultError(true)
          setMessage(response.message)
          setTimeout(function () {
            setResultError(false)
            setMessage('')
          }.bind(this),4000);
        }
      }
    }
  }, [key, response, props.history])


  // useEffect(() =>{
  //   let unmounted = false;
  //   localStorage.clear()
  //   setResultError(false)
  //   setMessage('')
  //   return () => { unmounted = true };
  // }, [])

  useEffect(() =>{
    localStorage.clear();
    clearLocalStorage();
    setResultError(false)
    setMessage('')
  }, [])


  //call login api to get result
  const loginApiCall = () =>{
    localStorage.clear()
    if (isValid()) {
      setIsLoading(true)
      setErrors({})
      dispatch(studentLogin(email, password, userType))

      setTimeout(function () {
        setKey(true)
        setIsLoading(false)
      }.bind(this),2000);

    }
  }

  //check input validation
  const isValid = () => {
    const { errors, isValid } = validateInput(email, password);
    if (!isValid) {
      setErrors(errors)
    }
    return isValid;
  }

  //go to landing page
  const goToLanding = () =>{
    props.history.push({pathname: '/landing/dashboard'});
  }

  const socialLogin = (response, socialType) => {
    var payload = ''
    if(socialType == 'facebook' && response.userID && response.accessToken && !response.error){
      var name = response.name.split(' ');
      payload = {
        firstName: name[0],
        lastName: name[1],
        email: response.email,
        socialType: socialType,
        socialId: response.id,
        socialToken: response.accessToken,
        userType: userType
      }
      socialLoginCall(payload)

    }else if(socialType == 'google' && response.accessToken && response.profileObj && response.profileObj.googleId){

      payload = {
        firstName: response.profileObj.givenName,
        lastName: response.profileObj.familyName,
        email: response.profileObj.email,
        socialType: socialType,
        socialId: response.profileObj.googleId,
        socialToken: response.accessToken,
        userType: userType
      }

      socialLoginCall(payload)

    }
  }

  const socialLoginCall = (payload) =>{
    setIsLoading(true);
    setKey(true)
    dispatch(studentSocialLogin(payload))
  }

  //const handleSuccess = (data) =>{
    // axios.get(`GET https://api.linkedin.com/v2/${data.code}`).then(function (response){
    //     console.log("got an access token");
    //     console.log(response)
    //   }).catch(err => {
    //     console.error(err);
    //   });
      // axios.post("https://www.linkedin.com/oauth/v2/accessToken",{
      //   headers:{
      //   "Content-Type": "application/x-www-form-urlencoded",
      //   "Access-Control-Allow-Origin": "*"
      //   },
      //   data:{
      //     grant_type : 'authorization_code',
      //     code : data.code,
      //     redirect_uri: "http%3A%2F%2Flocalhost%3A5000%2Flinkedin",
      //     client_id :'78gmy2atv769d5',
      //     client_secret: 'LLVyHVipUbUYqAe4'
      //   }
      // }).then(function (response){
      //   console.log("got an access token");
      //   console.log(response)
      // }).catch(err => {
      //   console.error(err);
      // });


      // https://www.linkedin.com/oauth/v2/accessToken?grant_type=authorization_code&client_id=78gmy2atv769d5&client_secret=LLVyHVipUbUYqAe4&redirect_uri=http://localhost:5000/linkedin&code=AQSsNDj7uDxtZ2c59V6yZ2eEyYbTDMgnjOGdO2n2RTW6AX1vSFLN2Di-pMpNJEX1Fp6VGcCNNS7YB6Q4kzbBNCHI1-DfqJ5H7Z-3_Z6COOw_ZNfieE_tU784db3EPndyGHBM13lcFKvgN8IJWBIknuUXYcQEhhZso_s3in6ZN0sCzccs6wpl7oDzAASx9ZkNOFnL0YSA0o2EydDLrGA

    //   axios({
    //     method: 'get', //you can set what request you want to be
    //     url: `https://www.linkedin.com/oauth/v2/accessToken?grant_type=authorization_code&client_id=78gmy2atv769d5&client_secret=LLVyHVipUbUYqAe4&redirect_uri=http://localhost:5000/linkedin&code=${data.code}`}).then((Response) => Response.json())
    //   .then((res) => {
    //     console.log(res)
    // })



  //   const requestOptions = {
  //     method: 'POST',
  //     body: JSON.stringify({ 
  //       grant_type: "authorization_code",
  //       code: data.code,
  //       redirect_uri: 'http%3A%2F%2Flocalhost%3A5000%2Flinkedin',
  //       client_id: '78gmy2atv769d5',
  //       client_secret: 'LLVyHVipUbUYqAe4'
  //      })
  // };
  // fetch(`https://www.linkedin.com/uas/oauth2/accessToken`, requestOptions)
  //     .then(response =>  console.log('kjkjkjkj', response), alert(response));



      // axios.get(`https://www.linkedin.com/uas/oauth2/accessToken?grant_type=authorization_code&code=${data.code}&redirect_uri=http%3A%2F%2Flocalhost%3A5000%2Flinkedin&client_id=78gmy2atv769d5&client_secret=LLVyHVipUbUYqAe4`)
      // .then(response =>  console.log('kjkjkjkj--', response));





      // fetch(`https://www.linkedin.com/oauth/v2/accessToken`, {
      //   method: "POST",
      //   headers:{
      //       "Content-Type": "application/x-www-form-urlencoded"
      //       },
      //     data:{
      //       grant_type : 'authorization_code',
      //     code : data.code,
      //     redirect_uri: "http://localhost:5000/linkedin",
      //     client_id :'78gmy2atv769d5',
      //     client_secret: 'LLVyHVipUbUYqAe4'
      //     }
      
      // }).then((Response) => Response.json())
      //   .then((res) => {
      //     console.log(res)
      // })

      
  //}

  const onKeyPress = (e) => {
    if(e.which == 13) {
      loginApiCall();
    }
  }

  
  return (
      <div className="bgBanner d-flex align-items-center justify-content-center w-100">      
            {/* <div className="topBox bgCol1">
                For better application compatibility, use Google chrome only. 
            </div>   */}
            <Container>
                <Col lg={10} md={10} className="m-auto">  
                      <div className="studentBox text-center bgCol3 shadow1 br10 loginUsers">
                          <div>
                          <Image src={Logos} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer mb-5 logos" onClick={() => goToLanding()} />
                          <div className="studentOne mt-4">
                                {/* <div className="fs22 fw600 col2 mb-1">Join Today</div>
                                <div className="fw500 col5 mb-4">Register using you social media profile</div> */}
                                <div className="socialMedia d-none justify-content-center mb-4 pb-2">  
                                    {/* <FacebookLogin
                                      appId="977794676341301"
                                      autoLoad={false}
                                      fields="name,email,picture"
                                      callback={(response) => socialLogin(response, 'facebook')}
                                      cssClass="my-facebook-button-class lgn mr-5"
                                      icon={<Image src={Facebook} alt="facebook" className="pointer mr-5"/>} 
                                      />
    
                                      <GoogleLogin
                                        clientId="29753036220-uc2qtdakjvl0ilmrgm7tuk97vnsbpkau.apps.googleusercontent.com"
                                        buttonText=""
                                        onSuccess={(response) => socialLogin(response, 'google')}
                                        onFailure={(response) => socialLogin(response)}
                                        render={renderProps => (
                                          <Image onClick={renderProps.onClick} src={Googles} alt="google" className="pointer"/>   
                                        )}
                                      /> */}

                                      {/* <LinkedIn
                                        clientId="78gmy2atv769d5"
                                        onFailure={handleFailure}
                                        onSuccess={(response) => handleSuccess(response)}
                                        redirectUri="http://localhost:5000/linkedin"
                                        scope={"r_emailaddress"}
                                      >
                                        <Image src={Linkedin} alt="linkedin" className="pointer"/> 
                                      </LinkedIn> */}


                                    {/* <Image src={Facebook} alt="facebook" className="pointer mr-5"/> */}
                                    {/* <Image src={Googles} alt="google" className="pointer mr-5"/> */}
                                    {/* <Image src={Linkedin} alt="linkedin" className="pointer"/>   */}
                                </div>
                                  {/* <hr className="hrBorderTwo orSet mb-4 pb-2" />   */}
                                   <div className="fw600 fs22 col2 mb-1">Sign In</div>   
                                   <div className="fw500 col5 mb-4">Welcome to your account</div>   
                                {
                                  resultError &&
                                  <div className="fw500 col5 mb-4" style={{color: 'red'}}>{message}</div> 
                                } 
                                <Form className="formLayoutUi">
                                    <Row>
                                          <Col md={6}> 
                                              <Form.Group controlId="formBasicEmailA"> 
                                                    <Form.Control 
                                                      type="email" 
                                                      placeholder="Email" 
                                                      className="inputType1"
                                                      value={email}
                                                      onKeyPress={(e) => onKeyPress(e)}
                                                      onChange={(e) => setEmail(e.target.value)} />
                                                      {
                                                        errors.email &&
                                                        <span className="help-block error-text">{errors.email}</span>
                                                      }
                                              </Form.Group>
                                          </Col>
                                          <Col md={6}>
                                              <Form.Group controlId="formBasicEmailB" className="mb-2">
                                                    <Form.Control 
                                                      type="password" 
                                                      placeholder="Password" 
                                                      className="inputType1"
                                                      value={password}
                                                      onKeyPress={(e) => onKeyPress(e)}
                                                      onChange={(e) => setPassword(e.target.value)} />
                                                      {
                                                        errors.password &&
                                                        <span className="help-block error-text">{errors.password}</span>
                                                      }
                                              </Form.Group>
                                              <div className="text-right col1 fs18 fw600 mb-3 pb-2 pointer" onClick={() => goToForgot()}>
                                                    Forgot Password?
                                              </div>
                                          </Col>
                                          <Col md={12}>  
                                            <div className="text-center align-xs-center">     
                                                  <Button className="btnType4 fw600 mr-4 mb-4" onClick={() => loginApiCall()}>
                                                  {
                                                    isLoading ?
                                                    <Loader type="bubble-top" bgColor={"#414180"} size={25} />
                                                    :
                                                    "Sign In"
                                                  }  
                                                  </Button>
                                                  <div className="fw500 col5 align-xs-center mb-2">Don’t have an account? 
                                                  <span className="col1 fw500 pointer ml-2" onClick={() => goToSignup()}>Sign Up</span></div>
                                                  <div className="fw500 col5 fs15"><span className="fs18">*</span> Be an early bird and register yourself before the launch to enjoy FREE membership benefits for 1 year.</div>   
                                            </div>     
                                           </Col>
                                    </Row>
                                    
                                </Form>
                          </div>
                          </div>
                      </div>
                </Col>
            </Container>
      </div>
  )
}

export default CollegeLogin; 

