import React, { useState, useEffect } from 'react';
import { Col, Row, Button, Image} from 'react-bootstrap';
import 'rsuite/dist/styles/rsuite-default.css';
import FormInput from './FormInput'
import { EmploymentInfo } from '../WizardQuestion'
import { getLocalStorage } from '../../../../../Lib/Utils';
import infoicon from "../../../../../assets/images/svg/questions.svg";
import TipModal from '../../../TipModal/TipModal'
import Validator from "validator";



const FiveStep = (props) => {

  const [state, setState] = useState({});
  const [errors, setErrors] = useState({});
  const [tipsData, setTipsData] = useState(false);
  const [show, setShow] = useState(false);
  const [urlMsg, setUrlMsg] = useState('');

  const handleClose = () => setShow(false);

  const handleShow = (tipsData) => {
    setTipsData(tipsData);
    setShow(true);
  }

  useEffect(() =>{
    setErrors({})
  },[props])

  const onchangeInput = event => {
    if (event && event.target && event.target.name) {
      const { target: { name, value } } = event;
      errors[[name]] = false
      setState({ ...state, [name]: value });
    } else {
      //setState({ ...state, DOB: event });
    }
  };

  const onKeyDownInput = event => {
    if (event && event.target && event.target.name) {
      const { target: { name, value } } = event;

      if (event.keyCode == 13) {
        setState({ ...state, [name]: '' });
        var arr = name.split('_');
        let Newname = arr[0];
        if (state[Newname]) {
          let ArryVale = [...state[Newname]];
          var indexB = ArryVale.indexOf(value);
          if (indexB > -1) {
            ArryVale.splice(indexB, 1);
            setState({ ...state, [name]: '', [Newname]: [...ArryVale] });
          } else {
            ArryVale.push(value);
            setState({ ...state, [name]: '', [Newname]: [...ArryVale] });
          }
        } else {
          setState({ ...state, [name]: '', [Newname]: [value] });
        }
      }
    } else {
      //setState({ ...state, DOB: event });
    }
  };

  const oncheckBox = (name, value) => {
    if (state[name]) {
      let ArryVale = state[name];
      var indexB = ArryVale.indexOf(value);
      if (indexB > -1) {
        ArryVale.splice(indexB, 1);
        if(ArryVale && ArryVale.length <= 0){
          delete state[name]
          setState({ ...state});
        }else{
          setState({ ...state, [name]: [...ArryVale] });
        }
      } else {
        ArryVale.push(value);
        setState({ ...state, [name]: [...ArryVale] });
      }
    } else {
      setState({ ...state, [name]: [value] });
    }
  }

  // var ArryVale = []
  // const oncheckBox = (value) => {
  //   if (ArryVale && ArryVale.length > 0) {
  //     var indexB = ArryVale.indexOf(value);
  //     if (indexB > -1) {
  //       ArryVale.splice(indexB, 1);
  //       if(ArryVale && ArryVale.length <= 0){
  //         toggalOption(ArryVale)
  //       }else{
  //         toggalOption(ArryVale)
  //       }
  //     } else {
  //       ArryVale.push(value);
  //       toggalOption(ArryVale)
  //     }
  //   } else {
  //     toggalOption([value])
  //   }
  // }

  const onDeleteArray = (name, value) => {
    if (state[name]) {
      let ArryVale = state[name];
      ArryVale.splice(value, 1);
      setState({ ...state, [name]: [...ArryVale] });
    } else {
      setState({ ...state, [name]: [value] });
    }
  }

  useEffect(() => {
    if (props.data && Object.keys(props.data).length != 0) {
      setState({
        ...state,
        ...props.data
      });
    }
  }, [props.data]);

  const stepSubmit = () => {
    let errors = {};
    let valid = true;
    setUrlMsg(false)

    for (var m = 0; m < EmploymentInfo.length; m++) {
      if (EmploymentInfo[m].isRequired) {
        let codevalue = state[EmploymentInfo[m].code];
        if (!codevalue || codevalue == 'undefined' || codevalue == '') {
          valid = false;
          errors[EmploymentInfo[m].code] = true;
        }

        // if (errors && errors["PERSNLITYTRAITSSTRY"] !== true) {
        //   if (state && state["PERSNLITYTRAITSSTRY"] && state["PERSNLITYTRAITSSTRY"].length < 100) {
        //     valid = false;
        //     errors["PERSNLITYTRAITSSTRY"] = true;
        //   }
        // }

        // Check child validation
        if (EmploymentInfo[m].isLogical) {
          let Newarry = EmploymentInfo[m].options;
          for (var n = 0; n < Newarry.length; n++) {
            if (Newarry[n].isLogical) {

              if (Newarry[n].value == state['EMPSTATUS']) {
                let newdata = Newarry[n].questions;
                for (var i = 0; i < newdata.length; i++) {
                  if (newdata[i].isRequired) {
                    let codevalueNew = state[newdata[i].code];
                    var regex = /(http|https):(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/; 
                    if(codevalueNew){
                      if((newdata[i].code == "EMPLOYEDFULLLINKEDINPRFL" || newdata[i].code == "EMPLOYEDPARTLINKEDINPRFL") && Validator.isURL(codevalueNew)){
                        if (codevalueNew && codevalueNew.match(regex)) {
                        }else{
                          valid = false;
                          errors[newdata[i].code] = true;
                          setUrlMsg('Valid URL is required (ex.- https://hiedharmony.com).')
                        }
                      }else if((newdata[i].code == "EMPLOYEDFULLLINKEDINPRFL" || newdata[i].code == "EMPLOYEDPARTLINKEDINPRFL") && codevalueNew !== "NA"){
                        valid = false;
                        errors[newdata[i].code] = true;
                        setUrlMsg('Valid URL is required (ex.- https://hiedharmony.com).')
                      }else if (!codevalueNew || codevalueNew == 'undefined' || codevalueNew == '') {
                        valid = false;
                        errors[newdata[i].code] = true;
                      }
                    }else{
                      valid = false;
                      errors[newdata[i].code] = true;
                    }
                    

                  }
                }
              }

            }
          }
        }


      }
    }

    console.log('11111111',state)
    console.log('22222222',errors)

    if (valid) {
      props.stepSubmitFive(state, 5);
      setErrors({});
    }
    else {
      setErrors({ ...errors });
    }

  }

  return (

    <div className="rightBox pt-5 pb-5 pl-4">

      <Col lg={10} md={12}>
        <div className="rightLayout">
          <div className="d-flex flex-wrap justify-content-between mb-1">
            <div className="stepTitle fw500 fs30 col4 position-relative mb-4 pb-1">Employment Info </div>
            {
              getLocalStorage('user') && getLocalStorage('user').isProfileCompleted &&
              <div className="col2 fw500 fs18 pointer" onClick={() => props.stepSelect('sixth')}>Skip</div>
            }
          </div>
          <Row>

            {
              EmploymentInfo && EmploymentInfo.map((data, index) =>
                <div key={index} style={{width: '100%'}}>
                  {data.type == 'dropDown' &&
                    <Col md={12} key={index + 'aaa'}>
                      <div className="col4 fw600 fs18 mb-2">
                        <span className="numericType1">{index + 1}.</span>
                        {data.text}
                        {data.isRequired ? <sup className="starQ">*</sup> : ''}
                        {data.tips ? <Image onClick={() => handleShow(data.tips)} src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                      </div>
                    </Col>
                 }

                  <Col md={12} key={index + 'bbb'}>
                    {(data.type !== 'dropDown') ?
                      <div className="col4 fw600 fs18 mb-2">
                        <span className="numericType1">{index + 1}.</span>
                        {data.text}
                        {data.isRequired ? <sup className="starQ">*</sup> : ''}
                        {data.tips ? <Image onClick={() => handleShow(data.tips)} src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                      </div>
                      : ''}
                    <FormInput
                      key={index + 'ddsd'}
                      inputData={data}
                      listOptions={data.options}
                      onchangeInput={onchangeInput}
                      onKeyDownInput={onKeyDownInput}
                      oncheckBox={oncheckBox}
                      onDeleteArray={onDeleteArray}
                      setData={state}
                      error={errors[data.code] ? true : false}
                      errorMessage={'This field is required.'}
                    />
                  </Col>

                  {
                    //code for logical questions
                    data.isLogical == true ?
                      data.options && data.options.map((option, indexNew) =>
                      (
                        option.isLogical == true ?
                          <div key={indexNew}>
                            {state[data.code] && state[data.code] == option.text ?
                              option.questions && option.questions.map((question, indexSecond) =>
                                <div key={indexSecond}>
                                  {question.type == 'dropDown' ?
                                    <Col md={12} key={indexSecond + 'qqqq'}>
                                      <div className="col4 fw600 fs18 mb-2">
                                        <span className="numericType1">{index + 1}.{indexSecond + 1}</span>
                                        {' '}{question.text}
                                        {question.isRequired ? <sup className="starQ">*</sup> : ''}
                                        {question.tips ? <Image onClick={() => handleShow(question.tips)} src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}

                                      </div>
                                    </Col>
                                    : ''}

                                  <Col md={12} key={indexSecond + 'ddfdf'}>
                                    {(question.type !== 'dropDown') ?
                                      <div className="col4 fw600 fs18 mb-2">
                                        <span className="numericType1">{index + 1}.{indexSecond + 1}</span>
                                        {' '}{question.text}
                                        {question.isRequired ? <sup className="starQ">*</sup> : ''}
                                        {question.tips ? <Image onClick={() => handleShow(question.tips)} src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                                      </div>
                                      : ''}
                                    <FormInput
                                      key={indexSecond + 'adfsf'}
                                      inputData={question}
                                      listOptions={question.options}
                                      onchangeInput={onchangeInput}
                                      onKeyDownInput={onKeyDownInput}
                                      oncheckBox={oncheckBox}
                                      setData={state}
                                      error={errors[question.code] ? true : false}
                                      errorMessage={(question.code == "EMPLOYEDFULLLINKEDINPRFL" || question.code == "EMPLOYEDPARTLINKEDINPRFL") && urlMsg ? urlMsg : 'This field is required.'}
                                    />
                                  </Col>
                                </div>
                              )
                              : ''}
                          </div>
                          : ''

                      ))
                      : ''
                  }

                </div>
              )
            }

          </Row>
        </div>
        <div className="footerBotton">
          <div className="hrBorder pt-3"></div>
          <div className="text-right d-flex flex-wrap justify-content-between  mt-3">
            <Button type="button" className="btnType7"
              onClick={() => props.stepSelect('fourth')}>
              Previous
            </Button>
            <Button type="button"
              onClick={() => stepSubmit()}
              className="btnType2">
              Save & Next
            </Button>
          </div>
        </div>
      </Col>


      <TipModal tipsData= {tipsData} title={"Info"} show= {show} handleClose= {handleClose}></TipModal>

    </div>


  );


}

export default FiveStep;