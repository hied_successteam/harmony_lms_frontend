export const BasicQuestions = [
  {
    "section": "basic",
    "text": "Name",
    "code": "NAME",
    "options": [],
    "placeholder": "Name of Student",
    "type": "text",
    "stream": "engineering",
    'tips': 'Your full name as it appears on official documents. You may also add Suffix, if applicable (i.e., Jr., Sr., etc.)',
    "isRequired": true,
    "NotEditable": false,
  },
  {
    "section": "basic",
    "text": "Email",
    "code": "EMAIL",
    "options": [],
    "placeholder": "Email Id",
    "type": "email",
    "stream": "engineering",
    'tips': 'List only one email address that you wish to use for communication with universities.',
    "isRequired": true,
    "disabled": true,
    "NotEditable": false,
  },
  {
    "section": "basic",
    "text": "Contact Number",
    "code": "CONTCTNUM",
    "options": [],
    "placeholder": "Contact Number",
    "type": "text",
    "stream": "engineering",
    "isRequired": true,
    "disabled": true,
    "NotEditable": false,
  },
  {
    "section": "basic",
    "text": "DOB",
    "code": "DOB",
    "options": [],
    "placeholder": "Date of birth (mm/dd/yyyy)",
    "type": "date",
    "stream": "engineering",
    "isRequired": true,
  },
  {
    "section": "basic",
    "text": "Place of Birth - City/Town, State/Province, Country",
    "code": "POBCITY",
    "options": [],
    "placeholder": "Los Angeles, California, United States",
    "type": "text",
    "stream": "engineering",
    'tips': 'Please follow the same order City – State –Country. And separate each entry with a comma.',
    "isRequired": true,
    "NotEditable": false,
  },
  {
    "section": "basic",
    "text": "Permanent Home Address - Number & Street",
    "code": "PERMANENTHOMESTREET",
    "options": [],
    "placeholder": "Number & Street",
    "type": "text",
    "stream": "engineering",
    "isRequired": true,

  },
  // {
  //   "section": "basic",
  //   "text": "Apartment#",
  //   "code": "PERMANENTHOMEAPARTMENT",
  //   "options": [],
  //   "placeholder": "Apartment#",
  //   "type": "text",
  //   "stream": "engineering",
  //   "isRequired": true,

  // },
  // {
  //   "section": "basic",
  //   "text": "State/Province Country",
  //   "code": "POBSTATE",
  //   "options": [],
  //   "placeholder": "State/Province Country",
  //   "type": "text",
  //   "stream": "engineering",
  //   "isRequired": true,
  //   "NotEditable": false,
  // },
  {
    "section": "basic",
    "text": "Country of residence",
    "code": "COUNTRY",
    "options": [],
    "placeholder": "",
    "type": "dropDown",
    "stream": "engineering",
    "isRequired": true,
    "NotEditable": false,
  },
  {
    "section": "basic",
    "text": "State",
    "code": "STATE",
    "options": [],
    "placeholder": "",
    "type": "dropDown",
    "stream": "engineering",
    "isRequired": true,

  },
  {
    "section": "basic",
    "text": "City",
    "code": "CITY",
    "options": [],
    "placeholder": "City",
    "type": "dropDown",
    "stream": "engineering",
    "isRequired": true,

  },
  {
    "section": "basic",
    "text": "Zip Code",
    "code": "ZIP",
    "options": [],
    "placeholder": "Zip Code",
    "type": "text",
    "stream": "engineering",
    "isRequired": true,

  },

  {
    "section": "basic",
    "text": "How long have you been in the US? - Months",
    "code": "HOWLONGUSMONTH",
    "options": [],
    "placeholder": "Months",
    "type": "number",
    "stream": "engineering",
    "isRequired": true,

  },
  {
    "section": "basic",
    "text": "Years",
    "code": "HOWLONGUSYEARS",
    "options": [],
    "placeholder": "Years",
    "type": "number",
    "stream": "engineering",
    "isRequired": true,

  },



  // {
  //   "section": "basic",
  //   "text": "How long have you been in the US?",
  //   "code": "HOWLONGUS",
  //   "questions": [
  //     {
  //       "text": "Month",
  //       "code": "HOWLONGUSMONTH",
  //       "type": "text",
  //       "placeholder": "Month",
  //       "isRequired": true,
  //     },
  //     {
  //       "text": "Years",
  //       "code": "HOWLONGUSYEARS",
  //       "type": "text",
  //       "placeholder": "Years",
  //       "isRequired": true,
  //     }
  //   ],
  //   "type": "combine",
  //   "stream": "engineering",
  //   "isRequired": true,
  //   "NotEditable": false,
  // },



  {
    "section": "basic",
    "text": "Is your Permanent Home Address same as your mailing address?",
    "code": "HOMEANDMAILINGADD",
    "isLogical": true,
    "isRequired": true,
    "placeholder": "",
    "type": "radio",
    "stream": "engineering",
    "options": [
      {
        "text": "Yes",
        "value": "Yes",
      },

      {
        "text": "No",
        "value": "No",
        "isLogical": true,
        "questions": [
          {
            "section": "basic",
            "text": "Current Country",
            "code": "CURRENT_COUNTRY",
            "options": [],
            "placeholder": "",
            "type": "dropDown",
            "stream": "engineering",
            "isRequired": true,
            "NotEditable": false,
          },
          {
            "section": "basic",
            "text": "Current State",
            "code": "CURRENT_STATE",
            "options": [],
            "placeholder": "",
            "type": "dropDown",
            "stream": "engineering",
            "isRequired": true,

          },
          {
            "section": "basic",
            "text": "Current City",
            "code": "CURRENT_CITY",
            "options": [],
            "placeholder": "City",
            "type": "dropDown",
            "stream": "engineering",
            "isRequired": true,

          },
          {
            "section": "basic",
            "text": "Current Mailing Address - Number & Street",
            "code": "MAILINGADDRESSSTREET",
            "options": [],
            "placeholder": "Number & Street",
            "type": "text",
            "stream": "engineering",
            "isRequired": true,

          },
          {
            "section": "basic",
            "text": "From",
            "code": "MAILINGADDRESSFROMDATE",
            "options": [],
            "placeholder": "DD/MM/YYYY",
            "type": "fullDate",
            "stream": "engineering",

          },
          {
            "section": "basic",
            "text": "To",
            "code": "MAILINGADDRESSTODATE",
            "options": [],
            "placeholder": "DD/MM/YYYY",
            "type": "fullDate",
            "stream": "engineering",
          },
        ]
      }
    ]
  },

  {
    "section": "basic",
    "text": "Do you use a different Whatsapp Number?",
    "code": "ISWHATSAPPNO",
    "isLogical": true,
    "options": [
      {
        "text": "Yes",
        "value": "Yes",
        "isLogical": true,
        "questions": [
          {
            "isRequired": true,
            "text": "Whatsapp Number",
            "code": "WHATSAPPNO",
            "type": "text",
            "placeholder": "Whatsapp Number",
          }
        ]
      },

      {
        "text": "No",
        "value": "No"
      },
      {
        "text": "Not a Whatsapp user",
        "value": "Not a Whatsapp user"
      }

    ],
    "placeholder": "",
    "type": "radio",
    "stream": "engineering",
    "isRequired": true
  },

  {
    "section": "basic",
    "type": "radio",
    "text": "Gender",
    "code": "GENDER",
    "placeholder": "",
    "options": [
      {
        "text": "Male",
        "value": "2"

      },
      {
        "text": "Female",
        "value": "3"
      },
      {
        "text": "Non-Binary",
        "value": "Non-Binary"
      },
      {
        "text": "Prefer not to say",
        "value": "Prefer not to say"
      }
    ],
    "stream": "engineering",
    "isRequired": true
  },
  {
    "section": "basic",
    "text": "Are you US Armed Services Veteran?",
    "code": "ARMEDSERVICES",
    "isRequired": true,
    "placeholder": "",
    "type": "radio",
    "stream": "engineering",
    "options": [
      {
        "text": "Yes",
        "value": "Yes",
      },

      {
        "text": "No",
        "value": "No"
      }
    ]
  },
  {
    "section": "basic",
    "type": "dropDown",
    "stream": "engineering",
    "text": "Religious Affiliation",
    "code": "RELAFFIL",
    "placeholder": "",
    "isRequired": false,
    "options": [
      {
        "text": "Buddhism",
        "value": "Buddhism"
      },
      {
        "text": "Catholicism",
        "value": "Catholicism"
      },
      {
        "text": "Hinduism",
        "value": "Hinduism"
      },
      {
        "text": "Islam",
        "value": "Islam"
      },
      {
        "text": "Judaism",
        "value": "Judaism"
      },
      {
        "text": "Mormonism",
        "value": "Mormonism"
      },
      {
        "text": "Non-religious",
        "value": "Non-religious"
      },
      {
        "text": "Other",
        "value": "Other"
      },
      {
        "text": "Protestantism",
        "value": "Protestantism"
      },
      {
        "text": "Sikhism",
        "value": "Sikhism"
      },
      {
        "text": "Wicca, Paganism, or other Pantheist Spirituality",
        "value": "Wicca, Paganism, or other Pantheist Spirituality"
      },
      {
        "text": "Prefer not to share",
        "value": "Prefer not to share"
      }
    ]
  },

  {
    "section": "basic",
    "type": "dropDown",
    "stream": "engineering",
    "text": "Visa Status",
    "code": "VISASTATUS",
    "placeholder": "Visa Status",
    "isRequired": true,
    "options": [
      {
        "text": "U.S. citizen",
        "value": "U.S. citizen"
      },
      {
        "text": "Permanent Resident of U.S.",
        "value": "Permanent Resident of U.S."
      },
      {
        "text": "Foreign academic student, when certain conditions are met (F-1)",
        "value": "Foreign academic student, when certain conditions are met (F-1)"
      },
      {
        "text": "Temporary worker (H-1B, H-1C, H-2A, H-2B, H-3)",
        "value": "Temporary worker (H-1B, H-1C, H-2A, H-2B, H-3)"
      },
      {
        "text": "Tourist/Business visa (B-1,B-2)",
        "value": "Tourist/Business visa (B-1,B-2)"
      },
      {
        "text": "Foreign information media representative (I)",
        "value": "Foreign information media representative (I)"
      },
      {
        "text": "Exchange visitor, when certain conditions are met (J-1)",
        "value": "Exchange visitor, when certain conditions are met (J-1)"
      },
      {
        "text": "Fiancé of a U.S. citizen (K-1)",
        "value": "Fiancé of a U.S. citizen (K-1)"
      },
      {
        "text": "Intra-company transferee (L-1)",
        "value": "Intra-company transferee (L-1)"
      },
      {
        "text": "Foreign vocational student (M-1)",
        "value": "Foreign vocational student (M-1)"
      },
      {
        "text": "Temporary worker in the sciences (O-1, O-2)",
        "value": "Temporary worker in the sciences (O-1, O-2)"
      },
      {
        "text": "Temporary worker in arts, athletics in exchange/cultural event (P-1, P-2, P-3)",
        "value": "Temporary worker in arts, athletics in exchange/cultural event (P-1, P-2, P-3)"
      },
      {
        "text": "Green card or Form I-551",
        "value": "Green card or Form I-551"
      },
      {
        "text": "T-1 or Parent with T-1",
        "value": "T-1 or Parent with T-1"
      },
      {
        "text": "Not applicable",
        "value": "Not applicable"
      }
    ]
  },

  {
    "section": "basic",
    "type": "radio",
    "text": "Do you have a Dual Citizenship?",
    "code": "DUALCITIZENSHIP",
    "placeholder": "",
    "options": [
      {
        "text": "Yes",
        "value": "Yes"

      },
      {
        "text": "No",
        "value": "No"
      },
    ],
    "stream": "engineering",
    "isRequired": true
  },

  {
    "section": "basic",
    "type": "dropDown",
    "stream": "engineering",
    "text": "Martial Status",
    "code": "MARITALSTATUS",
    "placeholder": "Martial Status",
    "isRequired": true,
    "options": [
      {
        "text": "Never married",
        "value": "Never married"
      },
      {
        "text": "Married",
        "value": "Married"
      },
      {
        "text": "Widowed",
        "value": "Widowed"
      },
      {
        "text": "Separated",
        "value": "Separated"
      },
      {
        "text": "Divorced",
        "value": "Divorced"
      },

    ]
  },

]

export const AcademicInfo = [
  {
    "section": "academicInfo",
    "isRequired": true,
    "NotEditable": false,
    "type": "dropDown",
    "stream": "engineering",
    "text": "What is your current progress in finding the suitable course/college in the U.S.?",
    "code": "CURRPROGCOLL",
    "placeholder": "",
    'tips': 'Your present status in terms of locating an appropriate course or college. For example, you can select “Seeking info. about college/ course” if you are trying to find information about a college/ course. Or you can select any of the other options if you have started the application process for any of the U.S Colleges.',
    "options": [
      {
        "text": "Seeking out info. about College/Course",
        "value": "Seeking out info. about College/Course"
      },
      {
        "text": "Filling out application form(s)",
        "value": "Filling out application form(s)"
      },
      {
        "text": "Submitted application(s)",
        "value": "Submitted application(s)"
      },
      {
        "text": "Received offer from preferred institute(s)",
        "value": "Received offer from preferred institute(s)"
      },
      {
        "text": "Accepted offer",
        "value": "Accepted offer"
      },
      {
        "text": "Successfully enrolled at College/Uni.",
        "value": "Successfully enrolled at College/Uni."
      }
    ],

  },
  {
    "section": "academicInfo",
    "isRequired": true,
    "NotEditable": false,
    "isLogical": true,
    "type": "combine",
    "stream": "engineering",
    "text": "Which batch (fall/spring) would you like to apply for?",
    "code": "SEMESTER",
    "placeholder": "",
    'tips': 'U.S colleges have 3 admission terms i.e., Fall (September), Spring (January), Summer (May).',
    "questions": [
      {
        "section": "academicInfo",
        "isRequired": true,
        "NotEditable": false,
        "type": "dropDown",
        "stream": "engineering",
        "text": "Semester",
        "code": "SEMESTER",
        "placeholder": "",
        "options": [
          {
            "text": "Summer",
            "value": "Summer",
          },
          {
            "text": "Spring",
            "value": "Spring"
          },
          {
            "text": "Fall",
            "value": "Fall"
          },
          {
            "text": "Winter",
            "value": "Winter"
          }
        ]
      },
      {
        "section": "academicInfo",
        "isRequired": true,
        "NotEditable": false,
        "type": "dropDown",
        "stream": "engineering",
        "text": "Year",
        "code": "SEMESTER_YEAR",
        "placeholder": "",
        "options": [
          {
            "text": new Date().getFullYear(),
            "value": new Date().getFullYear(),
          },
          {
            "text": new Date().getFullYear() + 1,
            "value": new Date().getFullYear() + 1
          },
          {
            "text": new Date().getFullYear() + 2,
            "value": new Date().getFullYear() + 2
          },
          {
            "text": new Date().getFullYear() + 3,
            "value": new Date().getFullYear() + 3
          },
          {
            "text": new Date().getFullYear() + 4,
            "value": new Date().getFullYear() + 4
          }
        ]
      },
    ]
    // "options": [
    //   {
    //     "text": "Spring 2022",
    //     "value": "Spring 2022"
    //   },
    //   {
    //     "text": "Fall 2022",
    //     "value": "Fall 2022"
    //   },
    //   {
    //     "text": "Spring 2023",
    //     "value": "Spring 2023"
    //   },
    //   {
    //     "text": "Fall 2023 or later",
    //     "value": "Fall 2023 or later"
    //   }
    // ],
  },
  {
    "section": "academicInfo",
    "isRequired": true,
    "isLogical": true,
    "type": "radio",
    "stream": "engineering",
    "text": "Which is your first (native) language?",
    "code": "NATIVELANGUAGE",
    "placeholder": "",
    "options": [
      {
        "text": "English",
        "value": "English",
        "isLogical": true,
        "isRequired": true,
        "questions": [
          {
            "section": "academicInfo",
            "isRequired": true,
            "NotEditable": false,
            "type": "dropDown",
            "stream": "engineering",
            "text": "How well do you speak English?",
            "code": "ENGLISH_FROFI",
            "placeholder": "",
            "options": [
              {
                "text": "Fluent",
                "value": "Fluent",
              },
              {
                "text": "Very well",
                "value": "Very well"
              },
              {
                "text": "Well",
                "value": "Well"
              },
              {
                "text": "Not well",
                "value": "Not well"
              },
              {
                "text": "Not at all",
                "value": "Not at all"
              },
            ]
          },
        ]
      },
      {
        "text": "Other",
        "value": "Other",
        "isLogical": true,
        "isRequired": true,
        "questions": [
          {
            "isRequired": true,
            "text": "Other",
            "code": "OTHER_NATIVELANGUAGE",
            "type": "text"
          },
          {
            "section": "academicInfo",
            "isRequired": true,
            "NotEditable": false,
            "type": "dropDown",
            "stream": "engineering",
            "text": "How well do you speak in the other language?",
            "code": "OTHER_FROFI",
            "placeholder": "",
            "options": [
              {
                "text": "Fluent",
                "value": "Fluent",
              },
              {
                "text": "Very well",
                "value": "Very well"
              },
              {
                "text": "Well",
                "value": "Well"
              },
              {
                "text": "Not well",
                "value": "Not well"
              },
              {
                "text": "Not at all",
                "value": "Not at all"
              },
            ]
          },
        ]
      },
    ]
  },
  {
    "section": "academicInfo",
    "isRequired": true,
    "isLogical": true,
    "NotEditable": false,
    "type": "dropDown",
    "stream": "engineering",
    "text": "What is the highest degree or the level of education you have completed?",
    "tips": 'Select the highest degree you have completed or the degree you are pursuing if you are currently enrolled in school/college.',
    "code": "HIGHDEGEDU",
    "placeholder": "",
    "options": [
      {
        "text": "Less than high school (Less than Grade-12)",
        "value": "Less than high school (Less than Grade-12)",
        "isLogical": true,
        "isRequired": true,
        "questions": [
          {
            "isRequired": true,
            "text": "School Name",
            "code": "SCHOOLNAME",
            "type": "text"
          },
          {
            "isRequired": true,
            "text": "Current Grade/Class/Level",
            "code": "CURRENTGRADECLASS",
            "type": "dropDown",
            "options": [
              {
                "text": "Less than middle school",
                "value": "Less than middle school"
              },
              {
                "text": "Grade 8",
                "value": "Grade 8"
              },
              {
                "text": "Grade 9",
                "value": "Grade 9"
              },
              {
                "text": "Grade 10",
                "value": "Grade 10"
              },
              {
                "text": "Grade 11",
                "value": "Grade 11"
              },
            ]
          },
          {
            "text": "Grading System",
            "type": "dropDown",
            "isRequired": true,

            "code": "GRADINGSYSTEM",
            "options": [
              {
                "text": "Lettergrade (A/A+)",
                "value": "Lettergrade (A/A+)",
              },
              {
                "text": "CGPA (10)",
                "value": "CGPA (10)"
              },
              {
                "text": "Weighted (out of 5)",
                "value": "Weighted (out of 5)"
              },
              {
                "text": "Unweighted (out of 4)",
                "value": "Unweighted (out of 4)"
              },
              {
                "text": "Iranian system (out of 20)",
                "value": "Iranian system (out of 20)"
              },
              {
                "text": "Percentage (100)",
                "value": "Percentage (100)"
              }
            ]
          },
          {
            "isRequired": true,
            "NotEditable": false,
            "text": "Marks Scored (CGPA/Percentage)",
            "type": "number",
            "code": "MARKSSCORED"
          },
          {
            "section": "academicInfo",
            "isLogical": true,
            "isRequired": true,

            "text": " Which entrance exams have you appeared for recently or planning to take?",
            "type": "checkbox",
            "code": "ENTRANCEAPPEARED",
            "tips": 'If you have appeared for any of the entrance exams, select all that apply to you. If you have not appeared for any of the exams, select the entrance exams which you would likely appear for in the future.',
            "options": [
              {
                "text": "TOEFL",
                "value": "TOEFL",
                "heading": "Enter your TOEFL Scores",
                "isLogical": true,
                "isRequired": true,
                "isCalculate": true,
                "questions": [
                  {
                    "section": "academicInfo",
                    "isRequired": true,
                    "isLogical": true,
                    "type": "radio",
                    'tips': "If you have appeared for the exam and you have your scores ready, select “Taken” or select “Planning” and give your expected scores.",
                    "stream": "engineering",
                    "text": "Have you taken TOEFL before or are planning to take it?",
                    "code": "TOEFL_PLANNING",
                    "placeholder": "",
                    "options": [
                      {
                        "text": "Planning",
                        "value": "Planning"
                      },
                      {
                        "text": "Taken",
                        "value": "Taken",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                          {
                            "isRequired": true,
                            "text": "Date Taken",
                            "code": "TOEFL_TAKEN_DATE",
                            "type": "fullDate"
                          }
                        ]
                      },
                    ]
                  },
                  {
                    "text": "Reading (0-30)",
                    "code": "TOEFL_READING",
                    "isRequired": true,
                    "placeholder": "Reading",
                    "minValue": 0,
                    "maxValue": 30,
                    "type": "number"
                  },
                  {
                    "text": "Listening (0-30)",
                    "code": "TOEFL_LISTENING",
                    "placeholder": "Listening",
                    "isRequired": true,
                    "minValue": 0,
                    "maxValue": 30,
                    "type": "number"
                  },
                  {
                    "text": "Speaking (0-30)",
                    "code": "TOEFL_SPEAKING",
                    "placeholder": "Speaking",
                    "isRequired": true,
                    "minValue": 0,
                    "maxValue": 30,
                    "type": "number"
                  },
                  {
                    "text": "Writing (0-30)",
                    "code": "TOEFL_WRITING",
                    "placeholder": "Writing",
                    "isRequired": true,
                    "minValue": 0,
                    "maxValue": 30,
                    "type": "number"
                  },
                  {
                    "text": "Total (0-120)",
                    "code": "TOEFL_TOTAL",
                    "placeholder": "Total",
                    "isRequired": true,
                    "minValue": 0,
                    "maxValue": 120,
                    "type": "number"
                  },

                ]
              },
              {
                "text": "SAT",
                "value": "SAT",
                "heading": "Enter your SAT Scores",
                "isLogical": true,
                "isRequired": true,

                "isCalculate": true,
                "questions": [
                  {
                    "section": "academicInfo",
                    "isRequired": true,

                    "type": "radio",
                    "stream": "engineering",
                    'tips': "If you have appeared for the exam and you have your scores ready, select “Taken” or select “Planning” and give your expected scores.",
                    "text": "Have you taken SAT before or are planning to take it?",
                    "code": "SAT_PLANNING",
                    "placeholder": "",
                    "options": [
                      {
                        "text": "Planning",
                        "value": "Planning"
                      },
                      {
                        "text": "Taken",
                        "value": "Taken",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                          {
                            "isRequired": true,
                            "text": "Date Taken",
                            "code": "SAT_TAKEN_DATE",
                            "type": "fullDate"
                          }
                        ]
                      },
                    ]
                  },
                  {
                    "text": "EBRW Score (200-800)",
                    "code": "SAT_EBRW",
                    "placeholder": "EBRW",
                    "isRequired": true,

                    "minValue": 200,
                    "maxValue": 800,
                    "type": "number"
                  },
                  {
                    "text": "Math Score (200-800)",
                    "code": "SAT_MATH",
                    "placeholder": "Math",
                    "isRequired": true,

                    "minValue": 200,
                    "maxValue": 800,
                    "type": "number"
                  },
                  {
                    "text": "Total Score (400-1600)",
                    "code": "SAT_TOTAL",
                    "placeholder": "Total",
                    "isRequired": true,

                    "minValue": 400,
                    "maxValue": 1600,
                    "type": "number"
                  },

                ]
              },
              {
                "text": "ACT",
                "value": "ACT",
                "heading": "Enter your ACT Scores",
                "isLogical": true,
                "isRequired": true,

                "questions": [
                  {
                    "section": "academicInfo",
                    "isRequired": true,

                    "type": "radio",
                    "stream": "engineering",
                    'tips': "If you have appeared for the exam and you have your scores ready, select “Taken” or select “Planning” and give your expected scores.",
                    "text": "Have you taken ACT before or are planning to take it?",
                    "code": "ACT_PLANNING",
                    "placeholder": "",
                    "options": [
                      {
                        "text": "Planning",
                        "value": "Planning"
                      },
                      {
                        "text": "Taken",
                        "value": "Taken",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                          {
                            "isRequired": true,
                            "text": "Date Taken",
                            "code": "ACT_TAKEN_DATE",
                            "type": "fullDate"
                          }
                        ]
                      },
                    ]
                  },
                  {
                    "text": "English (1-36)",
                    "code": "ACT_ENGLISH",
                    "placeholder": "English",
                    "isRequired": true,

                    "minValue": 1,
                    "maxValue": 36,
                    "type": "number"
                  },
                  {
                    "text": "Math (1-36)",
                    "code": "ACT_MATH",
                    "placeholder": "Math",
                    "isRequired": true,

                    "minValue": 1,
                    "maxValue": 36,
                    "type": "number"
                  },
                  {
                    "text": "Reading (1-36)",
                    "code": "ACT_READING",
                    "placeholder": "Reading",
                    "isRequired": true,

                    "minValue": 1,
                    "maxValue": 36,
                    "type": "number"
                  },
                  {
                    "text": "Science  (1-36)",
                    "code": "ACT_SCIENCE",
                    "placeholder": "Science",
                    "isRequired": true,

                    "minValue": 1,
                    "maxValue": 36,
                    "type": "number"
                  },
                  {
                    "text": "Composite Score (1-36)",
                    "code": "ACT_COMPOSITESCORE",
                    "placeholder": "Score",
                    "isRequired": true,

                    "minValue": 1,
                    "maxValue": 36,
                    "type": "number"
                  },

                ]
              },
              {
                "text": "IELTS",
                "value": "IELTS",
                "heading": "Enter your IELTS Scores",
                "isLogical": true,
                "isRequired": true,

                "questions": [
                  {
                    "section": "academicInfo",
                    "isRequired": true,
                    "type": "radio",
                    "stream": "engineering",
                    'tips': "If you have appeared for the exam and you have your scores ready, select “Taken” or select “Planning” and give your expected scores.",
                    "text": "Have you taken IELTS before or are planning to take it?",
                    "code": "IELTS_PLANNING",
                    "placeholder": "",
                    "options": [
                      {
                        "text": "Planning",
                        "value": "Planning"
                      },
                      {
                        "text": "Taken",
                        "value": "Taken",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                          {
                            "isRequired": true,
                            "text": "Date Taken",
                            "code": "IELTS_TAKEN_DATE",
                            "type": "fullDate"
                          }
                        ]
                      },
                    ]
                  },
                  {
                    "text": "Listening (1-9)",
                    "code": "IELTS_LISTENING",
                    "isRequired": true,
                    "placeholder": "Listening",
                    "minValue": 1,
                    "maxValue": 9,
                    "type": "float"
                  },
                  {
                    "text": "Reading (1-9)",
                    "code": "IELTS_READING",
                    "placeholder": "Reading",
                    "isRequired": true,
                    "minValue": 1,
                    "maxValue": 9,
                    "type": "float"
                  },
                  {
                    "text": "Writing (1-9)",
                    "code": "IELTS_WRITING",
                    "placeholder": "Writing",
                    "isRequired": true,
                    "minValue": 1,
                    "maxValue": 9,
                    "type": "float"
                  },
                  {
                    "text": "Speaking (1-9)",
                    "code": "IELTS_SPEAKING",
                    "placeholder": "Speaking",
                    "isRequired": true,
                    "minValue": 1,
                    "maxValue": 9,
                    "type": "float"
                  },
                  {
                    "text": "Band Score (1-9)",
                    "code": "IELTS_BANDSCORE",
                    "placeholder": "Score",
                    "isRequired": true,
                    "minValue": 1,
                    "maxValue": 9,
                    "type": "float"
                  },

                ]
              },
              {
                "text": "PSAT/NMSQT",
                "value": "PSAT/NMSQT",
                "heading": "Enter your PSAT/NMSQT Scores",
                "isLogical": true,
                "isRequired": true,
                "isCalculate": true,

                "questions": [
                  {
                    "section": "academicInfo",
                    "isRequired": true,

                    "type": "radio",
                    "stream": "engineering",
                    'tips': "If you have appeared for the exam and you have your scores ready, select “Taken” or select “Planning” and give your expected scores.",
                    "text": "Have you taken PSAT/NMSQT before or are planning to take it?",
                    "code": "PSAT_NMSQT_PLANNING",
                    "placeholder": "",
                    "options": [
                      {
                        "text": "Planning",
                        "value": "Planning"
                      },
                      {
                        "text": "Taken",
                        "value": "Taken",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                          {
                            "isRequired": true,
                            "text": "Date Taken",
                            "code": "PSAT_NMSQT_TAKEN_DATE",
                            "type": "fullDate"
                          }
                        ]
                      },
                    ]
                  },
                  {
                    "text": "EBRW Score (160-760)",
                    "code": "PSAT_NMSQT_EBRW",
                    "isRequired": true,

                    "placeholder": "EBRW",
                    "minValue": 160,
                    "maxValue": 760,
                    "type": "number"
                  },
                  {
                    "text": "Math Score (160-760)",
                    "code": "PSAT_NMSQT_MATH",
                    "placeholder": "Math",
                    "isRequired": true,

                    "minValue": 160,
                    "maxValue": 760,
                    "type": "number"
                  },
                  {
                    "text": "Total Score (320-1520)",
                    "code": "PSAT_NMSQT_TOTAL",
                    "placeholder": "Total",
                    "isRequired": true,

                    "minValue": 320,
                    "maxValue": 1520,
                    "type": "number"
                  },

                ]
              },
              // {
              //   "text": "Not Applicable",
              //   "value": "Not Applicable",
              //   "heading": "",
              //   "isLogical": true,

              //   "questions": [
              //     {
              //       "text": "If No when you are planning to  appear for entrance exams ? *\n *For admissions in most US colleges, you need to provide atleast 1 test score.",
              //       "code": "ENTRANCEAPPEARED_NOT_APPLI",
              //       "type": "checkbox",
              //       "isRequired": true,

              //       "options": [
              //         {
              //           "text": "3 months",
              //           "value": "3 months"
              //         },
              //         {
              //           "text": "6 months",
              //           "value": "6 months"
              //         },
              //         {
              //           "text": "6 - 12 months",
              //           "value": "6 - 12 months"
              //         },
              //         {
              //           "text": "More than a year",
              //           "value": "More than a year"
              //         }
              //       ]
              //     }
              //   ]
              // }
            ]
          },
          {
            "section": "academicInfo",
            "isRequired": true,

            "text": "Which educational degree would you like to pursue next?",
            "isNextQuestion": true,
            "type": "dropDown",
            "code": "EDUDEGNEXT",
            "options": [
              {
                "text": "Professional Certificate",
                "value": "Professional Certificate"
              },
              {
                "text": "Associates",
                "value": "Associates"
              },
              {
                "text": "Diploma",
                "value": "Diploma"
              },
              {
                "text": "Integrated Programs",
                "value": "Integrated Programs"
              },
              {
                "text": "Bachelor (BA/BS)",
                "value": "Bachelor (BA/BS)"
              },
              {
                "text": "Occupational Associates",
                "value": "Occupational Associates"
              },
              {
                "text": "Vocational Training",
                "value": "Vocational Training"
              },
              {
                "text": "Summer Internships",
                "value": "Summer Internships"
              }
            ]
          }
        ]
      },
      {
        "text": "High school degree or equivalent (Grade-12)",
        "value": "High school degree or equivalent (Grade-12)",
        "isLogical": true,
        "isRequired": true,
        "questions": [
          {
            "text": "High School Name",
            "code": "SCHOOLNAME",
            "isRequired": true,
            "type": "text"
          },
          {
            "text": "Country",
            "code": "HIGH_SCHL_COUNTRY",
            "options": [],
            "placeholder": "",
            "type": "dropDown",
            "stream": "engineering",
            "isRequired": true,
            "NotEditable": false,
          },
          {
            "text": "State/Province",
            "code": "HIGH_SCHL_STATE",
            "options": [],
            "placeholder": "",
            "type": "dropDown",
            "stream": "engineering",
            "isRequired": true,
            "NotEditable": false,
          },
          {
            "text": "City/Town",
            "code": "HIGH_SCHL_CITY",
            "options": [],
            "placeholder": "",
            "type": "dropDown",
            "stream": "engineering",
            "isRequired": true,
            "NotEditable": false,
          },
          {
            "text": "Address (Number & Street)",
            "code": "HIGH_SCHL_ADD",
            "options": [],
            "placeholder": "Address (Number & Street)",
            "type": "text",
            "stream": "engineering",
            "NotEditable": false,
          },
          {
            "text": "ZIP/Postal Code",
            "code": "HIGH_SCHL_ZIP",
            "options": [],
            "placeholder": "ZIP/Postal Code",
            "type": "number",
            "stream": "engineering",
            "NotEditable": false,
          },
          {
            "text": "Date of High School Graduation",
            "code": "HIGH_SCHL_DATE",
            "options": [],
            "placeholder": "mm/dd/yyyy",
            "type": "fullDate",
            "stream": "engineering",
            "NotEditable": false,
          },
          {
            "text": "Grading System",
            "type": "dropDown",
            "isRequired": true,

            "code": "GRADINGSYSTEM",
            "options": [
              {
                "text": "Lettergrade (A/A+)",
                "value": "Lettergrade (A/A+)"
              },
              {
                "text": "CGPA (10)",
                "value": "CGPA (10)"
              },
              {
                "text": "Weighted (out of 5)",
                "value": "Weighted (out of 5)"
              },
              {
                "text": "Unweighted (out of 4)",
                "value": "Unweighted (out of 4)"
              },
              {
                "text": "Iranian system (out of 20)",
                "value": "Iranian system (out of 20)"
              },
              {
                "text": "Percentage (100)",
                "value": "Percentage (100)"
              }
            ]
          },
          {
            "text": "Marks Scored (CGPA/Percentage)",
            "type": "number",
            "isRequired": true,

            "code": "MARKSSCORED"
          },
          {
            "text": " Which entrance exams have you appeared for recently or planning to take?",
            "type": "checkbox",
            "isRequired": true,
            "isLogical": true,
            "code": "ENTRANCEAPPEARED",
            "tips": 'If you have appeared for any of the entrance exams, select all that apply to you. If you have not appeared for any of the exams, select the entrance exams which you would likely appear for in the future.',
            "options": [
              {
                "text": "TOEFL",
                "value": "TOEFL",
                "heading": "Enter your TOEFL Scores",
                "isLogical": true,
                "isRequired": true,
                "isCalculate": true,

                "questions": [
                  {
                    "section": "academicInfo",
                    "isRequired": true,

                    "type": "radio",
                    "stream": "engineering",
                    'tips': "If you have appeared for the exam and you have your scores ready, select “Taken” or select “Planning” and give your expected scores.",
                    "text": "Have you taken TOEFL before or are planning to take it?",
                    "code": "TOEFL_PLANNING",
                    "placeholder": "",
                    "options": [
                      {
                        "text": "Planning",
                        "value": "Planning"
                      },
                      {
                        "text": "Taken",
                        "value": "Taken",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                          {
                            "isRequired": true,
                            "text": "Date Taken",
                            "code": "TOEFL_TAKEN_DATE",
                            "type": "fullDate"
                          }
                        ]
                      },
                    ]
                  },
                  {
                    "text": "Reading (0-30)",
                    "code": "TOEFL_READING",
                    "placeholder": "Reading",
                    "isRequired": true,

                    "minValue": 0,
                    "maxValue": 30,
                    "type": "number"
                  },
                  {
                    "text": "Listening (0-30)",
                    "code": "TOEFL_LISTENING",
                    "placeholder": "Listening",
                    "isRequired": true,

                    "minValue": 0,
                    "maxValue": 30,
                    "type": "number"
                  },
                  {
                    "text": "Speaking (0-30)",
                    "code": "TOEFL_SPEAKING",
                    "placeholder": "Speaking",
                    "isRequired": true,

                    "minValue": 0,
                    "maxValue": 30,
                    "type": "number"
                  },
                  {
                    "text": "Writing (0-30)",
                    "code": "TOEFL_WRITING",
                    "placeholder": "Writing",
                    "isRequired": true,

                    "minValue": 0,
                    "maxValue": 30,
                    "type": "number"
                  },
                  {
                    "text": "Total (0-120)",
                    "code": "TOEFL_TOTAL",
                    "placeholder": "Total",
                    "isRequired": true,

                    "minValue": 0,
                    "maxValue": 120,
                    "type": "number"
                  },

                ]
              },
              {
                "text": "SAT",
                "value": "SAT",
                "heading": "Enter your SAT Scores",
                "isLogical": true,
                "isRequired": true,
                "isCalculate": true,

                "questions": [
                  {
                    "section": "academicInfo",
                    "isRequired": true,

                    "type": "radio",
                    "stream": "engineering",
                    'tips': "If you have appeared for the exam and you have your scores ready, select “Taken” or select “Planning” and give your expected scores.",
                    "text": "Have you taken SAT before or are planning to take it?",
                    "code": "SAT_PLANNING",
                    "placeholder": "",
                    "options": [
                      {
                        "text": "Planning",
                        "value": "Planning"
                      },
                      {
                        "text": "Taken",
                        "value": "Taken",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                          {
                            "isRequired": true,
                            "text": "Date Taken",
                            "code": "SAT_TAKEN_DATE",
                            "type": "fullDate"
                          }
                        ]
                      },
                    ]
                  },
                  {
                    "text": "EBRW Score (200-800)",
                    "code": "SAT_EBRW",
                    "placeholder": "EBRW",
                    "isRequired": true,

                    "minValue": 200,
                    "maxValue": 800,
                    "type": "number"
                  },
                  {
                    "text": "Math Score (200-800)",
                    "code": "SAT_MATH",
                    "placeholder": "Math",
                    "isRequired": true,

                    "minValue": 200,
                    "maxValue": 800,
                    "type": "number"
                  },
                  {
                    "text": "Total Score (400-1600)",
                    "code": "SAT_TOTAL",
                    "placeholder": "Total",
                    "isRequired": true,

                    "minValue": 400,
                    "maxValue": 1600,
                    "type": "number"
                  },

                ]
              },
              {
                "text": "ACT",
                "value": "ACT",
                "heading": "Enter your ACT Scores",
                "isLogical": true,
                "isRequired": true,

                "questions": [
                  {
                    "section": "academicInfo",
                    "isRequired": true,

                    "type": "radio",
                    "stream": "engineering",
                    'tips': "If you have appeared for the exam and you have your scores ready, select “Taken” or select “Planning” and give your expected scores.",
                    "text": "Have you taken ACT before or are planning to take it?",
                    "code": "ACT_PLANNING",
                    "placeholder": "",
                    "options": [
                      {
                        "text": "Planning",
                        "value": "Planning"
                      },
                      {
                        "text": "Taken",
                        "value": "Taken",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                          {
                            "isRequired": true,
                            "text": "Date Taken",
                            "code": "ACT_TAKEN_DATE",
                            "type": "fullDate"
                          }
                        ]
                      },
                    ]
                  },
                  {
                    "text": "English (1-36)",
                    "code": "ACT_ENGLISH",
                    "placeholder": "English",
                    "isRequired": true,

                    "minValue": 1,
                    "maxValue": 36,
                    "type": "number"
                  },
                  {
                    "text": "Math (1-36)",
                    "code": "ACT_MATH",
                    "placeholder": "Math",
                    "isRequired": true,

                    "minValue": 1,
                    "maxValue": 36,
                    "type": "number"
                  },
                  {
                    "text": "Reading (1-36)",
                    "code": "ACT_READING",
                    "placeholder": "Reading",
                    "isRequired": true,

                    "minValue": 1,
                    "maxValue": 36,
                    "type": "number"
                  },
                  {
                    "text": "Science  (1-36)",
                    "code": "ACT_SCIENCE",
                    "placeholder": "Science",
                    "isRequired": true,

                    "minValue": 1,
                    "maxValue": 36,
                    "type": "number"
                  },
                  {
                    "text": "Composite Score (1-36)",
                    "code": "ACT_COMPOSITESCORE",
                    "placeholder": "Score",
                    "isRequired": true,

                    "minValue": 1,
                    "maxValue": 36,
                    "type": "number"
                  },

                ]
              },
              {
                "text": "IELTS",
                "value": "IELTS",
                "heading": "Enter your IELTS Scores",
                "isLogical": true,
                "isRequired": true,

                "questions": [
                  {
                    "section": "academicInfo",
                    "isRequired": true,

                    "type": "radio",
                    "stream": "engineering",
                    'tips': "If you have appeared for the exam and you have your scores ready, select “Taken” or select “Planning” and give your expected scores.",
                    "text": "Have you taken IELTS before or are planning to take it?",
                    "code": "IELTS_PLANNING",
                    "placeholder": "",
                    "options": [
                      {
                        "text": "Planning",
                        "value": "Planning"
                      },
                      {
                        "text": "Taken",
                        "value": "Taken",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                          {
                            "isRequired": true,
                            "text": "Date Taken",
                            "code": "IELTS_TAKEN_DATE",
                            "type": "fullDate"
                          }
                        ]
                      },
                    ]
                  },
                  {
                    "text": "Listening (1-9)",
                    "code": "IELTS_LISTENING",
                    "isRequired": true,
                    "placeholder": "Listening",
                    "minValue": 1,
                    "maxValue": 9,
                    "type": "float"
                  },
                  {
                    "text": "Reading (1-9)",
                    "code": "IELTS_READING",
                    "placeholder": "Reading",
                    "isRequired": true,

                    "minValue": 1,
                    "maxValue": 9,
                    "type": "float"
                  },
                  {
                    "text": "Writing (1-9)",
                    "code": "IELTS_WRITING",
                    "placeholder": "Writing",
                    "isRequired": true,

                    "minValue": 1,
                    "maxValue": 9,
                    "type": "float"
                  },
                  {
                    "text": "Speaking (1-9)",
                    "code": "IELTS_SPEAKING",
                    "placeholder": "Speaking",
                    "isRequired": true,

                    "minValue": 1,
                    "maxValue": 9,
                    "type": "float"
                  },
                  {
                    "text": "Band Score (1-9)",
                    "code": "IELTS_BANDSCORE",
                    "placeholder": "Score",
                    "isRequired": true,

                    "minValue": 1,
                    "maxValue": 9,
                    "type": "float"
                  },

                ]
              },
              {
                "text": "PSAT/NMSQT",
                "value": "PSAT/NMSQT",
                "heading": "Enter your PSAT/NMSQT Scores",
                "isLogical": true,
                "isRequired": true,
                "isCalculate": true,

                "questions": [
                  {
                    "section": "academicInfo",
                    "isRequired": true,

                    "type": "radio",
                    "stream": "engineering",
                    'tips': "If you have appeared for the exam and you have your scores ready, select “Taken” or select “Planning” and give your expected scores.",
                    "text": "Have you taken PSAT/NMSQT before or are planning to take it?",
                    "code": "PSAT_NMSQT_PLANNING",
                    "placeholder": "",
                    "options": [
                      {
                        "text": "Planning",
                        "value": "Planning"
                      },
                      {
                        "text": "Taken",
                        "value": "Taken",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                          {
                            "isRequired": true,
                            "text": "Date Taken",
                            "code": "PSAT_NMSQT_TAKEN_DATE",
                            "type": "fullDate"
                          }
                        ]
                      },
                    ]
                  },
                  {
                    "text": "EBRW Score (160-760)",
                    "code": "PSAT_NMSQT_EBRW",
                    "placeholder": "EBRW",
                    "isRequired": true,

                    "minValue": 160,
                    "maxValue": 760,
                    "type": "number"
                  },
                  {
                    "text": "Math Score (160-760)",
                    "code": "PSAT_NMSQT_MATH",
                    "placeholder": "Math",
                    "isRequired": true,

                    "minValue": 160,
                    "maxValue": 760,
                    "type": "number"
                  },
                  {
                    "text": "Total Score (320-1520)",
                    "code": "PSAT_NMSQT_TOTAL",
                    "placeholder": "Total",
                    "isRequired": true,

                    "minValue": 320,
                    "maxValue": 1520,
                    "type": "number"
                  },

                ]
              },
              // {
              //   "text": "Not Applicable",
              //   "value": "Not Applicable",
              //   "heading": "",
              //   "isLogical": true,

              //   "questions": [
              //     {
              //       "text": "If No when you are planning to  appear for entrance exams ? *\n *For admissions in most US colleges, you need to provide atleast 1 test score.",
              //       "code": "ENTRANCEAPPEARED_NOT_APPLI",
              //       "type": "checkbox",
              //       "isRequired": true,

              //       "options": [
              //         {
              //           "text": "3 months",
              //           "value": "3 months"
              //         },
              //         {
              //           "text": "6 months",
              //           "value": "6 months"
              //         },
              //         {
              //           "text": "6 - 12 months",
              //           "value": "6 - 12 months"
              //         },
              //         {
              //           "text": "More than a year",
              //           "value": "More than a year"
              //         }
              //       ]
              //     }
              //   ]
              // }
            ]
          },
          {
            "section": "academicInfo",
            "isRequired": true,
            "isLogical": true,
            "text": "Are you currently enrolled in a college or university?",
            "isNextQuestion": true,
            "type": "dropDown",
            "code": "CURR_ENROLL_CLG",
            "options": [
              {
                "text": "Yes",
                "value": "Yes",
                "isLogical": true,
                "isRequired": true,
                "questions": [
                  {
                    "section": "academicInfo",
                    "isRequired": true,
                    "text": "Which degree are you currently pursuing?",
                    "isNextQuestion": true,
                    "type": "dropDown",
                    "code": "CURR_ENROLL_CLG_DEGREE",
                    "options": [
                      {
                        "text": "Associate’s degree (2-Year)",
                        "value": "Associate’s degree (2-Year)"
                      },
                      {
                        "text": "Bachelor’s degree (e.g. B.S., B.A.)",
                        "value": "Bachelor’s degree (e.g. B.S., B.A.)"
                      },
                      {
                        "text": "Master’s degree (e.g. M.A., M.S., M.Ed)",
                        "value": "Master’s degree (e.g. M.A., M.S., M.Ed)"
                      },
                      {
                        "text": "Doctoral degree (e.g. Ph.D, Ed.D)",
                        "value": "Doctoral degree (e.g. Ph.D, Ed.D)"
                      }
                    ]
                  },
                  {
                    "text": "College/University Name",
                    "code": "CURR_ENROLL_CLG_NAME",
                    "isRequired": true,
                    "type": "text"
                  },
                  {
                    "text": "Type of Institution",
                    "code": "CURR_ENROLL_TYPE",
                    "isRequired": true,
                    "type": "checkbox",
                    "options":[
                      {
                        "text": "Public",
                        "value": "Public"
                      },
                      {
                        "text": "Private",
                        "value": "Private"
                      },
                      {
                        "text": "2-Year",
                        "value": "2-Year"
                      },
                      {
                        "text": "4-Year",
                        "value": "4-Year"
                      },
                      {
                        "text": "Graduate",
                        "value": "Graduate"
                      },
                      {
                        "text": "Not Applicable",
                        "value": "Not Applicable"
                      }
                    ]
                  },
                  {
                    "text": "Country",
                    "code": "CURR_ENROLL_COUNTRY",
                    "options": [],
                    "placeholder": "",
                    "type": "dropDown",
                    "stream": "engineering",
                    "isRequired": true,
                    "NotEditable": false,
                  },
                  {
                    "text": "State/Province",
                    "code": "CURR_ENROLL_STATE",
                    "options": [],
                    "placeholder": "",
                    "type": "dropDown",
                    "stream": "engineering",
                    "isRequired": true,
                    "NotEditable": false,
                  },
                  {
                    "text": "City/Town",
                    "code": "CURR_ENROLL_CITY",
                    "options": [],
                    "placeholder": "",
                    "type": "dropDown",
                    "stream": "engineering",
                    "isRequired": true,
                    "NotEditable": false,
                  },
                  {
                    "text": "Address (Number & Street)",
                    "code": "CURR_ENROLL_ADD",
                    "options": [],
                    "placeholder": "Address (Number & Street)",
                    "type": "text",
                    "stream": "engineering",
                    "NotEditable": false,
                  },
                  {
                    "text": "ZIP/Postal Code",
                    "code": "CURR_ENROLL_ZIP",
                    "options": [],
                    "placeholder": "ZIP/Postal Code",
                    "type": "number",
                    "stream": "engineering",
                    "NotEditable": false,
                  },
                  {
                    "text": "Website",
                    "code": "CURR_ENROLL_WEBSITE",
                    "options": [],
                    "placeholder": "Website",
                    "type": "text",
                    "stream": "engineering",
                    "NotEditable": false,
                  },
                  {
                    "text": "Field of Study (Major)",
                    "code": "CURR_ENROLL_MAJOR",
                    "options": [],
                    "placeholder": "Field of Study (Major)",
                    "type": "text",
                    "stream": "engineering",
                    "isRequired": true,
                    "NotEditable": false,
                  },
                  {
                    "text": "Date you started your enrollment ",
                    "code": "CURR_ENROLL_START_DATE",
                    "options": [],
                    "placeholder": "mm/dd/yyyy",
                    "type": "fullDate",
                    "stream": "engineering",
                    "isRequired": true,
                    "NotEditable": false,
                  },
                  {
                    "text": "Last Date of enrollment",
                    "code": "CURR_ENROLL_LAST_DATE",
                    "options": [],
                    "placeholder": "mm/dd/yyyy",
                    "type": "fullDate",
                    "stream": "engineering",
                    "isRequired": true,
                    "NotEditable": false,
                  },
                ]
              },
              {
                "text": "No",
                "value": "No"
              }
            ]
          },
          {
            "section": "academicInfo",
            "isRequired": true,
            "text": "Which educational degree would you like to pursue next?",
            "isNextQuestion": true,
            "type": "dropDown",
            "code": "EDUDEGNEXT",
            "options": [
              {
                "text": "Professional Certificate",
                "value": "Professional Certificate"
              },
              {
                "text": "Associates",
                "value": "Associates"
              },
              {
                "text": "Diploma",
                "value": "Diploma"
              },
              {
                "text": "Post Doctoral",
                "value": "Post Doctoral"
              },
              {
                "text": "Integrated Programs",
                "value": "Integrated Programs"
              },
              {
                "text": "Bachelor’s Degree (BS/BA)",
                "value": "Bachelor’s Degree (BS/BA)"
              },
              {
                "text": "Occupational Associates",
                "value": "Occupational Associates"
              },
              {
                "text": "Vocational Training",
                "value": "Vocational Training"
              },
              {
                "text": "Summer Internships",
                "value": "Summer Internships"
              }
            ]
          }
        ]
      },
      {
        "text": "Bachelor's degree (e.g. B.S., B.A.)",
        "value": "Bachelor's degree (e.g. B.S., B.A.)",
        "isLogical": true,
        "isRequired": true,
        "questions": [
          {
            "text": "University/College Name",
            "type": "text",
            "isRequired": true,
            "code": "COLLEGENAME"
          },
          {
            "text": "Country",
            "code": "BECH_COUNTRY",
            "options": [],
            "placeholder": "",
            "type": "dropDown",
            "stream": "engineering",
            "isRequired": true,
            "NotEditable": false,
          },
          {
            "text": "State/Province",
            "code": "BECH_STATE",
            "options": [],
            "placeholder": "",
            "type": "dropDown",
            "stream": "engineering",
            "isRequired": true,
            "NotEditable": false,
          },
          {
            "text": "City/Town",
            "code": "BECH_CITY",
            "options": [],
            "placeholder": "",
            "type": "dropDown",
            "stream": "engineering",
            "isRequired": true,
            "NotEditable": false,
          },
          {
            "text": "Address (Number & Street)",
            "code": "BECH_ADD",
            "options": [],
            "placeholder": "Address (Number & Street)",
            "type": "text",
            "stream": "engineering",
            "NotEditable": false,
          },
          {
            "text": "ZIP/Postal Code",
            "code": "BECH_ZIP",
            "options": [],
            "placeholder": "ZIP/Postal Code",
            "type": "number",
            "stream": "engineering",
            "NotEditable": false,
          },
          {
            "text": "Website",
            "code": "BECH_WEBSITE",
            "options": [],
            "placeholder": "Website",
            "type": "text",
            "stream": "engineering",
            "NotEditable": false,
          },
          {
            "text": "Field of Study",
            "type": "text",
            "isRequired": true,
            "code": "COURSENAME"
          },
          {
            "text": "Year of commencement",
            "minValue": 1965,
            "maxValue": new Date().getFullYear(),
            "type": "date",
            "isRequired": true,
            "code": "YOFCOMMENCEMENT"
          },
          {
            "text": "Year of completion",
            "minValue": 1965,
            "maxValue": new Date().getFullYear(),
            "type": "date",
            "isRequired": true,
            "code": "YOCOMPLETION"
          },
          {
            "text": "Grading System",
            "type": "dropDown",
            "isRequired": true,
            "code": "GRADINGSYSTEM",
            "options": [
              {
                "text": "Lettergrade (A/A+)",
                "value": "Lettergrade (A/A+)"
              },
              {
                "text": "CGPA (10)",
                "value": "CGPA (10)"
              },
              {
                "text": "Weighted (out of 5)",
                "value": "Weighted (out of 5)"
              },
              {
                "text": "Unweighted (out of 4)",
                "value": "Unweighted (out of 4)"
              },
              {
                "text": "Iranian system (out of 20)",
                "value": "Iranian system (out of 20)"
              },
              {
                "text": "Percentage (100)",
                "value": "Percentage (100)"
              }
            ]
          },
          {
            "text": "Total Percentage/Marks/CGPA scored",
            "type": "number",
            "isRequired": true,

            "code": "MARKSSCORED"
          },
          {
            "text": " Which entrance exams have you appeared for recently or planning to take?",
            "type": "checkbox",
            "isLogical": true,

            "isRequired": true,
            "code": "ENTRANCEAPPEARED",
            "tips": 'If you have appeared for any of the entrance exams, select all that apply to you. If you have not appeared for any of the exams, select the entrance exams which you would likely appear for in the future.',
            "options": [
              {
                "text": "GRE",
                "value": "GRE",
                "heading": "Enter your GRE Scores",
                "isLogical": true,
                "isRequired": true,
                "isCalculate": true,

                "questions": [
                  {
                    "section": "academicInfo",
                    "isRequired": true,

                    "type": "radio",
                    "stream": "engineering",
                    'tips': "If you have appeared for the exam and you have your scores ready, select “Taken” or select “Planning” and give your expected scores.",
                    "text": "Have you taken GRE before or are planning to take it?",
                    "code": "GRE_PLANNING",
                    "placeholder": "",
                    "options": [
                      {
                        "text": "Planning",
                        "value": "Planning"
                      },
                      {
                        "text": "Taken",
                        "value": "Taken",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                          {
                            "isRequired": true,
                            "text": "Date Taken",
                            "code": "GRE_TAKEN_DATE",
                            "type": "fullDate"
                          }
                        ]
                      },
                    ]
                  },
                  {
                    "text": "Quantitative (130-170)",
                    "code": "GRE_QUANTITATIVE",
                    "placeholder": "Quantitative",
                    "isRequired": true,

                    "minValue": 130,
                    "maxValue": 170,
                    "type": "number",
                  },
                  {
                    "text": "Verbal (130-170)",
                    "code": "GRE_VERBAL",
                    "placeholder": "Verbal",
                    "isRequired": true,

                    "minValue": 130,
                    "maxValue": 170,
                    "type": "number",
                  },
                  {
                    "text": "Analytical Writing AWA  (0-6)",
                    "code": "GRE_ANALYTICAL",
                    "placeholder": "Analytical",
                    "isRequired": true,

                    "minValue": 0,
                    "maxValue": 6,
                    "type": "float",
                  },
                  {
                    "text": "Total Score (260-340)",
                    "code": "GRE_TOTAL",
                    "placeholder": "Total",
                    "isRequired": true,

                    "minValue": 260,
                    "maxValue": 340,
                    "type": "number",
                  },

                ]
              },
              {
                "text": "TOEFL",
                "value": "TOEFL",
                "heading": "Enter your TOEFL Scores",
                "isLogical": true,
                "isRequired": true,
                "isCalculate": true,

                "questions": [
                  {
                    "section": "academicInfo",
                    "isRequired": true,

                    "type": "radio",
                    "stream": "engineering",
                    'tips': "If you have appeared for the exam and you have your scores ready, select “Taken” or select “Planning” and give your expected scores.",
                    "text": "Have you taken TOEFL before or are planning to take it?",
                    "code": "TOEFL_PLANNING",
                    "placeholder": "",
                    "options": [
                      {
                        "text": "Planning",
                        "value": "Planning"
                      },
                      {
                        "text": "Taken",
                        "value": "Taken",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                          {
                            "isRequired": true,
                            "text": "Date Taken",
                            "code": "TOEFL_TAKEN_DATE",
                            "type": "fullDate"
                          }
                        ]
                      },
                    ]
                  },
                  {
                    "text": "Reading (0-30)",
                    "code": "TOEFL_READING",
                    "placeholder": "Reading",
                    "isRequired": true,

                    "minValue": 0,
                    "maxValue": 30,
                    "type": "number",
                  },
                  {
                    "text": "Listening (0-30)",
                    "code": "TOEFL_LISTENING",
                    "placeholder": "Listening",
                    "isRequired": true,

                    "minValue": 0,
                    "maxValue": 30,
                    "type": "number",
                  },
                  {
                    "text": "Speaking (0-30)",
                    "code": "TOEFL_SPEAKING",
                    "placeholder": "Speaking",
                    "isRequired": true,

                    "minValue": 0,
                    "maxValue": 30,
                    "type": "number",
                  },
                  {
                    "text": "Writing (0-30)",
                    "code": "TOEFL_WRITING",
                    "placeholder": "Writing",
                    "isRequired": true,

                    "minValue": 0,
                    "maxValue": 30,
                    "type": "number",
                  },
                  {
                    "text": "Total (0-120)",
                    "code": "TOEFL_TOTAL",
                    "isRequired": true,

                    "placeholder": "Total",
                    "minValue": 0,
                    "maxValue": 120,
                    "type": "number",
                  },

                ]
              },
              {
                "text": "IELTS",
                "value": "IELTS",
                "heading": "Enter your IELTS Scores",
                "isLogical": true,
                "isRequired": true,

                "questions": [
                  {
                    "section": "academicInfo",
                    "isRequired": true,

                    "type": "radio",
                    "stream": "engineering",
                    'tips': "If you have appeared for the exam and you have your scores ready, select “Taken” or select “Planning” and give your expected scores.",
                    "text": "Have you taken IELTS before or are planning to take it?",
                    "code": "IELTS_PLANNING",
                    "placeholder": "",
                    "options": [
                      {
                        "text": "Planning",
                        "value": "Planning"
                      },
                      {
                        "text": "Taken",
                        "value": "Taken",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                          {
                            "isRequired": true,
                            "text": "Date Taken",
                            "code": "IELTS_TAKEN_DATE",
                            "type": "fullDate"
                          }
                        ]
                      },
                    ]
                  },
                  {
                    "text": "Listening (1-9)",
                    "code": "IELTS_LISTENING",
                    "placeholder": "Listening",
                    "isRequired": true,

                    "minValue": 1,
                    "maxValue": 9,
                    "type": "float",
                  },
                  {
                    "text": "Reading (1-9)",
                    "code": "IELTS_READING",
                    "placeholder": "Reading",
                    "isRequired": true,

                    "minValue": 1,
                    "maxValue": 9,
                    "type": "float",
                  },
                  {
                    "text": "Writing (1-9)",
                    "code": "IELTS_WRITING",
                    "placeholder": "Writing",
                    "isRequired": true,

                    "minValue": 1,
                    "maxValue": 9,
                    "type": "float",
                  },
                  {
                    "text": "Speaking (1-9)",
                    "code": "IELTS_SPEAKING",
                    "placeholder": "Speaking",
                    "isRequired": true,

                    "minValue": 1,
                    "maxValue": 9,
                    "type": "float",
                  },
                  {
                    "text": "Band Score (1-9)",
                    "code": "IELTS_BANDSCORE",
                    "placeholder": "Score",
                    "isRequired": true,

                    "minValue": 1,
                    "maxValue": 9,
                    "type": "float",
                  },

                ]
              },
              {
                "text": "LSAT",
                "value": "LSAT",
                "heading": "Enter your LSAT Scores",
                "isLogical": true,
                "isRequired": true,

                "questions": [
                  {
                    "section": "academicInfo",
                    "isRequired": true,

                    "type": "radio",
                    "stream": "engineering",
                    'tips': "If you have appeared for the exam and you have your scores ready, select “Taken” or select “Planning” and give your expected scores.",
                    "text": "Have you taken LSAT before or are planning to take it?",
                    "code": "LSAT_PLANNING",
                    "placeholder": "",
                    "options": [
                      {
                        "text": "Planning",
                        "value": "Planning"
                      },
                      {
                        "text": "Taken",
                        "value": "Taken",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                          {
                            "isRequired": true,
                            "text": "Date Taken",
                            "code": "LSAT_TAKEN_DATE",
                            "type": "fullDate"
                          }
                        ]
                      },
                    ]
                  },
                  {
                    "text": "LSAT Score (120-180)",
                    "code": "BACH_LSAT_LSATSCORE",
                    "placeholder": "LSAT",
                    "isRequired": true,

                    "minValue": 120,
                    "maxValue": 180,
                    "type": "number",
                  },
                  {
                    "text": "Percentile (1-100)",
                    "code": "BACH_LSAT_PERCENTILE",
                    "placeholder": "Percentile",
                    "isRequired": true,

                    "minValue": 1,
                    "maxValue": 100,
                    "type": "number",
                  },

                ]
              },
              {
                "text": "GMAT",
                "value": "GMAT",
                "heading": "Enter your GMAT Scores",
                "isLogical": true,

                "isRequired": true,
                "questions": [
                  {
                    "section": "academicInfo",
                    "isRequired": true,

                    "type": "radio",
                    "stream": "engineering",
                    'tips': "If you have appeared for the exam and you have your scores ready, select “Taken” or select “Planning” and give your expected scores.",
                    "text": "Have you taken GMAT before or are planning to take it?",
                    "code": "GMAT_PLANNING",
                    "placeholder": "",
                    "options": [
                      {
                        "text": "Planning",
                        "value": "Planning"
                      },
                      {
                        "text": "Taken",
                        "value": "Taken",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                          {
                            "isRequired": true,
                            "text": "Date Taken",
                            "code": "GMAT_TAKEN_DATE",
                            "type": "fullDate"
                          }
                        ]
                      },
                    ]
                  },
                  {
                    "text": "Quantitative (0-60)",
                    "code": "BACH_GMAT_QUANTITATIVE",
                    "placeholder": "Quantitative",
                    "isRequired": true,

                    "minValue": 0,
                    "maxValue": 60,
                    "type": "number",
                  },
                  {
                    "text": "Verbal (0-60)",
                    "code": "BACH_GMAT_VERBAL",
                    "placeholder": "Verbal",
                    "isRequired": true,

                    "minValue": 0,
                    "maxValue": 60,
                    "type": "number",
                  },
                  {
                    // "text": "Analytical Writing (0-8)",
                    "text": "Analytical Writing Assessment (0-6)",
                    "code": "BACH_GMAT_ANALYTICAL",
                    "placeholder": "Writing",
                    "isRequired": true,

                    "minValue": 0,
                    "maxValue": 6,
                    "type": "float",
                  },
                  {
                    // "text": "Integrated Reasoning (0-8)",
                    "text": "Integrated Reasoning (1-8)",
                    "code": "BACH_GMAT_INTEGRATEDREASONING",
                    "placeholder": "Reasoning",
                    "isRequired": true,

                    "minValue": 1,
                    "maxValue": 8,
                    "type": "float",
                  },
                  {
                    "text": "Total (200-800)",
                    "code": "BACH_GMAT_Total",
                    "placeholder": "Total",
                    "isRequired": true,

                    "minValue": 200,
                    "maxValue": 800,
                    "type": "number",
                  },

                ]
              },
              // {
              //   "text": "Not Applicable",
              //   "value": "Not Applicable",
              //   "heading": "",
              //   "isLogical": true,

              //   "questions": [
              //     {
              //       "text": "If No when you are planning to  appear for entrance exams ? *\n *For admissions in most US colleges, you need to provide atleast 1 test score.",
              //       "code": "ENTRANCEAPPEARED_NOT_APPLI",
              //       "type": "checkbox",
              //       "isRequired": true,

              //       "options": [
              //         {
              //           "text": "3 months",
              //           "value": "3 months"
              //         },
              //         {
              //           "text": "6 months",
              //           "value": "6 months"
              //         },
              //         {
              //           "text": "6 - 12 months",
              //           "value": "6 - 12 months"
              //         },
              //         {
              //           "text": "More than a year",
              //           "value": "More than a year"
              //         }
              //       ]
              //     }
              //   ]
              // }
            ]
          },
          {
            "section": "academicInfo",
            "isRequired": true,
            "isLogical": true,
            "text": "Are you currently enrolled in a college or university?",
            "isNextQuestion": true,
            "type": "dropDown",
            "code": "CURR_ENROLL_CLG",
            "options": [
              {
                "text": "Yes",
                "value": "Yes",
                "isLogical": true,
                "isRequired": true,
                "questions": [
                  {
                    "section": "academicInfo",
                    "isRequired": true,
                    "text": "Which degree are you currently pursuing?",
                    "isNextQuestion": true,
                    "type": "dropDown",
                    "code": "CURR_ENROLL_CLG_DEGREE",
                    "options": [
                      {
                        "text": "Associate’s degree (2-Year)",
                        "value": "Associate’s degree (2-Year)"
                      },
                      {
                        "text": "Bachelor’s degree (e.g. B.S., B.A.)",
                        "value": "Bachelor’s degree (e.g. B.S., B.A.)"
                      },
                      {
                        "text": "Master’s degree (e.g. M.A., M.S., M.Ed)",
                        "value": "Master’s degree (e.g. M.A., M.S., M.Ed)"
                      },
                      {
                        "text": "Doctoral degree (e.g. Ph.D, Ed.D)",
                        "value": "Doctoral degree (e.g. Ph.D, Ed.D)"
                      }
                    ]
                  },
                  {
                    "text": "College/University Name",
                    "code": "CURR_ENROLL_CLG_NAME",
                    "isRequired": true,
                    "type": "text"
                  },
                  {
                    "text": "Type of Institution",
                    "code": "CURR_ENROLL_TYPE",
                    "isRequired": true,
                    "type": "checkbox",
                    "options":[
                      {
                        "text": "Public",
                        "value": "Public"
                      },
                      {
                        "text": "Private",
                        "value": "Private"
                      },
                      {
                        "text": "2-Year",
                        "value": "2-Year"
                      },
                      {
                        "text": "4-Year",
                        "value": "4-Year"
                      },
                      {
                        "text": "Graduate",
                        "value": "Graduate"
                      },
                      {
                        "text": "Not Applicable",
                        "value": "Not Applicable"
                      }
                    ]
                  },
                  {
                    "text": "Country",
                    "code": "CURR_ENROLL_COUNTRY",
                    "options": [],
                    "placeholder": "",
                    "type": "dropDown",
                    "stream": "engineering",
                    "isRequired": true,
                    "NotEditable": false,
                  },
                  {
                    "text": "State/Province",
                    "code": "CURR_ENROLL_STATE",
                    "options": [],
                    "placeholder": "",
                    "type": "dropDown",
                    "stream": "engineering",
                    "isRequired": true,
                    "NotEditable": false,
                  },
                  {
                    "text": "City/Town",
                    "code": "CURR_ENROLL_CITY",
                    "options": [],
                    "placeholder": "",
                    "type": "dropDown",
                    "stream": "engineering",
                    "isRequired": true,
                    "NotEditable": false,
                  },
                  {
                    "text": "Address (Number & Street)",
                    "code": "CURR_ENROLL_ADD",
                    "options": [],
                    "placeholder": "Address (Number & Street)",
                    "type": "text",
                    "stream": "engineering",
                    "NotEditable": false,
                  },
                  {
                    "text": "ZIP/Postal Code",
                    "code": "CURR_ENROLL_ZIP",
                    "options": [],
                    "placeholder": "ZIP/Postal Code",
                    "type": "number",
                    "stream": "engineering",
                    "NotEditable": false,
                  },
                  {
                    "text": "Website",
                    "code": "CURR_ENROLL_WEBSITE",
                    "options": [],
                    "placeholder": "Website",
                    "type": "text",
                    "stream": "engineering",
                    "NotEditable": false,
                  },
                  {
                    "text": "Field of Study (Major)",
                    "code": "CURR_ENROLL_MAJOR",
                    "options": [],
                    "placeholder": "Field of Study (Major)",
                    "type": "text",
                    "stream": "engineering",
                    "isRequired": true,
                    "NotEditable": false,
                  },
                  {
                    "text": "Date you started your enrollment ",
                    "code": "CURR_ENROLL_START_DATE",
                    "options": [],
                    "placeholder": "mm/dd/yyyy",
                    "type": "fullDate",
                    "stream": "engineering",
                    "isRequired": true,
                    "NotEditable": false,
                  },
                  {
                    "text": "Last Date of enrollment",
                    "code": "CURR_ENROLL_LAST_DATE",
                    "options": [],
                    "placeholder": "mm/dd/yyyy",
                    "type": "fullDate",
                    "stream": "engineering",
                    "isRequired": true,
                    "NotEditable": false,
                  },
                ]
              },
              {
                "text": "No",
                "value": "No"
              }
            ]
          },
          {
            "text": "Which educational degree would you like to pursue next?",
            "isNextQuestion": true,

            "type": "dropDown",
            "code": "EDUDEGNEXT",
            "options": [
              {
                "text": "Masters (General)",
                "value": "Masters (General)"
              },
              {
                "text": "Masters (Science)",
                "value": "Masters (Science)"
              },
              {
                "text": "Masters (Management)",
                "value": "Masters (Management)"
              },
              {
                "text": "Professional Certificate",
                "value": "Professional Certificate"
              },
              {
                "text": "Post Graduate Certificates",
                "value": "Post Graduate Certificates"
              },
              {
                "text": "Transfer Degree",
                "value": "Transfer Degree"
              },
              {
                "text": "Doctorate / PhD / EdD",
                "value": "Doctorate / PhD / EdD"
              },
              {
                "text": "Occupational Associates",
                "value": "Occupational Associates"
              },
              {
                "text": "Integrated Programs",
                "value": "Integrated Programs"
              },
              {
                "text": "Vocational Training",
                "value": "Vocational Training"
              },
              {
                "text": "Summer Internships",
                "value": "Summer Internships"
              }
            ]
          },

        ]
      },
      {
        "text": "Master's degree (e.g. M.A., M.S., M.Ed)",
        "value": "Master's degree (e.g. M.A., M.S., M.Ed)",
        "isLogical": true,
        "isRequired": true,
        "questions": [
          {
            "text": "University Name",
            "code": "UNIVERSITYNAME",
            "isRequired": true,

            "type": "text"
          },
          {
            "text": "College Name",
            "type": "text",
            "isRequired": true,

            "code": "COLLEGENAME"
          },
          {
            "text": "Course Name",
            "type": "text",
            "isRequired": true,

            "code": "COURSENAME"
          },
          {
            "text": "Year of commencement",
            "minValue": 1965,
            "maxValue": new Date().getFullYear(),
            "type": "date",
            "isRequired": true,

            "code": "YOFCOMMENCEMENT"
          },
          {
            "text": "Year of completion",
            "minValue": 1965,
            "maxValue": new Date().getFullYear(),
            "type": "date",
            "isRequired": true,

            "code": "YOCOMPLETION"
          },
          {
            "text": "Grading System",
            "type": "dropDown",
            "isRequired": true,

            "code": "GRADINGSYSTEM",
            "options": [
              {
                "text": "Lettergrade (A/A+)",
                "value": "Lettergrade (A/A+)"
              },
              {
                "text": "CGPA (10)",
                "value": "CGPA (10)"
              },
              {
                "text": "Weighted (out of 5)",
                "value": "Weighted (out of 5)"
              },
              {
                "text": "Unweighted (out of 4)",
                "value": "Unweighted (out of 4)"
              },
              {
                "text": "Iranian system (out of 20)",
                "value": "Iranian system (out of 20)"
              },
              {
                "text": "Percentage (100)",
                "value": "Percentage (100)"
              }
            ]
          },
          {
            "text": "Total Percentage/Marks/CGPA scored",
            "type": "number",
            "isRequired": true,

            "code": "MARKSSCORED"
          },
          {
            "section": "academicInfo",
            "isLogical": true,
            "isRequired": true,

            "text": " Which entrance exams have you appeared for recently or planning to take?",
            "type": "checkbox",
            "code": "ENTRANCEAPPEARED",
            "tips": 'If you have appeared for any of the entrance exams, select all that apply to you. If you have not appeared for any of the exams, select the entrance exams which you would likely appear for in the future.',
            "options": [
              {
                "text": "TOEFL",
                "value": "TOEFL",
                "heading": "Enter your TOEFL Scores",
                "isLogical": true,
                "isRequired": true,
                "isCalculate": true,

                "questions": [
                  {
                    "section": "academicInfo",
                    "isRequired": true,

                    "type": "radio",
                    "stream": "engineering",
                    'tips': "If you have appeared for the exam and you have your scores ready, select “Taken” or select “Planning” and give your expected scores.",
                    "text": "Have you taken TOEFL before or are planning to take it?",
                    "code": "TOEFL_PLANNING",
                    "placeholder": "",
                    "options": [
                      {
                        "text": "Planning",
                        "value": "Planning"
                      },
                      {
                        "text": "Taken",
                        "value": "Taken",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                          {
                            "isRequired": true,
                            "text": "Date Taken",
                            "code": "TOEFL_TAKEN_DATE",
                            "type": "fullDate"
                          }
                        ]
                      },
                    ]
                  },
                  {
                    "text": "Reading (0-30)",
                    "code": "TOEFL_READING",
                    "isRequired": true,

                    "placeholder": "Reading",
                    "minValue": 0,
                    "maxValue": 30,
                    "type": "number"
                  },
                  {
                    "text": "Listening (0-30)",
                    "code": "TOEFL_LISTENING",
                    "placeholder": "Listening",
                    "isRequired": true,

                    "minValue": 0,
                    "maxValue": 30,
                    "type": "number"
                  },
                  {
                    "text": "Speaking (0-30)",
                    "code": "TOEFL_SPEAKING",
                    "placeholder": "Speaking",
                    "isRequired": true,

                    "minValue": 0,
                    "maxValue": 30,
                    "type": "number"
                  },
                  {
                    "text": "Writing (0-30)",
                    "code": "TOEFL_WRITING",
                    "placeholder": "Writing",
                    "isRequired": true,

                    "minValue": 0,
                    "maxValue": 30,
                    "type": "number"
                  },
                  {
                    "text": "Total (0-120)",
                    "code": "TOEFL_TOTAL",
                    "placeholder": "Total",
                    "isRequired": true,

                    "minValue": 0,
                    "maxValue": 120,
                    "type": "number"
                  },

                ]
              },
              {
                "text": "IELTS",
                "value": "IELTS",
                "heading": "Enter your IELTS Scores",
                "isLogical": true,
                "isRequired": true,
                "questions": [
                  {
                    "section": "academicInfo",
                    "isRequired": true,

                    "type": "radio",
                    "stream": "engineering",
                    'tips': "If you have appeared for the exam and you have your scores ready, select “Taken” or select “Planning” and give your expected scores.",
                    "text": "Have you taken IELTS before or are planning to take it?",
                    "code": "IELTS_PLANNING",
                    "placeholder": "",
                    "options": [
                      {
                        "text": "Planning",
                        "value": "Planning"
                      },
                      {
                        "text": "Taken",
                        "value": "Taken",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                          {
                            "isRequired": true,
                            "text": "Date Taken",
                            "code": "IELTS_TAKEN_DATE",
                            "type": "fullDate"
                          }
                        ]
                      },
                    ]
                  },
                  {
                    "text": "Listening (1-9)",
                    "code": "IELTS_LISTENING",
                    "isRequired": true,

                    "placeholder": "Listening",
                    "minValue": 1,
                    "maxValue": 9,
                    "type": "float"
                  },
                  {
                    "text": "Reading (1-9)",
                    "code": "IELTS_READING",
                    "placeholder": "Reading",
                    "isRequired": true,

                    "minValue": 1,
                    "maxValue": 9,
                    "type": "float"
                  },
                  {
                    "text": "Writing (1-9)",
                    "code": "IELTS_WRITING",
                    "placeholder": "Writing",
                    "isRequired": true,

                    "minValue": 1,
                    "maxValue": 9,
                    "type": "float"
                  },
                  {
                    "text": "Speaking (1-9)",
                    "code": "IELTS_SPEAKING",
                    "placeholder": "Speaking",
                    "isRequired": true,

                    "minValue": 1,
                    "maxValue": 9,
                    "type": "float"
                  },
                  {
                    "text": "Band Score (1-9)",
                    "code": "IELTS_BANDSCORE",
                    "placeholder": "Score",
                    "isRequired": true,

                    "minValue": 1,
                    "maxValue": 9,
                    "type": "float"
                  },

                ]
              },
              // {
              //   "text": "Not Applicable",
              //   "value": "Not Applicable",
              //   "heading": "",
              //   "isLogical": true,

              //   "questions": [
              //     {
              //       "text": "If No when you are planning to  appear for entrance exams ? *\n *For admissions in most US colleges, you need to provide atleast 1 test score.",
              //       "code": "ENTRANCEAPPEARED_NOT_APPLI",
              //       "type": "checkbox",
              //       "isRequired": true,

              //       "options": [
              //         {
              //           "text": "3 months",
              //           "value": "3 months"
              //         },
              //         {
              //           "text": "6 months",
              //           "value": "6 months"
              //         },
              //         {
              //           "text": "6 - 12 months",
              //           "value": "6 - 12 months"
              //         },
              //         {
              //           "text": "More than a year",
              //           "value": "More than a year"
              //         }
              //       ]
              //     }
              //   ]
              // }
            ]
          },
          {
            "section": "academicInfo",
            "isRequired": true,
            "isLogical": true,
            "text": "Are you currently enrolled in a college or university?",
            "isNextQuestion": true,
            "type": "dropDown",
            "code": "CURR_ENROLL_CLG",
            "options": [
              {
                "text": "Yes",
                "value": "Yes",
                "isLogical": true,
                "isRequired": true,
                "questions": [
                  {
                    "section": "academicInfo",
                    "isRequired": true,
                    "text": "Which degree are you currently pursuing?",
                    "isNextQuestion": true,
                    "type": "dropDown",
                    "code": "CURR_ENROLL_CLG_DEGREE",
                    "options": [
                      {
                        "text": "Associate’s degree (2-Year)",
                        "value": "Associate’s degree (2-Year)"
                      },
                      {
                        "text": "Bachelor’s degree (e.g. B.S., B.A.)",
                        "value": "Bachelor’s degree (e.g. B.S., B.A.)"
                      },
                      {
                        "text": "Master’s degree (e.g. M.A., M.S., M.Ed)",
                        "value": "Master’s degree (e.g. M.A., M.S., M.Ed)"
                      },
                      {
                        "text": "Doctoral degree (e.g. Ph.D, Ed.D)",
                        "value": "Doctoral degree (e.g. Ph.D, Ed.D)"
                      }
                    ]
                  },
                  {
                    "text": "College/University Name",
                    "code": "CURR_ENROLL_CLG_NAME",
                    "isRequired": true,
                    "type": "text"
                  },
                  {
                    "text": "Type of Institution",
                    "code": "CURR_ENROLL_TYPE",
                    "isRequired": true,
                    "type": "checkbox",
                    "options":[
                      {
                        "text": "Public",
                        "value": "Public"
                      },
                      {
                        "text": "Private",
                        "value": "Private"
                      },
                      {
                        "text": "2-Year",
                        "value": "2-Year"
                      },
                      {
                        "text": "4-Year",
                        "value": "4-Year"
                      },
                      {
                        "text": "Graduate",
                        "value": "Graduate"
                      },
                      {
                        "text": "Not Applicable",
                        "value": "Not Applicable"
                      }
                    ]
                  },
                  {
                    "text": "Country",
                    "code": "CURR_ENROLL_COUNTRY",
                    "options": [],
                    "placeholder": "",
                    "type": "dropDown",
                    "stream": "engineering",
                    "isRequired": true,
                    "NotEditable": false,
                  },
                  {
                    "text": "State/Province",
                    "code": "CURR_ENROLL_STATE",
                    "options": [],
                    "placeholder": "",
                    "type": "dropDown",
                    "stream": "engineering",
                    "isRequired": true,
                    "NotEditable": false,
                  },
                  {
                    "text": "City/Town",
                    "code": "CURR_ENROLL_CITY",
                    "options": [],
                    "placeholder": "",
                    "type": "dropDown",
                    "stream": "engineering",
                    "isRequired": true,
                    "NotEditable": false,
                  },
                  {
                    "text": "Address (Number & Street)",
                    "code": "CURR_ENROLL_ADD",
                    "options": [],
                    "placeholder": "Address (Number & Street)",
                    "type": "text",
                    "stream": "engineering",
                    "NotEditable": false,
                  },
                  {
                    "text": "ZIP/Postal Code",
                    "code": "CURR_ENROLL_ZIP",
                    "options": [],
                    "placeholder": "ZIP/Postal Code",
                    "type": "number",
                    "stream": "engineering",
                    "NotEditable": false,
                  },
                  {
                    "text": "Website",
                    "code": "CURR_ENROLL_WEBSITE",
                    "options": [],
                    "placeholder": "Website",
                    "type": "text",
                    "stream": "engineering",
                    "NotEditable": false,
                  },
                  {
                    "text": "Field of Study (Major)",
                    "code": "CURR_ENROLL_MAJOR",
                    "options": [],
                    "placeholder": "Field of Study (Major)",
                    "type": "text",
                    "stream": "engineering",
                    "isRequired": true,
                    "NotEditable": false,
                  },
                  {
                    "text": "Date you started your enrollment ",
                    "code": "CURR_ENROLL_START_DATE",
                    "options": [],
                    "placeholder": "mm/dd/yyyy",
                    "type": "fullDate",
                    "stream": "engineering",
                    "isRequired": true,
                    "NotEditable": false,
                  },
                  {
                    "text": "Last Date of enrollment",
                    "code": "CURR_ENROLL_LAST_DATE",
                    "options": [],
                    "placeholder": "mm/dd/yyyy",
                    "type": "fullDate",
                    "stream": "engineering",
                    "isRequired": true,
                    "NotEditable": false,
                  },
                ]
              },
              {
                "text": "No",
                "value": "No"
              }
            ]
          },
          {
            "text": "Which educational degree would you like to pursue next?",
            "isNextQuestion": true,
            "type": "dropDown",
            "code": "EDUDEGNEXT",
            "options": [
              {
                "text": "Doctorate / PhD / EdD",
                "value": "Doctorate / PhD / EdD"
              },
              {
                "text": "Professional Certificate",
                "value": "Professional Certificate"
              },
              {
                "text": "Transfer Degree",
                "value": "Transfer Degree"
              },
              {
                "text": "Post Doctoral",
                "value": "Post Doctoral"
              },
              {
                "text": "Occupational Associates",
                "value": "Occupational Associates"
              },
              {
                "text": "Integrated Programs",
                "value": "Integrated Programs"
              },
              {
                "text": "Vocational Training",
                "value": "Vocational Training"
              },
              {
                "text": "Summer Internships",
                "value": "Summer Internships"
              }
            ]
          }
        ]
      },
      {
        "text": "Doctoral degree (e.g. Ph.D, Ed.D)",
        "value": "Doctoral degree (e.g. Ph.D, Ed.D)",
        "isLogical": true,
        "isRequired": true,
        "questions": [
          {
            "text": "University Name",
            "code": "UNIVERSITYNAME",
            "isRequired": true,
            "type": "text"
          },
          {
            "text": "College Name",
            "type": "text",
            "isRequired": true,
            "code": "COLLEGENAME"
          },
          {
            "text": "Course Name",
            "type": "text",
            "isRequired": true,
            "code": "COURSENAME"
          },
          {
            "text": "Year of commencement",
            "minValue": 1965,
            "maxValue": new Date().getFullYear(),
            "type": "date",
            "isRequired": true,
            "code": "YOFCOMMENCEMENT"
          },
          {
            "text": "Year of completion",
            "minValue": 1965,
            "maxValue": new Date().getFullYear(),
            "type": "date",
            "isRequired": true,
            "code": "YOCOMPLETION"
          },
          {
            "text": "Grading System",
            "type": "dropDown",
            "isRequired": true,
            "code": "GRADINGSYSTEM",
            "options": [
              {
                "text": "Lettergrade (A/A+)",
                "value": "Lettergrade (A/A+)"
              },
              {
                "text": "CGPA (10)",
                "value": "CGPA (10)"
              },
              {
                "text": "Weighted (out of 5)",
                "value": "Weighted (out of 5)"
              },
              {
                "text": "Unweighted (out of 4)",
                "value": "Unweighted (out of 4)"
              },
              {
                "text": "Iranian system (out of 20)",
                "value": "Iranian system (out of 20)"
              },
              {
                "text": "Percentage (100)",
                "value": "Percentage (100)"
              }
            ]
          },
          {
            "text": "Total Percentage/Marks/CGPA scored",
            "type": "number",
            "isRequired": true,

            "code": "MARKSSCORED"
          },
          {
            "section": "academicInfo",
            "isLogical": true,
            "isRequired": true,
            "text": " Which entrance exams have you appeared for recently or planning to take?",
            "type": "checkbox",
            "code": "ENTRANCEAPPEARED",
            "tips": 'If you have appeared for any of the entrance exams, select all that apply to you. If you have not appeared for any of the exams, select the entrance exams which you would likely appear for in the future.',
            "options": [
              {
                "text": "TOEFL",
                "value": "TOEFL",
                "heading": "Enter your TOEFL Scores",
                "isLogical": true,
                "isRequired": true,
                "isCalculate": true,

                "questions": [
                  {
                    "section": "academicInfo",
                    "isRequired": true,

                    "type": "radio",
                    "stream": "engineering",
                    'tips': "If you have appeared for the exam and you have your scores ready, select “Taken” or select “Planning” and give your expected scores.",
                    "text": "Have you taken TOEFL before or are planning to take it?",
                    "code": "TOEFL_PLANNING",
                    "placeholder": "",
                    "options": [
                      {
                        "text": "Planning",
                        "value": "Planning"
                      },
                      {
                        "text": "Taken",
                        "value": "Taken",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                          {
                            "isRequired": true,
                            "text": "Date Taken",
                            "code": "TOEFL_TAKEN_DATE",
                            "type": "fullDate"
                          }
                        ]
                      },
                    ]
                  },
                  {
                    "text": "Reading (0-30)",
                    "code": "TOEFL_READING",
                    "isRequired": true,

                    "placeholder": "Reading",
                    "minValue": 0,
                    "maxValue": 30,
                    "type": "number"
                  },
                  {
                    "text": "Listening (0-30)",
                    "code": "TOEFL_LISTENING",
                    "placeholder": "Listening",
                    "isRequired": true,

                    "minValue": 0,
                    "maxValue": 30,
                    "type": "number"
                  },
                  {
                    "text": "Speaking (0-30)",
                    "code": "TOEFL_SPEAKING",
                    "placeholder": "Speaking",
                    "isRequired": true,

                    "minValue": 0,
                    "maxValue": 30,
                    "type": "number"
                  },
                  {
                    "text": "Writing (0-30)",
                    "code": "TOEFL_WRITING",
                    "placeholder": "Writing",
                    "isRequired": true,

                    "minValue": 0,
                    "maxValue": 30,
                    "type": "number"
                  },
                  {
                    "text": "Total (0-120)",
                    "code": "TOEFL_TOTAL",
                    "placeholder": "Total",
                    "isRequired": true,

                    "minValue": 0,
                    "maxValue": 120,
                    "type": "number"
                  },

                ]
              },
              {
                "text": "IELTS",
                "value": "IELTS",
                "heading": "Enter your IELTS Scores",
                "isLogical": true,
                "isRequired": true,

                "questions": [
                  {
                    "section": "academicInfo",
                    "isRequired": true,

                    "type": "radio",
                    "stream": "engineering",
                    'tips': "If you have appeared for the exam and you have your scores ready, select “Taken” or select “Planning” and give your expected scores.",
                    "text": "Have you taken IELTS before or are planning to take it?",
                    "code": "IELTS_PLANNING",
                    "placeholder": "",
                    "options": [
                      {
                        "text": "Planning",
                        "value": "Planning"
                      },
                      {
                        "text": "Taken",
                        "value": "Taken",
                        "isLogical": true,
                        "isRequired": true,
                        "questions": [
                          {
                            "isRequired": true,
                            "text": "Date Taken",
                            "code": "IELTS_TAKEN_DATE",
                            "type": "fullDate"
                          }
                        ]
                      },
                    ]
                  },
                  {
                    "text": "Listening (1-9)",
                    "code": "IELTS_LISTENING",
                    "isRequired": true,

                    "placeholder": "Listening",
                    "minValue": 1,
                    "maxValue": 9,
                    "type": "float"
                  },
                  {
                    "text": "Reading (1-9)",
                    "code": "IELTS_READING",
                    "placeholder": "Reading",
                    "isRequired": true,

                    "minValue": 1,
                    "maxValue": 9,
                    "type": "float"
                  },
                  {
                    "text": "Writing (1-9)",
                    "code": "IELTS_WRITING",
                    "placeholder": "Writing",
                    "isRequired": true,

                    "minValue": 1,
                    "maxValue": 9,
                    "type": "float"
                  },
                  {
                    "text": "Speaking (1-9)",
                    "code": "IELTS_SPEAKING",
                    "placeholder": "Speaking",
                    "isRequired": true,

                    "minValue": 1,
                    "maxValue": 9,
                    "type": "float"
                  },
                  {
                    "text": "Band Score (1-9)",
                    "code": "IELTS_BANDSCORE",
                    "placeholder": "Score",
                    "isRequired": true,

                    "minValue": 1,
                    "maxValue": 9,
                    "type": "float"
                  },

                ]
              },
              // {
              //   "text": "Not Applicable",
              //   "value": "Not Applicable",
              //   "heading": "",
              //   "isLogical": true,

              //   "questions": [
              //     {
              //       "text": "If No when you are planning to  appear for entrance exams ? *\n *For admissions in most US colleges, you need to provide atleast 1 test score.",
              //       "code": "ENTRANCEAPPEARED_NOT_APPLI",
              //       "type": "checkbox",
              //       "isRequired": true,

              //       "options": [
              //         {
              //           "text": "3 months",
              //           "value": "3 months"
              //         },
              //         {
              //           "text": "6 months",
              //           "value": "6 months"
              //         },
              //         {
              //           "text": "6 - 12 months",
              //           "value": "6 - 12 months"
              //         },
              //         {
              //           "text": "More than a year",
              //           "value": "More than a year"
              //         }
              //       ]
              //     }
              //   ]
              // }
            ]
          },
          {
            "section": "academicInfo",
            "isRequired": true,
            "isLogical": true,
            "text": "Are you currently enrolled in a college or university?",
            "isNextQuestion": true,
            "type": "dropDown",
            "code": "CURR_ENROLL_CLG",
            "options": [
              {
                "text": "Yes",
                "value": "Yes",
                "isLogical": true,
                "isRequired": true,
                "questions": [
                  {
                    "section": "academicInfo",
                    "isRequired": true,
                    "text": "Which degree are you currently pursuing?",
                    "isNextQuestion": true,
                    "type": "dropDown",
                    "code": "CURR_ENROLL_CLG_DEGREE",
                    "options": [
                      {
                        "text": "Associate’s degree (2-Year)",
                        "value": "Associate’s degree (2-Year)"
                      },
                      {
                        "text": "Bachelor’s degree (e.g. B.S., B.A.)",
                        "value": "Bachelor’s degree (e.g. B.S., B.A.)"
                      },
                      {
                        "text": "Master’s degree (e.g. M.A., M.S., M.Ed)",
                        "value": "Master’s degree (e.g. M.A., M.S., M.Ed)"
                      },
                      {
                        "text": "Doctoral degree (e.g. Ph.D, Ed.D)",
                        "value": "Doctoral degree (e.g. Ph.D, Ed.D)"
                      }
                    ]
                  },
                  {
                    "text": "College/University Name",
                    "code": "CURR_ENROLL_CLG_NAME",
                    "isRequired": true,
                    "type": "text"
                  },
                  {
                    "text": "Type of Institution",
                    "code": "CURR_ENROLL_TYPE",
                    "isRequired": true,
                    "type": "checkbox",
                    "options":[
                      {
                        "text": "Public",
                        "value": "Public"
                      },
                      {
                        "text": "Private",
                        "value": "Private"
                      },
                      {
                        "text": "2-Year",
                        "value": "2-Year"
                      },
                      {
                        "text": "4-Year",
                        "value": "4-Year"
                      },
                      {
                        "text": "Graduate",
                        "value": "Graduate"
                      },
                      {
                        "text": "Not Applicable",
                        "value": "Not Applicable"
                      }
                    ]
                  },
                  {
                    "text": "Country",
                    "code": "CURR_ENROLL_COUNTRY",
                    "options": [],
                    "placeholder": "",
                    "type": "dropDown",
                    "stream": "engineering",
                    "isRequired": true,
                    "NotEditable": false,
                  },
                  {
                    "text": "State/Province",
                    "code": "CURR_ENROLL_STATE",
                    "options": [],
                    "placeholder": "",
                    "type": "dropDown",
                    "stream": "engineering",
                    "isRequired": true,
                    "NotEditable": false,
                  },
                  {
                    "text": "City/Town",
                    "code": "CURR_ENROLL_CITY",
                    "options": [],
                    "placeholder": "",
                    "type": "dropDown",
                    "stream": "engineering",
                    "isRequired": true,
                    "NotEditable": false,
                  },
                  {
                    "text": "Address (Number & Street)",
                    "code": "CURR_ENROLL_ADD",
                    "options": [],
                    "placeholder": "Address (Number & Street)",
                    "type": "text",
                    "stream": "engineering",
                    "NotEditable": false,
                  },
                  {
                    "text": "ZIP/Postal Code",
                    "code": "CURR_ENROLL_ZIP",
                    "options": [],
                    "placeholder": "ZIP/Postal Code",
                    "type": "number",
                    "stream": "engineering",
                    "NotEditable": false,
                  },
                  {
                    "text": "Website",
                    "code": "CURR_ENROLL_WEBSITE",
                    "options": [],
                    "placeholder": "Website",
                    "type": "text",
                    "stream": "engineering",
                    "NotEditable": false,
                  },
                  {
                    "text": "Field of Study (Major)",
                    "code": "CURR_ENROLL_MAJOR",
                    "options": [],
                    "placeholder": "Field of Study (Major)",
                    "type": "text",
                    "stream": "engineering",
                    "isRequired": true,
                    "NotEditable": false,
                  },
                  {
                    "text": "Date you started your enrollment ",
                    "code": "CURR_ENROLL_START_DATE",
                    "options": [],
                    "placeholder": "mm/dd/yyyy",
                    "type": "fullDate",
                    "stream": "engineering",
                    "isRequired": true,
                    "NotEditable": false,
                  },
                  {
                    "text": "Last Date of enrollment",
                    "code": "CURR_ENROLL_LAST_DATE",
                    "options": [],
                    "placeholder": "mm/dd/yyyy",
                    "type": "fullDate",
                    "stream": "engineering",
                    "isRequired": true,
                    "NotEditable": false,
                  },
                ]
              },
              {
                "text": "No",
                "value": "No"
              }
            ]
          },
          {
            "text": "Which educational degree would you like to pursue next?",
            "isNextQuestion": true,
            "type": "dropDown",
            "code": "EDUDEGNEXT",
            "options": [
              {
                "text": "Professional Certificate",
                "value": "Professional Certificate"
              },
              {
                "text": "Transfer Degree",
                "value": "Transfer Degree"
              },
              {
                "text": "Post Doctoral",
                "value": "Post Doctoral"
              },
              {
                "text": "Occupational Associates",
                "value": "Occupational Associates"
              },
              {
                "text": "Integrated Programs",
                "value": "Integrated Programs"
              },
              {
                "text": "Vocational Training",
                "value": "Vocational Training"
              },
              {
                "text": "Summer Internships",
                "value": "Summer Internships"
              }
            ]
          }
        ]
      }
    ]
  },

  {
    "section": "academicInfo",
    "isRequired": true,

    "isLogical": true,
    "type": "dropDown",
    "stream": "engineering",
    "text": "Which field of study would you like to pursue?",
    "code": "STUDYFIELD",
    "placeholder": "",
    "options": [
      {
        "text": "Agriculture, Agriculture Operations and Related Sciences",
        "value": "Agriculture, Agriculture Operations and Related Sciences"
      },
      {
        "text": "Architecture and Related Services",
        "value": "Architecture and Related Services"
      },
      {
        "text": "Area, Ethnic, Cultural, Gender, and Group Studies",
        "value": "Area, Ethnic, Cultural, Gender, and Group Studies"
      },
      {
        "text": "Biological and Biomedical Sciences",
        "value": "Biological and Biomedical Sciences"
      },
      {
        "text": "Business Analytics or Data Science",
        "value": "Business Analytics or Data Science"
      },
      {
        "text": "Business, Management, Marketing, and Related Support Services",
        "value": "Business, Management, Marketing, and Related Support Services"
      },
      {
        "text": "Communication, Journalism, and Related Programs",
        "value": "Communication, Journalism, and Related Programs"
      },
      {
        "text": "Communications Technologies/Technicians and Support Services",
        "value": "Communications Technologies/Technicians and Support Services"
      },
      {
        "text": "Computer and Information Sciences and Support Services",
        "value": "Computer and Information Sciences and Support Services"
      },
      {
        "text": "Construction Trades",
        "value": "Construction Trades"
      },
      {
        "text": "Education",
        "value": "Education"
      },
      {
        "text": "Engineering",
        "value": "Engineering"
      },
      {
        "text": "Engineering Technologies and Engineering-related Fields",
        "value": "Engineering Technologies and Engineering-related Fields"
      },
      {
        "text": "English Language and Literature/Letters",
        "value": "English Language and Literature/Letters"
      },
      {
        "text": "Family and Consumer Sciences/Human Sciences",
        "value": "Family and Consumer Sciences/Human Sciences"
      },
      {
        "text": "Foreign Languages, Literatures, and Linguistics",
        "value": "Foreign Languages, Literatures, and Linguistics"
      },
      {
        "text": "Health Professions and Related Programs",
        "value": "Health Professions and Related Programs"
      },
      {
        "text": "History",
        "value": "History"
      },
      {
        "text": "Homeland Security, Law Enforcement, Firefighting, and Related Protective Service",
        "value": "Homeland Security, Law Enforcement, Firefighting, and Related Protective Service"
      },
      {
        "text": "Legal Professions and Studies",
        "value": "Legal Professions and Studies"
      },
      {
        "text": "Liberal Arts and Sciences, General Studies and Humanities",
        "value": "Liberal Arts and Sciences, General Studies and Humanities"
      },
      {
        "text": "Library Science",
        "value": "Library Science"
      },
      {
        "text": "Management and Data Analytics",
        "value": "Management and Data Analytics"
      },
      {
        "text": "Mathematics and Statistics",
        "value": "Mathematics and Statistics"
      },
      {
        "text": "Mechanic and Repair Technologies/Technicians",
        "value": "Mechanic and Repair Technologies/Technicians"
      },
      {
        "text": "Military Technologies and Applied Sciences",
        "value": "Military Technologies and Applied Sciences"
      },
      {
        "text": "Multi/Interdisciplinary Studies",
        "value": "Multi/Interdisciplinary Studies"
      },
      {
        "text": "Natural Resources and Conservation",
        "value": "Natural Resources and Conservation"
      },
      {
        "text": "Operations Management for Technical Industries",
        "value": "Operations Management for Technical Industries"
      },
      {
        "text": "Parks, Recreation, Leisure and Fitness Studies",
        "value": "Parks, Recreation, Leisure and Fitness Studies"
      },
      {
        "text": "Personal and Culinary Services",
        "value": "Personal and Culinary Services"
      },
      {
        "text": "Philosophy and Religious Studies",
        "value": "Philosophy and Religious Studies"
      },
      {
        "text": "Physical Sciences",
        "value": "Physical Sciences"
      },
      {
        "text": "Precision Production",
        "value": "Precision Production"
      },
      {
        "text": "Psychology",
        "value": "Psychology"
      },
      {
        "text": "Public Administration and Social Service Professions",
        "value": "Public Administration and Social Service Professions"
      },
      {
        "text": "Science Technologies/Technicians",
        "value": "Science Technologies/Technicians"
      },
      {
        "text": "Social Sciences",
        "value": "Social Sciences"
      },
      {
        "text": "Theology and Religious Vocations",
        "value": "Theology and Religious Vocations"
      },
      {
        "text": "Transportation and Materials Moving",
        "value": "Transportation and Materials Moving"
      },
      {
        "text": "Visual and Performing Arts",
        "value": "Visual and Performing Arts"
      },
      // {
      //     "text": "Other",
      //     "value": "Other",
      //     "isLogical": true,
      //     "questions": [
      //         {
      //             "text": "Other",
      //             "code": "STUDYFIELD_OTHER",
      //             "type": "text",
      //             "isRequired": true,
      //         }
      //     ]
      // }
    ]
  },


  {
    "section": "academicInfo",
    "isRequired": true,
    "isLogical": true,

    "type": "radio",
    "stream": "engineering",
    "text": "Do you want to pursue a specific major?",
    "code": "MAJORPURSUE",
    "placeholder": "",
    "tips": "Specific major is a particular course you want to pursue in a field. For example, Computer science is a specific major/ course in the field of Engineering.",
    "options": [
      {
        "text": "Yes",
        "value": "Yes",
        "isLogical": true,
        "isRequired": true,
        "questions": [
          {
            "section": "academicInfo",
            "type": "multi_dropDown",
            "isRequired": true,
            "stream": "engineering",
            "text": "Which specific major you like to pursue?",
            "code": "MOJOR_PURSUE",
            "placeholder": "",
            "options": []
          }
        ]
      },
      {
        "text": "No",
        "value": "No"
      },
    ]
  },
  {
    "section": "academicInfo",
    "text": "Do you have the Statement of Purpose (SoP) ready?",
    "code": "STATEMENTPURPOSEREADY",
    "options": [],
    "placeholder": "",
    "type": "dropDown",
    "stream": "engineering",
    "isLogical": true,
    "options": [
      {
        "text": "Yes",
        "value": "Yes",
        "isLogical": true,
        "isRequired": true,

        "questions": [
          {
            "section": "academicInfo",
            "text": "Write a Statement of Purpose (SoP) describing your passion for the field of study and how you will contribute to the college community. (Min. 250 words)",
            "code": "PASSIONESSAY",
            "options": [],
            "placeholder": "(Min. 250 words)",
            "type": "textArea",
            "stream": "engineering",
            "tips": "A Statement of Purpose (SOP) talks about your career path, interest, professional contributions, goals and how you'll add value to the program you're applying to and the driving force behind it.",
          },
        ]
      },
      {
        "text": "No",
        "value": "No"
      },
      {
        "text": "Need help from HiEd Counselor",
        "value": "Need help from HiEd Counselor"
      },
    ]
  },
  {
    "section": "academicInfo",
    "text": "Be Transparent on your short or long-term goals in pursuing your education in US college?",
    "code": "TRANSPARENTSHORTLONG",
    "options": [],
    "placeholder": "(Like to come for studies and work in US or Like to move to US or Like to advance my faculty certifications and apply in home country or like to do advance studies from reputed College or Like to do Internships all years during my engineering degree, Like to use my work experience to upskill myself in different domain)",
    "type": "textArea",
    "stream": "engineering",
  },
  {
    "section": "academicInfo",
    "text": "How many Letters of Recommendation (LoR) do you have?",
    "code": "RECOMMENDATION",
    "tips": "Letter of recommendation is a letter that recommends a student, employee, colleague, or co-worker. It is also called a letter of reference or LOR in short. When issued to a student, this letter helps the admission committee to know and understand more about the applicant.",
    "isRequired": true,
    "isLogical": true,

    "options": [
      {
        "text": "0",
        "value": "0"
      },
      {
        "text": "1",
        "value": "1",
        "isLogical": true,
        "isRequired": true,

        "questions": [
          {
            "text": "Who has written your Letter(s) of Recommendation? (Choose 1 or more)",
            "code": "RECOMMENDATION_SELECT",
            "type": "checkbox",
            "tips": "If you have a Letter(s) of Recommendation then you need to select below options by whom it has been written.",
            "isLogical": true,
            "isRequired": true,

            "options": [
              {
                "test": "Counselor/Advisor",
                "value": "Counselor/Advisor",
              },
              {
                "test": "Peer (Friend, Classmate, Family)",
                "value": "Peer (Friend, Classmate, Family)",
              },
              {
                "test": "Senior",
                "value": "Senior",
              },
              {
                "test": "Teacher (11th or 12th Grade)",
                "value": "Teacher (11th or 12th Grade)",
              },
              {
                "test": "Core Subject Teacher",
                "value": "Core Subject Teacher",
              },
              {
                "test": "Professor (PhD)",
                "value": "Professor (PhD)",
              },
              {
                "test": "Principal/Dean",
                "value": "Principal/Dean",
              },
              {
                "test": "Associate/Asst. Professor",
                "value": "Associate/Asst. Professor",
              },
              {
                "test": "Mentor/Coach",
                "value": "Mentor/Coach",
              },
              {
                "test": "Employer",
                "value": "Employer",
              },
              {
                "test": "Manager/Supervisor",
                "value": "Manager/Supervisor",
              },
              {
                "test": "Co-worker",
                "value": "Co-worker",
              }
            ]
          }
        ]
      },
      {
        "text": "2",
        "value": "2",
        "isLogical": true,
        "isRequired": true,

        "questions": [
          {
            "text": "Who has written your Letter(s) of Recommendation? (Choose 1 or more)",
            "code": "RECOMMENDATION_SELECT",
            "type": "checkbox",
            "tips": "If you have a Letter(s) of Recommendation then you need to select below options by whom it has been written.",
            "isLogical": true,
            "isRequired": true,

            "options": [
              {
                "test": "Counselor/Advisor",
                "value": "Counselor/Advisor",
              },
              {
                "test": "Peer (Friend, Classmate, Family)",
                "value": "Peer (Friend, Classmate, Family)",
              },
              {
                "test": "Senior",
                "value": "Senior",
              },
              {
                "test": "Teacher (11th or 12th Grade)",
                "value": "Teacher (11th or 12th Grade)",
              },
              {
                "test": "Core Subject Teacher",
                "value": "Core Subject Teacher",
              },
              {
                "test": "Professor (PhD)",
                "value": "Professor (PhD)",
              },
              {
                "test": "Principal/Dean",
                "value": "Principal/Dean",
              },
              {
                "test": "Associate/Asst. Professor",
                "value": "Associate/Asst. Professor",
              },
              {
                "test": "Mentor/Coach",
                "value": "Mentor/Coach",
              },
              {
                "test": "Employer",
                "value": "Employer",
              },
              {
                "test": "Manager/Supervisor",
                "value": "Manager/Supervisor",
              },
              {
                "test": "Co-worker",
                "value": "Co-worker",
              }
            ]
          }
        ]
      },
      {
        "text": "3",
        "value": "3",
        "isLogical": true,
        "isRequired": true,

        "questions": [
          {
            "text": "Who has written your Letter(s) of Recommendation? (Choose 1 or more)",
            "code": "RECOMMENDATION_SELECT",
            "type": "checkbox",
            "tips": "If you have a Letter(s) of Recommendation then you need to select below options by whom it has been written.",
            "isLogical": true,
            "isRequired": true,

            "options": [
              {
                "test": "Counselor/Advisor",
                "value": "Counselor/Advisor",
              },
              {
                "test": "Peer (Friend, Classmate, Family)",
                "value": "Peer (Friend, Classmate, Family)",
              },
              {
                "test": "Senior",
                "value": "Senior",
              },
              {
                "test": "Teacher (11th or 12th Grade)",
                "value": "Teacher (11th or 12th Grade)",
              },
              {
                "test": "Core Subject Teacher",
                "value": "Core Subject Teacher",
              },
              {
                "test": "Professor (PhD)",
                "value": "Professor (PhD)",
              },
              {
                "test": "Principal/Dean",
                "value": "Principal/Dean",
              },
              {
                "test": "Associate/Asst. Professor",
                "value": "Associate/Asst. Professor",
              },
              {
                "test": "Mentor/Coach",
                "value": "Mentor/Coach",
              },
              {
                "test": "Employer",
                "value": "Employer",
              },
              {
                "test": "Manager/Supervisor",
                "value": "Manager/Supervisor",
              },
              {
                "test": "Co-worker",
                "value": "Co-worker",
              }
            ]
          }
        ]
      },
      {
        "text": "4",
        "value": "4",
        "isLogical": true,
        "isRequired": true,

        "questions": [
          {
            "text": "Who has written your Letter(s) of Recommendation? (Choose 1 or more)",
            "code": "RECOMMENDATION_SELECT",
            "type": "checkbox",
            "tips": "If you have a Letter(s) of Recommendation then you need to select below options by whom it has been written.",
            "isLogical": true,
            "isRequired": true,

            "options": [
              {
                "test": "Counselor/Advisor",
                "value": "Counselor/Advisor",
              },
              {
                "test": "Peer (Friend, Classmate, Family)",
                "value": "Peer (Friend, Classmate, Family)",
              },
              {
                "test": "Senior",
                "value": "Senior",
              },
              {
                "test": "Teacher (11th or 12th Grade)",
                "value": "Teacher (11th or 12th Grade)",
              },
              {
                "test": "Core Subject Teacher",
                "value": "Core Subject Teacher",
              },
              {
                "test": "Professor (PhD)",
                "value": "Professor (PhD)",
              },
              {
                "test": "Principal/Dean",
                "value": "Principal/Dean",
              },
              {
                "test": "Associate/Asst. Professor",
                "value": "Associate/Asst. Professor",
              },
              {
                "test": "Mentor/Coach",
                "value": "Mentor/Coach",
              },
              {
                "test": "Employer",
                "value": "Employer",
              },
              {
                "test": "Manager/Supervisor",
                "value": "Manager/Supervisor",
              },
              {
                "test": "Co-worker",
                "value": "Co-worker",
              }
            ]
          }
        ]
      },
      {
        "text": "5",
        "value": "5",
        "isLogical": true,
        "isRequired": true,

        "questions": [
          {
            "text": "Who has written your Letter(s) of Recommendation? (Choose 1 or more)",
            "code": "RECOMMENDATION_SELECT",
            "type": "checkbox",
            "tips": "If you have a Letter(s) of Recommendation then you need to select below options by whom it has been written.",
            "isLogical": true,
            "isRequired": true,

            "options": [
              {
                "test": "Counselor/Advisor",
                "value": "Counselor/Advisor",
              },
              {
                "test": "Peer (Friend, Classmate, Family)",
                "value": "Peer (Friend, Classmate, Family)",
              },
              {
                "test": "Senior",
                "value": "Senior",
              },
              {
                "test": "Teacher (11th or 12th Grade)",
                "value": "Teacher (11th or 12th Grade)",
              },
              {
                "test": "Core Subject Teacher",
                "value": "Core Subject Teacher",
              },
              {
                "test": "Professor (PhD)",
                "value": "Professor (PhD)",
              },
              {
                "test": "Principal/Dean",
                "value": "Principal/Dean",
              },
              {
                "test": "Associate/Asst. Professor",
                "value": "Associate/Asst. Professor",
              },
              {
                "test": "Mentor/Coach",
                "value": "Mentor/Coach",
              },
              {
                "test": "Employer",
                "value": "Employer",
              },
              {
                "test": "Manager/Supervisor",
                "value": "Manager/Supervisor",
              },
              {
                "test": "Co-worker",
                "value": "Co-worker",
              }
            ]
          }
        ]
      },
      {
        "text": "More than 5",
        "value": "More than 5",
        "isLogical": true,
        "isRequired": true,
        "questions": [
          {
            "text": "Who has written your Letter(s) of Recommendation? (Choose 1 or more)",
            "code": "RECOMMENDATION_SELECT",
            "type": "checkbox",
            "tips": "If you have a Letter(s) of Recommendation then you need to select below options by whom it has been written.",
            "isLogical": true,
            "isRequired": true,

            "options": [
              {
                "test": "Counselor/Advisor",
                "value": "Counselor/Advisor",
              },
              {
                "test": "Peer (Friend, Classmate, Family)",
                "value": "Peer (Friend, Classmate, Family)",
              },
              {
                "test": "Senior",
                "value": "Senior",
              },
              {
                "test": "Teacher (11th or 12th Grade)",
                "value": "Teacher (11th or 12th Grade)",
              },
              {
                "test": "Core Subject Teacher",
                "value": "Core Subject Teacher",
              },
              {
                "test": "Professor (PhD)",
                "value": "Professor (PhD)",
              },
              {
                "test": "Principal/Dean",
                "value": "Principal/Dean",
              },
              {
                "test": "Associate/Asst. Professor",
                "value": "Associate/Asst. Professor",
              },
              {
                "test": "Mentor/Coach",
                "value": "Mentor/Coach",
              },
              {
                "test": "Employer",
                "value": "Employer",
              },
              {
                "test": "Manager/Supervisor",
                "value": "Manager/Supervisor",
              },
              {
                "test": "Co-worker",
                "value": "Co-worker",
              }
            ]
          }
        ]
      },

    ],
    "placeholder": "",
    "type": "dropDown",
    "stream": "engineering"
  },
  {
    "section": "academicInfo",
    "text": "List all other colleges or universities, including graduate programs you have attended beyond high school",
    "code": "ADDRESSANDOTHERDETAIL",
    "isLogical": true,
    "questions": [
      {
        "options": [
          {
            "type": "text",
            "text": "Post-Secondary School, College/University, English Language Institute",  
            "code": "POSTSECONDARYSCHOOL",
          },
          {
            "text": "Location (City, State, Country)",
            "type": "text",
            "code": "LOCATIONCITYSTATE",
          },
          {
            "text": "Degree Obtained/ Major",
            "type": "text",
            "code": "DEGREEOBTAINED",
          },
          {
            "text": "Years attended",
            "type": "text",
            "code": "YEARSATTENDED",
          },
          {
            "text": "Language of Instruction",
            "type": "text",
            "code": "LANGUAGEOFINST",
          }
        ]
      },
      {
        "options": [
          {
            "type": "text",
            "text": "",
            "code": "POSTSECONDARYSCHOOL2",
          },
          {
            "text": "",
            "type": "text",
            "code": "LOCATIONCITYSTATE2",
          },
          {
            "text": "",
            "type": "text",
            "code": "DEGREEOBTAINED2",
          },
          {
            "text": "",
            "type": "text",
            "code": "YEARSATTENDED2",
          },
          {
            "text": "",
            "type": "text",
            "code": "LANGUAGEOFINST2",
          }
        ]
      },
      {
        "options": [
          {
            "type": "text",
            "text": "",
            "code": "POSTSECONDARYSCHOOL3",
          },
          {
            "text": "",
            "type": "text",
            "code": "LOCATIONCITYSTATE3",
          },
          {
            "text": "",
            "type": "text",
            "code": "DEGREEOBTAINED3",
          },
          {
            "text": "",
            "type": "text",
            "code": "YEARSATTENDED3",
          },
          {
            "text": "",
            "type": "text",
            "code": "LANGUAGEOFINST3",
          }
        ]
      },
      {
        "options": [
          {
            "type": "text",
            "text": "",
            "code": "POSTSECONDARYSCHOOL4",
          },
          {
            "text": "",
            "type": "text",
            "code": "LOCATIONCITYSTATE4",
          },
          {
            "text": "",
            "type": "text",
            "code": "DEGREEOBTAINED4",
          },
          {
            "text": "",
            "type": "text",
            "code": "YEARSATTENDED4",
          },
          {
            "text": "",
            "type": "text",
            "code": "LANGUAGEOFINST4",
          }
        ]
      },
      {
        "options": [
          {
            "type": "text",
            "text": "",
            "code": "POSTSECONDARYSCHOOL5",
          },
          {
            "text": "",
            "type": "text",
            "code": "LOCATIONCITYSTATE5",
          },
          {
            "text": "",
            "type": "text",
            "code": "DEGREEOBTAINED5",
          },
          {
            "text": "",
            "type": "text",
            "code": "YEARSATTENDED5",
          },
          {
            "text": "",
            "type": "text",
            "code": "LANGUAGEOFINST5",
          }
        ]
      }
    ],
    "placeholder": "",
    "type": "categorical",
    "stream": "engineering"
  },
]

export const CollegePreference = [
  {
    "section": "collegePreference",
    "isRequired": true,

    "text": "What are your priorities when considering admission in a particular college? (Choose 1 or more)",
    "code": "PRIORITYCOLLEGE",
    "tips": 'Curricular Practical Training is a program that temporarily allows international students with an F-1 visa to gain practical experience directly related to their major through employment, paid or unpaid internships, or cooperative education.',
    "options": [
      {
        "text": "Academic & career support services",
        "value": "Academic & career support services"
      },
      {
        "text": "Affordable fees",
        "value": "Affordable fees"
      },
      {
        "text": "Clubs & activities",
        "value": "Clubs & activities"
      },
      {
        "text": "Expert faculty",
        "value": "Expert faculty"
      },
      {
        "text": "Flexible curriculum",
        "value": "Flexible curriculum"
      },
      {
        "text": "Health & wellness facilities",
        "value": "Health & wellness facilities"
      },
      {
        "text": "Higher acceptance",
        "value": "Higher acceptance"
      },
      {
        "text": "Higher grad rates",
        "value": "Higher grad rates"
      },
      {
        "text": "International student friendly",
        "value": "International student friendly"
      },
      {
        "text": "Internship opp.",
        "value": "Internship opp."
      },
      {
        "text": "Prayer room on campus",
        "value": "Prayer room on campus"
      },
      {
        "text": "Research opp.",
        "value": "Research opp."
      },
      {
        "text": "Great job prospects",
        "value": "Great job prospects"
      },
      {
        "text": "Strong corporate collaboration",
        "value": "Strong corporate collaboration"
      },
      {
        "text": "Location & setting",
        "value": "Location & setting"
      },
      {
        "text": "Work opportunity at campus",
        "value": "Work opportunity at campus"
      },
      {
        "text": "Musical and Broadway shows",
        "value": "Musical and Broadway shows"
      },
      {
        "text": "Athletics/Intramural Sports/Outdoors",
        "value": "Athletics/Intramural Sports/Outdoors"
      },
      {
        "text": "Low student to faculty ratio",
        "value": "Low student to faculty ratio"
      },
      {
        "text": "Social scene",
        "value": "Social scene"
      },
      {
        "text": "State-of-art labs & classrooms",
        "value": "State-of-art labs & classrooms"
      },
      {
        "text": "Strong alumni network",
        "value": "Strong alumni network"
      },
      {
        "text": "Student diversity",
        "value": "Student diversity"
      },
      {
        "text": "TOEFL optional for certain countries",
        "value": "TOEFL optional for certain countries"
      },
      {
        "text": "Optional admission entrances",
        "value": "Optional admission entrances"
      },
      {
        "text": "Optional essay req.",
        "value": "Optional essay req."
      },
      {
        "text": "CPT on day 1 (refer to tooltip)",
        "value": "CPT on day 1 (refer to tooltip)"
      }
    ],
    "placeholder": "",
    "type": "checkbox",
    "stream": "engineering"
  },
  {
    "section": "collegePreference",
    // "isRequired": true,
    "text": "Are you looking for Bridge programs that will enable you to pursue higher education in US?",
    "code": "BRIDGE_PROGRAM",
    "tips": 'This question is applicable for students who may not meet the eligibility criteria to pursue higher degrees. For e.g., minimum credit score, 4-year degree etc.',
    "options": [
      {
        "text": "Bridge program for Bachelors",
        "value": "Bridge program for Bachelors"
      },
      {
        "text": "Bridge program for Masters",
        "value": "Bridge program for Masters"
      },
      {
        "text": "Not Required",
        "value": "Not Required"
      },
    ],
    "placeholder": "",
    "type": "dropDown",
    "stream": "engineering"
  },
  {
    "section": "collegePreference",
    "isRequired": true,

    "text": " Which U.S. colleges/universities based on Ranking would you be interested in joining?",
    "code": "JOINRANKINGCOLLEGE",
    "tips": 'You can select here the colleges based on their rankings for e.g. – if you prefer top 20 colleges then select < Top 20 or if you have no such preference then select no preference/all colleges.',
    "options": [
      {
        "text": "Top 20",
        "value": "Top 20"
      },
      {
        "text": "Top 50",
        "value": "Top 50"
      },
      {
        "text": "Top 100",
        "value": "Top 100"
      },
      {
        "text": "Top 200",
        "value": "Top 200"
      },
      {
        "text": "No Preferences / All colleges",
        "value": "No Preferences / All colleges"
      }
    ],
    "placeholder": "",
    "type": "radio",
    "stream": "engineering"
  },
  {
    "section": "collegePreference",
    "text": " Which colleges/universities are you targeting for?",
    "code": "TARGETCOLLEGES",
    "options": [],
    "placeholder": "",
    "type": "text",
    "stream": "engineering",
    "tips": 'Type first few letters of the college/ university, to select from the dropdown.',
  },

  {
    "section": "collegePreference",
    "isRequired": true,

    "text": "What is your preferred college size? (student population, Choose 1 or more)",
    "code": "PREFRDCOLLSIZE",
    "options": [
      {
        "text": "1,000 - 4,999",
        "value": "1,000 - 4,999"
      },
      {
        "text": "5,000 - 9,999",
        "value": "5,000 - 9,999"
      },
      {
        "text": "10,000 - 19,999",
        "value": "10,000 - 19,999"
      },
      {
        "text": "20,000 and above",
        "value": "20,000 and above"
      }
    ],
    "placeholder": "",
    "type": "checkbox",
    "stream": "engineering"
  },

  {
    "section": "collegePreference",
    "isRequired": true,

    "text": " Which mode of education do you prefer?",
    "code": "MODOFEDUCATION",
    "options": [
      {
        "text": "Online",
        "value": "Online"
      },
      {
        "text": "On campus",
        "value": "On campus"
      },
      {
        "text": "Hybrid",
        "value": "Hybrid"
      }
    ],
    "placeholder": "",
    "type": "radio",
    "stream": "engineering"
  },
  {
    "section": "collegePreference",
    "isRequired": true,

    "text": "Which college clubs would you be most interested in joining? (Choose 1 or more)",
    "code": "COLLEGECLUBS",
    "isLogical": true,

    "options": [
      {
        "text": "Academic (math, literature, language, history)",
        "value": "Academic (math, literature, language, history)"
      },
      {
        "text": "Business & entrepreneurship",
        "value": "Business & entrepreneurship"
      },
      {
        "text": "Political & government (student senate)",
        "value": "Political & government (student senate)"
      },
      {
        "text": "Media & publication",
        "value": "Media & publication"
      },
      {
        "text": "Community service & social justice",
        "value": "Community service & social justice"
      },
      {
        "text": "Theater & the arts (drama, film, performing arts)",
        "value": "Theater & the arts (drama, film, performing arts)"
      },
      {
        "text": "International (country, multicultural)",
        "value": "International (country, multicultural)",
        "isLogical": true,
        "questions": [
          {
            "isRequired": true,
            "text": "Which country club are you looking to join?",
            "code": "MULTICULTURALCOUNTRY",
            "type": "dropDown"
          }
        ]
      },
      {
        "text": "Health & counseling",
        "value": "Health & counseling"
      },
      {
        "text": "Science & engineering",
        "value": "Science & engineering"
      },
      {
        "text": "Religious & spiritual (philosophy, greek life)",
        "value": "Religious & spiritual (philosophy, greek life)"
      },
      {
        "text": "Athletics, sports & recreation",
        "value": "Athletics, sports & recreation"
      },
      {
        "text": "Music & dance (orchestras, choirs, and music) ",
        "value": "Music & dance (orchestras, choirs, and music) "
      },
      {
        "text": "Special interest (culinary, games, travel)",
        "value": "Special interest (culinary, games, travel)"
      },
      {
        "text": "Professional development",
        "value": "Professional development"
      },
      {
        "text": "Other",
        "value": "Other",
        "isLogical": true,
        "questions": [
          {
            "isRequired": true,
            "text": "Other",
            "code": "COLLEGECLUBSOTHER",
            "type": "text"
          }
        ]
      }
    ],
    "placeholder": "",
    "type": "checkbox",
    "stream": "engineering"
  },

  {
    "section": "collegePreference",
    "isRequired": true,
    "text": "Which of the following sports facilities matter to you the most? (choose 1 or more)",
    "code": "SPORTSFACILITY",
    "isLogical": true,
    "options": [
      {
        "text": "Archery",
        "value": "Archery"
      },
      {
        "text": "Badminton",
        "value": "Badminton"
      },
      {
        "text": "Baseball",
        "value": "Baseball"
      },
      {
        "text": "Basketball",
        "value": "Basketball"
      },
      {
        "text": "Cross-country",
        "value": "Cross-country"
      },
      {
        "text": "Cycling",
        "value": "Cycling"
      },
      {
        "text": "Cricket",
        "value": "Cricket"
      },
      {
        "text": "Fencing",
        "value": "Fencing"
      },
      {
        "text": "Field hockey",
        "value": "Field hockey"
      },
      {
        "text": "Football",
        "value": "Football"
      },
      {
        "text": "Golf",
        "value": "Golf"
      },
      {
        "text": "Gymnastics",
        "value": "Gymnastics"
      },
      {
        "text": "Ice hockey  ",
        "value": "Ice hockey  "
      },
      {
        "text": "Lacrosse",
        "value": "Lacrosse"
      },
      {
        "text": "Martial arts",
        "value": "Martial arts"
      },
      {
        "text": "Rowing",
        "value": "Rowing"
      },
      {
        "text": "Rugby",
        "value": "Rugby"
      },
      {
        "text": "Skiing",
        "value": "Skiing"
      },
      {
        "text": "Soccer",
        "value": "Soccer"
      },
      {
        "text": "Softball",
        "value": "Softball"
      },
      {
        "text": "Squash",
        "value": "Squash"
      },
      {
        "text": "Swimming",
        "value": "Swimming"
      },
      {
        "text": "Tennis",
        "value": "Tennis"
      },
      {
        "text": "Track & field",
        "value": "Track & field"
      },
      {
        "text": "Volleyball",
        "value": "Volleyball"
      },
      {
        "text": "Wrestling",
        "value": "Wrestling"
      },
      {
        "text": "Other",
        "value": "Other",
        "isLogical": true,
        "questions": [
          {
            "isRequired": true,
            "text": "Other",
            "code": "SPORTSFACILITYOTHER",
            "type": "text"
          }
        ]
      }
    ],
    "placeholder": "",
    "type": "checkbox",
    "stream": "engineering"
  },

  {
    "section": "collegePreference",
    "isRequired": true,
    "text": "What is your preference in college housing (dorm)?",
    "code": "ROOMATESCOLLEGE",
    "tips": 'If you prefer a dorm consisting of only freshman (1st year) students, you can select “1st year only (Freshman)” or only seniors i.e 4th year students then you can select “4th year”.',
    "options": [
      {
        "text": "1st year only (Freshman)",
        "value": "1st year only (Freshman)"
      },
      {
        "text": "1st and 2nd year (Sophomore)",
        "value": "1st and 2nd year (Sophomore)"
      },
      {
        "text": "1st, 2nd and 3rd year (Junior)",
        "value": "1st, 2nd and 3rd year (Junior)"
      },
      {
        "text": "Till 4th year (Senior)",
        "value": "Till 4th year (Senior)"
      },
      {
        "text": "Graduate students",
        "value": "Graduate students"
      },
      {
        "text": "Families (Graduates / PhD)",
        "value": "Families (Graduates / PhD)"
      },
      {
        "text": "No preference",
        "value": "No preference"
      },
      {
        "text": "Not applicable",
        "value": "Not applicable"
      }
    ],
    "placeholder": "",
    "type": "dropDown",
    "stream": "engineering"
  },
  {
    "section": "collegePreference",
    "text": "Which campus safety features are important to you? (Choose 1 or more)",
    "code": "CAMPUSSAFTYFEATURE",
    "isLogical": true,

    "options": [
      {
        "text": "24-hour foot and vehicle patrols",
        "value": "24-hour foot and vehicle patrols"
      },
      {
        "text": "Lit pathways/sidewalks",
        "value": "Lit pathways/sidewalks"
      },
      {
        "text": "24-hour emergency helpline",
        "value": "24-hour emergency helpline"
      },
      {
        "text": "Controlled access to buildings (dormitory, campus, etc.)",
        "value": "Controlled access to buildings (dormitory, campus, etc.)"
      },
      {
        "text": "Security cameras used to monitor campus",
        "value": "Security cameras used to monitor campus"
      },
      {
        "text": "Require faculty, staff & students to wear picture IDs",
        "value": "Require faculty, staff & students to wear picture IDs"
      },
      {
        "text": "Late night transport/safe escort service",
        "value": "Late night transport/safe escort service"
      },
      {
        "text": "Emergency notification system",
        "value": "Emergency notification system"
      },
      {
        "text": "Other",
        "value": "Other",
        "isLogical": true,

        "questions": [
          {
            "isRequired": true,
            "text": "Other",
            "code": "CAMPUSSAFTYFEATUREOTHER",
            "type": "text"
          }
        ]
      }
    ],
    "placeholder": "",
    "type": "checkbox",
    "stream": "engineering"
  }
]

export const DemoGraphicPreference = [
  {
    "section": "demoGraphicPreference",
    "text": "Will you be the first generation student graduating ?",
    "code": "FIRSTGENGRADUATE",
    "tips": 'A first-generation student is a student who is the first in their family, not including siblings, to attend a post-secondary institution, such as a college, university, or apprenticeship in the United States.',
    "options": [
      {
        "text": "Yes",
        "value": "Yes"
      },
      {
        "text": "No",
        "value": "No"
      }
    ],
    "placeholder": "",
    "type": "radio",
    "stream": "engineering",
    "isRequired": true,

  },
  {
    "section": "demoGraphicPreference",
    "text": "What is your ethnicity?",
    "code": "ETHINICITY",
    "tips": 'The fact or state of belonging to a social group that has a common national or cultural tradition & identification.',
    "isLogical": true,

    "options": [
      {
        "text": "American Indian or Alaska Native",
        "value": "American Indian or Alaska Native"
      },
      {
        "text": "Asian",
        "value": "Asian"
      },
      {
        "text": "American",
        "value": "American"
      },
      {
        "text": "Asian American",
        "value": "Asian American"
      },
      {
        "text": "Black or African American",
        "value": "Black or African American"
      },
      {
        "text": "Hispanic / Latino / Spanish origin",
        "value": "Hispanic / Latino / Spanish origin"
      },
      {
        "text": "Mexican American, Chicano",
        "value": "Mexican American, Chicano"
      },
      {
        "text": "Native Hawaiian or Other Pacific Islander",
        "value": "Native Hawaiian or Other Pacific Islander"
      },
      {
        "text": "Puerto Rican",
        "value": "Puerto Rican"
      },
      {
        "text": "White or Caucasian",
        "value": "White or Caucasian"
      },
      {
        "text": "Others",
        "value": "Others",
        "isLogical": true,
        "questions": [
          {
            "isRequired": true,
            "text": "Please mention the ethnicity",
            "code": "ETHINICITYOTHER",
            "type": "text"
          }
        ]
      }
    ],
    "placeholder": "",
    "type": "dropDown",
    "stream": "engineering",
    "isRequired": true,

  },
  {
    "section": "demoGraphicPreference",
    "text": "Do you prefer colleges which follow a specialized mission?",
    "code": "COLLEGESPECIALISEDMISSION",
    "tips": 'Specialized mission colleges focus on educating specific groups of students. For example, if you are looking for an “only girls college” then you can select “Yes, Single sex – women”.',
    "options": [
      {
        "text": "Catholic",
        "value": "Catholic"
      },
      {
        "text": "Protestant",
        "value": "Protestant"
      },
      {
        "text": "Historically Black College or University",
        "value": "Historically Black College or University"
      },
      {
        "text": "Jewish",
        "value": "Jewish"
      },
      {
        "text": "Muslim",
        "value": "Muslim"
      },
      {
        "text": "Other Religious Affiliation",
        "value": "Other Religious Affiliation"
      },
      {
        "text": "Single-Sex: Men",
        "value": "Single-Sex: Men"
      },
      {
        "text": "Single-Sex: Women",
        "value": "Single-Sex: Women"
      },
      {
        "text": "Tribal College",
        "value": "Tribal College"
      },
      {
        "text": "No preference",
        "value": "No preference"
      },
    ],
    "type": "dropDown",
    "stream": "engineering",
    "placeholder": "",
    "isRequired": false
  }
]

export const ClimatePreference = [
  {
    "section": "climatePreference",
    "text": "What are your preferred weather conditions (Choose 1 or more,  Approx. Temperature)",
    "code": "PREFERREDWHEATHER",
    "options": [
      {
        "text": "Hot (Above 35C / 95F)",
        "value": "Hot (Above 35C / 95F)"
      },
      {
        "text": "Moderate (15C to 35C / 59F to 95F)",
        "value": "Moderate (15C to 35C / 59F to 95F)"
      },
      {
        "text": "Cold ( -15C to 15C / 5F to 59F)",
        "value": "Cold ( -15C to 15C / 5F to 59F)"
      }
    ],
    "placeholder": "",
    "type": "checkbox",
    "stream": "engineering",
    "isRequired": true,

  },
  {
    "section": "climatePreference",
    "text": "Do you have any seasonal allergies?",
    "code": "ALLERGY",
    "tips": 'Seasonal allergies, such as "hay fever", “Pollen” or any other allergies that happen during certain times of the year.',
    "isLogical": true,
    "isRequired": true,

    "placeholder": "",
    "type": "radio",
    "stream": "engineering",
    "options": [
      {
        "text": "Yes",
        "value": "Yes",
        "isLogical": true,
        "questions": [
          {
            "isRequired": false,
            "text": "Please mention the allergy",
            "code": "ALLERGYMENTION",
            "type": "text"
          }
        ]
      },
      {
        "text": "No",
        "value": "No"
      },
      {
        "text": "I don’t know",
        "value": "I don’t know"
      }
    ]
  }
]



export const LocationPreference = [
  {
    "section": "locationPreference",
    "text": "What are your preferred college settings? (Choose 1 or more)",
    "code": "PREFLOCATION",
    "tips": 'You need to select your preferred  college area/location in the below options.',
    "options": [
      {
        "text": "Rural",
        "value": "Rural"
      },
      {
        "text": "City",
        "value": "City"
      },
      {
        "text": "Town",
        "value": "Town"
      },
      {
        "text": "Suburbs",
        "value": "Suburbs"
      },
      {
        "text": "No preference",
        "value": "No preference"
      }
    ],
    "placeholder": "",
    "type": "checkbox",
    "stream": "engineering",
    "isRequired": true,

  },
  {
    "section": "locationPreference",
    "text": "Do you prefer colleges that are closer to home? If yes, please select the preferred proximity from home (miles)",
    "code": "CLOSERCOLLEGE",
    "options": [
      {
        "text": "No preference",
        "value": "No preference"
      },
      {
        "text": "5 miles",
        "value": "5 miles"
      },
      {
        "text": "10 miles",
        "value": "10 miles"
      },
      {
        "text": "15 miles",
        "value": "15 miles"
      },
      {
        "text": "20 miles",
        "value": "20 miles"
      },
      {
        "text": "25 miles",
        "value": "25 miles"
      },
      {
        "text": "50 miles",
        "value": "50 miles"
      },
      {
        "text": "100 miles",
        "value": "100 miles"
      },
      {
        "text": "200 miles",
        "value": "200 miles"
      },
      {
        "text": "250 miles",
        "value": "250 miles"
      },
      {
        "text": "Not applicable",
        "value": "Not applicable"
      }
    ],
    "placeholder": "",
    "type": "dropDown",
    "stream": "engineering",
    "isRequired": true,

  }
]

export const EmploymentInfo = [
  {
    "section": "employmentInfo",
    "text": "Which of the following personality traits do you possess or have experienced? (Choose 1 or more)",
    "code": "PERSNLITYTRAITS",
    "isRequired": true,

    "options": [
      {
        "text": "Commitment",
        "value": "Commitment"
      },
      {
        "text": "Creativity",
        "value": "Creativity"
      },
      {
        "text": "Critical thinking ability",
        "value": "Critical thinking ability"
      },
      {
        "text": "Effective decision making",
        "value": "Effective decision making"
      },
      {
        "text": "Honesty",
        "value": "Honesty"
      },
      {
        "text": "Initiative",
        "value": "Initiative"
      },
      {
        "text": "Integrity",
        "value": "Integrity"
      },
      {
        "text": "Intellectual curiosity",
        "value": "Intellectual curiosity"
      },
      {
        "text": "Leadership skills",
        "value": "Leadership skills"
      },
      {
        "text": "Overcoming challenges",
        "value": "Overcoming challenges"
      },
      {
        "text": "Problem solving skills",
        "value": "Problem solving skills"
      },
      {
        "text": "Sincerity",
        "value": "Sincerity"
      },
      {
        "text": "Teamwork",
        "value": "Teamwork"
      },
      {
        "text": "Self discipline",
        "value": "Self discipline"
      },
      {
        "text": "Empathy",
        "value": "Empathy"
      },
      {
        "text": "Sportsmanship",
        "value": "Sportsmanship"
      }
    ],
    "placeholder": "",
    "type": "checkbox",
    "stream": "engineering"
  },
  {
    "section": "employmentInfo",
    "text": "Share an experience or story where you have utilized or demonstrated the personality traits (Min. 100 words)",
    "code": "PERSNLITYTRAITSSTRY",
    "tips": 'Tell us about the time when you used or displayed any one of the personality traits mentioned in the previous question.',
    "options": [],
    "placeholder": "(Min. 100 words)",
    "type": "textArea",
    "stream": "engineering"
  },
  {
    "section": "employmentInfo",
    "text": "To help us assess your application, please provide a brief summary of your achievements, work experience, qualifications, certifications, extra-curricular activities, etc",
    "code": "APP_SUMMARY",
    "options": [],
    "placeholder": "achievements, work experience, qualifications, certifications, extra-curricular activities, etc",
    "type": "textArea",
    "stream": "engineering"
  },
  {
    "section": "employmentInfo",
    "text": "How many hours have you dedicated towards community service or volunteer work in the last 3 years?",
    "code": "HRSCOMMUNITYSERVICE",
    "isRequired": true,

    "options": [
      {
        "text": "1-10 hrs",
        "value": "1-10 hrs"
      },
      {
        "text": "10-20 hrs",
        "value": "10-20 hrs"
      },
      {
        "text": "20-30 hrs ",
        "value": "20-30 hrs "
      },
      {
        "text": "30-40hrs",
        "value": "30-40hrs"
      },
      {
        "text": "40-50 hrs",
        "value": "40-50 hrs"
      },
      {
        "text": "More than 50 hours",
        "value": "More than 50 hours"
      },
      {
        "text": "Not applicable",
        "value": "Not applicable"
      }
    ],
    "placeholder": "",
    "type": "dropDown",
    "stream": "engineering"
  },
  {
    "section": "employmentInfo",
    "text": "What volunteering activities did you participate in? List down those activities. (E.g.  Activity1, Activity2, etc.)",
    "code": "VOLUNACTIVITY",
    "tips": 'Please be specific with volunteering activities. For example, Fundraising for Non-profit, Mentorship for Non-profit middle school students, Old age home operations, Event planning for cultural event. Also mention name of NGO’s, if any.',
    "options": [],
    "placeholder": "",
    "type": "text",
    "stream": "engineering"
  },
  {
    "section": "employmentInfo",
    "text": "What is your current employment status?",
    "code": "EMPSTATUS",
    "isLogical": true,
    "isRequired": true,

    "options": [
      {
        "text": "Employed Full - time(40+ hours a week)",
        "value": "Employed Full - time(40+ hours a week)",
        "isLogical": true,

        "questions": [
          {
            "text": "Company Name",
            "code": "EMPLOYEDFULLCOMPANYDETAIL",
            "type": "text",
          },
          {
            "text": "Company Designation/Role",
            "code": "EMPLOYEDFULLCOMPANYROLE",
            "type": "text",
          },
          {
            "text": "LinkedIn profile page URL (type 'NA'  if not applicable)",
            "code": "EMPLOYEDFULLLINKEDINPRFL",
            "type": "text",
            "isRequired": true,

          },
          {
            "text": "How many years of experience do you have?",
            "code": "EMPLOYEDFULLTOTALEXPERIENCE",
            "type": "dropDown",
            "isRequired": true,

            "options": [
              {
                "text": "0-6 months",
                "value": "0-6 months"
              },
              {
                "text": "<1 year ",
                "value": "<1 year "
              },
              {
                "text": "1 year ",
                "value": "1 year "
              },
              {
                "text": "2 years ",
                "value": "2 years "
              },
              {
                "text": "3 years ",
                "value": "3 years "
              },
              {
                "text": "4 years ",
                "value": "4 years "
              },
              {
                "text": "5 years ",
                "value": "5 years "
              },
              {
                "text": "6 years ",
                "value": "6 years "
              },
              {
                "text": "7 years ",
                "value": "7 years "
              },
              {
                "text": "8 years ",
                "value": "8 years "
              },
              {
                "text": "9 years ",
                "value": "9 years "
              },
              {
                "text": "10 years ",
                "value": "10 years "
              },
              {
                "text": "10+ years",
                "value": "10+ years"
              }
            ]
          },
          {
            "text": "Which sector / industry have you been working in?",
            "code": "EMPLOYEDFULLSECTOR",
            "type": "dropDown",
            "options": [
              {
                "text": "Information technology & services",
                "value": "Information technology & services"
              },
              {
                "text": "Computer software",
                "value": "Computer software"
              },
              {
                "text": "Hospital & health care",
                "value": "Hospital & health care"
              },
              {
                "text": "Higher education",
                "value": "Higher education"
              },
              {
                "text": "Construction",
                "value": "Construction"
              },
              {
                "text": "Automotive",
                "value": "Automotive"
              },
              {
                "text": "Retail",
                "value": "Retail"
              },
              {
                "text": "Financial services",
                "value": "Financial services"
              },
              {
                "text": "Education management",
                "value": "Education management"
              },
              {
                "text": "Accounting",
                "value": "Accounting"
              },
              {
                "text": "Other ",
                "value": "Other "
              }
            ]
          }
        ]
      },
      {
        "text": "Employed Part - time (less than 40 hours a week)",
        "value": "Employed Part - time (less than 40 hours a week)",
        "isLogical": true,

        "questions": [
          {
            "text": "Company Name",
            "code": "EMPLOYEDPARTCOMPANYDETAIL",
            "type": "text"
          },
          {
            "text": "Company Designation/Role",
            "code": "EMPLOYEDPARTCOMPANYROLE",
            "type": "text",
          },
          {
            "text": "LinkedIn profile page URL (type 'NA'  if not applicable)",
            "code": "EMPLOYEDPARTLINKEDINPRFL",
            "type": "text",
            "isRequired": true,
          },
          {
            "text": "How many years of experience do you have?",
            "code": "EMPLOYEDPARTTOTALEXPERIENCE",
            "type": "dropDown",
            "options": [
              {
                "text": "0-6 months",
                "value": "0-6 months"
              },
              {
                "text": "<1 year ",
                "value": "<1 year "
              },
              {
                "text": "1 year ",
                "value": "1 year "
              },
              {
                "text": "2 years ",
                "value": "2 years "
              },
              {
                "text": "3 years ",
                "value": "3 years "
              },
              {
                "text": "4 years ",
                "value": "4 years "
              },
              {
                "text": "5 years ",
                "value": "5 years "
              },
              {
                "text": "6 years ",
                "value": "6 years "
              },
              {
                "text": "7 years ",
                "value": "7 years "
              },
              {
                "text": "8 years ",
                "value": "8 years "
              },
              {
                "text": "9 years ",
                "value": "9 years "
              },
              {
                "text": "10 years ",
                "value": "10 years "
              },
              {
                "text": "10+ years",
                "value": "10+ years"
              }
            ]
          },
          {
            "text": "Which sector / industry have you been working in?",
            "code": "EMPLOYEDPARTSECTOR",
            "type": "dropDown",
            "options": [
              {
                "text": "Information technology & services",
                "value": "Information technology & services"
              },
              {
                "text": "Computer software",
                "value": "Computer software"
              },
              {
                "text": "Hospital & health care",
                "value": "Hospital & health care"
              },
              {
                "text": "Higher education",
                "value": "Higher education"
              },
              {
                "text": "Construction",
                "value": "Construction"
              },
              {
                "text": "Automotive",
                "value": "Automotive"
              },
              {
                "text": "Retail",
                "value": "Retail"
              },
              {
                "text": "Financial services",
                "value": "Financial services"
              },
              {
                "text": "Education management",
                "value": "Education management"
              },
              {
                "text": "Accounting",
                "value": "Accounting"
              },
              {
                "text": "Other ",
                "value": "Other "
              }
            ]
          }
        ]
      },
      {
        "text": "Student",
        "value": "Student",
        "isLogical": true,
        "questions": [
          {
            "text": "LinkedIn profile page URL (type 'NA'  if not applicable)",
            "code": "EMPLOYEDFULLLINKEDINPRFL",
            "type": "text",
            "isRequired": true,

          }
        ]
      },
      {
        "text": "Unemployed ",
        "value": "Unemployed ",
        "isLogical": true,

        "questions": [
          {
            "text": "How many years of experience do you have?",
            "code": "UNEMPLOYETOTALEXPERIENCE",
            "type": "dropDown",
            "options": [
              {
                "text": "0-6 months",
                "value": "0-6 months"
              },
              {
                "text": "<1 year ",
                "value": "<1 year "
              },
              {
                "text": "1 year ",
                "value": "1 year "
              },
              {
                "text": "2 years ",
                "value": "2 years "
              },
              {
                "text": "3 years ",
                "value": "3 years "
              },
              {
                "text": "4 years ",
                "value": "4 years "
              },
              {
                "text": "5 years ",
                "value": "5 years "
              },
              {
                "text": "6 years ",
                "value": "6 years "
              },
              {
                "text": "7 years ",
                "value": "7 years "
              },
              {
                "text": "8 years ",
                "value": "8 years "
              },
              {
                "text": "9 years ",
                "value": "9 years "
              },
              {
                "text": "10 years ",
                "value": "10 years "
              },
              {
                "text": "10+ years",
                "value": "10+ years"
              }
            ]
          },
          {
            "text": "Which sector / industry have you been working in?",
            "code": "UNEMPLOYESECTOR",
            "type": "dropDown",
            "options": [
              {
                "text": "Information technology & services",
                "value": "Information technology & services"
              },
              {
                "text": "Computer software",
                "value": "Computer software"
              },
              {
                "text": "Hospital & health care",
                "value": "Hospital & health care"
              },
              {
                "text": "Higher education",
                "value": "Higher education"
              },
              {
                "text": "Construction",
                "value": "Construction"
              },
              {
                "text": "Automotive",
                "value": "Automotive"
              },
              {
                "text": "Retail",
                "value": "Retail"
              },
              {
                "text": "Financial services",
                "value": "Financial services"
              },
              {
                "text": "Education management",
                "value": "Education management"
              },
              {
                "text": "Accounting",
                "value": "Accounting"
              },
              {
                "text": "Other ",
                "value": "Other "
              }
            ]
          }
        ]
      },
      {
        "text": "Retired",
        "value": "Retired",
        "isLogical": true,

        "questions": [
          {
            "text": "How many years of experience do you have?",
            "code": "RETIREDTOTALEXPERIENCE",
            "type": "dropDown",
            "options": [
              {
                "text": "0-6 months",
                "value": "0-6 months"
              },
              {
                "text": "<1 year ",
                "value": "<1 year "
              },
              {
                "text": "1 year ",
                "value": "1 year "
              },
              {
                "text": "2 years ",
                "value": "2 years "
              },
              {
                "text": "3 years ",
                "value": "3 years "
              },
              {
                "text": "4 years ",
                "value": "4 years "
              },
              {
                "text": "5 years ",
                "value": "5 years "
              },
              {
                "text": "6 years ",
                "value": "6 years "
              },
              {
                "text": "7 years ",
                "value": "7 years "
              },
              {
                "text": "8 years ",
                "value": "8 years "
              },
              {
                "text": "9 years ",
                "value": "9 years "
              },
              {
                "text": "10 years ",
                "value": "10 years "
              },
              {
                "text": "10+ years",
                "value": "10+ years"
              }
            ]
          },
          {
            "text": "Which sector / industry have you been working in?",
            "code": "RETIREDSECTOR",
            "type": "dropDown",
            "options": [
              {
                "text": "Information technology & services",
                "value": "Information technology & services"
              },
              {
                "text": "Computer software",
                "value": "Computer software"
              },
              {
                "text": "Hospital & health care",
                "value": "Hospital & health care"
              },
              {
                "text": "Higher education",
                "value": "Higher education"
              },
              {
                "text": "Construction",
                "value": "Construction"
              },
              {
                "text": "Automotive",
                "value": "Automotive"
              },
              {
                "text": "Retail",
                "value": "Retail"
              },
              {
                "text": "Financial services",
                "value": "Financial services"
              },
              {
                "text": "Education management",
                "value": "Education management"
              },
              {
                "text": "Accounting",
                "value": "Accounting"
              },
              {
                "text": "Other ",
                "value": "Other "
              }
            ]
          }
        ]
      },
      {
        "text": "Self- employed",
        "value": "Self- employed",
        "isLogical": true,

        "questions": [
          {
            "text": "Company Name",
            "code": "SELFEMPLOYECOMPANYDETAIL",
            "type": "text"
          },
          {
            "text": "LinkedIn profile page URL (type 'NA'  if not applicable)",
            "code": "EMPLOYEDFULLLINKEDINPRFL",
            "type": "text",
            "isRequired": true,
          },
          {
            "text": "How many years of experience do you have?",
            "code": "SELFEMPLOYETOTALEXPERIENCE",
            "type": "dropDown",
            "options": [
              {
                "text": "0-6 months",
                "value": "0-6 months"
              },
              {
                "text": "<1 year ",
                "value": "<1 year "
              },
              {
                "text": "1 year ",
                "value": "1 year "
              },
              {
                "text": "2 years ",
                "value": "2 years "
              },
              {
                "text": "3 years ",
                "value": "3 years "
              },
              {
                "text": "4 years ",
                "value": "4 years "
              },
              {
                "text": "5 years ",
                "value": "5 years "
              },
              {
                "text": "6 years ",
                "value": "6 years "
              },
              {
                "text": "7 years ",
                "value": "7 years "
              },
              {
                "text": "8 years ",
                "value": "8 years "
              },
              {
                "text": "9 years ",
                "value": "9 years "
              },
              {
                "text": "10 years ",
                "value": "10 years "
              },
              {
                "text": "10+ years",
                "value": "10+ years"
              }
            ]
          },
          {
            "text": "Which sector / industry have you been working in?",
            "code": "SELFEMPLOYESECTOR",
            "type": "dropDown",
            "options": [
              {
                "text": "Information technology & services",
                "value": "Information technology & services"
              },
              {
                "text": "Computer software",
                "value": "Computer software"
              },
              {
                "text": "Hospital & health care",
                "value": "Hospital & health care"
              },
              {
                "text": "Higher education",
                "value": "Higher education"
              },
              {
                "text": "Construction",
                "value": "Construction"
              },
              {
                "text": "Automotive",
                "value": "Automotive"
              },
              {
                "text": "Retail",
                "value": "Retail"
              },
              {
                "text": "Financial services",
                "value": "Financial services"
              },
              {
                "text": "Education management",
                "value": "Education management"
              },
              {
                "text": "Accounting",
                "value": "Accounting"
              },
              {
                "text": "Other ",
                "value": "Other "
              }
            ]
          }
        ]
      },
      {
        "text": "Not having work authorization",
        "value": "Not having work authorization",
        "isLogical": true,

        "questions": [
          {
            "text": "LinkedIn profile page URL (type 'NA'  if not applicable)",
            "code": "EMPLOYEDFULLLINKEDINPRFL",
            "type": "text",
            "isRequired": true,
          },

        ]
      },
      {
        "text": "Internship / Training ",
        "value": "Internship / Training ",
        "isLogical": true,

        "questions": [
          {
            "text": "Company Name",
            "code": "COMPANYDETAIL",
            "type": "text"
          },
          {
            "text": "LinkedIn profile page URL (type 'NA'  if not applicable)",
            "code": "EMPLOYEDFULLLINKEDINPRFL",
            "type": "text",
            "isRequired": true,
          },
          {
            "text": "How many years of experience do you have?",
            "code": "TOTALEXPERIENCE",
            "type": "dropDown",
            "options": [
              {
                "text": "0-6 months",
                "value": "0-6 months"
              },
              {
                "text": "<1 year ",
                "value": "<1 year "
              },
              {
                "text": "1 year ",
                "value": "1 year "
              },
              {
                "text": "2 years ",
                "value": "2 years "
              },
              {
                "text": "3 years ",
                "value": "3 years "
              },
              {
                "text": "4 years ",
                "value": "4 years "
              },
              {
                "text": "5 years ",
                "value": "5 years "
              },
              {
                "text": "6 years ",
                "value": "6 years "
              },
              {
                "text": "7 years ",
                "value": "7 years "
              },
              {
                "text": "8 years ",
                "value": "8 years "
              },
              {
                "text": "9 years ",
                "value": "9 years "
              },
              {
                "text": "10 years ",
                "value": "10 years "
              },
              {
                "text": "10+ years",
                "value": "10+ years"
              }
            ]
          },
          {
            "text": "Which sector / industry have you been working in?",
            "code": "SECTOR",
            "type": "dropDown",
            "options": [
              {
                "text": "Information technology & services",
                "value": "Information technology & services"
              },
              {
                "text": "Computer software",
                "value": "Computer software"
              },
              {
                "text": "Hospital & health care",
                "value": "Hospital & health care"
              },
              {
                "text": "Higher education",
                "value": "Higher education"
              },
              {
                "text": "Construction",
                "value": "Construction"
              },
              {
                "text": "Automotive",
                "value": "Automotive"
              },
              {
                "text": "Retail",
                "value": "Retail"
              },
              {
                "text": "Financial services",
                "value": "Financial services"
              },
              {
                "text": "Education management",
                "value": "Education management"
              },
              {
                "text": "Accounting",
                "value": "Accounting"
              },
              {
                "text": "Other ",
                "value": "Other "
              }
            ]
          }
        ]
      },
      {
        "text": "Not applicable",
        "value": "Not applicable"
      }
    ],
    "placeholder": "",
    "type": "dropDown",
    "stream": "engineering"
  }

]

export const PLACEMENTPREFRENCEPRE = [
  {
    "section": "placementPreference",
    "text": "College-Company Partnerships",
    "code": "COLLEGECOMPANYPARTNERSHIP",
    "tips": 'Institute partnerships with certain companies for training/job opportunities.',
    "isRequired": true,
    "options": [
      {
        "text": 1,
        "value": 1
      },
      {
        "text": 2,
        "value": 2
      },
      {
        "text": 3,
        "value": 3
      },
      {
        "text": 4,
        "value": 4
      },
      {
        "text": 5,
        "value": 5
      },
      {
        "text": 6,
        "value": 6
      }
    ],
    "placeholder": "",
    "type": "dropDown",
    "conditionalDropDown": true,
    "stream": "engineering"
  },
  {
    "section": "placementPreference",
    "text": "Internship Opportunities",
    "code": "INTERNSHIPOPP",
    "tips": 'Institute to provide Internship opportunities for graduating students.',
    "isRequired": true,
    "options": [
      {
        "text": 1,
        "value": 1
      },
      {
        "text": 2,
        "value": 2
      },
      {
        "text": 3,
        "value": 3
      },
      {
        "text": 4,
        "value": 4
      },
      {
        "text": 5,
        "value": 5
      },
      {
        "text": 6,
        "value": 6
      }
    ],
    "placeholder": "",
    "type": "dropDown",
    "conditionalDropDown": true,
    "stream": "engineering"
  },
  {
    "section": "placementPreference",
    "text": "Employment Opportunities",
    "code": "EMPLOYMENTOPP",
    "tips": 'Institute to provide employment opportunities for graduating students.',
    "isRequired": true,
    "options": [
      {
        "text": 1,
        "value": 1
      },
      {
        "text": 2,
        "value": 2
      },
      {
        "text": 3,
        "value": 3
      },
      {
        "text": 4,
        "value": 4
      },
      {
        "text": 5,
        "value": 5
      },
      {
        "text": 6,
        "value": 6
      }
    ],
    "placeholder": "",
    "type": "dropDown",
    "conditionalDropDown": true,
    "stream": "engineering"
  },
  {
    "section": "placementPreference",
    "text": "Post-Graduation Job Assistance",
    "code": "POSTGRADUATIONJOB",
    "tips": 'Institute to provide job assistance for graduating students.',
    "isRequired": true,
    "options": [
      {
        "text": 1,
        "value": 1
      },
      {
        "text": 2,
        "value": 2
      },
      {
        "text": 3,
        "value": 3
      },
      {
        "text": 4,
        "value": 4
      },
      {
        "text": 5,
        "value": 5
      },
      {
        "text": 6,
        "value": 6
      }
    ],
    "placeholder": "",
    "type": "dropDown",
    "conditionalDropDown": true,
    "stream": "engineering"
  },
  {
    "section": "placementPreference",
    "text": "Optional Practical Training (OPT)",
    "code": "TRAININGOPT",
    "tips": 'Optional Practical Training is a period during which undergraduate and graduate students with F-1 status who have completed or have been pursuing their degrees for one academic year are permitted by the United States Citizenship and Immigration Services (USCIS) to work for one year on a student visa towards getting practical training to complement their education.',
    "isRequired": true,
    "options": [
      {
        "text": 1,
        "value": 1
      },
      {
        "text": 2,
        "value": 2
      },
      {
        "text": 3,
        "value": 3
      },
      {
        "text": 4,
        "value": 4
      },
      {
        "text": 5,
        "value": 5
      },
      {
        "text": 6,
        "value": 6
      }
    ],
    "placeholder": "",
    "type": "dropDown",
    "conditionalDropDown": true,
    "stream": "engineering"
  },
  {
    "section": "placementPreference",
    "text": "Curricular Practical Training (CPT)",
    "code": "TRAININGCPT",
    "tips": 'Curricular Practical Training is a program that temporarily allows international students with an F-1 visa to gain practical experience directly related to their major through employment, paid or unpaid internships, or cooperative education.',
    "isRequired": true,
    "options": [
      {
        "text": 1,
        "value": 1
      },
      {
        "text": 2,
        "value": 2
      },
      {
        "text": 3,
        "value": 3
      },
      {
        "text": 4,
        "value": 4
      },
      {
        "text": 5,
        "value": 5
      },
      {
        "text": 6,
        "value": 6
      }
    ],
    "placeholder": "",
    "type": "dropDown",
    "conditionalDropDown": true,
    "stream": "engineering"
  },

  // {
  //   "section": "placementPreference",
  //   "text": "College-Company Partnerships",
  //   "code": "COLLEGECOMPANYPARTNERSHIP",
  //   "tips": 'Institute partnerships with certain companies for training/job opportunities.',
  //   "placeholder": "",
  //   "type": "slider",
  //   "stream": "engineering",
  //   "isRequired": true,
  //   "hasQuestions": true,
  //   "minRange": 0,
  //   "maxRange": 6,
  //   "interval": 1
  // },
  // {
  //   "section": "placementPreference",
  //   "text": "Internship Opportunities",
  //   "code": "INTERNSHIPOPP",
  //   "tips": 'Institute to provide Internship opportunities for graduating students.',
  //   "placeholder": "",
  //   "type": "slider",
  //   "stream": "engineering",
  //   "isRequired": true,
  //   "hasQuestions": true,
  //   "minRange": 0,
  //   "maxRange": 6,
  //   "interval": 1
  // },
  // {
  //   "section": "placementPreference",
  //   "text": "Employment Opportunities",
  //   "code": "EMPLOYMENTOPP",
  //   "tips": 'Institute to provide employment opportunities for graduating students.',
  //   "placeholder": "",
  //   "type": "slider",
  //   "stream": "engineering",
  //   "isRequired": true,
  //   "hasQuestions": true,
  //   "minRange": 0,
  //   "maxRange": 6,
  //   "interval": 1
  // },
  // {
  //   "section": "placementPreference",
  //   "text": "Post-Graduation Job Assistance",
  //   "code": "POSTGRADUATIONJOB",
  //   "tips": 'Institute to provide job assistance for graduating students.',
  //   "placeholder": "",
  //   "type": "slider",
  //   "stream": "engineering",
  //   "isRequired": true,
  //   "hasQuestions": true,
  //   "minRange": 0,
  //   "maxRange": 6,
  //   "interval": 1
  // },
  // {
  //   "section": "placementPreference",
  //   "text": "Optional Practical Training (OPT)",
  //   "code": "TRAININGOPT",
  //   "tips": 'Optional Practical Training is a period during which undergraduate and graduate students with F-1 status who have completed or have been pursuing their degrees for one academic year are permitted by the United States Citizenship and Immigration Services (USCIS) to work for one year on a student visa towards getting practical training to complement their education.',
  //   "placeholder": "",
  //   "type": "slider",
  //   "stream": "engineering",
  //   "isRequired": true,
  //   "hasQuestions": true,
  //   "minRange": 0,
  //   "maxRange": 6,
  //   "interval": 1
  // },
  // {
  //   "section": "placementPreference",
  //   "text": "Curricular Practical Training (CPT)",
  //   "code": "TRAININGCPT",
  //   "tips": 'Curricular Practical Training is a program that temporarily allows international students with an F-1 visa to gain practical experience directly related to their major through employment, paid or unpaid internships, or cooperative education.',
  //   "placeholder": "",
  //   "type": "slider",
  //   "stream": "engineering",
  //   "isRequired": true,
  //   "hasQuestions": true,
  //   "minRange": 0,
  //   "maxRange": 6,
  //   "interval": 1
  // },
  // {
  //   "section": "placementPreference",
  //   "text": "Share your preferences on the below Placement options:",
  //   "code": "SHAREPREFERENCESPLACEMENT",
  //   "isRequired": false,
  //   "questions": [
  //     {
  //       "type": "radio",
  //       "text": "College-Company Partnerships",
  //       "code": "COLLEGECOMPANYPARTNERSHIP",
  //       "tips": 'Institute partnerships with certain companies for training/job opportunities.',
  //     },
  //     {
  //       "text": "Internship Opportunities",
  //       "type": "radio",
  //       "code": "INTERNSHIPOPP",
  //       "tips": 'Institute to provide Internship opportunities for graduating students.',
  //     },
  //     {
  //       "text": "Employment Opportunities",
  //       "type": "radio",
  //       "code": "EMPLOYMENTOPP",
  //       "tips": 'Institute to provide employment opportunities for graduating students.',
  //     },
  //     {
  //       "text": "Post-Graduation Job Assistance",
  //       "type": "radio",
  //       "code": "POSTGRADUATIONJOB",
  //       "tips": 'Institute to provide job assistance for graduating students.',
  //     },
  //     {
  //       "text": "Optional Practical Training (OPT)",
  //       "type": "radio",
  //       "code": "TRAININGOPT",
  //       "tips": 'Optional Practical Training is a period during which undergraduate and graduate students with F-1 status who have completed or have been pursuing their degrees for one academic year are permitted by the United States Citizenship and Immigration Services (USCIS) to work for one year on a student visa towards getting practical training to complement their education.',
  //     },
  //     {
  //       "text": "Curricular Practical Training (CPT)",
  //       "type": "radio",
  //       "code": "TRAININGCPT",
  //       "tips": 'Curricular Practical Training is a program that temporarily allows international students with an F-1 visa to gain practical experience directly related to their major through employment, paid or unpaid internships, or cooperative education.',
  //     }

  //   ],
  //   "placeholder": "",
  //   "type": "categorical",
  //   "stream": "engineering"
  // },
  {
    "section": "placementPreference",
    "text": "What is the minimum job placement rate that the college should have?",
    "code": "MINPLACEMENTRATE",
    "isRequired": true,
    "options": [
      {
        "text": "Excellent (Greater than 90%)",
        "value": "Excellent (Greater than 90%)"
      },
      {
        "text": "Very High (Greater than 80%)",
        "value": "Very High (Greater than 80%)"
      },
      {
        "text": "High (Greater than 70%)",
        "value": "High (Greater than 70%)"
      },
      {
        "text": "Medium (Greater than 60%) ",
        "value": "Medium (Greater than 60%) "
      },
      {
        "text": "Low (Greater than 50%)",
        "value": "Low (Greater than 50%)"
      },
      {
        "text": "Very Low (Greater than 40%)",
        "value": "Very Low (Greater than 40%)"
      },
      {
        "text": "No preference",
        "value": "No preference"
      }
    ],
    "placeholder": "",
    "type": "dropDown",
    "stream": "engineering"
  },
]

export const FINANCIALINFO = [
  {
    "section": "financialInfo",
    "text": "What is your current annual income?",
    "code": "CURRENTINCOME",
    "tips": 'Select your/ your family’s current income. For example, select “$0-$15,000”, if the current income is $5,000.',
    "isRequired": true,

    "options": [
      {
        "text": "$0-$15,000",
        "value": "$0-$15,000"
      },
      {
        "text": "$15,000-$30,000",
        "value": "$15,000-$30,000"
      },
      {
        "text": "$30,000-$45,000",
        "value": "$30,000-$45,000"
      },
      {
        "text": "$45,000-$60,000",
        "value": "$45,000-$60,000"
      },
      {
        "text": "$60,000-$75,000",
        "value": "$60,000-$75,000"
      },
      {
        "text": "$75,000-$85,000",
        "value": "$75,000-$85,000"
      },
      {
        "text": "$85,000-$1,00,000",
        "value": "$85,000-$1,00,000"
      },
      {
        "text": "$1,00,000 above",
        "value": "$1,00,000 above"
      }
    ],
    "placeholder": "",
    "type": "radio",
    "stream": "engineering"
  },

  {
    "section": "financialInfo",
    "text": "Did you apply for FAFSA (Federal Application for Financial Aid) ? ",
    "code": "FASA",
    "isRequired": true,

    "options": [
      {
        "text": "Yes",
        "value": "Yes"
      },
      {
        "text": "No",
        "value": "No"
      },
      {
        "text": "Not Applicable",
        "value": "Not Applicable"
      },
    ],
    "placeholder": "",
    "type": "radio",
    "stream": "engineering"
  },

  {
    "section": "financialInfo",
    "text": "What is your annual budget towards higher education? (Without applying any financial aid) ",
    "code": "EDUINVESTMENT",
    "tips": 'The budget that you can afford towards higher education without applying for any scholarship or financial aid.',
    "isRequired": true,
    "options": [
      {
        "text": "$0-$5,000",
        "value": "$0-$5,000"
      },
      {
        "text": "$5,000-$10,000",
        "value": "$5,000-$10,000"
      },
      {
        "text": "$10,000-$20,000",
        "value": "$10,000-$20,000"
      },
      {
        "text": "$20,000-$30,000",
        "value": "$20,000-$30,000"
      },
      {
        "text": "$30,000-$40,000",
        "value": "$30,000-$40,000"
      },
      {
        "text": "$40,000-$50,000",
        "value": "$40,000-$50,000"
      },
      {
        "text": "$50,000-$60,000",
        "value": "$50,000-$60,000"
      },
      {
        "text": "$60,000-$70,000",
        "value": "$60,000-$70,000"
      },
      {
        "text": "$70,000-$80,000",
        "value": "$70,000-$80,000"
      },
      {
        "text": "$80,000-$90,000",
        "value": "$80,000-$90,000"
      },
      {
        "text": "$90,000-$1,00,000",
        "value": "$90,000-$1,00,000"
      },
      {
        "text": "$1,00,000 above",
        "value": "$1,00,000 above"
      },
    ],
    "placeholder": "",
    "type": "radio",
    "stream": "engineering"
  },
  {
    "section": "financialInfo",
    "text": "Do you need any kind of financial assistance/aid? (Choose 1 or more)",
    "code": "FINANASSIST",
    "tips": <p><b>●Federal work study</b> - The FWS Program provides funds for part-time employment to help needy students to finance the costs of postsecondary education.<br />
      <b>●Scholarship (includes both, need based/merit based)</b> - A scholarship is an award of financial aid for a student to further their education at a private elementary or secondary school, or a private or public post-secondary college, university, or other academic institution.<br />
      <b>●Federal Student Aid</b> - Federal Student Aid (FSA), an office of the U.S. Department of Education, is the largest provider of student financial aid in the United States. Federal Student Aid provides student financial assistance in the form of grants, loans, and work-study funds.<br />
      <b>●Bursaries</b> - A bursary is a monetary award made by any educational institution or funding authority to individuals or groups. It is usually awarded to enable a student to attend school, university or college when they might not be able to, otherwise. Some awards are aimed at encouraging specific groups or individuals into study.<br />
      <b>●Grants</b> - A Pell Grant is a subsidy the U.S. federal government provides for students who need it to pay for college. Federal Pell Grants are limited to students with financial need, who have not earned their first bachelor's degree, or who are enrolled in certain post-baccalaureate programs, through participating institutions.<br />
      <b>●Subsidized and unsubsidized loans</b> - Subsidized and unsubsidized loans are federal student loans for eligible students to help cover the cost of higher education at a four-year college or university, community college, or trade, career, or technical school. The U.S. Department of Education offers eligible students at participating schools Direct Subsidized Loans and Direct Unsubsidized Loans.</p>,
    "isRequired": true,
    "isLogical": true,
    "options": [
      {
        "text": "Federal work study",
        "value": "Federal work study"
      },
      {
        "text": "Scholarships",
        "value": "Scholarships"
      },
      {
        "text": "Federal student aid",
        "value": "Federal student aid"
      },
      {
        "text": "Burasaries",
        "value": "Burasaries"
      },
      {
        "text": "Grants (Pell / Federal / state)",
        "value": "Grants (Pell / Federal / state)"
      },
      {
        "text": "Aid for international study",
        "value": "Aid for international study"
      },
      {
        "text": "Subsidized student loans",
        "value": "Subsidized student loans"
      },
      {
        "text": "Aid for military families",
        "value": "Aid for military families"
      },
      {
        "text": "Unsubsidized student loans",
        "value": "Unsubsidized student loans"
      },
      {
        "text": "Other ",
        "value": "Other ",
        "isLogical": true,
        "questions": [
          {
            "isRequired": true,
            "text": "Other",
            "type": "text",
            "code": "FINANASSISTOTHER",
          }
        ]
      },
      {
        "text": "Not applicable",
        "value": "Not applicable"
      }
    ],
    "placeholder": "",
    "isLogical": true,
    "type": "checkbox",
    "stream": "engineering"
  },
  // {
  //   "section": "financialPreference",
  //   "text": "What is your estimated cost of education or net price (min. $10000) ",
  //   "code": "ESTEDUCOST",
  //   "options": [],
  //   "placeholder": "",
  //   "isRequired": true,    
  //   "type": "number",
  //   "stream": "engineering",
  //   "minValue": "10000",
  // }
]

export const FINANCIALPREFRENCE = [
  {
    "section": "financialPreference",
    "text": "How important are the following factors to you in college selection?",
    "code": "COLLSELCFACTOR",
    "questions": [
      {
        "type": "radio",
        "text": "Demographics (ethnicity, gender)",
        "code": "DEMOETHIGEN",
        "isRequired": true,
      },
      {
        "text": "Academics (college ranking, facilities, dorm)",
        "type": "radio",
        "code": "ACADEMICOLRANKFACI",
        "isRequired": true,
      },
      {
        "text": "Location (setting, proximity)",
        "type": "radio",
        "code": "LOCATIONSETTPROX",
        "isRequired": true,
      },
      {
        "text": "Climate (weather, allergies)",
        "type": "radio",
        "code": "CLIMATEWEATHERALLERGIE",
        "isRequired": true,
      },
      {
        "text": "Placements (internship, campus placement, work-study)",
        "type": "radio",
        "code": "PLACEMENTINTERNSHIP",
        "isRequired": true,
      },
      {
        "text": "Financials (tuition fee, aid)",
        "type": "radio",
        "code": "FINANCIALSTUTION",
        "isRequired": true,
      }
    ],
    "placeholder": "",
    "type": "categorical",
    "stream": "engineering"
  },
  {
    "section": "financialPreference",
    "text": "Would you like to add extra information that is critical to your Institution search",
    "code": "EXTRAIMPSEARCH",
    "options": [],
    "placeholder": "",
    "isRequired": true,
    "type": "text",
    "stream": "engineering"
  },
  {
    "section": "placement",
    "text": "How did you discover HiEd Harmony? (Choose 1 or more)",
    "code": "DISCOVERHIED",
    "isLogical": true,
    "isRequired": true,
    "options": [
      {
        "text": "Facebook",
        "value": "Facebook"
      },
      {
        "text": "LinkedIn",
        "value": "LinkedIn"
      },
      {
        "text": "Instagram",
        "value": "Instagram"
      },
      {
        "text": "Internet Ad.",
        "value": "Internet Ad."
      },
      {
        "text": "School",
        "value": "School"
      },
      {
        "text": "College",
        "value": "College"
      },
      {
        "text": "Newspaper/Brochure",
        "value": "Newspaper/Brochure"
      },
      {
        "text": "HiEd Student Success Reps",
        "value": "HiEd Student Success Reps",
        "isLogical": true,
        "questions": [
          {
            "section": "placement",
            "text": "Who were you referred by?",
            "code": "DISCOVERHIEDREFERREDBY",
            "placeholder": "",
            "type": "dropDown",
            "stream": "engineering",
            "isLogical": true,
            "isRequired": true,
            "options": [
              {
                "text": "Preeti Tanwar",
                "value": "Preeti Tanwar"
              },
              {
                "text": "Sukanya Bhowmik",
                "value": "Sukanya Bhowmik"
              },
              {
                "text": "Kiran Yerawar",
                "value": "Kiran Yerawar"
              },
              {
                "text": "Salman Shaikh",
                "value": "Salman Shaikh"
              },
              {
                "text": "Shwan Muhammed",
                "value": "Shwan Muhammed"
              },
              {
                "text": "Destiny Ihenacho",
                "value": "Destiny Ihenacho"
              },
              {
                "text": "Kiza Mauridi",
                "value": "Kiza Mauridi"
              },
              {
                "text": "Archit Singh",
                "value": "Archit Singh"
              },
              {
                "text": "Vinod Badoni",
                "value": "Vinod Badoni"
              },
              {
                "text": "Ajay Sankhla",
                "value": "Ajay Sankhla"
              },
              {
                "text": "Sreedhar Kodali",
                "value": "Sreedhar Kodali"
              },
              {
                "text": "Fermata",
                "value": "Fermata"
              }
            ],
          },
        ]

      },
      {
        "text": "Recommended by friend/colleague",
        "value": "Recommended by friend/colleague"
      },
      {
        "text": "Others (please specify)",
        "value": "Others (please specify)",
        "isLogical": true,
        "questions": [
          {
            "isRequired": true,
            "text": "Other",
            "type": "text",
            "code": "OTHERDISCOVERHIED",
          }
        ]
      },
    ],
    "placeholder": "",
    "type": "checkbox",
    "stream": "engineering"
  },
]