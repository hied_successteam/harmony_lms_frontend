import React, { useState, useEffect } from 'react';
import { Col, Row, Image, Button, Form, Nav, Tab, Accordion, Card, Table } from 'react-bootstrap';
import { CardElement, CardNumberElement, CardExpiryElement, CardCvcElement, useElements, useStripe }
    from '@stripe/react-stripe-js';
import Loader from "react-js-loader";
import IntlTelInput from 'react-intl-tel-input';
import rocketUp from "../../../../../assets/images/svg/rocket-up.svg";
import validateInput from "./CheckoutFormValidation";

import { purchasePlan } from '../../../../../Actions/Student/register';


const CheckoutFormService = (props) => {

    const [state, setState] = useState({});
    const [errors, setErrors] = useState({});

    const [isLoading, setIsLoading] = useState(false);

    const stripe = useStripe();
    const elements = useElements();

    const onchangeInput = event => {
        const { target: { name, value } } = event;
        setState({ ...state, [name]: value });
    };

    const handleSubmitSubform = async () => {

        props.setIsLoading(true);
        props.setLoaderManual(true);

        let errorsNew = {};
        let valid = true;
        

        let data = {
            nameOnCard: state.nameOnCard ? state.nameOnCard : '',
            emailOnCard: state.emailOnCard ? state.emailOnCard : '',
            contactNumber: state.contactNumber ? state.contactNumber : '',
        };


        const { errors, isValid } = validateInput(data);
        if (!isValid) {
            setErrors({ ...errors });
            valid = false;
            props.setLoaderManual(false);
            props.setIsLoading(false);
        }

        if (props.valid == false) {
            setErrors({ ...errors });
            valid = false;
            props.setLoaderManual(false);
            props.setIsLoading(false);
        }


        if (!stripe || !elements) {
            // Stripe.js has not loaded yet. Make sure to disable
            // form submission until Stripe.js has loaded.
            return;
        }

        
            // Get a reference to a mounted CardElement. Elements knows how
            // to find your CardElement because there can only ever be one of
            // each type of element.
            const Cardc = elements.getElement(CardElement);
            // Use your card Element with other Stripe.js APIs
            const { error, paymentMethod } = await stripe.createPaymentMethod({
                type: 'card',
                card: Cardc,
                billing_details: {
                    email: state.emailOnCard,
                    phone: state.contactNumber,
                    name: state.nameOnCard
                }
            });


            if (error) {
                if (errors.emailOnCard) {
                    if (error.message != 'Your email address is invalid.')
                        errorsNew['cardNumber'] = error.message;
                }
                else {
                    errorsNew['cardNumber'] = error.message;
                }
                setErrors({ ...errorsNew, ...errors });
                valid = false;
            }
        


        if (valid) {
            setErrors({});
            if (paymentMethod && paymentMethod.id) {                
                props.submitData(paymentMethod.id);
            } else {
                props.setLoaderManual(false);
                props.setIsLoading(false);

            }
        } else {
            props.setLoaderManual(false);
            props.setIsLoading(false);

        }
    }


    const formatPhoneNumberOutput = (a, b, c) => {
        let num = /^[0-9\b]+$/;
        if (b == '' || num.test(b)) {
            if (b.length < 11) {
                setState({
                    ...state,
                    contactNumber: b,
                    contactNumberCountryCode: c.dialCode,
                    contactNumberiso2: c.iso2,
                });
            }
        }
    }

    const formatPhoneNumberOutputFlag = (b, c) => {
        let num = /^[0-9\b]+$/;
        if (b == '' || num.test(b)) {
            if (b.length < 11) {
                setState({
                    ...state,
                    contactNumber: b,
                    contactNumberCountryCode: c.dialCode,
                    contactNumberiso2: c.iso2,
                });
            }
        }
    }


    useEffect(() => {
        if (props.subFormClick) {
            handleSubmitSubform();
        }
    }, [props.subFormClick]);

    useEffect(() => {
        if (props.cardNumberError) {
            let errorsNew = {};
            errorsNew['cardNumber'] = props.cardNumberError;
            setErrors({ ...errorsNew });
        }
    }, [props.cardNumberError]);

    

    return (
        <>

            <Row>
                <Col md={6}>
                    <Form.Group className="mb-3" controlId="nameOnCard">
                        <Form.Label className="col2">
                            Name on card
                        </Form.Label>
                        <Form.Control
                            type="text"
                            name="nameOnCard"
                            value={state && state['nameOnCard'] || ''}
                            placeholder="Name on card"
                            className="inputType1"
                            onChange={onchangeInput}
                            autoComplete="off"
                        />
                        {
                            errors && errors['nameOnCard'] &&
                            <span className="help-block error-text">
                                <span style={{ color: "red", fontSize: 13 }}>
                                    {errors['nameOnCard']}
                                </span>
                            </span>
                        }
                    </Form.Group>
                </Col>
                <Col md={6}>
                    <Form.Group className="mb-3" controlId="emailOnCard">
                        <Form.Label className="col2">
                            Email
                        </Form.Label>
                        <Form.Control
                            type="email"
                            name="emailOnCard"
                            value={state && state['emailOnCard'] || ''}
                            placeholder="Email Address"
                            className="inputType1"
                            onChange={onchangeInput}
                            autoComplete="off"
                        />
                        {
                            errors && errors['emailOnCard'] &&
                            <span className="help-block error-text">
                                <span style={{ color: "red", fontSize: 13 }}>
                                    {errors['emailOnCard']}
                                </span>
                            </span>
                        }
                    </Form.Group>
                </Col>

                <Col md={6}>
                    <Form.Group
                        controlId={`formBasicPhoneNumber`}
                        className="mb-3 flagSelect">
                        <Form.Label className="col2">
                            Phone Number
                        </Form.Label>
                        <IntlTelInput
                            //ref={(elt) => props.textInput ? props.textInput = elt : elt = elt}
                            containerClassName="intl-tel-input"
                            inputClassName="form-control inputType1"
                            placeholder="Contact Number"
                            onPhoneNumberChange={(...args) => {
                                formatPhoneNumberOutput(...args);
                            }}
                            onSelectFlag={(...args) => {
                                formatPhoneNumberOutputFlag(...args);
                            }}
                            name='contactNumber'
                            value={state && state['contactNumber'] || ''}
                            formatOnInit={true}
                        />
                        {
                            errors && errors['contactNumber'] &&
                            <span className="help-block error-text">
                                <span style={{ color: "red", fontSize: 13 }}>
                                    {errors['contactNumber']}
                                </span>
                            </span>
                        }
                    </Form.Group>
                </Col>

                <Col md={6}>
                    <Form.Group className="mb-3" controlId="cardNumber">
                        <Form.Label className="col2">
                            Card Number
                        </Form.Label>
                        <CardElement
                            options={{
                                hidePostalCode: true,
                                autoComplete: "off",
                                style: {
                                    base: {
                                        fontSize: '16px',
                                        borderColor: 'red !important',
                                        color: '#424770',
                                        '::placeholder': {
                                            color: '#7E7E7E',
                                        },
                                    },
                                    invalid: {
                                        color: '#9e2146',
                                    },
                                },
                            }}
                        />
                        {
                            errors && errors['cardNumber'] &&
                            <span className="help-block error-text">
                                <span style={{ color: "red", fontSize: 13 }}>
                                    {errors['cardNumber']}
                                </span>
                            </span>
                        }
                    </Form.Group>
                </Col>
                {/* <Col md={12}>
                    <div className="text-center d-flex justify-content-center align-items-center flex-wrap align-xs-center">
                        <Button
                            className="btnType4 fw600 mr-4"
                            onClick={() => handleSubmitSubform()}
                            disabled={!stripe || isLoading}
                        >Sign In</Button>
                    </div>
                </Col> */}
            </Row>


        </>
    );
};

export default CheckoutFormService;
