import Validator from "validator";
import isEmpty from "lodash/isEmpty";

export default function validateInput(data) {
    let errors = {};
    let alphaOnly = "^[a-zA-Z .']*$";    

    if (Validator.isEmpty(data.nameOnCard)) {
        errors.nameOnCard = "Name is required.";     

    } else if (!Validator.matches(data.nameOnCard, alphaOnly)) {
        errors.nameOnCard = "Name can only contain alphabets.";       
    }

    if (Validator.isEmpty(data.emailOnCard)) {
        errors.emailOnCard = "Email is required.";
    } else {
        if (!Validator.isEmail(data.emailOnCard)) {
            errors.emailOnCard = "Email is invalid.";
        } else if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(data.emailOnCard)) {
        } else {
            errors.emailOnCard = "Email is invalid.";
        }
    }

    if (Validator.isEmpty(data.contactNumber.toString())) {
        errors.contactNumber = "Please enter contact number.";
    } else
    {        
        if (data.contactNumber.length < 10) {
                    errors.contactNumber = "Contact number must be 10 digit.";
         }
    }    

    // if (Validator.isEmpty(data.mobile)) {
    //     errors.mobile = "Contact number is required.";
    // } else {
    //     if (data.mobile.length < 10) {
    //         errors.mobile = "Contact number must be 10 digit.";
    //     }
    // }     
    
    return {
        errors,
        isValid: isEmpty(errors)
    };
}
