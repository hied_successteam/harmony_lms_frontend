import React, { useState, useEffect } from 'react';
import { Col, Row, Image, Button, Form, Nav, Tab, Accordion, Card, Table } from 'react-bootstrap';
import { Slider, RangeSlider } from 'rsuite';
import 'rsuite/dist/styles/rsuite-default.css';
//import FormInput from './FormInput'
import { FINANCIALINFO, FINANCIALPREFRENCE } from '../WizardQuestion'
import { getLocalStorage, setLocalStorage } from '../../../../../Lib/Utils';
import Loader from "react-js-loader";

import rocketDown from "../../../../../assets/images/svg/rocket-down.svg";
import rocketUp from "../../../../../assets/images/svg/rocket-up.svg";
import Sends from "../../../../../assets/images/svg/sends.svg";
import Speed from "../../../../../assets/images/svg/speed.svg";
import DownIcon from "../../../../../assets/images/svg/downIcon.svg";

import { Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import { CardElement, CardNumberElement, CardExpiryElement, CardCvcElement, useElements, useStripe }
  from '@stripe/react-stripe-js';

import CheckoutForm from "./CheckoutForm";
import CONSTANTS from '../../../../../Lib/Constants';
//const stripePromise = loadStripe(CONSTANTS.STRIPE_PUBLISH_KEY);
const stripePromise = loadStripe('pk_test_BX1dy9UdoNqfxgcCc4wtaYzE00UXgBkmf2')

const EightStep = (props) => {
  const [state, setState] = useState({});
  const [errors, setErrors] = useState({});
  const [totalDefth, setTotalDefth] = useState([1, 2, 3, 4, 5, 6, 7]);
  const [minLenMsg, setMinLenMsg] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [FeatureShow, setFeatureShow] = useState(true);
  const [ShowCardDetail, setShowCardDetail] = useState(false);
  const [subPlanData, setSubPlanData] = useState({});
  const [subformValue, setsubformValue] = useState('');
  const [selectedPlanData, setSelectedPlanData] = useState({});




  useEffect(() => {
    setShowCardDetail(false)
    setErrors({})
  }, [])


  const onSelectPlan = (id, type, data) => {
    setState({ ...state, ['planId']: id, ['planType']: type });
    setSubPlanData({ ...data });
  }

  useEffect(() => {
    if (props.data && Object.keys(props.data).length != 0) {
      setShowCardDetail(false)
      setState({
        ...state,
        ...props.data
      });
    }
  }, [props.data]);

  useEffect(() => {
    if (props.selectedPlanData && Object.keys(props.selectedPlanData).length != 0) {
      setSelectedPlanData(props.selectedPlanData);
      setState({
        ...state,
        ['planId']: props.selectedPlanData.id,
        ['planType']: props.selectedPlanData.type
      });
    }
  }, [props.selectedPlanData]);




  const stepSubmit = () => {

    setIsLoading(true);
    props.setLoaderManual(true);
    let errors = {};
    let valid = true;

    if (state.planId) {
      valid = true
    } else {
      valid = false
      errors['planId'] = "Please select plan.";
    }

    if (valid) {
      props.stepSubmitEight(state, 8);
      setTimeout(function () {
        setIsLoading(false)
      }.bind(this), 4000);
      setErrors({});
    }
    else {
      props.setLoaderManual(false);
      setIsLoading(false)
      setErrors({ ...errors });
    }

    var data = getLocalStorage("user")
    data.plan = { type: 'free' }
    setLocalStorage('user', data)
  }

  const stepSubmitPaid = () => {
    var data = getLocalStorage("user")
    data.plan = { type: 'paid' }
    setLocalStorage('user', data)
    setShowCardDetail(true);
  }



  return (
    <>
      {!ShowCardDetail ?
        <div className="rightBox pt-5 pb-5 pl-4">
          <Col lg={10} md={12}>
            <div className="rightLayout">
              <div className="d-flex flex-wrap justify-content-between mb-1">
                <div className="stepTitle fw500 fs30 col4 position-relative mb-4 pb-3">Select Plan </div>
                {
                  getLocalStorage('user') && getLocalStorage('user').isProfileCompleted &&
                  <a className="col1 fw500 fs18 pointer" onClick={() => props.stepSelect('final')}>Go To Dashboard</a>
                }
              </div>
              <Row>
                {props.planData && props.planData.map((data, index) =>
                  <React.Fragment key={index}>
                    <Col md={3}>
                      <div
                        className={`PlanList1 bgCol44 p-3 ${selectedPlanData && selectedPlanData.price > data.price ? 'disabledOne' : ''} ${state.planId == data._id ? 'active' : ''}`}
                        onClick={() =>
                          selectedPlanData && selectedPlanData.price > data.price ? '' :
                            onSelectPlan(data._id, data.type, data)
                        }
                      >
                        <div className="text-center mb-3">
                          <div className="circleType3">
                            <Image src={data.icon ? data.icon : Sends} alt="Icon" className="mw30" />
                          </div>
                        </div>
                        <div className="col47 fs30 fw500 mb-1 elippse1 activeColor1">{data.name}</div>
                        <div className="mb-3 col5 fw500 fs14 elippse2">{data.description}</div>
                        <div className="col30 fw600">
                          {selectedPlanData.id == data._id ? 'Current Plan'
                            :
                            data.price !== 0 ?
                              selectedPlanData && selectedPlanData.id && selectedPlanData.price < data.price ?
                                `$${data.price - selectedPlanData.price}` : `$${data.price}`
                              : 'Trial'}
                        </div>
                      </div>
                    </Col>
                  </React.Fragment>
                )}

                {
                  errors && errors['planId'] &&
                  <Col md={12}>
                    <div className="col2 fw600 d-flex fs18 mb-2 pointer">

                      <span className="help-block planError error-text">
                        <span style={{ color: "red", fontSize: 13 }}>
                          {errors['planId']}
                        </span>
                      </span>

                    </div>
                  </Col>
                }
              </Row>
              <Row>
                <Col md={12}>
                  <div
                    className="col2 fw600 d-flex fs18 mb-2 pointer"
                    onClick={() => setFeatureShow(!FeatureShow)}
                  >
                    Plan Selection Table
                    <span className={`openCard ${!FeatureShow ? 'active' : ''}`}>
                      <Image src={DownIcon} alt="Icon" className="ml-2" />
                    </span>
                  </div>

                  {FeatureShow ?
                    <Table bordered className="tableType2 planTable planUpdates br10 mb-4">
                      <thead>

                        <tr>
                          <th></th>
                          {props.planData && props.planData.map((data, index) =>
                            <th key={index}>
                              <span className="d-block mb-2 fs18">{data.price ? data.name : ''}</span>
                              <span className="priceOrder">
                                {selectedPlanData.id == data._id ? 'Current Plan'
                                  :
                                  data.price !== 0 ?
                                    selectedPlanData && selectedPlanData.id && selectedPlanData.price < data.price ?
                                      `$${data.price - selectedPlanData.price}` : `$${data.price}`
                                    : data.name}                                
                              </span></th>
                          )}
                        </tr>

                      </thead>
                      <tbody>
                        {props.featureData && props.featureData.map((data, index) =>
                          <tr key={index}>
                            <td>{data.name}</td>
                            <td>
                              <Form.Group controlId={`formBasicRadio1${index}`}>
                                {
                                  data.plan && data.plan.indexOf('free') > -1 &&
                                  <Form.Check
                                    type="radio"
                                    checked={data.plan && data.plan.indexOf('free') > -1 ? true : false}
                                    className="checkboxTyp4"
                                    onChange={() => { }}
                                    label="" />
                                }
                              </Form.Group>
                            </td>
                            <td>
                              <Form.Group controlId={`formBasicRadio2${index}`}>
                                {
                                  data.plan && data.plan.indexOf('basic') > -1 &&
                                  <Form.Check
                                    type="radio"
                                    checked={data.plan && data.plan.indexOf('basic') > -1 ? true : false}
                                    className="checkboxTyp4"
                                    onChange={() => { }}
                                    label="" />
                                }
                              </Form.Group>
                            </td>
                            <td>
                              <Form.Group controlId={`formBasicRadio3${index}`}>
                                {
                                  data.plan && data.plan.indexOf('standard') > -1 &&
                                  <Form.Check
                                    type="radio"
                                    checked={data.plan && data.plan.indexOf('standard') > -1 ? true : false}
                                    className="checkboxTyp4"
                                    onChange={() => { }}
                                    label="" />
                                }
                              </Form.Group>
                            </td>
                            <td>
                              <Form.Group controlId={`formBasicRadio4${index}`}>
                                {
                                  data.plan && data.plan.indexOf('premium') > -1 &&
                                  <Form.Check
                                    type="radio"
                                    checked={data.plan && data.plan.indexOf('premium') > -1 ? true : false}
                                    className="checkboxTyp4"
                                    onChange={() => { }}
                                    label="" />
                                }
                              </Form.Group>
                            </td>
                          </tr>
                        )}

                      </tbody>
                    </Table>
                    : ''}
                </Col>
              </Row>
            </div>
            <div className="footerBotton">
              <div className="hrBorder pt-3"></div>
              <div className="text-right d-flex flex-wrap justify-content-between  mt-3">
                <Button type="button" className="btnType7"
                  onClick={() => props.stepSelect('seven')}>
                  Previous
                </Button>
                {selectedPlanData.id == state.planId ? '' :
                  <Button
                    type="button"
                    className="btnType2"
                    onClick={() =>
                      selectedPlanData.id == state.planId ? '' :
                        state && state.planType == 'paid' ?
                          stepSubmitPaid()
                          :
                          stepSubmit()
                    }
                  >

                    {
                      isLoading ?
                        <Loader type="bubble-top" bgColor={"#414180"} size={25} />
                        :
                        "Pay Now"
                    }
                  </Button>
                }
              </div>
            </div>
          </Col>
        </div>
        : ''}

      {ShowCardDetail ?
        <Elements stripe={stripePromise}>
          <CheckoutForm
            subformValue={subformValue}
            subPlanData={subPlanData}
            selectedPlanData={selectedPlanData}
            setShowCardDetail={setShowCardDetail}
            ShowCardDetail={ShowCardDetail}
            isLoading={isLoading}
            planType={state.planType}
            planId={state.planId}
            stepSubmitEightAfterPayment={props.stepSubmitEightAfterPayment}
            setLoaderManual={props.setLoaderManual}
          />
        </Elements>
        : ''}
    </>
  );


}

export default EightStep;