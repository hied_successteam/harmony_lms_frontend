import React, { useState, useEffect } from 'react';
import { Col, Row, Image, Button, Form, Nav, Tab, Accordion, Card, Table } from 'react-bootstrap';
import { CardElement, CardNumberElement, CardExpiryElement, CardCvcElement, useElements, useStripe }
    from '@stripe/react-stripe-js';
import Loader from "react-js-loader";
import IntlTelInput from 'react-intl-tel-input';
import rocketUp from "../../../../../assets/images/svg/rocket-up.svg";
import validateInput from "./CheckoutFormValidation";

import { purchasePlan } from '../../../../../Actions/Student/register';


const CheckoutForm = (props) => {

    const [state, setState] = useState({});
    const [errors, setErrors] = useState({});

    const [isLoading, setIsLoading] = useState(false);

    const stripe = useStripe();
    const elements = useElements();

    const onchangeInput = event => {
        const { target: { name, value } } = event;
        setState({ ...state, [name]: value });
    };

    useEffect(() => {
        setState({ ...state, ['planType']: props.planType, ['planId']: props.planId  });
      }, [])    

    const handleSubmitSubform = async () => {

        setIsLoading(true);
        props.setLoaderManual(true);

        let errorsNew = {};
        let valid = true;

        let data = {
            nameOnCard: state.nameOnCard ? state.nameOnCard : '',
            emailOnCard: state.emailOnCard ? state.emailOnCard : '',
            contactNumber: state.contactNumber ? state.contactNumber : '',
        };

        const { errors, isValid } = validateInput(data);
        if (!isValid) {
            setErrors({ ...errors });
            valid = false;
            props.setLoaderManual(false);
            setIsLoading(false);
        }


        if (!stripe || !elements) {
            // Stripe.js has not loaded yet. Make sure to disable
            // form submission until Stripe.js has loaded.
            return;
        }

        // Get a reference to a mounted CardElement. Elements knows how
        // to find your CardElement because there can only ever be one of
        // each type of element.
        const Cardc = elements.getElement(CardElement);
        // Use your card Element with other Stripe.js APIs
        const { error, paymentMethod } = await stripe.createPaymentMethod({
            type: 'card',
            card: Cardc,
            billing_details: {
                email: state.emailOnCard,
                phone: state.contactNumber,
                name: state.nameOnCard
            }
        });

        if (error) {
            if (errors.emailOnCard) {
                if (error.message != 'Your email address is invalid.')
                    errorsNew['cardNumber'] = error.message;
            }
            else {
                errorsNew['cardNumber'] = error.message;
            }
            setErrors({ ...errorsNew, ...errors });
            valid = false;
        }


        if (valid) {
            setErrors({});
            if (paymentMethod && paymentMethod.id) {
                let data = {
                    "planType": props.planType,
                    "planId": props.planId,
                    "token": paymentMethod.id,
                }
                purchasePlan(data).then(({ data: { success, data, message } }) => {
                    if (success !== true) {
                        props.setLoaderManual(false);
                        setIsLoading(false);
                        errorsNew['cardNumber'] = message;
                        setErrors({ ...errorsNew });
                        //setMessage(message);          
                        //props.stepSubmitEightAfterPayment(state, 8);                                                          
                        return;
                    } else {                        
                        props.stepSubmitEightAfterPayment(state, 8);
                    }
                })
                    .catch((error) => {                       
                        if (error && error.response.data.message) {
                            errorsNew['cardNumber'] = error.response.data.message;
                            setErrors({ ...errorsNew });
                        }
                        props.setLoaderManual(false);
                        setIsLoading(false);
                        return;
                    });
            } else {
                props.setLoaderManual(false);
                setIsLoading(false);

            }
        } else {
            props.setLoaderManual(false);
            setIsLoading(false);

        }
    }


    const formatPhoneNumberOutput = (a, b, c) => {
        let num = /^[0-9\b]+$/;
        if (b == '' || num.test(b)) {
            if (b.length < 11) {
                setState({
                    ...state,
                    contactNumber: b,
                    contactNumberCountryCode: c.dialCode,
                    contactNumberiso2: c.iso2,
                });
            }
        }
    }

    const formatPhoneNumberOutputFlag = (b, c) => {
        let num = /^[0-9\b]+$/;
        if (b == '' || num.test(b)) {
            if (b.length < 11) {
                setState({
                    ...state,
                    contactNumber: b,
                    contactNumberCountryCode: c.dialCode,
                    contactNumberiso2: c.iso2,
                });
            }
        }
    }

    return (
        <>

            <div className="rightBox pt-5 pb-5 pl-4">
                <Col lg={10} md={12}>
                    <div className="rightLayout">
                        <div className="stepTitle fw500 fs30 col4 position-relative mb-4 pb-3">Payment</div>
                        <Row>
                            <Col md={12}>
                                <div className="fw600 fs22 col2 mb-3">Enter Card Details</div>
                            </Col>
                            <Col md={6} className="pr20">
                                <div className="planBox1">
                                    <Row>
                                        <Col md={12}>
                                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                                <Form.Label className="col2">
                                                Name on card
                                                </Form.Label>
                                                <Form.Control
                                                    type="text"
                                                    name="nameOnCard"
                                                    value={state && state['nameOnCard'] || ''}
                                                    placeholder="Name on card"
                                                    className="inputType1"
                                                    onChange={onchangeInput}
                                                />
                                                {
                                                    errors && errors['nameOnCard'] &&
                                                    <span className="help-block error-text">
                                                        <span style={{ color: "red", fontSize: 13 }}>
                                                            {errors['nameOnCard']}
                                                        </span>
                                                    </span>
                                                }
                                            </Form.Group>
                                        </Col>


                                        <Col md={12}>
                                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                                <Form.Label className="col2">
                                                    Email
                                                </Form.Label>
                                                <Form.Control
                                                    type="email"
                                                    name="emailOnCard"
                                                    value={state && state['emailOnCard'] || ''}
                                                    placeholder="Email Address"
                                                    className="inputType1"
                                                    onChange={onchangeInput}
                                                />
                                                {
                                                    errors && errors['emailOnCard'] &&
                                                    <span className="help-block error-text">
                                                        <span style={{ color: "red", fontSize: 13 }}>
                                                            {errors['emailOnCard']}
                                                        </span>
                                                    </span>
                                                }
                                            </Form.Group>
                                        </Col>

                                        <Col md={12}>
                                            <Form.Group
                                                controlId={`formBasicPhoneNumber`}
                                                className="mb-3 flagSelect">
                                                <Form.Label className="col2">
                                                    Phone Number
                                                </Form.Label>
                                                <IntlTelInput
                                                    //ref={(elt) => props.textInput ? props.textInput = elt : elt = elt}
                                                    containerClassName="intl-tel-input"
                                                    inputClassName="form-control inputType1"
                                                    placeholder="Contact Number"
                                                    onPhoneNumberChange={(...args) => {
                                                        formatPhoneNumberOutput(...args);
                                                    }}
                                                    onSelectFlag={(...args) => {
                                                        formatPhoneNumberOutputFlag(...args);
                                                    }}
                                                    name='contactNumber'
                                                    value={state && state['contactNumber'] || ''}
                                                    formatOnInit={true}
                                                />
                                                {
                                                    errors && errors['contactNumber'] &&
                                                    <span className="help-block error-text">
                                                        <span style={{ color: "red", fontSize: 13 }}>
                                                            {errors['contactNumber']}
                                                        </span>
                                                    </span>
                                                }
                                            </Form.Group>
                                        </Col>

                                        <Col md={12}>
                                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                                <Form.Label className="col2">
                                                    Card Number
                                                </Form.Label>
                                                <CardElement
                                                    options={{
                                                        hidePostalCode: true,
                                                        style: {
                                                            base: {
                                                                fontSize: '16px',
                                                                borderColor: 'red !important',
                                                                color: '#424770',
                                                                '::placeholder': {
                                                                    color: '#7E7E7E',
                                                                },
                                                            },
                                                            invalid: {
                                                                color: '#9e2146',
                                                            },
                                                        },
                                                    }}
                                                />
                                                {
                                                    errors && errors['cardNumber'] &&
                                                    <span className="help-block error-text">
                                                        <span style={{ color: "red", fontSize: 13 }}>
                                                            {errors['cardNumber']}
                                                        </span>
                                                    </span>
                                                }
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                </div>
                            </Col>
                            <Col md={6} className="pl20">
                                <div className="planBox2 bgCol44">
                                    <div className="d-flex mb-4">
                                        <div className="circleType3">
                                            <Image src={props.subPlanData.icon ? props.subPlanData.icon : rocketUp} alt="Icon" className="mw30" />
                                        </div>
                                        <div>
                                            <div className="fs30 fw600 col2 mb-0">{props.subPlanData.name}</div>
                                            <div className="fs14 fw600 col2">{props.subPlanData.description}
                                            </div>
                                        </div>
                                    </div>
                                    <ul className="planList mb-4">
                                        {props.subPlanData && props.subPlanData.feature && props.subPlanData.feature.map((data, index) =>
                                            <li key={index}>{data}</li>
                                        )}
                                    </ul>
                                    <div className="changePlan bgCol3 r5 d-flex justify-content-between">
                                        <div className="col30 fw600 fs18">
                                            Pay ${props.selectedPlanData && props.selectedPlanData.id ? 
                                            props.subPlanData.price - props.selectedPlanData.price :
                                            props.subPlanData.price
                                            }
                                            </div>
                                        <div
                                            className="fw600 fs18 col8 pointer"
                                            onClick={() => props.setShowCardDetail(!props.ShowCardDetail)}
                                        >Change Plan</div>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </div>
                    <div className="footerBotton">
                        <div className="hrBorder pt-3"></div>
                        <div className="text-right d-flex flex-wrap justify-content-between  mt-3">

                            <Button type="button" className="btnType7"
                                onClick={() => props.setShowCardDetail(!props.ShowCardDetail)}
                                disabled={isLoading}
                            >
                                Previous
                            </Button>

                            <Button
                                type="button"
                                className="btnType2"
                                onClick={() => handleSubmitSubform()}
                                disabled={!stripe || isLoading}
                            >
                                {
                                    isLoading ?
                                        <Loader type="bubble-top" bgColor={"#414180"} size={25} />
                                        :
                                        `Pay $${props.selectedPlanData && props.selectedPlanData.id ? 
                                            props.subPlanData.price - props.selectedPlanData.price :
                                            props.subPlanData.price}`
                                }
                            </Button>

                        </div>
                    </div>
                </Col>
            </div>
        </>
    );
};

export default CheckoutForm;
