import React, { useState, useEffect } from 'react';
import { Col, Row, Button, Image, Form } from 'react-bootstrap';
import 'rsuite/dist/styles/rsuite-default.css';
import FormInput from './FormInput'
import { CollegePreference } from '../WizardQuestion'
import { getLocalStorage } from '../../../../../Lib/Utils';
import infoicon from "../../../../../assets/images/svg/questions.svg";
import TipModal from '../../../TipModal/TipModal'
import AsyncSelect from 'react-select/async';
import CONSTANTS from '../../../../../Lib/Constants';
import axios from 'axios';
import { countryList } from '../../../../../Actions/Student/register'



const ThirdStep = (props) => {

  const [state, setState] = useState({});
  const [errors, setErrors] = useState({});
  const [tipsData, setTipsData] = useState(false);
  const [show, setShow] = useState(false);
  const [inputValue, setValue] = useState('');
  const [selectedValue, setSelectedValue] = useState('');
  const [openOptions, setOpenOptions] = useState(null);
  const [countryDatas, setCountryDatas] = useState([]);  


  const handleClose = () => setShow(false);

  const handleShow = (tipsData) => {
    setTipsData(tipsData);
    setShow(true);
  }

  useEffect(() => {
    setErrors({})
  }, [props])

  useEffect(() => {
    countryList().then(res => { 
      setCountryDatas(res.data.data);
    }).catch(err => { });

  }, []);

  const onchangeInput = event => {
    const { target: { name, value } } = event;
    setState({ ...state, [name]: value });
  };

  const onKeyDownInput = event => {
    if (event && event.target && event.target.name) {
      const { target: { name, value } } = event;

      if (event.keyCode == 13) {
        setState({ ...state, [name]: '' });

        var arr = name.split('_');
        let Newname = arr[0];

        if (state[Newname]) {
          let ArryVale = [...state[Newname]];
          var indexB = ArryVale.indexOf(value);
          if (indexB > -1) {
            ArryVale.splice(indexB, 1);
            setState({ ...state, [name]: '', [Newname]: [...ArryVale] });
          } else {
            ArryVale.push(value);
            setState({ ...state, [name]: '', [Newname]: [...ArryVale] });
          }
        } else {
          setState({ ...state, [name]: '', [Newname]: [value] });
        }
      }
    } else {
      //setState({ ...state, DOB: event });
    }
  };

  //select check box
  const oncheckBox = (name, value) => {
    if (state[name]) {
      let ArryVale = state[name];
      var indexB = ArryVale.indexOf(value);
      if (indexB > -1) {
        ArryVale.splice(indexB, 1);
        if (ArryVale && ArryVale.length <= 0) {
          delete state[name]
          setState({ ...state });
        } else {
          setState({ ...state, [name]: [...ArryVale] });
        }
      } else {
        ArryVale.push(value);
        setState({ ...state, [name]: [...ArryVale] });
      }
    } else {
      setState({ ...state, [name]: [value] });
    }
  }


  const onDeleteArray = (name, value) => {
    if (state[name]) {
      let ArryVale = state[name];
      ArryVale.splice(value, 1);
      setState({ ...state, [name]: [...ArryVale] });
    } else {
      setState({ ...state, [name]: [value] });
    }
  }


  useEffect(() => {
    if (props.data && Object.keys(props.data).length != 0) {
      setState({
        ...state,
        ...props.data
      });
      
      if(props.data['TARGETCOLLEGES']){
        setSelectedValue(props.data['TARGETCOLLEGES'])
      }
    }else{
      if(props.data && props.data.BRIDGE_PROGRAM){}else{
        setState({ ...state, BRIDGE_PROGRAM: 'Not Required' });
      }  
    }
  }, [props.data]);



  const stepSubmit = () => {
    let errors = {};
    let valid = true;
    for (var m = 0; m < CollegePreference.length; m++) {

      if (CollegePreference[m].isRequired) {
        let codevalue = state[CollegePreference[m].code];
        if (!codevalue || codevalue == 'undefined' || codevalue == '') {
          valid = false;
          errors[CollegePreference[m].code] = true;
        }

        let queArray = CollegePreference[m].options
        queArray && queArray.map((item, i) =>{ 
          if(item.questions && item.questions.length && state && state[CollegePreference[m].code] && state[CollegePreference[m].code].indexOf("Other") > -1){
            var NestedArr = item.questions
            for (var i = 0; i < NestedArr.length; i++) {
              if(NestedArr[i].code == "MULTICULTURALCOUNTRY" && state[CollegePreference[m].code].indexOf("International (country, multicultural)") > -1){
                if (NestedArr[i].isRequired) { 
                  let codevalueNew = state[NestedArr[i].code];
                  if (!codevalueNew || codevalueNew == 'undefined' || codevalueNew == '') {
                    valid = false;
                    errors[NestedArr[i].code] = true;
                  }
                }
              }else
              if (NestedArr[i].isRequired && NestedArr[i].code != "MULTICULTURALCOUNTRY") { 
                let codevalueNew = state[NestedArr[i].code];
                if (!codevalueNew || codevalueNew == 'undefined' || codevalueNew == '') {
                  valid = false;
                  errors[NestedArr[i].code] = true;
                }
              }
            }
          }
        })
      }else{
        let queArrays = CollegePreference[m].options
        queArrays && queArrays.map((item, i) =>{ 
          if(item.questions && item.questions.length && state && state[CollegePreference[m].code] && state[CollegePreference[m].code].indexOf("Other") > -1){
            var NestedArr = item.questions
            for (var i = 0; i < NestedArr.length; i++) {
              if (NestedArr[i].isRequired) {
                let codevalueNew = state[NestedArr[i].code];
                if (!codevalueNew || codevalueNew == 'undefined' || codevalueNew == '') {
                  valid = false;
                  errors[NestedArr[i].code] = true;
                }
              }
            }
          }
        })
      }
    }

    if (valid) {
      props.stepSubmitThird(state, 3);
      setErrors({});
    }
    else {
      setErrors({ ...errors });
    }
  }


  // handle input change event
  const handleInputChange = value => {
    setValue(value);
  };

  // handle selection
  const handleChange = value => {
    setSelectedValue(value);
    setState({ ...state, TARGETCOLLEGES: value });
  }


  // load options using API call
  const loadOptions = (inputValue) => {
    if (inputValue && inputValue.length >= 3) {
      //const searchUrlNew = `http://192.168.1.10:5000/api/institutions/${inputValue}`;
      //const searchUrl = `${CONSTANTS.CLIENT_SERVER_URL}institutions/${inputValue}`;
      const searchUrl = `http://192.168.1.10:5000/api/institutions/${inputValue}`;
      return axios.get(searchUrl).then((res) => {
        setOpenOptions(res.data.data.institutions)
        return res.data.data.institutions;
      });

    } else {
      return [];
    }
  };



  return (


    <div className="rightBox pt-5 pb-5 pl-4">

      <Col lg={10} md={12}>
        <div className="rightLayout">
          <div className="d-flex flex-wrap justify-content-between mb-1">
            <div className="stepTitle fw500 fs30 col4 position-relative mb-4 pb-1">College Preferences </div>
            {
              getLocalStorage('user') && getLocalStorage('user').isProfileCompleted &&
              <div className="col2 fw500 fs18 pointer" onClick={() => props.stepSelect('fourth')}>Skip</div>
            }
          </div>
          <Row className="stepForm7">

            {
              CollegePreference && CollegePreference.map((data, index) =>
                data.section == 'collegePreference' &&

                <div key={index}>
                  <Col md={12} key={index + '3423'}>
                    <div className="col4 fw600 fs18 mb-2">
                      <span className="numericType1">{index + 1}.</span>
                      {data.text}
                      {data.isRequired ? <sup className="starQ">*</sup> : ''}
                      {data.tips ? <Image onClick={() => handleShow(data.tips)} src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                    </div>
                  </Col>
                  <Col key={index + '2342'} lg={data.type == 'dropDown' ? '10' : '12'} md={'12'}>
                    {
                      data.code == "TARGETCOLLEGES" ?

                      <Form.Group controlId="exampleForm.ControlSelect1">
                        <AsyncSelect
                          noOptionsMessage={() => null}
                          isMulti
                          cacheOptions
                          defaultOptions={openOptions}
                          value={selectedValue}
                          getOptionLabel={e => e.INSTNM}
                          getOptionValue={e => e.UNITID}
                          loadOptions={inputValue && inputValue.length >= 3 ? loadOptions : ''}
                          onInputChange={handleInputChange}
                          onChange={handleChange}
                          //closeMenuOnSelect={true}
                          placeholder="College Name"
                          className="basic-multi-select selectTyp2"
                          classNamePrefix="select"
                          isSearchable={true}
                        //isLoading = {false}    
                        />
                        {
                          errors.instituteId &&
                          <span className="help-block error-text">
                            <span style={{ color: "red", fontSize: 13 }}>{errors.instituteId}</span>
                          </span>
                        }
                      </Form.Group>

                      :
                      <FormInput
                        inputData={data}
                        listOptions={data.options}
                        onchangeInput={onchangeInput}
                        onKeyDownInput={onKeyDownInput}
                        oncheckBox={oncheckBox}
                        onDeleteArray={onDeleteArray}
                        setData={state}
                        error={errors[data.code] ? true : false}
                        errorMessage={'This field is required.'}
                      />
                    }
                  </Col>

                  {

                    data.options &&
                    data.options.map((option, index_1) => (
                      <div key={index_1}>
                        {
                          option.questions && option.questions.map((question, index_2) => (
                            state && state[data.code] && state[data.code].indexOf(question.text) > -1 && question.text == "Other"  ?
                            <div key={index_2}>
                              <Col md={12} key={index_2 + 'i'}>
                                <div className="col4 fw600  fs18 mb-2">
                                  <span className="numericType1">{index + 1}.
                                    {(state[data.code].indexOf("Other") > -1 && state[data.code].indexOf("International (country, multicultural)") > -1 && question.code == "COLLEGECLUBSOTHER" ) ? index_2 + 2 : index_2 + 1}
                                  </span>
                                  {question.text}
                                  {question.isRequired ? <sup className="starQ">*</sup> : ''}
                                </div>
                              </Col>
                              <Col key={index_2 + 'j'} lg={'12'} md={'12'}>
                                <FormInput
                                  inputData={question}
                                  listOptions={question}
                                  onchangeInput={onchangeInput}
                                  onKeyDownInput={onKeyDownInput}
                                  oncheckBox={oncheckBox}
                                  setData={state}
                                  error={errors[question.code] ? true : false}
                                  errorNew={errors}
                                  errorNested={errors}
                                  errorMessage={'This field is required.'}
                                  OuterIndex={index_2}
                                />
                              </Col>
                            </div>
                            :
                            state && state[data.code] && state[data.code].indexOf("International (country, multicultural)") > -1 && question.text == "Which country club are you looking to join?" &&
                            <div key={index_2}>
                              <Col md={12} key={index_2 + 'i'}>
                                <div className="col4 fw600  fs18 mb-2">
                                  <span className="numericType1">{index + 1}.
                                    {index_2 + 1}
                                  </span>
                                  {question.text}
                                  {question.isRequired ? <sup className="starQ">*</sup> : ''}
                                </div>
                              </Col>
                              <Col key={index_2 + 'j'} lg={'12'} md={'12'}>
                                <FormInput
                                  inputData={question}
                                  listOptions={countryDatas}
                                  onchangeInput={onchangeInput}
                                  onKeyDownInput={onKeyDownInput}
                                  oncheckBox={oncheckBox}
                                  setData={state}
                                  error={errors[question.code] ? true : false}
                                  errorNew={errors}
                                  errorNested={errors}
                                  errorMessage={'This field is required.'}
                                  OuterIndex={index_2}
                                />
                              </Col>
                            </div>
                          ))
                        }
                      </div>
                    ))
                  }

                </div>

              )
            }

          </Row>
        </div>
        <div className="footerBotton">
          <div className="hrBorder pt-3"></div>
          <div className="text-right d-flex flex-wrap justify-content-between  mt-3">
            <Button type="button" className="btnType7"
              onClick={() => props.stepSelect('second')}>
              Previous
            </Button>
            <Button type="button" className="btnType2"
              onClick={() => stepSubmit()}>
              Save & Next
            </Button>
          </div>
        </div>

      </Col>

      <TipModal tipsData={tipsData} title={"Info"} show={show} handleClose={handleClose}></TipModal>

    </div>





  )
}

export default ThirdStep;




