import React from 'react';
import { Image, Form, } from 'react-bootstrap';
import 'rsuite/dist/styles/rsuite-default.css';
import "react-datepicker/dist/react-datepicker.css";
import Select from 'react-select';
import crossTwos from '../../../../../assets/images/crossTwo.png';

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'chocolate2', label: 'Chocolate2' },
  { value: 'strawberry2', label: 'Strawberry2' },
  { value: 'vanilla2', label: 'Vanilla2' },
  { value: 'chocolat3', label: 'Chocolate3' },
  { value: 'strawberr3', label: 'Strawberry3' },
  { value: 'vanilla3', label: 'Vanilla3' }
]

const FormInput = (props) => {


  switch (props.inputData.type) {

    case 'text':
      return (
        <Form.Group key={props.inputData.code}
          className={`${props.inputData.NotEditable && props.isProfileCompleted ? 'contentDisabledNew' : ''}`}
        >
          <Form.Control
            type="text"
            name={props.inputData.code == 'TARGETCOLLEGES' ? props.inputData.code + '_VV' : props.inputData.code}
            value={(props.inputData.code == 'TARGETCOLLEGES' ? props.setData[props.inputData.code + '_VV'] : props.setData[props.inputData.code]) || ''}
            placeholder={props.inputData.placeholder}
            className="inputType1"
            onChange={props.onchangeInput}
            onKeyDown={props.onKeyDownInput}
          />

          {props.inputData && props.inputData.code == 'TARGETCOLLEGES' ?
            <div className="tagType1 d-flex flex-wrap">
              {
                props.setData && props.inputData && props.setData[props.inputData.code] &&
                Array.isArray(props.setData[props.inputData.code]) &&
                props.setData[props.inputData.code].map((item, index) => {
                  return <span key={index} className="br8 col4 fw500 mr-3 selectData1">{item}
                    <Image onClick={() => props.onDeleteArray(props.inputData.code, index)} src={crossTwos} className="Cross Icon" /></span>
                })
              }
            </div>
            : ''}

          {
            props.error &&
            <span className="help-block error-text">
              <span style={{ color: "red", fontSize: 13 }}>
                {props.errorMessage}
              </span>
            </span>
          }
        </Form.Group>
      )


    case 'dropDown':
      return (
        <>
          <Form.Group controlId={`formBasicName${props.inputData.code}`}
            key={`formBasicName${props.inputData.code}`}
            className={`${props.inputData.NotEditable && props.isProfileCompleted ? 'contentDisabledNew' : ''}`}
          >
            <Form.Control as="select"
              className="selectTyp1 pointer"
              name={props.inputData.code}
              value={props.setData[props.inputData.code] || ''}
              onChange={props.onchangeInput}
            >
              {/* <option value="">{props.inputData.text}</option> */}
              <option value="">Select</option>
              {
                props && props.listOptions &&
                props.listOptions.map((item, index) => {
                  return <option key={index} value={item.name ? item.name : item.value}>{item.name ? item.name : item.value}</option>
                })
              }
            </Form.Control>
            {
              props.error &&
              <span className="help-block error-text">
                <span style={{ color: "red", fontSize: 13 }}>
                  {props.errorMessage}
                </span>
              </span>
            }
          </Form.Group>

        </>
      )

    case 'multi_dropDown':
      return (
        <>
          <Form.Group controlId="exampleForm.ControlSelect1">
            <Select
              defaultValue={[options[2], options[3]]}
              isMulti
              name="colors"
              options={options}
              className="basic-multi-select selectTyp2"
              classNamePrefix="select"
              placeholder="Country"
            />
          </Form.Group>

        </>
      )



    case 'radio':
      return (
        <Form.Group controlId={`formBasicName${props.inputData.code}`}
          key={`formBasicName${props.inputData.code}`}
          className={`radioOne ${props.inputData.NotEditable && props.isProfileCompleted ? 'contentDisabledNew' : ''}`}
        >
          {
            props.inputData.options.map((item, index) => {
              return <Form.Check
                key={index}
                type="radio"
                label={item.text}
                className={props.setData[props.inputData.code] == item.value ? "radioType1 actives " : "radioType1"}
                name={props.inputData.code}
                value={item.value || false}
                checked={props.setData[props.inputData.code] == item.text}
                onChange={props.onchangeInput}
              />
            })
          }
          {
            props.error &&
            <span className="help-block error-text">
              <span style={{ color: "red", fontSize: 13 }}>
                {props.errorMessage}
              </span>
            </span>
          }
        </Form.Group>
      )

    case 'checkbox':
      return (
        <div className="tagType1 d-flex flex-wrap mb-3">
          {
            props.inputData.options.map((item, index) => {
              return <span
                key={index}
                className={`br8 col4 fw500 mr-3 ${props.setData[props.inputData.code] && props.setData[props.inputData.code].indexOf(item.value) > -1 ? 'bgCol35' : ''}  `}
                name={props.inputData.code}
                onClick={() => props.oncheckBox(props.inputData.code, item.value)}
                value={item.value || false}>
                {item.value}
              </span>
            })
          }
          {
            props.error &&
            <span className="help-block error-text">
              <span style={{ color: "red", fontSize: 13 }}>
                {props.errorMessage}
              </span>
            </span>
          }
        </div>
      )

    default:
      return '';
  }

}



export default FormInput;




