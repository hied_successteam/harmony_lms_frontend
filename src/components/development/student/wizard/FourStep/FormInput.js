import React, { useState, useEffect } from 'react';
import { Form} from 'react-bootstrap';
import 'rsuite/dist/styles/rsuite-default.css';
import "react-datepicker/dist/react-datepicker.css";



const FormInput = (props) => {

    switch (props.inputData.type) {

        case 'text':
            return (
                <Form.Group className="priceBox" controlId={`formBasicName${props.inputData.code}`}
                className={`${props.inputData.NotEditable && props.isProfileCompleted ? 'contentDisabledNew' : ''}`}
                >
                    <Form.Control
                        type="text"
                        name={props.inputData.code == 'VOLUNACTIVITY' ? props.inputData.code + '_VV' : props.inputData.code}
                        value={(props.inputData.code == 'VOLUNACTIVITY' ? props.setData[props.inputData.code + '_VV'] : props.setData[props.inputData.code]) || ''}
                        placeholder={props.inputData.placeholder}
                        className={`inputType1 ${props.inputData.code == 'VOLUNACTIVITY' ? 'mb-3' : ''}`}
                        onChange={props.onchangeInput}
                        onKeyDown={props.onKeyDownInput}
                    />
                    {props.inputData.code == 'VOLUNACTIVITY' ?
                        <div className="tagType1 d-flex flex-wrap">
                            {
                                props.setData[props.inputData.code] &&
                                props.setData[props.inputData.code].map((item, index) => {
                                    return <span key={index} className="br8 col4 fw500 mr-3">{item}</span>
                                })
                            }
                        </div>
                        : ''}
                    {
                        props.error &&
                        <span className="help-block error-text">
                            <span style={{ color: "red", fontSize: 13 }}>
                                {props.errorMessage}
                            </span>
                        </span>
                    }
                </Form.Group>
            )

        case 'dropDown':
            return (
                <Form.Group controlId={`exampleForm.ControlSelect1${props.inputData.code}`} 
                key={props.inputData.code + 'aaa'}
                className={`${props.inputData.NotEditable && props.isProfileCompleted ? 'contentDisabledNew' : ''}`}
                >
                    <Form.Control as="select"
                        className="selectTyp1 pointer"
                        name={props.inputData.code}
                        value={props.setData[props.inputData.code] || ''}
                        onChange={props.onchangeInput}
                        disabled={props.inputData.code == "CLOSERCOLLEGE" && props.firstStepCountry != "United States" ? true : false} 
                        
                    >
                        {/* <option value="">{props.inputData.text}</option> */}   
                        {props.SelectFirstOption &&
                            <option value="">Select</option>
                        } 
                        {
                            props && props.listOptions &&
                            props.listOptions.map((item, index) => {
                                return <option key={index} title={item.name ? item.name : item.text} value={item.name ? item.name : item.value}>{item.name ? item.name : item.text}</option>
                            })
                        }
                    </Form.Control>
                    {
                        props.error &&
                        <span className="help-block error-text">
                            <span style={{ color: "red", fontSize: 13 }}>
                                {props.errorMessage}
                            </span>
                        </span>
                    }
                </Form.Group>
            )

        case 'checkbox':
            return (
                <div className="tagType1 d-flex flex-wrap mb-3" key={props.inputData.code + 'aaa'}>
                    {
                        props.inputData.options.map((item, index) => {
                            return <span
                                key={index + 'bbb'}
                                className={`br8 col4 fw500 mr-3 ${props.setData[props.inputData.code] && props.setData[props.inputData.code].indexOf(item.value) > -1 ? 'bgCol35' : ''}  `}
                                name={props.inputData.code}
                                onClick={() => props.oncheckBox(props.inputData.code, item.value)}
                                value={item.value}>{item.value || ''}</span>
                        })
                    }
                    {
                        props.error &&
                        <span className="help-block error-text">
                            <span style={{ color: "red", fontSize: 13 }}>
                                {props.errorMessage}
                            </span>
                        </span>
                    }
                </div>
            )


        case 'radio':
            return (
                <Form.Group controlId={`formBasicName${props.inputData.code}`} key={`formBasicName${props.inputData.code}`}
                className={`${props.inputData.NotEditable && props.isProfileCompleted ? 'contentDisabledNew' : ''}`}
                >
                    {
                        props.inputData.options.map((item, index) => {
                            return <Form.Check type="radio"
                                key={index}
                                label={item.text}
                                className={props.setData[props.inputData.code] == item.value ? "radioType1 actives" : "radioType1"}
                                id={props.inputData.code} name={props.inputData.code} value={item.value || ''}
                                checked={props.setData[props.inputData.code] == item.text}
                                onChange={props.onchangeInput} />
                        })
                    }
                    {
                        props.error &&
                        <span className="help-block error-text">
                            <span style={{ color: "red", fontSize: 13 }}>
                                {props.errorMessage}
                            </span>
                        </span>
                    }
                </Form.Group>
            )
        default:
            return '';
    }


}

export default FormInput;




