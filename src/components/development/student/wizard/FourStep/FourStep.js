import React, { useState, useEffect } from 'react';
import { Col, Row, Button, Image} from 'react-bootstrap';
import 'rsuite/dist/styles/rsuite-default.css';
import FormInput from './FormInput'
import { DemoGraphicPreference, ClimatePreference, LocationPreference } from '../WizardQuestion'
import { getLocalStorage } from '../../../../../Lib/Utils';
import infoicon from "../../../../../assets/images/svg/questions.svg";
import TipModal from '../../../TipModal/TipModal'


const FourStep = (props) => {

  const [state, setState] = useState({});
  const [errors, setErrors] = useState({}); 
  const [tipsData, setTipsData] = useState(false);
  const [show, setShow] = useState(false);
  const [firstStep, setFirstStep] = useState('');

  const handleClose = () => setShow(false);

  const handleShow = (tipsData) => {
    setTipsData(tipsData);
    setShow(true);
  }

  useEffect(() =>{
    setErrors({})
  },[props])

  const onchangeInput = event => {
    if (event && event.target && event.target.name) {
      const { target: { name, value } } = event;
      setState({ ...state, [name]: value });
    } else {
      //setState({ ...state, DOB: event });
    }
  };

  const oncheckBox = (name, value) => {
    if (state[name]) {
      let ArryVale = state[name];
      var indexB = ArryVale.indexOf(value);
      if (indexB > -1) {
        ArryVale.splice(indexB, 1);
        if(ArryVale && ArryVale.length <= 0){
          delete state[name]
          setState({ ...state});
        }else{
          setState({ ...state, [name]: [...ArryVale] });
        }
      } else {
        ArryVale.push(value);
        setState({ ...state, [name]: [...ArryVale] });
      }
    } else {
      setState({ ...state, [name]: [value] });
    }

    if (name == "PREFLOCATION" && state && state["PREFLOCATION"] && state["PREFLOCATION"].indexOf("No preference") > -1) {
      setState({ ...state, PREFLOCATION: ["No preference"] });
    }
  }

  useEffect(() => { 
    if (props.data && Object.keys(props.data).length != 0) {
      setState({
        ...state,
        ...props.data
      });

      if(!props.data.COLLEGESPECIALISEDMISSION)
      {
        setState({ ...state, ['COLLEGESPECIALISEDMISSION']: 'No preference' });
      }    
    }else
    {
      setState({ ...state, ['COLLEGESPECIALISEDMISSION']: 'No preference' });
    }
  }, [props.data]);


   

  useEffect(() =>{ 
    if(getLocalStorage('country')){
      setFirstStep(getLocalStorage('country'))
      if(getLocalStorage('country') == "United States"){
        setState({ ...state, CLOSERCOLLEGE: "No preference" });
      }else{
        setState({ ...state, CLOSERCOLLEGE: "Not applicable" });
      }
    }
  },[getLocalStorage('country')])




  const stepSubmit = () => {

    let errors = {};
    let valid = true;

    for (var m = 0; m < DemoGraphicPreference.length; m++) {

      if (DemoGraphicPreference[m].isRequired) {
        let codevalue = state[DemoGraphicPreference[m].code];
        if (!codevalue || codevalue == 'undefined' || codevalue == '') {
          valid = false;
          errors[DemoGraphicPreference[m].code] = true;
        }
      }

      if (DemoGraphicPreference[m].isLogical) {

        let Newarry = DemoGraphicPreference[m].options;

        for (var n = 0; n < Newarry.length; n++) {

          if (Newarry[n].isLogical) {

            if (Newarry[n].value == state['ETHINICITY']) {
              let newdata = Newarry[n].questions;
              for (var i = 0; i < newdata.length; i++) {
                if (newdata[i].isRequired) {
                  let codevalueNew = state[newdata[i].code];
                  if (!codevalueNew || codevalueNew == 'undefined' || codevalueNew == '') {
                    valid = false;
                    errors[newdata[i].code] = true;
                  }

                }
              }
            }

          }
        }
      }


    }

    for (var m = 0; m < ClimatePreference.length; m++) {

      let codevalue = state[ClimatePreference[m].code];
      if (!codevalue || codevalue == 'undefined' || codevalue == '') {
        valid = false;
        errors[ClimatePreference[m].code] = true;
      }

      if (ClimatePreference[m].isLogical) {

        let Newarry = ClimatePreference[m].options;

        for (var n = 0; n < Newarry.length; n++) {

          if (Newarry[n].isLogical) {

            if (Newarry[n].value == state['ALLERGY']) {
              let newdata = Newarry[n].questions;
              for (var i = 0; i < newdata.length; i++) {
                if (newdata[i].isRequired) {
                  let codevalueNew = state[newdata[i].code];
                  if (!codevalueNew || codevalueNew == 'undefined' || codevalueNew == '') {
                    valid = false;
                    errors[newdata[i].code] = true;
                  }

                }
              }
            }

          }
        }
      }



    }

    for (var m = 0; m < LocationPreference.length; m++) {

      if (LocationPreference[m].isRequired) {
        let codevalue = state[LocationPreference[m].code];
        if (!codevalue || codevalue == 'undefined' || codevalue == '') {
          valid = false;
          errors[LocationPreference[m].code] = true;
        }
      }
    }

    if (valid) {
      props.stepSubmitFour(state, 4);
      setErrors({});
    }
    else {
      setErrors({ ...errors });
    }

  }

  return (
    <div className="rightBox pt-5 pb-5 pl-4"> 
      <Col lg={10} md={12}>
        <div className="rightLayout">
          <div className="d-flex flex-wrap justify-content-between mb-1">
            <div className="stepTitle fw500 fs30 col4 position-relative mb-4 pb-1">Demographic Preferences </div>
            {
              getLocalStorage('user') && getLocalStorage('user').isProfileCompleted &&
              <div className="col2 fw500 fs18 pointer" onClick={() => props.stepSelect('fivth')}>Skip</div>
            }
          </div>
          <Row className="stepForm7">
            {
              DemoGraphicPreference && DemoGraphicPreference.map((data, index) =>
                <div key={index}>
                  {data.type == 'dropDown' ?
                    <Col md={12} key={index + 'aaa'}>
                      <div className="col4 fw600 fs18 mb-2">
                        <span className="numericType1">{index + 1}.</span>
                        {data.text}
                        {data.isRequired ? <sup className="starQ">*</sup> : ''}
                        {data.tips ? <Image onClick={() => handleShow(data.tips)} src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                      </div>
                    </Col>
                    : ''}

                  <Col md={(data.type == 'radio' || data.type == 'checkbox') ? '12' : (data.type == 'dropDown' || data.type == 'text') ? '10' : '6'} key={index + 'ccc'}>
                    {(data.type == 'radio' || data.type == 'checkbox') ?
                      <div className="col4 fw600 fs18 mb-2">
                        <span className="numericType1">{index + 1}.</span>
                        {data.text}
                        {data.isRequired ? <sup className="starQ">*</sup> : ''}
                        {data.tips ? <Image onClick={() => handleShow(data.tips)} src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                      </div>
                      : ''}
                    <FormInput
                      key={index + 'ddd'}
                      inputData={data}
                      listOptions={data.options}
                      onchangeInput={onchangeInput}
                      oncheckBox={oncheckBox}
                      setData={state}
                      error={errors[data.code] ? true : false}
                      errorMessage={'This field is required.'}
                      SelectFirstOption={data.code == 'COLLEGESPECIALISEDMISSION' ? false : true}                      
                    />
                  </Col>


                  {
                    //code for logical questions
                    data.isLogical == true ?
                      data.options && data.options.map((option, indexNew) =>
                      (
                        option.isLogical == true ?
                          <div key={indexNew}>
                            {state[data.code] && state[data.code] == option.text ?
                              option.questions && option.questions.map((question, indexSecond) =>
                                <div key={indexSecond}>
                                  {question.type == 'dropDown' ?
                                    <Col md={12} key={indexSecond + 'qqqq'}>
                                      <div className="col4 fw600 fs18 mb-2">
                                        <span className="numericType1">{index + 1}.{indexSecond + 1}</span>
                                        {' '}{question.text}
                                        {question.isRequired ? <sup className="starQ">*</sup> : ''}
                                        {question.tips ? <Image onClick={() => handleShow(question.tips)} src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                                      </div>
                                    </Col>
                                    : ''}

                                  <Col md={(question.type !== 'dropDown') ? '6' : '6'} key={indexSecond + 'ddfdf'}>
                                    {(question.type !== 'dropDown') ?
                                      <div className="col4 fw600 fs18 mb-2">
                                        <span className="numericType1">{index + 1}.{indexSecond + 1}</span>
                                        {' '}{question.text}
                                        {question.isRequired ? <sup className="starQ">*</sup> : ''}
                                        {question.tips ? <Image onClick={() => handleShow(question.tips)} src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                                      </div>
                                      : ''}
                                    <FormInput
                                      key={indexSecond + 'adfsf'}
                                      inputData={question}
                                      listOptions={question.options}
                                      onchangeInput={onchangeInput}
                                      oncheckBox={oncheckBox}
                                      setData={state}
                                      error={errors[question.code] ? true : false}
                                      errorMessage={'This field is required.'}
                                    />

                                  </Col>
                                </div>
                              )
                              : ''}
                          </div>
                          : ''

                      ))
                      : ''
                  }

                </div>
              )
            }

            <Col md={12}>
              {/* <div className="hrBorder mb-4 mt-1"></div> */}
              <div className="fs20 fw600 col2 mb-3">Climate Preferences</div>
            </Col>



            {
              ClimatePreference && ClimatePreference.map((data, index) =>
                <div key={index}>
                  {data.type == 'dropDown' ?
                    <Col md={12} key={index + 'aaaad'}>
                      <div className="col4 fw600 fs18 mb-2">
                        <span className="numericType1">{index + 1 + (DemoGraphicPreference && DemoGraphicPreference.length)}.</span>
                        {data.text}
                        {data.isRequired ? <sup className="starQ">*</sup> : ''}
                        {data.tips ? <Image onClick={() => handleShow(data.tips)} src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                      </div>
                    </Col>
                    : ''}

                  <Col md={(data.type == 'radio' || data.type == 'checkbox') ? '12' : (data.type == 'dropDown' || data.type == 'text') ? '10' : '6'} key={index + 'bbb'}>
                    {(data.type == 'radio' || data.type == 'checkbox') ?
                      <div className="col4 fw600 fs18 mb-2">
                        <span className="numericType1">{index + 1 + (DemoGraphicPreference && DemoGraphicPreference.length)}.</span>
                        {data.text}
                        {data.isRequired ? <sup className="starQ">*</sup> : ''}
                        {data.tips ? <Image onClick={() => handleShow(data.tips)} src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                      </div>
                      : ''}
                    <FormInput
                      key={index + 'eee'}
                      inputData={data}
                      listOptions={data.options}
                      onchangeInput={onchangeInput}
                      oncheckBox={oncheckBox}
                      setData={state}
                      error={errors[data.code] ? true : false}
                      errorMessage={'This field is required.'}
                    />
                  </Col>

                  {
                    //code for logical questions
                    data.isLogical == true ?
                      data.options && data.options.map((option, indexNew) =>
                      (
                        option.isLogical == true ?
                          <div key={indexNew}>
                            {state[data.code] && state[data.code] == option.text ?
                              option.questions && option.questions.map((question, indexSecond) =>
                                <div key={indexSecond}>
                                  {question.type == 'dropDown' ?
                                    <Col md={12} key={indexSecond + 'qqqq'}>
                                      <div className="col4 fw600 fs18 mb-2">
                                        <span className="numericType1">{index + 1 + (DemoGraphicPreference && DemoGraphicPreference.length)}.{indexSecond + 1}</span>
                                        {' '}{question.text}
                                        {question.isRequired ? <sup className="starQ">*</sup> : ''}

                                        {question.tips ? <Image onClick={() => handleShow(question.tips)} src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                                      </div>
                                    </Col>
                                    : ''}

                                  <Col md={(question.type !== 'dropDown') ? '10' : '6'} key={indexSecond + 'ddfdf'}>
                                    {(question.type !== 'dropDown') ?
                                      <div className="col4 fw600 fs18 mb-2">
                                        <span className="numericType1">{index + 1 + (DemoGraphicPreference && DemoGraphicPreference.length)}.{indexSecond + 1}</span>
                                        {' '}{question.text}
                                        {question.isRequired ? <sup className="starQ">*</sup> : ''}
                                        {question.tips ? <Image onClick={() => handleShow(question.tips)} src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                                      </div>
                                      : ''}
                                    <FormInput
                                      key={indexSecond + 'adfsf'}
                                      inputData={question}
                                      listOptions={question.options}
                                      onchangeInput={onchangeInput}
                                      oncheckBox={oncheckBox}
                                      setData={state}
                                      error={errors[question.code] ? true : false}
                                      errorMessage={'This field is required.'}
                                    />

                                  </Col>
                                </div>
                              )
                              : ''}
                          </div>
                          : ''

                      ))
                      : ''
                  }

                </div>

              )
            }

            <Col md={12}>
              {/* <div className="hrBorder mb-4 mt-1"></div> */}
              <div className="fs20 fw600 col2 mb-3">Location Preferences</div>
            </Col>

            {
              LocationPreference && LocationPreference.map((data, index) =>
                <div key={index}>
                  {data.type == 'dropDown' ?
                    <Col md={12} key={index + 'azs'}>
                      <div className="col4 fw600 fs18 mb-2">
                        <span className="numericType1">{index + 1 + (ClimatePreference && ClimatePreference.length) + (DemoGraphicPreference && DemoGraphicPreference.length)}.</span>
                        {data.text}
                        {data.isRequired ? <sup className="starQ">*</sup> : ''}
                        {data.tips ? <Image onClick={() => handleShow(data.tips)} src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                      </div>
                    </Col>
                    : ''}

                  <Col md={(data.type == 'radio' || data.type == 'checkbox') ? '12' : (data.type == 'dropDown' || data.type == 'text') ? '10' : '6'} key={index + 'cvdf'}>
                    {(data.type == 'radio' || data.type == 'checkbox') ?
                      <div className="col4 fw600 fs18 mb-2">
                        <span className="numericType1">{index + 1 + (ClimatePreference && ClimatePreference.length) + (DemoGraphicPreference && DemoGraphicPreference.length)}.</span>
                        {data.text}
                        {data.isRequired ? <sup className="starQ">*</sup> : ''}
                        {data.tips ? <Image onClick={() => handleShow(data.tips)} src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                      </div>
                      : ''}
                    <FormInput
                      key={index + 'ase'}
                      inputData={data}
                      listOptions={data.options}
                      onchangeInput={onchangeInput}
                      oncheckBox={oncheckBox}
                      firstStepCountry={firstStep}
                      setData={state}
                      error={errors[data.code] ? true : false}
                      errorMessage={'This field is required.'}
                    />
                  </Col>
                </div>
              )
            }

          </Row>

        </div>
        <div className="footerBotton">
          <div className="hrBorder pt-3"></div>
          <div className="text-right d-flex flex-wrap justify-content-between  mt-3">
            <Button type="button" className="btnType7"
              onClick={() => props.stepSelect('third')}>
              Previous
            </Button>
            <Button
              type="button"
              onClick={() => stepSubmit()}
              className="btnType2">
              Save & Next
            </Button>
          </div>
        </div>
      </Col>



      <TipModal tipsData= {tipsData} title={"Info"} show= {show} handleClose= {handleClose}></TipModal>

    </div>
  );


}

export default FourStep;