import React, { useState, useEffect } from 'react';
import { Col, Row, Image, Button, Form, Nav, Tab, Accordion, Card } from 'react-bootstrap';
import { Slider, RangeSlider } from 'rsuite';
import 'rsuite/dist/styles/rsuite-default.css';
import Select from 'react-select';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import IntlTelInput from 'react-intl-tel-input';

const options = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
    { value: 'chocolate2', label: 'Chocolate2' },
    { value: 'strawberry2', label: 'Strawberry2' },
    { value: 'vanilla2', label: 'Vanilla2' },
    { value: 'chocolat3', label: 'Chocolate3' },
    { value: 'strawberr3', label: 'Strawberry3' },
    { value: 'vanilla3', label: 'Vanilla3' }
]


const FormInput = (props) => {

    switch (props.inputData.type) {
        case 'text':
            return (
                props.inputData.code == 'WHATSAPPNO' ?
                    <Form.Group controlId={`formBasicName${props.inputData.code}`} key={`formBasicName${props.inputData.code}`} 
                    className={`mb-4 flagSelect ${props.inputData.NotEditable && props.isProfileCompleted ? 'contentDisabledNew' : ''}` }  
                    >
                        <IntlTelInput
                            //ref={(elt) => props.textInput ? props.textInput = elt : elt = elt}
                            containerClassName="intl-tel-input"
                            inputClassName="form-control inputType1"
                            placeholder={props.inputData.placeholder}
                            onPhoneNumberChange={(...args) => {
                                props.formatPhoneNumberOutput(...args);
                            }}
                            onSelectFlag={(...args) => {
                                props.formatPhoneNumberOutputFlag(...args);
                            }}                            
                            name={props.inputData.code}
                            value={props.setData[props.inputData.code] || ''}
                            formatOnInit={true}                            
                        />
                         {
                            props.error &&
                            <span className="help-block error-text">
                                <span style={{ color: "red", fontSize: 13 }}>
                                    {props.errorMessage}
                                </span>
                            </span>
                        }
                    </Form.Group>
                    :
                    <Form.Group controlId={`formBasicName${props.inputData.code}`} key={`formBasicName${props.inputData.code}`}
                    className={`${props.inputData.NotEditable && props.isProfileCompleted ? 'contentDisabledNew' : ''}`}
                    > 
                        <Form.Control
                            type="text"
                            name={props.inputData.code}
                            value={props.setData[props.inputData.code] || ''}
                            placeholder={props.inputData.placeholder}
                            className="inputType1"
                            onChange={(e) => (props.inputData.code == "POBCITY") ? (e.target.value.match(/^[ ,a-zA-Z]+$/) != null || e.target.value == '') ? props.onchangeInput(e) : '' : props.onchangeInput(e)}
                            disabled={props.inputData.disabled ? props.inputData.disabled : false}
                            autoComplete="none"                           
                        />
                        {
                            props.error &&
                            <span className="help-block error-text">
                                <span style={{ color: "red", fontSize: 13 }}>
                                    {props.errorMessage}
                                </span>
                            </span>
                        }
                    </Form.Group>
            )
            case 'number':
                return ( 
                        <Form.Group controlId={`formBasicName${props.inputData.code}`} key={`formBasicName${props.inputData.code}`}
                        className={`${props.inputData.NotEditable && props.isProfileCompleted ? 'contentDisabledNew' : ''}`}
                        >
                            <Form.Control
                                type="text" 
                                maxLength={props.inputData.code == "HOWLONGUSYEARS" && 2}
                                name={props.inputData.code}
                                value={props.setData[props.inputData.code] || ''}
                                placeholder={props.inputData.placeholder}
                                className="inputType1"
                                onChange={(e) => e.target.value.match('^[+0-9 ]*$') != null ? 
                                props.inputData.code == "HOWLONGUSMONTH" ?
                                (e.target.value <= 12 && e.target.value > 0 || e.target.value == '') && props.onchangeInput(e) : 
                                props.onchangeInput(e) : ''}
                                disabled={props.inputData.disabled ? props.inputData.disabled : false}
                                autoComplete="none"                           
                            />
                            {
                                props.error &&
                                <span className="help-block error-text">
                                    <span style={{ color: "red", fontSize: 13 }}>
                                        {props.errorMessage}
                                    </span>
                                </span>
                            }
                        </Form.Group>
                )

        case 'email':
            return (
                <Form.Group controlId={`formBasicName${props.inputData.code}`} key={`formBasicName${props.inputData.code}`}
                className={`${props.inputData.NotEditable && props.isProfileCompleted ? 'contentDisabledNew' : ''}`}
                >
                    <Form.Control
                        type="text"
                        name={props.inputData.code}
                        value={props.setData[props.inputData.code] || ''}
                        placeholder={props.inputData.placeholder}
                        className="inputType1"
                        onChange={props.onchangeInput}
                        disabled={props.inputData.code == 'EMAIL' ? true : false}
                    />
                    {
                        props.error &&
                        <span className="help-block error-text">
                            <span style={{ color: "red", fontSize: 13 }}>
                                {props.errorMessage}
                            </span>
                        </span>
                    }
                </Form.Group>
            )

        case 'date':
            return (
                <Form.Group controlId={`formBasicName${props.inputData.code}`} key={`formBasicName${props.inputData.code}`}
                className={`${props.inputData.NotEditable && props.isProfileCompleted ? 'contentDisabledNew' : ''}`}
                >
                    <DatePicker
                        selected={props.setData[props.inputData.code] ? props.setData[props.inputData.code] : new Date(Date.now() - 5 * 11 * 13 * 7 * 24 * 3600 * 1000)}
                        name={props.inputData.code}
                        value={props.setData[props.inputData.code] || ''}
                        className="inputType1 w-100"
                        maxDate={new Date()}
                        placeholderText="Date of Birth (mm/dd/yyyy)"
                        onChange={(e) => props.onchangeInput(e, props.inputData.code)}
                        dateFormat="MM/dd/yyyy"
                        autoComplete="off"

                        peekNextMonth
                        showMonthDropdown
                        showYearDropdown
                        dropdownMode="select"
                    //locale="es"
                    />
                    {
                        props.error &&
                        <span className="help-block error-text">
                            <span style={{ color: "red", fontSize: 13 }}>
                                {props.errorMessage}
                            </span>
                        </span>
                    }
                </Form.Group>
            )

        case 'dropDown':
            return (
                <Form.Group controlId={`exampleForm.ControlSelect1${props.inputData.code}`} key={`formBasicName${props.inputData.code}`}
                className={`${props.inputData.NotEditable && props.isProfileCompleted ? 'contentDisabledNew' : ''}`}
                >
                    <Form.Control as="select"
                        className="selectTyp1 pointer"
                        name={props.inputData.code}
                        value={props.setData[props.inputData.code] || ''}
                        onChange={props.onchangeInput}
                    >
                        {(props.inputData.code == 'COUNTRY' || props.inputData.code == 'CURRENT_COUNTRY' ||
                            props.inputData.code == 'STATE' || props.inputData.code == 'CURRENT_STATE' || props.inputData.code == 'CITY' || props.inputData.code == 'CURRENT_CITY')
                            ?
                            <option value="">{props.inputData.text}</option>
                            :
                            <option value="">Select</option>
                        }
                        {
                            props && props.listOptions && props.listOptions.length &&
                            props.listOptions.map((item, index) => {
                                return <option key={index} title={item.name ? item.name : item.text} value={item.name ? item.name : item.value}>{item.name ? item.name : item.text}</option>
                            })
                        }
                    </Form.Control>
                    {
                        props.error &&
                        <span className="help-block error-text">
                            <span style={{ color: "red", fontSize: 13 }}>
                                {props.errorMessage}
                            </span>
                        </span>
                    }
                </Form.Group>
            )
        case 'radio':
            return (
                <Form.Group controlId={`formBasicOne${props.inputData.code}`} key={`formBasicName${props.inputData.code}`}
                className={`${props.inputData.NotEditable && props.isProfileCompleted ? 'contentDisabledNew' : ''}`}
                >
                    {
                        props.inputData.options.map((item, index) => {
                            return <Form.Check key={index} type="radio" label={item.text} className={props.setData[props.inputData.code] == item.value ? "radioType1 actives" : "radioType1"} id={props.inputData.code} name={props.inputData.code} value={item.value || ''} checked={props.setData[props.inputData.code] == item.text} onChange={props.onchangeInput} />
                        })
                    }
                    {
                        props.error &&
                        <span className="help-block error-text">
                            <span style={{ color: "red", fontSize: 13 }}>
                                {props.errorMessage}
                            </span>
                        </span>
                    }
                </Form.Group>
            )
            case 'fullDate':
                return (
                    <Form.Group controlId={`formBasicName${props.inputData.code}`} key={`formBasicName${props.inputData.code}`}
                    className={`${props.inputData.NotEditable && props.isProfileCompleted ? 'contentDisabledNew' : ''}`}
                    >
                    <DatePicker
                        selected={props.setData[props.inputData.code] ? props.setData[props.inputData.code] : ''}
                        name={props.inputData.code}
                        value={props.setData[props.inputData.code] || ''}
                        className="inputType1 w-100"
                        maxDate={props.inputData.code == "MAILINGADDRESSFROMDATE" && new Date() }
                        minDate={props.inputData.code == "MAILINGADDRESSTODATE" && new Date() }
                        placeholderText="DD/MM/YYYY"
                        onChange={(e) => props.onchangeInput(e, props.inputData.code)}
                        dateFormat="dd/MM/yyyy"
                        autoComplete="off" 
                        peekNextMonth
                        showMonthDropdown
                        showYearDropdown
                        dropdownMode="select"
                    //locale="es"
                    />
                    {
                        props.error &&
                        <span className="help-block error-text">
                        <span style={{ color: "red", fontSize: 13 }}>
                            {props.errorMessage}
                        </span>
                        </span>
                    }
                    </Form.Group>
                )
        default:
            return '';
    }


}

export default FormInput;




