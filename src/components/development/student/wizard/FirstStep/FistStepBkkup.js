import React, { useState, useEffect } from 'react';
import { Col, Row, Image, Button, Form, Nav, Tab, Accordion, Card } from 'react-bootstrap';
import { Slider, RangeSlider } from 'rsuite';
import 'rsuite/dist/styles/rsuite-default.css';
import Select from 'react-select';
import FormInput from './FormInput'
import { BasicQuestions } from './WizardQuestion'
import moment from 'moment';
import { useSelector } from 'react-redux';
import { countryList, stateList, cityList } from '../../../../Actions/Student/register'
import { getLocalStorage } from '../../../../Lib/Utils';

const options = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
    { value: 'chocolate2', label: 'Chocolate2' },
    { value: 'strawberry2', label: 'Strawberry2' },
    { value: 'vanilla2', label: 'Vanilla2' },
    { value: 'chocolat3', label: 'Chocolate3' },
    { value: 'strawberr3', label: 'Strawberry3' },
    { value: 'vanilla3', label: 'Vanilla3' }
]

const FirstStep = (props) => {

    const [state, setState] = useState({});
    const [countryDatas, setCountryDatas] = useState("");
    const [stateDatas, setStateDatas] = useState("");
    const [cityDatas, setCityDatas] = useState("");
    const userData = useSelector(state => state.required_data);

    const onchangeInput = event => {

        if (event && event.target && event.target.name) {

            let { target: { name, value } } = event;

            if (name == "COUNTRY") {
                var id = countryDatas && countryDatas.find((item) => item.name == value)
                countryInput(id.countryId);
                setState({ ...state, [name]: value });
            } else if (name == "STATE") {
                var id = stateDatas && stateDatas.find((item) => item.name == value)
                cityInput(id.stateId);
                setState({ ...state, [name]: value });
            } else {
                setState({ ...state, [name]: value });
            }


        } else {
            setState({ ...state, DOB: event });
        }
    };

    //get country list

    useEffect(() => {
        var data = ""
        if (userData && userData.data) {
            data = userData.data;
            var id = countryDatas && countryDatas.find((item) => item.name == data.country);
            countryInput(id.countryId);
            setState({
                ...state,
                NAME: data.firstName + " " + data.lastName,
                EMAIL: data.email,
                CONTCTNUM: data.mobileNumber,
                COUNTRY: data.country,
                STATE: data.state,
                ZIP: data.zipCode,
            });

        } else {
            data = getLocalStorage('user')
            var id = countryDatas && countryDatas.find((item) => item.name == data.country)
            countryInput(id.countryId);
            setState({
                ...state,
                NAME: data.firstName + " " + data.lastName,
                EMAIL: data.email,
                CONTCTNUM: data.mobileNumber,
                COUNTRY: data.country,
                STATE: data.state,
                ZIP: data.zipCode,
            });

        }

    }, [userData]);

    useEffect(() => {
        var data = ""
        if (userData && userData.data) {
            data = userData.data;
            var id = countryDatas && countryDatas.find((item) => item.name == data.country)

            countryInput(id.countryId);
        } else {
            data = getLocalStorage('user')
            var id = countryDatas && countryDatas.find((item) => item.name == data.country)
            countryInput(id.countryId);
        }

    }, [countryDatas]);

    useEffect(() => {
        countryList().then(res => {
            setCountryDatas(res.data.data)
        }).catch(err => {
        });
    }, []);

    const countryInput = (id) => {
        setState('')
        stateList(id).then(res => {
            setStateDatas(res.data.data)
        }).catch(err => {
        });
    }

    const cityInput = (id) => {
        setCityDatas('');
        cityList(id).then(res => {
            setCityDatas(res.data.data)
        }).catch(err => {
        });
    }



    return (


        <div className="rightBox pt-5 pb-5 pl-4">


            <Col lg={10} md={12}>
                <div className="rightLayout">
                    <div className="stepTitle fw500 fs30 col4 position-relative mb-5">Basic </div>
                    <Row>

                        {
                            BasicQuestions && BasicQuestions.map((data, index) =>
                                data.section == 'basic' ? (
                                    <Col md={data.code == 'GENDER' ? '12' : '6'}>
                                        {((data.type == 'dropDown')
                                            && (data.code == 'RELAFFIL' || data.code == 'VISASTATUS')) ?
                                            <div className="col2 fw600 mb-1">{data.text}</div>
                                            :
                                            <div className="col2 fw600 mb-1"></div>
                                        }
                                        <FormInput
                                            inputData={data}
                                            listOptions={data.text == 'Country' ? countryDatas :
                                                data.text == 'State' ? stateDatas :                                                    
                                                        data.options}
                                            onchangeInput={onchangeInput}
                                            setData={state}
                                        />
                                    </Col>

                                ) : ''
                            )
                        }

                    </Row>
                </div>
                <div className="footerBotton">
                    <div className="hrBorder pt-3"></div>
                    <div className="text-right mt-3">
                        <Button type="button" className="btnType2">
                            Save & Next
                        </Button>
                    </div>
                </div>
            </Col>
        </div>





    )
}

export default FirstStep;




