import React, { useState, useEffect, useRef } from 'react';
import { Col, Row, Button, Image } from 'react-bootstrap';
import 'rsuite/dist/styles/rsuite-default.css';
import FormInput from './FormInput'
import { BasicQuestions } from '../WizardQuestion'
import { useSelector } from 'react-redux';
import { countryList, stateList, cityList } from '../../../../../Actions/Student/register'
import { getLocalStorage, setLocalStorage } from '../../../../../Lib/Utils';
import infoicon from "../../../../../assets/images/svg/questions.svg";
import TipModal from '../../../TipModal/TipModal'


const FirstStep = (props) => {

  const [state, setState] = useState({});
  const [countryDatas, setCountryDatas] = useState("");
  const [stateDatas, setStateDatas] = useState([]);
  const [cityDatas, setCityDatas] = useState("");
  const [currentStateDatas, setCurrentStateDatas] = useState([]);
  const [currentCityDatas, setCurrentCityDatas] = useState("");
  const [errors, setErrors] = useState({});
  const [tipsData, setTipsData] = useState(false);
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);

  const handleShow = (tipsData) => {
    setTipsData(tipsData);
    setShow(true);
  }

  //let textInput = useRef();
  const userData = useSelector(state => state.required_data);

  useEffect(() => {
    setErrors({})
  }, [props])

  const onchangeInput = (event, code, type) => {
    if (code) {
      // setState({ ...state, [code]: event });
      if (type && type == "slider") {
        setState({ ...state, [code]: event });
        errors[[code]] = false
      } else {
        if (event) {
          setState({ ...state, [code]: event.getTime() });
        } else {
          setState({ ...state, [code]: 0 });
        }
      }
    } else if (event && event.target && event.target.name) {

      let { target: { name, value } } = event;

      if (name == "COUNTRY") {
        var id = countryDatas && countryDatas.find((item) => item.name == value);
        if (id) {
          delete state["STATE"]
          delete state["CITY"]
          setState({ ...state });
          countryInput(id.countryId);
        } else
          countryInput('');
        setState({ ...state, [name]: value, HOWLONGUSMONTH: '', HOWLONGUSYEARS: '', ARMEDSERVICES: '' });
      } else if (name == "STATE") {
        var id = stateDatas && stateDatas.find((item) => item.name == value);
        if (id) {
          delete state["CITY"]
          setState({ ...state });
          cityInput(id.stateId);
        }
        else
          cityInput('');
        setState({ ...state, [name]: value });
      }else if (name == "CURRENT_COUNTRY") {
        var id = countryDatas && countryDatas.find((item) => item.name == value);
        if (id) {
          delete state["CURRENT_STATE"]
          delete state["CURRENT_CITY"]
          setState({ ...state });
          currentCountryInput(id.countryId);
        } else
          currentCountryInput('');
        setState({ ...state, [name]: value });
      } else if (name == "CURRENT_STATE") {
        var id = currentStateDatas && currentStateDatas.find((item) => item.name == value);
        if (id) {
          delete state["CURRENT_CITY"]
          setState({ ...state });
          currentCityInput(id.stateId);
        }
        else
          currentCityInput('');
        setState({ ...state, [name]: value });
      }else if (name == "NAME") {

        if (value.match("^[a-zA-Z .']*$")) {
          setState({ ...state, [name]: value });
        }
      }else if (name == "ZIP") {
        if (value.length <= 6 && (Number(value) || value == '')) {
          setState({ ...state, [name]: value });
        }
      }else if (name == "EMAIL" || name == "CONTCTNUM") {

      }else if (name == "WHATSAPPNO") {

        if (value.length <= 10 && (Number(value) || value == '')) {
          setState({ ...state, [name]: value });
        }
      }else if (name == "HOMEANDMAILINGADD"){
        setState({ ...state, [name]: value, CURRENT_COUNTRY: '', CURRENT_STATE: '', CURRENT_CITY: '', MAILINGADDRESSSTREET: '' });
      }else{
        setState({ ...state, [name]: value });
      }
    } 
  };


  const formatPhoneNumberOutput = (a, b, c) => {
    //textInput.setFlag('in');
    let num = /^[0-9\b]+$/;
    if (b == '' || num.test(b)) {
      if (b.length < 11) {
        setState({
          ...state,
          WHATSAPPNO: b,
          WHATSAPPNOCountryCode: c.dialCode,
          WHATSAPPNOiso2: c.iso2,
        });
      }
    }
  }

  const formatPhoneNumberOutputFlag = (b, c) => {
    let num = /^[0-9\b]+$/;
    if (b == '' || num.test(b)) {
      if (b.length < 11) {
        setState({
          ...state,
          WHATSAPPNO: b,
          WHATSAPPNOCountryCode: c.dialCode,
          WHATSAPPNOiso2: c.iso2,
        });

      }
    }
  }

  //get country list
  useEffect(() => {
    var data = ""
    if (userData && userData.data) {
      data = userData.data;
      var id = countryDatas && countryDatas.find((item) => item.name == data.country);
      countryInput(id.countryId);
      setState({
        ...state,
        NAME: data.firstName + " " + data.lastName,
        EMAIL: data.email,
        CONTCTNUM: data.mobileNumber,
        COUNTRY: data.country,
        STATE: data.state,
        ZIP: data.zipCode,
        VISASTATUS: data.country == "United States" || data.VISASTATUS ? '' : "Not applicable"
      });

    } else if (getLocalStorage('user') && getLocalStorage('user').country) {

      data = getLocalStorage('user');
      var id = countryDatas && countryDatas.find((item) => item.name == data.country)
      countryInput(id.countryId);
      setState({
        ...state,
        NAME: data.firstName + " " + data.lastName,
        EMAIL: data.email,
        CONTCTNUM: data.mobileNumber,
        COUNTRY: data.country,
        STATE: data.state,
        ZIP: data.zipCode,
      });
    }
  }, [userData]);

  useEffect(() => {
    var data = ""
    if (userData && userData.data) {
      data = userData.data;
      var id = countryDatas && countryDatas.find((item) => item.name == data.country)
      if (id) {
        countryInput(id.countryId);
      }
    } else {
      data = getLocalStorage('user')
      var id = countryDatas && countryDatas.find((item) => item.name == data.country)
      if (id) {
        countryInput(id.countryId);
      }
    }
  }, [countryDatas]);


  useEffect(() => {
    if (stateDatas && stateDatas.length > 0) {
      var data = ""
      if (userData && userData.data) {
        data = userData.data;
        var id = stateDatas && stateDatas.find((item) => item.name == data.state)
        if (id && id.stateId)
          cityInput(id.stateId);
      } else {
        data = getLocalStorage('user');
        var id = stateDatas && stateDatas.find((item) => item.name == data.state);
        if (id && id.stateId)
          cityInput(id.stateId);
      }
    }
  }, [stateDatas]);

  useEffect(() => {
    if (currentStateDatas && currentStateDatas.length > 0) {
      var data = ""
      if (userData && userData.data) {
        data = userData.data;
        var id = currentStateDatas && currentStateDatas.find((item) => item.name == data.state)
        if (id && id.stateId)
          currentCityInput(id.stateId);
      } else {
        data = getLocalStorage('user');
        var id = currentStateDatas && currentStateDatas.find((item) => item.name == data.state);
        if (id && id.stateId)
          currentCityInput(id.stateId);
      }
    }
  }, [currentStateDatas]);


  useEffect(() => {
    if (props.data && Object.keys(props.data).length != 0) {
      setState({
        ...state,
        ...props.data
      });
      setTimeout(
        function () {
          const element = document.querySelector('.iti-flag');
          if (element) {
            element.classList.remove(element.classList[1]);
            element.classList.add(props.data.WHATSAPPNOiso2);
          }
        }.bind(this),
        100
      );
    } else {
      const element = document.querySelector('.iti-flag');
      if (element) {
        element.classList.remove(element.classList[1]);
        element.classList.add(props.data.WHATSAPPNOiso2);
      }
    }
  }, [props.data]);

  useEffect(() => {
    countryList().then(res => {
      setCountryDatas(res.data.data);
    }).catch(err => { });
  }, []);

  const countryInput = (id) => {
    if (id) {
      setStateDatas('');
      setCityDatas('');
      stateList(id).then(res => {
        setStateDatas(res.data.data)
      }).catch(err => { });
    }
  }

  const currentCountryInput = (id) => {
    if (id) {
      setCurrentStateDatas('');
      setCurrentCityDatas('');
      stateList(id).then(res => {
        setCurrentStateDatas(res.data.data)
      }).catch(err => { });
    }
  }

  const cityInput = (id) => {
    if (id) {
      setCityDatas('');
      cityList(id).then(res => {
        if (res.data && res.data.data && res.data.data.length) {
          setCityDatas(res.data.data)
        } else {
          setCityDatas('')
        }
      }).catch(err => { });
    }
  }

  const currentCityInput = (id) => {
    if (id) {
      setCurrentCityDatas('');
      cityList(id).then(res => {
        if (res.data && res.data.data && res.data.data.length) {
          setCurrentCityDatas(res.data.data)
        } else {
          setCurrentCityDatas('')
        }
      }).catch(err => { });
    }
  }


  useEffect(() => {
    if (props.data && Object.keys(props.data).length != 0 && props.data["CURRENT_COUNTRY"]) {
      var c_id = c_id = countryDatas && countryDatas.find((item) => item.name == props.data["CURRENT_COUNTRY"]);
      if (c_id && c_id.countryId) currentCountryInput(c_id.countryId);
    }
  }, [props.data && countryDatas]);

  useEffect(() => {
    if (props.data && Object.keys(props.data).length != 0 && props.data["CURRENT_STATE"]) {
      var s_id = currentStateDatas && currentStateDatas.find((item) => item.name == props.data["CURRENT_STATE"]);
      if (s_id && s_id.stateId) currentCityInput(s_id.stateId);
    }
  }, [props.data && currentStateDatas]);



  const stepSubmit = () => {
    let errors = {};
    let valid = true;

    for (var m = 0; m < BasicQuestions.length; m++) { 
      if (BasicQuestions[m].isRequired == true) {
        if (BasicQuestions[m].code == "CITY" && cityDatas.length <= 0) {
          errors["CITY"] = false
          setState({ ...state, CITY: "" });
        }else {  
          if((BasicQuestions[m].code == "ARMEDSERVICES" || BasicQuestions[m].code == "HOWLONGUSMONTH" || BasicQuestions[m].code == "HOWLONGUSYEARS") ){
            if(state["COUNTRY"] == "United States"){
              let codevalue = state[BasicQuestions[m].code];
              if (!codevalue || codevalue == 'undefined' || codevalue == '') {
                valid = false;
                errors[BasicQuestions[m].code] = true;
              }
            } 
          }else{ 
            let codevalue = state[BasicQuestions[m].code];
            if (!codevalue || codevalue == 'undefined' || codevalue == '') {
              valid = false;
              errors[BasicQuestions[m].code] = true;
            }
          } 
        }
      }

      // Check child validation           
      if (BasicQuestions[m].isLogical && BasicQuestions[m].isRequired == true) {
        if(BasicQuestions[m].code == "HOMEANDMAILINGADD"){
          let Newarry = BasicQuestions[m].options;
          Newarry && Newarry.length > 0 &&
          Newarry.map((iten_1) =>{
            if(iten_1.isLogical == true && iten_1.value == state[BasicQuestions[m].code]){
              iten_1.questions && iten_1.questions.length > 0 &&
              iten_1.questions.map((item_2) =>{
                if(item_2.isRequired == true){
                  if (item_2.code == "CURRENT_CITY" && currentCityDatas.length <= 0) {
                    errors["CURRENT_CITY"] = false
                    setState({ ...state, CURRENT_CITY: "" });
                  } else{
                    let codevalueNew = state[item_2.code];
                    if (!codevalueNew || codevalueNew == 'undefined' || codevalueNew == '') {
                      valid = false;
                      errors[item_2.code] = true;
                    }
                  }
                }
              })
               
            }
          })
        }
        if(BasicQuestions[m].code == "ISWHATSAPPNO"){
          let Newarry = BasicQuestions[m].options;
          Newarry && Newarry.length > 0 &&
          Newarry.map((iten_1) =>{
            if(iten_1.isLogical == true && iten_1.value == state[BasicQuestions[m].code]){
              iten_1.questions && iten_1.questions.length > 0 &&
              iten_1.questions.map((item_2) =>{
                if(item_2.isRequired == true){
                  let codevalueNew = state[item_2.code];
                  if (!codevalueNew || codevalueNew == 'undefined' || codevalueNew == '') {
                    valid = false;
                    errors[item_2.code] = true;
                  }
                }
              })
               
            }
          })
        } 
      }

    }



    if (state['WHATSAPPNO']) {

      if (parseInt(state['WHATSAPPNO']) == parseInt(state['CONTCTNUM'])) {
        valid = false;
        errors['WHATSAPPNO'] = "Please use a different Whatsapp Number.";
      }

    }

    if (valid) {
      setLocalStorage('country', state['COUNTRY'])
      setLocalStorage('visa', state['VISASTATUS'])
      props.stepSubmit(state, 1);
      setErrors({});
    }
    else {

      setErrors({ ...errors });
    }

  }





  return (


    <div className="rightBox pt-5 pb-5 pl-4">

      <Col lg={10} md={12}>
        <div className="rightLayout">
          <div className="d-flex flex-wrap justify-content-between mb-1">
            <div className="stepTitle fw500 fs30 col4 position-relative mb-4 pb-1">Basic Info</div>
            {
              (getLocalStorage('user') && getLocalStorage('user').isProfileCompleted) &&
              <div className="col2 fw500 fs18 pointer" onClick={() => props.stepSelect('second')}>Skip</div>
            }
          </div>
          <Row>

            {
              BasicQuestions && BasicQuestions.map((data, index) =>
                <React.Fragment key={index}>

                  {((state && state.COUNTRY !== 'United States') && 
                    (data.code === 'HOWLONGUSMONTH' || data.code === 'HOWLONGUSYEARS')) ? ''
                    :
                    ((state && state.COUNTRY !== 'United States') && 
                    (data.code === 'ARMEDSERVICES' )) ? ''
                    :
                    <Col md={data.code == 'GENDER' || data.code == 'ISWHATSAPPNO' || data.code == 'HOMEANDMAILINGADD' || data.code == 'ARMEDSERVICES' ? '12' : '6'} key={index + 'abc'}>
                      <div className="col2 fw600 mb-1">{data.text}
                        {data.isRequired ? <sup className="starQ starQ2">{data.text ? '*' : ''}</sup> : ''} 
                        {data.tips ? <Image onClick={() => handleShow(data.tips)}
                          src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                      </div>
                      {
                        data.code == "PERMANENTHOMEAPARTMENT" && <div className="mt48"></div>
                      }

                      <FormInput 
                        key={index}
                        inputData={data}
                        listOptions={data.code == 'COUNTRY' ? countryDatas :
                          data.code == 'STATE' ? stateDatas :
                            data.code == 'CITY' ? cityDatas :
                              data.options}
                        onchangeInput={onchangeInput}
                        setData={state}
                        error={errors[data.code] ? true : false}
                        errorMessage={errors[data.code] == true ? 'This field is required.' :
                          errors[data.code] !== '' ? errors[data.code] : 'This field is required.'}
                      />
                    </Col>
                  }

                  {
                    //code for logical questions
                    data.isLogical == true ?
                      data.options && data.options.map((option, indexNew) =>
                      (
                        option.isLogical == true ?
                          state[data.code] && state[data.code] == option.text ?
                            option.questions && option.questions.map((question, indexSecond) =>
                              <React.Fragment key={indexSecond}>
                                <Col md={6} key={indexSecond + 'eee'}>
                                  <div className="col2 fw600 mb-1">{question.text}
                                    {question.isRequired ? <sup className="starQ starQ2">{question.text ? '*' : ''}</sup> : ''}
                                  </div>
                                  <FormInput
                                    key={indexSecond + 'adfsf'}
                                    //textInput={textInput}
                                    inputData={question}
                                    listOptions={question.code == 'CURRENT_COUNTRY' ? countryDatas :
                                      question.code == 'CURRENT_STATE' ? currentStateDatas :
                                      question.code == 'CURRENT_CITY' ? currentCityDatas :
                                      question.options}
                                    onchangeInput={onchangeInput}
                                    formatPhoneNumberOutput={formatPhoneNumberOutput}
                                    formatPhoneNumberOutputFlag={formatPhoneNumberOutputFlag}
                                    //oncheckBox={oncheckBox}
                                    setData={state}
                                    error={errors[question.code] ? true : false}
                                    errorMessage={errors[question.code] == true ? 'This field is required.' :
                                      errors[question.code] !== '' ? errors[question.code] : 'This field is required.'}
                                  />
                                </Col>
                              </React.Fragment>
                            )
                            : ''
                          : ''
                      )
                      )
                      : ''
                  }

                </React.Fragment>

              )
            }

            {/* <Col md={6}>
                            <Form.Group controlId="exampleForm.ControlSelect1dddasd">
                                <Select
                                    defaultValue={[options[2], options[3]]}
                                    isMulti
                                    name="colors"
                                    options={options}
                                    className="basic-multi-select selectTyp2"
                                    classNamePrefix="select"
                                    placeholder="Country"
                                />
                            </Form.Group>
                        </Col>                         */}
          </Row>
        </div>
        <div className="footerBotton">
          <div className="hrBorder pt-3"></div>
          <div className="text-right mt-3">
            <Button
              type="button"
              className="btnType2"
              onClick={() => stepSubmit()}
            >
              Save & Next
            </Button>
          </div>
        </div>
      </Col>

      <TipModal tipsData={tipsData} title={"Info"} show={show} handleClose={handleClose}></TipModal>
    </div>





  )
}

export default FirstStep;




