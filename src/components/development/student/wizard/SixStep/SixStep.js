import React, { useState, useEffect } from 'react';
import { Col, Row, Button, Image } from 'react-bootstrap';
import 'rsuite/dist/styles/rsuite-default.css';
import FormInput from './FormInput'
import { PLACEMENTPREFRENCEPRE } from '../WizardQuestion'
import { getLocalStorage } from '../../../../../Lib/Utils';
import infoicon from "../../../../../assets/images/svg/questions.svg";
import TipModal from '../../../TipModal/TipModal'


const SixStep = (props) => {

  const [state, setState] = useState({});
  const [errors, setErrors] = useState({});
  const [totalDefth, setTotalDefth] = useState(['Yes', 'No', 'Preferred but optional']);
  const [tipsData, setTipsData] = useState(false);
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);

  const handleShow = (tipsData) => {
    setTipsData(tipsData);
    setShow(true);
  }


  useEffect(() => {
    setErrors({})
  }, [props])

  const onchangeInput = (event, code, type) => {
    if (code) {
      if (type && type == "slider") {

        const { target: { name, value } } = event;
        var valFound = false
        var codeArray = ["COLLEGECOMPANYPARTNERSHIP", "INTERNSHIPOPP", "EMPLOYMENTOPP", "POSTGRADUATIONJOB", "TRAININGOPT", "TRAININGCPT"]
        let errors = {};
        codeArray.map((item, index) => {
          if (state[item] && (state[item] == value)) {
            //errors[item] = 'Please Use different preferences value';
            valFound = true
            //setState({ ...state, [item]: 0, [code]: event });
          }
        })

        setState({ ...state, [code]: value });
        if (!valFound) {
          //setState({ ...state, [code]: value });
          //errors[[code]] = false
        } else {
          //errors[code] = 'Please Use different preferences value';
          //setErrors({ ...errors });
        }

      }
    } else {
      if (event && event.target && event.target.name) {
        const { target: { name, value } } = event;
        setState({ ...state, [name]: value });
      } else {
        //setState({ ...state, DOB: event });
      }
    }

  };

  const onBlurChangeInput = (event, code, type) => {
    if (type && type == "slider") {
      const { target: { name, value } } = event;
      var valFound = false
      var codeArray = ["COLLEGECOMPANYPARTNERSHIP", "INTERNSHIPOPP", "EMPLOYMENTOPP", "POSTGRADUATIONJOB", "TRAININGOPT", "TRAININGCPT"]
      let errorsNew = errors;

      codeArray.map((item, index) => {       
        if (state[item] && (state[item] == value) && (item !=code )) {
          //errors[item] = 'Please Use different preferences value';
          valFound = true
          //setState({ ...state, [item]: 0, [code]: event });
        }
      })      
      if (!valFound) {       
        //setState({ ...state, [code]: value });
        //errors[[code]] = false
       
        errorsNew[code] = '';
        setErrors({ ...errorsNew });
      } else {
        errorsNew[code] = 'Selected preference order value is already assigned.';
        setErrors({ ...errorsNew });
        setState({ ...state, [code]: 0 });
      }
    }
  }




  const onKeyDownInput = event => {
    if (event && event.target && event.target.name) {
      const { target: { name, value } } = event;

      if (event.keyCode == 13) {
        setState({ ...state, [name]: '' });
        var arr = name.split('_');
        let Newname = arr[0];
        if (state[Newname]) {
          let ArryVale = state[Newname];
          var indexB = ArryVale.indexOf(value);
          if (indexB > -1) {
            ArryVale.splice(indexB, 1);
            setState({ ...state, [name]: '', [Newname]: [...ArryVale] });
          } else {
            ArryVale.push(value);
            setState({ ...state, [name]: '', [Newname]: [...ArryVale] });
          }
        } else {
          setState({ ...state, [name]: '', [Newname]: [value] });
        }

      }

    } else {
      //setState({ ...state, DOB: event });
    }
  };



  const oncheckBox = (name, value) => {
    if (state[name]) {
      let ArryVale = state[name];
      var indexB = ArryVale.indexOf(value);
      if (indexB > -1) {
        ArryVale.splice(indexB, 1);
        if (ArryVale && ArryVale.length <= 0) {
          delete state[name]
          setState({ ...state });
        } else {
          setState({ ...state, [name]: [...ArryVale] });
        }
      } else {
        ArryVale.push(value);
        setState({ ...state, [name]: [...ArryVale] });
      }
    } else {
      setState({ ...state, [name]: [value] });
    }
  }

  useEffect(() => {
    if (props.data && Object.keys(props.data).length != 0) {
      setState({
        ...state,
        ...props.data
      });
    }
  }, [props.data]);

  const stepSubmit = () => {
    let errors = {};
    let valid = true;

    for (var m = 0; m < PLACEMENTPREFRENCEPRE.length; m++) {
      if (PLACEMENTPREFRENCEPRE[m].isRequired) {
        let codevalue = state[PLACEMENTPREFRENCEPRE[m].code];
        if (!codevalue || codevalue == 'undefined' || codevalue == '') {
          valid = false;
          errors[PLACEMENTPREFRENCEPRE[m].code] = true;
        }
      }
    }

    if (valid) {
      props.stepSubmitSix(state, 6);
      setErrors({});
    }
    else {
      setErrors({ ...errors });
    }
  }

  return (

    <div className="rightBox pt-5 pb-5 pl-4">
      <Col lg={10} md={12}>
        <div className="rightLayout">
          <div className="d-flex flex-wrap justify-content-between mb-1">
            <div className="stepTitle fw500 fs30 col4 position-relative mb-4 pb-1">Placement Preferences </div>
            {
              getLocalStorage('user') && getLocalStorage('user').isProfileCompleted &&
              <div className="col2 fw500 fs18 pointer" onClick={() => props.stepSelect('seven')}>Skip</div>
            }
          </div>
          <Row className="stepForm7">

            {
              PLACEMENTPREFRENCEPRE && PLACEMENTPREFRENCEPRE.map((data, index) =>
                <div key={index} className={data.conditionalDropDown ? 'row ml-md-0' : ''}>
                  {
                        index == 0 && 
                        <Col md={12} key={index + 'aaa'}>
                        <div className="col4 fw600 fs18 mb-2">
                          <span className="numericType1">{1}.</span>
                          Please rank the following six placement options as per your order of preference and what matters to you the most
                        </div>
                        </Col>
                      }
                  {data.type == 'dropDown' ?
                    <Col md={data.conditionalDropDown ? 6 : 12} key={index + 'aaa'}> 
                      
                      <div className="col4 fw600 fs18 mb-2"> 
                        <span className="numericType1">
                          {index == 6 && 2 + '.'}
                          </span>
                        {data.text}
                        {data.isRequired ? <sup className="starQ">*</sup> : ''}
                        {data.tips ? <Image onClick={() => handleShow(data.tips)} src={infoicon} alt="Questions" className="ml-2 mt-2 pointer" /> : ''}
                      </div>
                    </Col>
                    : ''}

                  <Col md={(data.type !== 'dropDown' || data.type !== 'categorical') ? `${data.conditionalDropDown ? 6 : 12}` : '6'} key={index + 'bbb'}>
                    {(data.type !== 'dropDown') ?
                      <div className="col4 fw600 fs18 mb-2">
                        <span className="numericType1">{index + 1}.</span>
                        {data.text}
                        {data.isRequired ? <sup className="starQ">*</sup> : ''}
                        {data.tips ? <Image onClick={() => handleShow(data.tips)} src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                      </div>
                      : ''}
                    <FormInput
                      key={index + 'ddsd'}
                      inputData={data}
                      listOptions={data.options}
                      onchangeInput={onchangeInput}
                      onKeyDownInput={onKeyDownInput}
                      oncheckBox={oncheckBox}
                      handleShow={handleShow}
                      onBlurChangeInput={onBlurChangeInput}
                      setData={state}
                      totalDefth={totalDefth}
                      error={errors[data.code] ? true : false}
                      //errorMessage={'This field is required.'}
                      errorMessage={errors[data.code] == true ? 'This field is required.' :
                        errors[data.code] !== '' ? errors[data.code] : 'This field is required.'}
                    />
                  </Col>

                </div>
              )
            }

          </Row>
        </div>
        <div className="footerBotton">
          <div className="hrBorder pt-3"></div>
          <div className="text-right d-flex flex-wrap justify-content-between  mt-3">
            <Button type="button" className="btnType7"
              onClick={() => props.stepSelect('fivth')}>
              Previous
            </Button>
            <Button
              type="button"
              className="btnType2"
              onClick={() => stepSubmit()}
            >
              Save & Next
            </Button>
          </div>
        </div>
      </Col>


      <TipModal tipsData={tipsData} title={"Info"} show={show} handleClose={handleClose}></TipModal>

    </div>


  );


}

export default SixStep;

