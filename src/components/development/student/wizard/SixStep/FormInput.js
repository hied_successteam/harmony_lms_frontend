import React from 'react';
import { Image, Form, Table } from 'react-bootstrap';
import 'rsuite/dist/styles/rsuite-default.css';
import "react-datepicker/dist/react-datepicker.css";
import infoicon from "../../../../../assets/images/svg/questions.svg";
import { Slider, RangeSlider } from 'rsuite';

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'chocolate2', label: 'Chocolate2' },
  { value: 'strawberry2', label: 'Strawberry2' },
  { value: 'vanilla2', label: 'Vanilla2' },
  { value: 'chocolat3', label: 'Chocolate3' },
  { value: 'strawberr3', label: 'Strawberry3' },
  { value: 'vanilla3', label: 'Vanilla3' }
]


const FormInput = (props) => {

  switch (props.inputData.type) {

    case 'text':
      return (
        <Form.Group controlId={`formBasicName${props.inputData.code}`}>
          <Form.Control
            type="text"
            name={props.inputData.code == 'VOLUNACTIVITY' ? props.inputData.code + '_VV' : props.inputData.code}
            value={(props.inputData.code == 'VOLUNACTIVITY' ? props.setData[props.inputData.code + '_VV'] : props.setData[props.inputData.code]) || ''}
            placeholder={props.inputData.placeholder}
            className={`inputType1 ${props.inputData.code == 'VOLUNACTIVITY' ? 'mb-3' : ''}`}
            onChange={props.onchangeInput}
            onKeyDown={props.onKeyDownInput}
          />
          {props.inputData.code == 'VOLUNACTIVITY' ?
            <div className="tagType1 d-flex flex-wrap">
              {
                props.setData[props.inputData.code] &&
                props.setData[props.inputData.code].map((item, index) => {
                  return <span key={index} className="br8 col4 fw500 mr-3">{item}</span>
                })
              }
            </div>
            : ''}
          {
            props.error &&
            <span className="help-block error-text">
              <span style={{ color: "red", fontSize: 13 }}>
                {props.errorMessage}
              </span>
            </span>
          }
        </Form.Group>
      )

    case 'dropDown':
      return (
        <>
          <Form.Group controlId={`exampleForm.ControlSelect1${props.inputData.code}`}>
            <Form.Control as="select"
              className="selectTyp1 pointer"
              name={props.inputData.code}
              value={props.setData[props.inputData.code] || ''}
              //onChange={props.onchangeInput}            
              onChange={(e) => props.inputData.conditionalDropDown === true ?
                props.onchangeInput(e, props.inputData.code, "slider") : props.onchangeInput(e)}
              onBlur={(e) => props.inputData.conditionalDropDown === true ?
                props.onBlurChangeInput(e, props.inputData.code, "slider") : props.onBlurChangeInput(e)}
            >
              {/* <option value="">{props.inputData.text}</option> */}
              <option value="">Select</option>
              {
                props && props.listOptions &&
                props.listOptions.map((item, index) => {
                  return <option key={index} title={item.name ? item.name : item.text} value={item.name ? item.name : item.value}>{item.name ? item.name : item.text}</option>
                })
              }
            </Form.Control>
            {
              props.error &&
              <span className="help-block error-text">
                <span style={{ color: "red", fontSize: 13 }}>
                  {props.errorMessage}
                </span>
              </span>
            }
          </Form.Group>
        </>
      )

    case 'checkbox':
      return (
        <div className="tagType1 d-flex flex-wrap mb-3">
          {
            props.inputData.options.map((item, index) => {
              return <span
                key={index}
                className={`br8 col4 fw500 mr-3 ${props.setData[props.inputData.code] && props.setData[props.inputData.code].indexOf(item.value) > -1 ? 'bgCol35' : ''}  `}
                name={props.inputData.code}
                onClick={() => props.oncheckBox(props.inputData.code, item.value)}
                value={item.value || ''}>{item.value || ''}</span>
            })
          }
          {
            props.error &&
            <span className="help-block error-text">
              <span style={{ color: "red", fontSize: 13 }}>
                {props.errorMessage}
              </span>
            </span>
          }
        </div>
      )

    case 'textArea':
      return (
        <>
          <Form.Group className="mb-3" controlId={`formBasicName${props.inputData.code}`}>
            <Form.Control
              as="textarea"
              name={props.inputData.code}
              value={props.setData[props.inputData.code] || ''}
              placeholder={props.inputData.placeholder}
              className="inputType1"
              rows={5}
              onChange={props.onchangeInput}
            />
            {
              props.error &&
              <span className="help-block error-text">
                <span style={{ color: "red", fontSize: 13 }}>
                  {props.errorMessage}
                </span>
              </span>
            }
          </Form.Group>
        </>
      )

    case 'radio':
      return (
        <Form.Group className="radioTwo" controlId={`formBasicOne${props.inputData.code}`}>
          {
            props.inputData.options.map((item, index) => {
              return <Form.Check type="radio"
                key={index}
                label={item.text}
                className={props.setData[props.inputData.code] == item.value ? "radioType1 actives" : "radioType1"}
                id={props.inputData.code} name={props.inputData.code}
                value={item.value || ''}
                checked={props.setData[props.inputData.code] == item.text}
                onChange={props.onchangeInput} />
            })
          }
          {
            props.error &&
            <span className="help-block error-text">
              <span style={{ color: "red", fontSize: 13 }}>
                {props.errorMessage}
              </span>
            </span>
          }
        </Form.Group>
      )
    case 'categorical':
      return (
        <>

          <Table bordered className="tableType2 planTable br10 mb-4">
            <thead>
              <tr>
                <th></th>
                <th>Yes</th>
                <th>No</th>
                <th>Preferred but optional</th>
              </tr>
            </thead>
            <tbody>

              {props.inputData.questions && props.inputData.questions.map((ques, index) =>

                <tr key={index}>
                  <td>
                    {ques.text}
                    {ques.tips ? <Image onClick={() => props.handleShow(ques.tips)} src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                  </td>
                  {props.totalDefth && props.totalDefth.map((number, index) =>
                    <td key={index}>
                      <Form.Group controlId={`formBasicRadio1${ques.code}${number}`}>
                        <Form.Check
                          type="radio"
                          aria-label={ques.text}
                          className="checkboxTyp3"
                          id={`formBasicRadio1${ques.code}${number}`}
                          name={ques.code}
                          label=""
                          value={number || ''}
                          checked={true}
                          checked={props.setData[ques.code] == number}
                          onChange={props.onchangeInput}
                        />
                      </Form.Group>
                    </td>
                  )}
                </tr>
              )}

            </tbody>
          </Table>

        </>

      )
    case 'slider':
      return (
        <>
          {
            // props.inputData.questions && props.inputData.questions.map((item, index) => (
            <Form.Group controlId="formBasicRange"
              className={`${props.inputData.NotEditable && props.isProfileCompleted ? 'contentDisabledNew' : ''}`}
            >
              {
                <Form.Label className="fw600 col2 mb-3">
                  <span className="mr-3">{props.inputData.text}</span>
                </Form.Label>
              }
              {/* onChange={(e) => props.onchangeInput(e, props.inputData.code, "slider")} */}

              <Slider name={props.inputData.code}
                onChange={(e) => props.onchangeInput(e, props.inputData.code, "slider")}
                defaultValue={2} value={props.setData[props.inputData.code] || props.inputData.minRange} min={props.inputData.minRange} step={props.inputData.interval ? props.inputData.interval : 1} max={props.inputData.maxRange} graduated progress
                renderMark={mark => {
                  return mark;
                }}
              />
              {
                props.error &&
                <span className="help-block error-text">
                  <span style={{ color: "red", fontSize: 13 }}>
                    {props.errorMessage}
                  </span>
                </span>
              }
              <Form.Label className="fw600 col2 mb-3"></Form.Label>
            </Form.Group>
            // ))
          }

        </>

      )
    default:
      return '';
  }


}

export default FormInput;




