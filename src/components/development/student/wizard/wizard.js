import React, { useState, useEffect } from 'react';
import { Col, Row, Image, Button, Form, Nav, Tab, Modal, Accordion, Card, Table } from 'react-bootstrap';
import { Slider, RangeSlider } from 'rsuite';
import 'rsuite/dist/styles/rsuite-default.css';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import Select from 'react-select';
import footerLogo from '../../../../assets/images/footerlogo.png';
import navComments from '../../../../assets/images/svg/comments.svg';
// import { colourOptions } from '../data';   
// import AsyncSelect from 'react-select/lib/Async'; 
// import { colourOptions } from '../data';       

import rocketDown from "../../../../assets/images/svg/rocket-down.svg";
import rocketUp from "../../../../assets/images/svg/rocket-up.svg";
import Sends from "../../../../assets/images/svg/sends.svg";
import Speed from "../../../../assets/images/svg/speed.svg";
import DownIcon from "../../../../assets/images/svg/downIcon.svg";

import SecondStep from './SecondStep/SecondStep';
import FirstStep from './FirstStep/FirstStep';
import FourStep from './FourStep/FourStep';
import FiveStep from './FiveStep/FiveStep';
import SixStep from './SixStep/SixStep';
import SevenStep from './SevenStep/SevenStep';
import EightStep from './EightStep/EightStep';
import ThirdStep from './ThirdStep/ThirdStep';
import { logout } from '../../../../Actions/Student/register';
import { getLocalStorage, setLocalStorage, clearLocalStorage } from '../../../../Lib/Utils';
import { profile, getprofile, getPlan, getfeature, purchasePlan, dashboard } from '../../../../Actions/Student/register';
import { actionLogout } from '../../../../Redux/College/LoginAction';

import Loader from '../../../../Lib/LoaderNew';

const Wizard = (props) => {

  const [key, setKey] = useState('first');
  const [userData, setUserData] = useState({});
  const [profileData, setProfileData] = useState({});
  const [stage, setStage] = useState(false);
  const [profileStepCompleted, setProfileStepCompleted] = useState(1);
  const [basic, setBasic] = useState({});
  const [academicInfo, setAcademicInfo] = useState({});
  const [collegePreference, setCollegePreference] = useState({});
  const [demographicPreference, setDemographicPreference] = useState({});
  const [employmentInfo, setEmploymentInfo] = useState({});
  const [placementPreference, setPlacementPreference] = useState({});
  const [finalPreference, setFinalPreference] = useState({});
  const [planInfo, setPlanInfo] = useState({});
  const [planInfoPayment, setPlanInfoPayment] = useState({});
  const [isProfileCompleted, setIsProfileCompleted] = useState(false);
  const [planData, setPlanData] = useState([]);
  const [featureData, setFeatureData] = useState([]);
  const [planType, setPlanType] = useState('free');
  const [loader, setLoader] = useState(false);
  const [planChange, setPlanChange] = useState(false);
  const history = useHistory();
  const dispatch = useDispatch();


  useEffect(() => {

    if (history &&
      history.location &&
      history.location.state &&
      history.location.state.planChange) {
      setKey('eight');
      setPlanChange(true);
    } else {
      setKey('first');
      setPlanChange(false);
    }
    const unloadCallback = (event) => {
      event.preventDefault();
      event.returnValue = "";
      return "";
    };
    window.addEventListener("beforeunload", unloadCallback);
    return () => window.removeEventListener("beforeunload", unloadCallback);
  }, []);



  useEffect(() => {
    var user = getLocalStorage('user');

    if (user && user.id) {
      setIsProfileCompleted(user.isProfileCompleted ? user.isProfileCompleted : false);
      setUserData(user);
      getprofile(user.id).then(res => {
        setProfileData(res.data.data);
      }).catch(err => {
      });

      getPlan().then(res => {
        if (res.data.data && res.data.data.rows) {
          setPlanData(res.data.data.rows)
        }
      }).catch(err => {
      });

      getfeature().then(res => {
        if (res.data.data && res.data.data.rows) {
          setFeatureData(res.data.data.rows)
        }
      }).catch(err => {
      });

    }

  }, []);

  useEffect(() => {

    if (profileData && Object.keys(profileData).length != 0) {

      if (profileData.basic && Object.keys(profileData.basic).length != 0) {
        setBasic({ ...profileData.basic });
      }
      if (profileData.academicInfo && Object.keys(profileData.academicInfo).length != 0) {
        setAcademicInfo({ ...profileData.academicInfo });
      }
      if (profileData.collegePreference && Object.keys(profileData.collegePreference).length != 0) {
        setCollegePreference({ ...profileData.collegePreference });
      }
      if (profileData.demographicPreference && Object.keys(profileData.demographicPreference).length != 0) {
        setDemographicPreference({ ...profileData.demographicPreference });
      }
      if (profileData.employmentInfo && Object.keys(profileData.employmentInfo).length != 0) {
        setEmploymentInfo({ ...profileData.employmentInfo });
      }
      if (profileData.placementPreference && Object.keys(profileData.placementPreference).length != 0) {
        setPlacementPreference({ ...profileData.placementPreference });
      }
      if (profileData.finalPreference && Object.keys(profileData.finalPreference).length != 0) {
        setFinalPreference({ ...profileData.finalPreference });
      }
      if (profileData.planInfo && Object.keys(profileData.planInfo).length != 0) {
        setPlanInfo({ ...profileData.planInfo });
      }

      if (profileData.planInfoPayment && Object.keys(profileData.planInfoPayment).length != 0) {
        setPlanInfoPayment({ ...profileData.planInfoPayment });
      }

    }

  }, [profileData]);


  const logoutCall = () => {
    var user = getLocalStorage('user');
    if (user && user.id) {
      logout({
        userId: user.id
      }).then(res => {
        if (res.data.success == true) {
          dispatch(actionLogout('college'));
          localStorage.clear();
          clearLocalStorage();
          props.history.push({ pathname: '/student/login' });
        } else {
          dispatch(actionLogout('college'));
          localStorage.clear();
          clearLocalStorage();
          props.history.push({ pathname: '/student/login' });
        }
      }).catch(err => {
        dispatch(actionLogout('college'));
        localStorage.clear();
        clearLocalStorage();
        props.history.push({ pathname: '/student/login' });
      });
    } else {
      dispatch(actionLogout('college'));
      localStorage.clear();
      clearLocalStorage();
      props.history.push({ pathname: '/student/login' });
    }
  }

  const setLoaderManual = (flag) => {
    setLoader(flag);
  }


  const stepSubmit = (state, steps) => {
    window.scrollTo(0, 0);
    setProfileStepCompleted(steps);
    setBasic({ ...state });
    setStage(true);
    setKey('second')
    //apiCall('second')
  }

  const stepSubmitSecond = (state, steps) => {
    window.scrollTo(0, 0);
    setProfileStepCompleted(steps);
    setAcademicInfo({ ...state });
    setStage(true);
    setKey('third')
    //apiCall('third')
  }

  const stepSubmitThird = (state, steps) => {
    window.scrollTo(0, 0);
    setProfileStepCompleted(steps);
    setCollegePreference({ ...state });
    setStage(true);
    setKey('fourth')
    //apiCall('fourth')
  }

  const stepSubmitFour = (state, steps) => {
    window.scrollTo(0, 0);
    setProfileStepCompleted(steps);
    setDemographicPreference({ ...state });
    setStage(true);
    setKey('fivth')
    //apiCall('fivth')
  }

  const stepSubmitFive = (state, steps) => {
    window.scrollTo(0, 0);
    setProfileStepCompleted(steps);
    setEmploymentInfo({ ...state });
    setStage(true);
    setKey('sixth')
    //apiCall('sixth')
  }

  const stepSubmitSix = (state, steps) => {
    window.scrollTo(0, 0);
    setProfileStepCompleted(steps);
    setPlacementPreference({ ...state });
    setStage(true);
    setKey('seven')
    //apiCall('seven')
  }

  const stepSubmitSeven = (state, steps) => {
    window.scrollTo(0, 0);
    setProfileStepCompleted(steps);
    setFinalPreference({ ...state });
    setStage(true);
    setKey('eight')
    //apiCall('eight')
  }

  const stepSubmitEight = (state, steps) => {
    window.scrollTo(0, 0);
    setPlanType(state.planType);

    let data = {
      "planType": state.planType,
      "planId": state.planId,
      "token": state.planType == 'free' ? '' : state.token,
    }
    purchasePlan(data).then(({ data: { success, data, message } }) => {
      if (success !== true) {
        //setMessage(message);    
        setLoader(false);
        return;
      } else {
        setProfileStepCompleted(steps);
        setIsProfileCompleted(true);
        setPlanInfo({ ...state });
        setStage(true);
        //apiCall()
      }
    })
      .catch((error) => {
        return;
      });
  }

  const stepSubmitEightAfterPayment = (state, steps) => {
    setPlanType('paid');
    setProfileStepCompleted(steps);
    setIsProfileCompleted(true);
    setPlanInfoPayment({ ...state });
    setPlanInfo({ ...state });
    setStage(true);
    //apiCall()
  }


  const apiCall = (step) =>{}

  useEffect(() => {
    if (stage && stage == true) {
    setLoader(true);
    let data = {
      "profileStepCompleted": profileStepCompleted,
      "isProfileCompleted": isProfileCompleted,
      "profileDetail": {
        "basic": basic,
        "academicInfo": academicInfo,
        "collegePreference": collegePreference,
        "demographicPreference": demographicPreference,
        "employmentInfo": employmentInfo,
        "placementPreference": placementPreference,
        "finalPreference": finalPreference,
        "planInfo": planInfo,
        "planInfoPayment": planInfoPayment,
      }
    }

    setStage(false);
    profile(data, userData.id).then(({ data: { success, data, message } }) => {
      if (success !== true) {
        setLoader(false);
        return;
      } else {
        setLoader(false);
        //if (step) setKey(step)
        if (profileStepCompleted == 8 && isProfileCompleted == true) {
          var data = getLocalStorage('user');
          data.isProfileCompleted = true
          setLocalStorage('user', data);
          props.history.push({
            pathname: 'successwizard',
            state: { planType: planType },
          });
        }

        var user = getLocalStorage('user');
        if (user && user.isProfileCompleted && user.isProfileCompleted == true) {
          externalApiCall(data.user)
        }else if(profileStepCompleted == 7){
          externalApiCall(data.user)
        }

      }
    })
      .catch((error) => {
        setStage(false);
        setLoader(false);
        return;
      });
   }
  }, [stage]);

  const stepSelect = (key) => {
    window.scrollTo(0, 0);
    if (key == 'final') {
      props.history.push({ pathname: 'dashboard' });
    } else {
      setKey(key)
    }
  }


  // external API call
  const externalApiCall = (user) => {
    var params = {
      academicInfo: academicInfo,
      basic: basic,
      collegePreference: collegePreference,
      demographicPreference: demographicPreference,
      employmentInfo: employmentInfo,
      finalPreference: finalPreference,
      placementPreference: placementPreference,
      studentId: user.id ? user.id : user,
      page: 1,
      limit: 10,
      states: '',
      cities: '',
      controls: '',
      iclevels: '',
      codevalue: '',
      sort: 'ADM_PROB',
      isUpdate: true
    };
    dashboard(params)
      .then((res) => {
        setLoader(false);
      })
      .catch((err) => {
        setLoader(false);
      });
  }

  const manageSteps = (key) => {
    window.scrollTo(0, 0);
    if (key == "first" && Object.keys(basic).length != 0) {
      setKey(key);
    }
    if (key == "second" && Object.keys(academicInfo).length != 0) {
      setKey(key)
    }
    if (key == "third" && Object.keys(collegePreference).length != 0) {
      setKey(key)
    }
    if (key == "fourth" && Object.keys(demographicPreference).length != 0) {
      setKey(key)
    }
    if (key == "fivth" && Object.keys(employmentInfo).length != 0) {
      setKey(key)
    }

    if (key == "sixth" && Object.keys(placementPreference).length != 0) {
      setKey(key)
    }

    if (key == "seven" && Object.keys(finalPreference).length != 0) {
      setKey(key)
    }

    if (key == "eight" && ((Object.keys(finalPreference).length > 0) || (Object.keys(planInfo).length != 0))) {
      setKey(key)
    }

  }



  return (
    <div className="wizardLayout">
      <div className="container-fluid pl-0">
        <Tab.Container
          id="uncontrolled-tab-example"
          //defaultActiveKey="fourth"                    
          activeKey={key}
          onSelect={(k) => manageSteps(k)}
        >
          <Row>
            <Col sm={3}>

              <div className="sidebarBox d-flex align-items-center justify-content-center text-center">
                <Nav variant="pills" className="flex-column innerBox">
                  <Nav.Item onClick={() => logoutCall()}> 
                    <Nav.Link eventKey="profile">
                      <span className="titles text-uppercase fs16 fw600 d-block mt-3 col3">Logout</span>
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item className="pb-1 mb-2">
                    <Nav.Link eventKey="profile" disabled>    
                      <span className="profileTitl1 text-uppercase fs16 fw600 d-block col3">Profile</span>
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item className="pb-1">
                    <Nav.Link eventKey="first" className="listBox">
                      <span className="circleBox text-uppercase d-block m-auto col3 fs24 br50 mb-4 fw600">1</span>
                      <span className="titles text-uppercase fs15 fw600 d-block mt-3">Basic info</span>
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item className="pb-1">
                    <Nav.Link eventKey="second" className="listBox">
                      <span className="circleBox text-uppercase d-block m-auto col3 fs24 br50 mb-4 fw600">2</span>
                      <span className="titles text-uppercase fs15 fw600 d-block mt-3">Academic Info</span>
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item className="pb-1">
                    <Nav.Link eventKey="third" className="listBox">
                      <span className="circleBox text-uppercase d-block m-auto col3 fs24 br50 mb-4 fw600">3</span>
                      <span className="titles text-uppercase fs15 fw600 d-block mt-3">College Preferences</span>
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item className="pb-1">
                    <Nav.Link eventKey="fourth" className="listBox">
                      <span className="circleBox text-uppercase d-block m-auto col3 fs24 br50 mb-4 fw600">4</span>
                      <span className="titles text-uppercase fs15 fw600 d-block mt-3">
                        Demographic Preferences
                      </span>
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item className="pb-3">
                    <Nav.Link eventKey="fivth" className="listBox">
                      <span className="circleBox text-uppercase d-block m-auto col3 fs24 br50 mb-4 fw600">5</span>
                      <span className="titles text-uppercase fs15 fw600 d-block mt-3">Employment Info</span>
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item className="pb-3">
                    <Nav.Link eventKey="sixth" className="listBox">
                      <span className="circleBox text-uppercase d-block m-auto col3 fs24 br50 mb-4 fw600">6</span>
                      <span className="titles text-uppercase fs15 fw600 d-block mt-3">Placement Preferences</span>
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item className="pb-3">
                    <Nav.Link eventKey="seven" className="listBox">
                      <span className="circleBox text-uppercase d-block m-auto col3 fs24 br50 mb-4 fw600">7</span>
                      <span className="titles text-uppercase fs15 fw600 d-block mt-3">Financial Preferences</span>
                    </Nav.Link>
                  </Nav.Item>

                  <Nav.Item className="pb-3">
                    <Nav.Link eventKey="eight" className="listBox">
                      <span className="circleBox text-uppercase d-block m-auto col3 fs24 br50 mb-4 fw600">8</span>
                      <span className="titles text-uppercase fs15 fw600 d-block mt-3">Select Plan</span>
                    </Nav.Link>
                  </Nav.Item>
                </Nav>
                <div className="navBarBottom">
                  <div className="w-100 d-flex">
                    <div>
                      <Image src={navComments} alt="Comments" className="comment1" />
                    </div>
                    <div>
                      <div className="col3 fs13 fw500 mb-1">If you have any queries, email us at</div>
                      <a className="col1 fs13 fw500 mb-1" href={`mailto:${"info@hiedsuccess.com"}`}>
                        info@hiedsuccess.com 
                      </a>
                    </div>
                  </div>
                </div>
              </div>

            </Col>
            <Col sm={9}>
              <Tab.Content>
                <Tab.Pane eventKey="first" >
                  <FirstStep
                    stepSubmit={stepSubmit}
                    data={profileData ? profileData.basic : {}}
                    stepSelect={stepSelect}
                  />
                </Tab.Pane>

                <Tab.Pane eventKey="second">
                  <SecondStep
                    stepSubmitSecond={stepSubmitSecond}
                    data={profileData ? profileData.academicInfo : {}}
                    stepSelect={stepSelect}
                  />
                </Tab.Pane>

                <Tab.Pane eventKey="third" >
                  <ThirdStep
                    stepSubmitThird={stepSubmitThird}
                    data={profileData ? profileData.collegePreference : ''}
                    stepSelect={stepSelect}
                  />
                </Tab.Pane>

                <Tab.Pane eventKey="fourth">
                  <FourStep
                    stepSubmitFour={stepSubmitFour}
                    data={profileData ? profileData.demographicPreference : {}}
                    firstStepData={profileData ? profileData.basic : {}}
                    stepSelect={stepSelect}
                  />
                </Tab.Pane>

                <Tab.Pane eventKey="fivth">
                  <FiveStep
                    stepSubmitFive={stepSubmitFive}
                    data={profileData ? profileData.employmentInfo : {}}
                    stepSelect={stepSelect}
                  />
                </Tab.Pane>

                <Tab.Pane eventKey="sixth">
                  <SixStep
                    stepSubmitSix={stepSubmitSix}
                    data={profileData ? profileData.placementPreference : {}}
                    stepSelect={stepSelect}
                  />
                </Tab.Pane>

                <Tab.Pane eventKey="seven">
                  <SevenStep
                    stepSubmitSeven={stepSubmitSeven}
                    data={profileData ? profileData.finalPreference : {}}
                    firstStepData={profileData ? profileData.basic : {}}
                    stepSelect={stepSelect}
                  />
                </Tab.Pane>

                <Tab.Pane eventKey="eight">
                  <EightStep
                    stepSubmitEight={stepSubmitEight}
                    stepSubmitEightAfterPayment={stepSubmitEightAfterPayment}
                    data={profileData ? profileData.planInfo : {}}
                    Paymentdata={profileData ? profileData.planInfoPayment : {}}
                    stepSelect={stepSelect}
                    planData={planData}
                    featureData={featureData}
                    selectedPlanData={profileData?.user?.plan ? profileData.user.plan : {}}
                    setLoaderManual={setLoaderManual}
                    planChange={planChange}
                  />
                </Tab.Pane>

              </Tab.Content>
            </Col>
          </Row>
        </Tab.Container>
      </div>
      <div className="bottomFooter d-flex align-items-center bgCol43">
        <div className="container-fluid">
          <Row>
            <Col md={12}>
              <div className="d-flex align-items-center">
                <Image src={footerLogo} alt="footer-logo" className="fLogo" />
                <div className="col42 fs13 ml-4">Copyright © {new Date().getFullYear()} HiEd Success | Powered by HiEd Success</div>
              </div>
            </Col>
          </Row>
        </div>
      </div>

      {/* <Modal
        show={loader}
        //onHide={loader}
        centered
        className="loaderModal"
      >
        <Loader />
      </Modal> */}

      <Modal show={loader} backdrop="static" centered className="loaderModal">
        <Loader />
      </Modal>
    </div>
  )
}

export default Wizard;




