import React, { useState, useEffect } from 'react';
import { Col, Row, Container, Image, Form, Button } from 'react-bootstrap';

import { useHistory } from 'react-router-dom';

import UserCircleOne from '../../../../assets/images/svg/CheckCircle.svg';
import footerLogo from '../../../../assets/images/footerlogo.png';

import { getLocalStorage } from '../../../../Lib/Utils';


const SuccessWizard = (props) => {

  const Userdata = getLocalStorage('user');
  const [planTypeNew, setPlanTypeNew] = useState('');
  const history = useHistory();

  useEffect(() => {
    setTimeout(() => {
      props.history.push({ pathname: 'dashboard' });
    }, 20000)
  }, []);

  const goToDashboard = () => {
    props.history.push({ pathname: 'dashboard' });
  }

  useEffect(() => {
    let planType = history &&
      history.location &&
      history.location.state &&
      history.location.state.planType
      ? history.location.state.planType
      : false;
    setPlanTypeNew(planType);

  }, []);




  return (
    <div className="bgBanner d-flex align-items-center justify-content-center w-100">
      <Container>
        <Col lg={7} md={7} className="m-auto">
          <div className="studentBox successBox1 d-flex align-items-center justify-content-center text-center bgCol3 shadow1 br10">
            <div style={{ padding: 15 }}>
              <div className="text-center mb-3">
                <Image src={UserCircleOne} alt="Icon" className="" />
              </div>
              {
                getLocalStorage("ad_service") && getLocalStorage("ad_service") == true ? 
                <div className="fs22 fw600 col2 mb-1">
                  {/* Congratulations {Userdata.firstName + ' ' + Userdata.lastName}! You have successfully upgraded your subscription with the following: */}
                  Your HiEd Harmony subscription plan is now upgraded to
                </div> 
                :
                <div className="fs22 fw600 col2 mb-1">
                  Congratulations {Userdata.firstName + ' ' + Userdata.lastName}! You successfully completed
                  your HiEd Harmony student profile.
                </div>
              }

              {
              getLocalStorage("ad_service") && getLocalStorage("ad_service") == true ?
                <div className="fw500 col5 mb-4 pb-2" style={{textAlign: 'left', marginLeft: 100, marginTop: 20}}>
                  {
                    history && history.location && history.location.state &&
                    history.location.state.services &&
                    history.location.state.services.map((item, i) =>{
                      return <div key={i}>{i + 1}. {item}</div>
                    })
                  }
                </div>
              :

              history && history.location && history.location.state &&
                history.location.state.planType == 'free' ?
                <div className="fw500 col5 mb-4 pb-2">
                  Thank you for becoming part of HiEd Harmony student portal. Please check your registered email-id to schedule your pre-counseling session.
                </div>
                :
                <div className="fw500 col5 mb-4 pb-2">
                  Thank you for becoming part of HiEd Harmony student portal. You are now eligible for HiEd Ambassador Program. Click on <a className="fw700 col1 pointer" href="https://hiedsuccess.com/hied-ambassador/" target="_blank" rel="noopener noreferrer">https://hiedsuccess.com/hied-ambassador/</a> for more details. Please check your registered email-id to schedule your 1-1 Career Counseling session with Preeti Tanwar.
                </div>

              }

              {
                getLocalStorage("ad_service") && getLocalStorage("ad_service") == true ? '' :
                history && history.location && history.location.state &&
                history.location.state.planType == 'free' ?
                <a className="fw700 col1 pointer" href="https://calendly.com/info-32975" target="_blank" rel="noopener noreferrer">Click Here</a>
                :
                <a className="fw700 col1 pointer" href="https://calendly.com/preetitanwar/counseling?month=2022-01" target="_blank" rel="noopener noreferrer">Click Here</a>
              }

              
            </div>

          </div>
          <div className="bottomFooter d-flex align-items-center bgCol43">
            <div className="container-fluid">
              <Row>
                <Col md={12}>
                  <div className="d-flex align-items-center">
                    <Image src={footerLogo} alt="footer-logo" className="fLogo" />
                    <div className="col42 fs13 ml-4">Copyright © {new Date().getFullYear()} HiEd Success | Powered by HiEd Success</div>
                  </div>
                </Col>
              </Row>
            </div>
          </div>
        </Col>

      </Container>

    </div>
  )
}

export default SuccessWizard;

