import React, { useState, useEffect } from 'react';
import { Col, Row, Image, Button, Form, Nav, Tab, Accordion, Card } from 'react-bootstrap';
import { Slider, RangeSlider } from 'rsuite';
import 'rsuite/dist/styles/rsuite-default.css';
import Select from 'react-select';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'chocolate2', label: 'Chocolate2' },
  { value: 'strawberry2', label: 'Strawberry2' },
  { value: 'vanilla2', label: 'Vanilla2' },
  { value: 'chocolat3', label: 'Chocolate3' },
  { value: 'strawberr3', label: 'Strawberry3' },
  { value: 'vanilla3', label: 'Vanilla3' }
]

const FormInput = (props) => {

  const handleMultiSelect = (getValue) => {
    console.log('rks getValue', getValue)
  }  

  const multiDefault = (code) => {      
    let options = [];
    code && code.map((data)=>{
      options.push({value:data, label:data});
    }) 
    return options;
  }  

  

  switch (props.inputData.type) {
    case 'text':
      return (
        <Form.Group
          controlId={`formBasicName${props.inputData.code}`}
          key={`formBasicName${props.inputData.code}`}
          className={`${props.inputData.NotEditable && props.isProfileCompleted ? 'contentDisabledNew' : ''}`}
        >
          <Form.Control
            type="text"
            name={props.inputData.code}
            value={props.setData[props.inputData.code] || ''}
            placeholder={props.inputData.placeholder}
            className="inputType1"
            onChange={props.onchangeInput}
          />
          {
            props.error &&
            <span className="help-block error-text">
              <span style={{ color: "red", fontSize: 13 }}>
                {props.errorMessage}
              </span>
            </span>
          }
        </Form.Group>
      )

    case 'number':
      return (
        <Form.Group controlId={`formBasicName${props.inputData.code}`}
          key={`formBasicName${props.inputData.code}`}
          className={`${props.inputData.NotEditable && props.isProfileCompleted ? 'contentDisabledNew' : ''}`}
        >
          <Form.Control
            style={props.inputData.code == "CURR_ENROLL_ZIP" ? {} : { width: '82%' }}
            type={"text"}
            name={props.inputData.code}
            value={props.setData[props.inputData.code] || ''}
            placeholder={props.inputData.placeholder}
            className="inputType1 quantity"
            // onChange={props.onchangeInput}
            onChange={(e) =>
              props.inputData.code == "MARKSSCORED" && props.setData["GRADINGSYSTEM"] == "Lettergrade (A/A+)" ?
                (e.target.value == "A" || e.target.value == "A+" || e.target.value == "A++" ||
                  e.target.value == "B" || e.target.value == "B+" || e.target.value == "B++" ||
                  e.target.value == "C" || e.target.value == "C+" || e.target.value == "C++" ||
                  e.target.value == "D" || e.target.value == "D+" || e.target.value == "D++" ||
                  e.target.value == "E" || e.target.value == "") &&
                props.onchangeInput(e)
                :
                props.inputData.code == "MARKSSCORED" && props.setData["GRADINGSYSTEM"] == "CGPA (10)" ? e.target.value <= 10 && props.onchangeInput(e)
                  :
                  props.inputData.code == "MARKSSCORED" && props.setData["GRADINGSYSTEM"] == "Weighted (out of 5)" ? e.target.value <= 5 && props.onchangeInput(e)
                    :
                    props.inputData.code == "MARKSSCORED" && props.setData["GRADINGSYSTEM"] == "Unweighted (out of 4)" ? e.target.value <= 4 && props.onchangeInput(e)
                      :
                      props.inputData.code == "MARKSSCORED" && props.setData["GRADINGSYSTEM"] == "Iranian system (out of 20)" ? e.target.value <= 20 && props.onchangeInput(e)
                        :
                        props.inputData.code == "MARKSSCORED" && props.setData["GRADINGSYSTEM"] == "Percentage (100)" ? e.target.value <= 100 && props.onchangeInput(e)
                          :
                          (props.inputData.code == "CURR_ENROLL_ZIP" || props.inputData.code == "HIGH_SCHL_ZIP" || props.inputData.code == "BECH_ZIP") ? e.target.value.length <= 6 &&
                            e.target.value.match('^[+0-9 ]*$') != null &&
                            props.onchangeInput(e)
                            :


                            e.target.value.match('^[+0-9 ]*$') != null ?
                              props.onchangeInput(e)
                              : ''
            }
          />
          {
            props.error &&
            <span className="help-block error-text">
              <span style={{ color: "red", fontSize: 13 }}>
                {props.errorMessage}
              </span>
            </span>
          }
        </Form.Group>
      )

    case 'dropDown':
      return (
        <>
          <Form.Group controlId={`formBasicName${props.inputData.code}`}
            key={`formBasicName${props.inputData.code}`}
            className={`${props.inputData.NotEditable && props.isProfileCompleted ? 'contentDisabledNew' : ''}`}
          >
            <Form.Control as="select"
              className="selectTyp1 pointer"
              name={props.inputData.code}
              value={props.setData[props.inputData.code] || ''}
              onChange={props.onchangeInput}
            //disabled={props.inputData.NotEditable}
            >
              {(props.inputData.code == 'HIGH_SCHL_COUNTRY' || props.inputData.code == 'BECH_COUNTRY' ||
                props.inputData.code == 'HIGH_SCHL_STATE' || props.inputData.code == 'BECH_STATE' || props.inputData.code == 'HIGH_SCHL_CITY' || props.inputData.code == 'BECH_CITY')
                ?
                <option value="">{props.inputData.text}</option>
                :
                <option value="">Select</option>
              }
              {
                props && props.listOptions &&
                props.listOptions.map((item, index) => {
                  return <option key={index} value={item.name ? item.name : item.major ? item.major : item.value}>{item.name ? item.name : item.major ? item.major : item.value}</option>
                })
              }
            </Form.Control>
            {
              props.error &&
              <span className="help-block error-text">
                <span style={{ color: "red", fontSize: 13 }}>
                  {props.errorMessage}
                </span>
              </span>
            }
          </Form.Group>
        </>
      )

    case 'multi_dropDown':
      return (
        <>
          <Form.Group controlId="exampleForm.ControlSelect1">
            {console.log('ssssss',props.setData, props.inputData.code, props.setData[props.inputData.code])}
            <Select
              //noOptionsMessage={() => null}
              //defaultValue={props.setData && props.setData[props.inputData.code]}
              //defaultValue={props.setData && props.setData[props.inputData.code+'_MULTI']}
              value={multiDefault(props.setData && props.setData[props.inputData.code])}  
              defaultValue={multiDefault(props.setData && props.setData[props.inputData.code])}      
              isMulti
              name="colors"
              options={props.listOptions && props.listOptions.length > 0 ? props.listOptions : []}
              //onChange={handleMultiSelect}
              className="basic-multi-select selectTyp2"
              classNamePrefix="select"
              placeholder="Select"
              isSearchable={true}
              onChange={(value) => props.handleMultiSelect(props.inputData.code, value)}              
            />
             {
              props.error &&
              <span className="help-block error-text">
                <span style={{ color: "red", fontSize: 13 }}>
                  {props.errorMessage}
                </span>
              </span>
            }
          </Form.Group>

        </>
      )


    case 'radio':
      return (
        <Form.Group controlId={`formBasicName${props.inputData.code}`} key={`formBasicName${props.inputData.code}`}
          className={`${props.inputData.NotEditable && props.isProfileCompleted ? 'contentDisabledNew' : ''}`}
        >
          {
            props.inputData.options.map((item, index) => {
              return <Form.Check
                key={index}
                type="radio"
                label={item.text}
                className={props.setData[props.inputData.code] == item.value ? "radioType1 btm-margin actives" : "radioType1 btm-margin"}
                name={props.inputData.code} value={item.value}
                checked={props.setData[props.inputData.code] == item.text}
                onChange={props.onchangeInput}
              />
            })
          }
          {
            props.error &&
            <span className="help-block error-text">
              <span style={{ color: "red", fontSize: 13 }}>
                {props.errorMessage}
              </span>
            </span>
          }
        </Form.Group>
      )

    case 'checkbox':
      return (
        <>
          <div className={`tagType1 d-flex flex-wrap mb-3 ${props.inputData.NotEditable && props.isProfileCompleted ? 'contentDisabledNew' : ''}`}
          >
            {
              props.inputData.options && props.inputData.options.length ?
                props.inputData.options.map((item, index) => {
                  return <span key={index} className={`br8 col4 fw500 mr-3 ${props.setData[props.inputData.code] && props.setData[props.inputData.code].indexOf(item.value) > -1 ? 'bgCol35' : ''}  `}
                    name={props.inputData.code}
                    onClick={() => props.oncheckBox(props.inputData.code, item.value)}
                    value={item.value ? item.value : item.name ? item.name : ''}>
                    {item.value ? item.value : item.name ? item.name : ''}
                  </span>
                })
                :
                props.listOptions && props.listOptions.length > 0 &&
                props.listOptions.map((item, index) => {
                  return <span key={index} className={`br8 col4 fw500 mr-3 ${props.setData[props.inputData.code] && props.setData[props.inputData.code].indexOf(item.name) > -1 ? 'bgCol35' : ''}  `}
                    name={props.inputData.code}
                    onClick={() => props.oncheckBox(props.inputData.code, item.name)}
                    value={item.value ? item.value : item.name ? item.name : ''}>
                    {item.value ? item.value : item.name ? item.name : ''}
                  </span>
                })
            }

            {
              props.error &&
              <span className="help-block error-text">
                <span style={{ color: "red", fontSize: 13 }}>
                  {props.errorMessage}
                </span>
              </span>
            }
          </div>
        </>
      )

    case 'textArea':
      return (
        <Form.Group controlId={`formBasicName${props.inputData.code}`} key={`formBasicName${props.inputData.code}`}
          className={`${props.inputData.NotEditable && props.isProfileCompleted ? 'contentDisabledNew' : ''}`}
        >
          <Form.Control
            as="textarea"
            rows={3}
            name={props.inputData.code}
            value={props.setData[props.inputData.code] || ''}
            placeholder={props.inputData.placeholder}
            className="inputType1"
            onChange={props.onchangeInput}
          />
          {
            props.error &&
            <span className="help-block error-text">
              <span style={{ color: "red", fontSize: 13 }}>
                {props.errorMessage}
              </span>
            </span>
          }
        </Form.Group>
      )

    case 'slider':
      return (
        <>
          {
            props.inputData.questions && props.inputData.questions.map((item, index) => (
              <Form.Group controlId="formBasicRange" key={index}
                className={`${props.inputData.NotEditable && props.isProfileCompleted ? 'contentDisabledNew' : ''}`}
              >
                {
                  <Form.Label className="fw600 col2 mb-3">
                    <span className="mr-3">{item.text}</span>
                  </Form.Label>
                }

                <Slider name={item.code} onChange={(e) => props.onchangeInput(e, item.code, "slider")} defaultValue={2} value={props.setData[item.code] || item.minRange} min={item.minRange} step={item.interval ? item.interval : 1} max={item.maxRange} graduated progress
                  renderMark={mark => {
                    return mark;
                  }}
                />
                {
                  props.error &&
                  <span className="help-block error-text">
                    <span style={{ color: "red", fontSize: 13 }}>
                      {props.errorMessage}
                    </span>
                  </span>
                }
                <Form.Label className="fw600 col2 mb-3"></Form.Label>
              </Form.Group>
            ))
          }

        </>

      )

    case 'date':
      return (
        <Form.Group controlId={`formBasicName${props.inputData.code}`} key={`formBasicName${props.inputData.code}`}
          className={`${props.inputData.NotEditable && props.isProfileCompleted ? 'contentDisabledNew' : ''}`}
        >
          <DatePicker
            selected={props.setData[props.inputData.code] ? props.setData[props.inputData.code] : ''}
            name={props.inputData.code}
            value={props.setData[props.inputData.code] || ''}
            className="inputType1 w-100"

            maxDate={props.inputData.code == "YOFCOMMENCEMENT" && props.setData && props.setData["YOCOMPLETION"] ? new Date(props.setData["YOCOMPLETION"]) : new Date().setFullYear(props.inputData.maxValue)}

            minDate={props.inputData.code == "YOCOMPLETION" && props.setData && props.setData["YOFCOMMENCEMENT"] ? new Date(props.setData["YOFCOMMENCEMENT"]) : new Date().setFullYear(props.inputData.minValue)}
            placeholderText="YYYY format"
            onChange={(e) => props.onchangeInput(e, props.inputData.code)}
            dateFormat="yyyy"
            autoComplete="off"
            showYearPicker
            dropdownMode="select"
          //locale="es"
          />
          {
            props.error &&
            <span className="help-block error-text">
              <span style={{ color: "red", fontSize: 13 }}>
                {props.errorMessage}
              </span>
            </span>
          }
        </Form.Group>
      )


    case 'fullDate':
      return (
        <Form.Group controlId={`formBasicName${props.inputData.code}`} key={`formBasicName${props.inputData.code}`}
          className={`${props.inputData.NotEditable && props.isProfileCompleted ? 'contentDisabledNew' : ''}`}
        >
          <DatePicker
            selected={props.setData[props.inputData.code] ? props.setData[props.inputData.code] : ''}
            name={props.inputData.code}
            value={props.setData[props.inputData.code] || ''}
            className="inputType1 w-100"
            //maxDate={new Date()}

            maxDate={props.inputData.code == "CURR_ENROLL_START_DATE" && props.setData && props.setData["CURR_ENROLL_LAST_DATE"] ? new Date(props.setData["CURR_ENROLL_LAST_DATE"]) : new Date()}

            minDate={props.inputData.code == "CURR_ENROLL_LAST_DATE" && props.setData && props.setData["CURR_ENROLL_START_DATE"] ? new Date(props.setData["CURR_ENROLL_START_DATE"]) : new Date(1965)}

            placeholderText="MM/DD/YYYY"
            onChange={(e) => props.onchangeInput(e, props.inputData.code)}
            dateFormat="MM/dd/yyyy"
            autoComplete="off"
            peekNextMonth
            showMonthDropdown
            showYearDropdown
            dropdownMode="select"
          //locale="es"
          />
          {
            props.error &&
            <span className="help-block error-text">
              <span style={{ color: "red", fontSize: 13 }}>
                {props.errorMessage}
              </span>
            </span>
          }
        </Form.Group>
      )


    default:
      return '';
  }

}


export default FormInput;





