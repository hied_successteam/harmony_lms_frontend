import React, { useState, useEffect } from 'react';
import { Col, Row, Button, Image, Table, Form } from 'react-bootstrap';
import _ from "lodash";
import 'rsuite/dist/styles/rsuite-default.css';
import FormInput from './FormInput'
import Nested_FormInput from './Nested_FormInput'
import { AcademicInfo } from '../WizardQuestion'
import { major, countryList, stateList, cityList } from '../../../../../Actions/Student/register';
import { getLocalStorage } from '../../../../../Lib/Utils';
import infoicon from "../../../../../assets/images/svg/questions.svg";
import TipModal from '../../../TipModal/TipModal'
import moment from 'moment';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";



const SecondStep = (props) => {

  const [state, setState] = useState({});
  const [errors, setErrors] = useState({});
  const [validMsg, setValidMsg] = useState('')
  const [dateValidMsgF, setDateValidMsgF] = useState('')
  const [dateValidMsgS, setDateValidMsgS] = useState('')
  const [spicificMajor, setSpicificMajor] = useState([]);
  const [spicificMajorDrop, setSpicificMajorDrop] = useState([]);
  const [satReqMsg, setSatReqMsg] = useState('');
  const [countryDatas, setCountryDatas] = useState('');
  const [stateDatas, setStateDatas] = useState([]);
  const [cityDatas, setCityDatas] = useState([]);
  const [enrStateDatas, setEnrStateDatas] = useState([]);
  const [enrCityDatas, setEnrCityDatas] = useState([]);
  const [matrixLen, setMatrixLen] = useState([1, 2, 3, 4, 5]);
  const [matrixRowCount, setMatrixRowCount] = useState(1);

  const [tipsData, setTipsData] = useState(false);
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);

  const handleShow = (tipsData) => {
    setTipsData(tipsData);
    setShow(true);
  }

  useEffect(() => {
    setErrors({})
  }, [props])


  const onchangeInput = (event, code, type) => {
    setSatReqMsg('')
    if (code) {
      // setState({ ...state, [code]: event });
      if (type && type == "slider") {
        setState({ ...state, [code]: event });
        errors[[code]] = false
      } else {
        if (event) {
          setState({ ...state, [code]: event.getTime() });
        } else {
          setState({ ...state, [code]: 0 });
        }
      }
    } else {
      const { target: { name, value } } = event;
      errors[[name]] = false


      var reading = name == "TOEFL_READING" ? parseFloat(value) : parseFloat(state.TOEFL_READING)
      var writing = name == "TOEFL_WRITING" ? parseFloat(value) : parseFloat(state.TOEFL_WRITING)
      var listing = name == "TOEFL_LISTENING" ? parseFloat(value) : parseFloat(state.TOEFL_LISTENING)
      var speaking = name == "TOEFL_SPEAKING" ? parseFloat(value) : parseFloat(state.TOEFL_SPEAKING)
      var TOEFL_TOTAL = reading || writing || listing || speaking ? reading + writing + listing + speaking : reading == 0 && writing == 0 && listing == 0 && speaking == 0 && '0'

      // var quant = name == "GRE_QUANTITATIVE" ? parseFloat(value) : parseFloat(state.GRE_QUANTITATIVE)
      // var verbal = name == "GRE_VERBAL" ? parseFloat(value) : parseFloat(state.GRE_VERBAL)
      // var analy = ''

      // if(name == "GRE_ANALYTICAL"){
      //   var pointings = value.split('.', 2)
      //   if (pointings && pointings.length > 1) {
      //     if (pointings[1] == '') {
      //       analy = parseFloat(value)
      //     } else {
      //       analy = (Math.round(value / 0.5) * 0.5 == 0 ? '0' : Math.round(value / 0.5) * 0.5)
      //     }
      //   } else {
      //     analy = parseFloat(value)
      //   }
      // }else{
      //   analy = parseFloat(state.GRE_ANALYTICAL)
      // }
      
      // var GRE_TOTAL = quant || verbal || analy ? quant + verbal + parseFloat(analy) : quant == 0 && verbal == 0 && analy == 0 && '0'

      var IE_reading = name == "IELTS_LISTENING" ? parseFloat(value) : parseFloat(state.IELTS_LISTENING)
      var IE_writing = name == "IELTS_READING" ? parseFloat(value) : parseFloat(state.IELTS_READING)
      var IE_listing = name == "IELTS_WRITING" ? parseFloat(value) : parseFloat(state.IELTS_WRITING)
      var IE_speaking = name == "IELTS_SPEAKING" ? parseFloat(value) : parseFloat(state.IELTS_SPEAKING)
      var IELTS_BANDSCORE = IE_reading || IE_writing || IE_listing || IE_speaking ? Math.round(((IE_reading + IE_writing + IE_listing + IE_speaking) / 4) / 0.5) * 0.5 : IE_reading == 0 && IE_writing == 0 && IE_listing == 0 && IE_speaking == 0 && '0'


      var sat_ebrw = name == "SAT_EBRW" ? parseFloat(value) : parseFloat(state.SAT_EBRW)
      var sat_math = name == "SAT_MATH" ? parseFloat(value) : parseFloat(state.SAT_MATH)
      var SAT_TOTAL = sat_ebrw || sat_math ? (Math.round(sat_ebrw / 10) * 10) + (Math.round(sat_math / 10) * 10) : sat_ebrw == 0 && sat_math == 0 && '0'

      var ps_ebrw = name == "PSAT_NMSQT_EBRW" ? parseFloat(value) : parseFloat(state.PSAT_NMSQT_EBRW)
      var ps_math = name == "PSAT_NMSQT_MATH" ? parseFloat(value) : parseFloat(state.PSAT_NMSQT_MATH)
      var PSAT_NMSQT_TOTAL = ps_ebrw || ps_math ? ps_ebrw + ps_math : ps_ebrw == 0 && ps_math == 0 && '0'


      if (name == "HIGHDEGEDU") {
        setState({ ...state, [name]: value, TOEFL_TOTAL, SAT_TOTAL, PSAT_NMSQT_TOTAL, IELTS_BANDSCORE, });

        setTimeout(function () {
          refreshData(name, value)
        }.bind(this), 200);
      } else if (name == "GRADINGSYSTEM") {
        setState({ ...state, [name]: value, TOEFL_TOTAL, SAT_TOTAL, PSAT_NMSQT_TOTAL, IELTS_BANDSCORE, MARKSSCORED: '' });
      } else if (name == "MARKSSCORED" && state && state["GRADINGSYSTEM"] && (state["GRADINGSYSTEM"] !== "Lettergrade (A/A+)")) {
        var pointing = value.split('.', 4)

        if (pointing[1] && pointing[1].length > 4) {
          return
        } else {
          setState({ ...state, [name]: value, TOEFL_TOTAL, SAT_TOTAL, PSAT_NMSQT_TOTAL, IELTS_BANDSCORE, });
        }

      } else if (name == "STUDYFIELD") {
        errors[["MOJOR_PURSUE"]] = false
        if (state && state["MOJOR_PURSUE"]) {
          delete state["MOJOR_PURSUE"]
          setState({ ...state, [name]: value, TOEFL_TOTAL, SAT_TOTAL, PSAT_NMSQT_TOTAL, IELTS_BANDSCORE, });
          specificMajor(name, value);
        } else {
          setState({ ...state, [name]: value, TOEFL_TOTAL, SAT_TOTAL, PSAT_NMSQT_TOTAL, IELTS_BANDSCORE, });
          specificMajor(name, value);
        }

      } else if (name == "BACH_GMAT_INTEGRATEDREASONING" || name == "BACH_GMAT_ANALYTICAL" || name == "IELTS_LISTENING" || name == "IELTS_READING" || name == "IELTS_WRITING" || name == "IELTS_SPEAKING" || name == "GRE_ANALYTICAL") {
        var pointing = value.split('.', 2)

        if (pointing && pointing.length > 1) {
          if (pointing[1] == '') {
            setState({ ...state, [name]: value, TOEFL_TOTAL, SAT_TOTAL, PSAT_NMSQT_TOTAL, IELTS_BANDSCORE, });
          } else {
            if(name == "GRE_ANALYTICAL"){
              setState({ ...state, [name]: (Math.round(value / 0.5) * 0.5 == 0 ? '0' : Math.round(value / 0.5) * 0.5), TOEFL_TOTAL, SAT_TOTAL, PSAT_NMSQT_TOTAL, IELTS_BANDSCORE, });
            }else{
              setState({ ...state, [name]: (Math.round(value / 0.5) * 0.5), TOEFL_TOTAL, SAT_TOTAL, PSAT_NMSQT_TOTAL, IELTS_BANDSCORE, });
            }
          }
        } else {
          setState({ ...state, [name]: value, TOEFL_TOTAL, SAT_TOTAL, PSAT_NMSQT_TOTAL, IELTS_BANDSCORE, });
        }
      } else if (name == "BACH_GMAT_Total" || name == "SAT_EBRW" || name == "SAT_MATH") {
        if (value.length > 2) {
          setState({ ...state, [name]: (Math.round(value / 10) * 10), TOEFL_TOTAL, SAT_TOTAL, PSAT_NMSQT_TOTAL, IELTS_BANDSCORE, });
        } else {
          setState({ ...state, [name]: value, TOEFL_TOTAL, SAT_TOTAL, PSAT_NMSQT_TOTAL, IELTS_BANDSCORE, });
        }
      } else if (name == "HIGH_SCHL_COUNTRY" || name == "BECH_COUNTRY") {
        var id = countryDatas && countryDatas.find((item) => item.name == value);
        if (id) {
          delete state["HIGH_SCHL_STATE"]
          delete state["HIGH_SCHL_CITY"]
          delete state["BECH_STATE"]
          delete state["BECH_CITY"]
          setState({ ...state });
          getStateByCountry(id.countryId);
        } else
          getStateByCountry('');
        setState({ ...state, [name]: value, });
      } else if (name == "HIGH_SCHL_STATE" || name == "BECH_STATE") {
        var id = stateDatas && stateDatas.find((item) => item.name == value);
        if (id) {
          delete state["HIGH_SCHL_CITY"]
          delete state["BECH_CITY"]
          setState({ ...state });
          getCityByState(id.stateId);
        }
        else
          getCityByState('');
        setState({ ...state, [name]: value });
      } else if (name == "CURR_ENROLL_COUNTRY") {
        var id = countryDatas && countryDatas.find((item) => item.name == value);
        if (id) {
          delete state["CURR_ENROLL_STATE"]
          delete state["CURR_ENROLL_CITY"]
          setState({ ...state });
          getENStateByCountry(id.countryId);
        } else
          getENStateByCountry('');
        setState({ ...state, [name]: value, });
      } else if (name == "CURR_ENROLL_STATE") {
        var id = enrStateDatas && enrStateDatas.find((item) => item.name == value);
        if (id) {
          delete state["CURR_ENROLL_CITY"]
          setState({ ...state });
          getENCityByState(id.stateId);
        }
        else
          getENCityByState('');
        setState({ ...state, [name]: value });
      } else {
        setState({ ...state, [name]: value, TOEFL_TOTAL, SAT_TOTAL, PSAT_NMSQT_TOTAL, IELTS_BANDSCORE, });
      }

      if (name == "STUDYFIELD") {
        if (state && state["MOJOR_PURSUE"]) {
          //delete state["MOJOR_PURSUE"]
          setSpicificMajor([])
          setState({ ...state, [name]: value, MOJOR_PURSUE: [] });
        }
      }
    }
  };



  useEffect(() => {
    if (state && state["STUDYFIELD"]) {
      setState({...state, MOJOR_PURSUE: []})
      specificMajor("STUDYFIELD", state["STUDYFIELD"])
    }
  }, [])

  const specificMajor = (name, value) => {
    let data = { "fieldOfStudy": value }
    major(data).then(({ data: { success, data, message } }) => {
      if (success !== true) {
        return;
      } else {

        //setSpicificMajor(data.rows);

        let skillOptions = data.rows.map(function (MajorObject) {
          return {
            value: MajorObject.name,
            label: MajorObject.name,
          };
        });
        setSpicificMajor(skillOptions);
        setSpicificMajorDrop(skillOptions);

      }
    })
      .catch((error) => {
        setSpicificMajor([

        ]);
      });
  }

  const refreshData = (name, value) => {
    AcademicInfo && AcademicInfo.map((item_1) => {
      if (item_1.code == "HIGHDEGEDU" && item_1.isLogical) {
        item_1.options && item_1.options.map((item_2) => {
          if (item_2.isLogical) {
            item_2.questions && item_2.questions.map((item_3) => {
              delete state[item_3.code]
              setState({ ...state, [name]: value });
              if (item_3.isLogical) {
                item_3.options && item_3.options.map((item_4) => {
                  if (item_4.isLogical) {
                    item_4.questions && item_4.questions.map((item_5) => {
                      delete state[item_5.code]
                      setState({ ...state, [name]: value });
                      item_5.options && item_5.options.map((item_6) => {
                        item_6.questions && item_6.questions.map((item_7) => {
                          delete state[item_7.code]
                          setState({ ...state, [name]: value });
                        })
                      })
                    })
                  }
                })
              }
            })
          }
        })
      }
    })
  }


  //select check box
  const oncheckBox = (name, value) => {
    if (state[name]) {
      let ArryVale = state[name];
      var indexB = ArryVale.indexOf(value);
      if (indexB > -1) {
        ArryVale.splice(indexB, 1);
        if (ArryVale && ArryVale.length <= 0) {
          delete state[name]
          setState({ ...state });
          manageBlockCheck('1', name, value)
        } else {
          setState({ ...state, [name]: [...ArryVale] });
          manageBlockCheck('2', name, value, ArryVale)
        }
      } else {
        ArryVale.push(value);
        setState({ ...state, [name]: [...ArryVale] });
        manageBlockCheck('3', name, value, ArryVale)
      }
    } else {
      setState({ ...state, [name]: [value] });
    }



    if (name == "ENTRANCEAPPEARED" && state && state["ENTRANCEAPPEARED"] && state["ENTRANCEAPPEARED"].indexOf("Not Applicable") > -1) {
      //setState({ ...state, ENTRANCEAPPEARED: ["Not Applicable"] });
      AcademicInfo && AcademicInfo.map((item_1) => {
        if (item_1.code == "HIGHDEGEDU" && item_1.isLogical) {
          item_1.options && item_1.options.map((item_2) => {
            if (item_2.isLogical) {
              item_2.questions && item_2.questions.map((item_3) => {
                if (item_3.isLogical) {
                  item_3.options && item_3.options.map((item_4) => {
                    if (item_4.isLogical) {
                      item_4.questions && item_4.questions.map((item_5) => {
                        delete state[item_5.code]
                        setState({ ...state, [name]: value, ENTRANCEAPPEARED: ["Not Applicable"] });
                        item_5.options && item_5.options.map((item_6) => {
                          item_6.questions && item_6.questions.map((item_7) => {
                            delete state[item_7.code]
                            setState({ ...state, [name]: value });
                          })
                        })
                      })
                    }
                  })
                }
              })
            }
          })
        }
      })
    }
  }

  const handleMultiSelect = (name, values) => {      
    //setState({ ...state, 'MOJOR_PURSUE_multi': values });   
    let ArryVale = [];
    values && values.map((item) => {
      ArryVale.push(item.value);
    })   
    //setState({ ...state, [name]: [...ArryVale], [name+'_MULTI']: values });  
    setState({ ...state, [name]: [...ArryVale] });      
  }


  const manageBlockCheck = (sta, name, value, ArryVale) => {
    if (name == "ENTRANCEAPPEARED") {
      AcademicInfo && AcademicInfo.map((item_1) => {
        if (item_1.code == "HIGHDEGEDU" && item_1.isLogical) {
          item_1.options && item_1.options.map((item_2) => {
            if (item_2.isLogical) {
              item_2.questions && item_2.questions.map((item_3) => {
                if (item_3.isLogical) {
                  item_3.options && item_3.options.map((item_4) => {
                    if (item_4.isLogical) {
                      if (state && state["ENTRANCEAPPEARED"] && state["ENTRANCEAPPEARED"].indexOf(item_4.text) > -1) {
                      } else {
                        item_4.questions && item_4.questions.map((item_5) => {
                          delete state[item_5.code]
                          if (sta == '1') {
                            setState({ ...state });
                          }
                          if (sta == '2') {
                            setState({ ...state, [name]: [...ArryVale] });
                          }
                          if (sta == '3') {
                            setState({ ...state, [name]: [...ArryVale] });
                          }
                          if (sta == '4') {
                            setState({ ...state, [name]: [value] });
                          }
                          item_5.options && item_5.options.map((item_6) => {
                            item_6.questions && item_6.questions.map((item_7) => {
                              delete state[item_7.code]
                              setState({ ...state, [name]: value });
                            })
                          })
                        })
                      }
                    }
                  })
                }
              })
            }
          })
        }
      })
    }
  }



  useEffect(() => {

    if (props.data && Object.keys(props.data).length != 0) {
      setState({
        ...state,
        ...props.data,
      });
    }
    if (props && props.data && props.data["TBLROWCOUNT"]) {
      setMatrixRowCount(props.data["TBLROWCOUNT"])
    }

    if (props && props.data && props.data["STUDYFIELD"]) {
      specificMajor("STUDYFIELD", props.data["STUDYFIELD"])
    }


  }, [props.data]);



  const stepSubmit = () => {
    let errors = {};
    let valid = true;
    //Check parent validation
    for (var m = 0; m < AcademicInfo.length; m++) {
      if (AcademicInfo[m].isRequired) {

        let codevalue = state[AcademicInfo[m].code];       

        if (!codevalue || codevalue == 'undefined' || codevalue == '') {
          if (AcademicInfo[m].code != "ADDRESSANDOTHERDETAIL") {
            valid = false;
            errors[AcademicInfo[m].code] = true;
          }
        }

        // Check child validation
        if (AcademicInfo[m].isLogical) {
          let que_Array = AcademicInfo[m].questions;
          que_Array && que_Array.length > 0 &&
            que_Array.map((que_item) => {
              if (que_item.isRequired) {
                let que_code = state[que_item.code];
                if (!que_code || que_code == 'undefined' || que_code == '') {
                  valid = false;
                  errors[que_item.code] = true;
                }
              }
            })
          let Newarry = AcademicInfo[m].options;
          if (Newarry && Newarry.length > 0) {
            for (var n = 0; n < Newarry.length; n++) {
              if (Newarry[n].isLogical) {
                let s_data = Newarry[n].questions
                s_data && s_data.map((item, ind) => {
                  if (state["RECOMMENDATION"] && item.code == "RECOMMENDATION_SELECT" && state["RECOMMENDATION"] !== "0" && item.isRequired) {
                    let rec_code = state[item.code];
                    if (!rec_code || rec_code == 'undefined' || rec_code == '') {
                      valid = false;
                      errors[item.code] = true;
                    }
                  }

                  if (spicificMajor && spicificMajor.length > 0 && state["MAJORPURSUE"] && item.code == "MOJOR_PURSUE" && state["MAJORPURSUE"] == "Yes" && item.isRequired) {
                    let rec_code = state[item.code];
                    if (!rec_code || rec_code == 'undefined' || rec_code == '') {
                      valid = false;
                      errors[item.code] = true;
                    }
                  }

                  if (state["NATIVELANGUAGE"] && (item.code == "OTHER_NATIVELANGUAGE" || item.code == "OTHER_FROFI") && state["NATIVELANGUAGE"] == "Other" && item.isRequired) {
                    let rec_code = state[item.code];
                    if (!rec_code || rec_code == 'undefined' || rec_code == '') {
                      valid = false;
                      errors[item.code] = true;
                    }
                  }

                  if (state["NATIVELANGUAGE"] && item.code == "ENGLISH_FROFI" && state["NATIVELANGUAGE"] == "English" && item.isRequired) {
                    let rec_code = state[item.code];
                    if (!rec_code || rec_code == 'undefined' || rec_code == '') {
                      valid = false;
                      errors[item.code] = true;
                    }
                  }

                })



                if (Newarry[n].value == state.HIGHDEGEDU) {
                  let newdata = Newarry[n].questions;
                  for (var i = 0; i < newdata.length; i++) {
                    if (newdata[i].isRequired) {
                      if ((newdata[i].code == "HIGH_SCHL_CITY" || newdata[i].code == "BECH_CITY") && cityDatas.length <= 0) {
                        errors["HIGH_SCHL_CITY"] = false
                        errors["BECH_CITY"] = false
                        setState({ ...state, HIGH_SCHL_CITY: "", BECH_CITY: "" });
                      } else {
                        let codevalueNew = state[newdata[i].code];
                        if (!codevalueNew || codevalueNew == 'undefined' || codevalueNew == '') {
                          valid = false;
                          errors[newdata[i].code] = true;
                        }

                        //Check nested validation
                        if (newdata[i].isLogical) {
                          let NestedArray = newdata[i].options;
                          for (var j = 0; j < NestedArray.length; j++) {
                            if (NestedArray[j].isLogical) {
                              //check rang for number
                              newdata[i].options && state && newdata[i].options.map((r_item) => {
                                r_item.questions && r_item.questions.map((s_item, j) => {
                                  if (state[s_item.code]) {
                                    if (s_item.minValue && s_item.maxValue && state[s_item.code] && state[s_item.code] < s_item.minValue || state[s_item.code] > s_item.maxValue) {
                                      let codeValid = state[s_item.code];
                                      if (codeValid) {
                                        valid = false;
                                        errors[s_item.code] = true;
                                        setValidMsg("Valid number required")
                                      }
                                    }
                                  } else {
                                    if (state && state[r_item.questions[0].code] == 'Taken') {
                                      let codeValid = state[s_item.code];
                                      if (codeValid) {
                                        valid = false;
                                        errors[s_item.code] = true;
                                      }
                                    }

                                  }

                                  if (j === 0) {
                                    s_item && s_item.options && s_item.options.length > 0 &&
                                      s_item.options.map((taken_item, taken_index) => (
                                        state[s_item.code] && state[s_item.code].length > 0 && state[s_item.code].indexOf(taken_item.value) > -1 &&
                                        taken_item.questions && taken_item.questions.length > 0 && taken_item.questions.map((date_item, date_index) => {
                                          let codevalueNew = state[date_item.code];
                                          if (!codevalueNew || codevalueNew == 'undefined' || codevalueNew == '') {
                                            valid = false;
                                            errors[date_item.code] = true;
                                          }
                                        })
                                      ))
                                  }

                                })
                              })
                              //checkRang(newdata[i], errors, valid) 
                              if (state && (state.ENTRANCEAPPEARED) && state["ENTRANCEAPPEARED"].indexOf(NestedArray[j].value) > -1) {
                                let DataArray = NestedArray[j].questions
                                for (var k = 0; k < DataArray.length; k++) {
                                  if (DataArray[k].isRequired) {
                                    let codevalueDatas = state[DataArray[k].code];
                                    if (!codevalueDatas || codevalueDatas == 'undefined' || codevalueDatas == '') {
                                      if (k > 0) {
                                        if (state && state[DataArray[0].code] == 'Taken') {
                                          valid = false;
                                          errors[DataArray[k].code] = true;
                                        }
                                      } else {
                                        valid = false;
                                        errors[DataArray[k].code] = true;
                                      }

                                    }
                                  }
                                }
                              }
                              //checkRang(newdata[i], errors, valid) 
                              if (state && (state.CURR_ENROLL_CLG) && state["CURR_ENROLL_CLG"] == "Yes" && newdata[i].code == "CURR_ENROLL_CLG") {
                                let DataArr = newdata[i]
                                DataArr && DataArr.options.map((d_item) => {
                                  if (d_item.text == "Yes") {
                                    d_item.questions && d_item.questions.map((q_item) => {
                                      if (q_item.isRequired) {
                                        let codeAr = state[q_item.code];
                                        if (q_item.code == "CURR_ENROLL_CITY" && enrCityDatas.length <= 0) {
                                          errors["CURR_ENROLL_CITY"] = false
                                          setState({ ...state, CURR_ENROLL_CITY: "" });
                                        } else if (!codeAr || codeAr == 'undefined' || codeAr == '') {
                                          valid = false;
                                          errors[q_item.code] = true;
                                        }
                                      }
                                    })
                                  }
                                })

                              }
                            }
                          }


                        }
                      }

                    }
                  }


                }

              }
            }
          }
        }

      }
    }


    //SCT or SAT required validation
    if (state['HIGHDEGEDU'] && state['HIGHDEGEDU'] == "High school degree or equivalent (Grade-12)" && state['ENTRANCEAPPEARED']) {
      if (state['ENTRANCEAPPEARED'].indexOf('SAT') > -1 || state['ENTRANCEAPPEARED'].indexOf('ACT') > -1) {
        setSatReqMsg('')
      } else {
        valid = false;
        errors['ENTRANCEAPPEARED'] = true;
        setSatReqMsg("SAT OR ACT required")
      }
    }

    if (valid) {
      props.stepSubmitSecond(state, 2);
      setErrors({});
    }
    else {
      setErrors({ ...errors });
    }

  }


  const onKeyDownInput = event => {
    if (event && event.target && event.target.name) {
      const { target: { name, value } } = event;

      if (event.keyCode == 13) {
        setState({ ...state, [name]: '' });
        var arr = name.split('_');
        let Newname = arr[0];
        if (state[Newname]) {
          let ArryVale = [...state[Newname]];
          var indexB = ArryVale.indexOf(value);
          if (indexB > -1) {
            ArryVale.splice(indexB, 1);
            setState({ ...state, [name]: '', [Newname]: [...ArryVale] });
          } else {
            ArryVale.push(value);
            setState({ ...state, [name]: '', [Newname]: [...ArryVale] });
          }
        } else {
          setState({ ...state, [name]: '', [Newname]: [value] });
        }
      }
    } else {
      //setState({ ...state, DOB: event });
    }
  };

  //get country for high school
  useEffect(() => {
    countryList().then(res => {
      setCountryDatas(res.data.data);
    }).catch(err => { });
  }, []);

  //get state for high school
  const getStateByCountry = (id) => {
    if (id) {
      setStateDatas('');
      setCityDatas('');
      stateList(id).then(res => {
        setStateDatas(res.data.data)
      }).catch(err => { });
    }
  }

  //get city for high school
  const getCityByState = (id) => {
    if (id) {
      setCityDatas('');
      cityList(id).then(res => {
        if (res.data && res.data.data && res.data.data.length) {
          setCityDatas(res.data.data)
        } else {
          setCityDatas('')
        }
      }).catch(err => { });
    }
  }

  //get state for high school
  const getENStateByCountry = (id) => {
    if (id) {
      setEnrStateDatas('');
      setEnrCityDatas('');
      stateList(id).then(res => {
        setEnrStateDatas(res.data.data)
      }).catch(err => { });
    }
  }

  //get city for high school
  const getENCityByState = (id) => {
    if (id) {
      setEnrCityDatas('');
      cityList(id).then(res => {
        if (res.data && res.data.data && res.data.data.length) {
          setEnrCityDatas(res.data.data)
        } else {
          setEnrCityDatas('')
        }
      }).catch(err => { });
    }
  }



  useEffect(() => {
    if (props.data && Object.keys(props.data).length != 0 && (props.data["HIGH_SCHL_COUNTRY"] || props.data["BECH_COUNTRY"])) {
      var c_id = ''
      if (props.data["HIGH_SCHL_COUNTRY"]) {
        c_id = countryDatas && countryDatas.find((item) => item.name == props.data["HIGH_SCHL_COUNTRY"]);
      } else {
        c_id = countryDatas && countryDatas.find((item) => item.name == props.data["BECH_COUNTRY"]);
      }
      if (c_id && c_id.countryId) getStateByCountry(c_id.countryId);
    }
  }, [props.data && countryDatas]);

  useEffect(() => {
    if (props.data && Object.keys(props.data).length != 0 && (props.data["HIGH_SCHL_STATE"] || props.data["BECH_STATE"])) {
      var s_id = ''
      if (props.data["HIGH_SCHL_STATE"]) {
        s_id = stateDatas && stateDatas.find((item) => item.name == props.data["HIGH_SCHL_STATE"]);
      } else {
        s_id = stateDatas && stateDatas.find((item) => item.name == props.data["BECH_STATE"]);
      }
      if (s_id && s_id.stateId) getCityByState(s_id.stateId);
    }
  }, [props.data && stateDatas]);

  useEffect(() => {
    if (props.data && Object.keys(props.data).length != 0 && (props.data["CURR_ENROLL_COUNTRY"])) {
      var c_id = countryDatas && countryDatas.find((item) => item.name == props.data["CURR_ENROLL_COUNTRY"]);
      if (c_id && c_id.countryId) getENStateByCountry(c_id.countryId);
    }
  }, [props.data && countryDatas]);

  useEffect(() => {
    if (props.data && Object.keys(props.data).length != 0 && (props.data["CURR_ENROLL_STATE"])) {
      var s_id = enrStateDatas && enrStateDatas.find((item) => item.name == props.data["CURR_ENROLL_STATE"]);
      if (s_id && s_id.stateId) getENCityByState(s_id.stateId);
    }
  }, [props.data && enrStateDatas]);



  const addNewRow = () => {
    if (matrixRowCount < 5) {
      setMatrixRowCount(matrixRowCount + 1)
      setState({...state, TBLROWCOUNT: matrixRowCount + 1})
    }
  }

  const removeRow = () => {
    if (matrixRowCount > 1) {
      AcademicInfo && AcademicInfo.map((mat_item) => {
        if (mat_item.code == "ADDRESSANDOTHERDETAIL") {
          mat_item.questions[matrixRowCount - 1].options.map((r_itm) => {
            delete state[r_itm.code]
          })
          setState({ ...state });
          setMatrixRowCount(matrixRowCount - 1)
          setState({...state, TBLROWCOUNT: matrixRowCount - 1})
        }
      })
    }
  }





  return (
    <div className="rightBox pt-5 pb-5 pl-4">
      <Col lg={10} md={12}>
        <div className="rightLayout">
          <div className="d-flex flex-wrap justify-content-between mb-1">
            <div className="stepTitle fw500 fs30 col4 position-relative mb-4 pb-1">Academic Info </div>
            {
              getLocalStorage('user') && getLocalStorage('user').isProfileCompleted &&
              <div className="col2 fw500 fs18 pointer" onClick={() => props.stepSelect('third')}>Skip</div>
            }
          </div>


          {/* Uper Questions */}
          <Row className="stepForm7">
            {
              AcademicInfo && AcademicInfo.map((data, index) =>
                data.section == 'academicInfo' ? (
                  <div key={index}>
                    <Col md={12} key={index}>
                      <div className="col4 fw600  fs18 mb-2">
                        <span className="numericType1">{index + 1}.</span>
                        {data.text}
                        {data.isRequired ? <sup className="starQ">*</sup> : ''}
                        {data.tips ? <Image onClick={() => handleShow(data.tips)}
                          src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                      </div>
                    </Col>
                    <Col key={index + 'sa'} lg={data.type == 'dropDown' ? '10' : '12'} md={'12'}>
                      <FormInput
                        inputData={data}
                        listOptions={data.options}
                        onchangeInput={onchangeInput}
                        onKeyDownInput={onKeyDownInput}
                        oncheckBox={oncheckBox}
                        setData={state}
                        error={errors[data.code] ? true : false}
                        errorNew={errors}
                        errorNested={errors}
                        errorMessage={'This field is required.'}
                        OuterIndex={index}
                        isProfileCompleted={getLocalStorage('user') && getLocalStorage('user').isProfileCompleted}
                      />
                    </Col>


                    {/* Lower questions */}

                    {
                      data.type == "checkbox" &&
                      state && state["STUDYFIELD"] && state["STUDYFIELD"].indexOf("Other") > -1 &&
                      <>
                        <Col md={12} key={index}>
                          <div className="col4 fw600  fs18 mb-2">
                            <span className="numericType1">{index + 1 + "." + 1}</span>
                            {data.options[data.options.length - 1].questions[0].text}
                            {data.options[data.options.length - 1].questions[0].isRequired ? <sup className="starQ">*</sup> : ''}
                            {data.options[data.options.length - 1].questions[0].tips ? <Image onClick={() => handleShow(data.options[data.options.length - 1].questions[0].tips)}
                              src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                          </div>
                        </Col>
                        <Col md={12} key={index + 'ddfdf'}>
                          <Nested_FormInput
                            key={index + 'adfsf'}
                            inputData={data.options[data.options.length - 1].questions[0]}
                            listOptions={data.options[data.options.length - 1].questions[0]}
                            onchangeInput={onchangeInput}
                            onKeyDownInput={onKeyDownInput}
                            oncheckBox={oncheckBox}
                            setData={state}
                            error={errors[data.code] ? true : false}
                            errorMessage={'This field is required.'}
                          />
                        </Col>
                      </>
                    }


                    {
                      //code for logical questions
                      data.isLogical == true ?
                        data.options && data.options.map((option, indexNew) => (
                          option.isLogical == true ?
                            <div key={indexNew}>
                              {state[data.code] && state[data.code] == option.text ?
                                option.questions && option.questions.map((question, indexSecond) =>
                                  <div key={indexSecond}>

                                    <Col md={(question.type !== 'dropDown' && question.type !== 'text' && question.type !== 'date' && question.type !== 'fullDate' && question.type !== 'multi_dropDown') ? '12' : '10'} key={indexSecond + 'ddfdf'}>
                                      {

                                        question.code == 'MOJOR_PURSUE' && spicificMajor && spicificMajor.length > 0 ?
                                          <div className="col4 fw600  fs18 mb-2">
                                            <span className="numericType1">{index + 1}.{indexSecond + 1}</span>
                                            {' '}{question.text}
                                            {question.isRequired ? <sup className="starQ">*</sup> : ''}
                                            {question.tips ? <Image onClick={() => handleShow(question.tips)}
                                              src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                                          </div>
                                          :
                                          question.code !== 'MOJOR_PURSUE' &&
                                          <div className="col4 fw600  fs18 mb-2">
                                            <span className="numericType1">{index + 1}.{indexSecond + 1}</span>
                                            {' '}{question.text}
                                            {question.isRequired ? <sup className="starQ">*</sup> : ''}
                                            {question.tips ? <Image onClick={() => handleShow(question.tips)}
                                              src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                                          </div>
                                      }
                                      
                                      {
                                        question.code == 'MOJOR_PURSUE' ?
                                        spicificMajor && spicificMajor.length > 0 &&
                                        <FormInput
                                          key={indexSecond + 'adfsf'}
                                          inputData={question}
                                          listOptions={question.code == 'MOJOR_PURSUE' ? spicificMajor :
                                            (question.code == "HIGH_SCHL_COUNTRY" || question.code == "BECH_COUNTRY") ? countryDatas : (question.code == "HIGH_SCHL_STATE" || question.code == "BECH_STATE") ? stateDatas : (question.code == "HIGH_SCHL_CITY" || question.code == "BECH_CITY") ? cityDatas : question.options}
                                          onchangeInput={onchangeInput}
                                          onKeyDownInput={onKeyDownInput}
                                          oncheckBox={oncheckBox}
                                          handleMultiSelect={handleMultiSelect}
                                          setData={state}
                                          error={errors[question.code] ? true : false}
                                          errorMessage={question.code == "YOFCOMMENCEMENT" && dateValidMsgF ? dateValidMsgF : question.code == "YOCOMPLETION" && dateValidMsgS ? dateValidMsgS : question.code == "ENTRANCEAPPEARED" && satReqMsg ? satReqMsg : 'This field is required.'}
                                        />
                                        :
                                        <FormInput
                                          key={indexSecond + 'adfsf'}
                                          inputData={question}
                                          listOptions={question.code == 'MOJOR_PURSUE' ? spicificMajor :
                                            (question.code == "HIGH_SCHL_COUNTRY" || question.code == "BECH_COUNTRY") ? countryDatas : (question.code == "HIGH_SCHL_STATE" || question.code == "BECH_STATE") ? stateDatas : (question.code == "HIGH_SCHL_CITY" || question.code == "BECH_CITY") ? cityDatas : question.options}
                                          onchangeInput={onchangeInput}
                                          onKeyDownInput={onKeyDownInput}
                                          oncheckBox={oncheckBox}
                                          handleMultiSelect={handleMultiSelect}
                                          setData={state}
                                          error={errors[question.code] ? true : false}
                                          errorMessage={question.code == "YOFCOMMENCEMENT" && dateValidMsgF ? dateValidMsgF : question.code == "YOCOMPLETION" && dateValidMsgS ? dateValidMsgS : question.code == "ENTRANCEAPPEARED" && satReqMsg ? satReqMsg : 'This field is required.'}
                                        />
                                      }

                                      
                                    </Col>


                                    {/* Nested third level */}

                                    {
                                      //code for logical questions
                                      question.isLogical == true ?
                                        question.options && question.options.map((option, indexNew) => (
                                          option.isLogical == true ?
                                            <div key={indexNew} style={{ marginLeft: 10 }}>
                                              {state[question.code] && state[question.code].indexOf(option.text) > -1 ?
                                                <React.Fragment>
                                                  {
                                                    question.code == "CURR_ENROLL_CLG" && state[question.code] == "Yes" ?

                                                      option.questions && option.questions.map((date_item, date_index) => (
                                                        <Col md={10} key={date_index}>
                                                          <div className="col4 fw600  fs18 mb-2">
                                                            {date_item.text}
                                                            {date_item.isRequired && date_item.text && <sup className="starQ">*</sup>}
                                                            {date_item.tips ? <Image onClick={() => handleShow(date_item.tips)}
                                                              src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                                                          </div>

                                                          <FormInput
                                                            key={indexSecond + 'adfsf'}
                                                            inputData={date_item}
                                                            listOptions={date_item.code == "CURR_ENROLL_COUNTRY" ? countryDatas : date_item.code == "CURR_ENROLL_STATE" ? enrStateDatas : date_item.code == "CURR_ENROLL_CITY" ? enrCityDatas : date_item.options}
                                                            onchangeInput={onchangeInput}
                                                            onKeyDownInput={onKeyDownInput}
                                                            oncheckBox={oncheckBox}
                                                            setData={state}
                                                            error={errors[date_item.code] ? true : false}
                                                            errorMessage={validMsg && indexSecond > 0 ? validMsg : 'This field is required.'}
                                                            OuterIndex={indexSecond}
                                                          />
                                                        </Col>
                                                      ))
                                                      :

                                                      <Row className={option.questions[0] && option.questions[0].code == "BACH_LSAT_LSATSCORE" ? 'small-examList' : 'examList'} style={{ paddingLeft: 5 }}>
                                                        {

                                                          option.questions && option.questions.map((new_question, indexSecond) =>
                                                            <React.Fragment key={indexSecond}>
                                                              {
                                                                indexSecond === 1 &&
                                                                <Col md={12}>
                                                                  <div className="col4 fw600  fs18 mb-2">
                                                                    {option.heading}
                                                                    {question.isRequired && option.heading && (!state[option.questions[0].code] || state[option.questions[0].code] == 'Taken') ? <sup className="starQ">*</sup> : ''}
                                                                    {option.tips ? <Image onClick={() => handleShow(option.tips)}
                                                                      src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                                                                  </div>
                                                                </Col>
                                                              }
                                                              <div >
                                                                <Col md={option.text == 'Not Applicable' ? 12 : 12}>
                                                                  <div className={`${indexSecond == 0 ? 'col4 fw600  fs18 mb-2' : "col2 fw600 mb-3"}`}>
                                                                    {new_question.text}
                                                                    {new_question.isRequired && new_question.text && indexSecond == 0 ? <sup className="starQ">*</sup> : ''}
                                                                    {new_question.tips ? <Image onClick={() => handleShow(new_question.tips)}
                                                                      src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                                                                  </div>

                                                                  <Nested_FormInput
                                                                    key={indexSecond + 'adfsf'}
                                                                    inputData={new_question}
                                                                    listOptions={new_question.options}
                                                                    onchangeInput={onchangeInput}
                                                                    onKeyDownInput={onKeyDownInput}
                                                                    oncheckBox={oncheckBox}
                                                                    setData={state}
                                                                    error={errors[new_question.code] ? true : false}
                                                                    errorMessage={validMsg && indexSecond > 0 ? validMsg : 'This field is required.'}
                                                                    OuterIndex={indexSecond}
                                                                  />
                                                                </Col>
                                                                <div></div>
                                                              </div>
                                                              {
                                                                indexSecond === 0 &&
                                                                <Col md={12}></Col>
                                                              }


                                                              {/* Taken Date Logic */}
                                                              {
                                                                new_question && new_question.options && new_question.options.length > 0 &&
                                                                new_question.options.map((taken_item, taken_index) => (
                                                                  state[new_question.code] && (state[new_question.code].length > 0 && state[new_question.code].indexOf(taken_item.value) > -1) &&
                                                                  taken_item.questions && taken_item.questions.length > 0 && taken_item.questions.map((date_item, date_index) => (

                                                                    <Col md={6} key={date_index + taken_index}>
                                                                      <div className="col4 fw600  fs18 mb-2">
                                                                        {date_item.text}
                                                                        {date_item.isRequired && date_item.text && <sup className="starQ">*</sup>}
                                                                        {date_item.tips ? <Image onClick={() => handleShow(date_item.tips)}
                                                                          src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                                                                      </div>

                                                                      <FormInput
                                                                        key={indexSecond + 'adfsf'}
                                                                        inputData={date_item}
                                                                        //listOptions={date_item.options}
                                                                        onchangeInput={onchangeInput}
                                                                        onKeyDownInput={onKeyDownInput}
                                                                        oncheckBox={oncheckBox}
                                                                        setData={state}
                                                                        error={errors[date_item.code] ? true : false}
                                                                        errorMessage={validMsg && indexSecond > 0 ? validMsg : 'This field is required.'}
                                                                        OuterIndex={indexSecond}
                                                                      />
                                                                    </Col>
                                                                  ))

                                                                ))
                                                              }

                                                              {/* new logic for Are you currently enrolled in a college or university? */}
                                                              {
                                                                new_question && new_question.options && new_question.options.length > 0 &&
                                                                new_question.options.map((taken_item, taken_index) => (
                                                                  state[new_question.code] && state[new_question.code] == "Yes" &&
                                                                  taken_item.questions && taken_item.questions.length > 0 && taken_item.questions.map((date_item, date_index) => (

                                                                    <Col md={6} key={date_index + taken_index}>
                                                                      <div className="col4 fw600  fs18 mb-2">
                                                                        {date_item.text}
                                                                        {date_item.isRequired && date_item.text && <sup className="starQ">*</sup>}
                                                                        {date_item.tips ? <Image onClick={() => handleShow(date_item.tips)}
                                                                          src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                                                                      </div>

                                                                      <FormInput
                                                                        key={indexSecond + 'adfsf'}
                                                                        inputData={date_item}
                                                                        //listOptions={date_item.options}
                                                                        onchangeInput={onchangeInput}
                                                                        onKeyDownInput={onKeyDownInput}
                                                                        oncheckBox={oncheckBox}
                                                                        setData={state}
                                                                        error={errors[date_item.code] ? true : false}
                                                                        errorMessage={validMsg && indexSecond > 0 ? validMsg : 'This field is required.'}
                                                                        OuterIndex={indexSecond}
                                                                      />
                                                                    </Col>
                                                                  ))

                                                                ))
                                                              }
                                                            </React.Fragment>
                                                          )
                                                        }
                                                      </Row>
                                                  }
                                                </React.Fragment>
                                                :
                                                ''
                                              }

                                            </div>
                                            : ''

                                        ))
                                        : ''
                                    }
                                  </div>
                                )
                                : ''}
                            </div>
                            : ''

                        ))
                        : ''
                    }



                    <Row>
                      {
                        data.isLogical && data.questions && data.type == "combine" &&
                        data.questions.map((data_que, data_index) => (
                          data_que.options &&
                          <React.Fragment key={data_index}>
                            <Col md={5}>
                              <Col md={5}>
                                <div className="col4 fw600  fs18 mb-2">
                                  {/* <span className="numericType1">{data_index + 1}.</span> */}
                                  {data_que.text}
                                  {data_que.isRequired ? <sup className="starQ">*</sup> : ''}
                                  {data_que.tips ? <Image onClick={() => handleShow(data_que.tips)}
                                    src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                                </div>
                              </Col>
                              <FormInput
                                inputData={data_que}
                                listOptions={data_que.options}
                                onchangeInput={onchangeInput}
                                onKeyDownInput={onKeyDownInput}
                                oncheckBox={oncheckBox}
                                setData={state}
                                error={errors[data_que.code] ? true : false}
                                errorNew={errors}
                                errorNested={errors}
                                errorMessage={'This field is required.'}
                                OuterIndex={data_index}
                                isProfileCompleted={getLocalStorage('user') && getLocalStorage('user').isProfileCompleted}
                              />
                            </Col>
                          </React.Fragment>
                        ))
                      }
                    </Row>



                    {
                      // ----------  Matrix ------------//
                      // ---- Logical Question ---------// 



                      data.isLogical == true && data.questions && data.type == 'categorical' &&
                      <>
                        <div className="text-right d-flex flex-wrap justify-content-end  mt-3" style={{ marginBottom: 20 }}>
                          <Button type="button" className="btnRmRow"
                          style={{marginRight: 20}}
                            onClick={() => removeRow()}>
                            Remove Row
                          </Button>
                          <Button type="button" className="btnAddRow"
                            onClick={() => addNewRow()}>
                            Add New Row
                          </Button>
                        </div>
                        {/* style={{'overflow-y': 'auto', height: 250}} */}
                        <div className="tableLayout5 mb-4" >
                          <Table bordered className="tableType2 tablePreferences br10 mb-4">
                            <thead>
                              {
                                data.questions && data.questions.map((option, indexNew) => (
                                  <tr>
                                    {
                                      indexNew == 0 && option.options && option.options.map((opt, ind) => (
                                        <th key={indexNew + ind}>{opt.text ? opt.text : ''}</th>
                                      ))
                                    }
                                  </tr>
                                ))
                              }
                            </thead>

                            <tbody>
                              {
                                data.questions && data.questions.map((m_item, m_index) => (
                                  matrixRowCount >= m_index + 1 &&
                                  <tr key={index + m_index + 'p'}>
                                    {
                                      m_item.options && m_item.options.map((m_option, o_index) => (
                                        <td key={index + m_index + o_index + 'p'}>
                                          <Form.Group controlId={`formBasicPhone${m_option.code} `}>
                                            {
                                              <Form.Control
                                                type={m_option.type}
                                                className="inputType2 w-100 m-auto"
                                                name={m_option.code}
                                                value={state[m_option.code] || ''}
                                                onChange={(e) => (m_option.code == "YEARSATTENDED" || m_option.code == "YEARSATTENDED2" || m_option.code == "YEARSATTENDED3" || m_option.code == "YEARSATTENDED4" || m_option.code == "YEARSATTENDED5") ? e.target.value.match('^[+0-9 ]*$') != null && (e.target.value > 0 || e.target.value == '') && e.target.value.length <= 2 && onchangeInput(e) : onchangeInput(e)}
                                              ></Form.Control>
                                            }
                                            {
                                              errors && errors[m_option.code] && errors[m_option.code] == true &&
                                              <span className="help-block error-text">
                                                <span style={{ color: "red", fontSize: 13 }}>
                                                  This field is required.
                                                </span>
                                              </span>
                                            }
                                          </Form.Group>
                                        </td>
                                      ))
                                    }
                                  </tr>

                                ))
                              }
                            </tbody>
                          </Table>
                        </div>
                      </>
                    }
                  </div>
                ) : ''
              )
            }


          </Row>
        </div>
        <div className="footerBotton">
          <div className="hrBorder pt-3"></div>
          <div className="text-right d-flex flex-wrap justify-content-between  mt-3">
            <Button type="button" className="btnType7"
              onClick={() => props.stepSelect('first')}>
              Previous
            </Button>
            <Button type="button" className="btnType2"
              onClick={() => stepSubmit()}>
              Save & Next
            </Button>
          </div>
        </div>
      </Col>


      <TipModal tipsData={tipsData} title={"Info"} show={show} handleClose={handleClose}></TipModal>

    </div>





  )
}

export default SecondStep;





