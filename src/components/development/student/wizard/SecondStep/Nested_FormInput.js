import React, { useState, useEffect } from 'react';
import { Col, Row, Image, Button, Form, Nav, Tab, Accordion, Card } from 'react-bootstrap';
import { Slider, RangeSlider } from 'rsuite';
import 'rsuite/dist/styles/rsuite-default.css';
import Select from 'react-select';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'chocolate2', label: 'Chocolate2' },
  { value: 'strawberry2', label: 'Strawberry2' },
  { value: 'vanilla2', label: 'Vanilla2' },
  { value: 'chocolat3', label: 'Chocolate3' },
  { value: 'strawberr3', label: 'Strawberry3' },
  { value: 'vanilla3', label: 'Vanilla3' }
]

const Nested_FormInput = (props) => {
  switch (props.inputData.type) {
    case 'text':
      return (
        <>
          <Form.Group controlId={`formBasicName${props.inputData.code}`} key={`formBasicName${props.inputData.code}`}>
            <Form.Control
              type="text"
              name={props.inputData.code}
              value={props.setData[props.inputData.code] || ''}
              placeholder={props.inputData.placeholder}
              className="inputType2"
              onChange={props.onchangeInput}
            />
            {
              props.error &&
              <span className="help-block error-text">
                <span style={{ color: "red", fontSize: 13 }}>
                  {props.errorMessage}
                </span>
              </span>
            }
          </Form.Group>
        </>
      )

    case 'number':
      return (
        <>
          <Form.Group controlId={`formBasicName${props.inputData.code}`} key={`formBasicName${props.inputData.code}`}>
            <Form.Control
              type="text"
              name={props.inputData.code}
              value={props.setData[props.inputData.code] || ''}
              placeholder={props.inputData.placeholder}
              className="inputType2 quantity"              
              onChange={(e) => e.target.value.match('^[+0-9 ]*$') != null ?
                props.onchangeInput(e)
                : ''}     
            />
            {
              props.error &&
              <span className="help-block error-text">
                <span style={{ color: "red", fontSize: 11 }}>
                  {props.errorMessage}
                </span>
              </span>
            }
          </Form.Group>
        </>
      )

    case 'float':
      return (
        <>
          <Form.Group controlId={`formBasicName${props.inputData.code}`} key={`formBasicName${props.inputData.code}`}>
            <Form.Control
              type="text"
              name={props.inputData.code}
              value={props.setData[props.inputData.code] || ''}
              placeholder={props.inputData.placeholder}
              className="inputType2 quantity"   
              onChange={(e) =>
                (/^\d*\.?\d*$/).test(e.target.value) ?
                  props.onchangeInput(e)
                  : ''
              }
            />
            {
              props.error &&
              <span className="help-block error-text">
                <span style={{ color: "red", fontSize: 11 }}>
                  {props.errorMessage}
                </span>
              </span>
            }
          </Form.Group>
        </>
      )

    case 'dropDown':
      return (
        <>
          <Form.Group controlId={`formBasicName${props.inputData.code}`} key={`formBasicName${props.inputData.code}`}>
            <Form.Control as="select"
              className="selectTyp1 pointer"
              name={props.inputData.code}
              value={props.setData[props.inputData.code] || ''}
              onChange={props.onchangeInput}
            >
              <option value="">Select</option>
              {
                props && props.listOptions &&
                props.listOptions.map((item, index) => {
                  return <option key={index} value={item.name ? item.name : item.value}>{item.name ? item.name : item.value}</option>
                })
              }
            </Form.Control>
            {
              props.error &&
              <span className="help-block error-text">
                <span style={{ color: "red", fontSize: 13 }}>
                  {props.errorMessage}
                </span>
              </span>
            }
          </Form.Group>
        </>
      )
    case 'radio':
      return (
        <Form.Group controlId={`formBasicName${props.inputData.code}`} key={`formBasicName${props.inputData.code}`}>
          {
            props.inputData.options.map((item, index) => {
              return <Form.Check
                key={index}
                type="radio"
                label={item.text}
                className={props.setData[props.inputData.code] == item.value ? "radioType1 btm-margin actives" : "radioType1 btm-margin"}
                name={props.inputData.code} value={item.value}
                checked={props.setData[props.inputData.code] == item.text}
                onChange={props.onchangeInput}
              />
            })
          }
          {
            props.error &&
            <span className="help-block error-text">
              <span style={{ color: "red", fontSize: 13 }}>
                {props.errorMessage}
              </span>
            </span>
          }
        </Form.Group>
      )

    case 'checkbox':
      return (
        <>
          <div className="tagType1 d-flex flex-wrap mb-3">
            {
              props.inputData.options.map((item, index) => {
                return <span key={index} className={`br8 col4 fw500 mr-3 ${props.setData[props.inputData.code] && props.setData[props.inputData.code].indexOf(item.value) > -1 ? 'bgCol35' : ''}  `}
                  disabled={true}
                  name={props.inputData.code}
                  onClick={() => props.oncheckBox(props.inputData.code, item.value)}
                  value={item.value || ''}>
                  {item.value}
                </span>
              })
            }

            {
              props.error &&
              <span className="help-block error-text">
                <span style={{ color: "red", fontSize: 13 }}>
                  {props.errorMessage}
                </span>
              </span>
            }
          </div>

        </>
      )

    case 'textArea':
      return (
        <Form.Group controlId={`formBasicName${props.inputData.code}`} key={`formBasicName${props.inputData.code}`}>
          <Form.Control
            as="textarea"
            rows={3}
            name={props.inputData.code}
            value={props.setData[props.inputData.code] || ''}
            placeholder={props.inputData.placeholder}
            className="inputType1"
            onChange={props.onchangeInput}
          />
          {
            props.error &&
            <span className="help-block error-text">
              <span style={{ color: "red", fontSize: 13 }}>
                {props.errorMessage}
              </span>
            </span>
          }
        </Form.Group>
      )
    default:
      return '';
  }

}



export default Nested_FormInput;





