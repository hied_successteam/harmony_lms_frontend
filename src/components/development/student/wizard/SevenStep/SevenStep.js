import React, { useState, useEffect } from 'react';
import { Col, Row, Button, Image } from 'react-bootstrap';
import { Slider, RangeSlider } from 'rsuite';
import 'rsuite/dist/styles/rsuite-default.css';
import FormInput from './FormInput'
import { FINANCIALINFO, FINANCIALPREFRENCE } from '../WizardQuestion'
import { getLocalStorage } from '../../../../../Lib/Utils';
import Loader from "react-js-loader";
import infoicon from "../../../../../assets/images/svg/questions.svg";
import TipModal from '../../../TipModal/TipModal'


const SevenStep = (props) => {

  const [state, setState] = useState({});
  const [errors, setErrors] = useState({});
  const [totalDefth, setTotalDefth] = useState([1, 2, 3, 4, 5, 6, 7]);
  const [minLenMsg, setMinLenMsg] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [firstStep, setFirstStep] = useState('');

  const [tipsData, setTipsData] = useState(false);
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);

  const handleShow = (tipsData) => {
    setTipsData(tipsData);
    setShow(true);
  }

  useEffect(() => {
    setErrors({})
  }, [props])


  const onchangeInput = event => {

    if (event && event.target && event.target.name) {

      const { target: { name, value } } = event;


      setState({ ...state, [name]: value });


    } else {
      //setState({ ...state, DOB: event });
    }
  };

  const onKeyDownInput = event => {
    if (event && event.target && event.target.name) {
      const { target: { name, value } } = event;

      if (event.keyCode == 13) {
        setState({ ...state, [name]: '' });
        var arr = name.split('_');
        let Newname = arr[0];
        if (state[Newname]) {
          let ArryVale = state[Newname];
          var indexB = ArryVale.indexOf(value);
          if (indexB > -1) {
            ArryVale.splice(indexB, 1);
            setState({ ...state, [name]: '', [Newname]: [...ArryVale] });
          } else {
            ArryVale.push(value);
            setState({ ...state, [name]: '', [Newname]: [...ArryVale] });
          }
        } else {
          setState({ ...state, [name]: '', [Newname]: [value] });
        }

      }

    } else {
      //setState({ ...state, DOB: event });
    }
  };



  const oncheckBox = (name, value) => {
    if (state[name]) {
      let ArryVale = state[name];
      var indexB = ArryVale.indexOf(value);
      if (indexB > -1) {
        ArryVale.splice(indexB, 1);
        if (ArryVale && ArryVale.length <= 0) {
          delete state[name]
          setState({ ...state });
        } else {
          setState({ ...state, [name]: [...ArryVale] });
        }
      } else {
        ArryVale.push(value);
        setState({ ...state, [name]: [...ArryVale] });
      }
    } else {
      setState({ ...state, [name]: [value] });
    }

    if (name == "FINANASSIST" && state && state["FINANASSIST"] && state["FINANASSIST"].indexOf("Not applicable") > -1) {
      setState({ ...state, FINANASSIST: ["Not applicable"] });
    }
  }

  useEffect(() => {
    if (props.data && Object.keys(props.data).length != 0) {
      setState({
        ...state,
        ...props.data
      });
    }
  }, [props.data]);


  useEffect(() =>{ 
    if(getLocalStorage('visa')){
      setFirstStep(getLocalStorage('visa'))
      if(getLocalStorage('visa') == "U.S. citizen"){
        setState({ ...state, FASA: ""  });
      } else{
        setState({ ...state, FASA: "Not Applicable" });
      }
    }
  },[getLocalStorage('visa')])



  const stepSubmit = () => {
    setIsLoading(true)
    let errors = {};
    let valid = true;

    for (var m = 0; m < FINANCIALINFO.length; m++) {
      if (FINANCIALINFO[m].isRequired) {
        let codevalue = state[FINANCIALINFO[m].code];
        if (!codevalue || codevalue == 'undefined' || codevalue == '') {
          valid = false;
          errors[FINANCIALINFO[m].code] = true;
        }
      }

      if (FINANCIALINFO[m].minValue) {
        setMinLenMsg("")
        let min = state[FINANCIALINFO[m].code];
        if (parseFloat(min) < parseFloat(FINANCIALINFO[m].minValue)) {
          valid = false;
          errors[FINANCIALINFO[m].code] = "Please enter above minimum value.";
          setMinLenMsg("Please enter above minimum value.")
        }
      }

      if (FINANCIALINFO[m].isLogical) {
        let Newarry = FINANCIALINFO[m].options;
        for (var n = 0; n < Newarry.length; n++) {
          if (Newarry[n].isLogical) {
            if (state && state[FINANCIALINFO[m].code] && state[FINANCIALINFO[m].code].indexOf(Newarry[n].value) > -1) {
              let newdata = Newarry[n].questions;
              for (var i = 0; i < newdata.length; i++) {
                if (newdata[i].isRequired) {
                  let codevalueNew = state[newdata[i].code];
                  if (!codevalueNew || codevalueNew == 'undefined' || codevalueNew == '') {
                    valid = false;
                    errors[newdata[i].code] = true;
                  }

                }
              }
            }
          }
        }
      }
    }


    for (var m = 0; m < FINANCIALPREFRENCE.length; m++) {

      if (FINANCIALPREFRENCE[m].isRequired) {
        let codevalue = state[FINANCIALPREFRENCE[m].code];
        if (!codevalue || codevalue == 'undefined' || codevalue == '') {
          valid = false;
          errors[FINANCIALPREFRENCE[m].code] = true;
        }
      }

      if (FINANCIALPREFRENCE[m].code === 'COLLSELCFACTOR') {
        let newdata = FINANCIALPREFRENCE[m].questions;  
        for (var i = 0; i < newdata.length; i++) {
          if (newdata[i].isRequired) {
            let codevalueNew = state[newdata[i].code];
            if (!codevalueNew || codevalueNew == 'undefined' || codevalueNew == '') {
              valid = false;
              errors[newdata[i].code] = true;
            }

          }
        }        
      }



      if (FINANCIALPREFRENCE[m].isLogical) {

        let Newarry = FINANCIALPREFRENCE[m].options;
        for (var n = 0; n < Newarry.length; n++) {
          if (Newarry[n].isLogical) {
            if (state && state['DISCOVERHIED'] && state['DISCOVERHIED'].indexOf(Newarry[n].value) > -1) {
              let newdata = Newarry[n].questions;
              for (var i = 0; i < newdata.length; i++) {
                if (newdata[i].isRequired) {
                  let codevalueNew = state[newdata[i].code];
                  if (!codevalueNew || codevalueNew == 'undefined' || codevalueNew == '') {
                    valid = false;
                    errors[newdata[i].code] = true;
                  }

                }
              }
            }
          }
        }

      }
    }

    if (valid) {
      props.stepSubmitSeven(state, 7);
      setTimeout(function () {
        setIsLoading(false)
      }.bind(this), 2000);
      setErrors({});
    }
    else {
      setIsLoading(false)
      setErrors({ ...errors });
    }
  }

  return (

    <div className="rightBox pt-5 pb-5 pl-4">
      <Col lg={10} md={12}>
        <div className="rightLayout">
          <div className="d-flex flex-wrap justify-content-between mb-1">
            <div className="stepTitle fw500 fs30 col4 position-relative mb-4 pb-1">Financial Preferences </div>
            {
              getLocalStorage('user') && getLocalStorage('user').isProfileCompleted &&
              <div className="col2 fw500 fs18 pointer" onClick={() => props.stepSelect('eight')}>Skip</div>
            }
          </div>
          <Row className="stepForm7">  
            {
              FINANCIALINFO && FINANCIALINFO.map((data, index) =>
                <div key={index}>
                  {data.type == 'dropDown' ?
                    <Col md={12} key={index + 'aaa'}>
                      <div className="col4 fw600 fs18 mb-2">
                        <span className="numericType1">{index + 1}.</span>
                        {data.text}
                        {data.isRequired ? <sup className="starQ">*</sup> : ''}
                        {data.tips ? <Image onClick={() => handleShow(data.tips)} src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                      </div>
                    </Col>
                    : ''}

                  <Col md={(data.type !== 'dropDown' || data.type !== 'categorical') ? '12' : '6'} key={index + 'bbb'}>
                    {(data.type !== 'dropDown') ?
                      <div className="col4 fw600 fs18 mb-2">
                        <span className="numericType1">{index + 1}.</span>
                        {data.text}
                        {data.isRequired ? <sup className="starQ">*</sup> : ''}
                        {data.tips ? <Image onClick={() => handleShow(data.tips)} src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                      </div>
                      : ''}
                    <FormInput
                      key={index + 'ddsd'}
                      inputData={data}
                      listOptions={data.options}
                      onchangeInput={onchangeInput}
                      onKeyDownInput={onKeyDownInput}
                      oncheckBox={oncheckBox}
                      firstStepVisa={firstStep}
                      setData={state}
                      totalDefth={totalDefth}
                      error={errors[data.code] ? true : false}
                      errorMessage={data.code == "ESTEDUCOST" && minLenMsg ? minLenMsg : 'This field is required.'}
                      errors = {errors}
                    />
                  </Col>

                  {
                    //code for logical questions
                    data.isLogical == true ?
                      data.options && data.options.map((option, indexNew) =>
                      (
                        option.isLogical == true ?
                          <div key={indexNew}>
                            {
                              state[data.code] && state[data.code].indexOf(option.text) > -1 ?
                                option.questions && option.questions.map((question, indexSecond) =>
                                  <div key={indexSecond}>
                                    {question.type == 'dropDown' ?
                                      <Col md={12} key={indexSecond + 'qqqq'}>
                                        <div className="col4 fw600 fs18 mb-2">
                                          <span className="numericType1">{index + 1}.{indexSecond + 1}</span>
                                          {' '}{question.text}
                                          {question.isRequired ? <sup className="starQ">*</sup> : ''}
                                          {question.tips ? <Image onClick={() => handleShow(question.tips)} src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                                        </div>
                                      </Col>
                                      : ''}

                                    <Col md={(question.type !== 'dropDown') ? '12' : '6'} key={indexSecond + 'ddfdf'}>
                                      {(question.type !== 'dropDown') ?
                                        <div className="col4 fw600 fs18 mb-2">
                                          <span className="numericType1">{index + 1}.{indexSecond + 1}</span>
                                          {' '}{question.text}
                                          {question.isRequired ? <sup className="starQ">*</sup> : ''}
                                          {question.tips ? <Image onClick={() => handleShow(question.tips)} src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                                        </div>
                                        : ''}
                                      <FormInput
                                        key={indexSecond + 'adfsf'}
                                        inputData={question}
                                        listOptions={question.options}
                                        onchangeInput={onchangeInput}
                                        oncheckBox={oncheckBox}
                                        setData={state}
                                        error={errors[question.code] ? true : false}
                                        errorMessage={'This field is required.'}
                                        errors = {errors}
                                      />

                                    </Col>
                                  </div>
                                )
                                : ''}
                          </div>
                          : ''

                      ))
                      : ''
                  }

                </div>
              )
            }

            <Col md={12}>
              <div className="hrBorder mb-4 mt-1"></div>
              <div className="fs20 fw600 col2 mb-3">Final Steps</div>
            </Col>

            {
              FINANCIALPREFRENCE && FINANCIALPREFRENCE.map((data, index) =>
                <div key={index}>
                  {data.type == 'dropDown' ?
                    <Col md={12} key={index + 'aaa'}>
                      <div className="col4 fw600 fs18 mb-2">
                        <span className="numericType1">{index + 1}.</span>
                        {data.text}
                        {data.isRequired ? <sup className="starQ">*</sup> : ''}
                        {data.tips ? <Image onClick={() => handleShow(data.tips)} src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                      </div>
                    </Col>
                    : ''}

                  <Col md={(data.type !== 'dropDown' || data.type !== 'categorical') ? '12' : '6'} key={index + 'bbb'}>
                    {(data.type !== 'dropDown') ?
                      <div className="col4 fw600 fs18 mb-2">
                        <span className="numericType1">{index + 1}.</span>
                        {data.text}
                        {data.isRequired ? <sup className="starQ">*</sup> : ''}
                        {data.tips ? <Image onClick={() => handleShow(data.tips)} src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                      </div>
                      : ''}
                    <FormInput
                      key={index + 'ddsd'}
                      inputData={data}
                      listOptions={data.options}
                      onchangeInput={onchangeInput}
                      onKeyDownInput={onKeyDownInput}
                      oncheckBox={oncheckBox}
                      setData={state}
                      totalDefth={totalDefth}
                      error={errors[data.code] ? true : false}
                      errors = {errors}
                      //errorMessage={'This field is required.'}
                      errorMessage={errors[data.code] == true ? 'This field is required.' :
                        errors[data.code] !== '' ? errors[data.code] : 'This field is required.'}
                    />
                  </Col>

                  {
                    //code for logical questions
                    data.isLogical == true ?
                      data.options && data.options.map((option, indexNew) =>
                      (
                        option.isLogical == true ?
                          <div key={indexNew}>
                            {
                              state[data.code] && state[data.code].indexOf(option.text) > -1 ?
                                option.questions && option.questions.map((question, indexSecond) =>
                                  <div key={indexSecond}>
                                    {question.type == 'dropDown' ?
                                      <Col md={12} key={indexSecond + 'qqqq'}>
                                        <div className="col4 fw600 fs18 mb-2">
                                          <span className="numericType1">{index + 1}.{indexSecond + 1}</span>
                                          {' '}{question.text}
                                          {question.isRequired ? <sup className="starQ">*</sup> : ''}
                                          {question.tips ? <Image onClick={() => handleShow(question.tips)} src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                                        </div>
                                      </Col>
                                      : ''}

                                    <Col md={'12'} key={indexSecond + 'ddfdf'}>
                                      {(question.type !== 'dropDown') ?
                                        <div className="col4 fw600 fs18 mb-2">
                                          <span className="numericType1">{index + 1}.{state[data.code] && state[data.code].indexOf("HiEd Student Success Reps") > -1 && state[data.code].indexOf("Others (please specify)") > -1 ? indexSecond + 2 : indexSecond + 1}</span>
                                          {' '}{question.text}
                                          {question.isRequired ? <sup className="starQ">*</sup> : ''}
                                          {question.tips ? <Image onClick={() => handleShow(question.tips)} src={infoicon} alt="Questions" className="ml-2 pointer" /> : ''}
                                        </div>
                                        : ''}
                                      <FormInput
                                        key={indexSecond + 'adfsf'}
                                        inputData={question}
                                        listOptions={question.options}
                                        onchangeInput={onchangeInput}
                                        oncheckBox={oncheckBox}
                                        setData={state}
                                        error={errors[question.code] ? true : false}
                                        errorMessage={'This field is required.'}
                                        errors = {errors}
                                      />

                                    </Col>
                                  </div>
                                )
                                : ''}
                          </div>
                          : ''

                      ))
                      : ''
                  }



                </div>
              )
            }

          </Row>
        </div>
        <div className="footerBotton">
          <div className="hrBorder pt-3"></div>
          <div className="text-right d-flex flex-wrap justify-content-between  mt-3">
            <Button type="button" className="btnType7"
              onClick={() => props.stepSelect('sixth')}>
              Previous
            </Button>
            <Button
              type="button"
              className="btnType2"
              onClick={() => stepSubmit()}
            >

              {/* {
                isLoading ?
                  <Loader type="bubble-top" bgColor={"#414180"} size={25} />
                  :
                  "Save & Next"
              } */}
              Save & Next
            </Button>
          </div>
        </div>
      </Col>


      <TipModal tipsData={tipsData} title={"Info"} show={show} handleClose={handleClose}></TipModal>


    </div>


  );


}

export default SevenStep;