import React, { useState, useEffect } from 'react';
import { Col, Row, Image, Button, Form, Nav, Tab, Accordion, Card, Table } from 'react-bootstrap';
import { Slider, RangeSlider } from 'rsuite';
import 'rsuite/dist/styles/rsuite-default.css';
import Select from 'react-select';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';

const options = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
    { value: 'chocolate2', label: 'Chocolate2' },
    { value: 'strawberry2', label: 'Strawberry2' },
    { value: 'vanilla2', label: 'Vanilla2' },
    { value: 'chocolat3', label: 'Chocolate3' },
    { value: 'strawberr3', label: 'Strawberry3' },
    { value: 'vanilla3', label: 'Vanilla3' }
]


const FormInput = (props) => {

    switch (props.inputData.type) {

        case 'text':
            return (
                <Form.Group className="priceBox" controlId={`formBasicName${props.inputData.code}`}>
                    <Form.Control
                        type="text"
                        name={props.inputData.code == 'VOLUNACTIVITY' ? props.inputData.code + '_VV' : props.inputData.code}
                        value={(props.inputData.code == 'VOLUNACTIVITY' ? props.setData[props.inputData.code + '_VV'] : props.setData[props.inputData.code]) || ''}
                        placeholder={props.inputData.placeholder}
                        className={`inputType1 ${props.inputData.code == 'VOLUNACTIVITY' ? 'mb-3' : ''}`}
                        onChange={props.onchangeInput}
                        onKeyDown={props.onKeyDownInput}
                    />
                    {props.inputData.code == 'VOLUNACTIVITY' ?
                        <div className="tagType1 d-flex flex-wrap">
                            {
                                props.setData[props.inputData.code] &&
                                props.setData[props.inputData.code].map((item, index) => {
                                    return <span key={index} className="br8 col4 fw500 mr-3">{item}</span>
                                })
                            }
                        </div>
                        : ''}
                    {
                        props.error &&
                        <span className="help-block error-text">
                            <span style={{ color: "red", fontSize: 13 }}>
                                {props.errorMessage}
                            </span>
                        </span>
                    }
                </Form.Group>
            )

        case 'number':
            return (

                <Form.Group className={`priceBox ${props.inputData.code == 'ESTEDUCOST' ? 'input-group' : ''} `} controlId={`formBasicName${props.inputData.code}`}>
                    {props.inputData.code == 'ESTEDUCOST' ?
                        <div className="input-group-prepend inputGroup1">
                            <span className="input-group-text" id="basic-addon1ddfd">$</span>
                        </div>
                        : ''}

                    <Form.Control
                        //type="text"
                        type="text"
                        name={props.inputData.code == 'VOLUNACTIVITY' ? props.inputData.code + '_VV' : props.inputData.code}
                        value={(props.inputData.code == 'VOLUNACTIVITY' ? props.setData[props.inputData.code + '_VV'] : props.setData[props.inputData.code]) || ''}
                        placeholder={props.inputData.placeholder}
                        className={` ${props.inputData.code == 'ESTEDUCOST' ? 'inputType3' : 'inputType1'} ${props.inputData.code == 'VOLUNACTIVITY' ? 'mb-3' : ''}`}
                        onChange={(e) => e.target.value.match('^[+0-9 ]*$') != null ?
                            props.onchangeInput(e)
                            : ''}
                        // for only integer with space
                        // onChange={(e) =>
                        //     e.target.value.match('^[+0-9]*$') != null ?
                        //         props.onchangeInput(e)
                        //         : ''
                        // }
                        min={0}
                        //for only integer and $ sign in below
                        // onChange={(e) =>
                        //     e.target.value.match('^[+0-9$]*$') != null ?
                        //         props.onchangeInput(e)
                        //         : ''
                        // }
                        onKeyDown={props.onKeyDownInput}
                    />
                    {props.inputData.code == 'VOLUNACTIVITY' ?
                        <div className="tagType1 d-flex flex-wrap">
                            {
                                props.setData[props.inputData.code] &&
                                props.setData[props.inputData.code].map((item, index) => {
                                    return <span key={index} className="br8 col4 fw500 mr-3">{item}</span>
                                })
                            }
                        </div>
                        : ''}
                    {
                        props.error &&
                        <span className="help-block error-text">
                            <span style={{ color: "red", fontSize: 13 }}>
                                {props.errorMessage}
                            </span>
                        </span>
                    }
                </Form.Group>
            )

        case 'dropDown':
            return (
                <Form.Group controlId={`exampleForm.ControlSelect1${props.inputData.code}`}>
                    <Form.Control as="select"
                        className="selectTyp1 pointer"
                        name={props.inputData.code}
                        value={props.setData[props.inputData.code] || ''}
                        onChange={props.onchangeInput}
                    >
                        {/* <option value="">{props.inputData.text}</option> */}
                        <option value="">Select</option>
                        {
                            props && props.listOptions &&
                            props.listOptions.map((item, index) => {
                                return <option key={index} title={item.name ? item.name : item.text} value={item.name ? item.name : item.value}>{item.name ? item.name : item.text}</option>
                            })
                        }
                    </Form.Control>
                    {
                        props.error &&
                        <span className="help-block error-text">
                            <span style={{ color: "red", fontSize: 13 }}>
                                {props.errorMessage}
                            </span>
                        </span>
                    }
                </Form.Group>
            )

        case 'checkbox':
            return (
                <div className="tagType1 d-flex flex-wrap mb-3">
                    {
                        props.inputData.options.map((item, index) => {
                            return <span
                                key={index}
                                className={`br8 col4 fw500 mr-3 ${props.setData[props.inputData.code] && props.setData[props.inputData.code].indexOf(item.value) > -1 ? 'bgCol35' : ''}  `}
                                name={props.inputData.code}
                                onClick={() => props.oncheckBox(props.inputData.code, item.value)}
                                value={item.value || ''}>{item.value || ''}</span>
                        })
                    }
                    {
                        props.error &&
                        <span className="help-block error-text">
                            <span style={{ color: "red", fontSize: 13 }}>
                                {props.errorMessage}
                            </span>
                        </span>
                    }
                </div>
            )

        case 'textArea':
            return (
                <>
                    <Form.Group className="mb-3" controlId={`formBasicName${props.inputData.code}`}>
                        <Form.Control
                            as="textarea"
                            name={props.inputData.code}
                            value={props.setData[props.inputData.code] || ''}
                            placeholder={props.inputData.placeholder}
                            className="inputType1"
                            rows={5}
                            onChange={props.onchangeInput}
                        />
                        {
                            props.error &&
                            <span className="help-block error-text">
                                <span style={{ color: "red", fontSize: 13 }}>
                                    {props.errorMessage}
                                </span>
                            </span>
                        }
                    </Form.Group>
                </>
            )

        case 'categorical':
            return (
                <>

                    <Table bordered className="tableType2 tablePreferences br10 mb-4">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Not at all important</th>
                                <th>Low importance</th>
                                <th>Slightly important</th>
                                <th>Neutral</th>
                                <th>Moderately important</th>
                                <th>Very important</th>
                                <th>Extremely important</th>
                            </tr>
                        </thead>
                        <tbody>

                           

                            {props.inputData.questions && props.inputData.questions.map((ques, index) =>

                                <tr key={index}>
                                    <td>
                                        {ques.text}
                                        {
                                            props.errors[ques.code] &&
                                            <span className="help-block error-text">
                                                <span style={{ color: "red", fontSize: 13 }}>                                                   
                                                This field is required.
                                                </span>
                                            </span>
                                        }
                                    </td>
                                    {props.totalDefth && props.totalDefth.map((number, index) =>
                                        <td key={index}>
                                            <Form.Group controlId={`formBasicRadio1${ques.code}${number}`}>
                                                <Form.Check
                                                    type="radio"
                                                    aria-label={ques.text}
                                                    className="checkboxTyp3"
                                                    id={`formBasicRadio1${ques.code}${number}`}
                                                    name={ques.code}
                                                    label=""
                                                    value={number || ''}
                                                    //checked={true}
                                                    checked={props.setData[ques.code] == number}
                                                    onChange={props.onchangeInput}
                                                />
                                            </Form.Group>
                                        </td>
                                    )}
                                </tr>
                            )}

                        </tbody>
                    </Table>

                </>

            )

        case 'radio':
            return (
                <Form.Group controlId={`formBasicOne${props.inputData.code}`}>
                    {
                        props.inputData.options.map((item, index) => {
                            return <Form.Check type="radio"
                                key={index}
                                label={item.text}
                                className={props.setData[props.inputData.code] == item.value ? "radioType1 actives" : "radioType1"}
                                id={props.inputData.code} name={props.inputData.code}
                                value={item.value || ''}
                                checked={props.setData[props.inputData.code] == item.text}
                                onChange={props.onchangeInput} 
                                disabled={props.inputData.code == "FASA" && props.firstStepVisa != "U.S. citizen" ? true : false} 
                             />
                        })
                    }
                    {
                        props.error &&
                        <span className="help-block error-text">
                            <span style={{ color: "red", fontSize: 13 }}>
                                {props.errorMessage}
                            </span>
                        </span>
                    }
                </Form.Group>
            )
        default:
            return '';
    }


}

export default FormInput;




