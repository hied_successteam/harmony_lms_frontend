import React, { useEffect, useState } from 'react';
import { Col, Row, Container, Image, Form, Dropdown, Accordion, Card, Button, Table } from 'react-bootstrap';
import DownOne from "../../../../assets/images/svg/downOne.svg";
import {
  BasicQuestions,
  AcademicInfo,
  CollegePreference,
  DemoGraphicPreference,
  ClimatePreference,
  LocationPreference,
  EmploymentInfo,
  PLACEMENTPREFRENCEPRE,
  FINANCIALINFO,
  FINANCIALPREFRENCE,
} from '../wizard/WizardQuestion';

import MyViewField from './MyViewField';
const MyprofileContent = (props) => {
  const [profileData, setProfileData] = useState({});
  const [totalDefth, setTotalDefth] = useState([1, 2, 3, 4, 5, 6, 7]);
  const [totalDefthPrefren, setTotalDefthPrefren] = useState(['Yes', 'No', 'Preferred but optional']);

  const [basicShow, setBasicShow] = useState(true);
  const [academicShow, setAcademicShow] = useState(true);
  const [collegePrenShow, setCollegePrenShow] = useState(true);
  const [demographicShow, setDemographicShow] = useState(true);
  const [EmploymentShow, setEmploymentShow] = useState(true);
  const [PprefenceShow, setPprefenceShow] = useState(true);
  const [FinPrefShow, setFinPrefShow] = useState(true);
  const [PlanShow, setPlanShow] = useState(true);
  const [ServicesShow, setServicesShow] = useState(true);

  useEffect(() => {
    if (props && props.profileData) {
      setProfileData(props.profileData);
    }
  }, [props.profileData])


  return (

    <div className="middileAccordian mb-5">
      <div className="myProfile">
        {props && props.myprofileHeading == false ?

          <div className="d-flex justify-content-between">
            <div className="fs20 fw500 col2 mb-4 pointer" onClick={() => props.pageBack()}>
              <i className="fs20 mr-2 fa fa-chevron-left" aria-hidden="true"></i> Back
            </div>
            {/* <Button className="btnType3 updateBtn">Update</Button> */}
          </div>
          :
          <div className="fs20 fw500 col2 mb-4">{props && props.location && props.location.pathname == "/student/myProfile" ? 'My Profile' :profileData && profileData.basic && profileData.basic.NAME + '’s Profile'}</div>
        }

        <div className="exportnew">

          <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1 pr-3">
            <div>Basic</div>
            <div
              onClick={() => setBasicShow(!basicShow)}
              className="pointer"
            >
              <Image src={DownOne} alt="Icon"
                className={`pointer ${basicShow ? 'rotateArro' : ''}`} />
            </div>
          </div>

          {basicShow &&
            <Row className="mb-4">
              <MyViewField
                {...props}
                QuestionData={BasicQuestions}
                AnswerData={(profileData && profileData.basic) ? profileData.basic : (props.profileData && props.profileData.basic)}
              />
            </Row>
          }

        </div>
        <div style={{pageBreakBefore: 'always'}}></div>
        <div style={{pagebreakBefore: 'always'}}></div>


        <div className="exportnew2" break="true">

          <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1 pr-3">
            <div>Academic Info</div>
            <div
              onClick={() => setAcademicShow(!academicShow)}
              className="pointer"
            >
              <Image src={DownOne} alt="Icon"
                className={`pointer ${academicShow ? 'rotateArro' : ''}`}
              />
            </div>
          </div>

          <Row className="mb-4">
            {academicShow &&
              <MyViewField
                {...props}
                QuestionData={AcademicInfo}
                //AnswerData={profileData && profileData.academicInfo}
                AnswerData={(profileData && profileData.academicInfo) ? profileData.academicInfo : (props.profileData && props.profileData.academicInfo)}
              />
            }
          </Row>
        </div>
        <div className="pagebreak"></div>

        <div className="exportnew" break="true">

          <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1 pr-3">
            <div>College Preferences</div>
            <div
              onClick={() => setCollegePrenShow(!collegePrenShow)}
              className="pointer"
            >
              <Image src={DownOne}
                alt="Icon"
                className={`pointer ${collegePrenShow ? 'rotateArro' : ''}`}
              />
            </div>
          </div>

          <Row className="mb-4">
            {collegePrenShow &&
              <MyViewField
                {...props}
                QuestionData={CollegePreference}
                //AnswerData={profileData && profileData.collegePreference}
                AnswerData={(profileData && profileData.collegePreference) ? profileData.collegePreference : (props.profileData && props.profileData.collegePreference)}
              />
            }
          </Row>
        </div>
        <div className="pagebreak"></div>

        <div className="break" break="true">
          <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1 pr-3">
            <div>Demographic Preferences</div>
            <div
              onClick={() => setDemographicShow(!demographicShow)}
              className="pointer"
            >
              <Image src={DownOne} alt="Icon"
                className={`pointer ${demographicShow ? 'rotateArro' : ''}`}
              />
            </div>
          </div>
          {demographicShow &&
            <Row className="mb-4">
              <MyViewField
                {...props}
                QuestionData={DemoGraphicPreference}
                //AnswerData={profileData && profileData.demographicPreference}
                AnswerData={(profileData && profileData.demographicPreference) ? profileData.demographicPreference : (props.profileData && props.profileData.demographicPreference)}
              />
              <MyViewField
                {...props}
                QuestionData={ClimatePreference}
                //AnswerData={profileData && profileData.demographicPreference}
                AnswerData={(profileData && profileData.demographicPreference) ? profileData.demographicPreference : (props.profileData && props.profileData.demographicPreference)}
              />
              <MyViewField
                {...props}
                QuestionData={LocationPreference}
                //AnswerData={profileData && profileData.demographicPreference}
                AnswerData={(profileData && profileData.demographicPreference) ? profileData.demographicPreference : (props.profileData && props.profileData.demographicPreference)}
              />
            </Row>
          }
        </div>
        <div className="pagebreak"></div>

        <div className="exportnew" break="true">

          <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1 pr-3">
            <div>Employment Info</div>
            <div
              onClick={() => setEmploymentShow(!EmploymentShow)}
              className="pointer"
            >
              <Image src={DownOne} alt="Icon"
                className={`pointer ${EmploymentShow ? 'rotateArro' : ''}`}
              />
            </div>
          </div>
          {EmploymentShow &&
            <Row className="mb-4">
              <MyViewField
                {...props}
                QuestionData={EmploymentInfo}
                //AnswerData={profileData && profileData.employmentInfo}
                AnswerData={(profileData && profileData.employmentInfo) ? profileData.employmentInfo : (props.profileData && props.profileData.employmentInfo)}
              />
            </Row>
          }
        </div>
        <div className="pagebreak"></div>

        <div className="exportnew" break="true">
          <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1 pr-3">
            <div>Placement Preferences</div>
            <div
              onClick={() => setPprefenceShow(!PprefenceShow)}
              className="pointer"
            >
              <Image src={DownOne} alt="Icon"
                className={`pointer ${PprefenceShow ? 'rotateArro' : ''}`}
              />
            </div>
          </div>
          {PprefenceShow &&
            <Row className="mb-4">
              <MyViewField
                {...props}
                QuestionData={PLACEMENTPREFRENCEPRE}
                //AnswerData={profileData && profileData.placementPreference}
                AnswerData={(profileData && profileData.placementPreference) ? profileData.placementPreference : (props.profileData && props.profileData.placementPreference)}
                totalDefth={totalDefthPrefren}
              />
            </Row>
          }
        </div>
        <div className="pagebreak"></div>

        <div className="exportnew" break="true">

          <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1 pr-3">
            <div> Financial Preferences</div>
            <div
              onClick={() => setFinPrefShow(!FinPrefShow)}
              className="pointer"
            >
              <Image src={DownOne} alt="Icon"
                className={`pointer ${FinPrefShow ? 'rotateArro' : ''}`}
              />
            </div>
          </div>
          {FinPrefShow &&
            <Row className="mb-4 printHidden">
              <MyViewField
                {...props}
                QuestionData={FINANCIALINFO}
                // AnswerData={profileData && profileData.finalPreference}
                AnswerData={(profileData && profileData.finalPreference) ? profileData.finalPreference : (props.profileData && props.profileData.finalPreference)}
                totalDefth={totalDefth}
              />
              <MyViewField
                {...props}
                QuestionData={FINANCIALPREFRENCE}
                //AnswerData={profileData && profileData.finalPreference}
                AnswerData={(profileData && profileData.finalPreference) ? profileData.finalPreference : (props.profileData && props.profileData.finalPreference)}
                totalDefth={totalDefth}
              />
            </Row>
          }
        </div>
        <div className="pagebreak"></div>
        {props.hidePlan && props.hidePlan === true ? '' :
          <>
            {profileData && profileData.user && (profileData.user.plan || profileData.user.service && profileData.user.service.length > 0) &&
              <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1 pr-3">
                <div>Plan and Services</div>
                <div
                  onClick={() => setPlanShow(!PlanShow)}
                  className="pointer"
                >
                  <Image
                    src={DownOne}
                    alt="Icon"
                    className={`pointer ${PlanShow ? 'rotateArro' : ''}`}
                  />
                </div>
              </div>
            }

            {PlanShow && profileData && profileData.user && (profileData.user.plan || profileData.user.service && profileData.user.service.length > 0) &&
              <Row >
                <Col md={5}>
                  <div className="PlanList1 planList2 profilePlan active w-100 bgCol44 cursor-default p-3">
                    <Row className="align-items-center">
                      <Col md={3}>
                        <div className="text-center">
                          <div className="circleType3">
                            <Image src={profileData && profileData.user && profileData.user.plan && profileData.user.plan.icon} alt="Icon" className="mw30" />
                          </div>
                        </div>
                      </Col>
                      <Col md={6}>
                        <div className="mt-2">
                          <div className="col47 fs30 fw500 mb-1 elippse1">{profileData && profileData.user && profileData.user.plan && profileData.user.plan.name}</div>

                        </div>
                      </Col>
                      <Col md={3} className="text-right">
                        <div className="col30 fw600">${profileData && profileData.user && profileData.user.plan && profileData.user.plan.price}</div>
                      </Col>
                    </Row>
                  </div>
                </Col>
                {
                  profileData.user.service && profileData.user.service.length > 0 &&
                  <Col md={7}>
                    <div className="PlanList1 planList2 profilePlan serviceType1 active w-100 bgCol44 cursor-default p-3">
                      <div className="align-items-center">
                      <div className="tagType1 d-flex flex-wrap" style={{'pointer-events': 'none'}}>
                    {
                      profileData.user.service.map((item, index) =>{
                        return <span
                        key={index}
                        name="services"
                        className="br8 col4 fs14 fw500 mr-3"
                      >{item}</span>
                      })
                    }
                    </div> 
                      </div>
                      </div>
                  </Col>
                }
                
              </Row>
            }
          </>
        }
      </div>
    </div>

  )
}
export default MyprofileContent;






