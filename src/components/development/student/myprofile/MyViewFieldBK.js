import React, { useState, useEffect } from 'react';
import { Col, Row, Image, Button, Form, Nav, Tab, Accordion, Card, Table } from 'react-bootstrap';
import { Slider, RangeSlider } from 'rsuite';
import 'rsuite/dist/styles/rsuite-default.css';
import Select from 'react-select';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import CONSTANTS from '../../../../Lib/Constants';

const MyViewField = (props) => {

  return (
    <>
      {
        props && props.QuestionData && props.QuestionData.map((data, index) =>

          <React.Fragment key={index}>

            {data.code == 'COLLSELCFACTOR' ?

              <Col md={12}>
                <div className="mb-4">
                  <div className="col21 fw500 fs15 mb-2">{data.text}</div>
                  <div className="col2 fw500 fs15">
                    <Table bordered className="tableType2 tablePreferences br10 mb-4">
                      <thead>
                        <tr>
                          <th></th>
                          <th>Not at all important</th>
                          <th>Low importance</th>
                          <th>Slightly important</th>
                          <th>Neutral</th>
                          <th>Moderately important</th>
                          <th>Very important</th>
                          <th>Extremely important</th>
                        </tr>
                      </thead>
                      <tbody>

                        {data.questions && data.questions.map((ques, index) =>

                          <tr key={index}>
                            <td>
                              {ques.text}
                            </td>
                            {props.totalDefth && props.totalDefth.map((number, index) =>
                              <td key={index}>
                                <Form.Group controlId={`formBasicRadio1${ques.code}${number}`}>
                                  <Form.Check
                                    type="radio"
                                    aria-label={ques.text}
                                    className="checkboxTyp3"
                                    id={`formBasicRadio1${ques.code}${number}`}
                                    name={ques.code}
                                    label=""
                                    value={number || ''}
                                    checked={props.AnswerData && props.AnswerData[ques.code] == number}
                                  />
                                </Form.Group>
                              </td>
                            )}
                          </tr>
                        )}

                      </tbody>
                    </Table>
                  </div>
                </div>
              </Col>
              :
              data.code == 'SHAREPREFERENCESPLACEMENT' ?
                <Col md={12}>
                  <div className="mb-4">
                    <div className="col21 fw500 fs15 mb-2">{data.text}</div>
                    <div className="col2 fw500 fs15">
                      <Table bordered className="tableType2 planTable br10 mb-4">
                        <thead>
                          <tr>
                            <th></th>
                            <th>Yes</th>
                            <th>No</th>
                            <th>Preferred but optional</th>
                          </tr>
                        </thead>
                        <tbody>

                          {data.questions && data.questions.map((ques, index) =>

                            <tr key={index}>
                              <td>
                                {ques.text}
                              </td>
                              {props.totalDefth && props.totalDefth.map((number, index) =>
                                <td key={index}>
                                  <Form.Group controlId={`formBasicRadio1${ques.code}${number}`}>
                                    <Form.Check
                                      type="radio"
                                      aria-label={ques.text}
                                      className="checkboxTyp3"
                                      id={`formBasicRadio1${ques.code}${number}`}
                                      name={ques.code}
                                      label=""
                                      value={number || ''}
                                      checked={props.AnswerData && props.AnswerData[ques.code] == number}

                                    />
                                  </Form.Group>
                                </td>
                              )}
                            </tr>
                          )}

                        </tbody>
                      </Table>
                    </div>
                  </div>
                </Col>
                :
                <Col md={6}>
                  <div className="mb-4">
                    <div className="col21 fw500 fs15 mb-2">{data.text}</div>
                    <div className="col2 fw500 fs15">
                      {data.text == 'Gender' ?
                        CONSTANTS.GENDER[props.AnswerData && props.AnswerData[data.code]]
                        :
                        data.text == 'DOB' ?
                          moment(props.AnswerData && props.AnswerData[data.code]).format('DD/MM/YYYY')
                          :
                          Array.isArray(props.AnswerData && props.AnswerData[data.code]) ?

                            data.code == 'TARGETCOLLEGES' ?
                              props.AnswerData[data.code] && props.AnswerData[data.code].
                                map((data) => {
                                  return data.INSTNM
                                }).join(", ")
                              :
                              props.AnswerData[data.code].join(", ")
                            :
                            props.AnswerData && props.AnswerData[data.code]

                      }
                    </div>
                  </div>
                </Col>
            }

            { //Second level
            }
            {
              //code for logical questions
              data.isLogical == true ?
                data.options && data.options.map((option, indexNew) =>
                (
                  option.isLogical == true ?
                    props.AnswerData && props.AnswerData[data.code] == option.value ?
                      option.questions && option.questions.map((question, indexSecond) =>
                        <React.Fragment key={indexSecond}>
                          <Col md={6}>
                            <div className="mb-4">
                              <div className="col21 fw500 fs15 mb-2">{question.text}</div>
                              <div className="col2 fw500 fs15">
                                {Array.isArray(props.AnswerData && props.AnswerData[question.code]) ?
                                  props.AnswerData[question.code].join(", ")
                                  :
                                  
                                    (question.code == "YOFCOMMENCEMENT" || question.code == "YOCOMPLETION") && props.AnswerData ?
                                    props.AnswerData && props.AnswerData[question.code] &&
                                    moment(props.AnswerData && props.AnswerData[question.code]).format('YYYY')
                                    :
                                    props.AnswerData && props.AnswerData[question.code]
                                }
                                

                              </div>
                            </div>
                          </Col>
                          {
                            //Third level
                            question.isLogical == true ?
                              <>   
                                {
                                  question.options && question.options.map((que, indexThird) =>
                                    <>

                                      {
                                        props.AnswerData && props.AnswerData[question.code].indexOf(que.value) > -1 ?
                                        que.questions && que.questions.map((quesss, indexFour) =>

                                      <React.Fragment key={indexFour}>
                                        <Col md={6}>
                                          <div className="mb-4">
                                            <div className="col21 fw500 fs15 mb-2">{que.value} - {quesss.text}</div>
                                            <div className="col2 fw500 fs15">
                                              {Array.isArray(props.AnswerData && props.AnswerData[quesss.code]) ?
                                                props.AnswerData[quesss.code].join(", ")
                                                :
                                                props.AnswerData && props.AnswerData[quesss.code]
                                              }

                                            </div>
                                          </div>
                                        </Col>
                                      </React.Fragment>
                                      )
                                      :
                                      ''
                                      }
                                    </>
                                  )

                                }
                              </>
                              :
                              ''
                          }
                        </React.Fragment>
                      )
                      : ''
                    : ''
                ))
                : ''
            }

          </React.Fragment>


        )
      }
    </>
  )

}


export default MyViewField;