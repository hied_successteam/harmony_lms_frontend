import React, { useState, useEffect } from 'react';
import { Col, Row, Container, Image, Form, Button, Dropdown, Modal } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import IntlTelInput from 'react-intl-tel-input';
import { Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import { CardElement, CardNumberElement, CardExpiryElement, CardCvcElement, useElements, useStripe }
  from '@stripe/react-stripe-js';

import Logos from '../../../../assets/images/logos.png';
import footerLogo from '../../../../assets/images/footerlogo.png';
import Bookmark from '../../../../assets/images/svg/BookmarkSimple.svg';
import SearchIcon from '../../../../assets/images/svg/MagnifyingGlass.svg';
import Logoadmin from '../../../../assets/images/svg/logoadmin.svg';
import bellIcon from '../../../../assets/images/svg/Bell.svg';
import UserThree from '../../../../assets/images/user3.png';
import Loader from '../../../../Lib/LoaderNew';
import { purchaseServices } from '../../../../Actions/Student/register';
import CheckoutFormService from "../wizard/EightStep/CheckoutFormService";
import { actionLogout } from '../../../../Redux/College/LoginAction';
import CONSTANTS from '../../../../Lib/Constants';
import { profile, getprofile, dashboard, logout } from '../../../../Actions/Student/register';
import { getLocalStorage, clearLocalStorage, setLocalStorage } from '../../../../Lib/Utils';
const stripePromise = loadStripe(CONSTANTS.STRIPE_PUBLISH_KEY);



const UpGradePlan = (props) => {

  const [loader, setLoader] = useState(false);
  const [servicesData, setServicesData] = useState(['1-1 Counseling Call',
    'Confirmed Admission - assessment',
    'Exam Preparation',
    'Exam Registration',
    'Essay Writing',
    'Admission Application Submission',
    'USCIS Visa Interview prep',
    'Transcript evaluation',
    'Visa Counselling',
    'Other'
  ]);

  const [serviceSeleted, setServiceSeleted] = useState([]);
  const [amount, setAmount] = useState('');
  const [errors, setErrors] = useState({});
  const [subFormClick, setSubFormClick] = useState('');
  const [valid, setValid] = useState(true);
  const [isLoading, setIsLoading] = useState(false);
  const [otherService, setOtherService] = useState(false);
  const [otherServiceText, setOtherServiceText] = useState('');
  const dispatch = useDispatch();


  const setLoaderManual = (flag) => {
    setLoader(flag);
  }


  const logoutCall = () => {
    var user = getLocalStorage('user');

    if (user && user.id) {
      logout({
        userId: user.id
      }).then(res => {
        if (res.data.success == true) {
          dispatch(actionLogout('college'));
          localStorage.clear();
          clearLocalStorage();
          props.history.push({ pathname: '/student/login' });
        } else {
          dispatch(actionLogout('college'));
          localStorage.clear();
          clearLocalStorage();
          props.history.push({ pathname: '/student/login' });

        }
      }).catch(err => {
        dispatch(actionLogout('college'));
        localStorage.clear();
        clearLocalStorage();
        props.history.push({ pathname: '/student/login' });
      });
    } else {
      dispatch(actionLogout('college'));
      localStorage.clear();
      clearLocalStorage();
      props.history.push({ pathname: '/student/login' });
    }
  }


  const goToDashboard = () => {
    props.history.push({ pathname: '/student/dashboard' });
  }


  const oncheckBox = (value) => {
    let ArryVale = serviceSeleted;
    var index = ArryVale.indexOf(value);
    if (index > -1) {
      ArryVale.splice(index, 1);
      setServiceSeleted([...ArryVale]);
      if (value == 'Other') {
        setOtherService(false);
      }
    } else {
      setServiceSeleted([...serviceSeleted, value]);
      if (value == 'Other') {
        setOtherService(true);
      }
    }
  }

  const handleSubmitSubform = () => {
    setErrors({});
    let errorsNew = {};
    let valid = true;
    if (serviceSeleted.length == 0) {
      valid = false;
      errorsNew['serviceSeleted'] = 'Please select HiEd services';
    }
    if (amount == '') {
      valid = false;
      errorsNew['amount'] = 'Please enter amount';
    } else {
      if (parseFloat(amount) < 50) {
        valid = false;
        errorsNew['amount'] = 'Please enter amount greater than or equal to $50';
        
      }
    }


    setErrors({ ...errorsNew });
    setValid(valid);
    setSubFormClick(Math.floor(Math.random() * 100000 + 1))

  }

  const submitData = (paymentMethodId) => {
    let errorsNew = {};
    let ArryVale = serviceSeleted;
    var indexOther = ArryVale.indexOf('Other');
    if (indexOther > -1) {
      ArryVale.splice(indexOther, 1); 
    }     
    let data = {
      "service": ArryVale,
      "amount": amount,
      "token": paymentMethodId,
    }
    purchaseServices(data).then(({ data: { success, data, message } }) => {
      if (success !== true) {

        setSubFormClick('');
        setLoader(false);
        setIsLoading(false);
        errorsNew['cardNumber'] = message;
        setErrors({ ...errorsNew });
        return;
      } else {
        setErrors({});
        setLoader(false);
        setIsLoading(false);
        props.history.push({
          pathname: 'successwizard',
          state: { planType: 'paid', services: data.service },
        });

      }
    })
      .catch((error) => {
        if (error && error.response.data.message) {
          errorsNew['cardNumber'] = error.response.data.message;
          setErrors({ ...errorsNew });
        }
        setLoader(false);
        setIsLoading(false);
        return;
      });

  }

  const onchangeInput = event => {
    const { target: { name, value } } = event;
    if (name == 'otherServiceText') {
      setOtherServiceText(value);
    }
  };

  const onchangeInputBlur = () => {
    var stringArray = otherServiceText.split(",").map(item => item.trim());
    setServiceSeleted([...serviceSeleted, ...stringArray]);
  }

  return (
    <div className="bgBanner w-100 payMent">
      <div className="headerTwo">
        <div className="topHeaderBox bgCol25 pl-3 pr-3">
          <Container fluid>
            <Row>
              <Col md={6}>
                <Image src={Logoadmin} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" />
                <span className="fs16 col24 fw700 ml-1">HiEd Harmony</span>
              </Col>
              <Col md={6}>
                <div className="notifications d-flex justify-content-end">
                  {/* <div>
                    <Dropdown>
                      <Dropdown.Toggle id="dropdown-basic" className="DropdownType1">
                        <Image src={bellIcon} alt="Search" className="pointer" />
                      </Dropdown.Toggle>
                      <Dropdown.Menu>
                        <Dropdown.Item href="#">Action</Dropdown.Item>
                        <Dropdown.Item href="#">Another action</Dropdown.Item>
                        <Dropdown.Item href="#">Something else</Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                  </div>
                  <div className="bookMark ml-3">
                    <span className="d-inline-block br50 text-center">
                      <Image src={UserThree} alt="Search" className="pointer user1 mr-1" />
                    </span>
                  </div> */}
                  <div className="SettingType1 ml-3 d-flex align-items-center">
                    <Dropdown>
                      <Dropdown.Toggle id="dropdown-basic" className="DropdownType1 col2 fs14 fw600">
                        <i className="fa fa-chevron-down col1" aria-hidden="true"></i>
                      </Dropdown.Toggle>
                      <Dropdown.Menu>
                        {/* <Dropdown.Item>Edit Profile</Dropdown.Item>
                        <Dropdown.Item>Setting</Dropdown.Item> */}
                        <Dropdown.Item onClick={() => goToDashboard()}>Dashboard</Dropdown.Item>
                        <Dropdown.Item onClick={() => logoutCall()}>Logout</Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                  </div>
                </div>
              </Col>
            </Row>
          </Container>
        </div>
      </div>
      <Container className="d-flex align-items-center justify-content-center mt-5 mb-5">
        <Col lg={10} md={10} className="m-auto">
          <div className="studentBox text-center bgCol3 shadow1 br10 loginUsers">
            {/* <Image src={Logos} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" />   */}
            <div className="studentOne">
              <div className="fs20 fw600 col2 mb-3">Pay for Other HiEd Services</div>
              <Form className="formLayoutUi">
                <Row>
                  <Col md={12}>
                    <div className="position-relative mb-4">
                      <div className="col4 fw600 fs18 text-left mb-2">Select HiEd services you would like to opt?</div>
                      <div className="tagType1 d-flex flex-wrap">
                        {
                          servicesData.map((item, index) => {
                            return <span
                              key={index}
                              name="services"
                              className={`br8 col4 fs14 fw500 mr-3 ${serviceSeleted && serviceSeleted.indexOf(item) > -1 ? 'bgCol35' : ''}  `}
                              value={item}
                              onClick={() => oncheckBox(item)}
                            >{item}</span>
                          })
                        }
                      </div>
                      {
                        errors && errors['serviceSeleted'] &&
                        <span className="help-block error-text">
                          <span style={{ color: "red", fontSize: 13 }}>
                            {errors['serviceSeleted']}
                          </span>
                        </span>
                      }
                    </div>
                  </Col>

                  {otherService &&
                    <Col md={12}>
                      <Form.Group className="mb-3" controlId="nameOnCard">
                        <Form.Label className="col2">
                          Other Service
                        </Form.Label>
                        <Form.Control
                          type="text"
                          name="otherServiceText"
                          value={otherServiceText}
                          placeholder="Service1, Service2"
                          className="inputType1"
                          onChange={onchangeInput}
                          autoComplete="off"
                          onBlur={onchangeInputBlur}
                        />
                        {
                          errors && errors['otherServiceText'] &&
                          <span className="help-block error-text errorBottom">
                            <span style={{ color: "red", fontSize: 13 }}>
                              {errors['otherServiceText']}
                            </span>
                          </span>
                        }
                      </Form.Group>
                    </Col>
                  }

                  <Col md={6}>
                    <div className="col4 fw600 fs16 text-left mb-2">Enter Amount to Pay</div>
                    <Form.Group controlId="amounts">
                      <Form.Label className="col2 fw400 fs14 mb-1">Amount
                      </Form.Label>
                      <div className="input-group mb-3">
                        <div className="input-group-prepend inputGroup1">
                          <span className="input-group-text" id="basic-addon1">$</span>
                        </div>
                        <Form.Control
                          type="text"
                          placeholder="Enter Amount"
                          autoComplete="off"
                          className="inputType3"
                          value={amount}
                          maxLength={10}
                          onChange={(e) =>
                            e.target.value.match('^[+0-9 ]*$') != null ?
                              setAmount(e.target.value)
                              : ''
                          }
                        />
                      </div>
                      {
                        errors && errors['amount'] &&
                        <span className="help-block error-text errorBottom">
                          <span style={{ color: "red", fontSize: 13 }}>
                            {errors['amount']}
                          </span>
                        </span>
                      }
                    </Form.Group>
                  </Col>
                  <Col md={12}>
                    <div className="col4 fw600 fs16 text-left mb-2">Enter Card Details
                    </div>
                  </Col>
                </Row>

                <Elements stripe={stripePromise}>
                  <CheckoutFormService
                    // ShowCardDetail={ShowCardDetail}
                    // isLoading={isLoading}
                    // planType={state.planType}
                    // planId={state.planId}
                    setIsLoading={setIsLoading}
                    submitData={submitData}
                    setLoaderManual={setLoaderManual}
                    valid={valid}
                    subFormClick={subFormClick}
                    cardNumberError={errors && errors['cardNumber']}
                  />
                </Elements>
                <Row>
                  <Col md={12}>
                    <div className="text-center mt-4 d-flex justify-content-center align-items-center flex-wrap align-xs-center">
                      <Button
                        className="btnType4 fw600"
                        onClick={() => handleSubmitSubform()}
                        disabled={isLoading}
                      >Pay Now</Button>
                    </div>
                  </Col>
                </Row>

              </Form>
            </div>
          </div>
        </Col>
      </Container>

      <div className="bottomFooter footerTwo d-flex align-items-center bgCol43 pl-3 pr-3">
        <div className="container-fluid">
          <Row>
            <Col md={12}>
              <div className="d-flex align-items-center">
                <Image src={footerLogo} alt="footer-logo" className="fLogo" />
                <div className="col42 fs13 ml-4">Copyright © {new Date().getFullYear()} HiEd Success | Powered by HiEd Success</div>
              </div>
            </Col>
          </Row>
        </div>
      </div>

      <Modal
        show={loader}
        //onHide={loader}
        centered
        className="loaderModal"
      >
        <Loader />
      </Modal>

    </div >
  )
}

export default UpGradePlan;