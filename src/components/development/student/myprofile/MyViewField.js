import React, { useState, useEffect } from 'react';
import { Col, Row, Image, Button, Form, Nav, Tab, Accordion, Card, Table } from 'react-bootstrap';
import { Slider, RangeSlider } from 'rsuite';
import 'rsuite/dist/styles/rsuite-default.css';
import Select from 'react-select';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import CONSTANTS from '../../../../Lib/Constants';

const MyViewField = (props) => {

  return (
    <>

      {
        props && props.QuestionData && props.QuestionData.map((data, index) =>

          <React.Fragment key={index}>

            {data.code == 'COLLSELCFACTOR' ?

              <Col md={12}>
                <div style={{ pageBreakBefore: 'always', WordBreak: 'break-all' }}></div>
                <div style={{ pagebreakBefore: 'always', WordBreak: 'break-all' }}></div>
                <div className="mb-4">
                  <div className="col21 fw500 fs15 mb-2">{data.text}</div>
                  <div className="col2 fw500 fs15">
                    <Table bordered className="tableType2 tablePreferences br10 mb-4">
                      <thead>
                        <tr>
                          <th></th>
                          <th>Not at all important</th>
                          <th>Low importance</th>
                          <th>Slightly important</th>
                          <th>Neutral</th>
                          <th>Moderately important</th>
                          <th>Very important</th>
                          <th>Extremely important</th>
                        </tr>
                      </thead>
                      <tbody>

                        {data.questions && data.questions.map((ques, index) =>

                          <tr key={`${index}aaa`}>
                            <td>
                              {ques.text}
                            </td>
                            {props.totalDefth && props.totalDefth.map((number, index) =>
                              <td key={`${index}bbb`}>
                                <Form.Group controlId={`formBasicRadio1${ques.code}${number}`}>
                                  <Form.Check
                                    type="radio"
                                    aria-label={ques.text}
                                    className="checkboxTyp3"
                                    id={`formBasicRadio1${ques.code}${number}`}
                                    name={ques.code}
                                    label=""
                                    value={number || ''}
                                    checked={props.AnswerData && props.AnswerData[ques.code] == number}
                                  />
                                </Form.Group>
                              </td>
                            )}
                          </tr>
                        )}

                      </tbody>
                    </Table>
                  </div>

                </div>
              </Col>
              :
              data.code == 'SHAREPREFERENCESPLACEMENT' ?
                <Col md={12}>
                  <div className="mb-4">
                    <div className="col21 fw500 fs15 mb-2">{data.text}</div>
                    <div className="col2 fw500 fs15">
                      <Table bordered className="tableType2 planTable br10 mb-4">
                        <thead>
                          <tr>
                            <th></th>
                            <th>Yes</th>
                            <th>No</th>
                            <th>Preferred but optional</th>
                          </tr>
                        </thead>
                        <tbody>

                          {data.questions && data.questions.map((ques, index) =>

                            <tr key={`${index}ccc`}>
                              <td>
                                {ques.text}
                              </td>
                              {props.totalDefth && props.totalDefth.map((number, index) =>
                                <td key={`${index}ddd`}>
                                  <Form.Group controlId={`formBasicRadio1${ques.code}${number}`}>
                                    <Form.Check
                                      type="radio"
                                      aria-label={ques.text}
                                      className="checkboxTyp3"
                                      id={`formBasicRadio1${ques.code}${number}`}
                                      name={ques.code}
                                      label=""
                                      value={number || ''}
                                      checked={props.AnswerData && props.AnswerData[ques.code] == number}

                                    />
                                  </Form.Group>
                                </td>
                              )}
                            </tr>
                          )}

                        </tbody>
                      </Table>
                    </div>
                  </div>
                </Col>
                :
                <>
                  {
                    data.code == "ADDRESSANDOTHERDETAIL" ? '' :
                      <>

                        {
                          data.code == "SEMESTER" ?
                            data.questions && data.questions.map((s_itm, s_ind) => (
                              <Col md={6}>
                                <div className="mb-4" key={s_ind}>
                                  <div className="col21 fw500 fs15 mb-2">{s_ind == 0 && data.text}</div>
                                  <div className='mb-2'>
                                    <div className="col21 fw500 fs15 mb-2">{s_itm.text}</div>
                                    <div className="col2 fw500 fs15">
                                      {s_itm.text == 'Gender' ?
                                        CONSTANTS.GENDER[props.AnswerData && props.AnswerData[s_itm.code]]
                                        :
                                        s_itm.text == 'DOB' ?
                                          moment(props.AnswerData && props.AnswerData[s_itm.code]).format('MM/DD/yyyy')
                                          :
                                          Array.isArray(props.AnswerData && props.AnswerData[s_itm.code]) ?

                                            s_itm.code == 'TARGETCOLLEGES' ?
                                              props.AnswerData[s_itm.code] && props.AnswerData[s_itm.code].
                                                map((data) => {
                                                  return s_itm.INSTNM
                                                }).join(", ")
                                              :
                                              props.AnswerData[s_itm.code].join(", ")
                                            :
                                            props.AnswerData && props.AnswerData[s_itm.code] ? props.AnswerData[s_itm.code] : "-"

                                      }
                                    </div>
                                  </div>
                                </div>
                              </Col>
                            ))

                            :
                            <Col md={6}>
                              <div className="mb-4">
                                <div className="col21 fw500 fs15 mb-2">{data.text}</div>
                                <div className="col2 fw500 fs15">
                                  {data.text == 'Gender' ?
                                    CONSTANTS.GENDER[props.AnswerData && props.AnswerData[data.code]]
                                    :
                                    props.AnswerData && props.AnswerData[data.code] && data.text == 'DOB' ?                                      
                                      moment(props.AnswerData && props.AnswerData[data.code]).format('MM/DD/yyyy') 
                                    :
                                  Array.isArray(props.AnswerData && props.AnswerData[data.code]) ?

                                  data.code == 'TARGETCOLLEGES' ?
                                  props.AnswerData[data.code] && props.AnswerData[data.code].
                                            map((data) => {
                                              return data.INSTNM
                                            }).join(", ")
                                  :
                                  props.AnswerData[data.code].join(", ")
                                  :
                                  props.AnswerData && props.AnswerData[data.code] ? props.AnswerData[data.code] : "-"

                                  }
                                </div>
                              </div>

                            </Col>
                        }
                      </>
                  }

                </>
            }

            { //Second level
            }
            {
              //code for logical questions
              data.isLogical == true ?
                data.options && data.options.map((option, indexNew) =>
                (
                  option.isLogical == true ?
                    props.AnswerData && (props.AnswerData[data.code] == option.value || (props.AnswerData[data.code] && props.AnswerData[data.code].length > 0 && props.AnswerData[data.code].indexOf(option.value) > -1)) ?
                      option.questions && option.questions.map((question, indexSecond) =>
                        <React.Fragment key={`${indexSecond}eee`}>
                          <Col md={6}>
                            <div className="mb-4">
                              <div className="col21 fw500 fs15 mb-2">{question.text}</div>
                              <div className="col2 fw500 fs15">
                                {Array.isArray(props.AnswerData && props.AnswerData[question.code]) ?
                                  props.AnswerData[question.code].join(", ")
                                  :

                                  (question.code == "YOFCOMMENCEMENT" || question.code == "YOCOMPLETION") && props.AnswerData ?
                                    props.AnswerData && props.AnswerData[question.code] &&
                                    moment(props.AnswerData && props.AnswerData[question.code]).format('YYYY')
                                    :

                                    (question.type == "date" || question.type == "fullDate") && props.AnswerData ?
                                      props.AnswerData && props.AnswerData[question.code] &&
                                      moment(props.AnswerData && props.AnswerData[question.code]).format('MM/DD/yyyy')
                                      :
                                      props.AnswerData && props.AnswerData[question.code] ? props.AnswerData[question.code] : '-'
                                }


                              </div>
                            </div>
                          </Col>
                          {
                            //Third level
                            question.isLogical == true ?
                              <>
                                {
                                  question.options && question.options.map((que, indexThird) =>
                                    <>
                                      {
                                        props.AnswerData && (props.AnswerData[question.code] == que.value || (Array.isArray(props.AnswerData && props.AnswerData[question.code]) && props.AnswerData[question.code] && props.AnswerData[question.code].length > 0 && props.AnswerData[question.code].indexOf(que.value) > -1)) ?

                                          que.questions && que.questions.map((quesss, indexFour) =>

                                            <React.Fragment key={`${indexFour}fff`}>
                                              <Col md={6}>
                                                <div className="mb-4">
                                                  <div className="col21 fw500 fs15 mb-2">{question.code == "ENTRANCEAPPEARED" && que.value + ' - '}{quesss.text}</div>
                                                  <div className="col2 fw500 fs15">
                                                    {Array.isArray(props.AnswerData && props.AnswerData[quesss.code]) ?
                                                      props.AnswerData[quesss.code].join(", ")
                                                      :
                                                      (quesss.type == "date" || quesss.type == "fullDate") && props.AnswerData ?
                                                        props.AnswerData && props.AnswerData[quesss.code] &&
                                                        moment(props.AnswerData && props.AnswerData[quesss.code]).format('MM/DD/yyyy')
                                                        :
                                                        props.AnswerData && props.AnswerData[quesss.code] ? props.AnswerData[quesss.code] : '-'
                                                    }

                                                  </div>
                                                </div>
                                              </Col>

                                              {/* //Fourth level */}
                                              {
                                                quesss && quesss.options && quesss.options.length > 0 &&
                                                quesss.options.map((taken_item, taken_index) => (
                                                  props.AnswerData[quesss.code] && props.AnswerData[quesss.code].length > 0 && props.AnswerData[quesss.code].indexOf(taken_item.value) > -1 &&
                                                  taken_item.questions && taken_item.questions.length > 0 && taken_item.questions.map((date_item, date_index) => (

                                                    <Col md={6}>
                                                      <div className="mb-4">
                                                        <div className="col21 fw500 fs15 mb-2">{que.value} - {date_item.text}</div>
                                                        <div className="col2 fw500 fs15">
                                                          {Array.isArray(props.AnswerData && props.AnswerData[date_item.code]) ?
                                                            props.AnswerData[date_item.code].join(", ")
                                                            :
                                                            (date_item.type == "date" || date_item.type == "fullDate") && props.AnswerData ?
                                                              props.AnswerData && props.AnswerData[date_item.code] &&
                                                              moment(props.AnswerData && props.AnswerData[date_item.code]).format('MM/DD/yyyy')
                                                              :
                                                              props.AnswerData && props.AnswerData[date_item.code] ? props.AnswerData[date_item.code] : '-'
                                                          }

                                                        </div>
                                                      </div>
                                                    </Col>
                                                  ))

                                                ))
                                              }
                                            </React.Fragment>
                                          )
                                          :
                                          ''
                                      }
                                    </>
                                  )

                                }
                              </>
                              :
                              ''
                          }
                        </React.Fragment>
                      )
                      : ''
                    : ''
                ))
                : ''
            }

            {
              //code for logical questions
              data.questions && data.type == 'categorical' && data.code == "ADDRESSANDOTHERDETAIL" &&
              <div className="tableLayout5 mb-4">
                <div className="col21 fw500 fs15 mb-2">{data.text}</div>
                <Table bordered className="tableType2 br10 mb-4">
                  <thead>
                    {
                      data.questions && data.questions.map((option, indexNew) => (
                        <tr>
                          {
                            indexNew == 0 && option.options && option.options.map((opt, ind) => (
                              <th key={indexNew + ind}>{opt.text ? opt.text : ''}</th>
                            ))
                          }
                        </tr>
                      ))
                    }
                  </thead>

                  <tbody>
                    {
                      data.questions && data.questions.map((m_item, m_index) => (
                        <tr key={index + m_index + 'p'}>
                          {

                            m_item.options && m_item.options.map((m_option, o_index) => (
                              <td key={index + m_index + o_index + 'p'} style={{ textAlign: 'center' }}>{props.AnswerData && props.AnswerData[m_option.code] ? props.AnswerData[m_option.code] : ''}</td>
                            ))
                          }
                        </tr>

                      ))
                    }
                  </tbody>
                </Table>
              </div>
            }

          </React.Fragment>


        )
      }
    </>
  )

}


export default MyViewField;