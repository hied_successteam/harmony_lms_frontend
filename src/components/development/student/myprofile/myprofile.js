import React, { useEffect, useState } from 'react';
import { Col, Row, Container, Image, Form, Dropdown, Accordion, Card, Button, Table } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import Logoadmin from '../../../../assets/images/svg/logoadmin.svg';
import ProImg from "../../../../assets/images/proImg.png";
import { getLocalStorage, clearLocalStorage, setLocalStorage } from '../../../../Lib/Utils';
import { profile, getprofile, logout } from '../../../../Actions/Student/register';
import { actionLogout } from '../../../../Redux/College/LoginAction';
import footerLogo from '../../../../assets/images/footerlogo.png';
import BackOne from '../../../../assets/images/svg/backs.svg';
import MyprofileContent from './MyprofileContent';
const MyProfile = (props) => {
  const [profileData, setProfileData] = useState({});
  const [userData, setUserData] = useState([]);
  const dispatch = useDispatch();


  useEffect(() => {
    var user = getLocalStorage('user');
    if (user && user.id) {
      setUserData(user);
      getprofile(user.id).then(res => {
        setProfileData(res.data.data);
      }).catch(err => {
      });
    }
  }, [])

  const backToDashboard = () => {
    props.history.goBack()
  }


  const logoutCall = () => {
    var user = getLocalStorage('user');

    if (user && user.id) {
      logout({
        userId: user.id
      }).then(res => {
        if (res.data.success == true) {
          dispatch(actionLogout('college'));
          localStorage.clear();
          clearLocalStorage();
          props.history.push({ pathname: '/student/login' });
        } else {
          dispatch(actionLogout('college'));
          localStorage.clear();
          clearLocalStorage();
          props.history.push({ pathname: '/student/login' });
        }
      }).catch(err => {
        dispatch(actionLogout('college'));
        localStorage.clear();
        clearLocalStorage();
        props.history.push({ pathname: '/student/login' });
      });
    } else {
      dispatch(actionLogout('college'));
      localStorage.clear();
      clearLocalStorage();
      props.history.push({ pathname: '/student/login' });
    }

  }


  return (
    <div className="collegeAdminUi">
      <Container fluid>
        <Row>
          <Col md={8} className="topHeaderBox bgCol3 pl-0 pr-0">
            <div className="pl-4 pr-4 pt-4">
              <Row className="mb-4">
                <Col md={4}>
                  <Image src={Logoadmin} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" />
                  <span className="fs20 col24 fw700 ml-1">HiEd Harmony</span>
                </Col>
              </Row>
              <div className="col2 fs20 fw500 mt-4 mb-3">
                <div style={{ width: 100, cursor: 'pointer' }} onClick={() => backToDashboard()}>
                  <Image src={BackOne} alt="back" className="pointer mr-2 back1" /> Back
                </div>
              </div>

              <MyprofileContent
                {...props}
                profileData={profileData}
              />

            </div>
          </Col>

          <Col md={4} className="topHeaderBox bgCol28 pl-0 pr-0">
            <div className="rightSidebar fixedSidebar pl-4 pr-4 pt-4 mb-5">
              <div className="d-flex justify-content-start mb-3">
                <Row className="w-100">
                  {/* <Col md={3} className="pr-0">
                    <Image src={userTwo} alt="User" />
                  </Col> */}
                  <Col md={9}>
                    <div className="col2 fw600 fs20 mt-1 mb-1">{userData.firstName + " " + userData.lastName}</div>
                    <div className="col5 fw500">{userData.email}</div>
                    <div className="col5 fw500 mb-3">
                      {profileData && profileData.basic && profileData.basic.CITY && profileData.basic.CITY + ', '}
                      {profileData && profileData.basic && profileData.basic.STATE + ', '}
                      {profileData && profileData.basic && profileData.basic.COUNTRY}
                    </div>
                  </Col>
                </Row>
                <div>
                  <Dropdown className="DropdownType2">
                    <Dropdown.Toggle id="dropdown-basic" className="DropdownType1 col2 fs14 fw600">
                      <span className="circleType3 d-inline-block br50 text-center">
                        <i className="fa fa-chevron-down col1" aria-hidden="true"></i>
                      </span>
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                      {/* <Dropdown.Item onClick={() => editProfile()}>Edit Profile</Dropdown.Item>
                      <Dropdown.Item href="#">Settings</Dropdown.Item>
                      <Dropdown.Item href="#">Help</Dropdown.Item> */}
                      <Dropdown.Item onClick={() => logoutCall()}>Logout</Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
              </div>

              <div className="profileBox1 bgCol3 p-4 mb-4">
                <Image src={ProImg} alt="Profile" className="w-100 mb-4" />
                <div className="col2 fs20 fw600 mb-4">
                  If you would like to edit your profile, contact us at</div>
                <div className="col8 fs20 fw600">
                  <a className="textcol8 fs20 fw600 mb-1" href={`mailto:${"info@hiedsuccess.com"}`}>info@hiedsuccess.com</a>
                </div>
              </div>

            </div>
          </Col>
        </Row>
      </Container>
      <div className="bottomFooter d-flex align-items-center bgCol43">
        <div className="container-fluid">
          <Row>
            <Col md={12}>
              <div className="d-flex align-items-center">
                <Image src={footerLogo} alt="footer-logo" className="fLogo" />
                <div className="col42 fs13 ml-4">Copyright © {new Date().getFullYear()} HiEd Success | Powered by HiEd Success</div>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  )
}
export default MyProfile;






