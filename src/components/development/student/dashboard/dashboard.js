import React, { useEffect, useState } from "react";
import {
  Col,
  Row,
  Container,
  Image,
  Form,
  Dropdown,
  Modal,
  Button,
  Accordion,
  Card,
} from "react-bootstrap";
import { useDispatch } from "react-redux";
import Logoadmin from "../../../../assets/images/svg/logoadmin.svg";
import SearchIcon from "../../../../assets/images/svg/MagnifyingGlass.svg";
import Bookmark from "../../../../assets/images/svg/BookmarkSimple.svg";
import DotsThreeVertical from "../../../../assets/images/svg/DotsThreeVertical.svg";
import Scale from "../../../../assets/images/Scale.png";
import Scaletwo from "../../../../assets/images/Scaletwo.png";
import hearts from "../../../../assets/images/svg/hearts.svg";
import heartfill from "../../../../assets/images/svg/heartfill.svg";
import userTwo from "../../../../assets/images/user2.png";
import AwardIcon from "../../../../assets/images/awardIcon.png";
import BellsTwo from "../../../../assets/images/Bells2.png";
import CrossTwo from "../../../../assets/images/crossOne.png";
import groupDownload from "../../../../assets/images/groupdownload.png";
import BookMarks from "../../../../assets/images/bookmarks.png";
import BrandLogoOne from "../../../../assets/images/brandlogo.png";
import BrandLogoTwo from "../../../../assets/images/brandlogo2.png";
import BrandLogoThree from "../../../../assets/images/brandlogo3.png";
import BrandLogoFour from "../../../../assets/images/brandlogo4.png";
import Stars from "../../../../assets/images/svg/stars.svg";
import StarEmpty from "../../../../assets/images/svg/starempty.svg";
import BellOne from "../../../../assets/images/svg/Bell.svg";
import mapOne from "../../../../assets/images/map1.png";
import { Slider, RangeSlider } from "rsuite";
import {
  getLocalStorage,
  clearLocalStorage,
  setLocalStorage,
} from "../../../../Lib/Utils";
import "rsuite/dist/styles/rsuite-default.css";
import {
  studentFavourite,
  getprofile,
  dashboard,
  logout,
  countryList,
  cityList,
  collegeFinterData,
  getPartnerCollegeList
} from "../../../../Actions/Student/register";
import footerLogo from "../../../../assets/images/footerlogo.png";
import Blurimg from "../../../../assets/images/blurimg.png";
import Loader from "../../../../Lib/LoaderNew";
import CONSTANTS from "../../../../Lib/Constants";
import _ from "lodash";
import ReactStars from "react-rating-stars-component";
import { isConsole } from "react-device-detect";
import TipModal from "../../../development/TipModal/TipModal";
import { actionLogout } from "../../../../Redux/College/LoginAction";
import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";
import charts from "fusioncharts/fusioncharts.charts";
import widgets from "fusioncharts/fusioncharts.widgets";
import ReactFusioncharts from "react-fusioncharts";
import ReactSpeedometer from "react-d3-speedometer";
import BoyIcon from "../../../../assets/images/svg/boy1.svg";
import ReactFC from "react-fusioncharts";
// Step 3 - Include the fusioncharts library
import FusionCharts from "fusioncharts";
// Step 4 - Include the chart type
import Column2D from "fusioncharts/fusioncharts.charts";
// Step 5 - Include the theme as fusion
// import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";s
// Step 6 - Adding the chart and theme as dependency to the core fusioncharts
import { AcademicInfo } from "../../student/wizard/WizardQuestion";
import { major } from "../../../../Actions/Student/register";
import Pagination from "react-bootstrap-4-pagination";

ReactFC.fcRoot(FusionCharts, Column2D, FusionTheme);

charts(FusionCharts);
widgets(FusionCharts);

const Dashboard = (props) => {
  const [profileData, setProfileData] = useState({});
  const [isProfileCompleted, setIsProfileCompleted] = useState(false);
  const [dashboardData, setDashboardData] = useState("");
  const [fullDashboardData, setFullDashboardData] = useState("");
  const [loader, setLoader] = useState(false);
  const [selectCollege, setSelectCollege] = useState([]);
  const [userData, setUserData] = useState("");
  const [moreToggle, setMoreToggle] = useState([]);
  const [filterAllDatas, setFilterAllDatas] = useState([]);
  const [partnerClgLen, setPartnerClgLen] = useState(0);

  const [stateFilter, setStateFilter] = useState(props.location && props.location.page_data && props.location.page_data.states ? props.location.page_data.states : "");
  const [cityFilter, setCityFilter] = useState(props.location && props.location.page_data && props.location.page_data.cities ? props.location.page_data.cities : "");
  const [coltrolsFilter, setColtrolsFilter] = useState(props.location && props.location.page_data && props.location.page_data.controls ? props.location.page_data.controls : "");
  const [iclevelsFilter, setIclevelsFilter] = useState(props.location && props.location.page_data && props.location.page_data.iclevels ? props.location.page_data.iclevels : "");
  const [fossFilter, setFossFilter] = useState(props.location && props.location.page_data && props.location.page_data.codevalue ? props.location.page_data.codevalue : "");
  const [numOfRecord, setnumOfRecord] = useState(props.location && props.location.page_data && props.location.page_data.limit ? props.location.page_data.limit : 10);
  const [currentPage, setCurrentPage] = useState(props.location && props.location.page_data && props.location.page_data.page ? props.location.page_data.page : 1);


  const [localFavourite, setLocalFavourite] = useState("");
  //const [majorFilter, setMajorFilter] = useState("");
  //const [page, setpage] = useState(1);
  //const [offset, setOffset] = useState(1);
  //const [limit, setLimit] = useState(10);
  const [totalPages, setTotalPages] = useState(1);
  const [sortValue, setSortValue] = useState("ADM_PROB");
  const [allColleges, setAllColleges] = useState(0);
  const limitArr = [10, 20, 30, 40, 50, 60, 70, 80, 90];
  //const [totalRecords, setTotalRecords] = useState(0);
  const [uppDown, setUppDown] = useState(false);
  const [comparisionData, setComparisionData] = useState([]);
  const [partnerListData, setPartnerListData] = useState([]);
  //var moreToggle = []

  const dispatch = useDispatch();

  const [tipsData, setTipsData] = useState(false);
  const [show, setShow] = useState(false);
  const [moreDetailsShow, setmoreDetailsShow] = useState(false);

  var paginationConfig = {
    totalPages: totalPages ? totalPages : 5,
    currentPage: currentPage ? currentPage : 1,
    showMax: totalPages === currentPage ? 2 : 5,
    borderColor: currentPage === 1 || totalPages === currentPage ? "#f8f9fa" : "#604BE8",
    prevText: currentPage === 1 ? "" : "<",
    nextText: totalPages === currentPage ? "" : ">",
    threeDots: true,
    prevNext: true,
    onClick: function (page) {
      window.scrollTo(0, 0);
      setCurrentPage(page);
      dashboardLinsting({
        academicInfo: profileData.academicInfo,
        basic: profileData.basic,
        collegePreference: profileData.collegePreference,
        demographicPreference: profileData.demographicPreference,
        employmentInfo: profileData.employmentInfo,
        finalPreference: profileData.finalPreference,
        placementPreference: profileData.placementPreference,
        studentId: profileData.studentId,
        page: page,
        limit: parseInt(numOfRecord),
        states: stateFilter,
        cities: cityFilter,
        controls: coltrolsFilter,
        iclevels: iclevelsFilter,
        codevalue: fossFilter,
        sort: sortValue,
        isUpdate: false
      });
      // }
    },
  };

  const handleClose = () => setShow(false);

  const handleShow = (tipsData) => {
    setTipsData(tipsData);
    setShow(true);
  };

  const handleMoreDetailsShow = (e) => {
    setmoreDetailsShow(!moreDetailsShow);
  };

  useEffect(() => {
    setLocalStorage("ad_service", false)
    window.scrollTo(0, 0);
    var user = getLocalStorage("user");
    if (user.favourite) {
      setLocalFavourite(user.favourite);
    } else {
      user.favourite = [];
      setLocalStorage("user", user);
    }
    setUserData(user);
    localStorage.removeItem("compareData");
    if (user && user.id) {
      setIsProfileCompleted(
        user.isProfileCompleted ? user.isProfileCompleted : false
      );
      setLoader(true);
      getprofile(user.id)
        .then((res) => {
          setProfileData(res.data.data);
          if (user && user.plan && user.plan.type == "free") {
            setLoader(false);
            setDashboardData([]);
          } else {
            var res_data = {
              academicInfo: res.data.data.academicInfo,
              basic: res.data.data.basic,
              collegePreference: res.data.data.collegePreference,
              demographicPreference: res.data.data.demographicPreference,
              employmentInfo: res.data.data.employmentInfo,
              finalPreference: res.data.data.finalPreference,
              placementPreference: res.data.data.placementPreference,
              studentId: res.data.data.studentId,
              page: currentPage,
              limit: parseInt(numOfRecord),
              states: stateFilter,
              cities: cityFilter,
              controls: coltrolsFilter,
              iclevels: iclevelsFilter,
              codevalue: fossFilter,
              sort: sortValue,
              isUpdate: false
            };
            dashboardLinsting(res_data);
          }
        })
        .catch((err) => {
          setLoader(false);
        });

      if (user && user.plan && user.plan.type == "paid") {
        getCollegeFilterData();
      }

    }
  }, []);

  const getCollegeFilterData = () => {
    collegeFinterData()
      .then((res) => {
        if (res.data.data) {
          setFilterAllDatas(res.data.data);
        }
      })
      .catch((err) => { });
  };

  //get college listing
  const dashboardLinsting = (params) => {
    var user = getLocalStorage("user");
    if (user && user.plan && user.plan.type == "paid") {
      window.scrollTo(0, 0);
      setLoader(true);

      dashboard(params)
        .then((res) => {
          if (res && res.data && res.data.data && res.data.data.institutions) {
            localStorage.setItem(
              "listData",
              JSON.stringify(res.data.data.institutions)
            );

            setDashboardData(res.data.data.institutions);
            setFullDashboardData(res.data.data);
            setAllColleges(res.data.totalRecords);

            if (res.data.data.total <= 10) {
              setTotalPages(1);
            } else {
              if (Number.isInteger(res.data.totalRecords / params.limit)) {
                setTotalPages(parseInt(res.data.totalRecords / params.limit));
              } else {
                setTotalPages(parseInt(res.data.totalRecords / params.limit) + 1);
              }
            }
            setTimeout(() => {
              window.scrollTo(0, 0);
              setLoader(false);
            }, 500);
            setTimeout(() => {
              window.scrollTo(0, 0);
            }, 1000);
          } else {
            setLoader(false);
          }
        }).catch((err) => {setLoader(false);});


        getPartnerCollegeList({ "studentId": user._id }).then((res) => {
          if (res && res.data && res.data.data && res.data.data.institutions) {
            setPartnerClgLen(res.data.result)
            setPartnerListData(res.data.data.institutions);
          }
        }).catch((err) => {setLoader(false);});
    }
    
  };



  const logoutCall = () => {
    var user = getLocalStorage("user");

    if (user && user.id) {
      logout({
        userId: user.id,
      })
        .then((res) => {
          if (res.data.success == true) {
            dispatch(actionLogout("college"));
            localStorage.clear();
            clearLocalStorage();
            props.history.push({ pathname: "/student/login" });
          } else {
            dispatch(actionLogout("college"));
            localStorage.clear();
            clearLocalStorage();
            props.history.push({ pathname: "/student/login" });
          }
        })
        .catch((err) => {
          dispatch(actionLogout("college"));
          localStorage.clear();
          clearLocalStorage();
          props.history.push({ pathname: "/student/login" });
        });
    } else {
      dispatch(actionLogout("college"));
      localStorage.clear();
      clearLocalStorage();
      props.history.push({ pathname: "/student/login" });
    }
  };

  const editProfile = (change) => {
    var data = getLocalStorage("user");
    data.isProfileCompleted = true;
    setLocalStorage("user", data);

    if (change) {
      props.history.push({
        pathname: "/student/wizard",
        state: { planChange: true },
      });
    } else {
      props.history.push({ pathname: "/student/wizard" });
    }
  };

  //select check box
  const oncheckBox = (value, item) => {
    let ArryVal = selectCollege;
    let comData = comparisionData
    var indexB = ArryVal.indexOf(value);
    if (indexB > -1) {
      ArryVal.splice(indexB, 1);
      setSelectCollege([...ArryVal]);
    } else {
      ArryVal.push(value);
      setSelectCollege([...ArryVal]);
    }
    //for list
    var indexBeta = comData.indexOf(item);
    if (indexBeta > -1) {
      comData.splice(indexBeta, 1);
      setComparisionData([...comData]);
    } else {
      comData.push(item);
      setComparisionData([...comData]);
    }
  }

  const onCompare = () => {
    if (selectCollege && selectCollege.length) {
      if (selectCollege.length <= 4 && selectCollege.length >= 2) {
        setLocalStorage("compareData", comparisionData);
        props.history.push({
          pathname: "/student/comparison",
          data: comparisionData,
        });
      } else {
        handleShow(
          "Less than 2 and More than 4 colleges can't compare at a time"
        );
      }
    }
  };

  const dashboardDetail = (item) => {
    setLocalStorage("dashboardDetail", item);
    var params = {
      academicInfo: profileData.academicInfo,
      basic: profileData.basic,
      collegePreference: profileData.collegePreference,
      demographicPreference: profileData.demographicPreference,
      employmentInfo: profileData.employmentInfo,
      finalPreference: profileData.finalPreference,
      placementPreference: profileData.placementPreference,
      studentId: profileData.studentId,
      page: currentPage,
      limit: parseInt(numOfRecord),
      states: stateFilter,
      cities: cityFilter,
      controls: coltrolsFilter,
      iclevels: iclevelsFilter,
      codevalue: fossFilter,
      sort: sortValue,
      isUpdate: false
    };
    props.history.push({ pathname: "/student/collegeDetail", data: item, page_data: params });
  };

  const viewMyProfile = () => {
    props.history.push({ pathname: "/student/myProfile" });
  };

  //Manage more toggle option
  const moreToggleOption = (value) => {
    let ArryVale = moreToggle;
    var indexB = ArryVale.indexOf(value);
    if (indexB > -1) {
      ArryVale.splice(indexB, 1);
      setMoreToggle([...ArryVale]);
    } else {
      ArryVale.push(value);
      setMoreToggle([...ArryVale]);
    }
  };

  //filters ///////////////////////////

  const filterValueSet = (e, status) => {
    setCurrentPage(1)
    if (status === "1") {
      // if(stateDatas && stateDatas.length > 0){
      //   stateDatas.map((item) =>{
      //     if(item.stateId == e.target.value){
      //       setStateFilter(item.name)
      //     }
      //   })
      // }
      setStateFilter(e.target.value);
      //setStateFilterID(e.target.value)
      //getCities(e.target.value)
      // setCityFilter('')
      // setCityDatas('')
    }
    if (status === "2") setCityFilter(e.target.value);
    if (status === "3") setColtrolsFilter(e.target.value);
    if (status === "4") setIclevelsFilter(e.target.value);
    if (status === "5") setFossFilter(e.target.value);
  };

  const onResetFilter = () => {
    setStateFilter("");
    setCityFilter("");
    setColtrolsFilter("");
    setIclevelsFilter("");
    setFossFilter("");
  };

  useEffect(() => {
    setLocalStorage("ad_service", false)
    var params = {
      academicInfo: profileData.academicInfo,
      basic: profileData.basic,
      collegePreference: profileData.collegePreference,
      demographicPreference: profileData.demographicPreference,
      employmentInfo: profileData.employmentInfo,
      finalPreference: profileData.finalPreference,
      placementPreference: profileData.placementPreference,
      studentId: profileData.studentId,
      page: currentPage,
      limit: parseInt(numOfRecord),
      states: stateFilter,
      cities: cityFilter,
      controls: coltrolsFilter,
      iclevels: iclevelsFilter,
      codevalue: fossFilter,
      sort: sortValue,
      isUpdate: false
    };
    if (profileData && profileData.academicInfo) {
      dashboardLinsting(params);
    }
  }, [stateFilter, cityFilter, coltrolsFilter, iclevelsFilter, fossFilter]);

  //pagination change function
  const paginationOnChange = (evnt) => {
    window.scrollTo(0, 0);
    setnumOfRecord(evnt.target.value);
    setCurrentPage(1);
    //setOffset(1);
    //setLimit(evnt.target.value);
    var params = {
      academicInfo: profileData.academicInfo,
      basic: profileData.basic,
      collegePreference: profileData.collegePreference,
      demographicPreference: profileData.demographicPreference,
      employmentInfo: profileData.employmentInfo,
      finalPreference: profileData.finalPreference,
      placementPreference: profileData.placementPreference,
      studentId: profileData.studentId,
      page: 1,
      limit: parseInt(evnt.target.value),
      states: stateFilter,
      cities: cityFilter,
      controls: coltrolsFilter,
      iclevels: iclevelsFilter,
      codevalue: fossFilter,
      sort: sortValue,
      isUpdate: false
    };
    dashboardLinsting(params);
  };

  const manageFavouriteStatus = (id) => {
    var userFavourite = localFavourite;
    if (userFavourite && userFavourite.length) {
      var response = userFavourite.find((element) => element == id);
      if (response) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  };

  const favoriteApiCall = (id, type) => {
    var data = getLocalStorage("user");
    if (data && data.favourite && data.favourite.length >= 10 && type == true) {
    } else {
      //setLoader(true);
      studentFavourite({ favouriteId: id, type })
        .then((res) => {
          if (res.data.success == true) {
            if (type === true) {
              if (data.favourite) {
                data.favourite.push(id);
              } else {
                data.favourite = [id];
              }
              setLocalFavourite(data.favourite);
              setLocalStorage("user", data);
            } else {
              let ArryVale = data.favourite;
              var indexB = ArryVale.indexOf(id);
              if (indexB > -1) {
                ArryVale.splice(indexB, 1);
                data.favourite = [...ArryVale];
                setLocalFavourite(data.favourite);
                setLocalStorage("user", data);
              }
            }
            setLoader(false);
          } else {
            setLoader(false);
          }
        })
        .catch((err) => {
          setLoader(true);
        });
    }
  };

  const goTOFavouritePage = () => {
    props.history.push({ pathname: "/student/favouritecollege" });
  };

  const sortExternalAPI = (e) => {
    setSortValue(e.target.value);
    var params = {
      academicInfo: profileData.academicInfo,
      basic: profileData.basic,
      collegePreference: profileData.collegePreference,
      demographicPreference: profileData.demographicPreference,
      employmentInfo: profileData.employmentInfo,
      finalPreference: profileData.finalPreference,
      placementPreference: profileData.placementPreference,
      studentId: profileData.studentId,
      page: 1,
      limit: parseInt(numOfRecord),
      states: stateFilter,
      cities: cityFilter,
      controls: coltrolsFilter,
      iclevels: iclevelsFilter,
      codevalue: fossFilter,
      sort: e.target.value,
      isUpdate: false
    };
    dashboardLinsting(params);
  };



  const accordianClick = () => {
    uppDown ? setUppDown(false) : setUppDown(true)
  }

  const addService = () => {
    setLocalStorage("ad_service", true)
    props.history.push({ pathname: "/student/upgradePlan" })
  }

  return (
    <div className="collegeAdminUi">
      <Container fluid>
        <Row>
          <Col md={8} className="topHeaderBox bgCol3 pl-0 pr-0">
            <div className="pl-4 pr-4 pt-4">
              <Row className="mb-4">
                <Col md={4}>
                  <Image
                    src={Logoadmin}
                    alt="HiEd Success"
                    title="Welecome to HiEd Success"
                    className="logos"
                  />
                  <span className="fs20 col24 fw700 ml-1">HiEd Harmony</span>
                </Col>
                <Col md={5}>
                  {/* <div className="searchBox d-flex justify-content-center">
                    <Form.Group className="position-relative w-100">
                      <Form.Control type="text" placeholder="Search..." className="searchType1 shadow3 w-100" />
                      <Image src={SearchIcon} alt="Search" className="pointer searchOne" />
                    </Form.Group>
                  </div> */}
                </Col>
                <Col md={3}>
                  {
                    userData &&
                    userData.plan &&
                    userData.plan.type == "paid" &&
                    <div className="notifications d-flex justify-content-end">
                      <div className="bookMark ml-4">
                        <span className="d-inline-block bgCol1 br50 text-center">
                          <Image
                            src={hearts}
                            alt="Search"
                            className="pointer"
                            onClick={() => goTOFavouritePage()}
                          />
                        </span>
                      </div>
                    </div>
                  }

                </Col>
              </Row>
              <div className="middileAccordian mb-5">
                {
                  userData &&
                  userData.plan &&
                  userData.plan.type == "paid" &&
                  <Accordion style={{ marginBottom: 20 }}>
                    <Card className="shadow4">
                      <Card.Header onClick={() => accordianClick()}>
                        <Accordion.Toggle variant="link" eventKey="0">
                          Filters{" "}
                          <span
                            className="ml-4 col8"
                            onClick={() => onResetFilter()}
                          >
                            Reset
                          </span>{" "}
                          <i
                            className={`fa fa-chevron-down arrowA ${uppDown ? "rotateArrow" : ""} `}
                            aria-hidden="true"
                          ></i>
                        </Accordion.Toggle>
                      </Card.Header>
                      <Accordion.Collapse eventKey="0">
                        <Card.Body className="bgCol3">
                          <div>
                            <Row>
                              <Col>
                                <Form.Group
                                  controlId="exampleOne"
                                  className="mb-3"
                                >
                                  <Form.Label className="fw600 col2 mb-2">
                                    <span className="mr-3">State</span>
                                  </Form.Label>
                                  <Form.Control
                                    as="select"
                                    className="selectTyp1 select2 pointer"
                                    value={stateFilter || ""}
                                    onChange={(e) => filterValueSet(e, "1")}
                                  >
                                    <option value="">Select</option>
                                    {filterAllDatas.states &&
                                      filterAllDatas.states.length &&
                                      filterAllDatas.states.map(
                                        (f_item, index) => (
                                          <option
                                            key={index}
                                            value={f_item.value}
                                          >
                                            {f_item.value}
                                          </option>
                                        )
                                      )}
                                  </Form.Control>
                                </Form.Group>
                              </Col>
                              <Col>
                                <Form.Group
                                  controlId="exampleOne"
                                  className="mb-3"
                                >
                                  <Form.Label className="fw600 col2 mb-2">
                                    <span className="mr-3">City</span>
                                  </Form.Label>
                                  <Form.Control
                                    as="select"
                                    className="selectTyp1 select2 pointer"
                                    value={cityFilter || ""}
                                    onChange={(e) => filterValueSet(e, "2")}
                                  >
                                    <option value="">Select</option>
                                    {filterAllDatas.cities &&
                                      filterAllDatas.cities.length &&
                                      filterAllDatas.cities.map(
                                        (f_item, index) => (
                                          <option
                                            key={index}
                                            value={f_item.value}
                                          >
                                            {f_item.value}
                                          </option>
                                        )
                                      )}
                                  </Form.Control>
                                </Form.Group>
                              </Col>
                              <Col>
                                <Form.Group
                                  controlId="exampleOne"
                                  className="mb-3"
                                >
                                  <Form.Label className="fw600 col2 mb-2">
                                    <span className="mr-3">College Type</span>
                                  </Form.Label>
                                  <Form.Control
                                    as="select"
                                    className="selectTyp1 select2 pointer"
                                    value={coltrolsFilter || ""}
                                    onChange={(e) => filterValueSet(e, "3")}
                                  >
                                    <option value="">Select</option>
                                    {filterAllDatas.controls &&
                                      filterAllDatas.controls.length &&
                                      filterAllDatas.controls.map(
                                        (f_item, index) => (
                                          <option
                                            key={index}
                                            value={f_item.value}
                                          >
                                            {f_item.value}
                                          </option>
                                        )
                                      )}
                                  </Form.Control>
                                </Form.Group>
                              </Col>
                              <Col>
                                <Form.Group
                                  controlId="exampleOne"
                                  className="mb-3"
                                >
                                  <Form.Label className="fw600 col2 mb-2">
                                    <span className="mr-3">College Duration</span>
                                  </Form.Label>
                                  <Form.Control
                                    as="select"
                                    className="selectTyp1 select2 pointer"
                                    value={iclevelsFilter || ""}
                                    onChange={(e) => filterValueSet(e, "4")}
                                  >
                                    <option value="">Select</option>
                                    {filterAllDatas.iclevels &&
                                      filterAllDatas.iclevels.length &&
                                      filterAllDatas.iclevels.map(
                                        (f_item, index) => (
                                          <option
                                            key={index}
                                            value={f_item.value}
                                          >
                                            {f_item.value}
                                          </option>
                                        )
                                      )}
                                  </Form.Control>
                                </Form.Group>
                              </Col>
                              <Col>
                                <Form.Group
                                  controlId="exampleOne"
                                  className="mb-3"
                                >
                                  <Form.Label className="fw600 col2 mb-2">
                                    <span className="mr-3">Field Of Study</span>
                                  </Form.Label>
                                  <Form.Control
                                    as="select"
                                    className="selectTyp1 select2 pointer"
                                    value={fossFilter || ""}
                                    onChange={(e) => filterValueSet(e, "5")}
                                  >
                                    <option value="">Select</option>
                                    {filterAllDatas.foss &&
                                      filterAllDatas.foss.length &&
                                      filterAllDatas.foss.map(
                                        (f_item, index) => (
                                          <option
                                            key={index}
                                            value={f_item.id}
                                          >
                                            {f_item.value}
                                          </option>
                                        )
                                      )}
                                  </Form.Control>
                                </Form.Group>
                              </Col>
                            </Row>
                          </div>
                        </Card.Body>
                      </Accordion.Collapse>
                    </Card>
                  </Accordion>
                }


                <div>
                  {/* <div className="mb-4 bgCol30 p-2 br2">
                    <Row>
                      <Col md={10}>
                        <div className="d-flex flex-wrap">
                          <div className="mr-3">
                            <Image src={BellsTwo} alt="Bell Icon" className="pointer ml-1" />
                          </div>
                          <div>
                            <div className="fw500 col2">Profile Update</div>
                            <div className="fs14 fw400 col2">
                              Update your profile to improve your search!
                              <span className="ml-2 fw700 col2">Update</span>
                            </div>
                          </div>
                        </div>
                      </Col>
                      <Col md={2}>
                        <div className="text-right align-items-center mt-2">
                          <Image src={CrossTwo} alt="Bell Icon" className="pointer mr-3" />
                        </div>
                      </Col>
                    </Row>
                  </div> */}



                  <div className="mb-4">
                    <Row>
                      <Col md={5}>
                        <div className="col2 fs22 fw500">
                          Partner Colleges
                          <span className="ml-2 mr-3">
                            ({partnerClgLen && partnerClgLen})
                          </span>
                          {/* <Image src={groupDownload} alt="Icon" className="pointer" /> */}
                        </div>
                        <div className="col5 fs14 fw400">
                          Based on your profile best suitable colleges
                        </div>
                      </Col>
                      <Col md={7}>
                        <div className="d-flex flex-wrap justify-content-end mt-1">
                          {
                            userData &&
                            userData.plan &&
                            userData.plan.type == "paid" &&
                            <>
                              <div>
                                <Button
                                  type="button"
                                  disabled={
                                    selectCollege && selectCollege.length > 0
                                      ? false
                                      : true
                                  }
                                  className={`btnType5`}
                                  onClick={() => onCompare()}
                                >
                                  Compare
                                </Button>
                              </div>
                            </>
                          }
                        </div>
                      </Col>
                    </Row>
                  </div>

                  {/* partner college data */}
                  {partnerListData &&
                    partnerListData.map((item, index) => {
                      return (
                        <React.Fragment key={index}>
                          <Row key={index}>
                            <Col md={6} className="pr-md-0">
                              <div className="d-flex">
                                <input type="checkbox"
                                  checked={selectCollege &&
                                    selectCollege.length &&
                                    selectCollege.indexOf(item._id) > -1 ? true : false}
                                  onChange={() => oncheckBox(item._id, item)}
                                  className="checkboxTyp5" />
                                <div className="squareBox1 bgLight d-flex justify-content-center align-items-center mr-3">
                                  {
                                    item.IMAGEPATH ? (
                                      <Image
                                        src={
                                          CONSTANTS.CLIENT_SERVER_IMAGEPATH +
                                          item.IMAGEPATH
                                        }
                                        alt="Brand Logo"
                                      />
                                    ) : (
                                      <Image
                                        src={
                                          "https://edulytics.hiedsuccess.com/logo.png"
                                        }
                                        alt="Brand Logo"
                                        style={{
                                          width: "100%",
                                          height: "100%",
                                        }}
                                      />
                                    )
                                    // <div className="text-center">
                                    //   <div className="col2 fw500 fs15">HiEd Harmony</div>
                                    //   <div className="col2 fw500 fs14">No Image </div>
                                    // </div>
                                  }
                                </div>
                                <div>
                                  <div className="col2 fw600 fs18 mb-1 titleFour">
                                    {item.INSTNM}
                                  </div>

                                  <div className="col2 fw600 fs12 mb-1 text-uppercase">
                                    <span>{item.CONTROL ? item.CONTROL : ''}</span>
                                    <span className="ml-3">
                                      <ReactStars
                                        count={5}
                                        size={14}
                                        edit={false}
                                        isHalf={true}
                                        value={parseFloat(item.RATING)}
                                        emptyIcon={
                                          <i className="far fa-star"></i>
                                        }
                                        halfIcon={
                                          <i className="fa fa-star-half-alt"></i>
                                        }
                                        fullIcon={
                                          <i className="fa fa-star"></i>
                                        }
                                        activeColor="#ffd700"
                                      />
                                    </span>
                                  </div>

                                  <div className="col5 fs14 fw500 mb-3 titleFour">
                                    {item.ADDR +
                                      ", " +
                                      item.CITY +
                                      ", " +
                                      item.STABBR}
                                  </div>
                                  <div className="d-flex flex-wrap">
                                    <div className="mr-2">
                                      {
                                        manageFavouriteStatus(item.UNITID) ? (
                                          <span className="d-inline-block wishListOne bgCol1 grayCol br50 text-center">
                                            <Image
                                              src={hearts}
                                              alt="Search"
                                              className="pointer"
                                              onClick={() =>
                                                favoriteApiCall(
                                                  item.UNITID,
                                                  false
                                                )
                                              }
                                            />
                                          </span>
                                        ) : (
                                          // <Button type="button" className="btnType5 btnsmall5" onClick={() => favoriteApiCall(item.UNITID, false)}>Mark as Unfavorite</Button>
                                          <span className="d-inline-block wishListOne grayCol br50 text-center">
                                            <Image
                                              src={hearts}
                                              alt="Search"
                                              className="pointer"
                                              onClick={() =>
                                                favoriteApiCall(
                                                  item.UNITID,
                                                  true
                                                )
                                              }
                                            />
                                          </span>
                                        )
                                        // <Button type="button" className="btnType6 btnsmall6" onClick={() => favoriteApiCall(item.UNITID, true)}>Mark as Favorite</Button>
                                      }
                                    </div>
                                    <div className="mr-2">
                                      <Button
                                        type="button"
                                        className="btnType5 btnsmall5"
                                        onClick={() => dashboardDetail(item)}
                                      >
                                        View Details
                                      </Button>
                                    </div>
                                    <div>
                                      <Dropdown>
                                        <Dropdown.Toggle
                                          id="dropdown-basic2"
                                          className="DropdownType1 col2 fs14 fw600 pt-0 pb-0"
                                          onClick={() =>
                                            moreToggleOption(item._id)
                                          }
                                        >
                                          More
                                          <i
                                            className="fa fa-caret-down col2 ml-1"
                                            aria-hidden="true"
                                          ></i>
                                        </Dropdown.Toggle>
                                      </Dropdown>
                                    </div>

                                    {/* <div>
                                    <Dropdown className="DropdownType2">
                                      <Dropdown.Toggle id="dropdown-basic2" className="DropdownType1 mt-1 col2 fs14 fw600 pt-0 pb-0">
                                        More
                                      <i className="fa fa-caret-down col2 ml-1" aria-hidden="true"></i>
                                      </Dropdown.Toggle>
                                      <Dropdown.Menu>
                                        <Dropdown.Item href="#">Profile</Dropdown.Item>
                                        <Dropdown.Item href="#">Setting</Dropdown.Item>
                                        <Dropdown.Item href="#">Logout</Dropdown.Item>
                                      </Dropdown.Menu>
                                    </Dropdown>
                                  </div> */}
                                  </div>
                                </div>
                              </div>
                            </Col>
                            <Col md={6} className="row min15">
                              <Col md={6} className="speedMeter pr0">
                                <ReactSpeedometer
                                  width={190}
                                  maxheight={100}
                                  height={115}
                                  needleHeightRatio={0.7}
                                  value={Math.round(item.ADM_PROB)}
                                  maxValue={100}
                                  customSegmentStops={[0, 33, 66, 100]}
                                  segmentColors={[
                                    "#FE5E54",
                                    "#FFDB1B",
                                    "#89D667",
                                  ]}
                                  currentValueText={"${value}%"}
                                  segments={3}
                                  maxSegmentLabels={0}
                                  ringWidth={10}
                                  needleTransitionDuration={33}
                                  needleTransition="easeElastic"
                                  needleColor={"#a7ff83"}
                                  textColor={"#000000"}
                                />
                                <span className="titleGraph">
                                  Admission Probability
                                </span>
                              </Col>

                              <Col md={6} className="speedMeter pr0">
                                <ReactSpeedometer
                                  width={190}
                                  height={115}
                                  maxheight={100}
                                  needleHeightRatio={0.7}
                                  value={Math.round(item.PROF_MATCH)}
                                  maxValue={100}
                                  customSegmentStops={[0, 33, 66, 100]}
                                  segmentColors={[
                                    "#FE5E54",
                                    "#FFDB1B",
                                    "#89D667",
                                  ]}
                                  currentValueText={"${value}%"}
                                  segments={3}
                                  maxSegmentLabels={0}
                                  ringWidth={10}
                                  needleTransitionDuration={33}
                                  needleTransition="easeElastic"
                                  needleColor={"#a7ff83"}
                                  textColor={"#000000"}
                                />
                                <span className="titleGraph">
                                  Profile Compatibility
                                </span>
                              </Col>
                            </Col>

                            {moreToggle &&
                              moreToggle.length > 0 &&
                              moreToggle.indexOf(item._id) > -1 && (
                                <React.Fragment>
                                  <Col md={2}></Col>
                                  <Col md={10} className="pl-md-0">
                                    <div className="popularType1">
                                      <ul className="d-flex flex-wrap mt-3 mb-3 pl-1 w-100">
                                        {
                                          item.MATCHED_TAGS && item.MATCHED_TAGS.length > 0 && item.MATCHED_TAGS.map((m_tag) => {

                                            return m_tag && <li>{m_tag}</li>
                                          })
                                        }
                                        {
                                          item.EXTRA_TAGS && item.EXTRA_TAGS.length > 0 && item.EXTRA_TAGS.map((e_tag) => {
                                            return e_tag && <li className="active">{e_tag}</li>
                                          })
                                        }
                                      </ul>
                                      <div className="col2 fs14 fw500 mb-1">{item.pop_fields && item.pop_fields.length > 0 && "Popular Majors"}</div>
                                      <div className="popularType2 d-flex flex-wrap">
                                        {
                                          item.pop_fields && item.pop_fields.length > 0 && item.pop_fields.map((p_major) => {
                                            return <div className="text-uppercase d-inline-block mr-2 mb-2 bgCol11 br30 col10 fw500 fs12">{p_major}</div>
                                          })
                                        }
                                      </div>
                                    </div>
                                  </Col>
                                </React.Fragment>
                              )}

                            <hr className="borderOne" />
                          </Row>
                          <hr className="borderOne" />
                        </React.Fragment>
                      );
                    })}




                  <div className="mb-4">
                    <Row>
                      <Col md={5}>
                        <div className="col2 fs22 fw500">
                          Suggested Colleges
                          <span className="ml-2 mr-3">
                            ({allColleges && allColleges})
                          </span>
                          {/* <Image src={groupDownload} alt="Icon" className="pointer" /> */}
                        </div>
                        <div className="col5 fs14 fw400">
                          Based on your profile best suitable colleges
                        </div>
                      </Col>
                      <Col md={7}>
                        <div className="d-flex flex-wrap justify-content-end mt-1">
                          {
                            userData &&
                            userData.plan &&
                            userData.plan.type == "paid" &&
                            <>
                              <div className="mr-1 fs14 fw600 col2 pt-1">
                                Sort by:
                              </div>

                              <div className="mr-3">
                                <Form.Control
                                  as="select"
                                  className="selectTyp1 selectNumber1"
                                  onChange={(e) => sortExternalAPI(e)}
                                  value={sortValue}
                                >
                                  <option value="ADM_PROB">
                                    Admission Probability
                                  </option>
                                  <option value="PROF_MATCH">
                                    Profile Compatibility
                                  </option>
                                  {/* <option value="RANK">College Ranking</option> */}
                                </Form.Control>
                              </div>
                            </>
                          }


                        </div>
                      </Col>
                    </Row>
                  </div>

                  {/* Dumy data */}

                  {dashboardData.length <= 0 &&
                    userData &&
                    userData.plan &&
                    userData.plan.type == "free" && (
                      <React.Fragment>
                        <Row>
                          <Col md={8}>
                            <div className="blurBox">
                              <Image
                                src={Blurimg}
                                className="w-100"
                                alt="Blur"
                              />
                            </div>
                          </Col>
                          <Col md={4}>
                            <Row>
                              <Col md={6}>
                                <Image
                                  src={Scale}
                                  alt="Icon"
                                // className="pointer"
                                />
                              </Col>
                              <Col md={6}>
                                <Image
                                  src={Scaletwo}
                                  alt="Icon"
                                // className="pointer"
                                />
                              </Col>
                            </Row>


                          </Col>
                          <hr className="borderOne" />
                        </Row>
                        <Row style={{ marginTop: 120 }}>
                          <Col md={8}>
                            <div className="blurBox">
                              <Image
                                src={Blurimg}
                                className="w-100"
                                alt="Blur"
                              />
                            </div>
                          </Col>
                          <Col md={4}>
                            <Row>
                              <Col md={6}>
                                <Image
                                  src={Scale}
                                  alt="Icon"
                                // className="pointer"
                                />
                              </Col>
                              <Col md={6}>
                                <Image
                                  src={Scaletwo}
                                  alt="Icon"
                                //className="pointer"
                                />
                              </Col>
                            </Row>
                          </Col>
                          <hr className="borderOne" />
                        </Row>
                      </React.Fragment>
                    )}

                  {dashboardData &&
                    dashboardData.map((item, index) => {
                      return (
                        <React.Fragment key={index}>
                          <Row key={index}>
                            <Col md={6} className="pr-md-0">
                              <div className="d-flex">
                                <input type="checkbox"
                                  checked={selectCollege &&
                                    selectCollege.length &&
                                    selectCollege.indexOf(item._id) > -1 ? true : false}
                                  onChange={() => oncheckBox(item._id, item)}
                                  className="checkboxTyp5" />
                                <div className="squareBox1 bgLight d-flex justify-content-center align-items-center mr-3">
                                  {
                                    item.IMAGEPATH ? (
                                      <Image
                                        src={
                                          CONSTANTS.CLIENT_SERVER_IMAGEPATH +
                                          item.IMAGEPATH
                                        }
                                        alt="Brand Logo"
                                      />
                                    ) : (
                                      <Image
                                        src={
                                          "https://edulytics.hiedsuccess.com/logo.png"
                                        }
                                        alt="Brand Logo"
                                        style={{
                                          width: "100%",
                                          height: "100%",
                                        }}
                                      />
                                    )
                                    // <div className="text-center">
                                    //   <div className="col2 fw500 fs15">HiEd Harmony</div>
                                    //   <div className="col2 fw500 fs14">No Image </div>
                                    // </div>
                                  }
                                </div>
                                <div>
                                  <div className="col2 fw600 fs18 mb-1 titleFour">
                                    {item.INSTNM}
                                  </div>

                                  <div className="col2 fw600 fs12 mb-1 text-uppercase">
                                    <span>{item.CONTROL ? item.CONTROL : ''}</span>
                                    <span className="ml-3">
                                      <ReactStars
                                        count={5}
                                        size={14}
                                        edit={false}
                                        isHalf={true}
                                        value={parseFloat(item.RATING)}
                                        emptyIcon={
                                          <i className="far fa-star"></i>
                                        }
                                        halfIcon={
                                          <i className="fa fa-star-half-alt"></i>
                                        }
                                        fullIcon={
                                          <i className="fa fa-star"></i>
                                        }
                                        activeColor="#ffd700"
                                      />
                                    </span>
                                  </div>

                                  <div className="col5 fs14 fw500 mb-3 titleFour">
                                    {item.ADDR +
                                      ", " +
                                      item.CITY +
                                      ", " +
                                      item.STABBR}
                                  </div>
                                  <div className="d-flex flex-wrap">
                                    <div className="mr-2">
                                      {
                                        manageFavouriteStatus(item.UNITID) ? (
                                          <span className="d-inline-block wishListOne bgCol1 grayCol br50 text-center">
                                            <Image
                                              src={hearts}
                                              alt="Search"
                                              className="pointer"
                                              onClick={() =>
                                                favoriteApiCall(
                                                  item.UNITID,
                                                  false
                                                )
                                              }
                                            />
                                          </span>
                                        ) : (
                                          // <Button type="button" className="btnType5 btnsmall5" onClick={() => favoriteApiCall(item.UNITID, false)}>Mark as Unfavorite</Button>
                                          <span className="d-inline-block wishListOne grayCol br50 text-center">
                                            <Image
                                              src={hearts}
                                              alt="Search"
                                              className="pointer"
                                              onClick={() =>
                                                favoriteApiCall(
                                                  item.UNITID,
                                                  true
                                                )
                                              }
                                            />
                                          </span>
                                        )
                                        // <Button type="button" className="btnType6 btnsmall6" onClick={() => favoriteApiCall(item.UNITID, true)}>Mark as Favorite</Button>
                                      }
                                    </div>
                                    <div className="mr-2">
                                      <Button
                                        type="button"
                                        className="btnType5 btnsmall5"
                                        onClick={() => dashboardDetail(item)}
                                      >
                                        View Details
                                      </Button>
                                    </div>
                                    <div>
                                      <Dropdown>
                                        <Dropdown.Toggle
                                          id="dropdown-basic2"
                                          className="DropdownType1 col2 fs14 fw600 pt-0 pb-0"
                                          onClick={() =>
                                            moreToggleOption(item._id)
                                          }
                                        >
                                          More
                                          <i
                                            className="fa fa-caret-down col2 ml-1"
                                            aria-hidden="true"
                                          ></i>
                                        </Dropdown.Toggle>
                                      </Dropdown>
                                    </div>

                                    {/* <div>
                                    <Dropdown className="DropdownType2">
                                      <Dropdown.Toggle id="dropdown-basic2" className="DropdownType1 mt-1 col2 fs14 fw600 pt-0 pb-0">
                                        More
                                      <i className="fa fa-caret-down col2 ml-1" aria-hidden="true"></i>
                                      </Dropdown.Toggle>
                                      <Dropdown.Menu>
                                        <Dropdown.Item href="#">Profile</Dropdown.Item>
                                        <Dropdown.Item href="#">Setting</Dropdown.Item>
                                        <Dropdown.Item href="#">Logout</Dropdown.Item>
                                      </Dropdown.Menu>
                                    </Dropdown>
                                  </div> */}
                                  </div>
                                </div>
                              </div>
                            </Col>
                            <Col md={6} className="row min15">
                              <Col md={6} className="speedMeter pr0">
                                <ReactSpeedometer
                                  width={190}
                                  maxheight={100}
                                  height={115}
                                  needleHeightRatio={0.7}
                                  value={Math.round(item.ADM_PROB)}
                                  maxValue={100}
                                  customSegmentStops={[0, 33, 66, 100]}
                                  segmentColors={[
                                    "#FE5E54",
                                    "#FFDB1B",
                                    "#89D667",
                                  ]}
                                  currentValueText={"${value}%"}
                                  segments={3}
                                  maxSegmentLabels={0}
                                  ringWidth={10}
                                  needleTransitionDuration={33}
                                  needleTransition="easeElastic"
                                  needleColor={"#a7ff83"}
                                  textColor={"#000000"}
                                />
                                <span className="titleGraph">
                                  Admission Probability
                                </span>
                              </Col>

                              <Col md={6} className="speedMeter pr0">
                                <ReactSpeedometer
                                  width={190}
                                  height={115}
                                  maxheight={100}
                                  needleHeightRatio={0.7}
                                  value={Math.round(item.PROF_MATCH)}
                                  maxValue={100}
                                  customSegmentStops={[0, 33, 66, 100]}
                                  segmentColors={[
                                    "#FE5E54",
                                    "#FFDB1B",
                                    "#89D667",
                                  ]}
                                  currentValueText={"${value}%"}
                                  segments={3}
                                  maxSegmentLabels={0}
                                  ringWidth={10}
                                  needleTransitionDuration={33}
                                  needleTransition="easeElastic"
                                  needleColor={"#a7ff83"}
                                  textColor={"#000000"}
                                />
                                <span className="titleGraph">
                                  Profile Compatibility
                                </span>
                              </Col>
                            </Col>

                            {moreToggle &&
                              moreToggle.length > 0 &&
                              moreToggle.indexOf(item._id) > -1 && (
                                <React.Fragment>
                                  <Col md={2}></Col>
                                  <Col md={10} className="pl-md-0">
                                    <div className="popularType1">
                                      <ul className="d-flex flex-wrap mt-3 mb-3 pl-1 w-100">
                                        {
                                          item.MATCHED_TAGS && item.MATCHED_TAGS.length > 0 && item.MATCHED_TAGS.map((m_tag) => {

                                            return m_tag && <li>{m_tag}</li>
                                          })
                                        }
                                        {
                                          item.EXTRA_TAGS && item.EXTRA_TAGS.length > 0 && item.EXTRA_TAGS.map((e_tag) => {
                                            return e_tag && <li className="active">{e_tag}</li>
                                          })
                                        }
                                      </ul>
                                      <div className="col2 fs14 fw500 mb-1">{item.pop_fields && item.pop_fields.length > 0 && "Popular Majors"}</div>
                                      <div className="popularType2 d-flex flex-wrap">
                                        {
                                          item.pop_fields && item.pop_fields.length > 0 && item.pop_fields.map((p_major) => {
                                            return <div className="text-uppercase d-inline-block mr-2 mb-2 bgCol11 br30 col10 fw500 fs12">{p_major}</div>
                                          })
                                        }
                                      </div>
                                    </div>
                                  </Col>
                                </React.Fragment>
                              )}

                            <hr className="borderOne" />
                          </Row>
                          <hr className="borderOne" />
                        </React.Fragment>
                      );
                    })}




                  {
                    dashboardData && dashboardData.length > 0 && allColleges > 10 &&
                    <div className="d-flex pl-4 pr-4 mt-5 mb-5 justify-content-between">
                      <div className="d-flex align-items-center">
                        <div className="mr-2">Show</div>
                        <Form.Control
                          as="select"
                          className="selectTyp1 selectNumber1"
                          onChange={(e) => paginationOnChange(e)}
                          value={numOfRecord}
                        >
                          {limitArr.map((cVal, cIndx) => {
                            return <option key={cIndx} value={cVal}>{cVal}</option>
                          })}
                        </Form.Control>
                      </div>
                      <div className="paginationType4">
                        <Pagination {...paginationConfig} />
                      </div>
                    </div>
                  }






                </div>

                {/* { loader && <div className="loaderMiddle loaderModal"><Loader /></div> } */}

                <div className="mapType1 d-none">
                  <Image src={mapOne} className="w-100" />
                </div>
              </div>


            </div>
          </Col>

          <Col md={4} className="topHeaderBox bgCol28 pl-0 pr-0">
            <div className="rightSidebar pl-4 pr-4 pt-4 mb-5">
              <div className="d-flex justify-content-end mb-3">
                <Row className="w-100">
                  <Col md={9} className="pl-0">
                    <div className="col2 fw600 fs20 mt-1 mb-1">
                      {profileData &&
                        profileData.basic &&
                        profileData.basic.NAME
                        ? profileData.basic.NAME
                        : ""}
                    </div>
                    <div className="col5 fw500">
                      {profileData &&
                        profileData.basic &&
                        profileData.basic.EMAIL
                        ? profileData.basic.EMAIL
                        : ""}
                    </div>
                    <div className="col5 fw500 mb-3">
                      {profileData &&
                        profileData.basic &&
                        profileData.basic.CITY &&
                        profileData.basic.CITY + ", "}
                      {profileData &&
                        profileData.basic &&
                        profileData.basic.STATE + ", "}
                      {profileData &&
                        profileData.basic &&
                        profileData.basic.COUNTRY}
                    </div>
                    <div className="fw400 col2 fs14">
                      <div className="col5 fw500 fs14 mb-1">Course Level:</div>
                      <span className="fw500 fs17">
                        {" "}
                        {profileData &&
                          profileData.academicInfo &&
                          profileData.academicInfo.HIGHDEGEDU
                          ? profileData.academicInfo.HIGHDEGEDU
                          : ""}
                      </span>
                    </div>
                    <div className="fw400 col2 fs14" style={{ marginTop: 10 }}>
                      <div className="col5 fw500 fs14 mb-1">Current Plan:</div>
                      <span className="fw500 fs17">
                        {" "}
                        {profileData &&
                          profileData.user &&
                          profileData.user.plan &&
                          profileData.user.plan.name
                          ? profileData.user.plan.name
                          : ""}
                      </span>
                    </div>


                  </Col>
                </Row>
                <div>
                  <Dropdown className="DropdownType2">
                    <Dropdown.Toggle
                      id="dropdown-basic"
                      className="DropdownType1 col2 fs14 fw600"
                    >
                      <span className="circleType3 d-inline-block br50 text-center">
                        <i
                          className="fa fa-chevron-down col1"
                          aria-hidden="true"
                        ></i>
                      </span>
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                      <Dropdown.Item onClick={() => editProfile()}>
                        Edit Profile
                      </Dropdown.Item>
                      {/* <Dropdown.Item href="#">Settings</Dropdown.Item>
                      <Dropdown.Item href="#">Help</Dropdown.Item> */}
                      <Dropdown.Item onClick={() => viewMyProfile()}>
                        View Profile
                      </Dropdown.Item>
                      <Dropdown.Item onClick={() => logoutCall()}>
                        Logout
                      </Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
              </div>

              {/* <div className="text-center mb-4">
                <div className="col2 fs22 fw600 mb-2">Northwestern Oklahoma State University</div>
                <div className="col29 fw500">709 Oklahoma Blvd., Alva, OK 73717, United States</div>
              </div> */}
              <div className="boxLayout bgCol48 br4 p-3 mt-3 mb-4">
                {/* <Row>
                  <Col md={3}>
                    <div className="d-flex justify-content-center align-items-center mt-2">
                      <div className="text-center">
                        <div className="col3 fw700 fs28">100%</div>
                        <div className="col3 fs14 fw500">Done</div>
                      </div>
                    </div>
                  </Col>
                  <Col md={9}>
                    <div className="col3 fw400 fs18">The more complete your profile, the better we can match you with an institution.</div>
                  </Col>
                </Row> */}
                <div className="col2 fw500 fs18 mb-3">
                  Change your plans to get more offers and benefits for your
                  career growth
                </div>
                <Button
                  className="btnType5 upgradeBtn"
                  onClick={() => editProfile("change")}
                >
                  Change Plan
                </Button>
              </div>

              <div className="col5 fw500 fs14 mb-1">Scores & Ranks</div>
              <div className="d-flex flex-wrap mb-4">
                {
                  profileData && profileData.academicInfo && (!profileData.academicInfo.SAT_TOTAL && !profileData.academicInfo.PSAT_NMSQT_TOTAL && !profileData.academicInfo.TOEFL_TOTAL && !profileData.academicInfo.GRE_TOTAL && !profileData.academicInfo.IELTS_BANDSCORE && !profileData.academicInfo.ACT_COMPOSITESCORE && !profileData.academicInfo.BACH_GMAT_Total && !profileData.academicInfo.BACH_LSAT_LSATSCORE) &&
                  <div className="mr-5">
                    <div className="col2 fw600 fs20">
                      Not Applicable
                    </div>
                  </div>
                }
                {profileData &&
                  profileData.academicInfo &&
                  profileData.academicInfo.ACT_COMPOSITESCORE &&
                  <div className="mr-5">
                    <div className="col2 fw500">ACT</div>
                    <div className="col2 fw600 fs20">
                      {profileData.academicInfo.ACT_COMPOSITESCORE}
                    </div>
                  </div>
                }
                {profileData &&
                  profileData.academicInfo &&
                  profileData.academicInfo.BACH_GMAT_Total &&
                  <div className="mr-5">
                    <div className="col2 fw500">GMAT</div>
                    <div className="col2 fw600 fs20">
                      {profileData.academicInfo.BACH_GMAT_Total}
                    </div>
                  </div>
                }
                {profileData &&
                  profileData.academicInfo &&
                  profileData.academicInfo.SAT_TOTAL &&
                  <div className="mr-5">
                    <div className="col2 fw500">SAT</div>
                    <div className="col2 fw600 fs20">
                      {profileData.academicInfo.SAT_TOTAL}
                    </div>
                  </div>
                }
                {profileData &&
                  profileData.academicInfo &&
                  profileData.academicInfo.TOEFL_TOTAL &&
                  <div className="mr-5">
                    <div className="col2 fw500">TOEFL</div>
                    <div className="col2 fw600 fs20">
                      {profileData.academicInfo.TOEFL_TOTAL}
                    </div>
                  </div>
                }
                {profileData &&
                  profileData.academicInfo &&
                  profileData.academicInfo.PSAT_NMSQT_TOTAL &&
                  <div className="mr-5">
                    <div className="col2 fw500">PSAT/NMSQT</div>
                    <div className="col2 fw600 fs20">
                      {profileData.academicInfo.PSAT_NMSQT_TOTAL}
                    </div>
                  </div>
                }
                {profileData &&
                  profileData.academicInfo &&
                  profileData.academicInfo.GRE_TOTAL &&
                  <div className="mr-5">
                    <div className="col2 fw500">GRE</div>
                    <div className="col2 fw600 fs20">
                      {profileData.academicInfo.GRE_TOTAL}
                    </div>
                  </div>
                }
                {
                  profileData && profileData.academicInfo && profileData.academicInfo.IELTS_BANDSCORE &&
                  <div className="mr-5">
                    <div className="col2 fw500">IELTS</div>
                    <div className="col2 fw600 fs20">
                      {profileData.academicInfo.IELTS_BANDSCORE}
                    </div>
                  </div>
                }
                {
                  profileData && profileData.academicInfo && profileData.academicInfo.BACH_LSAT_LSATSCORE &&
                  <div>
                    <div className="col2 fw500">LSAT</div>
                    <div className="col2 fw600 fs20">
                      {profileData.academicInfo.BACH_LSAT_LSATSCORE}
                    </div>
                  </div>
                }

              </div>
              <div className="col5 fw500 fs14 mb-1">Field of Interest</div>
              <div className="d-flex flex-wrap mb-4">
                <div className="tagTitle br20 mr-2 mb-2 col2 fs12 fw500 pointer">
                  {profileData &&
                    profileData.academicInfo &&
                    profileData.academicInfo.STUDYFIELD
                    ? profileData.academicInfo.STUDYFIELD
                    : ""}
                </div>
                {/* <div className="tagTitle br20 mr-2 mb-2 col2 fs14 fw500 pointer">Data Science</div>
                                        <div className="tagTitle br20 mr-2 mb-2 col2 fs14 fw500 pointer">Programming</div> */}
              </div>
              {/* <div className="col5 fw500 fs14 mb-1">Awards</div>
              <ul className="awardType1 mb-4">
                <li><Image src={AwardIcon} alt="Icon" className="awardIcons" /> Guardian</li>
                <li><Image src={AwardIcon} alt="Icon" className="awardIcons" /> Student Media Award</li>
                <li><Image src={AwardIcon} alt="Icon" className="awardIcons" /> Morgan Prize</li>
              </ul> */}

              <div className="col5 fw500 fs14 mb-1">{fullDashboardData.S_TAGS && fullDashboardData.S_TAGS.length > 0 && "Preferred Options"}</div>
              <div className="d-flex flex-wrap mb-4">
                {
                  fullDashboardData && fullDashboardData.S_TAGS && fullDashboardData.S_TAGS.length > 0 &&
                  fullDashboardData.S_TAGS.map((s_tag, ind) => {
                    return <div className="tagTitle br20 col2 fw500 fs12" style={{ margin: 5 }}>{s_tag} <span className="ml-1 mr-1"></span></div>
                  })
                }
              </div>
              <Row>
                <Col md={6}>
                  <div className="col5 fw500 fs14 mb-1">Volunteer Hours</div>
                  <div className="col2 fw500">
                    {profileData &&
                      profileData.employmentInfo &&
                      profileData.employmentInfo.HRSCOMMUNITYSERVICE
                      ? profileData.employmentInfo.HRSCOMMUNITYSERVICE
                      : ""}
                  </div>
                </Col>
                <Col md={6}>
                  <div className="col5 fw500 fs14 mb-1">Current Income</div>
                  <div className="col2 fw500">
                    {profileData &&
                      profileData.finalPreference &&
                      profileData.finalPreference.CURRENTINCOME
                      ? profileData.finalPreference.CURRENTINCOME
                      : ""}
                  </div>
                </Col>
              </Row>
              {/* <div className="borderType5 mt-3 mb-3"></div>
              <div className="col2 fw600 fs22 mb-2">Notifications</div>
              <div className="notificationType1 bgCol20 br10 mb-3">
                <div className="col2 fw600 mb-1">General Alert</div>
                <div className="col2 fw500 fs14">Webtest #5 is now unlocked. Please visit the
                  <span className="fw600"> Resources</span> section to view it.</div>
                <div className="timeOne col2 fs14 fw500">1:30 PM</div>
              </div>
              <div className="notificationType1 bgCol20 br10 mb-3">
                <div className="col2 fw600 mb-1">Admission Alert</div>
                <div className="col2 fw500 fs14">
                  Texas A&M University admission forms are available now. Visit
                  <span className="fw600"> Admission</span> page for more information
                </div>
                <div className="timeOne col2 fs14 fw500">1:30 PM</div>
              </div>
              <div className="col8 fs18 fw600 text-center pointer">See all</div> */}
              <div className="serviceBanner br4 p-3 mt-3">
                <div className="col2 fw500 fs18 mb-4">
                  Add extra services to your plan better <br /> career growth
                </div>
                <Button
                  className="btnType5 upgradeBtn"
                  onClick={() => addService()}
                >
                  Add Service
                </Button>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
      <div className="bottomFooter d-flex align-items-center bgCol43">
        <div className="container-fluid">
          <Row>
            <Col md={12}>
              <div className="d-flex align-items-center">
                <Image src={footerLogo} alt="footer-logo" className="fLogo" />
                <div className="col42 fs13 ml-4">
                  Copyright © {new Date().getFullYear()} HiEd Success | Powered
                  by HiEd Success
                </div>
              </div>
            </Col>
          </Row>
        </div>
      </div>

      <Modal show={loader} backdrop="static" centered className="loaderModal">
        <Loader />
      </Modal>

      <TipModal
        tipsData={tipsData}
        title={"Info"}
        show={show}
        handleClose={handleClose}
      ></TipModal>
    </div>
  );
};

export default Dashboard;
