import React, { Suspense } from "react";
import { Modal } from 'react-bootstrap';
import { Route } from "react-router-dom";
import Register from "./Register/register";
import Login from "./Login/login";
import PublicRoute from '../../../routes/PublicRoute';
import PrivateRoute from '../../../routes/PrivateRoute';
import PrivateRouteWizard from '../../../routes/PrivateRouteWizard';
import PrivateRouteDashboard from '../../../routes/PrivateRouteDashboard';
import CollegeDetail from '../../development/student/collegeDetail/detail';
//import Wizard from '../student/wizard/wizard';
import dashboard from '../student/dashboard/dashboard';
import comparison from '../student/comparison/comparison';
import FavouriteCollegeList from '../student/FavouriteCollege/FavouriteCollegeList';
import Success from "./Register/success";
import successWizard from "./wizard/successWizard";
import Loader from '../../../Lib/LoaderNew';
import MyProfile from '../student/myprofile/myprofile';
import UpGradePlan from './myprofile/UpGradePlan';

const Wizard = React.lazy(() => import("../student/wizard/wizard"));

const StudentModule = ({ match }) => (
  <div>
    <PublicRoute path={`${match.url}/register`} component={Register} />
    <PublicRoute path={`${match.url}/login`} component={Login} />   
    <Suspense fallback={<div>
      <Modal
        show={true}
        centered
        className="loaderModal"
      >
        <Loader />
      </Modal></div>}>
      <PrivateRoute path={`${match.url}/wizard`} component={Wizard} />
      {/* <PrivateRouteWizard path={`${match.url}/wizard`} component={Wizard} /> */}
    </Suspense>
    <PrivateRouteDashboard path={`${match.url}/dashboard`} component={dashboard} />
    <PrivateRoute path={`${match.url}/comparison`} component={comparison} />
    <PrivateRoute path={`${match.url}/collegeDetail`} component={CollegeDetail} />
    <PublicRoute path={`${match.url}/success`} component={Success} />
    <PrivateRoute path={`${match.url}/successwizard`} component={successWizard} />
    <PrivateRoute path={`${match.url}/myProfile`} component={MyProfile} />
    <PrivateRoute path={`${match.url}/upgradePlan`} component={UpGradePlan} />
    <PrivateRoute path={`${match.url}/favouritecollege`} component={FavouriteCollegeList} />
  </div>
);

export default StudentModule;
