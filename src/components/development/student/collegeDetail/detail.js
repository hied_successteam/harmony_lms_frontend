import React, { useState, useEffect } from 'react';
import { Col, Row, Container, Table, Image, Modal, Tab, Tabs, Dropdown, Accordion, Card, Button, OverlayTrigger, Tooltip, } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import Logoadmin from '../../../../assets/images/svg/logoadmin.svg';
import SearchIcon from '../../../../assets/images/svg/MagnifyingGlass.svg';
import Bookmark from '../../../../assets/images/svg/BookmarkSimple.svg';
import Stars from '../../../../assets/images/svg/stars.svg';
import StarEmpty from '../../../../assets/images/svg/starempty.svg';
import BellOne from '../../../../assets/images/svg/Bell.svg';
import MapIcon from '../../../../assets/images/svg/MapPin2.svg';
import PhoneIcon from '../../../../assets/images/svg/Phone.svg';
import ProgressGraph1 from '../../../../assets/images/svg/progressOne.svg';
import ProgressGraph2 from '../../../../assets/images/svg/progressTwo.svg';
import ProgressGraph3 from '../../../../assets/images/svg/progressThree.svg';
import ProgressGraph4 from '../../../../assets/images/svg/progressFour.svg';
import { getLocalStorage, clearLocalStorage, setLocalStorage } from '../../../../Lib/Utils';
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import infoicon from "../../../../assets/images/svg/questions.svg";
import 'react-circular-progressbar/dist/styles.css';
import { logout, getCollegeListByID } from '../../../../Actions/Student/register';
import BackOne from '../../../../assets/images/svg/backs.svg';

import { actionLogout } from '../../../../Redux/College/LoginAction';

import ReactStars from "react-rating-stars-component";
import Loader from '../../../../Lib/LoaderNew';
import CONSTANTS from '../../../../Lib/Constants';
import Sicon1 from '../../../../assets/images/svg/sicon1.svg';
import Sicon2 from '../../../../assets/images/svg/sicon2.svg';
import Sicon3 from '../../../../assets/images/svg/sicon3.svg';
import Sicon4 from '../../../../assets/images/svg/sicon4.svg';
import Sicon5 from '../../../../assets/images/svg/sicon5.svg';
import Sicon6 from '../../../../assets/images/svg/sicon6.svg';
import Sicon7 from '../../../../assets/images/svg/sicon7.svg';
import Sicon8 from '../../../../assets/images/svg/sicon8.svg';
import ProgressOne from '../../../../assets/images/svg/progress1.svg';
import ProgressRed from '../../../../assets/images/svg/progressRed.svg';
import GraphCost from '../../../../assets/images/svg/graphcost.svg';
import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
import charts from "fusioncharts/fusioncharts.charts";
import widgets from "fusioncharts/fusioncharts.widgets";
import ReactFusioncharts from "react-fusioncharts";
import BoyIcon from '../../../../assets/images/svg/boy1.svg';
import ReactFC from "react-fusioncharts";
// Step 3 - Include the fusioncharts library
import FusionCharts from "fusioncharts";
// Step 4 - Include the chart type
import Column2D from "fusioncharts/fusioncharts.charts";
// Step 5 - Include the theme as fusion
// import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";s
// Step 6 - Adding the chart and theme as dependency to the core fusioncharts

ReactFC.fcRoot(FusionCharts, Column2D, FusionTheme); 

charts(FusionCharts);
widgets(FusionCharts);

const renderTooltip = (props) => (
  <Tooltip id="button-tooltipA" {...props}>
    The rate of applicants who are admitted
  </Tooltip>
);

const renderTooltipTwo = (props) => (
  <Tooltip id="button-tooltipB" {...props}>
    Students who completed Bachelor’s or other degree certificates.
  </Tooltip>
);

const renderTooltipThree = (props) => (             
  <Tooltip id="button-tooltipC" {...props}>
    Cost of education for full-time, first-time degree/certificate seeking. It includes tuition and fees, books and supplies, room and board, and other expenses    
  </Tooltip>
);

const renderTooltipFour = (props) => (             
  <Tooltip id="button-tooltipD" {...props}>
    Average cost of education for students from a certain economic background.    
  </Tooltip>
);

const renderTooltipFive = (props) => (             
  <Tooltip id="button-tooltipE" {...props}>
    Average amount of aid awarded to full-time first-time undergraduates
  </Tooltip>
);

const renderTooltipSix = (props) => (             
  <Tooltip id="button-tooltipF" {...props}>
    Average tuition for full-time Undergraduates and Graduates    
  </Tooltip>
);


const Dashboard = (props) => {

  const [details, setdetails] = useState({});
  const [loader, setLoader] = useState(false);
  const [user, setUser] = useState(false);
  const [categoriesData, setCategoriesData] = useState([]);
  const [demographicData, setDemographicData] = useState([]);
  const [enrollmentCategory, setEnrollmentCategory] = useState([]);
  const [enrollmentSeriesOne, setEnrollmentSeriesOne] = useState([]);
  const [enrollmentSeriesTwo, setEnrollmentSeriesTwo] = useState([]);
  const [enrollmentSeriesThree, setEnrollmentSeriesThree] = useState([]);
  const [genderData, setGenderData] = useState([]);
  const [costEducationCategory, setCostEducationCategory] = useState([]);
  const [costEducationSeriesOne, setCostEducationSeriesOne] = useState([]);
  const [costEducationSeriesTwo, setCostEducationSeriesTwo] = useState([]);
  const [costEducationSeriesThree, setCostEducationSeriesThree] = useState([]);
  const [costHouseholdData, setCostHouseholdData] = useState([]);
  const [fullTirstData, setFullTirstData] = useState([]);
  const [tutionFeesCategory, setTutionFeesCategory] = useState([]);
  const [tutionFeesSeriesOne, setTutionFeesSeriesOne] = useState([]);
  const [tutionFeesSeriesTwo, setTutionFeesSeriesTwo] = useState([]);
  const [tutionFeesSeriesThree, setTutionFeesSeriesThree] = useState([]);
  const dispatch = useDispatch();

  useEffect(() => {
    window.scrollTo(0, 0);
    var graphDetails = ''
    var user = getLocalStorage('user');
    var UNITID = ''
    setUser(user)
    if (props && props.location.data) {
      UNITID = props.location.data.UNITID
      // graphDetails = props.location.data
      // setdetails(props.location.data)
    } else {
      var data = getLocalStorage('dashboardDetail');
      UNITID = data.UNITID
      // graphDetails = data
      // setdetails(data)
    }
    getCollegeListByID(UNITID).then(res => {
      if (res && res.data && res.data.data && res.data.data.institutions && res.data.data.institutions[0]) {  
        setLoader(false)        
        setdetails(res.data.data.institutions[0])
        manageAllGraphs(res.data.data.institutions[0])        
      }else{
        setLoader(false)
      }
    }).catch(err => {setLoader(false)});




  }, [])

  const viewMyProfile = () => {
    props.history.push({ pathname: '/student/myProfile' });
  }

  const backToDashboard = () => {
    //props.history.goBack()
    if(props && props.location && props.location.page_data){
      props.history.push({pathname: '/student/dashboard', page_data: props.location.page_data})
    }else{
      props.history.push({pathname: '/student/dashboard'})
    }
  }

  const logoutCall = () => {
    var user = getLocalStorage('user');

    if (user && user.id) {
      logout({
        userId: user.id
      }).then(res => {
        if (res.data.success == true) {
          dispatch(actionLogout('college'));
          localStorage.clear();
          clearLocalStorage();
          props.history.push({ pathname: '/student/login' });
        } else {
          dispatch(actionLogout('college'));
        }
      }).catch(err => {
        dispatch(actionLogout('college'));
      });
    } else {
      dispatch(actionLogout('college'));
      props.history.push({ pathname: '/student/login' });
    }
  }

  const manageAllGraphs = (graphDetails) => {
    //manage for Categories graph
    if (graphDetails && graphDetails.EFCIPLEV && graphDetails.EFTOTLT) { 
      const graphLabel = graphDetails.EFCIPLEV;
      const graphValue = graphDetails.EFTOTLT;
      var Arrays = []
      graphLabel && graphValue && graphLabel.length && graphValue.length &&
        graphLabel.map((item, i) => {
          if(item){
            var data = {
              //label: item,
              label: CONSTANTS.EFCIPLEV[item],
              value: graphValue[i]
            }
            Arrays.push(data)
          }
        })
      setCategoriesData(Arrays)
    }

    //manage for Demographic graph
    if (graphDetails && graphDetails.EFNRALT && graphDetails.EFUNKNT && graphDetails.EFAIANT && graphDetails.EFASIAT && graphDetails.EFBKAAT && graphDetails.EFHISPT && graphDetails.EFNHPIT && graphDetails.EFWHITT && graphDetails.EF2MORT) {
      var demoData = [
        { label: "Nonresident alien", value: graphDetails.EFNRALT },
        { label: "Race/ethnicity unknown", value: graphDetails.EFUNKNT },
        { label: "American Indian or Alaska Native", value: graphDetails.EFAIANT },
        { label: "Asian", value: graphDetails.EFASIAT },
        { label: "Black or African American", value: graphDetails.EFBKAAT },
        { label: "Hispanic", value: graphDetails.EFHISPT },
        { label: "Native Hawaiian or Other Pacific Islander", value: graphDetails.EFNHPIT },
        { label: "White", value: graphDetails.EFWHITT },
        { label: "Two or more races", value: graphDetails.EF2MORT },
      ]
      setDemographicData(demoData)
    }


    //manage for Enrollment trend graph
    if (graphDetails && graphDetails.ADMSSN_CY && graphDetails.ADMSSN_CY - 1 && graphDetails.ADMSSN_CY - 2 && graphDetails.APPLCN_CY && graphDetails.APPLCN_CY - 1 && graphDetails.APPLCN_CY - 2 && graphDetails.ENRLT_CY && graphDetails.ENRLT_CY - 1 && graphDetails.ENRLT_CY - 2) {

      //CY - 2020,CY-1 - 2019,CY-2 - 2018
      var category = [
        { label: "2020" },
        { label: "2019" },
        { label: "2018" }
      ]

      var seriesOne = [
        { value: graphDetails.ADMSSN_CY },
        { value: graphDetails.ADMSSN_CY - 1 },
        { value: graphDetails.ADMSSN_CY - 2 },
      ]
      var seriesTwo = [
        { value: graphDetails.APPLCN_CY },
        { value: graphDetails.APPLCN_CY - 1 },
        { value: graphDetails.APPLCN_CY - 2 },
      ]
      var seriesThree = [
        { value: graphDetails.ENRLT_CY },
        { value: graphDetails.ENRLT_CY - 1 },
        { value: graphDetails.ENRLT_CY - 2 },
      ]
      setEnrollmentCategory(category)
      setEnrollmentSeriesOne(seriesOne)
      setEnrollmentSeriesTwo(seriesTwo)
      setEnrollmentSeriesThree(seriesThree)
    }

    //manage for Gender graph
    if (graphDetails && graphDetails.ENRLM && graphDetails.ENRLW) {

      var g_Arrays = [
        { label: "Male", value: graphDetails.ENRLM },
        { label: "Female", value: graphDetails.ENRLW },
      ]
      setGenderData(g_Arrays)
    }


    //manage for Cost of Education graph
    if (graphDetails && graphDetails.CHG1AY3 && graphDetails.CHG4AY3 && graphDetails.CHG5AY3 && graphDetails.CHG6AY3 && graphDetails.CHG2AY3 && graphDetails.CHG3AY3 && graphDetails.CHG9AY3 && graphDetails.CHG7AY3 && graphDetails.CHG8AY3) {

      var cost_category = [
        { label: "Living On-campus" },
        { label: "Living off-campus (with family" },
        { label: "Living off-campus (not with family)" }
      ]

      var cost_seriesOne = [
        { value: parseFloat(graphDetails.CHG1AY3) + parseFloat(graphDetails.CHG4AY3) + parseFloat(graphDetails.CHG5AY3) + parseFloat(graphDetails.CHG6AY3) },
        { value: parseFloat(graphDetails.CHG2AY3) + parseFloat(graphDetails.CHG4AY3) + parseFloat(graphDetails.CHG5AY3) + parseFloat(graphDetails.CHG6AY3) },
        { value: parseFloat(graphDetails.CHG3AY3) + parseFloat(graphDetails.CHG4AY3) + parseFloat(graphDetails.CHG5AY3) + parseFloat(graphDetails.CHG6AY3) },
      ]
      var cost_seriesTwo = [
        { value: parseFloat(graphDetails.CHG1AY3) + parseFloat(graphDetails.CHG4AY3) + parseFloat(graphDetails.CHG9AY3) },
        { value: parseFloat(graphDetails.CHG2AY3) + parseFloat(graphDetails.CHG4AY3) + parseFloat(graphDetails.CHG9AY3) },
        { value: parseFloat(graphDetails.CHG3AY3) + parseFloat(graphDetails.CHG4AY3) + parseFloat(graphDetails.CHG9AY3) },
      ]
      var cost_seriesThree = [
        { value: parseFloat(graphDetails.CHG1AY3) + parseFloat(graphDetails.CHG4AY3) + parseFloat(graphDetails.CHG7AY3) + parseFloat(graphDetails.CHG8AY3) },
        { value: parseFloat(graphDetails.CHG2AY3) + parseFloat(graphDetails.CHG4AY3) + parseFloat(graphDetails.CHG7AY3) + parseFloat(graphDetails.CHG8AY3) },
        { value: parseFloat(graphDetails.CHG3AY3) + parseFloat(graphDetails.CHG4AY3) + parseFloat(graphDetails.CHG7AY3) + parseFloat(graphDetails.CHG8AY3) },
      ]
      setCostEducationCategory(cost_category)
      setCostEducationSeriesOne(cost_seriesOne)
      setCostEducationSeriesTwo(cost_seriesTwo)
      setCostEducationSeriesThree(cost_seriesThree)
    }

    //manage for Cost of Household Income graph
    if (graphDetails && graphDetails.NPTIS412 && graphDetails.NPTIS422 && graphDetails.NPTIS432 && graphDetails.NPTIS442 && graphDetails.NPTIS452) {

      var cost_Arrays = [
        { label: "> $30,000", value: graphDetails.NPTIS412 },
        { label: "$30,001 - $48,000", value: graphDetails.NPTIS422 },
        { label: "$48,001 - $75,000", value: graphDetails.NPTIS432 },
        { label: "$75,001 - $110,000", value: graphDetails.NPTIS442 },
        { label: "< $110,000", value: graphDetails.NPTIS452 },
      ]
      setCostHouseholdData(cost_Arrays)
    }

    //manage for Full Time First Time Undergraduates graph
    if (graphDetails && graphDetails.FGRNT_A && graphDetails.PGRNT_A && graphDetails.SGRNT_A && graphDetails.IGRNT_A && graphDetails.LOAN_A && graphDetails.FLOAN_A && graphDetails.OLOAN_A && graphDetails.OFGRT_A) {

      var full_first_Arrays = [
        { label: "Federal grant aid", value: graphDetails.FGRNT_A },
        { label: "Pell grant aid", value: graphDetails.PGRNT_A },
        { label: "State/local grant aid", value: graphDetails.SGRNT_A },
        { label: "Institutional grant aid", value: graphDetails.IGRNT_A },
        { label: "Student loans", value: graphDetails.LOAN_A },
        { label: "Federal student loans", value: graphDetails.FLOAN_A },
        { label: "Other student loans", value: graphDetails.OLOAN_A },
        { label: "Other federal grant aid", value: graphDetails.OFGRT_A },
      ]
      setFullTirstData(full_first_Arrays)
    }

    //manage for Tuition and Fees graph
    if (graphDetails && graphDetails.TUITION1 && graphDetails.FEE1 && graphDetails.TUITION2 && graphDetails.FEE2 && graphDetails.TUITION3 && graphDetails.FEE3 && graphDetails.TUITION5 && graphDetails.FEE5 && graphDetails.TUITION6 && graphDetails.FEE6 && graphDetails.TUITION7 && graphDetails.FEE7) {

      var tution_category = [
        { label: "Undergraduates" },
        { label: "Graduates" },
      ]

      var tution_seriesOne = [
        { value: parseFloat(graphDetails.TUITION1) + parseFloat(graphDetails.FEE1) },
        { value: parseFloat(graphDetails.TUITION5) + parseFloat(graphDetails.FEE5) },
      ]
      var tution_seriesTwo = [
        { value: parseFloat(graphDetails.TUITION2) + parseFloat(graphDetails.FEE2) },
        { value: parseFloat(graphDetails.TUITION6) + parseFloat(graphDetails.FEE6) },
      ]

      var tution_seriesThree = [
        { value: parseFloat(graphDetails.TUITION3) + parseFloat(graphDetails.FEE3) },
        { value: parseFloat(graphDetails.TUITION7) + parseFloat(graphDetails.FEE7) },
      ]

      setTutionFeesCategory(tution_category)
      setTutionFeesSeriesOne(tution_seriesOne)
      setTutionFeesSeriesTwo(tution_seriesTwo)
      setTutionFeesSeriesThree(tution_seriesThree)
    }

  }


  //Categories graph data

  const categoriesGraph = {
    chart: {
      caption: "Categories",
      subcaption: "",
      baseFontSize: "14",
      showpercentvalues: true,
      showValues: true,
      showPercentInToolTip: false,
      defaultcenterlabel: "",
      aligncaptionwithcanvas: "0",
      captionpadding: "0",
      formatNumber: false,
      formatNumberScale: false,
      showLegend: false,
      labeldisplay: "wrap", 
      //legendCaption:"asf",
      //legendNumRows:10,
      decimals: "0", 
      plottooltext:
        " <b> <b>$value</b>",
      centerlabel: "$value",
      theme: "fusion"
    },
    data: categoriesData 
  };

  //Demographic graph data
  const demographicGraph = {
    chart: {
      caption: "Demographic",
      subcaption: "",
      baseFontSize: "14",
      showpercentvalues: "1",
      defaultcenterlabel: "",
      aligncaptionwithcanvas: "0",
      captionpadding: "0",
      //legendNumRows:10,
      showLegend: false,
      decimals: "1",
      labeldisplay: "wrap", 
      plottooltext:
        " <b> <b>$value</b>",
      centerlabel: "$value",
      theme: "fusion"
    },
    data: demographicData
  };


  //Enrollment trends graph data
  const enrollmentTrends = {
    chart: {
      caption: "Enrollment trends",
      subcaption: "",
      baseFontSize: "14",
      yaxisname: "",
      numvisibleplot: "9",
      //numDivLines: 2,
      labeldisplay: "auto",
      formatNumber: false,
      formatNumberScale: false,
      xAxisNameFontColor: "#000000",
      yAxisNameFontColor: "#000000",
      //divLineThickness: 1,
      //scrollheight: 1, 
      labeldisplay: "wrap",      
      theme: "fusion"
    },
    categories: [{ category: enrollmentCategory }],
    dataset: [
      { seriesname: "Admissions", data: enrollmentSeriesOne },
      { seriesname: "Applicants", data: enrollmentSeriesTwo },
      { seriesname: "Enrolled", data: enrollmentSeriesThree }
    ]
  };

  //Gender graph data
  const genderGraph = {
    chart: {
      caption: "Gender",
      subcaption: "",
      baseFontSize: "14",
      showpercentvalues: "1",
      defaultcenterlabel: "",
      aligncaptionwithcanvas: "0",
      captionpadding: "0",
      decimals: "1",
      plottooltext:
        " <b> <b>$value</b>",
      centerlabel: "$value",
      labeldisplay: "wrap", 
      theme: "fusion"
    },
    data: genderData
  };

  //Cost of Education graph data
  const costOfEducationGraph = {
    chart: {
      caption: "",
      subcaption: "",
      baseFontSize: "14",
      xaxisname: "TYPE OF STUDENT",
      yaxisname: "COST OF EDUCATION",
      numvisibleplot: "9",
      labeldisplay: "auto",
      formatNumber: false,
      formatNumberScale: false,
      xAxisNameFontColor: "#000000",
      yAxisNameFontColor: "#000000",
      labeldisplay: "wrap", 
      theme: "fusion"
    },
    categories: [{ category: costEducationCategory }],
    dataset: [
      { seriesname: "In-district", data: costEducationSeriesOne },
      { seriesname: "In-state", data: costEducationSeriesTwo },
      { seriesname: "Out-of-state", data: costEducationSeriesThree }
    ]
  };

  //Cost of Household graph data
  const costOfHouseholdGraph = {
    chart: {
      caption: "",
      subcaption: "",
      baseFontSize: "14",
      xaxisname: "HOUSEHOLD INCOME",
      yaxisname: "AVERAGE COST AFTER AID (NET PRICE)",
      numbersuffix: "",
      formatNumber: false,
      formatNumberScale: false,
      xAxisNameFontColor: "#000000",
      yAxisNameFontColor: "#000000",
      labeldisplay: "wrap", 
      theme: "fusion"
    },
    data: costHouseholdData
  };

  //Full-Time First-Time Undergraduate graph data
  const fullFirstUndergraduateGraph = {
    chart: {
      caption: "",
      subcaption: "",
      baseFontSize: "14",
      xaxisname: "TYPE OF AID AWARDED",
      yaxisname: "AVERAGE COSTING",
      xAxisNameFontColor: "#000000",
      yAxisNameFontColor: "#000000",
      numbersuffix: "",
      formatNumber: false,
      formatNumberScale: false,
      labeldisplay: "wrap", 
      theme: "fusion"
    },
    data: fullTirstData
  };

  //Tution Fees graph data
  const tutionFeesGraph = {
    chart: {
      caption: "",
      subcaption: "",
      xaxisname: "",
      baseFontSize: "14",
      yaxisname: "",
      numvisibleplot: "6",
      labeldisplay: "wrap", 
      formatNumber: false,
      formatNumberScale: false,
      xAxisNameFontColor: "#000000",
      yAxisNameFontColor: "#000000",
      theme: "fusion"
    },
    categories: [{ category: tutionFeesCategory }],
    dataset: [
      { seriesname: "In-district", data: tutionFeesSeriesOne },
      { seriesname: "In-state", data: tutionFeesSeriesTwo },
      { seriesname: "Out-of-state", data: tutionFeesSeriesThree }
    ]
  };

  return (
    <div className="collegeAdminUi bgCol25 pb-5">
      <Container fluid>
        <div className="topHeaderBox bgCol25 pl-0 pr-0">
          <div className="pl-4 pr-4 pt-4">
            <Row>
              <Col md={3}>
                <Image src={Logoadmin} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" />
                <span className="fs20 col24 fw700 ml-1">HiEd Harmony</span>
              </Col>
              <Col md={6}></Col>
              <Col md={3}>
                <div className="powerBtn d-flex justify-content-end">
                  {/* <div className="mr-4">
                    <Dropdown>
                      <Dropdown.Toggle id="dropdown-basic" className="DropdownType1">
                        <Image src={bellIcon} alt="Search" className="pointer setingOne mt-1" />
                      </Dropdown.Toggle>
                    </Dropdown>
                  </div> */}
                  {/* <Image src={Settings} alt="Search" className="pointer setingOne mr-5" /> */}
                  <div style={{ paddingTop: 6 }}>
                    <Dropdown>
                      <Dropdown.Toggle id="dropdown-basic" className="DropdownType1 col2 fs14 fw600">
                        {/* <Image src={UserThree} alt="Search" className="pointer user1 mr-1" /> */}
                        {user.firstName + " " + user.lastName}   <i className="fa fa-chevron-down ml-1 col8" aria-hidden="true"></i>
                      </Dropdown.Toggle>
                      <Dropdown.Menu>
                        <Dropdown.Item onClick={() => viewMyProfile()}>Profile</Dropdown.Item>
                        {/* <Dropdown.Item href="#">Setting</Dropdown.Item> */}
                        <Dropdown.Item onClick={() => logoutCall()}>Logout</Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                  </div>
                </div>
              </Col>
            </Row>
            <div className="col2 fs20 fw500 mt-4 mb-3">
              <div style={{ width: 100, cursor: 'pointer' }} onClick={() => backToDashboard()}>
                <Image src={BackOne} alt="back" className="pointer mr-2 back1" /> Back
              </div>
            </div>
            <div className="middileAccordian mb-5">
              <Row className="mb-4">
                <Col md={8}>
                  <div className="col2 fw600 fs20 mb-1 titleFour">
                    {details.INSTNM}
                  </div>
                  <div className="col2 fw600 fs14 mb-2">
                    <Row>
                      <span className="text-uppercase" style={{ marginLeft: 15 }}>{details.CONTROL}</span>
                      {
                        details.RATING ?
                        <span className="ml-3">
                          <ReactStars
                              count={5}
                              size={14}
                              edit={false}
                              isHalf={true}
                              value={parseFloat(details.RATING)}
                              emptyIcon={<i className="far fa-star"></i>}
                              halfIcon={<i className="fa fa-star-half-alt"></i>}
                              fullIcon={<i className="fa fa-star"></i>}
                              activeColor="#ffd700"
                            />
                        </span>
                        :
                        <span className="ml-3">
                          {
                            setTimeout(() => {
                              <ReactStars
                                count={5}
                                size={14}
                                edit={false}
                                isHalf={true}
                                value={parseFloat(details.RATING)}
                                emptyIcon={<i className="far fa-star"></i>}
                                halfIcon={<i className="fa fa-star-half-alt"></i>}
                                fullIcon={<i className="fa fa-star"></i>}
                                activeColor="#ffd700"
                              />
                            }, 1000)
                          }
                        </span>
                      }
                      
                      <span className="ml-3 col5 fw400">(College rating as per StateUniversity.com)</span>
                    </Row>
                  </div>
                  <div className="col2 fw500 mb-2">
                    <Image src={MapIcon} alt="Icon" className="mr-1 mapview" />
                    {details && details.ADDR && details.CITY && details.STABBR ? details.ADDR + ', ' + details.CITY + ', ' + details.STABBR:'-'}
                    {/* {details.ADDR + ', ' + details.CITY + ', ' + details.STABBR} */}
                  </div>
                  <div className="col2 fw500 mb-4">
                    <Image src={PhoneIcon} alt="Icon" className="mr-1 mapview" /> {details.GENTELE}
                  </div>
                  {/* <div className="col2 fs14 fw600 mb-1 text-uppercase">
                    popular tags
                  </div>
                  <div className="tagType1 d-flex flex-wrap">
                    <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">Diversity support</span>
                    <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize"> Indian culture club</span>
                    <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">
                      HBCU
                    </span>
                    <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">Baseball</span>
                    <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">
                      online programs
                    </span>
                    <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">student centre</span>
                  </div> */}
                </Col>
                <Col md={4}>
                  <div className="col8 fs22 fw500 pointer mb-1 text-right title5">
                    <a className="fw500 col8 pointer" href={details.WEBADDR} target="_blank" rel="noopener noreferrer">{details.WEBADDR}</a>
                  </div>
                  <div className="col5 fw500 text-right">Visit website for more details</div>
                </Col>
              </Row>
              {/* <Row className="mb-5">
                <Col md={7}>
                  <div className="CompareBox4 shadow3">
                    <div className="col2 fw400 lineH1 mb-3">
                      Located in Alva, a city of approximately 5,000 residents in northwest Oklahoma. Alva is located
                      14 miles south of the Kansas border, approximately 115 miles northwest of Oklahoma City, and
                      100 miles southwest of Wichita, Kansas
                    </div>

                    <div className="col2 lineH1 fw400">
                      Students can choose from more than 40 areas of study to earn their Bachelor of Arts or
                      Bachelor of Science degrees. Northwestern offers master's degree programs in education,
                      counselling psychology, and American Studies, and a doctoral program in nursing practice.
                    </div>  
                  </div>
                </Col>
                <Col md={5}>
                  <div className="text-center CompareBox5 d-flex align-items-center">
                    <div>
                      <div className="fs22 fw600 col1 mb-2">What’s Remarkable?</div>
                      <div className="fw600 col2">
                        The community is fully engaged with the University and even dedicates sales tax money to fund scholarships.</div>
                    </div>
                  </div>
                </Col>
              </Row> */}
              <div className="tabType1 mb-5">
                <Tabs defaultActiveKey="home" id="uncontrolled-tab-example" className="mb-3">
                  <Tab eventKey="home" title="Basic Information">
                    <Row className="mb-4">
                      <Col md={6} className="mb-4">
                        {
                          categoriesGraph && categoriesGraph.data && categoriesGraph.data.length > 0 ?
                          <ReactFusioncharts
                            type="doughnut2d"
                            width="100%"
                            height="500"
                            dataFormat="JSON"
                            dataSource={categoriesGraph}
                          />
                          :
                          <> 
                            <div className="fw600 text-titles1" style={{marginTop: 12}}>Categories</div>
                            <div className='graphSpace'>No data available</div> 
                          </>
                        }
                        
                      </Col>
                      <Col md={6} className="mb-4">
                        {
                          demographicGraph && demographicGraph.data && demographicGraph.data.length > 0 ?
                          <ReactFusioncharts
                            type="doughnut2d"
                            width="100%"
                            height="500"
                            dataFormat="JSON"
                            dataSource={demographicGraph}
                          />
                          :
                          <> 
                            <div className="fw600 text-titles1" style={{marginTop: 12}}>Demographic</div>
                            <div className='graphSpace'>No data available</div> 
                          </> 
                        } 
                      </Col>
                      <Col md={6} className="mb-4">
                        {
                          enrollmentTrends && enrollmentTrends.dataset && enrollmentTrends.dataset.length > 0 && enrollmentTrends.dataset[0].data.length > 0 && enrollmentTrends.dataset[1].data.length > 0 && enrollmentTrends.dataset[2].data.length > 0 ?
                          <ReactFusioncharts
                            type="scrollcolumn2d"
                            width="100%"
                            height="500"
                            dataFormat="JSON"
                            dataSource={enrollmentTrends}
                          />
                          :
                          <> 
                            <div className="fw600 text-titles1" style={{marginTop: 12}}>Enrollment Trends</div>
                            <div className='graphSpace'>No data available</div> 
                          </>  
                        } 
                      </Col>
                      <Col md={6} className="mb-4">
                        <div className="minh500">
                          {
                            genderGraph && genderGraph.data && genderGraph.data.length > 0 ?
                            <ReactFusioncharts
                              type="doughnut2d"
                              width="100%"
                              height="350"
                              dataFormat="JSON"
                              dataSource={genderGraph}
                            />
                            :
                            <> 
                              <div className="fw600 text-titles1" style={{marginTop: 12}}>Gender</div>
                              <div className='graphSpace'>No data available</div> 
                            </>  
                          } 
                        </div>
                      </Col>
                    </Row>
                    <Row className="mb-4 tableGraph1">
                      <Col md={8}>
                        <div className="fs18 fw600 col2 mb-3">Admission Prerequisites</div>
                        <Table bordered className="tableType2 planTable planUpdates br10 mb-4">
                          <tbody>
                            <tr>
                              <td>Secondary school GPA</td>
                              <td>{details.ADMCOM1}</td>
                            </tr>
                            <tr>
                              <td>Secondary school rank</td>
                              <td>{details.ADMCOM2}</td>
                            </tr>
                            <tr>
                              <td>Secondary school record</td>
                              <td>{details.ADMCOM3}</td>
                            </tr>
                            <tr>
                              <td>Completion of college-preparatory program</td>
                              <td>{details.ADMCOM4}</td>
                            </tr>
                            <tr>
                              <td>Recommendations</td>
                              <td>{details.ADMCOM5}</td>
                            </tr>
                            <tr>
                              <td>Formal demonstration of competencies</td>
                              <td>{details.ADMCOM6}</td>
                            </tr>
                            <tr>
                              <td>Admission test scores</td>
                              <td>{details.ADMCOM7}</td>
                            </tr>
                            <tr>
                              <td>TOEFL (Test of English as a Foreign Language)</td>
                              <td>{details.ADMCOM8}</td>
                            </tr>
                            <tr>
                              <td>SAT (Scholastic Assessment Test)</td>
                              <td>{details.SATPCT >= 50 ? "Required" : "Recommended"}</td>
                            </tr>
                            <tr>
                              <td>ACT (American College Testing)</td>
                              <td>{details.ACTPCT >= 50 ? "Required" : "Recommended"}</td>
                            </tr>
                            <tr>
                              <td>Other Tests (Wonderlic, WISC-III, etc.)</td>
                              <td>{details.ADMCOM9}</td>
                            </tr>
                          </tbody>
                        </Table>
                      </Col>
                    </Row>
                  </Tab>
                  <Tab eventKey="profile" title="Advanced Information">
                    <div className="summaryBox1">
                      <div className="text-uppercase fs18 fw500 col2 summaryBox2 mb-4 bgCol28 d-flex align-items-center">Summary</div>
                      <Row className="mb-4">
                        <Col md={3}>
                          <div className="summaryBox3 d-flex justify-content-between shadow3 br10">
                            <div>
                              <div className="fw400 col2 mb-2">Rating</div>
                              <div className="fw500 col2 fs20">{details.RATING}</div>
                            </div>
                            <div>
                              <Image src={Sicon1} alt="Icon" className="SImg1" />
                            </div>
                          </div>
                        </Col>
                        <Col md={3}>
                          <div className="summaryBox3 d-flex justify-content-between shadow3 br10">
                            <div>
                              <div className="fw400 col2 mb-2">Type</div>
                              <div className="fw500 col2 fs20">{details.CONTROL}</div>
                            </div>
                            <div>
                              <Image src={Sicon4} alt="Icon" className="SImg1" />
                            </div>
                          </div>
                        </Col>
                        <Col md={3}>
                          <div className="summaryBox3 d-flex justify-content-between shadow3 br10">
                            <div>
                              <div className="fw400 col2 mb-2">Level of Institution</div>
                              <div className="fw500 col2 fs20">{details.ICLEVEL}</div>
                            </div>
                            <div>
                              <Image src={Sicon5} alt="Icon" className="SImg1" />
                            </div>
                          </div>
                        </Col>
                        <Col md={3}>
                          <div className="summaryBox3 d-flex justify-content-between shadow3 br10">
                            <div>
                              <div className="fw400 col2 mb-2">Size of Institution</div>
                              <div className="fw500 col2 fs20">{details.INSTSIZE}</div>
                            </div>
                            <div>
                              <Image src={Sicon6} alt="Icon" className="SImg1" />
                            </div>
                          </div>
                        </Col>
                        <Col md={3}>
                          <OverlayTrigger
                            placement="top"
                            delay={{ show: 0, hide: 400 }}
                            overlay={renderTooltip}
                          >
                            <div className="summaryBox3 d-flex justify-content-between shadow3 br10 pointer">
                              <div>
                                <div className="fw400 col2 mb-2 pointer">Acceptance rate</div>
                                <div style={{ width: 70, height: 70 }}>
                                  {
                                    details?.ADMSSN_CY?
                                    <CircularProgressbar
                                    styles={buildStyles({
                                      textColor: "#000",
                                      pathColor: "#89D667",
                                      trailColor: "#ececff",
                                    })}
                                    strokeWidth={10}
                                    value={parseFloat((parseFloat(details.ADMSSN_CY) / parseFloat(details.APPLCN_CY)) * 100).toFixed(1)}
                                    text={`${parseFloat((parseFloat(details.ADMSSN_CY) / parseFloat(details.APPLCN_CY)) * 100).toFixed(1)}%`}
                                  />:'-'
                                  }
                                  
                                </div>
                              </div>
                              <div>
                                <Image src={Sicon2} alt="Icon" className="SImg1" />
                              </div>
                            </div>
                          </OverlayTrigger>
                        </Col>
                        <Col md={3}>
                          <div className="summaryBox3 d-flex justify-content-between shadow3 br10">
                            <div>
                              <div className="fw400 col2 mb-2">Enrollment</div>
                              <div className="fw400 col2 mb-2">
                                Undergraduate 
                                <span className="fw500 col2 fs20">{details.EFTOTLT_UG && ' - '+details.EFTOTLT_UG}
                                </span>
                              </div>
                              <div className="fw400 col2 mb-2">
                                Graduate 
                                <span className="fw500 col2 fs20">{details.EFTOTLT_G && ' - '+details.EFTOTLT_G}
                                </span>
                              </div>
                            </div>
                            {/* <div>
                              <div className="fw400 col2 mb-2">
                                Undergraduate enrollment 
                              </div>
                              <div className="fw500 col2 fs20 text-center">{details.EFTOTLT_UG} {details && details.EFTOTLT_G ? `and ${details.EFTOTLT_G}` :'' }</div>
                            </div>
                            <div>
                              <div className="fw400 col2 mb-2"> 
                                 Graduate enrollment 
                              </div>
                              <div className="fw500 col2 fs20 text-center">{details.EFTOTLT_UG} {details && details.EFTOTLT_G ? `and ${details.EFTOTLT_G}` :'' }</div>
                            </div> */}
                            <div>
                              <Image src={Sicon8} alt="Icon" className="SImg1" />
                            </div>
                          </div>
                        </Col>
                        <Col md={3}>             
                          <OverlayTrigger
                            placement="top"
                            delay={{ show: 250, hide: 400 }}
                            overlay={renderTooltipTwo}
                          >   
                          <div className="summaryBox3 d-flex justify-content-between shadow3 br10 pointer">
                            <div>
                              <div className="fw400 col2 mb-2 pointer">Graduation Rate</div>
                              <div style={{ width: 70, height: 70 }}>
                                {details?.GRTOTLT_3?
                                  <CircularProgressbar
                                  styles={buildStyles({
                                    textColor: "#000",
                                    pathColor: "#FE5E54",
                                    trailColor: "#ececff",
                                  })}
                                  strokeWidth={10} 
                                  value={parseFloat((parseFloat(details.GRTOTLT_3) / parseFloat(details.GRTOTLT_2)) * 100).toFixed(1)}
                                  text={`${parseFloat((parseFloat(details.GRTOTLT_3) / parseFloat(details.GRTOTLT_2)) * 100).toFixed(1)}%`}
                                />:'-'
                                }  
                              </div>  
                            </div>                                 
                            <div>
                              <Image src={Sicon3} alt="Icon" className="SImg1" />
                            </div>
                          </div>
                          </OverlayTrigger>
                        </Col>
                        <Col md={3}>
                          <div className="summaryBox3 d-flex justify-content-between shadow3 br10">
                            <div>
                              <div className="fw400 col2 mb-2">  
                                Student to faculty ratio
                              </div>
                              <div className="fw500 col2 fs20">{details.STUFACR}</div>
                            </div>
                            <div>
                              <Image src={Sicon8} alt="Icon" className="SImg1" />
                            </div>
                          </div>
                        </Col>
                      </Row>
                      <div className="text-uppercase fs18 fw500 col2 summaryBox2 mb-4 bgCol28 d-flex align-items-center">Statistics</div>
                      <Row>
                        <Col md={6} style={{ height: 500 }}>
                         <div className="d-flex justify-content-center">  
                        <div className="fw600 text-titles1">Cost of Education</div>
                          <OverlayTrigger
                              placement="top"
                              delay={{ show: 250, hide: 400 }}
                              overlay={renderTooltipThree}      
                            >         
                                
                              <Image src={infoicon} alt="InfoIcon" className="ml-2 pointer infoIcon2" /> 
                          </OverlayTrigger>   
                          </div>  
                          {
                            costOfEducationGraph && costOfEducationGraph.dataset && costOfEducationGraph.dataset && costOfEducationGraph.dataset[0].data.length > 0 && costOfEducationGraph.dataset[1].data.length > 0 && costOfEducationGraph.dataset[2].data.length > 0 ?
                            <div className="summaryBox4">
                              <ReactFusioncharts
                                type="scrollcolumn2d"
                                width="100%"
                                height="100%"
                                dataFormat="JSON"
                                dataSource={costOfEducationGraph}
                              /> 
                            </div>
                            : 
                            <div className='graphSpace'>No data available</div>  
                          } 
                          
                        </Col>
                        <Col md={6}>
                           <div className='d-flex justify-content-center'>
                            <div className="fw600 text-titles1">Cost of Household Income</div> 
                            <OverlayTrigger  
                              placement="top"
                              delay={{ show: 250, hide: 400 }}
                              overlay={renderTooltipFour}      
                            >       
                            <Image src={infoicon} alt="InfoIcon" className="ml-2 pointer infoIcon2" />  
                                
                            </OverlayTrigger> 
                            </div>
                            {
                              costOfHouseholdGraph && costOfHouseholdGraph.data && costOfHouseholdGraph.data.length > 0 ?
                              <div className="summaryBox4">
                                <ReactFusioncharts
                                  type="column2d"
                                  width="100%"
                                  height="100%"
                                  dataFormat="JSON"
                                  dataSource={costOfHouseholdGraph}
                                /> 
                              </div>
                              :
                              <div className='graphSpace'>No data available</div>
                            }
                          
                        </Col>
                      </Row>
                      <Row> 
                        <Col md={6} style={{ height: 500 }}>
                        <div className='d-flex justify-content-center'> 
                        <div className="fw600 text-titles1">Full-time First-time undergraduates Financial aid</div> 
                          <OverlayTrigger   
                            placement="top"
                            delay={{ show: 250, hide: 400 }}
                            overlay={renderTooltipFive}      
                          >            

                          <Image src={infoicon} alt="InfoIcon" className="ml-2 pointer infoIcon2" />     
                          </OverlayTrigger> 
                          </div>
                          {
                            fullFirstUndergraduateGraph && fullFirstUndergraduateGraph.data && fullFirstUndergraduateGraph.data.length > 0 ?
                            <div className="summaryBox4"> 
                              <ReactFusioncharts
                                type="column2d"
                                width="100%"
                                height="100%"
                                dataFormat="JSON"
                                dataSource={fullFirstUndergraduateGraph}
                              /> 
                            </div>
                            :
                            <div className='graphSpace'>No data available</div>
                          }
                          
                        </Col>
                        <Col md={6}>
                        <div className='d-flex justify-content-center'> 
                        <div className="fw600 text-titles1">Tuition and Fees</div>  
                          <OverlayTrigger  
                            placement="top"
                            delay={{ show: 250, hide: 400 }}
                            overlay={renderTooltipSix}            
                          >         
                                 
                          <Image src={infoicon} alt="InfoIcon" className="ml-2 pointer infoIcon2" />  
                          </OverlayTrigger>   
                          </div> 
                          {
                            tutionFeesGraph && tutionFeesGraph.dataset && tutionFeesGraph.dataset && tutionFeesGraph.dataset[0].data.length > 0 && tutionFeesGraph.dataset[1].data.length > 0 && tutionFeesGraph.dataset[2].data.length > 0 ?
                            <div className="summaryBox4">
                              <ReactFusioncharts
                                type="scrollcolumn2d"
                                width="100%"
                                height="100%"
                                dataFormat="JSON"
                                dataSource={tutionFeesGraph}
                              /> 
                            </div>
                            :
                            <div className='graphSpace'>No data available</div>
                          }
                          
                        </Col>
                      </Row>
                    </div>
                  </Tab>
                </Tabs>
              </div>
            </div>
          </div>
        </div>
      </Container>

      <Modal
        show={loader}
        // onHide={loader}
        centered
        className="loaderModal"
      >
        <Loader />
      </Modal>
    </div>
  )
}
export default Dashboard;  




