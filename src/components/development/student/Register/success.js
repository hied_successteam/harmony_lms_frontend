import React from 'react';
import { Col, Row, Container, Image, Form, Button } from 'react-bootstrap'; 
import UserCircleOne from '../../../../assets/images/svg/UserCircle.svg'; 

const Success = (props) => {  

    //go to login in page
    const goToLogin = () => {
        props.history.push({ pathname: 'login' });  
   }
     return (
          <div className="bgBanner d-flex align-items-center justify-content-center w-100">
               <Container>
                    <Col lg={7} md={7} className="m-auto">  
                         <div className="studentBox successBox1 d-flex align-items-center justify-content-center text-center bgCol3 shadow1 br10">    
                              <div>
                                   <div className="text-center mb-3">
                                        <Image src={UserCircleOne} alt="Icon" className="" />
                                   </div>
                                   <div className="fs22 fw600 col2 mb-1">Registered</div> 
                                   <div className="fw500 col5 mb-4 pb-2">Student has been registered successfully. 
                                   </div> 
                                                                     
                                   <div className="fw700 col1 pointer" onClick={() => goToLogin()}>Sign In</div>  
                              </div>
                         </div>
                    </Col>
               </Container>
          </div>
     )
}

export default Success; 

