import React, { useState, useEffect, useRef } from 'react';
import { Col, Row, Container, Image, Form, Button } from 'react-bootstrap';
import Logos from '../../../../assets/images/logos.png';
import Linkedin from '../../../../assets/images/linkedin.png';
import Facebook from '../../../../assets/images/facebook.png';
import { register, countryList, stateList } from '../../../../Actions/Student/register'
import validateInput from '../../../../Lib/Validation/studRegValidation'
import Loader from "react-js-loader";
import IntlTelInput from 'react-intl-tel-input';
import TipModal from '../../../development/TipModal/TipModal'
// import {registermsg} from '../../../../Lib/message'

const StudentRegister = (props) => {

  //set local variables in state
  const [firstname, setFirstname] = useState('');
  const [lastname, setLastname] = useState('');
  const [email, setEmail] = useState('');
  const [mobile, setMobile] = useState('');
  const [country, setCountry] = useState('');
  const [state, setState] = useState('');
  const [zipCode, setZipCode] = useState('');
  const [password, setPassword] = useState('');
  const [confPassword, setConfPassword] = useState('');
  const [errors, setErrors] = useState({});
  const [check, setCheck] = useState(false);
  const [errorMsg, setErrorMsg] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [countryDatas, setCountryDatas] = useState("");
  const [stateDatas, setStateDatas] = useState("");
  const [loading, setLoading] = useState(false);
  const [countryCode, setCountryCode] = useState('');
  const [dialCountryName, setDialCountryName] = useState('')
  const [dialCountryShortName, setDialCountryShortName] = useState('')
  const [flagchange, setFlagchange] = useState(false)
  const [tipsData, setTipsData] = useState(false);
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);

  const handleShow = (tipsData) => {
    setTipsData(tipsData);
    setShow(true);
  }


  //let textInput = useRef();




  useEffect(() => {
    var country_names = countryDatas && countryDatas.find(item => item.countryId == country)
    if (country_names && country_names.shortName) {
      setDialCountryShortName(country_names.shortName)
    }
  }, [country]);

  //go to login in page
  const goToLogin = () => {
    props.history.push({ pathname: 'login' });
  }

  //submit register data
  const registerSubmit = () => {

    if (isValid()) {
      setErrors({})
      setLoading(true);

      var country_name = countryDatas && countryDatas.find(item => item.countryId == country);
      var state_name = stateDatas.find(item => item.stateId == state)
     

      register({
        firstName: firstname, lastName: lastname, email, mobileNumber: mobile, countryCode, country: country_name.name, state: state_name.name, zipCode, password, confirmPassword: confPassword, userType: "student"
      }).then(res => {
        setLoading(false)

        if (res.data.success == true) {
          props.history.push({ pathname: 'success' });
        } else {
          setErrorMsg(true)
          setErrorMessage(res.data.message)
          setTimeout(function () {
            setErrorMsg(false)
          }.bind(this), 5000);
        }
      }).catch(err => { 
        setLoading(false)
        setErrorMsg(true)
        setErrorMessage(err.response.data.message)
        setTimeout(function () {
          setErrorMsg(false)
        }.bind(this), 5000);
      });
    }

  }

  //get country list
  useEffect(() => {

    countryList().then(res => { 
      console.log(res);     
      setCountryDatas(res.data.data)

    }).catch(err => { });
  }, []);


  //check input validation
  const isValid = () => {
    const { errors, isValid } = validateInput(firstname, lastname, email, mobile, country, state, zipCode, password, confPassword, dialCountryName, dialCountryShortName);
    if (!isValid) {
      setErrors(errors)
    }
    return isValid;
  }

  const conditionCheck = (e) => {
    setCheck(check ? false : true)
  }

  const countryInput = (e) => {
    setCountry(e.target.value)
    setState('');
    stateList(e.target.value).then(res => {      
      setStateDatas(res.data.data)

    }).catch(err => { });
  }

  const formatPhoneNumberOutput = (a, b, c) => {    
    //textInput.setFlag(flagchange ? dialCountryName : 'us');
    //props.history.push({ pathname: 'Login' }); 
    let num = /^[0-9\b]+$/;
    if (b == '' || num.test(b)) {
      if (b.length < 11) {
        setMobile(b);
        setCountryCode(c.dialCode);
        setDialCountryName(c.iso2);
      }
    }

  }

  const formatPhoneNumberOutputFlag = (b, c) => {  
    //props.history.push({ pathname: 'Login' }); 
    setFlagchange(true);
    let num = /^[0-9\b]+$/;
    if (b == '' || num.test(b)) {
      if (b.length < 11) {
        setMobile(b);
        setCountryCode(c.dialCode);
        setDialCountryName(c.iso2);
      }
    }
  }

  //go to landing page
  const goToLanding = () => {
    props.history.push({ pathname: '/landing/dashboard' });
  }

  const zipCodeChange = (value) => {
    if (value.length <= 6) {
      setZipCode(value)
    }
  }




  return (
    <div className="bgBanner d-flex align-items-center justify-content-center w-100">
      <Container>
        <Col lg={10} md={10} className="m-auto">
          <div className="studentBox text-center bgCol3 shadow1 br10 registerUsers">
            <div>
              <Image src={Logos} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" onClick={() => goToLanding()} />
              <div className="studentOne mt-4 text-left">
                <div className="fs22 fw600 col2 mb-1 text-center">Sign Up</div>
                <div className="fw500 col5 mb-3 text-center mb-4 pb-1">Create your account</div>
                {
                  errorMsg &&
                  <div className="fw500 col5 mb-3 text-center mb-4 pb-1" style={{ color: 'red' }}>{errorMessage}</div>
                }

                <Form className="formLayoutUi">
                  <Row>
                    <Col md={6}>
                      <Form.Group controlId="formBasicEmailA">
                        <Form.Control
                          type="text"
                          placeholder="First Name"
                          className="inputType1"
                          value={firstname}
                          maxLength={35}
                          onChange={(e) => setFirstname(e.target.value)}
                        />
                        {
                          errors.firstname &&
                          <span className="help-block error-text">{errors.firstname}</span>
                        }
                      </Form.Group>
                    </Col>
                    <Col md={6}>
                      <Form.Group controlId="formBasicEmailB">
                        <Form.Control
                          type="text"
                          placeholder="Last Name"
                          className="inputType1"
                          value={lastname}
                          maxLength={35}
                          onChange={(e) => setLastname(e.target.value)}
                        />
                        {
                          errors.lastname &&
                          <span className="help-block error-text">{errors.lastname}</span>
                        }
                      </Form.Group>
                    </Col>
                    <Col md={6}>
                      <Form.Group controlId="formBasicEmailC">
                        <Form.Control
                          type="email"
                          placeholder="Email"
                          className="inputType1"
                          value={email}
                          onChange={(e) => setEmail(e.target.value)} />
                        {
                          errors.email &&
                          <span className="help-block error-text">{errors.email}</span>
                        }
                      </Form.Group>
                    </Col>
                    <Col md={6}>
                      <Form.Group controlId="formBasicEmailD" className="mb-4 flagSelect">
                        <IntlTelInput                          
                         // ref={(elt) => textInput = elt}
                          containerClassName="intl-tel-input"
                          inputClassName="form-control inputType1"
                          placeholder="Contact Number"
                          onPhoneNumberChange={(...args) => {
                            formatPhoneNumberOutput(...args);
                          }}
                          onSelectFlag={(...args) => {
                            formatPhoneNumberOutputFlag(...args); 
                          }}
                          name="mobile"
                          value={mobile}
                          formatOnInit={true}
                        />
                        {
                          errors.mobile &&
                          <span className="help-block error-text">{errors.mobile}</span>
                        }
                      </Form.Group>
                      {/* <Form.Group controlId="formBasicEmail"> 
                                                    <Form.Control 
                                                      type="number" 
                                                      placeholder="Contact Number" 
                                                      className="inputType1"
                                                      maxLength={10}
                                                      value={mobile}
                                                      onChange={(e) => setMobile(e.target.value)} /> 
                                                      {
                                                        errors.mobile &&
                                                        <span className="help-block error-text">{errors.mobile}</span>
                                                      }
                                                    </Form.Group> */}
                    </Col>
                    <Col md={4}>
                      <Form.Group controlId="exampleForm.ControlSelect1">
                        <Form.Control
                          as="select"
                          className="selectTyp1 pointer"
                          // value={firstname}
                          onChange={(e) => countryInput(e)}
                        >

                          <option>Country</option>
                          {
                            countryDatas && countryDatas.map((items, i) => {
                              return <option key={i} title={items.name} value={items.countryId}>{items.name}</option>
                            })
                          }
                        </Form.Control>
                        {
                          errors.country &&
                          <span className="help-block error-text">{errors.country}</span>
                        }
                      </Form.Group>
                    </Col>
                    <Col md={4}>
                      <Form.Group controlId="exampleForm.ControlSelect1">
                        <Form.Control
                          as="select"
                          className="selectTyp1 pointer"
                          value={state}
                          onChange={(e) => setState(e.target.value)}>
                          <option value="">State</option>
                          {
                            stateDatas && stateDatas.map((items, i) => {
                              return <option key={i} title={items.name} value={items.stateId}>{items.name}</option>
                            })
                          }
                        </Form.Control>
                        {
                          errors.state &&
                          <span className="help-block error-text">{errors.state}</span>
                        }
                      </Form.Group>
                    </Col>
                    <Col md={4}>
                      <Form.Group controlId="formBasicZipcode" className="mb-4">
                        <Form.Control
                          type="text"
                          placeholder="Zip Code"
                          className="inputType1 quantity"
                          value={zipCode}
                          //onChange={(e) => zipCodeChange(e.target.value)} 
                          onChange={(e) =>
                            e.target.value.match('^[+0-9 ]*$') != null ?
                              zipCodeChange(e.target.value)
                              : ''
                          }
                        />
                        {
                          errors.zipCode &&
                          <span className="help-block error-text">{errors.zipCode}
                          </span>
                        }


                      </Form.Group>
                    </Col>
                    <Col md={6}>
                      <Form.Group controlId="formBasicPwd" className="mb-4">
                        <Form.Control
                          type="password"
                          placeholder="Password"
                          className="inputType1"
                          value={password}
                          onChange={(e) => setPassword(e.target.value)} />
                        {
                          errors.password &&
                          <span className="help-block error-text">{errors.password}</span>
                        }
                      </Form.Group>
                    </Col>
                    <Col md={6}>
                      <Form.Group controlId="formBasicCpwd" className="mb-4">
                        <Form.Control
                          type="password"
                          placeholder="Confirm Password"
                          className="inputType1"
                          value={confPassword}
                          onChange={(e) => setConfPassword(e.target.value)} />
                        {
                          errors.confPassword &&
                          <span className="help-block error-text">{errors.confPassword}</span>
                        }
                      </Form.Group>
                    </Col>
                    <Col md={12}>    
                      <Form.Group controlId="formBasicCheckbox" className="mb-3">
                        <Form.Check type="checkbox" checked={check ? true : false} className="checkboxTyp1 mang-align" label="" onChange={(e) => conditionCheck(e)} />     
                        <span className="left-pad">I agree to the <span className="fw600 pointer" onClick={() => handleShow('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.')}>Terms & Conditions</span></span>  
                      </Form.Group>
                      <div className="mt-3 text-center align-items-center">
                        <Button
                          className="btnType4 fw600 mr-4 mb-3"
                          disabled={check ? false : true}
                          onClick={() => registerSubmit()}>
                          {
                            loading ?
                              <Loader type="bubble-top" bgColor={"#414180"} size={25} />
                              :
                              "Sign Up"
                          }
                        </Button>

                        <div className="fw500 col5">Already have an account?
                          <span className="col1 fw700 pointer ml-2" onClick={() => goToLogin()}>Sign In</span></div>
                      </div>
                    </Col>
                  </Row>
                </Form>
              </div>
            </div>
          </div>
        </Col>
      </Container>

      <TipModal tipsData= {tipsData} title={"Terms & Conditions"} show= {show} handleClose= {handleClose}></TipModal>

    </div>
  )
}

export default StudentRegister;