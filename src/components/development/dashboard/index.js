import React from "react";
import { Route } from "react-router-dom";

import dashboard from "./dashboard";


const Landing = ({ match }) => (
  <div>
    <Route path={`${match.url}/dashboard`} component={dashboard} />
  </div>
);

export default Landing;
