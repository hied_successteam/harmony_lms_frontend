import React, { useState, useEffect } from 'react';
import { Col, Container, Image } from 'react-bootstrap';
import { isChrome, 
     //browserName
 } from "react-device-detect";
import Logos from '../../../assets/images/logos.png';
import Graduate from '../../../assets/images/svg/graduated.svg';
import Colleges from '../../../assets/images/svg/colleges.svg';

const Dashboard = (props) => {    

     useEffect(() => {
          if (!isChrome) {
               alert("For better application compatibility, use Google chrome only.")
          }
     }, [])

     const loginPage = (status) => {
          var pageType = {
               pathname: status == "student" ? '/student/login' : '/college/login'
          }
          props.history.push(pageType);
     }
     return (
          <div className="bgBanner d-flex align-items-center justify-content-center w-100">
               {/* <div className="topBox bgCol1">
                For better application compatibility, use Google chrome only.                
            </div> */}
               <Container>
                    <Col lg={10} md={10} className="m-auto">     
                         <div className="studentBox text-center bgCol3 shadow1 br10">
                              <div>
                                   <Image src={Logos} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" />
                                   <div className="studentInfo d-flex justify-content-center">
                                        <div className="boxLeft mr-5">
                                             {/* mr-5   */}
                                             <div className="boxInner bgCol3 shadow2 br8 pt-1 d-flex align-items-center justify-content-center" onClick={() => loginPage("student")}>
                                                  <Image src={Graduate} alt="Graduate" className="pointer" />
                                             </div>
                                             <div className="fw600 col2 mt-3">Student</div>
                                        </div>
                                        <div className="boxRight">
                                             <div className="boxInner bgCol3 shadow2 br8 pt-1 d-flex align-items-center justify-content-center" onClick={() => loginPage("college")}>
                                                  <Image src={Colleges} alt="Graduate" className="pointer" />
                                             </div>
                                             <div className="fw600 col2 mt-3">College</div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </Col>
               </Container>
          </div>
     )
}



export default Dashboard;

