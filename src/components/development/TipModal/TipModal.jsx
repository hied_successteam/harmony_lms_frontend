
import {Modal, Button } from 'react-bootstrap';

const TipModal = (props) =>{

  return(
    <Modal className="modalType4 modal-tip" show={props.show} onHide={props.handleClose} animation={false} >
      <Modal.Header closeButton style={{backgroundColor: '#fff', color: '#000', borderRadius: 10}}>
        <Modal.Title>{props.title}</Modal.Title>
      </Modal.Header>
      <Modal.Body style={{backgroundColor: '#fff', color: '#000', borderRadius: 10}}>
        <div className="mb-4">
          {props.tipsData}
        </div>
        <div className="text-right">
          <Button variant="primary" onClick={props.handleClose}>
            Okay
          </Button>
        </div>
      </Modal.Body>
    </Modal>
  )
  
}

export default TipModal