import React, { Suspense } from "react";
import { Modal } from 'react-bootstrap';
import { Route } from "react-router-dom";
import AdminPublicRoute from '../../../routes/AdminPublicRoute';
import AdminPrivateRoute from '../../../routes/AdminPrivateRoute';
import Login from "../admin/login";
import Transactions from "../admin/Transactions";
import StudentList from "../admin/StudentList";
import StudentView from "../admin/StudentView";
import CollegeList from "../admin/CollegeList";
import CollegeProfile from "../admin/CollegeView";


const AdminModule = ({ match }) => (
  <div> 
    <AdminPublicRoute path={`${match.url}/login`} component={Login} />  
    <AdminPrivateRoute path={`${match.url}/Transactions`} component={Transactions} />
    <AdminPrivateRoute path={`${match.url}/StudentList`} component={StudentList} />
    <AdminPrivateRoute path={`${match.url}/studentview`} component={StudentView} />    
    <AdminPrivateRoute path={`${match.url}/InstituteList`} component={CollegeList} />
    <AdminPrivateRoute path={`${match.url}/collegedetails`} component={CollegeProfile} />
  </div>
);

export default AdminModule;
