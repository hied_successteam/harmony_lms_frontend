import React, { useState, useEffect } from 'react';
import { Col, Row, Image, Form, Tab, Modal, Accordion, Card, Table } from 'react-bootstrap';
import { useHistory, useLocation } from 'react-router-dom';
import Logoadmin from '../../../assets/images/svg/logoadmin.svg';
import SearchIcon from '../../../assets/images/svg/MagnifyingGlass.svg';
import { studentList, studentListView } from '../../../Actions/Admin/Admin';
import Leftbar from './Leftbar';
import RightBar from './RightBar';
import MyprofileContent from '../student/myprofile/MyprofileContent';
//import ReactToPdf from "react-to-pdf";
import ReactDOMServer from "react-dom/server";
//import ReactPDF from '@react-pdf/renderer';
//import { PDFDownloadLink, Page, Text, View, Document, StyleSheet } from '@react-pdf/renderer';
//import { PDFViewer } from '@react-pdf/renderer';
//import html2canvas from 'html2canvas';
//import * as htmlToImage from 'html-to-image';
//import { toPng, toJpeg, toBlob, toPixelData, toSvg } from 'html-to-image';
import { jsPDF } from "jspdf";


const StudentView = (props) => {
     const [profileData, setProfileData] = useState({});
     const history = useHistory();
     const location = useLocation();
     const { pathname } = location;
     const splitLocation = pathname.split("/");
     const ref = React.createRef();
     const ref2 = React.createRef();

     const options = {
          orientation: 'p',
          unit: 'mm',
          format: "Letter",
          scaleFactor: '2',
          margin: 3
     };      

     // const printDocument = () => {

     //      const pdf = new jsPDF();
     //      const input = document.getElementById('divToPrint');
     //      html2canvas(input)
     //           .then((canvas) => {
     //                const imgData = canvas.toDataURL('image/png');

     //                pdf.addImage(imgData, 'JPEG', 0, 0);
     //                // pdf.output('dataurlnewwindow');

     //           })
     //           ;

     //      setTimeout(() => {
     //           pdf.save("download.pdf");
     //      }, 8000);


     // }

     const saveNew = () => {

          const docA = new jsPDF('p', 'pt', 'letter');

          //const docA = new jsPDF();

          docA.canvas.height = 72 * 11;
          docA.canvas.width = 72 * 8.5;

         docA.setFontSize(10);

          docA.html(ReactDOMServer.renderToStaticMarkup(
               <div
               style={{
                    backgroundColor: '#f5f5f5',
                    width: '450pt',
                    height: '700pt',
                    minHeight: '297pt',
                    marginLeft: 'auto',
                    marginRight: 'auto',
                    fontSize: '10px',
               }}>
                    <MyprofileContent
                         {...props}
                         profileData={profileData}
                         myprofileHeading={false}
                         hidePlan={false}
                    />
               </div>
          ), {
               callback: () => {
                   
                    docA.save("myDocumentAA.pdf");                   
               },                     
               autoPaging: 'text',              
               //x: 10,
               //y: 10 ,
               scale: 0.1,     
               margin: [10, 10, 10, 10]
          });
     };

     const toPdfAA = () => {

          // const string = renderToString(<MyprofileContent
          //      {...props}
          //      profileData={profileData}
          //      myprofileHeading={false}
          // />);

          // const pdf = new jsPDF({
          //      orientation: "p",
          //      unit: "in",
          //      format: "A4"
          // });

          // pdf.html(string);

          var doc = new jsPDF();
          doc.html(ReactDOMServer.renderToStaticMarkup(
               <MyprofileContent
                    {...props}
                    profileData={profileData}
                    myprofileHeading={false}
               />
          ));

          //doc.save('demo.pdf')
          setTimeout(() => {
               doc.save("myDocumentss.pdf");
          }, 8000);

     }




     const toPdfAAvvvv = () => {

          // const pdf = new jsPDF();
          // let pdfWidth = 0
          // let pdfHeight = 0
          // let pdfHeight2 = 0
          // htmlToImage.toPng(document.querySelector('.exportnew'), { quality: 0.95 })
          //      .then(function (dataUrl) {
          //           var link = document.createElement('a');
          //           link.download = 'my-image-name.jpeg';
          //           const imgProps = pdf.getImageProperties(dataUrl);
          //           pdfWidth = pdf.internal.pageSize.getWidth();
          //           pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
          //           console.log('pdfWidth,pdfHeight', pdfWidth, pdfHeight);
          //           pdf.addImage(dataUrl, 'PNG', 1, 1, pdfWidth, pdfHeight);
          //           pdf.addPage();
          //      });

          // htmlToImage.toPng(document.querySelector('.exportnew2'), { quality: 0.95 })
          //      .then(function (dataUrl2) {
          //           var link = document.createElement('a');
          //           link.download = 'my-image-name.jpeg';
          //           const imgProps = pdf.getImageProperties(dataUrl2);
          //           let pdfWidth = pdf.internal.pageSize.getWidth();
          //           pdfHeight2 = (imgProps.height * pdfWidth) / imgProps.width;
          //           console.log('pdfWidth,pdfHeight', pdfWidth, pdfHeight2);
          //           pdf.addImage(dataUrl2, 'PNG', 1, 1, pdfWidth, pdfHeight2);
          //           pdf.addPage();

          //      });



          // setTimeout(() => {
          //      pdf.save("downloadaaa.pdf");
          // }, 4000);


          //      let elems = document.querySelectorAll('.exportnew');

          //     // let pdf = new jsPDF("portrait", "mm", "a4");
          //      const pdf = new jsPDF();

          //      pdf.scaleFactor = 2;

          //      let addPages = new Promise((resolve, reject) => {

          //           elems.forEach((elem, idx) => {

          //                htmlToImage.toPng(elem, { quality: 0.95 })
          //                .then(function (dataUrl) {

          //                  var link = document.createElement('a');
          //                  link.download = 'my-image-name.jpeg';                    
          //                  const imgProps= pdf.getImageProperties(dataUrl);
          //                  const pdfWidth = pdf.internal.pageSize.getWidth();
          //                  const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;

          //                  if (idx < elems.length - 1) {
          //                  pdf.addImage(dataUrl, 'PNG', 0, 0,pdfWidth, pdfHeight);        
          //                  pdf.addPage();  
          //                  }else
          //                  {
          //                     pdf.addImage(dataUrl, 'PNG', 0, 0,pdfWidth, pdfHeight); 
          //                  }      

          //                });



          //                // html2canvas(elem, { scale: "2" })
          //                //      .then(canvas => {
          //                //           if (idx < elems.length - 1) {
          //                //                console.log("Reached frst page, completing",idx,elems.length);
          //                //                pdf.addImage(canvas.toDataURL("image/png"), 0, 0, 210, 297);
          //                //                pdf.addPage();
          //                //           } else {
          //                //                pdf.addImage(canvas.toDataURL("image/png"), 0, 0, 210, 297);
          //                //                console.log("Reached last page, completing",idx,elems.length);
          //                //           }
          //                //      })
          //           })

          //           console.log('end');             
          //           setTimeout(resolve, 6000, "Timeout adding page #");

          //      });

          //      addPages.finally(() => {
          //           console.log("Saving PDF");
          //           pdf.save("xddd.pdf");
          //      });



          // htmlToImage.toPng(document.getElementById('capture'), { quality: 0.95 })
          // .then(function (dataUrl) {
          //   var link = document.createElement('a');
          //   link.download = 'my-image-name.jpeg';
          //   const pdf = new jsPDF();
          //   const imgProps= pdf.getImageProperties(dataUrl);
          //   const pdfWidth = pdf.internal.pageSize.getWidth();
          //   const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;            

          //   for (let index = 0; index <= pdfHeight; index=index+200) {
          //      pdf.addImage(dataUrl, 'PNG', 0, 0,pdfWidth, pdfHeight);    
          //      pdf.addPage();           
          //   }           

          //   pdf.save("finalff.pdf"); 
          // });


     }



     useEffect(() => {

          let userId = history &&
               history.location &&
               history.location.state &&
               history.location.state.userId
               ? history.location.state.userId
               : false;

          if (userId) {
               studentListView(userId).then(res => {
                    setProfileData(res?.data?.data ? res.data.data : {});
               }).catch(err => {
               });
          }
     }, [props]);

     const backToList = () =>{
          if(props && props.location && props.location.page_data){
          props.history.push({pathname: '/admin/StudentList', page_data: props.location.page_data})
          }else{
          props.history.push({pathname: '/admin/StudentList'})
          }
     }

     return (
          <div className="wizardLayout userDashboard">
               <div className="container-fluid pl-0">
                    <Row>
                         <Leftbar {...props}
                              splitLocation={splitLocation}
                         />

                         <Col sm={9}>

                              <div className="userrightBox pr-4">
                                   <RightBar {...props} />
                                   <div>
                                        {/* <ReactToPdf targetRef={ref} filename="div-bluedda.pdf" options={options} x={.5} y={.5} scale={0.57}>
                                             {({ toPdf }) => (
                                                  <button onClick={toPdf}>Generate pdf</button>
                                             )}
                                        </ReactToPdf> */}

                                        {/* <button onClick={saveNew}>Generate pdf</button> */}

                                        {/* <PDFDownloadLink document={<MyDocaaa />} fileName="somename.pdf">
                                             {({ blob, url, loading, error }) =>
                                                  loading ? 'Loading document...' : 'Download now!'
                                             }
                                        </PDFDownloadLink> */}



                                   </div>
                                   <div className="pl-4 pr-4 pt-4" ref={ref} id="capture" >

                                        <MyprofileContent
                                             {...props}
                                             profileData={profileData}
                                             myprofileHeading={false}
                                             pageBack={backToList}
                                        />
                                   </div>

                                   {/* <div id="divToPrint" className="mt4" style={{
                                        backgroundColor: '#f5f5f5',
                                        width: '210mm',
                                        minHeight: '297mm',
                                        marginLeft: 'auto',
                                        marginRight: 'auto'
                                   }}>
                                        <div>Note: Here the dimensions of div are same as A4</div>
                                        <div>
                                             <MyprofileContent
                                                  {...props}
                                                  profileData={profileData}
                                                  myprofileHeading={false}
                                             />
                                        </div>
                                   </div> */}

                              </div>

                         </Col>

                    </Row>
               </div>
          </div>
     )
}

export default StudentView;





