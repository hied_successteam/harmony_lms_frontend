import React, { useState, useEffect } from 'react';
import { Dropdown} from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import { getLocalStorage,  clearLocalStorage } from '../../../Lib/Utils';
import { logout } from '../../../Actions/Admin/Admin';
import { actionLogout } from '../../../Redux/College/LoginAction';
const RightBar = (props) => {

    const dispatch = useDispatch();

  const logoutCall = () => {

    var admin = getLocalStorage('admin');  
    
    if (admin && admin._id) {
        logout({
            userId: admin._id
        }).then(res => {   
            if (res.data.success == true) {
                dispatch(actionLogout('college'));
                localStorage.clear();
                clearLocalStorage();
                props.history.push({ pathname: '/admin/login' });
            } else {
                dispatch(actionLogout('college'));
                localStorage.clear();
                clearLocalStorage();
                props.history.push({ pathname: '/admin/login' });
            }
        }).catch(err => {
            dispatch(actionLogout('college'));
            localStorage.clear();
            clearLocalStorage();
            props.history.push({ pathname: '/admin/login' });
        });
    } else {
        dispatch(actionLogout('college'));
        localStorage.clear();
        clearLocalStorage();
        props.history.push({ pathname: '/admin/login' });
    }
}

  return (
        <>
        <div className="rightUser">         
                <div className="d-flex justify-content-end mt-4">
                <Dropdown className="DropdownType2">
                    <Dropdown.Toggle id="dropdown-basic" className="DropdownType1 col2 fs14 fw600">
                   {getLocalStorage('admin').firstName+' '+getLocalStorage('admin').lastName}
                   <span className="circleType3 d-inline-block br50 text-center">
                        <i className="fa fa-chevron-down col1" aria-hidden="true"></i> </span>
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                      {/* <Dropdown.Item href="#">Profile</Dropdown.Item>                       */}
                      <Dropdown.Item onClick={() => logoutCall()}>Logout</Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                  
                </div>
          </div>            
        </>
    )
}
export default RightBar;