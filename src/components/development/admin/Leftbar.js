import React, { useState, useEffect } from 'react';
import { Col, Row, Image, Form, Tab, Modal, Accordion, Card, Table, Nav } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { getLocalStorage, clearLocalStorage } from '../../../Lib/Utils';
import Logoadmin from '../../../assets/images/svg/logoadmin.svg';
import UserOne from '../../../assets/images/svg/user1.png';  
import UserBlue from '../../../assets/images/svg/userblue.png';    
import Transaction from '../../../assets/images/svg/transaction.png';  
import TransactionBlue from '../../../assets/images/svg/transactionblue.png';  
import Dash from '../../../assets/images/svg/dash.png';  
import DashBlue from '../../../assets/images/svg/dashblue.png';      
import { logout } from '../../../Actions/Admin/Admin';
import { actionLogout } from '../../../Redux/College/LoginAction';
//import TransactionIcon from '../../../assets/images/transacttions.png';

const Leftbar = (props) => {    

    const dispatch = useDispatch();
    

    const logoutCall = () => {

        var admin = getLocalStorage('admin');
        if (admin && admin._id) {
            logout({
                userId: admin._id
            }).then(res => {
                if (res.data.success == true) {
                    dispatch(actionLogout('college'));
                    localStorage.clear();
                    clearLocalStorage();
                    props.history.push({ pathname: '/admin/login' });
                } else {
                    dispatch(actionLogout('college'));
                    localStorage.clear();
                    clearLocalStorage();
                    props.history.push({ pathname: '/admin/login' });
                }
            }).catch(err => {
                dispatch(actionLogout('college'));
                localStorage.clear();
                clearLocalStorage();
                props.history.push({ pathname: '/admin/login' });
            });
        } else {
            dispatch(actionLogout('college'));
            localStorage.clear();
            clearLocalStorage();
            props.history.push({ pathname: '/admin/login' });
        }
    }

    return (
        <>
            <Col sm={3}>
                <div className="sidebarBox">
                    <div className="pl-4 pt-4 mt-2 mb-5 d-flex align-items-center">
                        <Image src={Logoadmin} alt="HiEd Success" title="Welecome to HiEd Success" className="logos" />
                        <span className="fs18 col3 fw600 ml-2">HiEd Harmony</span>
                    </div>
                    <Nav variant="pills" className="flex-column innerBox"> 
                        <Nav.Item>
                            <NavLink
                                exact
                                activeClassName="active"
                                to="/admin/studentList"
                                className={`listBox nav-link ${props && props.splitLocation && props.splitLocation[2] === 'studentview' ? 'active' : '' }`}>
                                {/* <i className="fa fa-university" aria-hidden="true"></i> */}
                                <Image src={UserBlue} alt="icon" className="w20 hideIcon1 usershow" />
                                <Image src={UserOne} alt="icon" className="w20 showIcon usershow" />          
                                <span className="titles text-uppercase fs15 d-block">students</span>  
                            </NavLink>    
                        </Nav.Item>

                        <Nav.Item>       
                            <NavLink
                                exact
                                activeClassName="active"
                                to="/admin/InstituteList"
                                className={`listBox nav-link ${props && props.splitLocation && props.splitLocation[2] === 'collegedetails' ? 'active' : '' }`}>
                                <Image src={DashBlue} alt="icon" className="w20 hideIcon1 dashshow" />
                                <Image src={Dash} alt="icon" className="w20 showIcon dashshow" />   
                                <span className="titles text-uppercase fs15 d-block">Institutes</span>
                            </NavLink>
                        </Nav.Item>
                        <Nav.Item>
                            <NavLink
                               exact                                 
                                activeClassName="active"
                                to="/admin/Transactions"
                                className="listBox nav-link">
                                <Image src={TransactionBlue} alt="icon" className="w20 hideIcon1 dashshow" />
                                <Image src={Transaction} alt="icon" className="w20 showIcon dashshow" />   
                                <span className="titles text-uppercase fs15 d-block">Transactions</span>
                            </NavLink>
                        </Nav.Item>

                        <Nav.Item onClick={() => logoutCall()}>
                            <Nav.Link className="listBox">
                                <i className="fa fa-sign-out" aria-hidden="true"></i>
                                <span className="titles text-uppercase fs15 d-block">Logout</span>
                            </Nav.Link>
                        </Nav.Item>
                    </Nav>
                </div>
            </Col>
        </>
    )

}


export default Leftbar;