import React, { useState, useEffect } from 'react';
import { Col, Row, Image, Form, Tab, Modal, Accordion, Card, Table } from 'react-bootstrap';
import { useHistory, useLocation } from 'react-router-dom';
import Logoadmin from '../../../assets/images/svg/logoadmin.svg';
import SearchIcon from '../../../assets/images/svg/MagnifyingGlass.svg';
import { studentList, studentListView, collegeViewProfile } from '../../../Actions/Admin/Admin';
import Leftbar from './Leftbar';
import RightBar from './RightBar';
import CollegeProfileContent from '../college/myprofile/CollegeProfileContent';


const CollegeView = (props) => {
     const [profileData, setProfileData] = useState({});
     const history = useHistory();
     const location = useLocation();
     const { pathname } = location;
     const splitLocation = pathname.split("/");

     //  useEffect(() => {

     //       let userId = history &&
     //            history.location &&
     //            history.location.state &&
     //            history.location.state.userId
     //            ? history.location.state.userId
     //            : false;

     //       if (userId) {
     //            studentListView(userId).then(res => {
     //                 console.log(res.data.data)
     //                 setProfileData(res?.data?.data ? res.data.data : {});
     //            }).catch(err => {
     //            });
     //       }
     //  }, []);

     useEffect(() => {
          if (props && props.location && props.location.user_ID) {
               collegeViewProfile(props.location.user_ID).then(res => {
                    setProfileData(res.data.data);
               }).catch(err => {
               });
          }
     }, [])

     const backToList = () =>{
          if(props && props.location && props.location.page_data){
          props.history.push({pathname: '/admin/InstituteList', page_data: props.location.page_data})
          }else{
          props.history.push({pathname: '/admin/InstituteList'})
          }
     }

     return (
          <div className="wizardLayout userDashboard">
               <div className="container-fluid pl-0">
                    <Row>
                         <Leftbar {...props}
                              splitLocation={splitLocation}
                         />

                         <Col sm={9}>

                              <div className="userrightBox pr-4">
                                   <RightBar {...props} />
                                   <div className="pl-4 pr-4 pt-4">
                                        <CollegeProfileContent
                                             {...props}
                                             profileData={profileData}
                                             myprofileHeading={false}
                                             pageBack={backToList}
                                        />
                                   </div>

                              </div>
                         </Col>

                    </Row>
               </div>
          </div>
     )
}

export default CollegeView;

