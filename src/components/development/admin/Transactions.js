import React, { useState, useEffect } from "react";
import {
  Col, Row, Image, Form, Table, Modal, Accordion,
  Card, Button
} from "react-bootstrap";
import Logoadmin from "../../../assets/images/svg/logoadmin.svg";
import SearchIcon from "../../../assets/images/svg/MagnifyingGlass.svg";
import { transactionList } from "../../../Actions/Admin/Admin";
import Leftbar from "./Leftbar";
import RightBar from "./RightBar";
import moment from "moment";
import Pagination from "react-bootstrap-4-pagination";
import Loader from "../../../Lib/LoaderNew";
import { arrToStr } from '../../../Lib/Utils';

const Transactions = (props) => {
  const [transactionData, setTransactionData] = useState({});
  const [totalRecords, setTotalRecords] = useState(0);
  const [totalPages, setTotalPages] = useState(1);
  const [currentPage, setCurrentPage] = useState(1);
  const [numOfRecord, setnumOfRecord] = useState(10);
  const [searchKey, setSearchKey] = useState("");
  const limitArr = [10, 20, 30, 40, 50];
  const [loader, setLoader] = useState(false);
  const [planDatas, setPlanDatas] = useState(['Free', 'Basic', 'Standard', 'Premium']);
  const [purchaseTypeDatas, setPurchaseTypeDatas] = useState(["Plan", "Service"]);
  const [plan, setPlan] = useState("");
  const [purchaseType, setPurchaseType] = useState("");

  useEffect(() => {
    // transactionList().then(res => {
    //   setTransactionData(res.data.data);
    // }).catch(err => {
    // });
    getTransactionList(parseInt(numOfRecord), currentPage - 1, searchKey);
  }, []);
  

  const onchangeInput = event => {

    let { target: { name, value } } = event;
    if (name == "plan") {
      setPlan(value)
    } else if (name == "purchaseType") {
      setPurchaseType(value)
    }
  };


  const getTransactionList = (num_Of_records, curr_page, search = "") => {
    setLoader(true);
    transactionList(
      `limit=${num_Of_records}&offset=${curr_page}&search=${search}&plan=${plan}&purchasetype=${purchaseType}`
    )
      .then((res) => {
        setLoader(false);
        setTransactionData(res?.data?.data?.rows ? res.data.data : []);
        setTotalRecords(res?.data?.data?.total ? res.data.data.total : 0);
        if (res.data.data.total <= 10) {
          setTotalPages(1);
        } else {
          if (Number.isInteger(res.data.data.total / num_Of_records)) {
            setTotalPages(parseInt(res.data.data.total / num_Of_records));
          } else {
            setTotalPages(parseInt(res.data.data.total / num_Of_records) + 1);
          }
        }
      })
      .catch((err) => {
        setLoader(false);
      });
  };

  const paginationConfig = {
    totalPages: totalPages ? totalPages : 5,
    currentPage: currentPage ? currentPage : 1,
    showMax: totalPages === currentPage ? 2 : 5,
    borderColor:
      currentPage === 1 || totalPages === currentPage ? "#f8f9fa" : "#604BE8",
    prevText: currentPage === 1 ? "" : "<",
    nextText: totalPages === currentPage ? "" : ">",
    // size: "lg",
    threeDots: true,
    prevNext: true,
    onClick: function (page) {
      window.scrollTo(0, 0);
      setCurrentPage(page);
      getTransactionList(numOfRecord, page - 1, searchKey);
    },
  };

  const paginationOnChange = (evnt) => {
    setnumOfRecord(evnt.target.value);
    setCurrentPage(1);
    getTransactionList(evnt.target.value, 0, searchKey);
  };

  const SearchResult = () => {
    getTransactionList(parseInt(numOfRecord), 0, searchKey);
  };

  const ResetSearch = () => {
    setCurrentPage(1)
    setnumOfRecord(10)
    setSearchKey('')
    setPlan('')
    setPurchaseType('')
    getTransactionList(10, 0, "");
  }

  const onKeyPress = (e) => {
    if (e.which == 13) {
      getTransactionList(parseInt(numOfRecord), 0, searchKey);
    }
  };

  const setSearchKeyOnCHange = (e) => {
    if (!e.target.value) {
      getTransactionList(parseInt(numOfRecord), 0, "");
    }
    setSearchKey(e.target.value);
  };

  return (
    <div className="wizardLayout userDashboard">
      <div className="container-fluid pl-0">
        <Row>
          <Leftbar {...props} />

          <Col sm={9}>
            <div className="userrightBox pr-4">      
              <RightBar {...props} />  
              <div className="rightUser">
                <div className="filterType1 mt-3">
                  <Accordion>
                    <Card className="shadow4">
                      <Card.Header>
                        <Accordion.Toggle variant="link" eventKey="0">
                          Filters <i className="fa fa-chevron-down arrowA" aria-hidden="true"></i>
                        </Accordion.Toggle>
                      </Card.Header>
                      <Accordion.Collapse eventKey="0">
                        <Card.Body className="bgCol3">
                          <div>
                            <Row>

                              <Col md={6}>
                                <Form.Group controlId="exampleOne" className="mb-4">
                                  <Form.Control
                                    type="text"
                                    placeholder="Search by Full Name / Email / Mobile No. / Tranx ID"
                                    name="searchKey"
                                    value={searchKey}
                                    className="inputType1 select2"
                                    autoComplete="off"
                                    onKeyPress={(e) => onKeyPress(e)}
                                    onChange={(e) => setSearchKeyOnCHange(e)}
                                  />
                                  {/* <Image
                                    src={SearchIcon}
                                    alt="Search"
                                    className="pointer searchOne"
                                    onClick={() => SearchResult()}
                                  /> */}
                                </Form.Group>
                              </Col>
                              <Col md={3}>
                                <Form.Group controlId="exampleOne" className="mb-4">
                                  <Form.Control
                                    as="select"
                                    name="plan"
                                    value={plan}
                                    onChange={onchangeInput}
                                    className="selectTyp1 select2 pointer"
                                  >
                                    <option value="">Select Plan</option>
                                    {
                                      planDatas && planDatas.length &&
                                      planDatas.map((item, index) => {
                                        return <option
                                          key={index}
                                          title={item}
                                          value={item}>
                                          {item}</option>
                                      })
                                    }
                                  </Form.Control>
                                </Form.Group>
                              </Col>
                              <Col md={3}>    
                                <Form.Group controlId="exampleOne" className="mb-4">
                                  <Form.Control
                                    as="select"
                                    name="purchaseType"
                                    value={purchaseType}
                                    onChange={onchangeInput}
                                    className="selectTyp1 select2 pointer"
                                  >
                                    <option value="">Select PurchaseType</option>
                                    {
                                      purchaseTypeDatas && purchaseTypeDatas.length &&
                                      purchaseTypeDatas.map((item, index) => {
                                        return <option
                                          key={index}
                                          title={item}
                                          value={item}>
                                          {item}</option>
                                      })
                                    }
                                  </Form.Control>
                                </Form.Group>
                              </Col>

                            </Row>
                            <Row>
                              <Col md={12}>
                                <Button
                                  type="button"
                                  className="btnType3 updateBlue mr-4"
                                  onClick={() => SearchResult()}
                                >
                                  Search
                                </Button>
                                <Button
                                  type="button"
                                  className="btnType3 fillRed"
                                  onClick={() => ResetSearch()}
                                >
                                  Reset
                                </Button>
                              </Col>
                            </Row>
                          </div>
                        </Card.Body>
                      </Accordion.Collapse>
                    </Card>
                  </Accordion>
                </div>
              </div>

              <Row>
                <Col md={5}>
                  <div className="rightBox pt-4 pb-2 pl-2">
                    <div className="searchBox d-flex justify-content-center">
                      {/* <Form.Group className="position-relative w-100">
                        <Form.Control
                          type="text"
                          placeholder="Search..."
                          name="searchKey"
                          value={searchKey}
                          className="searchType1 shadow3 w-100"
                          autoComplete="off"
                          onKeyPress={(e) => onKeyPress(e)}
                          onChange={(e) => setSearchKeyOnCHange(e)}
                        />
                        <Image
                          src={SearchIcon}
                          alt="Search"
                          onClick={() => SearchResult()}
                          className="pointer searchOne"
                        />
                      </Form.Group> */}
                    </div>
                  </div>
                </Col>
              </Row>
              <Row>
                <Col md={12}>
                  {/* start from here  */}
                  <div className="table-responsive pl-2">
                    <Table bordered className="studentTable transactionTable br10 mb-4">  
                      <thead>
                        <tr>
                          <th>Full Name</th>
                          <th>Email</th>
                          <th>Mobile Number</th>
                          <th>Amount</th>
                          <th>Payment Date</th>
                          <th>Purchase Type</th>
                          <th>Plan</th>
                          <th>Service</th>
                          <th>Transaction Id</th>
                          <th>Transaction Status</th>
                          <th>Payer Name</th>
                          <th>Payer Email</th>
                          <th>Payer Phone </th>
                        </tr>
                      </thead>
                      <tbody>
                        {transactionData &&
                          transactionData.rows &&
                          transactionData.rows.map((data, index) => (
                            <tr key={index}>
                              <td>
                                {data.user &&
                                  data.user.firstName &&
                                  data.user.lastName
                                  ? `${data.user.firstName} ${data.user.lastName}`
                                  : "-"}
                              </td>
                              <td>{data.user && data.user.email}</td>
                              <td>{data.user && data.user.mobileNumber}</td>
                              <td>{data?.amount ? data.amount : "-"}</td>
                              <td>
                                {moment(new Date(data.createdAt)).format(
                                  "MM/DD/YYYY"
                                )}
                              </td>
                              <td>
                                {data.purchaseType &&
                                  data.purchaseType.charAt(0).toUpperCase() +
                                  data.purchaseType.slice(1)}
                              </td>
                              <td>{data?.plan?.name ? data.plan.name : "-"}</td>
                              <td>
                                {/* {data?.service
                                  ? data.service.length > 0
                                    ? data.service.toString()
                                    : "-"
                                  : "-"} */}
                                {data.service && data.service.length > 0
                                  ? arrToStr(data.service)
                                  : "-"}
                              </td>
                              <td>{data?.paymentId ? data.paymentId : "-"}</td>
                              <td>
                                {data.status && data.status == "succeeded"
                                  ? "Success"
                                  : data.status.charAt(0).toUpperCase() +
                                  data.status.slice(1)}
                              </td>
                              <td>
                                {data?.source?.billing_details?.name
                                  ? data.source.billing_details.name
                                  : "-"}
                              </td>
                              <td>
                                {data?.source?.billing_details?.email
                                  ? data.source.billing_details.email
                                  : "-"}
                              </td>
                              <td>
                                {data?.source?.billing_details?.phone
                                  ? data.source.billing_details.phone
                                  : "-"}
                              </td>
                            </tr>
                          ))}
                      </tbody>
                    </Table>
                  </div>
                </Col>
              </Row>

              {totalRecords >= 10 ? (
                <div className="d-flex mt-5 mb-5 justify-content-between">
                  <div className="d-flex align-items-center">
                    <div className="mr-2">Show</div>
                    {/* <Form.Select>  
                                                                                       <option>Large select</option>
                                                                                   </Form.Select> */}
                    <Form.Control
                      as="select"
                      className="selectTyp1 selectNumber1"
                      onChange={(e) => paginationOnChange(e)}
                      value={numOfRecord}
                    >
                      {limitArr.map((cVal, cIndx) => {
                        return (
                          <option key={cIndx} value={cVal}>
                            {cVal}
                          </option>
                        );
                      })}
                    </Form.Control>
                  </div>
                  {totalRecords > numOfRecord ? (
                    <div className="paginationType4">
                      <Pagination {...paginationConfig} />
                    </div>
                  ) : (
                      ""
                    )}
                </div>
              ) : null}
            </div>
          </Col>
        </Row>
      </div>
      <Modal show={loader} centered className="loaderModal">
        <Loader />
      </Modal>
    </div>
  );
};

export default Transactions;
