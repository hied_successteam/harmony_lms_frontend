import React, { useState, useEffect } from "react";
import {
  Container,
  Col,
  Row,
  Image,
  Button,
  Dropdown,
  Form,
  Nav,
  Tab,
  Modal,
  Card,
  Accordion,
  Table,
} from "react-bootstrap";
import Logoadmin from "../../../assets/images/svg/logoadmin.svg";
import SearchIcon from "../../../assets/images/svg/MagnifyingGlass.svg"; 
import { collegeList } from "../../../Actions/Admin/Admin";
import { stateList } from "../../../Actions/Student/register";
import Leftbar from "./Leftbar";
import RightBar from "./RightBar";
import moment from "moment";
import Pagination from "react-bootstrap-4-pagination";
import Loader from "../../../Lib/LoaderNew";
import { arrToStr, getLocalStorage } from "../../../Lib/Utils";
import groupDownload from '../../../assets/images/groupdownload.png';
import CONSTANTS from '../../../Lib/Constants';

const CollegeList = (props) => {
  const [collegeListData, setCollegeListData] = useState([]);
  const [totalRecords, setTotalRecords] = useState(0);
  const [totalPages, setTotalPages] = useState(1);
  const [currentPage, setCurrentPage] = useState(1);
  const [numOfRecord, setnumOfRecord] = useState(10);
  const [searchKey, setSearchKey] = useState("");
  const limitArr = [10, 20, 30, 40, 50];
  const [loader, setLoader] = useState(false);
  const [referredbydatas, setreferredbydatas] = useState([
    "Preeti Tanwar",
    "Sukanya Bhowmik",
    "Kiran Yerawar",
    "Salman Shaikh",
    "Destiny Ihenacho",
    "Kiza Mauridi",
    "Archit Singh",
    "Vinod Badoni",
    "Ajay Sankhla",
    "Sreedhar Kodali",
    "Fermata",
    "Shwan Muhammed",
  ]);

  const [referredby, setReferredby] = useState("");
  const [isProfileCompleted,setisProfileCompleted] = useState("");

  useEffect(() => {
    if(props && props.location && props.location.page_data){
      setSearchKey(props.location.page_data.searchKey)
      setReferredby(props.location.page_data.referredby)
      setisProfileCompleted(props.location.page_data.isProfileCompleted)
      setCurrentPage(parseInt(props.location.page_data.currentPage))
      setnumOfRecord(props.location.page_data.numOfRecord)
      getcollegeList(props.location.page_data.numOfRecord, parseInt(props.location.page_data.currentPage) - 1, props.location.page_data.searchKey);
    }else{
      getcollegeList(numOfRecord, currentPage - 1, searchKey);
    }
  }, []);

  const getcollegeList = (num_Of_records, curr_page, search = "",status) => {
    setLoader(true);
    collegeList(`limit=${num_Of_records}&offset=${curr_page}&search=${search}&referredby=${status ? '' :referredby}&isProfileCompleted=${status ? '' :isProfileCompleted}`)
      .then((res) => {
        setLoader(false);
        setCollegeListData(res?.data?.data?.rows ? res.data.data.rows : []);
        setTotalRecords(res?.data?.data?.rows ? res.data.data.total : 0);
        if (res.data.data.total <= 10) {
          setTotalPages(1);
        } else {
          if (Number.isInteger(res.data.data.total / num_Of_records)) {
            setTotalPages(parseInt(res.data.data.total / num_Of_records));
          } else {
            setTotalPages(parseInt(res.data.data.total / num_Of_records) + 1);
          }
        }
      })
      .catch((err) => {
        setLoader(false);
      });
  };

  let paginationConfig = {
    totalPages: totalPages ? totalPages : 1,
    currentPage: currentPage ? currentPage : 1,
    showMax: totalPages === currentPage ? 2 : 5,
    borderColor:
      currentPage === 1 || totalPages === currentPage ? "#f8f9fa" : "#604BE8", 
    prevText: currentPage === 1 ? "" : "<",
    nextText: totalPages === currentPage ? "" : ">",
    threeDots: true,
    prevNext: true,
    onClick: function (page) {
      window.scrollTo(0, 0);
      setCurrentPage(page);
      getcollegeList(parseInt(numOfRecord), page - 1, searchKey); 
    },
  };

  const paginationOnChange = (evnt) => {
    window.scrollTo(0, 0);
    setnumOfRecord(evnt.target.value);
    setCurrentPage(1);
    getcollegeList(evnt.target.value, 0, searchKey);
  };

  const SearchResult = () => {
    setCurrentPage(1)
    getcollegeList(parseInt(numOfRecord), 0, searchKey);
  };

  const onKeyPress = (e) => {
    if (e.which == 13) {
      getcollegeList(parseInt(numOfRecord), 0, searchKey);
    }
  };

  const setSearchKeyOnCHange = (e) => {
    if (!e.target.value) {
      getcollegeList(numOfRecord, 0, "");
    }
    setSearchKey(e.target.value);
  };

  const viewCollegeDetail = (data) => {var params = {
    currentPage,
    numOfRecord,
    searchKey,
    referredby,
    isProfileCompleted
  }
    props.history.push({
      pathname: "/admin/collegedetails",
      user_ID: data._id,
      page_data: params
    });
  };

  const onchangeInput = (event) => {
    let {
      target: { name, value },
    } = event;
    if (name == "referredby") {
      setReferredby(value);
    }
    else if (name == "isProfileCompleted") {
     setisProfileCompleted(value)
   }  
  };

  const ResetSearch = () => {
     setCurrentPage(1)
     setnumOfRecord(10)
     setSearchKey('')
     setReferredby('')
     setisProfileCompleted('')
     getcollegeList(10, 0, "", 'refresh');
   }

   const exportColleges = () => {      
     let urlex = `${CONSTANTS.SERVER_URL}admin/college/download?token=${getLocalStorage('admin').accessToken}&limit=${numOfRecord}&offset=${currentPage - 1}&search=${searchKey}&referredby=${referredby}`;
     window.open(urlex, '_blank');
   }

  return (
    <div className="wizardLayout userDashboard">
      <div className="container-fluid pl-0">
        <Row>
          <Leftbar {...props} />

          <Col sm={9}>
            <div className="userrightBox pr-4">
              <RightBar {...props} />
              <div className="rightUser">
                <div className="filterType1 mt-3">
                  <Accordion>
                    <Card className="shadow4">
                      <Card.Header>
                        <Accordion.Toggle variant="link" eventKey="0">
                          Filters{" "}
                          <i
                            className="fa fa-chevron-down arrowA"
                            aria-hidden="true"
                          ></i>
                        </Accordion.Toggle>
                      </Card.Header>
                      <Accordion.Collapse eventKey="0">
                        <Card.Body className="bgCol3">
                          <div>
                            <Row>
                              <Col md={6}>
                                <Form.Group className="position-relative w-100">
                                  <Form.Control
                                    type="text"
                                    placeholder="Search by Institute Name / Full Name / Email / Phone No."
                                    name="searchKey"
                                    value={searchKey}
                                    className="inputType1 select2"
                                    autoComplete="off"
                                    onKeyPress={(e) => onKeyPress(e)}
                                    onChange={(e) =>
                                      setSearchKeyOnCHange(e)
                                    }
                                  />
                                </Form.Group>
                              </Col>
                              <Col md={3}>
                                <Form.Group
                                  controlId="exampleOne"
                                  className="mb-4"
                                >
                                  <Form.Control
                                    as="select"
                                    className="selectTyp1 select2 pointer"
                                    name="referredby"
                                    value={referredby}
                                    onChange={onchangeInput}
                                  >
                                    <option value="">Referred By</option>
                                    {referredbydatas &&
                                      referredbydatas.length &&
                                      referredbydatas.map((item, index) => {
                                        return (
                                          <option
                                            key={index}
                                            title={item}
                                            value={item}
                                          >
                                            {item}
                                          </option>
                                        );
                                      })}
                                  </Form.Control>
                                </Form.Group>
                              </Col>

                              <Col md={3}>
                                <Form.Group
                                  controlId="exampleOne"
                                  className="mb-4"
                                >
                                  <Form.Control
                                    as="select"
                                    className="selectTyp1 select2 pointer"
                                    name="isProfileCompleted"
                                    value={isProfileCompleted}
                                    onChange={onchangeInput}
                                  > 
                                   <option value="">Is Profile Completed</option>
                                   <option value={false}>Pending</option>
                                   <option value={true}>Completed</option>
                                  </Form.Control>
                                </Form.Group>
                              </Col>
                            </Row>
                            <Row>
                              <Col md={12}>
                                <Button
                                  type="button"
                                  className="btnType3 updateBlue mr-4"
                                  onClick={() => SearchResult()}
                                >
                                  Search
                                </Button>
                                <Button
                                  type="button"
                                  className="btnType3 fillRed"
                                  onClick={() => ResetSearch()}
                                >
                                  Reset
                                </Button>
                              </Col>
                            </Row>
                          </div>
                        </Card.Body>
                      </Accordion.Collapse>
                    </Card>
                  </Accordion>
                </div>
              </div>              
              <div className="text-right mt-2 mb-2">
                  <Image onClick={() => exportColleges()} src={groupDownload} alt="Export Institutes" title="Export Institutes" className="pointer" />
                </div> 
              <Row>
                <Col md={12}>
                  {/* start from here  */}
                  <div className="table-responsive pl-2">
                    <Table bordered className="studentTable br10 mb-4">
                      <thead>
                        <tr>
                          <th>Institute Name</th>
                          <th>Campus location (State)</th>
                          <th>Job Title</th>
                          <th>Full Name</th>
                          <th>Email</th>
                          <th>Phone Number</th>
                          <th>Created At</th>
                          <th>Is Profile Completed</th>
                          <th>Target Students</th>
                          <th>Referred By</th>
                          <th>Interested in Ads</th>
                          <th>Status</th>
                          <th>Updated At</th>
                        </tr>
                      </thead>
                      <tbody>
                        {collegeListData.length > 0
                          ? collegeListData.map((row, rowIndx) => {
                              return (
                                <tr key={rowIndx}>
                                  <td>
                                    <a
                                      className="pointer"
                                      onClick={() => viewCollegeDetail(row)}
                                    >
                                      {row?.instituteName
                                        ? row.instituteName
                                        : "-"}
                                    </a>
                                  </td>
                                  <td>
                                    {row?.campusState ? row.campusState : "-"}
                                  </td>
                                  <td>{row?.title ? row.title : "-"}</td>
                                  <td>
                                    {row?.firstName ? row.firstName : "-"}
                                  </td>
                                  <td>{row?.email ? row.email : "-"}</td>
                                  <td>
                                    {row?.mobileNumber ? row.mobileNumber : "-"}
                                  </td>
                                  <td>
                                    {row?.createdAt
                                      ? moment(new Date(row.createdAt)).format(
                                          "MM/DD/YYYY"
                                        )
                                      : "-"}
                                  </td>
                                  <td>
                                    {row.isProfileCompleted === true
                                      ? "Completed"
                                      : "Pending"}
                                  </td>
                                  <td>
                                    {row.targetStudents &&
                                    row.targetStudents.length > 0
                                      ? arrToStr(row.targetStudents)
                                      : "-"}
                                  </td>
                                  <td>
                                    {row?.referredby ? row.referredby : "-"}
                                  </td>
                                  
                                  <td>{row?.interestInAds ? row.interestInAds : "-"}</td>
                                  <td>
                                    {row?.status ? row.status : "-"}
                                  </td>
                                  <td>
                                    {row?.updatedAt
                                      ? moment(new Date(row.updatedAt)).format(
                                          "MM/DD/YYYY"
                                        )
                                      : "-"}
                                  </td>
                                </tr>
                              );
                            })
                          : []}
                      </tbody>
                    </Table>
                  </div>
                </Col>
              </Row>

              {collegeListData &&
                collegeListData.length > 0 &&
                totalRecords >= 10 && (
                  <div className="d-flex mt-5 mb-5 justify-content-between">
                    <div className="d-flex align-items-center">
                      <div className="mr-2">Show</div>
                      {/* <Form.Select>  
                                                                                        <option>Large select</option>
                                                                                    </Form.Select> */}
                      <Form.Control
                        as="select"
                        className="selectTyp1 selectNumber1"
                        onChange={(e) => paginationOnChange(e)}
                        value={numOfRecord}
                      >
                        {limitArr.map((cVal, cIndx) => {
                          return (
                            <option key={cIndx} value={cVal}>
                              {cVal}
                            </option>
                          );
                        })}
                      </Form.Control>
                    </div>
                    {totalRecords > numOfRecord ? (
                      <div className="paginationType4">
                        <Pagination {...paginationConfig} />
                      </div>
                    ) : (
                      ""
                    )}
                  </div>
                )}
            </div>
          </Col>
        </Row>
      </div>
      <Modal show={loader} centered className="loaderModal">
        <Loader />
      </Modal>
    </div>
  );
};

export default CollegeList;
