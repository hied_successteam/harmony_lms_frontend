import React, { useState, useEffect } from "react";
import {
  Col,
  Row,
  Image,
  Form,
  Tab,
  Modal,
  Accordion,
  Card,
  Table,
  Button
} from "react-bootstrap";
import Logoadmin from "../../../assets/images/svg/logoadmin.svg";
import SearchIcon from "../../../assets/images/svg/MagnifyingGlass.svg";
import groupDownload from '../../../assets/images/groupdownload.png';
import { studentList, exportStudentList } from "../../../Actions/Admin/Admin";
import Leftbar from "./Leftbar";
import RightBar from "./RightBar";
import Pagination from "react-bootstrap-4-pagination";
import moment from "moment";
import Loader from '../../../Lib/LoaderNew';
import { countryList, stateList, cityList } from '../../../Actions/Student/register';
import { getLocalStorage } from '../../../Lib/Utils';
import CONSTANTS from '../../../Lib/Constants';
import { arrToStr } from '../../../Lib/Utils';

const UserDetail = (props) => {
  const [studentListData, setStudentListData] = useState([]);
  const [totalRecords, setTotalRecords] = useState(0);
  const [totalPages, setTotalPages] = useState(1);
  const limitArr = [10, 20, 30, 40, 50];
  const [loader, setLoader] = useState(false);
  const [countryDatas, setCountryDatas] = useState([]);
  const [stateDatas, setStateDatas] = useState([]);
  const [cityDatas, setCityDatas] = useState([]);
  const [planDatas, setPlanDatas] = useState(['Free', 'Basic', 'Standard', 'Premium']);
  const [referredbydatas, setreferredbydatas] = useState(['Preeti Tanwar',
                                            'Sukanya Bhowmik',
                                             'Kiran Yerawar',
                                              'Salman Shaikh',
                                              'Destiny Ihenacho',
                                              'Kiza Mauridi',
                                              'Archit Singh',
                                              'Vinod Badoni',
                                              'Ajay Sankhla',
                                              'Sreedhar Kodali',
                                              'Fermata',
                                              'Shwan Muhammed'
                                            ]);

  
  const [currentPage, setCurrentPage] = useState(1);
  const [numOfRecord, setnumOfRecord] = useState(10);                                          
  const [searchKey, setSearchKey] = useState("");
  const [country, setCountry] = useState("");
  const [state, setState] = useState("");
  const [city, setCity] = useState("");
  const [plan, setPlan] = useState("");
  const [referredby, setReferredby] = useState(""); 
  const [isProfileCompleted,setisProfileCompleted] = useState("");


  useEffect(() => {
    if(props && props.location && props.location.page_data){
      setCurrentPage(props.location.page_data.currentPage)
      setnumOfRecord(parseInt(props.location.page_data.numOfRecord))
      setSearchKey(props.location.page_data.searchKey)
      setCountry(props.location.page_data.country)
      setState(props.location.page_data.state)
      setCity(props.location.page_data.city)
      setPlan(props.location.page_data.plan)
      setReferredby(props.location.page_data.referredby)
      setisProfileCompleted(props.location.page_data.isProfileCompleted)

      if(props.location.page_data.country){
        var value = props.location.page_data.country
        var id = countryDatas && countryDatas.find((item) => item.name == value);
        if (id) {
          countryInput(id.countryId);
        } else
          countryInput('');
      }
      if(props.location.page_data.state){
        var value = props.location.page_data.state
        var id = stateDatas && stateDatas.find((item) => item.name == value);
        if (id) {
          cityInput(id.stateId);
        } else
          cityInput('');
      }
      manageBackAPI(parseInt(props.location.page_data.numOfRecord), props.location.page_data.currentPage - 1, props.location.page_data.searchKey, props.location.page_data.country, props.location.page_data.state, props.location.page_data.city, props.location.page_data.plan, props.location.page_data.referredby, props.location.page_data.isProfileCompleted);
    }else{
      getStudentList(parseInt(numOfRecord), currentPage - 1, searchKey);
    }
    countryList().then(res => {
      setCountryDatas(res.data.data);
    }).catch(err => { });
  }, []);

  const manageBackAPI = (num_Of_records, curr_page, search = "", country, state, city, plan, referredby, isProfileCompleted) =>{
    setLoader(true)
    studentList(`limit=${num_Of_records}&offset=${curr_page}&search=${search}&country=${country}&state=${state}&city=${city}&plan=${plan}&referredby=${referredby}&isProfileCompleted=${isProfileCompleted}`)
      .then((res) => {
        setLoader(false)
        setStudentListData(res?.data?.data?.rows ? res.data.data.rows : []);
        setTotalRecords(res?.data?.data?.rows ? res.data.data.total : 0);
        if (res.data.data.total <= 10) {
          setTotalPages(1);
        } else {
          if (Number.isInteger(res.data.data.total / num_Of_records)) {
            setTotalPages(parseInt(res.data.data.total / num_Of_records));
          } else {
            setTotalPages(parseInt(res.data.data.total / num_Of_records) + 1);
          }
        }
      })
      .catch((err) => { setLoader(false) });
  }

  const countryInput = (id) => {
    if (id) {
      setStateDatas('');
      setCityDatas('');
      stateList(id).then(res => {
        setStateDatas(res.data.data)
      }).catch(err => { });
    }
  }

  const cityInput = (id) => {
    if (id) {
      setCityDatas('');
      cityList(id).then(res => {
        if (res.data && res.data.data && res.data.data.length) {
          setCityDatas(res.data.data)
        } else {
          setCityDatas('')
        }
      }).catch(err => { });
    }
  }

  const onchangeInput = event => {
    let { target: { name, value } } = event;

    if (name == "country") {
      setCountry(value);
      var id = countryDatas && countryDatas.find((item) => item.name == value);
      if (id) {
        countryInput(id.countryId);
      } else
        countryInput('');
    } else if (name == "state") {
      setState(value);
      var id = stateDatas && stateDatas.find((item) => item.name == value);
      if (id) {
        cityInput(id.stateId);
      } else
        cityInput('');
    } else if (name == "plan") {
      setPlan(value)
    }
    else if (name == "referredby") {
      setReferredby(value)
    } 
    else if (name == "isProfileCompleted") {
      setisProfileCompleted(value)
    } 
    else {
      setCity(value);
    }
  };


  const getStudentList = (num_Of_records, curr_page, search = "", status) => {
    setLoader(true)
    studentList(`limit=${num_Of_records}&offset=${curr_page}&search=${search}&country=${status ? '' : country}&state=${status ? '' : state}&city=${status ? '' : city}&plan=${status ? '' : plan}&referredby=${status ?  '' : referredby}&isProfileCompleted=${status ?  '' : isProfileCompleted}`)
      .then((res) => {
        setLoader(false)
        setStudentListData(res?.data?.data?.rows ? res.data.data.rows : []);
        setTotalRecords(res?.data?.data?.rows ? res.data.data.total : 0);
        if (res.data.data.total <= 10) {
          setTotalPages(1);
        } else {
          if (Number.isInteger(res.data.data.total / num_Of_records)) {
            setTotalPages(parseInt(res.data.data.total / num_Of_records));
          } else {
            setTotalPages(parseInt(res.data.data.total / num_Of_records) + 1);
          }
        }
      })
      .catch((err) => { setLoader(false) });
  };

  let paginationConfig = {
    totalPages: totalPages ? totalPages : 1,
    currentPage: currentPage ? currentPage : 1,
    showMax: totalPages === currentPage ? 2 : 5,
    borderColor: currentPage === 1 || totalPages === currentPage ? "#f8f9fa" : "#604BE8",
    prevText: currentPage === 1 ? "" : "<",
    nextText: totalPages === currentPage ? "" : ">",
    threeDots: true,
    prevNext: true,
    onClick: function (page) {
      window.scrollTo(0, 0);
      setCurrentPage(page)
      getStudentList(numOfRecord, page - 1, searchKey)
    }
  };

  const Gotostudentview = (row) => {
    var params = {
      currentPage,
      numOfRecord,
      searchKey,
      country,
      state,
      city,
      plan,
      referredby,
      isProfileCompleted
    }
    props.history.push({
      pathname: "/admin/studentview",
      state: { userId: row._id },
      page_data: params
    });
  };

  const paginationOnChange = (evnt) => {
    window.scrollTo(0, 0);
    setnumOfRecord(evnt.target.value)
    getStudentList(evnt.target.value, 0, searchKey)
  }

  const SearchResult = () => {
    setCurrentPage(1)
    getStudentList(parseInt(numOfRecord), 0, searchKey);
  };

  const ResetSearch = () => {
    setCurrentPage(1)
    setnumOfRecord(10)
    setSearchKey('')
    setCountry('')
    setState('')
    setCity('')
    setPlan('')
    setReferredby('')
    setisProfileCompleted('')
    getStudentList(10, 0, "", 'refresh');
  }

  const onKeyPress = (e) => {
    if (e.which == 13) {
      getStudentList(parseInt(numOfRecord), 0, searchKey);
    }
  };

  const setSearchKeyOnCHange = (e) => {
    if (!e.target.value) {
      getStudentList(numOfRecord, 0, "");
    }
    setSearchKey(e.target.value);
  };

  const exportStudents = () => {
    let urlex = `${CONSTANTS.SERVER_URL}admin/student/download?token=${getLocalStorage('admin').accessToken}&limit=${numOfRecord}&offset=${currentPage - 1}&search=${searchKey}&country=${country}&state=${state}&city=${city}&plan=${plan}&referredby=${referredby}`;
    window.open(urlex, '_blank');
  }

  return (
    <div className="wizardLayout userDashboard">
      <div className="container-fluid pl-0">
        <Row>
          <Leftbar {...props} />

          <Col sm={9}>
            <div className="userrightBox pr-4">
              <RightBar {...props} />
              <div className="rightUser">
                <div className="filterType1 mt-3">
                  <Accordion>
                    <Card className="shadow4">
                      <Card.Header>
                        <Accordion.Toggle variant="link" eventKey="0">
                          Filters <i className="fa fa-chevron-down arrowA" aria-hidden="true"></i>
                        </Accordion.Toggle>
                      </Card.Header>
                      <Accordion.Collapse eventKey="0">
                        <Card.Body className="bgCol3">
                          <div>
                            <Row>
                              <Col md={6}>
                                <Form.Group controlId="exampleOne" className="mb-4">
                                  <Form.Control
                                    type="text"
                                    placeholder="Search by Full Name / Mobile No. / Email"
                                    name="searchKey"
                                    value={searchKey}
                                    className="inputType1 select2"
                                    autoComplete="none"
                                    onKeyPress={(e) => onKeyPress(e)}
                                    onChange={(e) => setSearchKeyOnCHange(e)}
                                  />
                                  {/* <Image
                                    src={SearchIcon}
                                    alt="Search"
                                    className="pointer searchOne"
                                    onClick={() => SearchResult()}
                                  /> */}
                                </Form.Group>
                              </Col>
                              <Col md={3}>
                                <Form.Group controlId="exampleOne" className="mb-4">
                                  <Form.Control
                                    as="select"
                                    className="selectTyp1 select2 pointer"
                                    name="referredby"
                                    value={referredby}
                                    onChange={onchangeInput}
                                  >
                                    <option value="">Referred By</option>
                                    {
                                      referredbydatas && referredbydatas.length &&
                                      referredbydatas.map((item, index) => {
                                        return <option
                                          key={index}
                                          title={item}
                                          value={item}>
                                          {item}</option>
                                      })
                                    }
                                  </Form.Control>
                                </Form.Group>
                              </Col>
                              <Col md={3}>
                                <Form.Group controlId="exampleOne" className="mb-4">
                                  <Form.Control
                                    as="select"
                                    className="selectTyp1 select2 pointer"
                                    name="country"
                                    value={country}
                                    onChange={onchangeInput}
                                  >
                                    <option value="">Select Country</option>
                                    {
                                      countryDatas && countryDatas.length &&
                                      countryDatas.map((item, index) => {
                                        return <option
                                          key={index}
                                          title={item.name ? item.name : item.text}
                                          value={item.name ? item.name : item.value}>
                                          {item.name ? item.name : item.text}</option>
                                      })
                                    }
                                  </Form.Control>
                                </Form.Group>
                              </Col>
                              <Col md={3}>
                                <Form.Group controlId="exampleOne" className="mb-4">
                                  <Form.Control
                                    as="select"
                                    name="state"
                                    value={state}
                                    onChange={onchangeInput}
                                    className="selectTyp1 select2 pointer">
                                    <option value="">Select State</option>
                                    {
                                      stateDatas && stateDatas.length &&
                                      stateDatas.map((item, index) => {
                                        return <option
                                          key={index}
                                          title={item.name ? item.name : item.text}
                                          value={item.name ? item.name : item.value}>
                                          {item.name ? item.name : item.text}</option>
                                      })
                                    }
                                  </Form.Control>
                                </Form.Group>
                              </Col>
                              <Col md={3}>
                                <Form.Group controlId="exampleOne" className="mb-4">
                                  <Form.Control
                                    as="select"
                                    name="city"
                                    value={city}
                                    onChange={onchangeInput}
                                    className="selectTyp1 select2 pointer"
                                  >
                                    <option value="">Select City</option>
                                    {
                                      cityDatas && cityDatas.length &&
                                      cityDatas.map((item, index) => {
                                        return <option
                                          key={index}
                                          title={item.name ? item.name : item.text}
                                          value={item.name ? item.name : item.value}>
                                          {item.name ? item.name : item.text}</option>
                                      })
                                    }
                                  </Form.Control>
                                </Form.Group>
                              </Col>
                              <Col md={3}>
                                <Form.Group controlId="exampleOne" className="mb-4">
                                  <Form.Control
                                    as="select"
                                    name="plan"
                                    value={plan}
                                    onChange={onchangeInput}
                                    className="selectTyp1 select2 pointer">
                                    <option value="">Select Plan</option>
                                    {
                                      planDatas && planDatas.length &&
                                      planDatas.map((item, index) => {
                                        return <option
                                          key={index}
                                          title={item}
                                          value={item}>
                                          {item}</option>
                                      })
                                    }
                                  </Form.Control>
                                </Form.Group>
                              </Col>
                              <Col md={3}>
                                <Form.Group controlId="exampleOne" className="mb-4">
                                  <Form.Control
                                    as="select"
                                    name="isProfileCompleted"
                                    value={isProfileCompleted}
                                    onChange={onchangeInput}
                                    className="selectTyp1 select2 pointer"
                                  >
                                    <option value="">Is Profile Completed</option>
                                    <option value={false}>Pending</option>
                                    <option value={true}>Completed</option>
                                    {/* {
                                      isProfileCompletedDatas && isProfileCompletedDatas.length &&
                                      isProfileCompletedDatas.map((item, index) => {
                                        return <option
                                          key={index}
                                          title={item}
                                          value={item}>
                                          {item}</option>
                                      })
                                    } */}
                                  </Form.Control>
                                </Form.Group>
                              </Col>

                            </Row>
                            <Row>
                              <Col md={12}>
                                <Button
                                  type="button"
                                  className="btnType3 updateBlue mr-4"
                                  onClick={() => SearchResult()}
                                >
                                  Search
                                </Button>
                                <Button
                                  type="button"
                                  className="btnType3 fillRed"
                                  onClick={() => ResetSearch()}
                                >
                                  Reset
                                </Button>
                              </Col>
                            </Row>
                          </div>
                        </Card.Body>
                      </Accordion.Collapse>
                    </Card>
                  </Accordion>
                </div>
                <div className="text-right mt-2 mb-2">
                  <Image onClick={() => exportStudents()} src={groupDownload} alt="Export Students" title="Export Students" className="pointer" />
                </div>               

              </div>

              <Row  >
                <Col md={12}>
                  {/* start from here  */}
                  <div className="table-responsive pl-2">
                    <Table bordered className="studentTable StudentTableOne br10 mb-4">
                      <thead>
                        <tr>
                          <th>Full Name</th>
                          <th>Mobile Number</th>
                          <th>Email</th>
                          <th>City</th>
                          <th>State</th>
                          <th>Country</th>
                          <th>Referred By</th>
                          <th>Plan</th>
                          <th>Service</th>                          
                          <th>Date of Registration</th>
                          <th>Is Profile Completed</th>
                          <th>Degree to pursue</th>
                          <th>Field of Study</th>
                          {/* <th>Profile Step Completed</th> */}
                          <th>status</th>
                          <th>Updated At</th>
                          <th>Zipcode</th>
                        </tr>
                      </thead>
                      <tbody>
                        {studentListData.length > 0
                          ? studentListData.map((row, rowIndx) => {
                            return (
                              <tr key={rowIndx}>
                                <td>
                                  <a
                                    className="pointer"
                                    onClick={() => Gotostudentview(row)}
                                  >
                                    {row?.firstName
                                      ? `${row.firstName} ${row.lastName}`
                                      : "-"}
                                  </a>
                                </td>
                                {/* <td>{row?.lastName ? row.lastName : "-"}</td> */}
                                <td>
                                  {row?.mobileNumber ? row.mobileNumber : "-"}
                                </td>
                                <td>{row?.email ? row.email : "-"}</td>
                                <td>{row?.city ? row.city : "-"}</td>
                                <td>{row?.state ? row.state : "-"}</td>
                                <td>{row?.country ? row.country : "-"}</td>
                                <td>{row?.referredby ? row.referredby : "-"}</td>
                                <td>
                                  {row?.plan?.name ? row.plan.name : "-"}
                                </td>
                                <td>                               
                                {row.service && row.service.length > 0
                                  ? arrToStr(row.service)
                                  : "-"}
                              </td>
                                <td>
                                  {row?.createdAt
                                    ? moment(new Date(row.createdAt)).format(
                                      "MM/DD/YYYY"
                                    )
                                    : "-"}
                                </td>
                                <td>
                                  {row && row.isProfileCompleted
                                    ? "Completed"
                                    : "Pending"}
                                </td>

                                {/* <td>
                                  {row?.profileStepCompleted
                                    ? row.profileStepCompleted
                                    : "-"}
                                </td> */}
                                <td>{row?.degreePursue ? row.degreePursue : "-"}</td>
                                <td>{row?.fieldOfStudy ? row.fieldOfStudy : "-"}</td>
                                <td>{row?.status ? row.status : "-"}</td>
                                <td>
                                  {row?.updatedAt
                                    ? moment(new Date(row.updatedAt)).format(
                                      "MM/DD/YYYY"
                                    )
                                    : "-"}
                                </td>
                                <td>{row?.zipCode ? row.zipCode : "-"}</td>
                              </tr>
                            );
                          })
                          : []}
                      </tbody>
                    </Table>
                  </div>
                </Col>
              </Row>
              {studentListData && studentListData.length > 0 && totalRecords >= 10 && (
                <div className="d-flex mt-5 mb-5 justify-content-between">
                  <div className="d-flex align-items-center">
                    <div className="mr-2">Show</div>
                    <Form.Control
                      as="select"
                      className="selectTyp1 selectNumber1"
                      value={numOfRecord}
                      onChange={(e) => paginationOnChange(e)}
                    >
                      {limitArr.map((cVal, cIndx) => {
                        return (
                          <option key={cIndx} value={cVal}>
                            {cVal}
                          </option>
                        );
                      })}
                    </Form.Control>
                  </div>
                  {totalRecords > numOfRecord ? (
                    <div className="paginationType4">
                      <Pagination {...paginationConfig} />
                    </div>
                  ) : (
                    ""
                  )}
                </div>
              )}
            </div>
          </Col>
        </Row>
      </div>
      <Modal show={loader} centered className="loaderModal"><Loader /></Modal>
    </div>
  );
};

export default UserDetail;
