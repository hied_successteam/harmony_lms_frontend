import React, { useState, useEffect } from 'react';
import { Col, Row, Container, Image, Form, Button } from 'react-bootstrap';
import Loader from "react-js-loader";
import Logos from '../../assets/images/logos.png';
import { forgotPassword } from '../../../src/Actions/Student/register'
import validateInput from '../../Lib/Validation/forgotpasswordValidation'


const ForgetPassword = (props) => {

  const [email, setEmail] = useState('');
  const [errors, setErrors] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [resultError, setResultError] = useState(false);
  const [message, setMessage] = useState('');

  //go to landing page
  const goToLanding = () => {
    props.history.push({ pathname: '/landing/dashboard' });
  }


  const goToLogin = () => {
    if (props.location && props.location.userType && props.location.userType == 'student') {
      props.history.push({ pathname: '/student/login' });
    } else {
      props.history.push({ pathname: '/college/login' });
    }
  }

  //call login api to get result
  const forgotPasswordApiCall = () => {
    if (isValid()) {
      setIsLoading(true)
      setErrors({})
      forgotPassword({ email, userType: props.location.userType }).then(({ data: { success, data, message } }) => {
        if (success == false) {
          setIsLoading(false)
          setResultError(true)
          setMessage(message)
          setTimeout(function () {
            setResultError(false)
            setMessage('')
          }.bind(this), 4000);
          return;
        } else {
          setIsLoading(false);
          setResultError(false);
          props.history.push({ pathname: '/success', userType: data.userType });
        }
      })
        .catch((err) => {
          setIsLoading(false)
          setResultError(true)
          setMessage(err.response.data.message)
          setTimeout(function () {
            setResultError(false)
            setMessage('')
          }.bind(this), 4000);
          return;
        });
    }
  }

  //check input validation
  const isValid = () => {
    const { errors, isValid } = validateInput(email);
    if (!isValid) {
      setErrors(errors)
    }
    return isValid;
  }

  const onKeyPress = (e) => {
    if(e.which == 13) {
      if (isValid()) {
        forgotPasswordApiCall();
      }
    }
  }


  return (
    <div className="bgBanner d-flex align-items-center justify-content-center w-100">
      <Container>
        <Col lg={10} md={10} className="m-auto">
          <div className="studentBox text-center bgCol3 shadow1 br10 loginUsers">
            <div className="w-100">
              <Image src={Logos} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" onClick={() => goToLanding()} />
              <div className="studentOne mt-4">
                <div className="fs22 fw600 col2 mb-1">Forgot Password</div>
                <div className="fw500 col5 mb-4">Please enter the email address and we’ll send the
                  <br /> reset password link</div>
                {
                  resultError &&
                  <div className="fw500 col5 mb-4 pb-2 text-center" style={{ color: 'red' }}>{message}</div>
                }
                <Form className="formLayoutUi">
                  <Row>
                    <Col md={6} className="m-auto">
                      <Form.Group controlId="formBasicEmail">
                        <Form.Control
                          type="email"
                          placeholder="Email" className="inputType1"
                          value={email}
                          // onKeyPress={(e) => onKeyPress(e)}
                          onChange={(e) => setEmail(e.target.value)} />
                        {
                          errors.email &&
                          <span className="help-block error-text">{errors.email}</span>
                        }
                      </Form.Group>
                    </Col>
                  </Row>
                  <div className="text-center mt-3">
                    <Button className="btnType4 fw600 mb-4" onClick={() => forgotPasswordApiCall()} >
                      {
                        isLoading ?
                          <Loader type="bubble-top" bgColor={"#414180"} size={25} />
                          :
                          "Submit"
                      }
                    </Button>
                    <div className="fw700 col1 pointer pt-1" onClick={() => goToLogin()}>
                      <span className="fw400 col5">Back to</span> Sign In
                    </div>
                  </div>
                </Form>
              </div>
            </div>
          </div>
        </Col>
      </Container>
    </div>
  )
}

export default ForgetPassword;