import React, { useState, useEffect } from 'react';
import { Col, Row, Container, Image, Form, Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import * as qs from 'query-string';
import Logos from '../../assets/images/logos.png';
import { resetPassword } from '../../../src/Actions/Student/register'
import validateInput from '../../Lib/Validation/resetpasswordvalidation'
import Loader from "react-js-loader";

const ResetPassword = (props) => {

  const history = useHistory();

  const [password, setPassword] = useState('');
  const [token, setToken] = useState('');
  const [confPassword, setConfPassword] = useState('');
  const [errors, setErrors] = useState({});
  const [success, setSuccess] = useState(false);
  const [message, setMessage] = useState('');
  const [isLoading, setIsLoading] = useState(false);


  //go to landing page
  const goToLanding = () => {
    props.history.push({ pathname: '/landing/dashboard' });
  }

  useEffect(() => {

    const { location } = props;
    if (location) {
      const parsed = qs.parse(location.search);
      let token = parsed.token.replace(/\s/g, '+');
      setToken(token);
    }

  }, []);

  //submit register data
  const resetSubmit = () => {
    if (isValid()) {
      setErrors({})
      setIsLoading(true)
      let data = {
        token,
        password,
      };

      resetPassword(data).then(({ data: { success, data, message } }) => {
        setIsLoading(false)
        if (success == false) {
          setMessage(message);
          setSuccess(true)
          setTimeout(function () {
            setSuccess(false);
          }.bind(this), 15000);
          return;
        } else {
          props.history.push({
            pathname: '/success',
            state: { fromresetPassword: true },
            userType: data.userType
          });
        }
      })
        .catch((err) => {
          setMessage(err.response.data.message);
          setSuccess(true)
          setTimeout(function () {
            setSuccess(false);
          }.bind(this), 15000);
          return;
        });

    }
  }

  const isValid = () => {
    let data = {
      password,
      confPassword
    };
    const { errors, isValid } = validateInput(data);
    if (!isValid) {
      setErrors(errors)
    }
    return isValid;
  }

  const onKeyPress = (e) => {
    if(e.which == 13) {
      resetSubmit();
    }
  }

  return (
    <div className="bgBanner d-flex align-items-center justify-content-center w-100">
      <Container>
        <Col lg={10} md={10} className="m-auto">
          <div className="studentBox text-center bgCol3 shadow1 br10 loginUsers">
            <div className="w-100">
              <Image src={Logos} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" onClick={() => goToLanding()} />
              <div className="studentOne mt-4">
                <div className="fs22 fw600 col2 mb-1">Reset Password</div>
                <div className="fw500 col5 mb-4">
                  Enter your new password below to change your password
                </div>
                {
                  success &&
                  <div className="fw500 col5 mb-3 text-center mb-4 pb-1" style={{ color: 'red' }}>{message}</div>
                }
                <Form className="formLayoutUi">
                  <Row>
                    <Col md={6}>
                      <Form.Group controlId="formBasicEmailA">
                        <Form.Control
                          type="password"
                          placeholder="New Password"
                          value={password}
                          onChange={(e) => setPassword(e.target.value)}
                          className="inputType1" />
                        {
                          errors.password &&
                          <span className="help-block error-text">
                            <span style={{ color: "red", fontSize: 13 }}>{errors.password}</span>
                          </span>
                        }
                      </Form.Group>
                    </Col>
                    <Col md={6}>
                      <Form.Group controlId="formBasicEmailB" className="mb-5">
                        <Form.Control
                          type="password"
                          value={confPassword}
                          placeholder="Confirm Password"
                          onKeyPress={(e) => onKeyPress(e)}
                          onChange={(e) => setConfPassword(e.target.value)}
                          className="inputType1" />
                        {
                          errors.confPassword &&
                          <span className="help-block error-text">
                            <span style={{ color: "red", fontSize: 13 }}>{errors.confPassword}</span>
                          </span>
                        }
                      </Form.Group>
                    </Col>
                  </Row>

                  <Col md={12}>
                    <div className="text-center align-xs-center">
                      <Button className="btnType4 fw600 mr-4 mb-4" onClick={() => resetSubmit()}>
                        {
                          isLoading ?
                            <Loader type="bubble-top" bgColor={"#414180"} size={25} />
                            :
                            "Reset"
                        }
                      </Button>

                    </div>
                  </Col>



                </Form>
              </div>
            </div>
          </div>
        </Col>
      </Container>
    </div>
  )
}

export default ResetPassword;