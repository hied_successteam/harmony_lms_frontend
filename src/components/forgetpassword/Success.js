import React, { useState, useEffect } from 'react';
import { Col, Row, Container, Image, Form, Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import CheckCircleOne from '../../assets/images/svg/CheckCircle.svg';
import CheckCircleReset from '../../assets/images/svg/UserCirclereset.svg';
const Success = (props) => {
    const history = useHistory();

    const [fromresetPasswordNew, setFromresetPasswordNew] = useState(false);


    useEffect(() => {        
        let fromresetPassword = history &&
            history.location &&
            history.location.state &&
            history.location.state.fromresetPassword
            ? history.location.state.fromresetPassword
            : false;        
        setFromresetPasswordNew(fromresetPassword);
        //setFromresetPasswordNew(true);

    }, []);



    const goToLogin = () => {
        if (props.location && props.location.userType && props.location.userType == 'student') {
            props.history.push({ pathname: '/student/login' });
        } else {
            props.history.push({ pathname: '/college/login' });
        }
    }

    return (
        <div className="bgBanner d-flex align-items-center justify-content-center w-100">
            <Container>
                <Col lg={7} md={7} className="m-auto">
                    <div className="studentBox successBox1 d-flex align-items-center justify-content-center text-center bgCol3 shadow1 br10">
                        <div>
                            <div className="text-center mb-3">
                                <Image
                                    //src={CheckCircleOne}
                                    src={fromresetPasswordNew ? CheckCircleReset : CheckCircleOne}
                                    alt="Icon"
                                    className="" />
                            </div>
                            <div className="fs22 fw600 col2 mb-1">{fromresetPasswordNew ? 'Password Reset successfully.' : 'Mail Sent'}</div>
                            {fromresetPasswordNew ?
                                <div className="fw500 col5 mb-5"></div>
                                :
                                <div className="fw500 col5 mb-4 pb-2">An email with Reset Password link has been sent to your email ID.
                                    <br /> Please check your mailbox.</div>
                            }

                            <div
                                className="fw700 col1 "
                            >
                                <span className="fw400 col5 mr-1">Back to</span>
                                <span className="pointer" onClick={() => goToLogin()}>Sign In</span>
                            </div>
                        </div>
                    </div>
                </Col>
            </Container>
        </div>
    )
}

export default Success;