import React, { useState }  from 'react'; 
import { Col, Row, Container, Image, Form, Dropdown, Accordion, Card, Button, Table, Modal } from 'react-bootstrap';
import Logoadmin from '../../../../assets/images/svg/logoadmin.svg';
import bellIcon from '../../../../assets/images/svg/Bell.svg';
import Settings from '../../../../assets/images/svg/GearSix.svg';
import UserThree from '../../../../assets/images/user3.png';
import BackOne from '../../../../assets/images/svg/backs.svg';
import AccorIcon from '../../../../assets/images/accoricon.png';
import saveOne from '../../../../assets/images/svg/save1.svg';
import Prints from '../../../../assets/images/svg/prints.svg';
import BookmarkSimple from '../../../../assets/images/svg/BookmarkSimple.svg';
import TrashTwo from '../../../../assets/images/svg/Trash2.svg';
import AwardIcon from '../../../../assets/images/awardIcon.png'; 

const Dashboard = () => {
     const [show, setShow] = useState(false);
     const handleClose = () => setShow(false);
     const handleShow = () => setShow(true);  
     return (
          <div className="collegeAdminUi">
               <Container fluid>
                    <div className="pl-2 pr-2 pt-4">  
                         <Row>
                              <Col md={3}>
                                   <Image src={Logoadmin} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" />
                                   <span className="fs20 col24 fw700 ml-1">HiEd Harmony</span>
                              </Col>
                              <Col md={6}></Col>
                              <Col md={3}>
                                   <div className="powerBtn d-flex justify-content-end">
                                        <div className="mr-4">
                                             <Dropdown>
                                                  <Dropdown.Toggle id="dropdown-basic" className="DropdownType1">
                                                       <Image src={bellIcon} alt="Search" className="pointer setingOne mt-1" />
                                                  </Dropdown.Toggle>
                                             </Dropdown>
                                        </div>
                                        <Image src={Settings} alt="Search" className="pointer setingOne mr-5" />
                                        <div>
                                             <Dropdown>  
                                                  <Dropdown.Toggle id="dropdown-basic" className="DropdownType1 col2 fs14 fw600">
                                                       <Image src={UserThree} alt="Search" className="pointer user1 mr-1" />
                                                         Jone Aly   <i className="fa fa-chevron-down ml-1 col8" aria-hidden="true"></i>
                                                  </Dropdown.Toggle>
                                                  <Dropdown.Menu>
                                                       <Dropdown.Item href="#">Profile</Dropdown.Item>
                                                       <Dropdown.Item href="#">Setting</Dropdown.Item>
                                                       <Dropdown.Item href="#">Logout</Dropdown.Item>
                                                  </Dropdown.Menu>
                                             </Dropdown>
                                        </div>
                                   </div>
                              </Col>
                         </Row>
                         <div className="col2 fs20 fw500 mt-4 mb-3">
                              <Image src={BackOne} alt="back" className="pointer mr-2 back1" /> Back
                         </div>
                         <div className="d-flex flex-wrap justify-content-between">
                              <div>
                                   <Dropdown className="dropdownBox1">
                                        <Dropdown.Toggle id="dropdown-basic" className="DropdownType4 col2 fw600 fs20">
                                             Saved Comparison <Image src={AccorIcon} alt="back" className="pointer ml-1" />
                                        </Dropdown.Toggle>
                                        <Dropdown.Menu className="shadow5">
                                             <Dropdown.Item>
                                                  <div className="fs22 fw500 col2">Science Colleges</div>
                                                  <div className="fs14 fw400 col5">Created Jan 05, 2021</div>
                                             </Dropdown.Item>
                                             <Dropdown.Item>
                                                  <div className="fs22 fw500 col2">Best Football Team</div>
                                                  <div className="fs14 fw400 col5">Created Dec 22, 2020</div>
                                             </Dropdown.Item>
                                             <Dropdown.Item>
                                                  <div className="fs22 fw500 col2">Arts College</div>
                                                  <div className="fs14 fw400 col5">Created Oct 10, 2020</div>
                                             </Dropdown.Item>
                                        </Dropdown.Menu>
                                   </Dropdown>  
                              </div>
                              <div className="d-flex">
                                   <div className="col8 fw600 fs20 mr-4 pointer" onClick={handleShow}>Save Comparison</div>
                                   <Image src={saveOne} alt="back" className="pointer save1 mr-2" />
                                   <Image src={Prints} alt="back" className="pointer save1" />
                              </div>
                         </div>
                    </div>  
                    <div className="bgCol4 comparisonType1 mb-4">      
                         <Row>
                              <Col md={2}></Col> 
                              <Col md={10} className="pl-5"> 
                                   <Row> 
                                        <Col md={3}>
                                             <div className="bgCol39 br10 compareBox1"> 
                                                  <div className="p-3 mb-3">
                                                       <div className="w-100">
                                                            <Form.Control as="select" className="selectTyp3 pointer">
                                                                 <option>Kate Wilson</option>
                                                                 <option>College1</option>
                                                                 <option>College2</option>
                                                            </Form.Control>
                                                            <div className="col3 fs15 fw400">USA, TX <span className="ml-3">2 years</span></div>
                                                       </div>
                                                  </div>
                                                  <div className="d-flex align-items-center bgCol1 compareBox2"> 
                                                       <Image src={BookmarkSimple} alt="Book Mark" className="pointer mr-4" />
                                                       <Image src={TrashTwo} alt="Trash" className="pointer" />
                                                  </div>
                                             </div>
                                        </Col>
                                        <Col md={3}>
                                             <div className="bgCol39 br10 compareBox1"> 
                                                  <div className="p-3 mb-3">
                                                       <div className="w-100">
                                                            <Form.Control as="select" className="selectTyp3 pointer">
                                                                 <option>Kate Wilson</option>
                                                                 <option>College1</option>
                                                                 <option>College2</option>
                                                            </Form.Control>
                                                            <div className="col3 fs15 fw400">USA, TX <span className="ml-3">2 years</span></div>
                                                       </div>
                                                  </div>
                                                  <div className="d-flex align-items-center bgCol1 compareBox2"> 
                                                       <Image src={BookmarkSimple} alt="Book Mark" className="pointer mr-4" />
                                                       <Image src={TrashTwo} alt="Trash" className="pointer" />
                                                  </div>
                                             </div>
                                        </Col>
                                        <Col md={3}>
                                             <div className="bgCol39 br10 compareBox1"> 
                                                  <div className="p-3 mb-3">
                                                       <div className="w-100">
                                                            <Form.Control as="select" className="selectTyp3 pointer">
                                                                 <option>Kate Wilson</option>
                                                                 <option>College1</option>
                                                                 <option>College2</option>
                                                            </Form.Control>
                                                            <div className="col3 fs15 fw400">USA, TX <span className="ml-3">2 years</span></div>
                                                       </div>
                                                  </div>
                                                  <div className="d-flex align-items-center bgCol1 compareBox2"> 
                                                       <Image src={BookmarkSimple} alt="Book Mark" className="pointer mr-4" />
                                                       <Image src={TrashTwo} alt="Trash" className="pointer" />
                                                  </div>
                                             </div>
                                        </Col>
                                        <Col md={3}>
                                             <div className="bgCol39 br10 compareBox1"> 
                                                  <div className="p-3 mb-3">
                                                       <div className="w-100">
                                                            <Form.Control as="select" className="selectTyp3 pointer">
                                                                 <option>Kate Wilson</option>
                                                                 <option>College1</option>
                                                                 <option>College2</option>
                                                            </Form.Control>
                                                            <div className="col3 fs15 fw400">USA, TX <span className="ml-3">2 years</span></div>
                                                       </div>
                                                  </div>
                                                  <div className="d-flex align-items-center bgCol1 compareBox2"> 
                                                       <Image src={BookmarkSimple} alt="Book Mark" className="pointer mr-4" />
                                                       <Image src={TrashTwo} alt="Trash" className="pointer" />
                                                  </div>
                                             </div>
                                        </Col>
                                   </Row>
                              </Col>
                         </Row>
                    </div> 
                    
                    <div className="comparisonType5 br10">   
                         <Table striped bordered className="tableType1 br10">
                              <tbody>
                                   <tr>
                                        <td>
                                             <div className="mb-2 fs18 fw600 col2">Scores & Ranks</div>
                                             <div className="fw500 col2 mb-2">SAT</div>
                                             <div className="fw500 col2 mb-2">ACT</div>
                                             <div className="fw500 col2 mb-2">TOEFL</div>
                                        </td> 
                                        <td>
                                             <div className="fw500 col2 mb-2">1250</div>
                                             <div className="fw500 col2 mb-2">25</div>
                                             <div className="fw500 col2 mb-2">75</div>
                                        </td>
                                        <td>
                                             <div className="fw500 col2 mb-2">1200</div>
                                             <div className="fw500 col2 mb-2">24</div>
                                             <div className="fw500 col2 mb-2">81</div>
                                        </td>
                                        <td>
                                             <div className="fw500 col2 mb-2">1200</div>
                                             <div className="fw500 col2 mb-2">24</div>
                                             <div className="fw500 col2 mb-2">81</div>
                                        </td>
                                        <td>
                                             <div className="fw500 col2 mb-2">1200</div>
                                             <div className="fw500 col2 mb-2">24</div>
                                             <div className="fw500 col2 mb-2">81</div>
                                        </td>
                                   </tr>
                                   <tr>
                                        <td><div className="fs18 col2 fw600">Field of Interest</div></td>
                                        <td>
                                             <div className="d-flex flex-wrap mb-4">
                                                  <div className="tagTitle actives text-uppercase fw500 br40 col2 fs14 mr-2 mb-2 pointer">Statistics</div>
                                                  <div className="tagTitle actives text-uppercase fw500 br40 col2 fs14 mr-2 mb-2 pointer">Data Science</div>
                                                  <div className="tagTitle actives text-uppercase fw500 br40 col2 fs14 mr-2 mb-2 pointer">Mathematics</div>
                                                  <div className="tagTitle actives text-uppercase fw500 br40 col2 fs14 mr-2 mb-2 pointer">Management</div>  
                                             </div>
                                        </td>
                                        <td>
                                             <div className="d-flex flex-wrap mb-4">
                                                  <div className="tagTitle actives text-uppercase fw500 br40 col2 fs14 mr-2 mb-2 pointer">Statistics</div>
                                                  <div className="tagTitle actives text-uppercase fw500 br40 col2 fs14 mr-2 mb-2 pointer">Data Science</div>
                                                  <div className="tagTitle actives text-uppercase fw500 br40 col2 fs14 mr-2 mb-2 pointer">Mathematics</div>
                                                  <div className="tagTitle actives text-uppercase fw500 br40 col2 fs14 mr-2 mb-2 pointer">Management</div>  
                                             </div>
                                        </td>
                                        <td>
                                             <div className="d-flex flex-wrap mb-4">
                                                  <div className="tagTitle actives text-uppercase fw500 br40 col2 fs14 mr-2 mb-2 pointer">Statistics</div>
                                                  <div className="tagTitle actives text-uppercase fw500 br40 col2 fs14 mr-2 mb-2 pointer">Data Science</div>
                                                  <div className="tagTitle actives text-uppercase fw500 br40 col2 fs14 mr-2 mb-2 pointer">Mathematics</div>
                                                  <div className="tagTitle actives text-uppercase fw500 br40 col2 fs14 mr-2 mb-2 pointer">Management</div>  
                                             </div>
                                        </td>
                                        <td>
                                             <div className="d-flex flex-wrap mb-4">
                                                  <div className="tagTitle actives text-uppercase fw500 br40 col2 fs14 mr-2 mb-2 pointer">Statistics</div>
                                                  <div className="tagTitle actives text-uppercase fw500 br40 col2 fs14 mr-2 mb-2 pointer">Data Science</div>
                                                  <div className="tagTitle actives text-uppercase fw500 br40 col2 fs14 mr-2 mb-2 pointer">Mathematics</div>
                                                  <div className="tagTitle actives text-uppercase fw500 br40 col2 fs14 mr-2 mb-2 pointer">Management</div>  
                                             </div>
                                        </td>
                                   </tr>
                                   <tr>
                                        <td>
                                             <div className="fs18 col2 fw600">Sports & Curricular Activities</div>
                                        </td>
                                        <td> 
                                             <div className="tagType1 d-flex flex-wrap"> 
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">Basketball</span> 
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">Swimming</span>
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">Community Service
                                                  </span>
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">Tennis</span>         
                                             </div> 
                                        </td>
                                        <td> 
                                             <div className="tagType1 d-flex flex-wrap"> 
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">Basketball</span> 
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">Swimming</span>
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">Community Service
                                                  </span>
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">Tennis</span>         
                                             </div> 
                                        </td>
                                        <td> 
                                             <div className="tagType1 d-flex flex-wrap"> 
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">Basketball</span> 
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">Swimming</span>
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">Community Service
                                                  </span>
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">Tennis</span>         
                                             </div> 
                                        </td>
                                        <td> 
                                             <div className="tagType1 d-flex flex-wrap"> 
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">Basketball</span> 
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">Swimming</span>
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">Community Service
                                                  </span>
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">Tennis</span>         
                                             </div> 
                                        </td>
                                   </tr>
                                   <tr>
                                        <td><div className="fs18 col2 fw600">International Student Clubs Support</div></td>
                                        <td><div className="fs18 col2 fw500">India, China, Malaysia</div></td>
                                        <td><div className="fs18 col2 fw500">Canada, Turkey, France</div></td>
                                        <td><div className="fs18 col2 fw500">Malaysia, Vietnam</div></td>
                                        <td><div className="fs18 col2 fw500">Germany, Turkey</div></td>
                                   </tr>
                                   <tr>
                                        <td><div className="fs18 col2 fw600">College Preference Tags</div></td>
                                        <td> 
                                             <div className="tagType1 d-flex flex-wrap">
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">
                                                       Historically black
                                                  </span>
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">
                                                       Girl Grants
                                                  </span>
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">
                                                       More Chinese
                                                  </span> 
                                             </div>
                                        </td> 
                                        <td> 
                                             <div className="tagType1 d-flex flex-wrap">
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">
                                                       Historically black
                                                  </span>
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">
                                                       Girl Grants
                                                  </span>
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">
                                                       More Chinese
                                                  </span> 
                                             </div>
                                        </td>
                                        <td> 
                                             <div className="tagType1 d-flex flex-wrap">
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">
                                                       Historically black
                                                  </span>
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">
                                                       Girl Grants
                                                  </span>
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">
                                                       More Chinese
                                                  </span> 
                                             </div>
                                        </td>
                                        <td> 
                                             <div className="tagType1 d-flex flex-wrap">
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">
                                                       Historically black
                                                  </span>
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">
                                                       Girl Grants
                                                  </span>
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">
                                                       More Chinese
                                                  </span> 
                                             </div>
                                        </td>
                                   </tr>
                                   <tr>
                                        <td><div className="fs18 col2 fw600">Volunteer Hours</div></td>
                                        <td><div className="fs18 col2 fw500">40 Hours</div></td>
                                        <td><div className="fs18 col2 fw500">55 Hours</div></td>
                                        <td><div className="fs18 col2 fw500">40 Hours</div></td>
                                        <td><div className="fs18 col2 fw500">55 Hours</div></td>
                                   </tr>
                                   <tr>
                                        <td><div className="fs18 col2 fw600">Need-Based Aid</div></td>
                                        <td>  
                                             <div className="awardType1 mb-4">
                                                  <li><Image src={AwardIcon} alt="Icon" className="awardIcons" /> Guardian</li>
                                                  <li><Image src={AwardIcon} alt="Icon" className="awardIcons" /> Student Media Award</li>
                                                  <li><Image src={AwardIcon} alt="Icon" className="awardIcons" /> Morgan Prize</li>
                                             </div>
                                        </td>
                                        <td>  
                                             <div className="awardType1 mb-4">
                                                  <li><Image src={AwardIcon} alt="Icon" className="awardIcons" /> Guardian</li>
                                                  <li><Image src={AwardIcon} alt="Icon" className="awardIcons" /> Student Media Award</li>
                                                  <li><Image src={AwardIcon} alt="Icon" className="awardIcons" /> Morgan Prize</li>
                                             </div>
                                        </td>
                                        <td>  
                                             <div className="awardType1 mb-4">
                                                  <li><Image src={AwardIcon} alt="Icon" className="awardIcons" /> Guardian</li>
                                                  <li><Image src={AwardIcon} alt="Icon" className="awardIcons" /> Student Media Award</li>
                                                  <li><Image src={AwardIcon} alt="Icon" className="awardIcons" /> Morgan Prize</li>
                                             </div>
                                        </td>
                                        <td>  
                                             <div className="awardType1 mb-4">
                                                  <li><Image src={AwardIcon} alt="Icon" className="awardIcons" /> Guardian</li>
                                                  <li><Image src={AwardIcon} alt="Icon" className="awardIcons" /> Student Media Award</li>
                                                  <li><Image src={AwardIcon} alt="Icon" className="awardIcons" /> Morgan Prize</li>
                                             </div>
                                        </td>
                                   </tr>
                                   <tr>
                                        <td><div className="fs18 col2 fw600">Need-Based Aid</div></td>
                                        <td><div className="fs18 col2 fw500">Required</div></td>
                                        <td><div className="fs18 col2 fw500">Not Required</div></td>
                                        <td><div className="fs18 col2 fw500">Required</div></td>
                                        <td><div className="fs18 col2 fw500">Not Required</div></td> 
                                   </tr>

                              </tbody>
                         </Table>
                    </div> 
              
               </Container>
               <Modal show={show} onHide={handleClose} className="modalType1">
                    <Modal.Header closeButton>
                         <Modal.Title className="col8 fw600 fs22 text-center">Save Comparison</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>  
                         <div className="p-3">
                              <div className="fw500 col5 text-center mb-4">
                                   Please enter a name below to save your comparison for future reference
                               </div>
                              <Col md={9} className="m-auto">
                                   <Form.Group controlId="formBasicAdmissions" className="mb-5">
                                        <Form.Control type="text" placeholder="Admission 2021" className="inputType1" />
                                   </Form.Group>
                              </Col>
                              <div className="text-center">
                                   <Button onClick={handleClose} className="btnType2 compareBtn1 mr-4">
                                        Save
                                   </Button>
                                   <Button onClick={handleClose} className="btnType8">
                                        Cancel
                                   </Button>
                              </div>
                         </div>
                    </Modal.Body>
               </Modal>
          </div>
     )
}
export default Dashboard; 

