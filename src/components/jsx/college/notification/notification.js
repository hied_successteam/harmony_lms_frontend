import React from 'react';
import { Col, Row, Container, Image, Form, Dropdown, Accordion, Card, Button } from 'react-bootstrap';
import SearchIcon from '../../../../assets/images/svg/MagnifyingGlass.svg';
import Bookmark from '../../../../assets/images/svg/BookmarkSimple.svg';
import Logoadmin from '../../../../assets/images/svg/logoadmin.svg';
import bellIcon from '../../../../assets/images/svg/Bell.svg';
import Settings from '../../../../assets/images/svg/GearSix.svg';
import UserThree from '../../../../assets/images/user3.png'

const Dashboard = () => {
     return (
          <div className="collegeAdminUi bgCol25"> 
               <Container fluid>  
                    <div className="pl-4 pr-4 pt-4 pb-4">   
                         <Row>
                              <Col md={3}>
                                   <Image src={Logoadmin} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" />
                                   <span className="fs20 col24 fw700 ml-1">HiEd Harmony</span>
                              </Col>
                              <Col md={6}></Col>
                              <Col md={3}>
                                   <div className="powerBtn d-flex justify-content-end">
                                        <div className="mr-4"> 
                                             <Dropdown>
                                                  <Dropdown.Toggle id="dropdown-basic" className="DropdownType1">
                                                       <Image src={bellIcon} alt="Search" className="pointer setingOne mt-1" />
                                                  </Dropdown.Toggle>
                                             </Dropdown>
                                        </div>
                                        <Image src={Settings} alt="Search" className="pointer setingOne mr-5" />
                                        <div>
                                             <Dropdown>
                                                  <Dropdown.Toggle id="dropdown-basic" className="DropdownType1 col2 fs14 fw600">
                                                       <Image src={UserThree} alt="Search" className="pointer user1 mr-1" />
                                                         Jone Aly   <i className="fa fa-chevron-down ml-1 col8" aria-hidden="true"></i>
                                                  </Dropdown.Toggle>
                                                  <Dropdown.Menu>
                                                       <Dropdown.Item href="#">Profile</Dropdown.Item>
                                                       <Dropdown.Item href="#">Setting</Dropdown.Item>
                                                       <Dropdown.Item href="#">Logout</Dropdown.Item>
                                                  </Dropdown.Menu>
                                             </Dropdown>
                                        </div>
                                   </div>
                              </Col>
                         </Row>
                         <div className="col2 fs22 fw600 mt-4 mb-3">Notifications</div>
                         <div className="bgCol32 borderTwo p-3 position-relative br8 mb-3">
                              <div className="col2 fw600 mb-1">General Alert</div>
                              <div className="timeOne col2 fs14 fw500">1:30 PM</div>
                              <div className="col2 fs14 fw400">Texas A&M University <span className="fw600">Admission</span> forms are available now. Visit Admission page for more information</div>
                         </div>
                         <div className="bgCol32 borderTwo p-3 position-relative br8 mb-3"> 
                              <div className="col2 fw600 mb-1">Admission Alert</div>
                              <div className="timeOne col2 fs14 fw500">1:30 PM</div>
                              <div className="col2 fs14 fw400">Texas A&M University admission forms are available now. Visit <span className="fw600">Admission</span> page for more information</div>
                         </div>  
                         <div className="bgCol3 borderTwo p-3 position-relative br8 mb-3"> 
                              <div className="col2 fw600 mb-1">General Alert</div>
                              <div className="timeOne col2 fs14 fw500">1:30 PM</div>
                              <div className="col2 fs14 fw400">UI/UX Designer with a strong design aesthetic who can bring a modern, contemporary and minimalistic style to all our market facing collateral. The ideal candidate would be a person.Please visit the Resources section to view it. 
                              <span className="col8 fw500 ml-1">Learn more</span></div> 
                         </div>
                         <div className="bgCol3 borderTwo p-3 position-relative br8 mb-3"> 
                              <div className="col2 fw600 mb-1">General Alert</div>
                              <div className="timeOne col2 fs14 fw500">1:30 PM</div>
                              <div className="col2 fs14 fw400">UI/UX Designer with a strong design aesthetic who can bring a modern, contemporary and minimalistic style to all our market facing collateral. The ideal candidate would be a person.Please visit the Resources section to view it. 
                              <span className="col8 fw500 ml-1">Learn more</span></div> 
                         </div>
                         <div className="bgCol3 borderTwo p-3 position-relative br8 mb-3"> 
                              <div className="col2 fw600 mb-1">General Alert</div>
                              <div className="timeOne col2 fs14 fw500">1:30 PM</div>
                              <div className="col2 fs14 fw400">UI/UX Designer with a strong design aesthetic who can bring a modern, contemporary and minimalistic style to all our market facing collateral. The ideal candidate would be a person.Please visit the Resources section to view it. 
                              <span className="col8 fw500 ml-1">Learn more</span></div> 
                         </div>
                         <div className="bgCol3 borderTwo p-3 position-relative br8 mb-3"> 
                              <div className="col2 fw600 mb-1">General Alert</div>
                              <div className="timeOne col2 fs14 fw500">1:30 PM</div>
                              <div className="col2 fs14 fw400">UI/UX Designer with a strong design aesthetic who can bring a modern, contemporary and minimalistic style to all our market facing collateral. The ideal candidate would be a person.Please visit the Resources section to view it. 
                              <span className="col8 fw500 ml-1">Learn more</span></div> 
                         </div>
                         <div className="bgCol3 borderTwo p-3 position-relative br8 mb-3"> 
                              <div className="col2 fw600 mb-1">General Alert</div>
                              <div className="timeOne col2 fs14 fw500">1:30 PM</div>
                              <div className="col2 fs14 fw400">UI/UX Designer with a strong design aesthetic who can bring a modern, contemporary and minimalistic style to all our market facing collateral. The ideal candidate would be a person.Please visit the Resources section to view it. 
                              <span className="col8 fw500 ml-1">Learn more</span></div> 
                         </div>
                         <div className="bgCol3 borderTwo p-3 position-relative br8 mb-3"> 
                              <div className="col2 fw600 mb-1">General Alert</div>
                              <div className="timeOne col2 fs14 fw500">1:30 PM</div>
                              <div className="col2 fs14 fw400">UI/UX Designer with a strong design aesthetic who can bring a modern, contemporary and minimalistic style to all our market facing collateral. The ideal candidate would be a person.Please visit the Resources section to view it. 
                              <span className="col8 fw500 ml-1">Learn more</span></div> 
                         </div>
                         
                    </div>
               </Container>
          </div>
     )
}
export default Dashboard; 