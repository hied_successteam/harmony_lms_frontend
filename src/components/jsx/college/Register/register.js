import React from 'react';
import { Col, Row, Container, Image, Form, Button } from 'react-bootstrap';
import Logos from '../../../../assets/images/logos.png'; 
import InputPhoneDropDown from './InputPhoneDropDown';                            

const CollegeRegister = () => {
    return ( 
            <div className="bgBanner d-flex align-items-center justify-content-center w-100">             
                 <Container> 
                      <Col lg={10} md={10} className="m-auto">          
                            <div className="studentBox text-center bgCol3 shadow1 br10 registerUsers">   
                                <Image src={Logos} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" />  
                                <div className="studentOne mt-4 text-left">  
                                     <div className="fs22 fw600 col2 mb-1 text-center">Sign Up</div>
                                     <div className="fw500 col5 mb-4 pb-2 text-center">Create your account</div> 
                                     <Form className="formLayoutUi"> 
                                           <Row> 
                                                  <Col md={6}> 
                                                       <Form.Group controlId="exampleForm.ControlSelect1"> 
                                                       <Form.Control as="select" className="selectTyp1 pointer">
                                                            <option>College Name</option>
                                                            <option>Oriental college bhopal</option>
                                                            <option>RKDF University</option>  
                                                       </Form.Control>
                                                       </Form.Group>
                                                  </Col> 
                                                  <Col md={6}>
                                                       <Form.Group controlId="formBasicEmail">
                                                       <Form.Control type="text" placeholder="User ID" className="inputType1" /> 
                                                       </Form.Group> 
                                                  </Col> 
                                                  <Col md={6}>
                                                       <Form.Group controlId="formBasicEmail">
                                                       <Form.Control type="email" placeholder="Email" className="inputType1" /> 
                                                       </Form.Group>
                                                  </Col> 
                                                  <Col md={6}>
                                                       <Form.Group controlId="formBasicEmail" className="flagSelect">  
                                                       <InputPhoneDropDown />         
                                                       </Form.Group> 
                                                  </Col>  
                                                  <Col md={6}>
                                                       <Form.Group controlId="formBasicEmail">
                                                       <Form.Control type="password" placeholder="Password" className="inputType1" /> 
                                                       </Form.Group>
                                                  </Col> 
                                                  <Col md={6}>
                                                       <Form.Group controlId="formBasicEmail"> 
                                                       <Form.Control type="password" placeholder="Confirm Password" className="inputType1" /> 
                                                       </Form.Group>
                                                  </Col> 
                                                  <Col md={12}>    
                                                       <Form.Group controlId="formBasicCheckbox">  
                                                            <Form.Check type="checkbox" className="checkboxTyp1" label="I agree to the Terms & Conditions" />  
                                                       </Form.Group>  
                                                       <div className="d-md-flex mt-4 align-sm-center align-items-center">   
                                                            <Button className="btnType4 fw600 mr-4 mb-2">Sign Up</Button>
                                                            <div className="fw500 col5">Already have an account?  
                                                            <span className="col1 fw700 pointer ml-2">Sign In</span></div>
                                                       </div>    
                                                  </Col>
                                           </Row>
                                     </Form>
                                </div>
                            </div> 
                      </Col>
                 </Container>
            </div> 
    )
}

export default CollegeRegister;   

