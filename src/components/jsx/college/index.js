import React from "react";
import { Route } from "react-router-dom";

import Login from "./Login/login";
import Register from "./Register/register";
import Dashboard from "./dashboard/dashboard";
import Notification from "./notification/notification";


const CollegeModule = ({ match }) => (
  <div>
    <Route path={`${match.url}/Login`} component={Login} />
    <Route path={`${match.url}/Register`} component={Register} />
    <Route path={`${match.url}/Dashboard`} component={Dashboard} />
    <Route path={`${match.url}/Notification`} component={Notification} />
  </div>
);

export default CollegeModule;
