import React from 'react';
import { Col, Row, Container, Image, Form, Dropdown, Accordion, Card, Button } from 'react-bootstrap';
import Logoadmin from '../../../../assets/images/svg/logoadmin.svg';
import SearchIcon from '../../../../assets/images/svg/MagnifyingGlass.svg';
import bellIcon from '../../../../assets/images/svg/Bell.svg';
import Bookmark from '../../../../assets/images/svg/BookmarkSimple.svg';
import Settings from '../../../../assets/images/svg/GearSix.svg';
import Downloads from '../../../../assets/images/svg/downloads.svg';
import MapPin from '../../../../assets/images/svg/MapPin.svg';
import Hand from '../../../../assets/images/svg/Hand.svg';
import Money from '../../../../assets/images/svg/Money.svg';
import DotsThreeVertical from '../../../../assets/images/svg/DotsThreeVertical.svg';
import UserOne from '../../../../assets/images/teamtop.png';
import UserThree from '../../../../assets/images/user3.png';
import Scale from '../../../../assets/images/Scale.png';
import Scaletwo from '../../../../assets/images/Scaletwo.png';
import { Slider, RangeSlider } from 'rsuite';
import 'rsuite/dist/styles/rsuite-default.css';


const Dashboard = () => {
     return (
          <div className="collegeAdminUi">
               <Container fluid>
                    <Row>
                         <Col md={8} className="topHeaderBox bgCol25 pl-0 pr-0">
                              <div className="pl-4 pr-4 pt-4">
                                   <Row>
                                        <Col md={3}>
                                             <Image src={Logoadmin} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" />
                                             <span className="fs20 col24 fw700 ml-1">HiEd Harmony</span>
                                        </Col>
                                        <Col md={6}>
                                             <div className="searchBox d-flex justify-content-center">
                                                  <Form.Group className="position-relative w-100">
                                                       <Form.Control type="text" placeholder="Search..." className="searchType1 shadow3 w-100" />
                                                       <Image src={SearchIcon} alt="Search" className="pointer searchOne" />
                                                  </Form.Group>
                                             </div>
                                        </Col>
                                        <Col md={3}>
                                             <div className="notifications d-flex justify-content-end">
                                                  <div>
                                                       {/* <Dropdown>
                                                            <Dropdown.Toggle id="dropdown-basic" className="DropdownType1">
                                                                 <Image src={bellIcon} alt="Search" className="pointer" />
                                                                 <span className="fs10 d-inline-block bgCol27 br50 text-center">2</span>
                                                            </Dropdown.Toggle>
                                                            <Dropdown.Menu>
                                                                 <Dropdown.Item href="#">Action</Dropdown.Item>
                                                                 <Dropdown.Item href="#">Another action</Dropdown.Item>
                                                                 <Dropdown.Item href="#">Something else</Dropdown.Item>
                                                            </Dropdown.Menu> 
                                                       </Dropdown> */}
                                                  </div>
                                                  <div className="bookMark ml-4">
                                                       <span className="d-inline-block bgCol1 br50 text-center">
                                                            <Image src={Bookmark} alt="Search" className="pointer" />
                                                       </span>
                                                  </div>
                                             </div>
                                        </Col>
                                   </Row>
                                   <div className="middileAccordian mt-4">
                                        <Accordion>
                                             <Card className="shadow4">
                                                  <Card.Header>
                                                       <Accordion.Toggle variant="link" eventKey="0">
                                                            Filters <span className="ml-4 col8">Reset</span> <i className="fa fa-chevron-down arrowA" aria-hidden="true"></i>
                                                       </Accordion.Toggle>
                                                  </Card.Header>
                                                  <Accordion.Collapse eventKey="0">
                                                       <Card.Body className="bgCol3">
                                                            <div>
                                                                 <Row>
                                                                      <Col md={3}>
                                                                           <Form.Group controlId="exampleOne" className="mb-4">
                                                                                <Form.Control as="select" className="selectTyp1 select2 pointer">
                                                                                     <option>In State</option>
                                                                                     <option>UP</option>
                                                                                     <option>MP</option>
                                                                                     <option>MH</option>
                                                                                     <option>RJ</option>
                                                                                </Form.Control>
                                                                           </Form.Group>
                                                                      </Col>
                                                                      <Col md={3}>
                                                                           <Form.Group controlId="exampleOne" className="mb-4">
                                                                                <Form.Control as="select" className="selectTyp1 select2 pointer">
                                                                                     <option>Term</option>
                                                                                     <option>Fall 2021</option>
                                                                                     <option>Spring 2022</option>
                                                                                     <option>Fall 2022</option>
                                                                                </Form.Control>
                                                                           </Form.Group>
                                                                      </Col>
                                                                      <Col md={3}>
                                                                           <Form.Group controlId="exampleOne" className="mb-4">
                                                                                <Form.Control as="select" className="selectTyp1 select2 pointer">
                                                                                     <option>Bachelors</option>
                                                                                     <option>1</option>
                                                                                     <option>2</option>
                                                                                     <option>3</option>
                                                                                     <option>4</option>
                                                                                </Form.Control>
                                                                           </Form.Group>
                                                                      </Col>
                                                                      <Col md={3}>
                                                                           <Form.Group controlId="exampleOne" className="mb-4">
                                                                                <Form.Control as="select" className="selectTyp1 select2 pointer">
                                                                                     <option>Major</option>
                                                                                     <option>1</option>
                                                                                     <option>2</option>
                                                                                     <option>3</option>
                                                                                     <option>4</option>
                                                                                </Form.Control>
                                                                           </Form.Group>
                                                                      </Col>
                                                                      <Col md={3}>
                                                                           <Form.Group controlId="exampleOne" className="mb-4">
                                                                                <Form.Control as="select" className="selectTyp1 select2 pointer">
                                                                                     <option>Applicant Type</option>
                                                                                     <option>App</option>
                                                                                     <option>App</option>
                                                                                     <option>App</option>
                                                                                     <option>App</option>
                                                                                </Form.Control>
                                                                           </Form.Group>
                                                                      </Col>
                                                                      <Col md={3}>
                                                                           <Form.Group controlId="exampleOne" className="mb-4">
                                                                                <Form.Control as="select" className="selectTyp1 select2 pointer">
                                                                                     <option>Entrance Exams</option>
                                                                                     <option>1</option>
                                                                                     <option>2</option>
                                                                                     <option>3</option>
                                                                                     <option>4</option>
                                                                                </Form.Control>
                                                                           </Form.Group>
                                                                      </Col>
                                                                 </Row>
                                                                 <div className="mt-2 d-flex">
                                                                      <div className="rangeSlider w-100">
                                                                           <Row className="justify-content-center">
                                                                                <Col md={4}>
                                                                                     <Form.Group controlId="formBasicRange">
                                                                                          <Form.Label className="fw600 col2 mb-3">
                                                                                               <span className="mr-3">SAT</span> 400+ to 1300+
                                                                                           </Form.Label>
                                                                                          <RangeSlider defaultValue={[0, 60]} />
                                                                                     </Form.Group>
                                                                                </Col>
                                                                                <Col md={4}>
                                                                                     <Form.Group controlId="formBasicRange">
                                                                                          <Form.Label className="fw600 col2 mb-3">
                                                                                               <span className="mr-3">ACT</span> 22+ to 28+
                                                                                          </Form.Label>
                                                                                          <RangeSlider defaultValue={[0, 60]} />
                                                                                     </Form.Group>
                                                                                </Col>
                                                                           </Row>
                                                                      </div>
                                                                 </div>
                                                            </div>
                                                       </Card.Body>
                                                  </Accordion.Collapse>
                                             </Card>
                                        </Accordion>
                                        <div className="mt-4 pt-2">
                                             <Row>
                                                  <Col md={6}>
                                                       <div className="col2 fs22 fw500">Candidates Results (500)</div>
                                                       <div className="col5 fs14 fw400">Eligible students matching your criteria</div>
                                                  </Col>
                                                  <Col md={6}>
                                                       <div className="d-flex flex-wrap justify-content-end">
                                                            <div className="mr-1 fs14 fw600 col2 pt-1">Sort by:
                                                            </div>
                                                            <div className="mr-3">
                                                                 <Dropdown>
                                                                      <Dropdown.Toggle id="dropdown-basic" className="DropdownType1 col2 fs14 fw400 pt-0 pb-0">
                                                                           Admission Probability <i className="fa fa-chevron-down ml-1 col2" aria-hidden="true"></i>
                                                                      </Dropdown.Toggle>
                                                                      <Dropdown.Menu>
                                                                           <Dropdown.Item href="#">Profile</Dropdown.Item>
                                                                           <Dropdown.Item href="#">Setting</Dropdown.Item>
                                                                           <Dropdown.Item href="#">Logout</Dropdown.Item>
                                                                      </Dropdown.Menu>
                                                                 </Dropdown>
                                                            </div>
                                                            <div className="mr-3">
                                                                 <Image src={Downloads} alt="Icon" className="pointer " />
                                                            </div>
                                                            <div>
                                                                 <Button type="button" className="btnType5 opacityActive">Compare</Button>
                                                            </div>
                                                       </div>
                                                  </Col>
                                             </Row>
                                        </div>
                                        <div className="mt-4">
                                             <Row>
                                                  <Col md={7}>
                                                       <div className="d-flex flex-wrap">
                                                            <Form.Group controlId="formBasicCheckbox" className="formCheckboxs mr-2">
                                                                 <Form.Check type="checkbox" className="pointer checkboxTyp1" label="" />  
                                                            </Form.Group>
                                                            <div>
                                                                 <div className="mb-4">
                                                                      <div className="mb-2 col2 fw600 fs20">Kate Wilson</div>
                                                                      <div className="d-flex flex-wrap">
                                                                           <div className="mr-3 col5 fs14 fw500 d-flex align-items-center">
                                                                                <Image src={MapPin} alt="Icon" className="pointer mr-1" />
                                                                                USA, AL
                                                                           </div>
                                                                           <div className="mr-3 col5 fs14 fw500 d-flex align-items-center">
                                                                                <Image src={Hand} alt="Icon" className="pointer mr-1" />
                                                                                40 Hours
                                                                           </div>
                                                                           <div className="col5 fs14 fw500 d-flex align-items-center">  
                                                                                <Image src={Money} alt="Icon" className="pointer mr-1" />
                                                                                     Need Financing
                                                                           </div>  
                                                                      </div>
                                                                 </div>
                                                                 <div className="col29 fw500 fs14 mb-1">Field of Interest</div>
                                                                 <div className="d-flex flex-wrap mb-4">
                                                                      <div className="tagTitle actives text-uppercase fw500 br40 col2 fs14 mr-2 mb-2 pointer">
                                                                           Statistics
                                                                  </div>
                                                                      <div className="tagTitle text-uppercase fw500 br40 col2 fs14 mr-2 mb-2 pointer">
                                                                           Data science
                                                                     </div>
                                                                      <div className="tagTitle text-uppercase fw500 br40 col2 fs14 mr-2 mb-2 pointer">
                                                                           Programming
                                                                      </div>
                                                                 </div>
                                                                 <div className="d-flex flex-wrap">
                                                                      <div className="mr-3">
                                                                           <Button type="button" className="btnType6">Mark as Favorite</Button>
                                                                      </div>
                                                                      <div className="mr-4">
                                                                           <Button type="button" className="btnType5">View Details</Button>
                                                                      </div>
                                                                      <div>
                                                                           <Dropdown>
                                                                                <Dropdown.Toggle id="dropdown-basic2" className="DropdownType1 col2 fs14 fw600 pt-0 pb-0">
                                                                                     More
                                                                      <i className="fa fa-caret-down col2 ml-1" aria-hidden="true"></i>
                                                                                </Dropdown.Toggle>
                                                                                <Dropdown.Menu>
                                                                                     <Dropdown.Item href="#">Profile</Dropdown.Item>
                                                                                     <Dropdown.Item href="#">Setting</Dropdown.Item>
                                                                                     <Dropdown.Item href="#">Logout</Dropdown.Item>
                                                                                </Dropdown.Menu>
                                                                           </Dropdown>
                                                                      </div>
                                                                 </div>
                                                            </div>
                                                       </div>
                                                  </Col>
                                                  <Col md={5}>
                                                       <div className="d-flex justify-content-between">
                                                            <div className="bgCol34 d-flex w-100 p-2 mb-5">
                                                                 <div className="mr-4">
                                                                      <div className="fs14 col2 fw500">SAT</div>
                                                                      <div className="col2 fw600">1250</div>
                                                                 </div>
                                                                 <div>
                                                                      <div className="fs14 col2 fw500">ACT</div>
                                                                      <div className="col2 fw600">25</div>
                                                                 </div>
                                                            </div>
                                                            <div className="pl-4">
                                                                 <Dropdown>
                                                                      <Dropdown.Toggle id="dropdown-basic2" className="DropdownType1 col2 fs14 fw600 pt-0 pb-0">
                                                                           <Image src={DotsThreeVertical} alt="Bar" className="pointer" />
                                                                      </Dropdown.Toggle>
                                                                      <Dropdown.Menu>
                                                                           <Dropdown.Item href="#">Profile</Dropdown.Item>
                                                                           <Dropdown.Item href="#">Setting</Dropdown.Item>
                                                                           <Dropdown.Item href="#">Logout</Dropdown.Item>
                                                                      </Dropdown.Menu>
                                                                 </Dropdown>
                                                            </div>
                                                       </div>
                                                       <Row>
                                                            <Col md={6}>
                                                                 <Image src={Scale} alt="Icon" className="pointer" />
                                                            </Col>
                                                            <Col md={6}>
                                                                 <Image src={Scaletwo} alt="Icon" className="pointer" />
                                                            </Col>
                                                       </Row>
                                                  </Col>
                                             </Row>
                                             <hr className="borderOne" />
                                        </div> 

                                        <div className="mt-4">
                                             <Row>
                                                  <Col md={7}>
                                                       <div className="d-flex flex-wrap">
                                                            <Form.Group controlId="formBasicCheckboxTwo" className="formCheckboxs mr-2">
                                                                 <Form.Check type="checkbox" className="pointer checkboxTyp1" label="" />  
                                                            </Form.Group>
                                                            <div>
                                                                 <div className="mb-4">
                                                                      <div className="mb-2 col2 fw600 fs20">Kate Wilson</div>
                                                                      <div className="d-flex flex-wrap">
                                                                           <div className="mr-3 col5 fs14 fw500 d-flex align-items-center">
                                                                                <Image src={MapPin} alt="Icon" className="pointer mr-1" />
                                                                                USA, AL
                                                                           </div>
                                                                           <div className="mr-3 col5 fs14 fw500 d-flex align-items-center">
                                                                                <Image src={Hand} alt="Icon" className="pointer mr-1" />
                                                                                40 Hours
                                                                           </div>
                                                                           <div className="col5 fs14 fw500 d-flex align-items-center">
                                                                                <Image src={Money} alt="Icon" className="pointer mr-1" />
                                                                                     Need Financing
                                                                           </div>  
                                                                      </div>
                                                                 </div>
                                                                 <div className="col29 fw500 fs14 mb-1">Field of Interest</div>
                                                                 <div className="d-flex flex-wrap mb-4">
                                                                      <div className="tagTitle actives text-uppercase fw500 br40 col2 fs14 mr-2 mb-2 pointer">
                                                                           Statistics
                                                            </div>
                                                                      <div className="tagTitle text-uppercase fw500 br40 col2 fs14 mr-2 mb-2 pointer">
                                                                           Data science
                                                            </div>
                                                                      <div className="tagTitle text-uppercase fw500 br40 col2 fs14 mr-2 mb-2 pointer">
                                                                           Programming
                                                       </div>
                                                                 </div>
                                                                 <div className="d-flex flex-wrap">
                                                                      <div className="mr-3">
                                                                           <Button type="button" className="btnType6">Mark as Favorite</Button>
                                                                      </div>
                                                                      <div className="mr-4">
                                                                           <Button type="button" className="btnType5">View Details</Button>
                                                                      </div>
                                                                      <div>
                                                                           <Dropdown>
                                                                                <Dropdown.Toggle id="dropdown-basic2" className="DropdownType1 col2 fs14 fw600 pt-0 pb-0">
                                                                                     More
                                                                      <i className="fa fa-caret-down col2 ml-1" aria-hidden="true"></i>
                                                                                </Dropdown.Toggle>
                                                                                <Dropdown.Menu>
                                                                                     <Dropdown.Item href="#">Profile</Dropdown.Item>
                                                                                     <Dropdown.Item href="#">Setting</Dropdown.Item>
                                                                                     <Dropdown.Item href="#">Logout</Dropdown.Item>
                                                                                </Dropdown.Menu>
                                                                           </Dropdown>
                                                                      </div>
                                                                 </div>
                                                            </div>
                                                       </div>
                                                  </Col>
                                                  <Col md={5}>
                                                       <div className="d-flex justify-content-between">
                                                            <div className="bgCol34 d-flex w-100 p-2 mb-5">
                                                                 <div className="mr-4">
                                                                      <div className="fs14 col2 fw500">SAT</div>
                                                                      <div className="col2 fw600">1250</div>
                                                                 </div>
                                                                 <div>
                                                                      <div className="fs14 col2 fw500">ACT</div>
                                                                      <div className="col2 fw600">25</div>
                                                                 </div>
                                                            </div>
                                                            <div className="pl-4">
                                                                 <Dropdown>
                                                                      <Dropdown.Toggle id="dropdown-basic2" className="DropdownType1 col2 fs14 fw600 pt-0 pb-0">
                                                                           <Image src={DotsThreeVertical} alt="Bar" className="pointer" />
                                                                      </Dropdown.Toggle>
                                                                      <Dropdown.Menu>
                                                                           <Dropdown.Item href="#">Profile</Dropdown.Item>
                                                                           <Dropdown.Item href="#">Setting</Dropdown.Item>
                                                                           <Dropdown.Item href="#">Logout</Dropdown.Item>
                                                                      </Dropdown.Menu>
                                                                 </Dropdown>
                                                            </div>
                                                       </div>
                                                       <Row>
                                                            <Col md={6}>
                                                                 <Image src={Scale} alt="Icon" className="pointer" />
                                                            </Col>
                                                            <Col md={6}>
                                                                 <Image src={Scaletwo} alt="Icon" className="pointer" />
                                                            </Col>
                                                       </Row>
                                                  </Col>
                                             </Row>
                                             <hr className="borderOne" />  
                                        </div> 
                                   </div>
                              </div>
                         </Col>
                         <Col md={4} className="topHeaderBox bgCol28 pl-0 pr-0">
                              <div className="rightSidebar pl-4 pr-4 pt-4">
                                   <div className="powerBtn d-flex justify-content-end">
                                        <div className="mr-4">
                                             <Dropdown>
                                                  <Dropdown.Toggle id="dropdown-basic" className="DropdownType1">
                                                       <Image src={bellIcon} alt="Search" className="pointer setingOne mt-1" />
                                                       <span className="fs10 d-inline-block bgCol27 br50 text-center">2</span>
                                                  </Dropdown.Toggle>
                                             </Dropdown>
                                        </div>
                                        <Image src={Settings} alt="Search" className="pointer setingOne mr-5" />
                                        <div>
                                             <Dropdown>
                                                  <Dropdown.Toggle id="dropdown-basic" className="DropdownType1 col2 fs14 fw600">
                                                       <Image src={UserThree} alt="Search" className="pointer user1 mr-1" />
                                                         Jone Aly   <i className="fa fa-chevron-down ml-1 col8" aria-hidden="true"></i>
                                                  </Dropdown.Toggle>
                                                  <Dropdown.Menu>
                                                       <Dropdown.Item href="#">Profile</Dropdown.Item>
                                                       <Dropdown.Item href="#">Setting</Dropdown.Item>
                                                       <Dropdown.Item href="#">Logout</Dropdown.Item>
                                                  </Dropdown.Menu>
                                             </Dropdown>
                                        </div>
                                   </div>
                                   <div className="mt-5 text-center mb-3">
                                        <Image src={UserOne} alt="User" className="br20 mhSet" />
                                   </div>
                                   <div className="text-center mb-4 pb-2">
                                        <div className="col2 fs22 fw600 mb-2">Northwestern Oklahoma State University</div>
                                        <div className="col29 fw500">709 Oklahoma Blvd., Alva, OK 73717, United States</div>
                                   </div>
                                   <div className="boxLayout bgCol30 br4 p-3 mt-3">
                                        <Row>
                                             <Col md={3}>
                                                  <div className="d-flex justify-content-center align-items-center">
                                                       <div className="text-center">
                                                            <div className="col3 fw700 fs24">40%</div>
                                                            <div className="col3 fs12 fw500">Done</div>
                                                       </div>
                                                  </div>
                                             </Col>
                                             <Col md={9}>
                                                  <div className="col3 fw400 fs18">A 100% complete profile gets you free access to the first 50 students*</div>
                                             </Col>
                                        </Row>
                                   </div>
                              </div>
                         </Col>
                    </Row>
               </Container>
          </div>
     )
}
export default Dashboard;  




