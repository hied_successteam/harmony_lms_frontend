import React from 'react';
import { Col, Row, Container, Image, Form, Button } from 'react-bootstrap';  

const ResetLink = () => {  
     return (
          <div className="bgBanner d-flex align-items-center justify-content-center w-100">
               <Container>
                    <Col lg={10} md={10} className="m-auto">  
                         <div className="studentBox d-flex align-items-center justify-content-center text-center bgCol3 shadow1 br10">    
                              <div className="mt-4">
                                   <div className="fs22 fw600 col2 mb-1">Mail Sent</div> 
                                   <div className="fw500 col5 mb-5">Please check your Inbox and click in the recieved 
                                   <br /> link to reset password</div> 
                                   <div className="fw700 col1 pointer">Resend Link</div>  
                              </div>
                         </div>
                    </Col>
               </Container>
          </div>
     )
}

export default ResetLink;   