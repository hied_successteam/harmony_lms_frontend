import React from 'react';
import { Col, Row, Container, Image, Form, Button } from 'react-bootstrap';
import Logos from '../../../../assets/images/logos.png';
import Facebook from '../../../../assets/images/svg/facebook.svg';
import Googles from '../../../../assets/images/svg/googles.svg';
import Linkedin from '../../../../assets/images/svg/linkedin.svg';      

const ResetPassword = () => {
     return (
          <div className="bgBanner d-flex align-items-center justify-content-center w-100">
               <Container>
                    <Col lg={10} md={10} className="m-auto">
                         <div className="studentBox text-center bgCol3 shadow1 br10 loginUsers">
                              <Image src={Logos} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" />
                              <div className="studentOne mt-4">  
                                   <div className="fs22 fw600 col2 mb-1">Reset Password</div>
                                   <div className="fw500 col5 mb-4"> 
                                        Enter your new password below to change your password
                                   </div> 
                                   <Form className="formLayoutUi">  
                                        <Row>
                                             <Col md={6}> 
                                                  <Form.Group controlId="formBasicEmail"> 
                                                       <Form.Control type="password" placeholder="New Password" className="inputType1" />
                                                  </Form.Group>
                                             </Col> 
                                             <Col md={6}>
                                                  <Form.Group controlId="formBasicEmail" className="mb-5"> 
                                                       <Form.Control type="password" placeholder="Confirm Password" className="inputType1" />          
                                                  </Form.Group> 
                                             </Col>
                                        </Row>  
                                        <div className="text-center d-flex align-items-center flex-wrap align-xs-center">   
                                             <Button className="btnType4 fw600 mr-4">Reset</Button> 
                                        </div>      
                                   </Form>
                              </div>
                         </div>
                    </Col>
               </Container>
          </div>
     )
}

export default ResetPassword; 