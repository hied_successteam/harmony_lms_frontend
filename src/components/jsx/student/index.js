import React from "react";
import { Route } from "react-router-dom";

import Register from "./Register/register";
import dashboard from "../dashboard/dashboard";
// import QuestionWizard from "../questionwizard/questionwizard";


const StudentModule = ({ match }) => (
  <div>
    <Route path={`${match.url}/Register`} component={Register} />
    <Route path={`${match.url}/dashboard`} component={dashboard} />
    {/* <Route path={`${match.url}/QuestionWizard`} component={QuestionWizard} /> */}
  </div>
);

export default StudentModule;
