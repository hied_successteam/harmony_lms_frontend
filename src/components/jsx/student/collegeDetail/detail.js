import React from 'react';
import { Col, Row, Container, Image, Form, Tab, Tabs, Dropdown, Accordion, Card, Button } from 'react-bootstrap';
import Logoadmin from '../../../../assets/images/svg/logoadmin.svg';
import SearchIcon from '../../../../assets/images/svg/MagnifyingGlass.svg';
import Bookmark from '../../../../assets/images/svg/BookmarkSimple.svg';
import Stars from '../../../../assets/images/svg/stars.svg';
import StarEmpty from '../../../../assets/images/svg/starempty.svg';
import BellOne from '../../../../assets/images/svg/Bell.svg';
import MapIcon from '../../../../assets/images/svg/MapPin2.svg';
import PhoneIcon from '../../../../assets/images/svg/Phone.svg';
import ProgressGraph1 from '../../../../assets/images/svg/progressOne.svg';
import ProgressGraph2 from '../../../../assets/images/svg/progressTwo.svg';
import ProgressGraph3 from '../../../../assets/images/svg/progressThree.svg';
import ProgressGraph4 from '../../../../assets/images/svg/progressFour.svg';
import Sicon1 from '../../../../assets/images/svg/sicon1.svg';
import Sicon2 from '../../../../assets/images/svg/sicon2.svg';
import Sicon3 from '../../../../assets/images/svg/sicon3.svg';
import Sicon4 from '../../../../assets/images/svg/sicon4.svg';
import Sicon5 from '../../../../assets/images/svg/sicon5.svg';
import Sicon6 from '../../../../assets/images/svg/sicon6.svg';
import Sicon7 from '../../../../assets/images/svg/sicon7.svg';
import Sicon8 from '../../../../assets/images/svg/sicon8.svg';
import ProgressOne from '../../../../assets/images/svg/progress1.svg';
import ProgressRed from '../../../../assets/images/svg/progressRed.svg';
import GraphCost from '../../../../assets/images/svg/graphcost.svg';
import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
import charts from "fusioncharts/fusioncharts.charts";
import ReactFusioncharts from "react-fusioncharts";
import ReactFC from "react-fusioncharts";
import FusionCharts from "fusioncharts";
import Column2D from "fusioncharts/fusioncharts.charts";

ReactFC.fcRoot(FusionCharts, Column2D, FusionTheme);

charts(FusionCharts);
FusionCharts.options.license({
     key: '<YOUR KEY>',
     creditLabel: false,
  });
const dataSources = {                 
     chart: {
          caption: "Yearly Energy Production Rate",
          subcaption: " Top 5 Developed Countries",
          numbersuffix: " TWh",
          showsum: "1",
          plottooltext:
               "$label produces <b>$dataValue</b> of energy from $seriesName",
          theme: "fusion",
          drawcrossline: "1"
     },
     categories: [
          {
               category: [
                    {
                         label: "Canada"
                    },
                    {
                         label: "China"
                    },
                    {
                         label: "Russia"
                    },
                    {
                         label: "Australia"
                    },
                    {
                         label: "United States"
                    },
                    {
                         label: "France"
                    }
               ]
          }
     ],
     dataset: [
          {
               seriesname: "Coal",
               data: [
                    {
                         value: "400"
                    },
                    {
                         value: "830"
                    },
                    {
                         value: "500"
                    },
                    {
                         value: "420"
                    },
                    {
                         value: "790"
                    },
                    {
                         value: "380"
                    }
               ]
          },
          {
               seriesname: "Hydro",
               data: [
                    {
                         value: "350"
                    },
                    {
                         value: "620"
                    },
                    {
                         value: "410"
                    },
                    {
                         value: "370"
                    },
                    {
                         value: "720"
                    },
                    {
                         value: "310"
                    }
               ]
          },
          {
               seriesname: "Nuclear",
               data: [
                    {
                         value: "210"
                    },
                    {
                         value: "400"
                    },
                    {
                         value: "450"
                    },
                    {
                         value: "180"
                    },
                    {
                         value: "570"
                    },
                    {
                         value: "270"
                    }
               ]
          },
          {
               seriesname: "Gas",
               data: [
                    {
                         value: "180"
                    },
                    {
                         value: "330"
                    },
                    {
                         value: "230"
                    },
                    {
                         value: "160"
                    },
                    {
                         value: "440"
                    },
                    {
                         value: "350"
                    }
               ]
          },
          {
               seriesname: "Oil",
               data: [
                    {
                         value: "60"
                    },
                    {
                         value: "200"
                    },
                    {
                         value: "200"
                    },
                    {
                         value: "50"
                    },
                    {
                         value: "230"
                    },
                    {
                         value: "150"
                    }
               ]
          }
     ]
};

const chartData = [
     {
          label: "Venezuela",

          value: "290"
     },
     {
          label: "Saudi",

          value: "260"
     },
     {
          label: "Canada",

          value: "180"
     },
     {
          label: "Iran",

          value: "140"
     },
     {
          label: "Russia",

          value: "115"
     },
     {
          label: "UAE",

          value: "100"
     },
     {
          label: "US",

          value: "30"
     },
     {
          label: "China",

          value: "30"
     }

];

const chartConfigs = {
     type: "column2d", // The chart type
     width: "100%", // Width of the chart
     height: "400", // Height of the chart
     dataFormat: "json", // Data type
     "plotGradientColor": "#ffffff",
     dataSource: {
          // Chart Configuration
          chart: {
               caption: "Countries With Most Oil Reserves [2017-18]", //Set the chart caption
               subCaption: "In MMbbl = One Million barrels", //Set the chart subcaption
               xAxisName: "Country", //Set the x-axis name
               yAxisName: "Reserves (MMbbl)", //Set the y-axis name
               numberSuffix: "K",
               theme: "fusion" //Set the theme for your chart
          },
          // Chart Data - from step 2
          data: chartData
     }
};

const Dashboard = () => {    
     return (
          <div className="collegeAdminUi pb-5">
               <Container fluid>
                    <div className="topHeaderBox bgCol25 pl-0 pr-0">
                         <div className="pl-4 pr-4 pt-4">
                              <Row className="mb-4">
                                   <Col md={3}>
                                        <Image src={Logoadmin} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" />
                                        <span className="fs20 col24 fw700 ml-1">HiEd Harmony</span>
                                   </Col>
                                   <Col md={6}>
                                        <div className="searchBox d-flex justify-content-center">
                                             <Form.Group className="position-relative w-100">
                                                  <Form.Control type="text" placeholder="Search..." className="searchType1 shadow3 w-100" />
                                                  <Image src={SearchIcon} alt="Search" className="pointer searchOne" />
                                             </Form.Group>
                                        </div>
                                   </Col>
                                   <Col md={3}>
                                        <div className="notifications d-flex justify-content-end">
                                             <div className="bookMark ml-4">
                                                  <span className="d-inline-block mr-3 text-center position-relative">
                                                       <Image src={BellOne} alt="Bell" className="bell1 pointer" />
                                                       <span className="bugdeTwo bgCol27 br50 fs10 col3 fw500">2</span>
                                                  </span>
                                                  <span className="d-inline-block bgCol1 br50 text-center">
                                                       <Image src={Bookmark} alt="BookMark" className="pointer" />
                                                  </span>
                                             </div>
                                        </div>
                                   </Col>
                              </Row>
                              <div className="middileAccordian mb-5">
                                   <Row className="mb-4">
                                        <Col md={8}>
                                             <div className="col2 fw600 fs20 mb-1 titleFour">
                                                  Northwestern Oklahoma State University
                                             </div>
                                             <div className="col2 fw600 fs14 mb-2">
                                                  <span className="text-uppercase">Public</span>
                                                  <span className="ml-3">
                                                       <Image src={Stars} alt="star" className="mr-1" />
                                                       <Image src={Stars} alt="star" className="mr-1" />
                                                       <Image src={Stars} alt="star" className="mr-1" />
                                                       <Image src={Stars} alt="star" className="mr-1" />
                                                       <Image src={StarEmpty} alt="star" className="mr-1" />
                                                  </span>
                                                  <span className="ml-3 col5 fw400">(College Ratings as per US News)</span>
                                             </div>
                                             <div className="col2 fw500 mb-2">
                                                  <Image src={MapIcon} alt="Icon" className="mr-1 mapview" /> 709 Oklahoma Blvd., Alva, OK 73717, United States
                                             </div>
                                             <div className="col2 fw500 mb-4">
                                                  <Image src={PhoneIcon} alt="Icon" className="mr-1 mapview" /> +1 580-327-1700
                                             </div>
                                             <div className="col2 fs14 fw600 mb-1 text-uppercase">
                                                  popular tags
                                             </div>
                                             <div className="tagType1 d-flex flex-wrap">
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">Diversity support</span>
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize"> Indian culture club</span>
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">
                                                       HBCU
                                                  </span>
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">Baseball</span>
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">
                                                       online programs
                                                  </span>
                                                  <span className="bgCol35 br8 col4 fw500 mr-2 mb-2 text-capitalize">student centre</span>
                                             </div>
                                        </Col>
                                        <Col md={4}>
                                             <div className="col8 fs22 fw500 pointer mb-1 text-right title5">https://www.nwosu.edu/</div>
                                             <div className="col5 fw500 text-right">Visit website for more details</div>
                                        </Col>
                                   </Row>  
                                   <Row className="mb-5">
                                        <Col md={7}>
                                             <div className="CompareBox4 shadow3">
                                                  <div className="col2 fw400 lineH1 mb-3">
                                                       Located in Alva, a city of approximately 5,000 residents in northwest Oklahoma. Alva is located
                                                       14 miles south of the Kansas border, approximately 115 miles northwest of Oklahoma City, and
                                                       100 miles southwest of Wichita, Kansas
                                                  </div>

                                                  <div className="col2 lineH1 fw400">
                                                       Students can choose from more than 40 areas of study to earn their Bachelor of Arts or
                                                       Bachelor of Science degrees. Northwestern offers master's degree programs in education,
                                                       counselling psychology, and American Studies, and a doctoral program in nursing practice.
                                                  </div>

                                             </div>
                                        </Col>
                                        <Col md={5}>
                                             <div className="text-center CompareBox5 d-flex align-items-center">
                                                  <div>
                                                       <div className="fs22 fw600 col1 mb-2">What’s Remarkable?</div>
                                                       <div className="fw600 col2">
                                                            The community is fully engaged with the University and even dedicates sales tax money to fund scholarships.</div>
                                                  </div>
                                             </div>
                                        </Col>
                                   </Row>
                                   <div className="tabType1 mb-5">
                                        <Tabs defaultActiveKey="home" id="uncontrolled-tab-example" className="mb-3">
                                             <Tab eventKey="home" title="Basic Information">
                                                  <Row className="mb-4">
                                                       <Col md={4}>
                                                            <Image src={ProgressGraph1} alt="Progress" />
                                                       </Col>
                                                       <Col md={4}>
                                                            <Image src={ProgressGraph2} alt="Progress" />
                                                       </Col>
                                                       <Col md={4}>
                                                            <Image src={ProgressGraph4} alt="Progress" />
                                                             
                                                            {/* 
                                                            <ReactFusioncharts
                                                                 type="stackedcolumn2d"
                                                                 width="100%"
                                                                 height="100%"
                                                                 dataFormat="JSON"
                                                                 dataSource={dataSources}
                                                            /> */}      
                                                            
                                                       </Col>  
                                                  </Row>   
                                                  <Row className="mb-4">   
                                                       <Col md={8}>
                                                            {/* <Image src={ProgressGraph3} alt="Progress" /> */}
                                                            {/* Chart Graph */}
                                                            <div className="chartOne">
                                                                 {/* <ReactFusioncharts
                                                                      type="scrollstackedcolumn2d"
                                                                      width="100%"
                                                                      height="100%"
                                                                      dataFormat="JSON"
                                                                      dataSource={chartData}
                                                                 />  */}
                                                                 <ReactFC {...chartConfigs} />
                                                            </div>
                                                       </Col>
                                                       <Col md={4}>
                                                            <div className="tabBox1 mb-4">
                                                                 <Row>
                                                                      <Col md={6}>
                                                                           <div className="fw600 col2 mb-3">Male</div>
                                                                           <div className="fw600 col5 fs24">62.4%</div>
                                                                      </Col>
                                                                      <Col md={6}>
                                                                           <div className="fw600 col2 mb-3">Female</div>
                                                                           <div className="fw600 col5 fs24">37.6%</div>
                                                                      </Col>
                                                                 </Row>
                                                            </div>
                                                            <div className="tabBox1 mb-4">
                                                                 <Row>
                                                                      <Col md={6}>
                                                                           <div className="fw600 col2 mb-3">International</div>
                                                                           <div className="fw600 col5 fs24">37.6%</div>
                                                                      </Col>
                                                                 </Row>
                                                            </div>
                                                       </Col>
                                                  </Row>
                                                  <div className="tabBox2 bgCol3 shadow3">
                                                       <div className="col2 fs18 fw600 mb-2">Admissions Prerequisites</div>
                                                       <div className="col2 fw500 mb-1">First Year Prerequisites</div>
                                                       <div className="col5 fw400 mb-4">Applicants to Northwestern Oklahoma State University are expected to have completed the high school curriculum that will best prepare them for first-year studies at the college level. Please review the admission prerequisites for your major. To find out which college or school hosts your intended major, review our Undergraduate Majors.</div>
                                                       <Row>
                                                            <Col md={3}>
                                                                 <div className="tabBox4 d-flex align-items-center justify-content-center shadow3 br2">
                                                                      <div className="fw500 col2">Arts & Sciences</div>
                                                                 </div>
                                                            </Col>
                                                            <Col md={3}>
                                                                 <div className="tabBox4 d-flex align-items-center justify-content-center shadow3 br2">
                                                                      <div className="fw500 col2">Biomedical Engineering, Science, and Health Systems</div>
                                                                 </div>
                                                            </Col>
                                                            <Col md={3}>
                                                                 <div className="tabBox4 d-flex align-items-center justify-content-center shadow3 br2">
                                                                      <div className="fw500 col2">Business (Except Business and Engineering)</div>
                                                                 </div>
                                                            </Col>
                                                            <Col md={3}>
                                                                 <div className="tabBox4 d-flex align-items-center justify-content-center shadow3 br2">
                                                                      <div className="fw500 col2">Business and Engineering</div>
                                                                 </div>
                                                            </Col>
                                                            <Col md={3}>
                                                                 <div className="tabBox4 d-flex align-items-center justify-content-center shadow3 br2 mb-4">
                                                                      <div className="fw500 col2">Computing and Informatics</div>
                                                                 </div>
                                                            </Col>
                                                            <Col md={3}>
                                                                 <div className="tabBox4 d-flex align-items-center justify-content-center shadow3 br2 mb-4">
                                                                      <div className="fw500 col2">Engineering</div>
                                                                 </div>
                                                            </Col>
                                                            <Col md={3}>
                                                                 <div className="tabBox4 d-flex align-items-center justify-content-center shadow3 br2 mb-4">
                                                                      <div className="fw500 col2">Engineering (Engineering Technology and Construction Management)</div>
                                                                 </div>
                                                            </Col>
                                                            <Col md={3}>
                                                                 <div className="tabBox4 d-flex align-items-center justify-content-center shadow3 br2 mb-4">
                                                                      <div className="fw500 col2">Arts & Sciences</div>
                                                                 </div>
                                                            </Col>
                                                       </Row>
                                                  </div>
                                             </Tab>

                                             <Tab eventKey="profile" title="Advanced Information">
                                                  <div className="summaryBox1">
                                                       <div className="text-uppercase fs18 fw500 col2 summaryBox2 mb-4 bgCol28 d-flex align-items-center">Summary</div>
                                                       <Row className="mb-4">
                                                            <Col md={3}>
                                                                 <div className="summaryBox3 d-flex justify-content-between shadow3 br10">
                                                                      <div>
                                                                           <div className="fw400 col2 mb-2">Rating</div>
                                                                           <div className="fw500 col2 fs22">4.2</div>
                                                                      </div>
                                                                      <div>
                                                                           <Image src={Sicon1} alt="Icon" className="SImg1" />
                                                                      </div>
                                                                 </div>
                                                            </Col>
                                                            <Col md={3}>
                                                                 <div className="summaryBox3 d-flex justify-content-between shadow3 br10">
                                                                      <div>
                                                                           <div className="fw400 col2 mb-2">Acceptance rate</div>
                                                                           <div className="fw500 col2 fs22">
                                                                                <    Image src={ProgressOne} alt="Progress" />
                                                                           </div>
                                                                      </div>
                                                                      <div>
                                                                           <Image src={Sicon2} alt="Icon" className="SImg1" />
                                                                      </div>
                                                                 </div>
                                                            </Col>
                                                            <Col md={3}>
                                                                 <div className="summaryBox3 d-flex justify-content-between shadow3 br10">
                                                                      <div>
                                                                           <div className="fw400 col2 mb-2">Graduation Rate</div>
                                                                           <div className="fw500 col2 fs22">
                                                                                <Image src={ProgressRed} alt="Progress" />
                                                                           </div>
                                                                      </div>
                                                                      <div>
                                                                           <Image src={Sicon3} alt="Icon" className="SImg1" />
                                                                      </div>
                                                                 </div>
                                                            </Col>
                                                            <Col md={3}>
                                                                 <div className="summaryBox3 d-flex justify-content-between shadow3 br10">
                                                                      <div>
                                                                           <div className="fw400 col2 mb-2">Type</div>
                                                                           <div className="fw500 col2 fs22 text-uppercase">PUBLIC</div>
                                                                      </div>
                                                                      <div>
                                                                           <Image src={Sicon4} alt="Icon" className="SImg1" />
                                                                      </div>
                                                                 </div>
                                                            </Col>
                                                            <Col md={3}>
                                                                 <div className="summaryBox3 d-flex justify-content-between shadow3 br10">
                                                                      <div>
                                                                           <div className="fw400 col2 mb-2">Level of Institution</div>
                                                                           <div className="fw500 col2 fs22">4 or More Years</div>
                                                                      </div>
                                                                      <div>
                                                                           <Image src={Sicon5} alt="Icon" className="SImg1" />
                                                                      </div>
                                                                 </div>
                                                            </Col>
                                                            <Col md={3}>
                                                                 <div className="summaryBox3 d-flex justify-content-between shadow3 br10">
                                                                      <div>
                                                                           <div className="fw400 col2 mb-2">Size of Institution</div>
                                                                           <div className="fw500 col2 fs22">4.2</div>
                                                                      </div>
                                                                      <div>
                                                                           <Image src={Sicon6} alt="Icon" className="SImg1" />
                                                                      </div>
                                                                 </div>
                                                            </Col>
                                                            <Col md={3}>
                                                                 <div className="summaryBox3 d-flex justify-content-between shadow3 br10">
                                                                      <div>
                                                                           <div className="fw400 col2 mb-2">Tuition Fee</div>
                                                                           <div className="fw500 col2 fs22">$ 8018</div>
                                                                      </div>
                                                                      <div>
                                                                           <Image src={Sicon7} alt="Icon" className="SImg1" />
                                                                      </div>
                                                                 </div>
                                                            </Col>
                                                            <Col md={3}>
                                                                 <div className="summaryBox3 d-flex justify-content-between shadow3 br10">
                                                                      <div>
                                                                           <div className="fw400 col2 mb-2">
                                                                                Undergraduate Enrollment
                                                                           </div>
                                                                           <div className="fw500 col2 fs22">1849</div>
                                                                      </div>
                                                                      <div>
                                                                           <Image src={Sicon8} alt="Icon" className="SImg1" />
                                                                      </div>
                                                                 </div>
                                                            </Col>
                                                       </Row>

                                                       <Row>
                                                            <Col md={6}> 
                                                                 <div className="summaryBox3 d-flex justify-content-between shadow3 br10">
                                                                      <div>
                                                                           <div className="fw400 col2 mb-2">
                                                                                Undergraduate Enrollment
                                                                           </div> 
                                                                           <div className="fw500 col2 fs22">1849</div>
                                                                      </div>
                                                                      <div>
                                                                           <div className="fw400 col2 mb-2">
                                                                                Undergraduate Enrollment
                                                                           </div>
                                                                           <div className="fw500 col2 fs22">1849</div>
                                                                      </div>
                                                                      <div>
                                                                           <Image src={Sicon8} alt="Icon" className="SImg1" />
                                                                      </div>
                                                                 </div>
                                                            </Col>
                                                       </Row>
                                                       <div className="text-uppercase fs18 fw500 col2 summaryBox2 mb-4 bgCol28 d-flex align-items-center">Statistics</div>
                                                       <Row>
                                                            <Col md={6}>
                                                                 <div className="summaryBox4">
                                                                      <Image src={GraphCost} alt="" />
                                                                 </div>
                                                            </Col>
                                                            <Col md={6}>
                                                                 <div className="summaryBox4">
                                                                      <Image src={GraphCost} alt="" />
                                                                 </div>
                                                            </Col>
                                                       </Row>
                                                  </div>
                                             </Tab>
                                        </Tabs>
                                   </div>
                              </div>
                         </div>
                    </div>
               </Container>
          </div>
     )
}
export default Dashboard;




