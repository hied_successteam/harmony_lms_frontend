import React from 'react';
import { Col, Row, Container, Image, Form, Dropdown, Accordion, Card, Button } from 'react-bootstrap';
import Logoadmin from '../../../../assets/images/svg/logoadmin.svg';
import Heartmark from '../../../../assets/images/svg/hearts.svg';
import userTwo from '../../../../assets/images/user2.png';
import AwardIcon from '../../../../assets/images/awardIcon.png';
import 'rsuite/dist/styles/rsuite-default.css';

const Help = () => {
  return (
    <div className="collegeAdminUi">
      <Container fluid>
        <Row>
          <Col md={8} className="topHeaderBox bgCol3 pl-0 pr-0">
            <div className="pl-4 pr-4 pt-4">
              <Row className="mb-4">
                <Col md={3}>
                  <Image src={Logoadmin} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" />
                  <span className="fs20 col24 fw700 ml-1">HiEd Harmony</span>
                </Col>
                <Col md={3} className="ml-auto">
                  <div className="notifications d-flex justify-content-end">
                    <div className="bookMark ml-4">
                      <span className="d-inline-block bgCol1 br50 text-center">
                        <Image src={Heartmark} alt="BookMark" className="pointer" />
                      </span>
                    </div>
                  </div>
                </Col>
              </Row>

              <div className="middileAccordian">
                <div className="col2 fw500 fs22 mt-5 mb-3">Help</div>
                <div className="col2 fw600 fs22 mb-2">About HiEd Harmony</div>
                <div className="col2 fw400 mb-4">Focused on delivering solutions for higher education and related organizations, HiEd Success was launched in 2017. HiEd Success is a Social Enterprise headquartered in Atlanta, Georgia with an off-shore office in India. We offer technical consulting expertise for Business Process Automation, Reporting and Analytics, Student Information System and Integration. Our strong network of Technology partners and Affiliations combined with a large clientele is a testimony to the highest level of customer success we offer. We also extend our support for Staffing Support Services across industries by giving you access to validated resources to fulfil your IT and project management needs.</div>
                <div className="col2 fw400 mb-1">Please drop your queries at <span className="fw600">info@hiedsuccess.com</span></div>
                <div className="col2 fw400 mb-1">Contact Us at <span className="fw600">+91 78410 45100</span></div>
              </div>

            </div>
          </Col>

          <Col md={4} className="topHeaderBox bgCol28 pl-0 pr-0">
            <div className="rightSidebar pl-4 pr-4 pt-4 mb-5">
              <div className="d-flex justify-content-end mb-3">
                <Row className="w-100">
                  <Col md={3} className="pr-0">
                    <Image src={userTwo} alt="User" />
                  </Col>
                  <Col md={9}>
                    <div className="col2 fw600 fs20 mt-1 mb-1">Kate Wilson</div>
                    <div className="col5 fw500">kate.wilson20@gmail.com</div>
                    <div className="col5 fw500 mb-3">Alva, Oklahoma</div>
                    <div className="fw400 col2 fs14">Course Level: <span className="fw500">Bachelors</span></div>
                  </Col>
                </Row>
                <div>
                  <Dropdown className="DropdownType2">
                    <Dropdown.Toggle id="dropdown-basic" className="DropdownType1 col2 fs14 fw600">
                      <span className="circleType3 d-inline-block br50 text-center">
                        <i className="fa fa-chevron-down col1" aria-hidden="true"></i>
                      </span>
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                      <Dropdown.Item href="#">Edit Profile</Dropdown.Item>
                      <Dropdown.Item href="#">Settings</Dropdown.Item>
                      <Dropdown.Item href="#">Help</Dropdown.Item>
                      <Dropdown.Item href="#">Logout</Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
              </div>

              <div className="text-center mb-4">
                <div className="col2 fs22 fw600 mb-2">Northwestern Oklahoma State University</div>
                <div className="col29 fw500">709 Oklahoma Blvd., Alva, OK 73717, United States</div>
              </div>
              <div className="boxLayout bgCol30 br4 p-3 mt-3 mb-4">
                <Row>
                  <Col md={3}>
                    <div className="d-flex justify-content-center align-items-center mt-2">
                      <div className="text-center">
                        <div className="col3 fw700 fs28">30%</div>
                        <div className="col3 fs14 fw500">Done</div>
                      </div>
                    </div>
                  </Col>
                  <Col md={9}>
                    <div className="col3 fw400 fs18">The more complete your profile, the better we can match you with an institution.</div>
                  </Col>
                </Row>
              </div>
              <div className="col5 fw500 fs14 mb-1">Scores & Ranks</div>
              <div className="d-flex flex-wrap mb-4">
                <div className="mr-5">
                  <div className="col2 fw500">SAT</div>
                  <div className="col2 fw600 fs20">1500</div>
                </div>
                <div className="mr-5">
                  <div className="col2 fw500">ACT</div>
                  <div className="col2 fw600 fs20">75</div>
                </div>
                <div>
                  <div className="col2 fw500">TOEFL</div>
                  <div className="col2 fw600 fs20">80</div>
                </div>
              </div>
              <div className="col5 fw500 fs14 mb-1">Field of Interest</div>
              <div className="d-flex flex-wrap mb-4">
                <div className="tagTitle br20 mr-2 mb-2 col2 fs14 fw500 pointer">Statistics</div>
                <div className="tagTitle br20 mr-2 mb-2 col2 fs14 fw500 pointer">Data Science</div>
                <div className="tagTitle br20 mr-2 mb-2 col2 fs14 fw500 pointer">Programming</div>
              </div>
              <div className="col5 fw500 fs14 mb-1">Awards</div>
              <ul className="awardType1 mb-4">
                <li><Image src={AwardIcon} alt="Icon" className="awardIcons" /> Guardian</li>
                <li><Image src={AwardIcon} alt="Icon" className="awardIcons" /> Student Media Award</li>
                <li><Image src={AwardIcon} alt="Icon" className="awardIcons" /> Morgan Prize</li>
              </ul>
              <div className="col5 fw500 fs14 mb-1">Hobbies</div>
              <div className="d-flex flex-wrap mb-4">
                <div className="col2 fw500">Traveling <span className="ml-1 mr-1">|</span></div>
                <div className="col2 fw500">Swimming <span className="ml-1 mr-1">|</span></div>
                <div className="col2 fw500">Baseball</div>
              </div>
              <Row>
                <Col md={6}>
                  <div className="col5 fw500 fs14 mb-1">Volunteer Hours</div>
                  <div className="col2      fw500">60 hours</div>
                </Col>
                <Col md={6}>
                  <div className="col5 fw500 fs14 mb-1">Current Income</div>
                  <div className="col2 fw500">$45,001 - $60,000 </div>
                </Col>
              </Row>
              <div className="borderType5 mt-3 mb-3"></div>
              <div className="col2 fw600 fs22 mb-2">Notifications</div>
              <div className="notificationType1 bgCol20 br10 mb-3">
                <div className="col2 fw600 mb-1">General Alert</div>
                <div className="col2 fw500 fs14">Webtest #5 is now unlocked. Please visit the
                  <span className="fw600"> Resources</span> section to view it.</div>
                <div className="timeOne col2 fs14 fw500">1:30 PM</div>
              </div>
              <div className="notificationType1 bgCol20 br10 mb-3">
                <div className="col2 fw600 mb-1">Admission Alert</div>
                <div className="col2 fw500 fs14">
                  Texas A&M University admission forms are available now. Visit
                  <span className="fw600"> Admission</span> page for more information
                </div>
                <div className="timeOne col2 fs14 fw500">1:30 PM</div>
              </div>
              <div className="col8 fs18 fw600 text-center pointer">See all</div>
            </div>
          </Col>
        </Row>
      </Container>  
    </div>
  )
}
export default Help;




