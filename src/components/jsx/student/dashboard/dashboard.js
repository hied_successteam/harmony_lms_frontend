import React from 'react';
import { Col, Row, Container, Image, Form, Dropdown, Accordion, Card, Button } from 'react-bootstrap';
import Logoadmin from '../../../../assets/images/svg/logoadmin.svg';
import SearchIcon from '../../../../assets/images/svg/MagnifyingGlass.svg';
import Bookmark from '../../../../assets/images/svg/BookmarkSimple.svg';
import DotsThreeVertical from '../../../../assets/images/svg/DotsThreeVertical.svg';
import Scale from '../../../../assets/images/Scale.png';
import Scaletwo from '../../../../assets/images/Scaletwo.png';
import userTwo from '../../../../assets/images/user2.png';
import AwardIcon from '../../../../assets/images/awardIcon.png';
import BellsTwo from '../../../../assets/images/Bells2.png';
import CrossTwo from '../../../../assets/images/crossOne.png';
import groupDownload from '../../../../assets/images/groupdownload.png';
import BookMarks from '../../../../assets/images/bookmarks.png';
import BrandLogoOne from '../../../../assets/images/brandlogo.png';
import BrandLogoTwo from '../../../../assets/images/brandlogo2.png';
import BrandLogoThree from '../../../../assets/images/brandlogo3.png';
import BrandLogoFour from '../../../../assets/images/brandlogo4.png';
import Stars from '../../../../assets/images/svg/stars.svg';
import StarEmpty from '../../../../assets/images/svg/starempty.svg';
import BellOne from '../../../../assets/images/svg/Bell.svg';
import mapOne from '../../../../assets/images/map1.png';
import { Slider, RangeSlider } from 'rsuite';
import 'rsuite/dist/styles/rsuite-default.css';





const Dashboard = () => {
  const options = {
    animationEnabled: true,
    exportEnabled: true,
    theme: "dark2", // "light1", "dark1", "dark2"
    title: {
      text: "Trip Expenses"
    },
    data: [{
      type: "pie",
      indexLabel: "{label}: {y}%",
      startAngle: -90,
      dataPoints: [
        { y: 20, label: "Airfare" },
        { y: 24, label: "Food & Drinks" },
        { y: 20, label: "Accomodation" },
        { y: 14, label: "Transportation" },
        { y: 12, label: "Activities" },
        { y: 10, label: "Misc" }
      ]
    }]
  }





  return (
    <div className="collegeAdminUi">
      <Container fluid>
        <Row>
          <Col md={8} className="topHeaderBox bgCol3 pl-0 pr-0">
            <div className="pl-4 pr-4 pt-4">
              <Row className="mb-4">
                <Col md={3}>
                  <Image src={Logoadmin} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" />
                  <span className="fs20 col24 fw700 ml-1">HiEd Harmony</span>
                </Col>
                <Col md={6}>
                  <div className="searchBox d-flex justify-content-center">
                    <Form.Group className="position-relative w-100">
                      <Form.Control type="text" placeholder="Search..." className="searchType1 shadow3 w-100" />
                      <Image src={SearchIcon} alt="Search" className="pointer searchOne" />
                    </Form.Group>
                  </div>
                </Col>
                <Col md={3}>
                  <div className="notifications d-flex justify-content-end">
                    <div className="bookMark ml-4">
                      <span className="d-inline-block mr-3 text-center position-relative">
                        <Image src={BellOne} alt="Bell" className="bell1 pointer" />
                        <span className="bugdeTwo bgCol27 br50 fs10 col3 fw500">2</span>
                      </span>
                      <span className="d-inline-block bgCol1 br50 text-center">
                        <Image src={Bookmark} alt="BookMark" className="pointer" />
                      </span>
                    </div>
                  </div>
                </Col>
              </Row>
              <div className="middileAccordian mb-5">
                <Accordion className="mb-4">
                  <Card className="shadow4">
                    <Card.Header>
                      <Accordion.Toggle variant="link" eventKey="0">
                        Filters <i className="fa fa-chevron-down arrowA" aria-hidden="true"></i>
                      </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="0">
                      <Card.Body className="bgCol3">
                        <div>
                          <Row>
                            <Col md={3}>
                              <Form.Group controlId="exampleOne" className="mb-4">
                                <Form.Control as="select" className="selectTyp1 select2 pointer">
                                  <option>In State</option>
                                  <option>UP</option>
                                  <option>MP</option>
                                  <option>MH</option>
                                  <option>RJ</option>
                                </Form.Control>
                              </Form.Group>
                            </Col>
                            <Col md={3}>
                              <Form.Group controlId="exampleOne" className="mb-4">
                                <Form.Control as="select" className="selectTyp1 select2 pointer">
                                  <option>Term</option>
                                  <option>Fall 2021</option>
                                  <option>Spring 2022</option>
                                  <option>Fall 2022</option>
                                </Form.Control>
                              </Form.Group>
                            </Col>
                            <Col md={3}>
                              <Form.Group controlId="exampleOne" className="mb-4">
                                <Form.Control as="select" className="selectTyp1 select2 pointer">
                                  <option>Bachelors</option>
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                </Form.Control>
                              </Form.Group>
                            </Col>
                            <Col md={3}>
                              <Form.Group controlId="exampleOne" className="mb-4">
                                <Form.Control as="select" className="selectTyp1 select2 pointer">
                                  <option>Major</option>
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                </Form.Control>
                              </Form.Group>
                            </Col>
                            <Col md={3}>
                              <Form.Group controlId="exampleOne" className="mb-4">
                                <Form.Control as="select" className="selectTyp1 select2 pointer">
                                  <option>Applicant Type</option>
                                  <option>App</option>
                                  <option>App</option>
                                  <option>App</option>
                                  <option>App</option>
                                </Form.Control>
                              </Form.Group>
                            </Col>
                            <Col md={3}>
                              <Form.Group controlId="exampleOne" className="mb-4">
                                <Form.Control as="select" className="selectTyp1 select2 pointer">
                                  <option>Entrance Exams</option>
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                </Form.Control>
                              </Form.Group>
                            </Col>
                          </Row>
                          <div className="mt-2 d-flex">
                            <div className="rangeSlider w-100">
                              <Row className="justify-content-center">
                                <Col md={4}>
                                  <Form.Group controlId="formBasicRange">
                                    <Form.Label className="fw600 col2 mb-3">
                                      <span className="mr-3">SAT</span> 400+ to 1300+
                                    </Form.Label>
                                    <RangeSlider defaultValue={[0, 60]} />
                                  </Form.Group>
                                </Col>
                                <Col md={4}>
                                  <Form.Group controlId="formBasicRange">
                                    <Form.Label className="fw600 col2 mb-3">
                                      <span className="mr-3">ACT</span> 22+ to 28+
                                    </Form.Label>
                                    <RangeSlider defaultValue={[0, 60]} />
                                  </Form.Group>
                                </Col>
                              </Row>
                            </div>
                          </div>
                        </div>
                      </Card.Body>
                    </Accordion.Collapse>
                  </Card>
                </Accordion>
                <div>
                  <div className="mb-4 bgCol30 p-2 br2">
                    <Row>
                      <Col md={10}>

                        <div className="d-flex flex-wrap">
                          <div className="mr-3">
                            <Image src={BellsTwo} alt="Bell Icon" className="pointer ml-1" />
                          </div>
                          <div>
                            <div className="fw500 col2">Profile Update</div>
                            <div className="fs14 fw400 col2">
                              Update your profile to improve your search!
                              <span className="ml-2 fw700 col2">Update</span>
                            </div>
                          </div>
                        </div>
                      </Col>
                      <Col md={2}>
                        <div className="text-right align-items-center mt-2">
                          <Image src={CrossTwo} alt="Bell Icon" className="pointer mr-3" />
                        </div>
                      </Col>
                    </Row>
                  </div>

                  <div className="mb-4">
                    <Row>
                      <Col md={6}>
                        <div className="col2 fs22 fw500">Suggested Colleges<span className="ml-2 mr-3">(12)</span> <Image src={groupDownload} alt="Icon" className="pointer" /></div>
                        <div className="col5 fs14 fw400">Based on your profile best suitable colleges</div>
                      </Col>
                      <Col md={6}>
                        <div className="d-flex flex-wrap justify-content-end mt-1">
                          <div className="mr-1 fs14 fw600 col2 pt-1">Sort by:
                          </div>
                          <div className="mr-3">
                            <Dropdown className="DropdownType3 mr-4">
                              <Dropdown.Toggle id="dropdown-basic" className="DropdownType1 col2 fs14 fw400 pt-0 pb-0">
                                Admission Probability
                                <i className="fa fa-chevron-down ml-2 col8 fs14" aria-hidden="true"></i>
                              </Dropdown.Toggle>
                              <Dropdown.Menu>
                                <Dropdown.Item>Profile Compatibility</Dropdown.Item>
                                <Dropdown.Item>Location</Dropdown.Item>
                                <Dropdown.Item>College Ranking</Dropdown.Item>
                              </Dropdown.Menu>
                            </Dropdown>
                          </div>
                          <div className="mr-3">
                            <Image src={BookMarks} alt="Icon" className="pointer " />
                          </div>
                          <div>
                            <Button type="button" className="btnType5 opacityActive">Compare</Button>
                          </div>
                        </div>
                      </Col>
                    </Row>
                  </div>
                  <Row>
                    <Col md={8}>
                      <div className="d-flex">
                        <Form.Group controlId="formBasicCheckbox" className="formCheckboxOne">
                          <Form.Check type="checkbox" className="pointer checkboxTyp1" label="" />
                        </Form.Group>
                        <div className="squareBox1 d-flex justify-content-center align-items-center mr-3">
                          <Image src={BrandLogoThree} alt="Brand Logo" />
                        </div>
                        <div>
                          <div className="col2 fw600 fs18 mb-1 titleFour">Northwestern Oklahoma State University</div>
                          <div className="col2 fw600 fs12 mb-1 text-uppercase">
                            <span>Public</span>
                            <span className="ml-3">
                              <Image src={Stars} alt="star" className="mr-1" />
                              <Image src={Stars} alt="star" className="mr-1" />
                              <Image src={Stars} alt="star" className="mr-1" />
                              <Image src={Stars} alt="star" className="mr-1" />
                              <Image src={StarEmpty} alt="star" className="mr-1" />
                            </span>
                          </div>
                          <div className="col5 fs14 fw500 mb-3 titleFour">
                            709 Oklahoma Blvd., Alva, OK 73717, United States
                          </div>
                          <div className="d-flex flex-wrap">
                            <div className="mr-3">
                              <Button type="button" className="btnType6">Mark as Favorite</Button>
                            </div>
                            <div className="mr-4">
                              <Button type="button" className="btnType5">View Details</Button>
                            </div>
                            <div>
                              <Dropdown className="DropdownType2">
                                <Dropdown.Toggle id="dropdown-basic2" className="DropdownType1 mt-1 col2 fs14 fw600 pt-0 pb-0">
                                  More
                                  <i className="fa fa-caret-down col2 ml-1" aria-hidden="true"></i>
                                </Dropdown.Toggle>
                              </Dropdown>
                            </div>
                          </div>
                        </div>
                      </div>
                    </Col>
                    <Col md={4}>
                      <div className="d-flex justify-content-end mb-4">
                        <div className="pl-4">
                          <Dropdown className="DropdownType2">
                            <Dropdown.Toggle id="dropdown-basic2" className="DropdownType1 col2 fs14 fw600 pt-0 pb-0">
                              <Image src={DotsThreeVertical} alt="Bar" className="pointer" />
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                              <Dropdown.Item href="#">Profile</Dropdown.Item>
                              <Dropdown.Item href="#">Setting</Dropdown.Item>
                              <Dropdown.Item href="#">Logout</Dropdown.Item>
                            </Dropdown.Menu>
                          </Dropdown>
                        </div>
                      </div>
                      <Row>
                        <Col md={6}>
                          <Image src={Scale} alt="Icon" className="pointer" />
                        </Col>
                        <Col md={6}>
                          <Image src={Scaletwo} alt="Icon" className="pointer" />
                        </Col>
                      </Row>
                    </Col>
                    <Col md={2}></Col>
                    <Col md={10}>
                      <div className="popularType1">
                        <ul className="d-flex flex-wrap mt-3 mb-3 pl-1 w-100">
                          <li>Accreditation</li>
                          <li>Amenities</li>
                          <li>College Size</li>
                          <li>Location</li>
                          <li>Sports Scholarship</li>
                          <li>Indian Culture Club</li>
                          <li>Admission Ranking</li>
                          <li>Campus</li>
                          <li>Dining</li>
                          <li>Placement</li>
                          <li>Diversity Support</li>
                          <li>Online Programs</li>
                          <li>Affordability</li>
                          <li>Culture</li>
                          <li>Entrance</li>
                          <li>Ranking</li>
                          <li>First Generation Grant</li>
                        </ul>
                        <div className="col2 fs14 fw500 mb-1">Popular Majors</div>
                        <div className="popularType2 d-flex flex-wrap">
                          <div className="text-uppercase d-inline-block mr-2 mb-2 bgCol11 br30 col10 fw500 fs12">Recreation</div>
                          <div className="text-uppercase d-inline-block mr-2 mb-2 bgCol11 br30 col10 fw500 fs12">Registered Nursing</div>
                          <div className="text-uppercase d-inline-block mr-2 mb-2 bgCol11 br30 col10 fw500 fs12">Business Administration</div>
                        </div>
                      </div>
                    </Col>
                  </Row>
                  <hr className="borderOne" />
                  <Row>
                    <Col md={8}>
                      <div className="d-flex">
                        <Form.Group controlId="formBasicCheckbox" className="formCheckboxOne">
                          <Form.Check type="checkbox" className="pointer checkboxTyp1" label="" />
                        </Form.Group>
                        <div className="squareBox1 d-flex justify-content-center align-items-center mr-3">
                          <Image src={BrandLogoTwo} alt="Brand Logo" />
                        </div>
                        <div>
                          <div className="col2 fw600 fs18 mb-1 titleFour">Sul Ross State University</div>
                          <div className="col2 fw600 fs12 mb-1 text-uppercase">
                            <span>Public</span>
                            <span className="ml-3">
                              <Image src={Stars} alt="star" className="mr-1" />
                              <Image src={Stars} alt="star" className="mr-1" />
                              <Image src={Stars} alt="star" className="mr-1" />
                              <Image src={Stars} alt="star" className="mr-1" />
                              <Image src={StarEmpty} alt="star" className="mr-1" />
                            </span>
                          </div>
                          <div className="col5 fs14 fw500 mb-3 titleFour">
                            79830 US-90, Alpine, TX 79830, United States
                          </div>
                          <div className="d-flex flex-wrap">
                            <div className="mr-3">
                              <Button type="button" className="btnType6">Mark as Favorite</Button>
                            </div>
                            <div className="mr-4">
                              <Button type="button" className="btnType5">View Details</Button>
                            </div>
                            <div>
                              <Dropdown className="DropdownType2">
                                <Dropdown.Toggle id="dropdown-basic2" className="DropdownType1 mt-1 col2 fs14 fw600 pt-0 pb-0">
                                  More
                                  <i className="fa fa-caret-down col2 ml-1" aria-hidden="true"></i>
                                </Dropdown.Toggle>
                                <Dropdown.Menu>
                                  <Dropdown.Item href="#">Profile</Dropdown.Item>
                                  <Dropdown.Item href="#">Setting</Dropdown.Item>
                                  <Dropdown.Item href="#">Logout</Dropdown.Item>
                                </Dropdown.Menu>
                              </Dropdown>
                            </div>
                          </div>
                        </div>
                      </div>
                    </Col>
                    <Col md={4}>
                      <div className="d-flex justify-content-end mb-4">
                        <div className="pl-4">
                          <Dropdown className="DropdownType2">
                            <Dropdown.Toggle id="dropdown-basic2" className="DropdownType1 col2 fs14 fw600 pt-0 pb-0">
                              <Image src={DotsThreeVertical} alt="Bar" className="pointer" />
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                              <Dropdown.Item href="#">Profile</Dropdown.Item>
                              <Dropdown.Item href="#">Setting</Dropdown.Item>
                              <Dropdown.Item href="#">Logout</Dropdown.Item>
                            </Dropdown.Menu>
                          </Dropdown>
                        </div>
                      </div>
                      <Row>
                        <Col md={6}>
                          <Image src={Scale} alt="Icon" className="pointer" />
                        </Col>
                        <Col md={6}>
                          <Image src={Scaletwo} alt="Icon" className="pointer" />
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                  <hr className="borderOne" />
                  <Row>
                    <Col md={8}>
                      <div className="d-flex">
                        <Form.Group controlId="formBasicCheckbox" className="formCheckboxOne">
                          <Form.Check type="checkbox" className="pointer checkboxTyp1" label="" />
                        </Form.Group>
                        <div className="squareBox1 d-flex justify-content-center align-items-center mr-3">
                          <Image src={BrandLogoOne} alt="Brand Logo" />
                        </div>
                        <div>
                          <div className="col2 fw600 fs18 mb-1 titleFour">Texas A&M University</div>
                          <div className="col2 fw600 fs12 mb-1 text-uppercase">
                            <span>Public</span>
                            <span className="ml-3">
                              <Image src={Stars} alt="star" className="mr-1" />
                              <Image src={Stars} alt="star" className="mr-1" />
                              <Image src={Stars} alt="star" className="mr-1" />
                              <Image src={Stars} alt="star" className="mr-1" />
                              <Image src={StarEmpty} alt="star" className="mr-1" />
                            </span>
                          </div>
                          <div className="col5 fs14 fw500 mb-3 titleFour">
                            400 Bizzell St, College Station, TX 77843, United States
                          </div>
                          <div className="d-flex flex-wrap">
                            <div className="mr-3">
                              <Button type="button" className="btnType6">Mark as Favorite</Button>
                            </div>
                            <div className="mr-4">
                              <Button type="button" className="btnType5">View Details</Button>
                            </div>
                            <div>
                              <Dropdown className="DropdownType2">
                                <Dropdown.Toggle id="dropdown-basic2" className="DropdownType1 mt-1 col2 fs14 fw600 pt-0 pb-0">
                                  More
                                  <i className="fa fa-caret-down col2 ml-1" aria-hidden="true"></i>
                                </Dropdown.Toggle>
                                <Dropdown.Menu>
                                  <Dropdown.Item href="#">Profile</Dropdown.Item>
                                  <Dropdown.Item href="#">Setting</Dropdown.Item>
                                  <Dropdown.Item href="#">Logout</Dropdown.Item>
                                </Dropdown.Menu>
                              </Dropdown>
                            </div>
                          </div>
                        </div>
                      </div>
                    </Col>
                    <Col md={4}>
                      <div className="d-flex justify-content-end mb-4">
                        <div className="pl-4">
                          <Dropdown className="DropdownType2">
                            <Dropdown.Toggle id="dropdown-basic2" className="DropdownType1 col2 fs14 fw600 pt-0 pb-0">
                              <Image src={DotsThreeVertical} alt="Bar" className="pointer" />
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                              <Dropdown.Item href="#">Profile</Dropdown.Item>
                              <Dropdown.Item href="#">Setting</Dropdown.Item>
                              <Dropdown.Item href="#">Logout</Dropdown.Item>
                            </Dropdown.Menu>
                          </Dropdown>
                        </div>
                      </div>
                      <Row>
                        <Col md={6}>
                          <Image src={Scale} alt="Icon" className="pointer" />
                        </Col>
                        <Col md={6}>
                          <Image src={Scaletwo} alt="Icon" className="pointer" />
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                  <hr className="borderOne" />
                  <Row>
                    <Col md={8}>
                      <div className="d-flex">
                        <Form.Group controlId="formBasicCheckbox" className="formCheckboxOne">
                          <Form.Check type="checkbox" className="pointer checkboxTyp1" label="" />
                        </Form.Group>
                        <div className="squareBox1 d-flex justify-content-center align-items-center mr-3">
                          <Image src={BrandLogoFour} alt="Brand Logo" />
                        </div>
                        <div>
                          <div className="col2 fw600 fs18 mb-1 titleFour">Mississippi University for Women</div>
                          <div className="col2 fw600 fs12 mb-1 text-uppercase">
                            <span>Public</span>
                            <span className="ml-3">
                              <Image src={Stars} alt="star" className="mr-1" />
                              <Image src={Stars} alt="star" className="mr-1" />
                              <Image src={Stars} alt="star" className="mr-1" />
                              <Image src={Stars} alt="star" className="mr-1" />
                              <Image src={StarEmpty} alt="star" className="mr-1" />
                            </span>
                          </div>
                          <div className="col5 fs14 fw500 mb-3 titleFour">
                            709 Oklahoma Blvd., Alva, OK 73717, United States
                          </div>
                          <div className="d-flex flex-wrap">
                            <div className="mr-3">
                              <Button type="button" className="btnType6">Mark as Favorite</Button>
                            </div>
                            <div className="mr-4">
                              <Button type="button" className="btnType5">View Details</Button>
                            </div>
                            <div>
                              <Dropdown className="DropdownType2">
                                <Dropdown.Toggle id="dropdown-basic2" className="DropdownType1 mt-1 col2 fs14 fw600 pt-0 pb-0">
                                  More
                                  <i className="fa fa-caret-down col2 ml-1" aria-hidden="true"></i>
                                </Dropdown.Toggle>
                                <Dropdown.Menu>
                                  <Dropdown.Item href="#">Profile</Dropdown.Item>
                                  <Dropdown.Item href="#">Setting</Dropdown.Item>
                                  <Dropdown.Item href="#">Logout</Dropdown.Item>
                                </Dropdown.Menu>
                              </Dropdown>
                            </div>
                          </div>
                        </div>
                      </div>
                    </Col>
                    <Col md={4}>
                      <div className="d-flex justify-content-end mb-4">
                        <div className="pl-4">
                          <Dropdown className="DropdownType2">
                            <Dropdown.Toggle id="dropdown-basic2" className="DropdownType1 col2 fs14 fw600 pt-0 pb-0">
                              <Image src={DotsThreeVertical} alt="Bar" className="pointer" />
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                              <Dropdown.Item href="#">Profile</Dropdown.Item>
                              <Dropdown.Item href="#">Setting</Dropdown.Item>
                              <Dropdown.Item href="#">Logout</Dropdown.Item>
                            </Dropdown.Menu>
                          </Dropdown>
                        </div>
                      </div>
                      <Row>
                        <Col md={6}>
                          <Image src={Scale} alt="Icon" className="pointer" />
                        </Col>
                        <Col md={6}>
                          <Image src={Scaletwo} alt="Icon" className="pointer" />
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                  <hr className="borderOne" />
                  <Row>
                    <Col md={8}>
                      <div className="d-flex">
                        <Form.Group controlId="formBasicCheckbox" className="formCheckboxOne">
                          <Form.Check type="checkbox" className="pointer checkboxTyp1" label="" />
                        </Form.Group>
                        <div className="squareBox1 d-flex justify-content-center align-items-center mr-3">
                          <Image src={BrandLogoFour} alt="Brand Logo" />
                        </div>
                        <div>
                          <div className="col2 fw600 fs18 mb-1 titleFour">Mississippi University for Women</div>
                          <div className="col2 fw600 fs12 mb-1 text-uppercase">
                            <span>Public</span>
                            <span className="ml-3">
                              <Image src={Stars} alt="star" className="mr-1" />
                              <Image src={Stars} alt="star" className="mr-1" />
                              <Image src={Stars} alt="star" className="mr-1" />
                              <Image src={Stars} alt="star" className="mr-1" />
                              <Image src={StarEmpty} alt="star" className="mr-1" />
                            </span>
                          </div>
                          <div className="col5 fs14 fw500 mb-3 titleFour">
                            709 Oklahoma Blvd., Alva, OK 73717, United States
                          </div>
                          <div className="d-flex flex-wrap">
                            <div className="mr-3">
                              <Button type="button" className="btnType6">Mark as Favorite</Button>
                            </div>
                            <div className="mr-4">
                              <Button type="button" className="btnType5">View Details</Button>
                            </div>
                            <div>
                              <Dropdown className="DropdownType2">
                                <Dropdown.Toggle id="dropdown-basic2" className="DropdownType1 mt-1 col2 fs14 fw600 pt-0 pb-0">
                                  More
                                  <i className="fa fa-caret-down col2 ml-1" aria-hidden="true"></i>
                                </Dropdown.Toggle>
                                <Dropdown.Menu>
                                  <Dropdown.Item href="#">Profile</Dropdown.Item>
                                  <Dropdown.Item href="#">Setting</Dropdown.Item>
                                  <Dropdown.Item href="#">Logout</Dropdown.Item>
                                </Dropdown.Menu>
                              </Dropdown>
                            </div>
                          </div>
                        </div>
                      </div>
                    </Col>
                    <Col md={4}>
                      <div className="d-flex justify-content-end mb-4">
                        <div className="pl-4">
                          <Dropdown className="DropdownType2">
                            <Dropdown.Toggle id="dropdown-basic2" className="DropdownType1 col2 fs14 fw600 pt-0 pb-0">
                              <Image src={DotsThreeVertical} alt="Bar" className="pointer" />
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                              <Dropdown.Item href="#">Profile</Dropdown.Item>
                              <Dropdown.Item href="#">Setting</Dropdown.Item>
                              <Dropdown.Item href="#">Logout</Dropdown.Item>
                            </Dropdown.Menu>
                          </Dropdown>
                        </div>
                      </div>
                      <Row>
                        <Col md={6}>
                          <Image src={Scale} alt="Icon" className="pointer" />
                        </Col>
                        <Col md={6}>
                          <Image src={Scaletwo} alt="Icon" className="pointer" />
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </div>

                <div className="mapType1 d-none">
                  <Image src={mapOne} className="w-100" />
                </div>
              </div>
            </div>
          </Col>

          <Col md={4} className="topHeaderBox bgCol28 pl-0 pr-0">
            <div className="rightSidebar pl-4 pr-4 pt-4 mb-5">
              <div className="d-flex justify-content-end mb-3">
                <Row className="w-100">
                  <Col md={3} className="pr-0">
                    <Image src={userTwo} alt="User" />
                  </Col>
                  <Col md={9}>
                    <div className="col2 fw600 fs20 mt-1 mb-1">Kate Wilson</div>
                    <div className="col5 fw500">kate.wilson20@gmail.com</div>
                    <div className="col5 fw500 mb-3">Alva, Oklahoma</div>
                    <div className="fw400 col2 fs14">Course Level: <span className="fw500">Bachelors</span></div>
                  </Col>
                </Row>
                <div>
                  <Dropdown className="DropdownType2">
                    <Dropdown.Toggle id="dropdown-basic" className="DropdownType1 col2 fs14 fw600">
                      <span className="circleType3 d-inline-block br50 text-center">
                        <i className="fa fa-chevron-down col1" aria-hidden="true"></i>
                      </span>
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                      <Dropdown.Item href="#">Edit Profile</Dropdown.Item>
                      <Dropdown.Item href="#">Settings</Dropdown.Item>
                      <Dropdown.Item href="#">Help</Dropdown.Item>
                      <Dropdown.Item href="#">Logout</Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
              </div>

              <div className="text-center mb-4">
                <div className="col2 fs22 fw600 mb-2">Northwestern Oklahoma State University</div>
                <div className="col29 fw500">709 Oklahoma Blvd., Alva, OK 73717, United States</div>
              </div>
              <div className="boxLayout bgCol30 br4 p-3 mt-3 mb-4">
                <Row>
                  <Col md={3}>
                    <div className="d-flex justify-content-center align-items-center mt-2">
                      <div className="text-center">
                        <div className="col3 fw700 fs28">30%</div>
                        <div className="col3 fs14 fw500">Done</div>
                      </div>
                    </div>
                  </Col>
                  <Col md={9}>
                    <div className="col3 fw400 fs18">The more complete your profile, the better we can match you with an institution.</div>
                  </Col>
                </Row>
              </div>
              <div className="col5 fw500 fs14 mb-1">Scores & Ranks</div>
              <div className="d-flex flex-wrap mb-4">
                <div className="mr-5">
                  <div className="col2 fw500">SAT</div>
                  <div className="col2 fw600 fs20">1500</div>
                </div>
                <div className="mr-5">
                  <div className="col2 fw500">ACT</div>
                  <div className="col2 fw600 fs20">75</div>
                </div>
                <div>
                  <div className="col2 fw500">TOEFL</div>
                  <div className="col2 fw600 fs20">80</div>
                </div>
              </div>
              <div className="col5 fw500 fs14 mb-1">Field of Interest</div>
              <div className="d-flex flex-wrap mb-4">
                <div className="tagTitle br20 mr-2 mb-2 col2 fs14 fw500 pointer">Statistics</div>
                <div className="tagTitle br20 mr-2 mb-2 col2 fs14 fw500 pointer">Data Science</div>
                <div className="tagTitle br20 mr-2 mb-2 col2 fs14 fw500 pointer">Programming</div>
              </div>
              <div className="col5 fw500 fs14 mb-1">Awards</div>
              <ul className="awardType1 mb-4">
                <li><Image src={AwardIcon} alt="Icon" className="awardIcons" /> Guardian</li>
                <li><Image src={AwardIcon} alt="Icon" className="awardIcons" /> Student Media Award</li>
                <li><Image src={AwardIcon} alt="Icon" className="awardIcons" /> Morgan Prize</li>
              </ul>
              <div className="col5 fw500 fs14 mb-1">Hobbies</div>
              <div className="d-flex flex-wrap mb-4">
                <div className="col2 fw500">Traveling <span className="ml-1 mr-1">|</span></div>
                <div className="col2 fw500">Swimming <span className="ml-1 mr-1">|</span></div>
                <div className="col2 fw500">Baseball</div>
              </div>
              <Row>
                <Col md={6}>
                  <div className="col5 fw500 fs14 mb-1">Volunteer Hours</div>
                  <div className="col2      fw500">60 hours</div>
                </Col>
                <Col md={6}>
                  <div className="col5 fw500 fs14 mb-1">Current Income</div>
                  <div className="col2 fw500">$45,001 - $60,000 </div>
                </Col>
              </Row>
              <div className="borderType5 mt-3 mb-3"></div>
              <div className="col2 fw600 fs22 mb-2">Notifications</div>
              <div className="notificationType1 bgCol20 br10 mb-3">
                <div className="col2 fw600 mb-1">General Alert</div>
                <div className="col2 fw500 fs14">Webtest #5 is now unlocked. Please visit the
                  <span className="fw600"> Resources</span> section to view it.</div>
                <div className="timeOne col2 fs14 fw500">1:30 PM</div>
              </div>
              <div className="notificationType1 bgCol20 br10 mb-3">
                <div className="col2 fw600 mb-1">Admission Alert</div>
                <div className="col2 fw500 fs14">
                  Texas A&M University admission forms are available now. Visit
                  <span className="fw600"> Admission</span> page for more information
                </div>
                <div className="timeOne col2 fs14 fw500">1:30 PM</div>
              </div>
              <div className="col8 fs18 fw600 text-center pointer">See all</div>
            </div>
          </Col>
        </Row>



        
     
      </Container>
    </div>
  )
}
export default Dashboard;




