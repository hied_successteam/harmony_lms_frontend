import React from 'react';
import { Col, Row, Container, Image, Button, Form } from 'react-bootstrap'; 
import { Slider, RangeSlider } from 'rsuite';
import 'rsuite/dist/styles/rsuite-default.css'; 

const Student = () => { 
    return (  
            <div className="wizardLayout">   
               <div className="container-fluid pl-0"> 
                    <Row> 
                         <Col md={3}>
                              <div className="sidebarBox d-flex align-items-center justify-content-center text-center">      
                                   <div className="innerBox">  
                                        <div className="listBox pointer pb-5">                   
                                             <div className="text-uppercase col3 fs24 mb-4 fw600 profileTxt">Profile</div> 
                                        </div>
                                        <div className="listBox active pointer pb-5">  
                                             <div className="circleBox text-uppercase d-inline-block col3 fs24 br50 mb-4 fw600">1</div> 
                                             <div className="titles text-uppercase fs22 fw600">Basic details</div>   
                                        </div>
                                        <div className="listBox pointer pb-5">      
                                             <div className="circleBox text-uppercase d-inline-block col3 fs24 br50 mb-4 fw600">2</div> 
                                             <div className="titles text-uppercase fs22 fw600">Advanced Info</div>  
                                        </div>  
                                   </div> 
                              </div>
                         </Col>
                         <Col md={9}> 
                              <div className="rightBox pt-5 pb-5 pl-4">    
                                   <div className="stepTitle fw500 fs30 col19 position-relative">Step 1</div> 
                                   <div className="col7 fw600 fs18 mt-5">1. Please select the level of course you are interested</div> 
                                        <div className="graduation d-flex flex-wrap mt-2"> 
                                             <Form.Check
                                                  type="radio"
                                                  label="Bachelors"
                                                  name="formHorizontalRadios"
                                                  id="formHorizontalRadios1"
                                                  className="active" 
                                             /> 
                                             <Form.Check
                                                  type="radio"
                                                  label="Masters"
                                                  name="formHorizontalRadios"
                                                  id="formHorizontalRadios2"
                                             />
                                             <Form.Check
                                                  type="radio"
                                                  label="PhD"
                                                  name="formHorizontalRadios"
                                                  id="formHorizontalRadios3"
                                             />
                                             <Form.Check
                                                  type="radio"
                                                  label="Certificate"
                                                  name="formHorizontalRadios"
                                                  id="formHorizontalRadios4"
                                             />
                                        </div>
                                   <div className="col7 fw600 fs18 mt-4">
                                        2. Provide some details related to the entrance exams you have opted
                                   </div>  
                                   <div className="examList">   
                                        <Row>
                                             <Col md={12}>
                                                  <div className="col2 fw600 mt-2 mb-1">SAT</div>
                                             </Col>
                                             <Col md={6}>
                                                  <Form.Group controlId="formBasicEmail"> 
                                                  <Form.Control type="text" placeholder="Overall Score" className="inputType1" />
                                                  </Form.Group>
                                             </Col>   
                                             <Col md={6}>
                                                  <Form.Group controlId="formBasicEmail">
                                                  <Form.Control type="text" placeholder="Section Scores" className="inputType1" />
                                                  <Form.Text className="text-muted text-right col6 fs14"> 
                                                       Evidence-based Reading Writing, Math    
                                                  </Form.Text> 
                                                  </Form.Group>
                                             </Col>   
                                             <Col md={12}>
                                                  <div className="col2 fw600 mb-1">ACT</div>
                                             </Col>
                                             <Col md={6}>
                                                  <Form.Group controlId="formBasicEmail"> 
                                                  <Form.Control type="text" placeholder="Overall Score" className="inputType1" />
                                                  </Form.Group>
                                             </Col>   
                                             <Col md={6}>
                                                  <Form.Group controlId="formBasicEmail">
                                                  <Form.Control type="text" placeholder="Sub-test Scores" className="inputType1" />
                                                  <Form.Text className="text-muted text-right col6 fs14"> 
                                                       English, Math, Reading Science
                                                  </Form.Text> 
                                                  </Form.Group>
                                             </Col>   
                                             <Col md={12}>
                                                  <div className="col2 fw600 mb-1">TOEFL</div>
                                             </Col>
                                             <Col md={6}>
                                                  <Form.Group controlId="formBasicEmail"> 
                                                  <Form.Control type="text" placeholder="Overall Score" className="inputType1" />
                                                  </Form.Group>
                                             </Col>   
                                             <Col md={6}>
                                                  <Form.Group controlId="formBasicEmail">
                                                  <Form.Control type="text" placeholder="Sub-test Scores" className="inputType1" />
                                                  <Form.Text className="text-muted text-right col6 fs14"> 
                                                       (Reading, Listening, Speaking, Writing)     
                                                  </Form.Text> 
                                                  </Form.Group>
                                             </Col>
                                             <Col md={12}>
                                                  <Button className="btnType1 fs14 fw600 mb-4">
                                                       <span className="circles d-inline-block br50 fs12 mr-2 text-center"><i className="fa fa-plus" aria-hidden="true"></i></span> 
                                                       Add More 
                                                   </Button>
                                             </Col>
                                             <Col md={6}>
                                                  <div className="col7 fw600 fs18 mt-3 mb-2">
                                                       3. Please select your preferred range of tuition fees 
                                                  </div>     
                                                  <div className="rangeSlider">
                                                       <Form.Group controlId="formBasicRange">
                                                            <Form.Label className="fw600 col2 mb-3">$0 - $35,000
                                                            </Form.Label>
                                                            <RangeSlider defaultValue={[0, 60]} />  
                                                       </Form.Group>
                                                  </div> 
                                             </Col> 
                                             <Col md={6}></Col>
                                             <Col md={6}>
                                                  <div className="col7 fw600 fs18 mt-4 mb-2">
                                                       4. State you are looking for college options
                                                  </div> 
                                                  <Form.Group controlId="exampleForm.ControlSelect1"> 
                                                  <Form.Control as="select" className="selectTyp1 pointer">
                                                       <option>Texas, California</option>
                                                       <option>MP</option>
                                                       <option>MH</option>
                                                       <option>RJ</option>
                                                       <option>UP</option>
                                                  </Form.Control>
                                                  </Form.Group> 
                                             </Col>
                                             <Col md={6}></Col>
                                             <Col md={6}>
                                                  <div className="col7 fw600 fs18 mt-4 mb-2">   
                                                       5. Please select the major you are looking for
                                                  </div> 
                                                  <Form.Group controlId="exampleForm.ControlSelect1"> 
                                                  <Form.Control as="select" className="selectTyp1 pointer">
                                                       <option>Engineering, Mathematics</option>
                                                       <option>Science</option>
                                                       <option>Economics</option>
                                                       <option>English</option>
                                                       <option>Hindi</option>
                                                  </Form.Control>
                                                  </Form.Group> 
                                             </Col>
                                             <Col md={6}></Col> 
                                             <Col md={6}>
                                                  <div className="col7 fw600 fs18 mt-4 mb-2">   
                                                       6. Will you be in need of any financial aids  
                                                  </div>
                                                  <div className="graduation boxOne d-flex flex-wrap mt-2">
                                                       <Form.Check
                                                            type="radio"
                                                            label="Yes"
                                                            name="formHorizontalRadios"
                                                            id="formHorizontalRadios5"
                                                            className="active" 
                                                       /> 
                                                       <Form.Check
                                                            type="radio"
                                                            label="No"
                                                            name="formHorizontalRadios"
                                                            id="formHorizontalRadios6"
                                                       />
                                                  </div>
                                             </Col>
                                             <Col md={6}></Col> 
                                        </Row>
                                        <div className="hrBorder mt-5 pt-3"></div>
                                        <div className="text-right mt-3"> 
                                             <Button type="button" className="btnType2">
                                                  Save & Next 
                                             </Button> 
                                        </div>
                                   </div>
                              </div>
                         </Col>
                    </Row>
               </div>
            </div>  
    )
}

export default Student; 


