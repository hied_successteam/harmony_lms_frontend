import React from 'react';
import { Col, Row, Container, Image, Form, Button } from 'react-bootstrap';
import Logos from '../../../../assets/images/logos.png';     
import IntlTelInput from 'react-intl-tel-input';
import 'react-intl-tel-input/dist/main.css';

const StudentRegister = () => {      
    return ( 
            <div className="bgBanner d-flex align-items-center justify-content-center w-100">                 
                 <Container>
                      <Col lg={10} md={10} className="m-auto">      
                            <div className="studentBox text-center bgCol3 shadow1 br10 registerUsers">   
                                <Image src={Logos} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" />  
                                <div className="studentOne mt-4 text-left"> 
                                     <div className="fs22 fw600 col2 mb-1 text-center">Sign Up</div>
                                     <div className="fw500 col5 mb-3 text-center mb-4 pb-1">Create your account</div>
                                     <Form className="formLayoutUi">  
                                           <Row> 
                                                  <Col md={6}>
                                                       <Form.Group controlId="formBasicEmail"> 
                                                       <Form.Control type="text" placeholder="First Name" className="inputType1" />
                                                       </Form.Group>
                                                  </Col>   
                                                  <Col md={6}>
                                                       <Form.Group controlId="formBasicEmail">
                                                       <Form.Control type="text" placeholder="Last Name" className="inputType1" /> 
                                                       </Form.Group>
                                                  </Col> 
                                                  <Col md={6}>
                                                       <Form.Group controlId="formBasicEmail">
                                                       <Form.Control type="email" placeholder="Email" className="inputType1" /> 
                                                       </Form.Group>
                                                  </Col> 
                                                  <Col md={6}>
                                                       <Form.Group controlId="formBasicEmail" className="mb-4 flagSelect"> 
                                                       <IntlTelInput
                                                            containerClassName="intl-tel-input"
                                                            inputClassName="form-control inputType1"
                                                            placeholder="Contact Number"  
                                                       />     
                                                       </Form.Group>   
                                                  </Col> 
                                                  <Col md={4}>
                                                       <Form.Group controlId="exampleForm.ControlSelect1"> 
                                                       <Form.Control as="select" className="selectTyp1 pointer">
                                                            <option>Country</option>
                                                            <option>India</option>
                                                            <option>Australia</option>
                                                            <option>England</option>
                                                            <option>Russia</option>
                                                       </Form.Control>
                                                       </Form.Group> 
                                                  </Col>  
                                                  <Col md={4}> 
                                                       <Form.Group controlId="exampleForm.ControlSelect1"> 
                                                       <Form.Control as="select" className="selectTyp1 pointer">
                                                            <option>State</option>
                                                            <option>MP</option>
                                                            <option>MH</option>
                                                            <option>RJ</option>
                                                            <option>UP</option>
                                                       </Form.Control>
                                                       </Form.Group>  
                                                  </Col>  
                                                  <Col md={4}>    
                                                       <Form.Group controlId="formBasicZipcode" className="mb-4">
                                                       <Form.Control type="text" placeholder="Zip Code" className="inputType1" /> 
                                                       </Form.Group>
                                                  </Col>  
                                                  <Col md={6}> 
                                                       <Form.Group controlId="formBasicPwd" className="mb-4">    
                                                       <Form.Control type="password" placeholder="Password"  className="inputType1" />   
                                                       </Form.Group>
                                                  </Col>
                                                  <Col md={6}>
                                                       <Form.Group controlId="formBasicCpwd" className="mb-4">
                                                       <Form.Control type="password" placeholder="Confirm Password" className="inputType1" /> 
                                                       </Form.Group>
                                                  </Col>
                                                  <Col md={12}>      
                                                       <Form.Group controlId="formBasicCheckbox" className="mb-4">
                                                            <Form.Check type="checkbox" className="checkboxTyp1" label="I agree to the Terms & Conditions" />  
                                                       </Form.Group>  
                                                       <div className="d-md-flex mt-5 align-sm-center align-items-center">   
                                                            <Button className="btnType4 fw600 mr-4">Sign Up</Button>
                                                            <div className="fw500 col5">Already have an account?  
                                                            <span className="col1 fw700 pointer ml-2">Sign In</span></div>
                                                       </div>       
                                                  </Col>  
                                           </Row>
                                     </Form>
                                </div>
                            </div> 
                      </Col>
                 </Container>
            </div> 
    )
}

export default StudentRegister; 

