import React from 'react';
import { Col, Row, Container, Image, Form, Button } from 'react-bootstrap';
import Logos from '../../../../assets/images/logos.png';
import Facebook from '../../../../assets/images/svg/facebook.svg';
import Googles from '../../../../assets/images/svg/googles.svg';
import Linkedin from '../../../../assets/images/svg/linkedin.svg';      

const CollegeLogin = () => {
     return (
          <div className="bgBanner d-flex align-items-center justify-content-center w-100">
               <Container>
                    <Col lg={10} md={10} className="m-auto">
                         <div className="studentBox text-center bgCol3 shadow1 br10 loginUsers">
                              <Image src={Logos} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" />
                              <div className="studentOne mt-4">  
                                   <div className="fs22 fw600 col2 mb-1">Join Today</div>
                                   <div className="fw500 col5 mb-4">Register using you social media profile</div>
                                   <div className="socialMedia mb-5">  
                                        <Image src={Facebook} alt="facebook" className="pointer mr-5"/>
                                        <Image src={Googles} alt="google" className="pointer mr-5"/>
                                        <Image src={Linkedin} alt="linkedin" className="pointer"/>  
                                   </div>  
                                   <hr className="hrBorderTwo orSet mb-5" />  
                                   <div className="fw600 fs22 col2 mb-1">Sign In</div>   
                                   <div className="fw500 col5 mb-4">Welcome to your account</div>  
                                   <Form className="formLayoutUi">  
                                        <Row>
                                             <Col md={6}> 
                                                  <Form.Group controlId="formBasicEmail"> 
                                                       <Form.Control type="email" placeholder="User ID" className="inputType1" />
                                                  </Form.Group>
                                             </Col> 
                                             <Col md={6}>
                                                  <Form.Group controlId="formBasicEmail" className="mb-2">
                                                       <Form.Control type="password" placeholder="Password" className="inputType1" />  
                                                  </Form.Group>
                                                  <div className="text-right col1 fs18 fw600 mb-4 pb-2 pointer">
                                                       Forgot Password?
                                                  </div>
                                             </Col>
                                             <Col md={12}>
                                             <div className="text-center d-flex align-items-center flex-wrap align-xs-center">   
                                                  <Button className="btnType4 fw600 mr-4">Sign In</Button>
                                                  <div className="fw500 col5 d-flex flex-wrap align-xs-center">Don’t have an account? 
                                                  <span className="col1 fw700 pointer ml-2">Sign Up</span></div>
                                             </div>  
                                        </Col>  
                                        </Row>
                                          
                                   </Form>
                              </div>
                         </div>
                    </Col>
               </Container>
          </div>
     )
}

export default CollegeLogin; 