import React, { useState } from 'react';
import { Col, Row, Container, Image, Form, Dropdown, Accordion, Card, Button, Table, Modal } from 'react-bootstrap';
import Logoadmin from '../../../../assets/images/svg/logoadmin.svg';
import bellIcon from '../../../../assets/images/svg/Bell.svg';
import Settings from '../../../../assets/images/svg/GearSix.svg';
import UserThree from '../../../../assets/images/user3.png';
import BackOne from '../../../../assets/images/svg/backs.svg';
import AccorIcon from '../../../../assets/images/accoricon.png';
import saveOne from '../../../../assets/images/svg/save1.svg';
import Prints from '../../../../assets/images/svg/prints.svg';
import BookmarkSimple from '../../../../assets/images/svg/BookmarkSimple.svg';
import TrashTwo from '../../../../assets/images/svg/Trash2.svg';
import Stars from '../../../../assets/images/svg/stars.svg';
import StarEmpty from '../../../../assets/images/svg/starempty.svg';
import WomanIcon from '../../../../assets/images/svg/woman1.svg';
import BoyIcon from '../../../../assets/images/svg/boy1.svg';
import ProgressOne from '../../../../assets/images/svg/progress1.svg';
import ProgressRed from '../../../../assets/images/svg/progressRed.svg';
import ProgressYellow from '../../../../assets/images/svg/progressYellow.svg';
import { render } from 'react-dom';   

const Dashboard = () => { 
     const [show, setShow] = useState(false);
     const handleClose = () => setShow(false);
     const handleShow = () => setShow(true);

     return (
          <div className="collegeAdminUi">      
               <Container fluid>
                    <div className="pl-2 pr-2 pt-4">
                         <Row>
                              <Col md={3}>
                                   <Image src={Logoadmin} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" />
                                   <span className="fs20 col24 fw700 ml-1">HiEd Harmony</span>
                              </Col>
                              <Col md={6}></Col>
                              <Col md={3}>
                                   <div className="powerBtn d-flex justify-content-end">
                                        <div className="mr-4">
                                             <Dropdown>
                                                  <Dropdown.Toggle id="dropdown-basic" className="DropdownType1">
                                                       <Image src={bellIcon} alt="Search" className="pointer setingOne mt-1" />
                                                  </Dropdown.Toggle>
                                             </Dropdown>
                                        </div>
                                        <Image src={Settings} alt="Search" className="pointer setingOne mr-5" />
                                        <div>
                                             <Dropdown>
                                                  <Dropdown.Toggle id="dropdown-basic" className="DropdownType1 col2 fs14 fw600">
                                                       <Image src={UserThree} alt="Search" className="pointer user1 mr-1" />
                                                         Jone Aly   <i className="fa fa-chevron-down ml-1 col8" aria-hidden="true"></i>
                                                  </Dropdown.Toggle>
                                                  <Dropdown.Menu>
                                                       <Dropdown.Item href="#">Profile</Dropdown.Item>
                                                       <Dropdown.Item href="#">Setting</Dropdown.Item>
                                                       <Dropdown.Item href="#">Logout</Dropdown.Item>
                                                  </Dropdown.Menu>
                                             </Dropdown>
                                        </div>
                                   </div>
                              </Col>
                         </Row>
                         <div className="col2 fs20 fw500 mt-4 mb-3">
                              <Image src={BackOne} alt="back" className="pointer mr-2 back1" /> Back
                         </div>
                         <div className="d-flex flex-wrap justify-content-between">
                              <div>
                                   <Dropdown className="dropdownBox1">
                                        <Dropdown.Toggle id="dropdown-basic" className="DropdownType4 col2 fw600 fs20">
                                             Saved Comparison <Image src={AccorIcon} alt="back" className="pointer ml-1" />
                                        </Dropdown.Toggle>
                                        <Dropdown.Menu className="shadow5">
                                             <Dropdown.Item>
                                                  <div className="fs22 fw500 col2">Science Colleges</div>
                                                  <div className="fs14 fw400 col5">Created Jan 05, 2021</div>
                                             </Dropdown.Item>
                                             <Dropdown.Item>
                                                  <div className="fs22 fw500 col2">Best Football Team</div>
                                                  <div className="fs14 fw400 col5">Created Dec 22, 2020</div>
                                             </Dropdown.Item>
                                             <Dropdown.Item>
                                                  <div className="fs22 fw500 col2">Arts College</div>
                                                  <div className="fs14 fw400 col5">Created Oct 10, 2020</div>
                                             </Dropdown.Item>
                                        </Dropdown.Menu>
                                   </Dropdown>
                              </div>
                              <div className="d-flex">
                                   <div className="col8 fw600 fs20 mr-4 pointer" onClick={handleShow}>Save Comparison</div>
                                   <Image src={saveOne} alt="back" className="pointer save1 mr-2" />
                                   <Image src={Prints} alt="back" className="pointer save1" />
                              </div>
                         </div>
                    </div>
                    <div className="bgCol4 comparisonType1 mb-4">
                         <Row>
                              <Col md={2}></Col>
                              <Col md={10} className="pl-5"> 
                                   <Row>
                                        <Col md={3}>
                                             <div className="bgCol39 br10 compareBox1">
                                                  <div className="p-3 mb-3">
                                                       <div className="w-100">
                                                            <Form.Control as="select" className="selectTyp3 pointer">
                                                                 <option>Kate Wilson</option>
                                                                 <option>College1</option>
                                                                 <option>College2</option>
                                                            </Form.Control>
                                                            <div className="col3 fs15 fw400">USA, TX <span className="ml-3">2 years</span></div>
                                                       </div>
                                                  </div>
                                                  <div className="d-flex align-items-center bgCol1 compareBox2">
                                                       <Image src={BookmarkSimple} alt="Book Mark" className="pointer mr-4" />
                                                       <Image src={TrashTwo} alt="Trash" className="pointer" />
                                                  </div>
                                             </div>
                                        </Col>
                                        <Col md={3}>
                                             <div className="bgCol39 br10 compareBox1">
                                                  <div className="p-3 mb-3">
                                                       <div className="w-100">
                                                            <Form.Control as="select" className="selectTyp3 pointer">
                                                                 <option>Kate Wilson</option>
                                                                 <option>College1</option>
                                                                 <option>College2</option>
                                                            </Form.Control>
                                                            <div className="col3 fs15 fw400">USA, TX <span className="ml-3">2 years</span></div>
                                                       </div>
                                                  </div>
                                                  <div className="d-flex align-items-center bgCol1 compareBox2">
                                                       <Image src={BookmarkSimple} alt="Book Mark" className="pointer mr-4" />
                                                       <Image src={TrashTwo} alt="Trash" className="pointer" />
                                                  </div>
                                             </div>
                                        </Col>
                                        <Col md={3}>
                                             <div className="bgCol39 br10 compareBox1">
                                                  <div className="p-3 mb-3">
                                                       <div className="w-100">
                                                            <Form.Control as="select" className="selectTyp3 pointer">
                                                                 <option>Kate Wilson</option>
                                                                 <option>College1</option>
                                                                 <option>College2</option>
                                                            </Form.Control>
                                                            <div className="col3 fs15 fw400">USA, TX <span className="ml-3">2 years</span></div>
                                                       </div>
                                                  </div>
                                                  <div className="d-flex align-items-center bgCol1 compareBox2">
                                                       <Image src={BookmarkSimple} alt="Book Mark" className="pointer mr-4" />
                                                       <Image src={TrashTwo} alt="Trash" className="pointer" />
                                                  </div>
                                             </div>
                                        </Col>
                                        <Col md={3}>
                                             <div className="bgCol39 br10 compareBox1">
                                                  <div className="p-3 mb-3">
                                                       <div className="w-100">
                                                            <Form.Control as="select" className="selectTyp3 pointer">
                                                                 <option>Kate Wilson</option>
                                                                 <option>College1</option>
                                                                 <option>College2</option>
                                                            </Form.Control>
                                                            <div className="col3 fs15 fw400">USA, TX <span className="ml-3">2 years</span></div>
                                                       </div>
                                                  </div>
                                                  <div className="d-flex align-items-center bgCol1 compareBox2">
                                                       <Image src={BookmarkSimple} alt="Book Mark" className="pointer mr-4" />
                                                       <Image src={TrashTwo} alt="Trash" className="pointer" />
                                                  </div>
                                             </div>
                                        </Col>
                                   </Row>
                              </Col>
                         </Row>
                    </div>
                    <div className="comparisonType4 br10"> 
                         <Table striped bordered className="tableType1 br10">
                              <tbody>
                                   <tr>
                                        <td>RATING</td>
                                        <td>
                                        {/* <StarRating
                                             defaultValue={5}
                                             min={2}
                                             max={10}
                                             step={3} />  */}
                                        </td>
                                        <td>
                                             <div className="fw500 col2 mb-2">
                                                  4.2
                                                  <span className="ml-2 ratingOne">
                                                       <Image src={Stars} alt="star" className="mr-1" />
                                                       <Image src={Stars} alt="star" className="mr-1" />
                                                       <Image src={Stars} alt="star" className="mr-1" />
                                                       <Image src={Stars} alt="star" className="mr-1" />
                                                       <Image src={StarEmpty} alt="star" className="mr-1" />
                                                  </span>
                                             </div>
                                        </td>
                                        <td>
                                             <div className="fw500 col2 mb-2">
                                                  4.2
                                                  <span className="ml-2 ratingOne">
                                                       <Image src={Stars} alt="star" className="mr-1" />
                                                       <Image src={Stars} alt="star" className="mr-1" />
                                                       <Image src={Stars} alt="star" className="mr-1" />
                                                       <Image src={Stars} alt="star" className="mr-1" />
                                                       <Image src={StarEmpty} alt="star" className="mr-1" />
                                                  </span>
                                             </div>
                                        </td>
                                        <td>
                                             <div className="fw500 col2 mb-2">
                                                  4.2
                                                  <span className="ml-2 ratingOne">
                                                       <Image src={Stars} alt="star" className="mr-1" />
                                                       <Image src={Stars} alt="star" className="mr-1" />
                                                       <Image src={Stars} alt="star" className="mr-1" />
                                                       <Image src={Stars} alt="star" className="mr-1" />
                                                       <Image src={StarEmpty} alt="star" className="mr-1" />
                                                  </span>
                                             </div>
                                        </td>
                                   </tr>
                                   <tr>
                                        <td>College Type</td>  
                                        <td>PUBLIC</td>
                                        <td>PUBLIC</td>
                                        <td>PUBLIC</td>
                                        <td>PUBLIC</td>
                                   </tr>
                                   <tr>
                                        <td>Campus Setting</td>
                                        <td>Rural</td>
                                        <td>Rural</td>
                                        <td>Urban</td>
                                        <td>Rural</td>
                                   </tr>
                                   <tr>
                                        <td>Total Enrollment</td>    
                                        <td>1,768</td>
                                        <td>1,568</td>
                                        <td>1,268</td>
                                        <td>1,868</td>
                                   </tr>  
                                   <tr>
                                        <td>Gender Distribution</td> 
                                        <td>
                                             <div className="col2 fw500 text-uppercase ratioType1">
                                                  <Image src={WomanIcon} alt="Icon" />
                                                  <span className="ml-2 mr-4">60%</span>
                                                  <Image src={BoyIcon} alt="Icon" />
                                                  <span className="ml-2">40%</span>
                                             </div>
                                        </td>
                                        <td>
                                             <div className="col2 fw500 text-uppercase ratioType1">
                                                  <Image src={WomanIcon} alt="Icon" />
                                                  <span className="ml-2 mr-4">60%</span>
                                                  <Image src={BoyIcon} alt="Icon" />
                                                  <span className="ml-2">40%</span>
                                             </div>
                                        </td>
                                        <td>
                                             <div className="col2 fw500 text-uppercase ratioType1">
                                                  <Image src={WomanIcon} alt="Icon" />
                                                  <span className="ml-2 mr-4">60%</span>
                                                  <Image src={BoyIcon} alt="Icon" />
                                                  <span className="ml-2">40%</span>
                                             </div>
                                        </td>
                                        <td>
                                             <div className="col2 fw500 text-uppercase ratioType1">
                                                  <Image src={WomanIcon} alt="Icon" />
                                                  <span className="ml-2 mr-4">60%</span>
                                                  <Image src={BoyIcon} alt="Icon" />
                                                  <span className="ml-2">40%</span>
                                             </div>
                                        </td>
                                   </tr>
                                   <tr>
                                        <td>Acceptance Rate</td>
                                        <td><Image src={ProgressOne} alt="Progress" /></td>
                                        <td><Image src={ProgressOne} alt="Progress" /></td>
                                        <td><Image src={ProgressOne} alt="Progress" /></td>
                                        <td><Image src={ProgressOne} alt="Progress" /></td>
                                   </tr>
                                   <tr>
                                        <td>Graduation Rate</td>
                                        <td><Image src={ProgressRed} alt="Progress" /></td>
                                        <td><Image src={ProgressYellow} alt="Progress" /></td>
                                        <td><Image src={ProgressRed} alt="Progress" /></td>
                                        <td><Image src={ProgressYellow} alt="Progress" /></td>
                                   </tr>
                                   <tr>
                                        <td>
                                             <div className="col2 fw500 mb-2 text-uppercase">Admissions</div>
                                             <div className="fs14 col2 fw500 mb-2">Applicants</div>
                                             <div className="fs14 col2 fw500 mb-2">Admissions</div>
                                             <div className="fs14 col2 fw500">Enrolled</div>
                                        </td>
                                        <td>
                                             <div className="col2 fw600 mb-2">1143</div>
                                             <div className="col2 fw600 mb-2">
                                                  <span>715</span>
                                                  <span className="ml-4 col8">62.55%</span>
                                             </div>
                                             <div className="col2 fw600 mb-2">
                                                  <span>346</span>
                                                  <span className="ml-4 col8">30.27%</span>
                                             </div>
                                        </td>
                                        <td>
                                             <div className="col2 fw600 mb-2">1143</div>
                                             <div className="col2 fw600 mb-2">
                                                  <span>715</span>
                                                  <span className="ml-4 col8">62.55%</span>
                                             </div>
                                             <div className="col2 fw600 mb-2">
                                                  <span>346</span>
                                                  <span className="ml-4 col8">30.27%</span>
                                             </div>
                                        </td>
                                        <td>
                                             <div className="col2 fw600 mb-2">1143</div>
                                             <div className="col2 fw600 mb-2">
                                                  <span>715</span>
                                                  <span className="ml-4 col8">62.55%</span>
                                             </div>
                                             <div className="col2 fw600 mb-2">
                                                  <span>346</span>
                                                  <span className="ml-4 col8">30.27%</span>
                                             </div>
                                        </td>
                                        <td>
                                             <div className="col2 fw600 mb-2">1143</div>
                                             <div className="col2 fw600 mb-2">
                                                  <span>715</span>
                                                  <span className="ml-4 col8">62.55%</span>
                                             </div>
                                             <div className="col2 fw600 mb-2">
                                                  <span>346</span>
                                                  <span className="ml-4 col8">30.27%</span>
                                             </div>
                                        </td>
                                   </tr>
                                   <tr>
                                        <td>Test Pre-requisite</td>
                                        <td>Either SAT or ACT</td>
                                        <td>Neither SAT or ACT</td>
                                        <td>Either SAT or ACT</td>
                                        <td>Either SAT or ACT</td>
                                   </tr>
                                   <tr>
                                        <td>Student - Faculty Ratio</td>
                                        <td>15:1</td>
                                        <td>13:1</td>
                                        <td>19:1</td>
                                        <td>19:1</td>
                                   </tr>
                                   <tr>
                                        <td>Popular Majors</td>
                                        <td>
                                             <div className="mb-2">
                                                  <div className="col2 fw500 mb-2">12%</div>
                                                  <div className="d-flex flex-wrap tagType6">
                                                       <div className="fs12 col10 bgCol11 fw500 mb-2 mr-2 text-uppercase">Parks</div>
                                                       <div className="fs12 col10 bgCol11 fw500 mb-2 mr-2 text-uppercase">recreation</div>
                                                       <div className="fs12 col10 bgCol11 fw500 mb-2 mr-2 text-uppercase">recreation</div>
                                                       <div className="fs12 col10 bgCol11 fw500 mb-2 mr-2 text-uppercase">leisure</div>
                                                       <div className="fs12 col10 bgCol11 fw500 mb-2 mr-2 text-uppercase">fitness studies</div>
                                                  </div>
                                             </div>
                                             <div className="mb-2">
                                                  <div className="col2 fw500 mb-2">11%</div>
                                                  <div className="d-flex flex-wrap tagType6">
                                                       <div className="fs12 col10 bgCol11 fw500 mb-2 mr-2 text-uppercase">nursing</div>
                                                  </div>
                                             </div>
                                             <div className="mb-2">
                                                  <div className="col2 fw500 mb-2">8%</div>
                                                  <div className="d-flex flex-wrap tagType6">
                                                       <div className="fs12 col10 bgCol11 fw500 mb-2 mr-2 text-uppercase">business administration</div>
                                                  </div>
                                             </div>
                                        </td>
                                        <td>
                                             <div className="mb-2">
                                                  <div className="col2 fw500 mb-2">12%</div>
                                                  <div className="d-flex flex-wrap tagType6">
                                                       <div className="fs12 col10 bgCol11 fw500 mb-2 mr-2 text-uppercase">Parks</div>
                                                       <div className="fs12 col10 bgCol11 fw500 mb-2 mr-2 text-uppercase">recreation</div>
                                                       <div className="fs12 col10 bgCol11 fw500 mb-2 mr-2 text-uppercase">recreation</div>
                                                       <div className="fs12 col10 bgCol11 fw500 mb-2 mr-2 text-uppercase">leisure</div>
                                                       <div className="fs12 col10 bgCol11 fw500 mb-2 mr-2 text-uppercase">fitness studies</div>
                                                  </div>
                                             </div>
                                             <div className="mb-2">
                                                  <div className="col2 fw500 mb-2">11%</div>
                                                  <div className="d-flex flex-wrap tagType6">
                                                       <div className="fs12 col10 bgCol11 fw500 mb-2 mr-2 text-uppercase">nursing</div>
                                                  </div>
                                             </div>
                                             <div className="mb-2">
                                                  <div className="col2 fw500 mb-2">8%</div>
                                                  <div className="d-flex flex-wrap tagType6">
                                                       <div className="fs12 col10 bgCol11 fw500 mb-2 mr-2 text-uppercase">business administration</div>
                                                  </div>
                                             </div>
                                        </td>
                                        <td>
                                             <div className="mb-2">
                                                  <div className="col2 fw500 mb-2">12%</div>
                                                  <div className="d-flex flex-wrap tagType6">
                                                       <div className="fs12 col10 bgCol11 fw500 mb-2 mr-2 text-uppercase">Parks</div>
                                                       <div className="fs12 col10 bgCol11 fw500 mb-2 mr-2 text-uppercase">recreation</div>
                                                       <div className="fs12 col10 bgCol11 fw500 mb-2 mr-2 text-uppercase">recreation</div>
                                                       <div className="fs12 col10 bgCol11 fw500 mb-2 mr-2 text-uppercase">leisure</div>
                                                       <div className="fs12 col10 bgCol11 fw500 mb-2 mr-2 text-uppercase">fitness studies</div>
                                                  </div>
                                             </div>
                                             <div className="mb-2">
                                                  <div className="col2 fw500 mb-2">11%</div>
                                                  <div className="d-flex flex-wrap tagType6">
                                                       <div className="fs12 col10 bgCol11 fw500 mb-2 mr-2 text-uppercase">nursing</div>
                                                  </div>
                                             </div>
                                             <div className="mb-2">
                                                  <div className="col2 fw500 mb-2">8%</div>
                                                  <div className="d-flex flex-wrap tagType6">
                                                       <div className="fs12 col10 bgCol11 fw500 mb-2 mr-2 text-uppercase">business administration</div>
                                                  </div>
                                             </div>
                                        </td>
                                        <td>
                                             <div className="mb-2">
                                                  <div className="col2 fw500 mb-2">12%</div>
                                                  <div className="d-flex flex-wrap tagType6">
                                                       <div className="fs12 col10 bgCol11 fw500 mb-2 mr-2 text-uppercase">Parks</div>
                                                       <div className="fs12 col10 bgCol11 fw500 mb-2 mr-2 text-uppercase">recreation</div>
                                                       <div className="fs12 col10 bgCol11 fw500 mb-2 mr-2 text-uppercase">recreation</div>
                                                       <div className="fs12 col10 bgCol11 fw500 mb-2 mr-2 text-uppercase">leisure</div>
                                                       <div className="fs12 col10 bgCol11 fw500 mb-2 mr-2 text-uppercase">fitness studies</div>
                                                  </div>
                                             </div>
                                             <div className="mb-2">
                                                  <div className="col2 fw500 mb-2">11%</div>
                                                  <div className="d-flex flex-wrap tagType6">
                                                       <div className="fs12 col10 bgCol11 fw500 mb-2 mr-2 text-uppercase">nursing</div>
                                                  </div>
                                             </div>
                                             <div className="mb-2">
                                                  <div className="col2 fw500 mb-2">8%</div>
                                                  <div className="d-flex flex-wrap tagType6">
                                                       <div className="fs12 col10 bgCol11 fw500 mb-2 mr-2 text-uppercase">business administration</div>
                                                  </div>
                                             </div>
                                        </td>
                                   </tr>
                              </tbody>
                         </Table>
                    </div>

               </Container>

               <Modal show={show} onHide={handleClose} className="modalType1">
                    <Modal.Header closeButton>
                         <Modal.Title className="col8 fw600 fs22 text-center">Save Comparison</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                         <div className="p-3">
                              <div className="fw500 col5 text-center mb-4">
                                   Please enter a name below to save your comparison for future reference
                               </div>
                              <Col md={9} className="m-auto">
                                   <Form.Group controlId="formBasicAdmissions" className="mb-5">
                                        <Form.Control type="text" placeholder="Admission 2021" className="inputType1" />
                                   </Form.Group>
                              </Col>
                              <div className="text-center">
                                   <Button onClick={handleClose} className="btnType2 compareBtn1 mr-4">
                                        Save
                                   </Button>
                                   <Button onClick={handleClose} className="btnType8">
                                        Cancel
                                   </Button>
                              </div>
                         </div>
                    </Modal.Body>
               </Modal>
          </div>
     )
}
export default Dashboard;

