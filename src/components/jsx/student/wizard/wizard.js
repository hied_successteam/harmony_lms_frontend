import React, { useState } from 'react';
import { Col, Row, Image, Button, Form, Nav, Tab, Modal, Accordion, Card, Table } from 'react-bootstrap';
import CheckGreen from '../../../../assets/images/svg/check3.svg';
import CheckGray from '../../../../assets/images/svg/check1.svg';
import 'rsuite/dist/styles/rsuite-default.css';
import Select from 'react-select';
import DatePicker from "react-datepicker"; 
import "react-datepicker/dist/react-datepicker.css";
import Questions from "../../../../assets/images/svg/questions.svg";
import rocketDown from "../../../../assets/images/svg/rocket-down.svg";
import rocketUp from "../../../../assets/images/svg/rocket-up.svg";
import Sends from "../../../../assets/images/svg/sends.svg";
import Speed from "../../../../assets/images/svg/speed.svg";
import DownIcon from "../../../../assets/images/svg/downIcon.svg"; 

const options = [
     { value: 'chocolate', label: 'Chocolate' },
     { value: 'strawberry', label: 'Strawberry' },
     { value: 'vanilla', label: 'Vanilla' },
     { value: 'chocolate2', label: 'Chocolate2' },
     { value: 'strawberry2', label: 'Strawberry2' },
     { value: 'vanilla2', label: 'Vanilla2' },
     { value: 'chocolat3', label: 'Chocolate3' },
     { value: 'strawberr3', label: 'Strawberry3' },
     { value: 'vanilla3', label: 'Vanilla3' }
]

const Student = () => { 

     const [startDate, setStartDate] = useState(new Date());
     const [sex, setSex] = useState('');

     return (
          <div className="wizardLayout">  
               <div className="container-fluid pl-0">
                    <Tab.Container id="left-tabs-example" defaultActiveKey="fifth"> 
                         <Row>
                              <Col sm={3}>
                                   <div className="sidebarBox d-flex align-items-center justify-content-center text-center">
                                        <Nav variant="pills" className="flex-column innerBox">
                                             <Nav.Item className="pb-3 mb-2">
                                                  <Nav.Link eventKey="profile">
                                                       <span className="profileTitl1 fs20 text-uppercase fs15 fw600 d-block mt-3 col3">Profile</span>
                                                  </Nav.Link>
                                             </Nav.Item>
                                             <Nav.Item className="pb-3">
                                                  <Nav.Link eventKey="fifth" className="listBox">
                                                       <span className="circleBox text-uppercase d-block m-auto col3 fs24 br50 mb-4 fw600">1</span>
                                                       <span className="titles text-uppercase fs15 fw600 d-block mt-3">Basic info</span>
                                                  </Nav.Link>
                                             </Nav.Item>
                                             <Nav.Item className="pb-3">
                                                  <Nav.Link eventKey="first" className="listBox">
                                                       <span className="circleBox text-uppercase d-block m-auto col3 fs24 br50 mb-4 fw600">2</span>
                                                       <span className="titles text-uppercase fs15 fw600 d-block mt-3">Academic Info</span>
                                                  </Nav.Link>
                                             </Nav.Item>
                                             <Nav.Item className="pb-3">
                                                  <Nav.Link eventKey="second" className="listBox">
                                                       <span className="circleBox text-uppercase d-block m-auto col3 fs24 br50 mb-4 fw600">3</span>
                                                       <span className="titles text-uppercase fs15 fw600 d-block mt-3">College Preferences</span>
                                                  </Nav.Link>
                                             </Nav.Item>
                                             <Nav.Item className="pb-3">
                                                  <Nav.Link eventKey="third" className="listBox">
                                                       <span className="circleBox text-uppercase d-block m-auto col3 fs24 br50 mb-4 fw600">4</span>
                                                       <span className="titles text-uppercase fs15 fw600 d-block mt-3">
                                                            Demographic Preferences
                                                       </span>
                                                  </Nav.Link>
                                             </Nav.Item>
                                             <Nav.Item className="pb-3">
                                                  <Nav.Link eventKey="fourth" className="listBox">
                                                       <span className="circleBox text-uppercase d-block m-auto col3 fs24 br50 mb-4 fw600">5</span>
                                                       <span className="titles text-uppercase fs15 fw600 d-block mt-3">Employment Info</span>
                                                  </Nav.Link>
                                             </Nav.Item>
                                             <Nav.Item className="pb-3">
                                                  <Nav.Link eventKey="six" className="listBox">
                                                       <span className="circleBox text-uppercase d-block m-auto col3 fs24 br50 mb-4 fw600">6</span>
                                                       <span className="titles text-uppercase fs15 fw600 d-block mt-3">Placement Preferences</span>
                                                  </Nav.Link>
                                             </Nav.Item>
                                             <Nav.Item className="pb-3">
                                                  <Nav.Link eventKey="seven" className="listBox">
                                                       <span className="circleBox text-uppercase d-block m-auto col3 fs24 br50 mb-4 fw600">7</span>
                                                       <span className="titles text-uppercase fs15 fw600 d-block mt-3">Financial Preferences</span>
                                                  </Nav.Link>
                                             </Nav.Item>
                                             <Nav.Item className="pb-3">
                                                  <Nav.Link eventKey="Eight" className="listBox">
                                                       <span className="circleBox text-uppercase d-block m-auto col3 fs24 br50 mb-4 fw600">8</span>
                                                       <span className="titles text-uppercase fs15 fw600 d-block mt-3">Financial Preferences</span>
                                                  </Nav.Link>
                                             </Nav.Item>
                                        </Nav>
                                   </div>
                              </Col>
                              <Col sm={9}>
                                   <Tab.Content>    
                                        <Tab.Pane eventKey="fifth">
                                             <div className="rightBox pt-5 pb-5 pl-4">
                                                  <Col lg={10} md={12}>
                                                       <div className="rightLayout">
                                                            <div className="stepTitle fw500 fs30 col4 position-relative mb-4 pb-2">Basic </div>
                                                            <Row>
                                                                 <Col md={6}>
                                                                      <Form.Group controlId="formBasicName">
                                                                           <Form.Control type="text" placeholder="Name of Student" className="inputType1" />
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={6}>
                                                                      <Form.Group controlId="formBasicEmail">
                                                                           <Form.Control type="email" placeholder="Email ID" className="inputType1" />
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={6}>
                                                                      <Form.Group controlId="formBasicPhone">
                                                                           <Form.Control type="text" placeholder="Contact Number" className="inputType1" />
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={6}>
                                                                      <Form.Group controlId="formBasicDate">
                                                                           <DatePicker
                                                                                selected={startDate}
                                                                                onChange={(date) => setStartDate(date)}
                                                                                className="inputType1 w-100"
                                                                                placeholder="Date of Birth (DD/MM/YYYY)"
                                                                           />
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={6}>
                                                                      <Form.Group controlId="exampleForm.ControlSelect1">
                                                                           <Select
                                                                                defaultValue={[options[2], options[3]]}
                                                                                isMulti
                                                                                name="colors"
                                                                                options={options}
                                                                                className="basic-multi-select selectTyp2"
                                                                                classNamePrefix="select"
                                                                                placeholder="Country"
                                                                           />
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={6}>
                                                                      <Form.Group controlId="exampleForm.ControlSelect1">
                                                                           <Form.Control as="select" className="selectTyp1 pointer">
                                                                                <option>State</option>
                                                                                <option>UP</option>
                                                                                <option>MP</option>
                                                                           </Form.Control>
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={6}>
                                                                      <Form.Group controlId="exampleForm.ControlSelect1">
                                                                           <Form.Control as="select" className="selectTyp1 pointer">
                                                                                <option>City</option>
                                                                                <option>Indore</option>
                                                                                <option>Bhopal</option>
                                                                           </Form.Control>
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={6}>
                                                                      <Form.Group controlId="formBasicCode">
                                                                           <Form.Control type="text" placeholder="Zipcode" className="inputType1" />
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col2 fw600 mb-1">Gender</div>
                                                                      <Form.Group controlId="formBasicOne">
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="Male"
                                                                                className="radioType1 actives"
                                                                                name="sex"
                                                                                id="male"
                                                                                onChange={(e) => setSex(e.target.value)}
                                                                           />
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="Female"
                                                                                className="radioType1"
                                                                                id="female"
                                                                                name="sex"
                                                                                onChange={(e) => setSex(e.target.value)}
                                                                           />
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="Non-binary"
                                                                                className="radioType1"
                                                                                id="female"
                                                                                name="sex"
                                                                                onChange={(e) => setSex(e.target.value)}
                                                                           />
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="Prefer not to say"
                                                                                className="radioType1"
                                                                                id="female"
                                                                                name="sex"
                                                                                onChange={(e) => setSex(e.target.value)}
                                                                           />
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={6}>
                                                                      <div className="col2 fw600 mb-1">Religious Affiliation</div>
                                                                      <Form.Group controlId="exampleForm.ControlSelect1">
                                                                           <Form.Control as="select" className="selectTyp1 pointer">
                                                                                <option>Select</option>
                                                                                <option>Indore</option>
                                                                                <option>Bhopal</option>
                                                                           </Form.Control>
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={6}>
                                                                      <div className="col2 fw600 mb-1">Visa Status</div>
                                                                      <Form.Group controlId="exampleForm.ControlSelect1">
                                                                           <Form.Control as="select" className="selectTyp1 pointer">
                                                                                <option>Select</option>
                                                                                <option>Indore</option>
                                                                                <option>Bhopal</option>
                                                                           </Form.Control>
                                                                      </Form.Group>
                                                                 </Col>
                                                            </Row>
                                                       </div>
                                                       <div className="footerBotton">
                                                            <div className="hrBorder pt-3"></div>
                                                            <div className="text-right mt-3">
                                                                 <Button type="button" className="btnType2">
                                                                      Save & Next
                                                                 </Button>
                                                            </div>
                                                       </div>
                                                  </Col>
                                             </div>
                                        </Tab.Pane>

                                        <Tab.Pane eventKey="first">
                                             <div className="rightBox pt-5 pb-5 pl-4">
                                                  <Col lg={10} md={12}>
                                                       <div className="rightLayout">
                                                            <div className="stepTitle fw500 fs30 col4 position-relative mb-4 pb-2">Academic Info </div>
                                                            <Row>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 fs18 mb-2">
                                                                           <span className="numericType1">1.</span>
                                                                           What is your current progress in finding the suitable course or college?
                                                                           <sup className="starQ">*</sup>
                                                                      </div>
                                                                 </Col>
                                                                 <Col md={6}>
                                                                      <Form.Group controlId="exampleForm.ControlSelect1">
                                                                           <Form.Control as="select" className="selectTyp1 pointer">
                                                                                <option>Seeking out info. about College/Course</option>
                                                                                <option>Certificate</option>
                                                                                <option>Diploma</option>
                                                                           </Form.Control>
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 fs18 d-flex mb-2">
                                                                           <span className="numericType1">2.</span> Which batch (fall/spring) would you like to apply for?
                                                                      </div>
                                                                      <Form.Group controlId="formBasicOne">
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="Spring 2022"
                                                                                className="radioType1 actives"
                                                                                name="sex"
                                                                                id="male"
                                                                                onChange={(e) => setSex(e.target.value)}
                                                                           />
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="Fall 2022"
                                                                                className="radioType1"
                                                                                id="female"
                                                                                name="sex"
                                                                                onChange={(e) => setSex(e.target.value)}
                                                                           />
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="Spring 2023"
                                                                                className="radioType1"
                                                                                id="female"
                                                                                name="sex"
                                                                                onChange={(e) => setSex(e.target.value)}
                                                                           />
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="Fall 2023 or Later"
                                                                                className="radioType1"
                                                                                id="female"
                                                                                name="sex"
                                                                                onChange={(e) => setSex(e.target.value)}
                                                                           />
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 fs18 mb-2">
                                                                           <span className="numericType1">3.</span> What is the highest degree or the level of education you have completed
                                                                           (If you are currently enrolled in school/college, indicate the highest degree you have  achieved)?
                                                                           <sup className="starQ">*</sup>
                                                                      </div>
                                                                 </Col>
                                                                 <Col md={6}>
                                                                      <Form.Group controlId="exampleForm.ControlSelect1">
                                                                           <Form.Control as="select" className="selectTyp1 pointer">
                                                                                <option>Seeking out info. about College/Course</option>
                                                                                <option>Certificate</option>
                                                                                <option>Diploma</option>
                                                                           </Form.Control>
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 fs18 mb-2 d-flex">
                                                                           <span className="numericType1">4.</span> Which entrance exams have you appeared recently  (Select all that you have appeared)?
                                                                      </div>
                                                                      <div className="tagType1 d-flex flex-wrap mb-3">
                                                                           <span className="br8 col4 fw500 mr-3">TOEFL</span>
                                                                           <span className="bgCol35 br8 col4 fw500 mr-3">SAT</span>
                                                                           <span className="br8 col4 fw500 mr-3">ACT</span>
                                                                           <span className="bgCol35 br8 col4 fw500 mr-3">IELTS</span>
                                                                           <span className="bgCol35 br8 col4 fw500 mr-3">PSAT/NMSQT</span>
                                                                           <span className="br8 col4 fw500 mr-3">Not Applicable</span>
                                                                      </div>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 fs18 d-flex mb-2">
                                                                           <span className="numericType1">5.</span> Which educational degree would you like to pursue next?
                                                                      </div>
                                                                 </Col>
                                                                 <Col md={6}>
                                                                      <Form.Group controlId="exampleForm.ControlSelect1">
                                                                           <Form.Control as="select" className="selectTyp1 pointer">
                                                                                <option>Less than a high school diploma</option>
                                                                                <option>Certificate</option>
                                                                                <option>Diploma</option>
                                                                           </Form.Control>
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 fs18 d-flex mb-2">
                                                                           <span className="numericType1">6.</span> Which field of study would you like to pursue?
                                                                      </div>

                                                                      <div className="tagType1 d-flex flex-wrap mb-3">
                                                                           <span className="br8 col4 fw500 mr-3">Business</span>
                                                                           <span className="bgCol35 br8 col4 fw500 mr-3">Health professions & related programs</span>
                                                                           <span className="br8 col4 fw500 mr-3">Social sciences & history</span>
                                                                           <span className="bgCol35 br8 col4 fw500 mr-3">Engineering</span>
                                                                           <span className="bgCol35 br8 col4 fw500 mr-3">Education</span>
                                                                      </div>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 fs18 d-flex mb-2">
                                                                           <span className="numericType1">7.</span> Write a short essay describing your passion for the field of study and how you will contribute to the college community.</div>

                                                                      <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                                                                           <Form.Control as="textarea" className="inputType1" rows={5} placeholder="(Min. 100 words)" />
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 fs18 d-flex mb-2">
                                                                           <span className="numericType1">8.</span> How strong are your letters of recommendation?
                                                                      </div>
                                                                      <Form.Group controlId="formBasicOne">
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="Very strong"
                                                                                className="radioType1 actives"
                                                                                name="sex"
                                                                                id="male"
                                                                                onChange={(e) => setSex(e.target.value)}
                                                                           />
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="Fairly strong"
                                                                                className="radioType1"
                                                                                id="female"
                                                                                name="sex"
                                                                                onChange={(e) => setSex(e.target.value)}
                                                                           />
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="Average"
                                                                                className="radioType1"
                                                                                id="female"
                                                                                name="sex"
                                                                                onChange={(e) => setSex(e.target.value)}
                                                                           />
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="Not applicable"
                                                                                className="radioType1"
                                                                                id="female"
                                                                                name="sex"
                                                                                onChange={(e) => setSex(e.target.value)}
                                                                           />
                                                                      </Form.Group>
                                                                 </Col>
                                                            </Row>

                                                            <Row className="examList">
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 fs18 d-flex mb-3">
                                                                           <span className="numericType1">9.</span>
                                                                           Enter Your ACT Scores
                                                                      </div>
                                                                 </Col>
                                                                 <Col md={2}>
                                                                      <div className="col2 fw600 mb-3">English</div>
                                                                      <Form.Group controlId="formBasicPhone">
                                                                           <Form.Control type="text" className="inputType2" />
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={2}>
                                                                      <div className="col2 fw600 mb-3">Maths</div>
                                                                      <Form.Group controlId="formBasicPhone">
                                                                           <Form.Control type="text" className="inputType2" />
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={2}>
                                                                      <div className="col2 fw600 mb-3">Reading</div>
                                                                      <Form.Group controlId="formBasicPhone">
                                                                           <Form.Control type="text" className="inputType2" />
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={2}>
                                                                      <div className="col2 fw600 mb-3">Science</div>
                                                                      <Form.Group controlId="formBasicPhone">
                                                                           <Form.Control type="text" className="inputType2" />
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={2}>
                                                                      <div className="col2 fw600 mb-3">Composite</div>
                                                                      <Form.Group controlId="formBasicPhone">
                                                                           <Form.Control type="text" className="inputType2" />
                                                                      </Form.Group>
                                                                 </Col>
                                                            </Row>

                                                            <Row className="examList">
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 fs18 d-flex mb-3">
                                                                           <span className="numericType1">9.</span>
                                                                           Enter Your ACT Scores
                                                                      </div>
                                                                 </Col>
                                                                 <Col md={2}>
                                                                      <div className="col2 fw600 mb-3">English</div>
                                                                      <Form.Group controlId="formBasicPhone">
                                                                           <Form.Control type="text" className="inputType2" />
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={2}>
                                                                      <div className="col2 fw600 mb-3">Maths</div>
                                                                      <Form.Group controlId="formBasicPhone">
                                                                           <Form.Control type="text" className="inputType2" />
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={2}>
                                                                      <div className="col2 fw600 mb-3">Reading</div>
                                                                      <Form.Group controlId="formBasicPhone">
                                                                           <Form.Control type="text" className="inputType2" />
                                                                      </Form.Group>
                                                                 </Col>
                                                            </Row>

                                                            <Row className="examList">
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 fs18 d-flex mb-3">
                                                                           <span className="numericType1">9.</span>
                                                                           Enter Your ACT Scores
                                                                      </div>
                                                                 </Col>
                                                                 <Col md={2}>
                                                                      <div className="col2 fw600 mb-3">English</div>
                                                                      <Form.Group controlId="formBasicPhone">
                                                                           <Form.Control type="text" className="inputType2" />
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={2}>
                                                                      <div className="col2 fw600 mb-3">Maths</div>
                                                                      <Form.Group controlId="formBasicPhone">
                                                                           <Form.Control type="text" className="inputType2" />
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={2}>
                                                                      <div className="col2 fw600 mb-3">Reading</div>
                                                                      <Form.Group controlId="formBasicPhone">
                                                                           <Form.Control type="text" className="inputType2" />
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={2}>
                                                                      <div className="col2 fw600 mb-3">Reading</div>
                                                                      <Form.Group controlId="formBasicPhone">
                                                                           <Form.Control type="text" className="inputType2" />
                                                                      </Form.Group>
                                                                 </Col>
                                                            </Row>

                                                            <Row className="examList">
                                                                 <Col md={2}>
                                                                      <div className="col2 fw600 mb-3">English</div>
                                                                      <Form.Group controlId="formBasicPhone">
                                                                           <Form.Control type="text" className="inputType2" />
                                                                      </Form.Group>
                                                                 </Col>
                                                            </Row>


                                                       </div>
                                                       <div className="footerBotton">
                                                            <div className="hrBorder pt-3"></div>
                                                            <div className="text-right d-flex flex-wrap justify-content-between  mt-3">
                                                                 <Button type="button" className="btnType7">
                                                                      Previous
                                                                 </Button>
                                                                 <Button type="button" className="btnType2">
                                                                      Save & Next
                                                                 </Button>
                                                            </div>
                                                       </div>
                                                  </Col>
                                             </div>
                                        </Tab.Pane>

                                        <Tab.Pane eventKey="second">
                                             <div className="rightBox pt-5 pb-5 pl-4">
                                                  <Col lg={10} md={12}>
                                                       <div className="rightLayout">
                                                            <div className="d-flex flex-wrap justify-content-between mb-1">
                                                                 <div className="stepTitle fw500 fs30 col4 position-relative mb-4 pb-1">College Preferences </div>
                                                                 <div className="col2 fw500 fs18 pointer">Skip</div>
                                                            </div>
                                                            <Row>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 fs18 d-flex mb-2">
                                                                           <span className="numericType1">1.</span> Select top priorities when considering admission in a particular college (Choose 1 or more).
                                                                      </div>
                                                                      <div className="tagType1 d-flex flex-wrap mb-3">
                                                                           <span className="br8 col4 fw500 mr-3">Campus safety</span>
                                                                           <span className="bgCol35 br8 col4 fw500 mr-3">First generation grants</span>
                                                                           <span className="br8 col4 fw500 mr-3">International student friendly</span>
                                                                           <span className="bgCol35 br8 col4 fw500 mr-3">Career guidance</span>
                                                                           <span className="br8 col4 fw500 mr-3">Flexible curriculum</span>
                                                                           <span className="bgCol35 col4 fw500 mr-3">Internships & research opportunitites</span>
                                                                      </div>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 fs18 mb-2 d-flex">
                                                                           <span className="numericType1">2.</span> Which colleges/universities based on Ranking would you be interested in joining?
                                                                      </div>
                                                                      <div className="tagType1 d-flex flex-wrap mb-3">
                                                                           <span className="br8 col4 fw500 mr-3">Top20</span>
                                                                           <span className="bgCol35 br8 col4 fw500 mr-3">Top50</span>
                                                                           <span className="br8 col4 fw500 mr-3">Top100</span>
                                                                           <span className="bgCol35 br8 col4 fw500 mr-3">
                                                                                Top200
                                                                           </span>
                                                                           <span className="br8 col4 fw500 mr-3">Flexible curriculum</span>
                                                                           <span className="bgCol35 col4 fw500 mr-3">
                                                                                No Preferences / All colleges
                                                                           </span>
                                                                      </div>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 d-flex fs18 mb-2">
                                                                           <span className="numericType1">3.</span> Any specific target colleges you would like to mention.
                                                                      </div>
                                                                 </Col>
                                                                 <Col md={6}>
                                                                      <Form.Group controlId="exampleForm.ControlSelect1">
                                                                           <Form.Control as="select" className="selectTyp1 pointer">
                                                                                <option>Select</option>
                                                                                <option>Certificate</option>
                                                                                <option>Diploma</option>
                                                                           </Form.Control>
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 d-flex fs18 mb-2">
                                                                           <span className="numericType1">4.</span> Preferred college size (student population, Choose 1 or more).
                                                                      </div>
                                                                      <div className="tagType1 d-flex flex-wrap mb-3">
                                                                           <span className="br8 col4 fw500 mr-3">1,000 - 4,999</span>
                                                                           <span className="bgCol35 br8 col4 fw500 mr-3">5,000 - 9,999</span>
                                                                           <span className="br8 col4 fw500 mr-3">10,000 - 19,999</span>
                                                                           <span className="bgCol35 br8 col4 fw500 mr-3">20,000 and above</span>
                                                                      </div>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 d-flex fs18 mb-2">
                                                                           <span className="numericType1">5.</span> Which mode of education is preferred for you?
                                                                      </div>
                                                                      <Form.Group controlId="formBasicOne">
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="Online"
                                                                                className="radioType1 actives"
                                                                                name="option1"
                                                                                id="option1"
                                                                           />
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="Face to Face"
                                                                                className="radioType1"
                                                                                id="option2"
                                                                                name="option2"
                                                                           />
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="Hybrid"
                                                                                className="radioType1"
                                                                                id="option3"
                                                                                name="option3"
                                                                           />
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 d-flex fs18 mb-2">
                                                                           <span className="numericType1">6.</span> Which college clubs would you be most interested in joining? (Choose 1 or more).
                                                                      </div>
                                                                      <div className="tagType1 d-flex flex-wrap mb-3">
                                                                           <span className="br8 col4 fw500 mr-3">
                                                                                Academic (math, literature, language, history)
                                                                           </span>
                                                                           <span className="bgCol35 br8 col4 fw500 mr-3">
                                                                                Business & entrepreneurship
                                                                           </span>
                                                                           <span className="br8 col4 fw500 mr-3">
                                                                                Media & publication
                                                                           </span>
                                                                           <span className="bgCol35 br8 col4 fw500 mr-3">
                                                                                Political & government (student senate)
                                                                           </span>
                                                                           <span className="bgCol35 br8 col4 fw500 mr-3">
                                                                                Community service & social justice
                                                                           </span>
                                                                           <span className="br8 col4 fw500 mr-3">
                                                                                Science & engineering
                                                                           </span>
                                                                      </div>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 d-flex fs18 mb-2">
                                                                           <span className="numericType1">7.</span> Which of the following sports facilities would you like to use? (Choose 1 or more).
                                                                      </div>
                                                                      <div className="tagType1 d-flex flex-wrap mb-3">
                                                                           <span className="br8 col4 fw500 mr-3">
                                                                                Archery
                                                                           </span>
                                                                           <span className="bgCol35 br8 col4 fw500 mr-3">
                                                                                Badminton
                                                                           </span>
                                                                           <span className="br8 col4 fw500 mr-3">
                                                                                Baseball
                                                                           </span>
                                                                           <span className="bgCol35 br8 col4 fw500 mr-3">
                                                                                Baseball
                                                                           </span>
                                                                           <span className="bgCol35 br8 col4 fw500 mr-3">
                                                                                Cross-country
                                                                           </span>
                                                                           <span className="br8 col4 fw500 mr-3">
                                                                                Cycling
                                                                           </span>
                                                                      </div>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 d-flex fs18 mb-2">
                                                                           <span className="numericType1">8.</span> Please share your preference of college (dorm) roommates.
                                                                      </div>
                                                                 </Col>
                                                                 <Col md={6}>
                                                                      <Form.Group controlId="exampleForm.ControlSelect1">
                                                                           <Form.Control as="select" className="selectTyp1 pointer">
                                                                                <option>1st year only</option>
                                                                                <option>Certificate</option>
                                                                                <option>Diploma</option>
                                                                           </Form.Control>
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 d-flex fs18 mb-2">
                                                                           <span className="numericType1">9.</span> Which campus safety features are important to you? (Choose 1 or more).
                                                                      </div>
                                                                      <div className="tagType1 d-flex flex-wrap mb-3">
                                                                           <span className="br8 col4 fw500 mr-3">
                                                                                24-hour foot and vehicle patrols
                                                                           </span>
                                                                           <span className="bgCol35 br8 col4 fw500 mr-3">
                                                                                Lit pathways/sidewalks
                                                                           </span>
                                                                           <span className="bgCol35 br8 col4 fw500 mr-3">
                                                                                24-hour emergency helpline
                                                                           </span>
                                                                           <span className="br8 col4 fw500 mr-3">
                                                                                Controlled access to buildings (dormitory, campus, etc.)
                                                                           </span>
                                                                      </div>
                                                                 </Col>

                                                            </Row>
                                                       </div>
                                                       <div className="footerBotton">
                                                            <div className="hrBorder pt-3"></div>
                                                            <div className="text-right d-flex flex-wrap justify-content-between  mt-3">
                                                                 <Button type="button" className="btnType7">
                                                                      Previous
                                                                 </Button>
                                                                 <Button type="button" className="btnType2">
                                                                      Save & Next
                                                                 </Button>
                                                            </div>
                                                       </div>
                                                  </Col>
                                             </div>
                                        </Tab.Pane>

                                        <Tab.Pane eventKey="third">
                                             <div className="rightBox pt-5 pb-5 pl-4">
                                                  <Col lg={10} md={12}>
                                                       <div className="rightLayout">
                                                            <div className="stepTitle fw500 fs30 col4 position-relative mb-4 pb-2">Demographic Preferences </div>
                                                            <Row>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 d-flex fs18 mb-2">
                                                                           <span className="numericType1">1.</span> Will  you be the first generation student graduating?
                                                                      </div>
                                                                      <Form.Group controlId="formBasicOne">
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="Yes"
                                                                                className="radioType1 actives"
                                                                                name="option5"
                                                                                id="option5"
                                                                           />
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="No"
                                                                                className="radioType1"
                                                                                id="option5"
                                                                                name="option5"
                                                                           />
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 d-flex fs18 mb-2">
                                                                           <span className="numericType1">2.</span> What is your ethnicity?
                                                                      </div>
                                                                 </Col>
                                                                 <Col md={6}>
                                                                      <Form.Group controlId="exampleForm.ControlSelect1">
                                                                           <Form.Control as="select" className="selectTyp1 pointer">
                                                                                <option>American Indian or Alaska Native

                                                                                </option>
                                                                                <option>Certificate</option>
                                                                                <option>Diploma</option>
                                                                           </Form.Control>
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 d-flex fs18 mb-2">
                                                                           <span className="numericType1">3.</span> Do you prefer colleges which follow a Specialized Mission?
                                                                      </div>
                                                                 </Col>
                                                                 <Col md={6}>
                                                                      <Form.Group controlId="exampleForm.ControlSelect1">
                                                                           <Form.Control as="select" className="selectTyp1 pointer">
                                                                                <option>Yes, Single-sex: Men

                                                                                </option>
                                                                                <option>Certificate</option>
                                                                                <option>Diploma</option>
                                                                           </Form.Control>
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="hrBorder mb-4 mt-1"></div>
                                                                      <div className="fs20 fw600 col2 mb-3">Climate Preferences</div>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 d-flex fs18 mb-2">
                                                                           <span className="numericType1">4.</span> What are your preferred weather conditions (Choose 1 or more,  Approx. Temperature).
                                                                      </div>
                                                                      <div className="tagType1 d-flex flex-wrap mb-3">
                                                                           <span className="br8 col4 fw500 mr-3">Hot (Above 35C / 95F)</span>
                                                                           <span className="bgCol35 br8 col4 fw500 mr-3">Moderate (15C to 35C / 59F to 59F)</span>
                                                                           <span className="br8 col4 fw500 mr-3">Cold ( -15C - 15C / 5F to 15F)</span>
                                                                      </div>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 d-flex fs18 mb-2">
                                                                           <span className="numericType1">5.</span> Do you have any seasonal allergy?(E.g. Pollen).
                                                                      </div>
                                                                      <Form.Group controlId="formBasicOne">
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="Yes"
                                                                                className="radioType1 actives"
                                                                                name="option5"
                                                                                id="option5"
                                                                           />
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="No"
                                                                                className="radioType1"
                                                                                id="option5"
                                                                                name="option5"
                                                                           />
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="I Don’t know"
                                                                                className="radioType1"
                                                                                id="option5"
                                                                                name="option5"
                                                                           />
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="hrBorder mb-4 mt-1"></div>
                                                                      <div className="fs20 fw600 col2 mb-3">Location Preferences</div>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 d-flex fs18 mb-2">
                                                                           <span className="numericType1">6.</span> What are your preferred weather conditions (Choose 1 or more,  Approx. Temperature).
                                                                      </div>
                                                                      <div className="tagType1 d-flex flex-wrap mb-3">
                                                                           <span className="br8 col4 fw500 mr-3">
                                                                                Rural
                                                                           </span>
                                                                           <span className="bgCol35 br8 col4 fw500 mr-3">
                                                                                City
                                                                           </span>
                                                                           <span className="br8 col4 fw500 mr-3">
                                                                                Town
                                                                           </span>
                                                                           <span className="br8 col4 fw500 mr-3">
                                                                                Suburbs
                                                                           </span>
                                                                           <span className="br8 col4 fw500 mr-3">
                                                                                No Preference
                                                                           </span>
                                                                      </div>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 d-flex fs18 mb-2">
                                                                           <span className="numericType1">7.</span> Do you prefer colleges that are closer to home? If yes, please select the preferred proximity from home (miles).
                                                                      </div>
                                                                 </Col>
                                                                 <Col md={6}>
                                                                      <Form.Group controlId="exampleForm.ControlSelect1">
                                                                           <Form.Control as="select" className="selectTyp1 pointer">
                                                                                <option>10 miles</option>
                                                                                <option>Certificate</option>
                                                                                <option>Diploma</option>
                                                                           </Form.Control>
                                                                      </Form.Group>
                                                                 </Col>

                                                            </Row>

                                                       </div>
                                                       <div className="footerBotton">
                                                            <div className="hrBorder pt-3"></div>
                                                            <div className="text-right d-flex flex-wrap justify-content-between  mt-3">
                                                                 <Button type="button" className="btnType7">
                                                                      Previous
                                                                 </Button>
                                                                 <Button type="button" className="btnType2">
                                                                      Save & Next
                                                                 </Button>
                                                            </div>
                                                       </div>
                                                  </Col>
                                             </div>
                                        </Tab.Pane>

                                        <Tab.Pane eventKey="fourth">

                                             <div className="rightBox pt-5 pb-5 pl-4">
                                                  <Col lg={10} md={12}>
                                                       <div className="rightLayout">
                                                            <div className="stepTitle fw500 fs30 col4 position-relative mb-4 pb-2">Employment Info</div>
                                                            <Row>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 d-flex fs18 mb-2">
                                                                           <span className="numericType1">1.</span> Which of the following personality traits do you possess or have experienced? (Choose 1 or more).
                                                                      </div>

                                                                      <div className="tagType1 d-flex flex-wrap mb-3">
                                                                           <span className="br8 col4 fw500 mr-3">
                                                                                Commitment
                                                                           </span>
                                                                           <span className="bgCol35 br8 col4 fw500 mr-3">
                                                                                Creativity
                                                                           </span>
                                                                           <span className="br8 col4 fw500 mr-3">
                                                                                Critical thinking ability
                                                                           </span>
                                                                           <span className="bgCol35 br8 col4 fw500 mr-3">Effective decision making</span>
                                                                           <span className="bgCol35 br8 col4 fw500 mr-3">Honesty</span>
                                                                      </div>
                                                                 </Col>

                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 d-flex fs18 mb-2">
                                                                           <span className="numericType1">2.</span> Share an experience or story where you have utilized or demonstrated the personality traits.
                                                                      </div>

                                                                      <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                                                                           <Form.Control as="textarea" className="inputType1" rows={5} placeholder="(Min. 100 words)" />
                                                                      </Form.Group>
                                                                 </Col>

                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 d-flex fs18 mb-2">
                                                                           <span className="numericType1">3.</span> How many hours have you dedicated towards community service or volunteer work in the last 3 years?
                                                                      </div>
                                                                 </Col>
                                                                 <Col md={6}>
                                                                      <Form.Group controlId="exampleForm.ControlSelect1">
                                                                           <Form.Control as="select" className="selectTyp1 pointer">
                                                                                <option>More than 50 hours</option>
                                                                                <option>Certificate</option>
                                                                                <option>Diploma</option>
                                                                           </Form.Control>
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 d-flex fs18 mb-2">
                                                                           <span className="numericType1">4.</span> What volunteering activities did you participate in? List down those activities.
                                                                      </div>
                                                                      <Form.Group controlId="formBasicCode">
                                                                           <Form.Control type="text" placeholder="Bas" className="inputType1 mb-3" />
                                                                           <div className="tagType1 d-flex flex-wrap">
                                                                                <span className="br8 col4 fw500 mr-3">Basketball</span>
                                                                                <span className="br8 col4 fw500 mr-3">Baseball</span>
                                                                                <span className="br8 col4 fw500 mr-3">Badminton</span>
                                                                                <span className="br8 col4 fw500 mr-3">
                                                                                     Ball Hockey
                                                                                </span>
                                                                           </div>
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 d-flex fs18 mb-2">
                                                                           <span className="numericType1">5.</span> What is your current employment status?
                                                                      </div>
                                                                 </Col>
                                                                 <Col md={6}>
                                                                      <Form.Group controlId="exampleForm.ControlSelect1">
                                                                           <Form.Control as="select" className="selectTyp1 pointer">
                                                                                <option>Employed Full - time(40+ hours a week)</option>
                                                                                <option>Certificate</option>
                                                                                <option>Diploma</option>
                                                                           </Form.Control>
                                                                      </Form.Group>
                                                                 </Col>
                                                            </Row>
                                                       </div>
                                                       <div className="footerBotton">
                                                            <div className="hrBorder pt-3"></div>
                                                            <div className="text-right d-flex flex-wrap justify-content-between  mt-3">
                                                                 <Button type="button" className="btnType7">
                                                                      Previous
                                                                 </Button>
                                                                 <Button type="button" className="btnType2">
                                                                      Save & Next
                                                                 </Button>
                                                            </div>
                                                       </div>
                                                  </Col>
                                             </div>
                                        </Tab.Pane>


                                        <Tab.Pane eventKey="six">
                                             <div className="rightBox pt-5 pb-5 pl-4">
                                                  <Col lg={10} md={12}>
                                                       <div className="rightLayout">
                                                            <div className="stepTitle fw500 fs30 col4 position-relative mb-4 pb-2">Employment Info</div>
                                                            <Row>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 d-flex fs18 mb-2">
                                                                           <span className="numericType1">1.</span> Is it required for the institute to have partnerships with certain companies for
                                                                           training/job opportunities?
                                                                      </div>
                                                                      <Form.Group controlId="formBasicOne">
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="Yes"
                                                                                className="radioType1 actives"
                                                                                name="option7"
                                                                                id="option7"
                                                                           />
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="No"
                                                                                className="radioType1"
                                                                                name="option7"
                                                                                id="option7"
                                                                           />
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="Preferred optional"
                                                                                className="radioType1"
                                                                                name="option7"
                                                                                id="option7"
                                                                           />
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 d-flex fs18 mb-2">
                                                                           <span className="numericType1">2.</span> Is it required for the institute to provide job assistance or employment opportunities for graduating students?
                                                                      </div>
                                                                      <Form.Group controlId="formBasicOne">
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="Yes"
                                                                                className="radioType1 actives"
                                                                                name="option7"
                                                                                id="option7"
                                                                           />
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="No"
                                                                                className="radioType1"
                                                                                name="option7"
                                                                                id="option7"
                                                                           />
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="Preferred optional"
                                                                                className="radioType1"
                                                                                name="option7"
                                                                                id="option7"
                                                                           />
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 d-flex fs18 mb-2">
                                                                           <span className="numericType1">3.</span> What is the minimum job placement rate that the college should have?
                                                                      </div>
                                                                 </Col>
                                                                 <Col md={6}>
                                                                      <Form.Group controlId="exampleForm.ControlSelect1">
                                                                           <Form.Control as="select" className="selectTyp1 pointer">
                                                                                <option>Medium (Greater than 60%)</option>
                                                                                <option>Certificate</option>
                                                                                <option>Diploma</option>
                                                                           </Form.Control>
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 d-flex fs18 mb-2">
                                                                           <span className="numericType1">4.</span> Is it important for the institute to offer Optional Practical Training (OPT) or Curricular Practical Training (CPT).
                                                                      </div>
                                                                      <Form.Group controlId="formBasicOne">
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="Yes"
                                                                                className="radioType1 actives"
                                                                                name="option7"
                                                                                id="option7"
                                                                           />
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="No"
                                                                                className="radioType1"
                                                                                name="option7"
                                                                                id="option7"
                                                                           />
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="Preferred optional"
                                                                                className="radioType1"
                                                                                name="option7"
                                                                                id="option7"
                                                                           />
                                                                      </Form.Group>
                                                                 </Col>
                                                            </Row>
                                                       </div>
                                                       <div className="footerBotton">
                                                            <div className="hrBorder pt-3"></div>
                                                            <div className="text-right d-flex flex-wrap justify-content-between  mt-3">
                                                                 <Button type="button" className="btnType7">
                                                                      Previous
                                                                 </Button>
                                                                 <Button type="button" className="btnType2">
                                                                      Save & Next
                                                                 </Button>
                                                            </div>
                                                       </div>
                                                  </Col>
                                             </div>
                                        </Tab.Pane>


                                        <Tab.Pane eventKey="seven">
                                             <div className="rightBox pt-5 pb-5 pl-4">
                                                  <Col lg={10} md={12}>
                                                       <div className="rightLayout">
                                                            <div className="stepTitle fw500 fs30 col4 position-relative mb-4 pb-2">Financial Preferences</div>
                                                            <Row>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 d-flex fs18 mb-2">
                                                                           <span className="numericType1">1.</span> What is your Current Income ?
                                                                      </div>
                                                                      <Form.Group controlId="formBasicOne">
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="$0 - $15,000"
                                                                                className="radioType1 actives"
                                                                                name="option7"
                                                                                id="option7"
                                                                           />
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="$15,000 - $30,000"
                                                                                className="radioType1"
                                                                                name="option7"
                                                                                id="option7"
                                                                           />
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="$15,000 - $30,000"
                                                                                className="radioType1"
                                                                                name="option7"
                                                                                id="option7"
                                                                           />
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 d-flex fs18 mb-2">
                                                                           <span className="numericType1">2.</span> What is the parents/household investment for  higher education?
                                                                      </div>
                                                                      <Form.Group controlId="formBasicOne">
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="$0 - $15,000"
                                                                                className="radioType1 actives"
                                                                                name="option7"
                                                                                id="option7"
                                                                           />
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="$10,000 - $20,000"
                                                                                className="radioType1"
                                                                                name="option7"
                                                                                id="option7"
                                                                           />
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="$20,000 - $30,000"
                                                                                className="radioType1"
                                                                                name="option7"
                                                                                id="option7"
                                                                           />
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="$30,000 - $40,000"
                                                                                className="radioType1"
                                                                                name="option7"
                                                                                id="option7"
                                                                           />
                                                                      </Form.Group>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 d-flex fs18 mb-2">
                                                                           <span className="numericType1">3.</span> Do you need any kind of financial assistance/aid? (Select the suitable option one or more).
                                                                      </div>
                                                                      <div className="tagType1 d-flex flex-wrap mb-3">
                                                                           <span className="br8 col4 fw500 mr-3">
                                                                                NSFAS
                                                                           </span>
                                                                           <span className="bgCol35 br8 col4 fw500 mr-3">Federal work study</span>
                                                                           <span className="br8 col4 fw500 mr-3">
                                                                                Scholarships
                                                                           </span>
                                                                           <span className="bgCol35 br8 col4 fw500 mr-3">
                                                                                Federal student aid
                                                                           </span>
                                                                           <span className="bgCol35 br8 col4 fw500 mr-3">Grants (Pell / Federal / state)</span>
                                                                      </div>
                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 d-flex fs18 mb-2">
                                                                           <span className="numericType1">4.</span> What is your estimated cost of education or net price.
                                                                      </div>
                                                                      <Form.Group controlId="formBasicOne">
                                                                           <Form.Check
                                                                                type="radio"
                                                                                label="Min $15000"
                                                                                className="radioType1 actives"
                                                                                name="option7"
                                                                                id="option7"
                                                                           />
                                                                      </Form.Group>
                                                                 </Col>

                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 d-flex fs18 mb-2">
                                                                           <span className="numericType1">5.</span> What is your estimated cost of education or net price.
                                                                      </div>

                                                                      <div className="table-responsive tableLayout5 mb-4"> 

                                                                      <Table bordered className="tableType2 br10 mb-4">
                                                                           <thead>
                                                                                <tr>
                                                                                     <th></th>
                                                                                     <th>Not at all important</th>
                                                                                     <th>Low importance</th>
                                                                                     <th>Slightly important</th>
                                                                                     <th>Neutral</th>
                                                                                     <th>Moderately important</th>
                                                                                     <th>Very important</th>
                                                                                     <th>Extremely important</th>
                                                                                     <th>Post-Secondary School, College/University, English Language Institute Name</th>
                                                                                     <th>Address (Number & Street)</th>
                                                                                     <th>Location (City, State, Country)</th>
                                                                                     <th>ZIPCODE</th>
                                                                                     <th>Type of Institution (Public/Private/Not Applicable)</th>
                                                                                     <th>2-Year/4-Year/Graduate/Not Applicable</th>
                                                                                     <th>Degree Obtained/ Major</th>
                                                                                     <th>Date of Enrollment (mm/dd/yyyy)</th>
                                                                                     <th>Date of Completion (mm/dd/yyyy)</th>
                                                                                     <th>Years attended</th>
                                                                                     <th>School/College Website</th>
                                                                                     <th>Language of Instruction</th>
                                                                                </tr>
                                                                           </thead>
                                                                           <tbody> 
                                                                                <tr>
                                                                                     <td>
                                                                                          Demographics (ehtnicity, gender)
                                                                                     </td>
                                                                                     <td>
                                                                                          <Form.Group controlId="formBasicRadio1">
                                                                                               <Form.Check type="radio" aria-label="radio 1" className="checkboxTyp3" label="" />
                                                                                          </Form.Group>
                                                                                     </td>
                                                                                     <td>
                                                                                          <Form.Group controlId="formBasicRadio2">
                                                                                               <Form.Check type="radio" aria-label="radio 1" className="checkboxTyp3" label="" />
                                                                                          </Form.Group>
                                                                                     </td>
                                                                                     <td>
                                                                                          <Form.Group controlId="formBasicRadio3">
                                                                                               <Form.Check type="radio" aria-label="radio 1" className="checkboxTyp3" label="" />
                                                                                          </Form.Group>
                                                                                     </td>
                                                                                     <td>
                                                                                          <Form.Group controlId="formBasicRadio4">
                                                                                               <Form.Check type="radio" aria-label="radio 1" className="checkboxTyp3" label="" />
                                                                                          </Form.Group>
                                                                                     </td>
                                                                                     <td>

                                                                                     </td>
                                                                                     <td>

                                                                                     </td>
                                                                                     <td>

                                                                                     </td>
                                                                                     
                                                                                     <td>
                                                                                          <Form.Group controlId="formBasicPhone">
                                                                                               <Form.Control type="text" className="inputType2 w-100 m-auto" />
                                                                                          </Form.Group>                                                                                         
                                                                                     </td>
                                                                                     <td>
                                                                                          <Form.Group controlId="formBasicPhone">
                                                                                               <Form.Control type="text" className="inputType2 w-100 m-auto" />
                                                                                          </Form.Group>                                                                                         
                                                                                     </td>
                                                                                     <td>
                                                                                          <Form.Group controlId="formBasicPhone">
                                                                                               <Form.Control type="text" className="inputType2 w-100 m-auto" />
                                                                                          </Form.Group>                                                                                         
                                                                                     </td>
                                                                                     <td>
                                                                                          <Form.Group controlId="formBasicPhone">
                                                                                               <Form.Control type="text" className="inputType2 w-100 m-auto" />
                                                                                          </Form.Group>                                                                                         
                                                                                     </td>
                                                                                     <td>
                                                                                          <Form.Group controlId="formBasicPhone">
                                                                                               <Form.Control type="text" className="inputType2 w-100 m-auto" />
                                                                                          </Form.Group>                                                                                         
                                                                                     </td>
                                                                                     <td>
                                                                                          <Form.Group controlId="formBasicPhone">
                                                                                               <Form.Control type="text" className="inputType2 w-100 m-auto" />
                                                                                          </Form.Group>                                                                                         
                                                                                     </td>
                                                                                     <td>
                                                                                          <Form.Group controlId="formBasicPhone">
                                                                                               <Form.Control type="text" className="inputType2 w-100 m-auto" />
                                                                                          </Form.Group>                                                                                         
                                                                                     </td>
                                                                                     <td>
                                                                                          <Form.Group controlId="formBasicPhone">
                                                                                               <Form.Control type="text" className="inputType2 w-100 m-auto" />
                                                                                          </Form.Group>                                                                                         
                                                                                     </td>
                                                                                     <td>
                                                                                          <Form.Group controlId="formBasicPhone">
                                                                                               <Form.Control type="text" className="inputType2 w-100 m-auto" />
                                                                                          </Form.Group>                                                                                         
                                                                                     </td>
                                                                                     <td>
                                                                                          <Form.Group controlId="formBasicPhone">
                                                                                               <Form.Control type="text" className="inputType2 w-100 m-auto" />
                                                                                          </Form.Group>                                                                                         
                                                                                     </td>
                                                                                     <td>
                                                                                          <Form.Group controlId="formBasicPhone">
                                                                                               <Form.Control type="text" className="inputType2 w-100 m-auto" />
                                                                                          </Form.Group>                                                                                         
                                                                                     </td>
                                                                                     <td>
                                                                                          <Form.Group controlId="formBasicPhone">
                                                                                               <Form.Control type="text" className="inputType2 w-100 m-auto" />
                                                                                          </Form.Group>                                                                                           
                                                                                     </td>

                                                                                </tr>

                                                                                <tr>
                                                                                     <td>
                                                                                          Academics (college ranking, facilities, dorm)
                                                                                     </td>
                                                                                     <td>
                                                                                          <Form.Group controlId="formBasicRadio5">
                                                                                               {/* <Form.Check
type="checkbox"
className="checkboxTyp3"
label="" /> */}

                                                                                               <Form.Check type="radio" aria-label="radio 1" className="checkboxTyp3" label="" />

                                                                                          </Form.Group>


                                                                                     </td>
                                                                                     <td>

                                                                                     </td>
                                                                                     <td>

                                                                                     </td>
                                                                                     <td>

                                                                                     </td>
                                                                                     <td>

                                                                                     </td>
                                                                                     <td>

                                                                                     </td>
                                                                                     <td>

                                                                                     </td>
                                                                                </tr>

                                                                                <tr>
                                                                                     <td>
                                                                                          Location (setting, proximity)
                                                                                     </td>
                                                                                     <td>

                                                                                     </td>
                                                                                     <td>

                                                                                     </td>
                                                                                     <td>

                                                                                     </td>
                                                                                     <td>

                                                                                     </td>
                                                                                     <td>

                                                                                     </td>
                                                                                     <td>

                                                                                     </td>
                                                                                     <td>

                                                                                     </td>
                                                                                </tr>

                                                                                <tr>
                                                                                     <td>
                                                                                          Climate (weather, allergies)
                                                                                     </td>
                                                                                     <td>

                                                                                     </td>
                                                                                     <td>

                                                                                     </td>
                                                                                     <td>

                                                                                     </td>
                                                                                     <td>

                                                                                     </td>
                                                                                     <td>

                                                                                     </td>
                                                                                     <td>

                                                                                     </td>
                                                                                     <td>

                                                                                     </td>
                                                                                </tr>
                                                                                <tr>
                                                                                     <td>
                                                                                          Financials (tuition fee, aid)
                                                                                     </td>
                                                                                     <td>

                                                                                     </td>
                                                                                     <td>

                                                                                     </td>
                                                                                     <td>

                                                                                     </td>
                                                                                     <td>

                                                                                     </td>
                                                                                     <td>

                                                                                     </td>
                                                                                     <td>

                                                                                     </td>
                                                                                     <td>

                                                                                     </td>
                                                                                </tr>
                                                                           </tbody>
                                                                      </Table>
  
                                                                      </div>  

                                                                 </Col>
                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 d-flex fs18 mb-2">
                                                                           <span className="numericType1">6.</span> Would you like to add extra information that is critical to your Institution search.</div>

                                                                      <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                                                                           <Form.Control as="textarea" className="inputType1" rows={5} placeholder="Write your text..." />
                                                                      </Form.Group>
                                                                 </Col>

                                                                 <Col md={12}>
                                                                      <div className="col4 fw600 d-flex fs18 mb-2">
                                                                           <span className="numericType1">7.</span>
                                                                           Price</div>

                                                                      <div className="input-group mb-3">
                                                                           <div className="input-group-prepend inputGroup1">
                                                                                <span className="input-group-text" id="basic-addon1">$</span>
                                                                           </div>
                                                                           <Form.Control type="text" placeholder="Pricing" className="inputType3" />
                                                                      </div>
                                                                 </Col>
                                                            </Row>
                                                       </div>
                                                       <div className="footerBotton">
                                                            <div className="hrBorder pt-3"></div>
                                                            <div className="text-right d-flex flex-wrap justify-content-between  mt-3">
                                                                 <Button type="button" className="btnType7">
                                                                      Previous
                                                                 </Button>
                                                                 <Button type="button" className="btnType2">
                                                                      Save & Next
                                                                 </Button>
                                                            </div>
                                                       </div>
                                                  </Col>
                                             </div>
                                        </Tab.Pane>


                                        <Tab.Pane eventKey="Eight">  
                                             <div className="rightBox pt-5 pb-5 pl-4">
                                                  <Col lg={10} md={12}>
                                                       <div className="rightLayout">
                                                            <div className="stepTitle fw500 fs30 col4 position-relative mb-4 pb-3">Select Plan</div>
                                                            <Row>
                                                                 <Col md={3}>
                                                                      <div className="PlanList1 active bgCol44 p-3">
                                                                           <div className="text-center mb-3">
                                                                                <div className="circleType3">
                                                                                     <Image src={Sends} alt="Icon" className="mw30" />
                                                                                </div>
                                                                           </div>
                                                                           <div className="col2 fs30 fw500 mb-1 elippse1">Free</div>
                                                                           <div className="mb-3 col5 fw500 fs14 elippse2">Lorem ipsum dolor sit amet, consectet adipicing.</div>
                                                                           <div className="col30 fw600">$00</div>
                                                                      </div>
                                                                 </Col>
                                                                 <Col md={3}>
                                                                      <div className="PlanList1 bgCol44 p-3">
                                                                           <div className="text-center mb-3">
                                                                                <div className="circleType3">
                                                                                     <Image src={rocketUp} alt="Icon" className="mw30" />
                                                                                </div>
                                                                           </div>
                                                                           <div className="col2 fs30 fw500 mb-1 elippse1">Advance</div>
                                                                           <div className="mb-3 col5 fw500 fs14 elippse2">Lorem ipsum dolor sit amet, consectet adipicing.</div>
                                                                           <div className="col30 fw600">$200</div>
                                                                      </div>
                                                                 </Col>
                                                                 <Col md={3}>
                                                                      <div className="PlanList1 bgCol44 p-3">
                                                                           <div className="text-center mb-3">
                                                                                <div className="circleType3">
                                                                                     <Image src={rocketDown} alt="Icon" className="mw30" />
                                                                                </div>
                                                                           </div>
                                                                           <div className="col2 fs30 fw500 mb-1 elippse1">Elite</div>
                                                                           <div className="mb-3 col5 fw500 fs14 elippse2">Lorem ipsum dolor sit amet, consectet adipicing.</div>
                                                                           <div className="col30 fw600">$350</div>
                                                                      </div>
                                                                 </Col>
                                                                 <Col md={3}>
                                                                      <div className="PlanList1 bgCol44 disabledOne p-3">
                                                                           <div className="text-center mb-3">
                                                                                <div className="circleType3">
                                                                                     <Image src={Speed} alt="Icon" className="mw30" />
                                                                                </div>
                                                                           </div>
                                                                           <div className="col2 fs30 fw500 mb-1 elippse1">Pro</div>
                                                                           <div className="mb-3 col5 fw500 fs14 elippse2">Lorem ipsum dolor sit amet, consectet adipicing.</div>
                                                                           <div className="col30 fw600">$2200</div>
                                                                      </div>
                                                                 </Col>
                                                            </Row>
                                                            
                                                            <Row>
                                                                 <Col md={12}>
                                                                      <div className="col2 fw600 d-flex pointer fs18 mb-2">
                                                                           Plan Selection Table
                                                                           <span className="openCard active">     
                                                                                <Image src={DownIcon} alt="Icon" className="ml-2" />
                                                                           </span>
                                                                      </div>
                                                                      <Table bordered className="tableType2 planTable br10 mb-4">
                                                                           <thead>
                                                                                <tr>
                                                                                     <th></th>
                                                                                     <th>Free</th>
                                                                                     <th>$200</th>
                                                                                     <th>$350</th>
                                                                                     <th>$2200</th>
                                                                                </tr>
                                                                           </thead>
                                                                           <tbody>
                                                                                <tr>
                                                                                     <td>15 Minutes session</td>
                                                                                     <td>
                                                                                          <Form.Group controlId="formBasicRadio9">
                                                                                               <Form.Check type="radio" aria-label="radio 9" className="checkboxTyp3" label="" />
                                                                                          </Form.Group>
                                                                                     </td>
                                                                                     <td>
                                                                                          <Form.Group controlId="formBasicRadio6">
                                                                                               <Form.Check type="radio" aria-label="radio 5" className="checkboxTyp3" label="" />
                                                                                          </Form.Group>
                                                                                     </td>
                                                                                     <td>
                                                                                          <Form.Group controlId="formBasicRadio7">
                                                                                               <Form.Check type="radio" aria-label="radio 7" className="checkboxTyp3" label="" />
                                                                                          </Form.Group>
                                                                                     </td>
                                                                                     <td>
                                                                                          <Form.Group controlId="formBasicRadio8">
                                                                                               <Form.Check type="radio" aria-label="radio 8" className="checkboxTyp3" label="" />
                                                                                          </Form.Group>
                                                                                     </td>

                                                                                </tr>

                                                                           </tbody>
                                                                      </Table>
                                                                 </Col>
                                                            </Row>

                                                            <Row> 
                                                                 <Col md={12}>
                                                                      <div className="fw600 fs22 col2 mb-3">Enter Card Details</div>  
                                                                 </Col>
                                                                 <Col md={6} className="pr20">      
                                                                      <div className="planBox1">
                                                                           <Row>
                                                                                <Col md={12}>   
                                                                                     <Form.Group className="mb-3" controlId="formBasicEmail">
                                                                                          <Form.Label className="col2">
                                                                                               Card Number
                                                                                          </Form.Label> 
                                                                                          <Form.Control type="email" placeholder="Enter email" className="inputType1" />
                                                                                     </Form.Group>   
                                                                                </Col>
                                                                                <Col md={6}>   
                                                                                     <Form.Group className="mb-3" controlId="formBasicEmail">
                                                                                          <Form.Label className="col2">
                                                                                               Expiry / Validity Date
                                                                                          </Form.Label> 
                                                                                          <Form.Control type="email" placeholder="Enter email" className="inputType1" />
                                                                                     </Form.Group>   
                                                                                </Col>
                                                                                <Col md={6}>   
                                                                                     <Form.Group className="mb-3" controlId="formBasicEmail">
                                                                                          <Form.Label className="d-flex justify-content-between col2">  
                                                                                               <span>CVV</span>  
                                                                                               <span className="col8">Help</span>  
                                                                                          </Form.Label> 
                                                                                          <Form.Control type="email" placeholder="Enter email" className="inputType1" />
                                                                                     </Form.Group>   
                                                                                </Col>
                                                                           </Row>
                                                                      </div>
                                                                 </Col>
                                                                 <Col md={6} className="pl20">  
                                                                      <div className="planBox2 bgCol44">
                                                                            <div className="d-flex mb-4">
                                                                                 <div className="circleType3">
                                                                                     <Image src={rocketUp} alt="Icon" className="mw30" />
                                                                                 </div>
                                                                                 <div>
                                                                                       <div className="fs30 fw600 col2 mb-0">Advance</div>
                                                                                       <div className="fs14 fw600 col2">Lorem ipsum dolor sit amet, construc consectetur adipiscing.
                                                                                       </div>
                                                                                 </div>
                                                                            </div>
                                                                            <ul className="planList mb-4"> 
                                                                                 <li>1 Hour session </li> 
                                                                                 <li>Pre counselling with HiEd Counselor </li>
                                                                                 <li>1-1 counselling with Preeti Tanwar </li>
                                                                                 <li>Basic Info Collection </li>
                                                                                 <li>Evaluate the case for admission </li>
                                                                                 <li>Personalized College Recommendations </li>
                                                                                 <li>Career Coaching</li> 
                                                                                 <li>Evaluate the case for full / partial scholarship & admission </li>
                                                                                 <li>Evaluate the case for CPT Internship with HiEd or HiEd clients </li>
                                                                                 <li>Essay support </li>
                                                                                 <li>Mapping for 100% guaranteed admission </li> 
                                                                            </ul>
                                                                            <div className="changePlan bgCol3 r5 d-flex justify-content-between">  
                                                                                 <div className="col30 fw600 fs18 pointer">Pay $200</div>
                                                                                 <div className="fw600 fs18 col8 pointer">Change Plan</div>
                                                                            </div>
                                                                      </div>
                                                                 </Col>
                                                            </Row>

                                                       </div>
                                                       <div className="footerBotton">
                                                            <div className="hrBorder pt-3"></div>
                                                            <div className="text-right d-flex flex-wrap justify-content-between  mt-3">
                                                                 <Button type="button" className="btnType7">
                                                                      Previous
                                                                 </Button>
                                                                 <Button type="button" className="btnType2">
                                                                      Save & Next
                                                                 </Button>
                                                            </div>
                                                       </div>
                                                  </Col>
                                             </div>

                                        </Tab.Pane>


                                   </Tab.Content>
                              </Col>
                         </Row>
                    </Tab.Container>

               </div>
          </div>
     )
}

export default Student;




