import React from 'react';
import { Col, Row, Container, Image, Form, Button } from 'react-bootstrap';
import Logos from '../../../../assets/images/logos.png';
import Facebook from '../../../../assets/images/svg/facebook.svg';
import Googles from '../../../../assets/images/svg/googles.svg';
import Linkedin from '../../../../assets/images/svg/linkedin.svg';      

const ForgotPassword = () => {
     return (
          <div className="bgBanner d-flex align-items-center justify-content-center w-100">
               <Container>
                    <Col lg={10} md={10} className="m-auto">
                         <div className="studentBox text-center bgCol3 shadow1 br10 loginUsers">
                              <Image src={Logos} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" />
                              <div className="studentOne mt-4">   
                                   <div className="fs22 fw600 col2 mb-1">Forgot Password</div>
                                   <div className="fw500 col5 mb-4">Please enter the email address and we’ll send the 
                                   <br /> reset password link</div> 
                                   <Form className="formLayoutUi">  
                                        <Row className="mb-3"> 
                                             <Col md={6} className="m-auto"> 
                                                  <Form.Group controlId="formBasicEmail"> 
                                                       <Form.Control type="email" placeholder="User ID" className="inputType1" />
                                                  </Form.Group>
                                             </Col> 
                                        </Row>  
                                        <div className="text-center">   
                                             <Button className="btnType4 fw600 mb-4">Submit</Button>
                                             <div className="fw700 col1 pointer pt-1">
                                                  Back to Sign In   
                                             </div>
                                        </div>      
                                   </Form>
                              </div>
                         </div>
                    </Col>
               </Container>
          </div>
     )
}

export default ForgotPassword; 