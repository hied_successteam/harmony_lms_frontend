import React from 'react';
import { Col, Row, Container, Image, Form, Button, Dropdown } from 'react-bootstrap';
import Logos from '../../../assets/images/logos.png';
import footerLogo from '../../../assets/images/footerlogo.png';
import Bookmark from '../../../assets/images/svg/BookmarkSimple.svg';
import SearchIcon from '../../../assets/images/svg/MagnifyingGlass.svg';
import Logoadmin from '../../../assets/images/svg/logoadmin.svg';
import bellIcon from '../../../assets/images/svg/Bell.svg';
import UserThree from '../../../assets/images/user3.png';  
import IntlTelInput from 'react-intl-tel-input';

const Payment = () => {
     return (
          <div className="bgBanner w-100 payMent">
               <div className="headerTwo">
                    <div className="topHeaderBox bgCol25 pl-3 pr-3">
                         <Container fluid>
                              <Row>
                                   <Col md={6}>
                                        <Image src={Logoadmin} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" />
                                        <span className="fs16 col24 fw700 ml-1">HiEd Harmony</span>
                                   </Col>
                                   <Col md={6}>
                                        <div className="notifications d-flex justify-content-end">  
                                             <div>
                                                  <Dropdown>
                                                       <Dropdown.Toggle id="dropdown-basic" className="DropdownType1">
                                                            <Image src={bellIcon} alt="Search" className="pointer" />
                                                            {/* <span className="fs10 d-inline-block bgCol27 br50 text-center">2</span> */}
                                                       </Dropdown.Toggle>  
                                                       <Dropdown.Menu>
                                                            <Dropdown.Item href="#">Action</Dropdown.Item>
                                                            <Dropdown.Item href="#">Another action</Dropdown.Item>
                                                            <Dropdown.Item href="#">Something else</Dropdown.Item>
                                                       </Dropdown.Menu>
                                                  </Dropdown>
                                             </div>
                                             <div className="bookMark ml-3"> 
                                                  <span className="d-inline-block br50 text-center">
                                                     <Image src={UserThree} alt="Search" className="pointer user1 mr-1" />
                                                  </span>
                                             </div>
                                             <div className="SettingType1 ml-3 d-flex align-items-center">  
                                                  <Dropdown>
                                                       <Dropdown.Toggle id="dropdown-basic" className="DropdownType1 col2 fs14 fw600">
                                                          <i className="fa fa-chevron-down col1" aria-hidden="true"></i>
                                                       </Dropdown.Toggle>
                                                       <Dropdown.Menu>
                                                            <Dropdown.Item>Edit Profile</Dropdown.Item>
                                                            <Dropdown.Item>Setting</Dropdown.Item>
                                                            <Dropdown.Item>Help</Dropdown.Item>
                                                            <Dropdown.Item>Logout</Dropdown.Item>
                                                       </Dropdown.Menu>
                                                  </Dropdown> 
                                             </div>
                                        </div>
                                   </Col>
                              </Row>
                         </Container>  
                    </div>  
               </div>
               <Container className="d-flex align-items-center justify-content-center mt-5 mb-5">
                    <Col lg={10} md={10} className="m-auto">
                         <div className="studentBox text-center bgCol3 shadow1 br10 loginUsers">
                              {/* <Image src={Logos} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" />   */}
                              <div className="studentOne">
                                   <div className="fs20 fw600 col2 mb-3">Pay for Other HiEd Services</div>
                                   <Form className="formLayoutUi">
                                        <Row>
                                             <Col md={12}>
                                                  <div className="col4 fw600 fs18 text-left mb-2">Select HiEd services you would like to opt?</div>
                                                  <div className="tagType1 d-flex flex-wrap">
                                                       <span className="br8 col4 fs14 fw500 mr-3">Onboarding Assessment</span>
                                                       <span className="br8 col4 fs14 fw500 mr-3">Match Making</span>
                                                       <span className="br8 col4 fs14 fw500 mr-3">Exam Preparation</span>
                                                       <span className="br8 col4 fs14 fw500 mr-3">Take Exam</span>
                                                       <span className="bgCol35 fs14 br8 col4 fw500 mr-3">Essay Writing</span>
                                                       <span className="br8 col4 fs14 fw500 mr-3">Admission Application Submission</span>
                                                       <span className="bgCol35 fs14 br8 col4 fw500 mr-3">Pay Registration Fees</span>
                                                       <span className="bgCol35 fs14 br8 col4 fw500 mr-3">Get Admitted</span>
                                                       <span className="br8 col4 fs14 fw500 mr-3">Give USCIS Visa Interview</span>
                                                       <span className="bgCol35 fs14 br8 col4 fw500 mr-3">Attend U.s. University</span>
                                                  </div>
                                             </Col>

                                             <Col md={6}>
                                                  <div className="col4 fw600 fs16 text-left mb-2">Enter Amount to Pay</div>
                                                  <Form.Group controlId="formBasicEmail">
                                                       <Form.Label className="col2 fw400 fs14 mb-1">Amount
                                                       </Form.Label>
                                                       <div className="input-group mb-3">
                                                            <div className="input-group-prepend inputGroup1">
                                                                 <span className="input-group-text" id="basic-addon1">$</span>
                                                            </div>
                                                            <Form.Control type="text" placeholder="0000" className="inputType3" />
                                                       </div>
                                                  </Form.Group>
                                             </Col>
                                             <Col md={12}>
                                                  <div className="col4 fw600 fs16 text-left mb-2">Enter Card Details
                                                  </div>
                                             </Col>
                                        </Row>

                                        <Row>
                                             <Col md={6}>
                                                  <Form.Group controlId="formBasicEmail">
                                                       <Form.Label className="col2 fw400 fs14 mb-1">Name</Form.Label>
                                                       <Form.Control type="text" placeholder="Name on the card" className="inputType1" />
                                                  </Form.Group>
                                             </Col>
                                             <Col md={6}>
                                                  <Form.Group controlId="formBasicEmail">
                                                       <Form.Label className="col2 fw400 fs14 mb-1">Email</Form.Label>
                                                       <Form.Control type="email" placeholder="Email Address" className="inputType1" />
                                                  </Form.Group>
                                             </Col>
                                             <Col md={6}>
                                                  <Form.Group controlId="formBasicEmailE" className="mb-4 flagSelect">
                                                       <Form.Label className="col2 fw400 fs14 mb-1">Phone Number</Form.Label>
                                                       <IntlTelInput
                                                            containerClassName="intl-tel-input"
                                                            inputClassName="form-control inputType1"
                                                            placeholder="Contact Number"

                                                       />
                                                  </Form.Group>
                                             </Col>
                                             <Col md={6}>
                                                  <Form.Group controlId="formBasicEmail">
                                                       <Form.Label className="col2 fw400 fs14 mb-1">Card Number</Form.Label>
                                                       <Form.Control type="text" placeholder="Card Number" className="inputType1" />
                                                  </Form.Group>
                                             </Col>
                                             <Col md={12}>
                                                  <div className="text-center d-flex justify-content-center align-items-center flex-wrap align-xs-center">
                                                       <Button className="btnType4 fw600 mr-4">Sign In</Button>
                                                  </div>
                                             </Col>
                                        </Row>

                                   </Form>
                              </div>
                         </div>
                    </Col>
               </Container>

               <div className="bottomFooter footerTwo d-flex align-items-center bgCol43 pl-3 pr-3">
                    <div className="container-fluid">
                         <Row>
                              <Col md={12}>
                                   <div className="d-flex align-items-center">
                                        <Image src={footerLogo} alt="footer-logo" className="fLogo" />
                                        <div className="col42 fs13 ml-4">Copyright © {new Date().getFullYear()} HiEd Success | Powered by HiEd Success</div>
                                   </div>
                              </Col>
                         </Row>
                    </div>
               </div>
          </div>
     )
}

export default Payment;   