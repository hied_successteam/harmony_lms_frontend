import React from 'react';
import { Col, Row, Container, Image, Form, Dropdown, Accordion, Card, Button, Table } from 'react-bootstrap';
import Logoadmin from '../../../assets/images/svg/logoadmin.svg';
import SearchIcon from '../../../assets/images/svg/MagnifyingGlass.svg';
import Bookmark from '../../../assets/images/svg/BookmarkSimple.svg';
import userTwo from '../../../assets/images/user2.png';
import StarEmpty from '../../../assets/images/svg/starempty.svg';
import BellOne from '../../../assets/images/svg/Bell.svg';
import mapOne from '../../../assets/images/map1.png';
import { Slider, RangeSlider } from 'rsuite';
import rocketUp from "../../../assets/images/svg/rocket-up.svg";
import DownOne from "../../../assets/images/svg/downOne.svg";
import ProImg from "../../../assets/images/proImg.png"; 



const MyProfile = () => {
     return (
          <div className="collegeAdminUi">
               <Container fluid>
                    <Row>
                         <Col md={8} className="topHeaderBox bgCol3 pl-0 pr-0">

                                   
                                   <div className="pl-4 pr-4 pt-4">
                                        <Row className="mb-4">
                                             <Col md={3}>
                                                  <Image src={Logoadmin} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" />
                                                  <span className="fs20 col24 fw700 ml-1">HiEd Harmony</span>
                                             </Col>
                                             <Col md={6}>
                                                  <div className="searchBox d-flex justify-content-center">
                                                       <Form.Group className="position-relative w-100">
                                                            <Form.Control type="text" placeholder="Search..." className="searchType1 shadow3 w-100" />
                                                            <Image src={SearchIcon} alt="Search" className="pointer searchOne" />
                                                       </Form.Group>
                                                  </div>
                                             </Col>
                                             <Col md={3}>
                                                  <div className="notifications d-flex justify-content-end">
                                                       <div className="bookMark ml-4">
                                                            <span className="d-inline-block mr-3 text-center position-relative">
                                                                 <Image src={BellOne} alt="Bell" className="bell1 pointer" />
                                                                 <span className="bugdeTwo bgCol27 br50 fs10 col3 fw500">2</span>
                                                            </span>
                                                            <span className="d-inline-block bgCol1 br50 text-center">
                                                                 <Image src={Bookmark} alt="BookMark" className="pointer" />
                                                            </span>
                                                       </div>
                                                  </div>
                                             </Col>
                                        </Row>
                                        <div className="middileAccordian mb-5">
                                             <div className="myProfile">
                                                  <div className="fs20 fw500 col2 mb-4">My Profile</div>
                                                  <div className="stepTitle fw500 fs30 col4 position-relative mb-4 pb-1">Basic</div>

                                                  <Row className="mb-4">
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Name of Student</div>
                                                                 <div className="col2 fw500 fs15">Shanaya Fernandes</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Email ID</div>
                                                                 <div className="col2 fw500 fs15">shanayafernandes@gmail.com</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Contact Number</div>
                                                                 <div className="col2 fw500 fs15">(704) 555-0127</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Date of Birth (DD/MM/YYYY)</div>
                                                                 <div className="col2 fw500 fs15">03/12/2001</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Country</div>
                                                                 <div className="col2 fw500 fs15">USA</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">State</div>
                                                                 <div className="col2 fw500 fs15">Florida</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">City</div>
                                                                 <div className="col2 fw500 fs15">Miami</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Zipcode</div>
                                                                 <div className="col2 fw500 fs15">452007</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Religious affiliation</div>
                                                                 <div className="col2 fw500 fs15">Catholics</div>
                                                            </div>
                                                       </Col>

                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Visa Status</div>
                                                                 <div className="col2 fw500 fs15">Contact the National Visa Center (NVC)</div>
                                                            </div>
                                                       </Col>
                                                  </Row>

                                                  <div className="stepTitle fw500 fs30 col4 position-relative mb-4 pb-1">Academic Info</div>
                                                  <Row className="mb-4">
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">What is your current progress in finding the suitable course or college ?</div>
                                                                 <div className="col2 fw500 fs15">Seeking out info. about College/Course</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Which batch (fall/spring) would you like to apply for?</div>
                                                                 <div className="col2 fw500 fs15">Spring 2022</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">What is the highest degree or the level of education you have completed? (If you are currently enrolled in school/college, indicate the highest degree you have  achieved)</div>
                                                                 <div className="col2 fw500 fs15">Less than a high school diploma</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Which entrance exams have you appeared recently?  (Select all that you have appeared)</div>
                                                                 <div className="col2 fw500 fs15">SAT, TOEFL</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Which educational degree would you like to pursue next?</div>
                                                                 <div className="col2 fw500 fs15">Professional Certificate</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Which field of study would you like to pursue?</div>
                                                                 <div className="col2 fw500 fs15">Computer & information sciences, Parks, recreation, leisure, & fitness studies, Engineering</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Write a short essay describing your passion for the field of study and how you will contribute to the college community.</div>
                                                                 <div className="col2 fw500 fs15">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vitae id eget sem ornare. At nisi vel a, nullam et. Ipsum scelerisque sed nunc, ut accumsan, molestie at ac volutpat.</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">How strong are your letters of recommendation?</div>
                                                                 <div className="col2 fw500 fs15">Very Strong</div>
                                                            </div>
                                                       </Col>
                                                  </Row>

                                                  <div className="stepTitle fw500 fs30 col4 position-relative mb-4 pb-1">College Preferences</div>
                                                  <Row className="mb-4">
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Select top priorities when considering admission in a particular college (Choose 1 or more)</div>
                                                                 <div className="col2 fw500 fs15">First generation grants, Career guidance</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Which entrance exams have you appeared recently?  (Select all that you have appeared)</div>
                                                                 <div className="col2 fw500 fs15">Top100,  Top50</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Any specific target colleges you would like to mention</div>
                                                                 <div className="col2 fw500 fs15">Northwestern Oklahoma State University</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Preferred college size (student population, Choose 1 or more)</div>
                                                                 <div className="col2 fw500 fs15">5,000 - 9,999, 20,000 and above</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Which mode of education is preferred for you?</div>
                                                                 <div className="col2 fw500 fs15">Online</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Which college clubs would you be most interested in joining? (Choose 1 or more)</div>
                                                                 <div className="col2 fw500 fs15">Academic (math, literature, language, history), Media & publication</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Which of the following sports facilities would you like to use? (Choose 1 or more)</div>
                                                                 <div className="col2 fw500 fs15">Football</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Please share your preference of college (dorm) roommates</div>
                                                                 <div className="col2 fw500 fs15">
                                                                      24-hour emergency helpline
                                                                 </div>
                                                            </div>
                                                       </Col>
                                                  </Row>

                                                  <div className="stepTitle fw500 fs30 col4 position-relative mb-4 pb-1">Demographic Preferences</div>
                                                  <Row className="mb-4">
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Will  you be the first generation student graduating ?</div>
                                                                 <div className="col2 fw500 fs15">Yes</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">
                                                                      What is your ethnicity?
                                                                 </div>
                                                                 <div className="col2 fw500 fs15">American Indian or Alaska Native</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Do you prefer colleges which follow a Specialized Mission?</div>
                                                                 <div className="col2 fw500 fs15">Yes, Single-sex: Men</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Do you have any seasonal allergy?(E.g. Pollen)</div>
                                                                 <div className="col2 fw500 fs15">Yes</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="col2 fw600 fs22 mb-4">Climate Preferences</div>
                                                       </Col>
                                                       <Col md={6}></Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">What are your preferred weather conditions (Choose 1 or more,  Approx. Temperature)</div>
                                                                 <div className="col2 fw500 fs15">Moderate (15C to 35C / 59F to 59F)</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Please share your preference of college (dorm) roommates</div>
                                                                 <div className="col2 fw500 fs15">24-hour emergency helpline</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Which of the following sports facilities would you like to use? (Choose 1 or more)</div>
                                                                 <div className="col2 fw500 fs15">Football</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}></Col>
                                                       <Col md={6}><div className="col2 fw600 fs22 mb-4">Location Preferences</div></Col>
                                                       <Col md={6}></Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Choose preferred college setting</div>
                                                                 <div className="col2 fw500 fs15">City</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Do you prefer colleges that are closer to home? If yes, please select the preferred proximity from home (miles)</div>
                                                                 <div className="col2 fw500 fs15">10 miles</div>
                                                            </div>
                                                       </Col>
                                                  </Row>

                                                  <div className="stepTitle fw500 fs30 col4 position-relative mb-4 pb-1">Employment Info</div>
                                                  <Row className="mb-4">
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Which of the following personality traits do you possess or have experienced?</div>
                                                                 <div className="col2 fw500 fs15">Effective decision making</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Share an experience or story where you have utilized or demonstrated the personality traits</div>
                                                                 <div className="col2 fw500 fs15">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean aliquam ullamcorper mauris risus mi ut morbi nulla. Consectetur egestas habitasse elementum nullam sed enim.</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">How many hours have you dedicated towards community service or volunteer work in the last 3 years?</div>
                                                                 <div className="col2 fw500 fs15">More than 50 hours</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">What volunteering activities did you participate in? List down those activities.</div>
                                                                 <div className="col2 fw500 fs15">Basketball</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">What is your current employment status?</div>
                                                                 <div className="col2 fw500 fs15">Employed Full - time(40+ hours a week)</div>
                                                            </div>
                                                       </Col>

                                                  </Row>

                                                  <div className="stepTitle fw500 fs30 col4 position-relative mb-4 pb-1">Placement Preferences</div>
                                                  <Row className="mb-4">
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Is it required for the institute to have partnerships with certain companies for training/job opportunities?</div>
                                                                 <div className="col2 fw500 fs15">Yes</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Is it required for the institute to provide job assistance or employment opportunities for graduating students?</div>
                                                                 <div className="col2 fw500 fs15">Yes</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">How many hours have you dedicated towards community service or volunteer work in the last 3 years?</div>
                                                                 <div className="col2 fw500 fs15">More than 50 hours</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Is it important for the institute to offer Optional Practical Training (OPT) or Curricular Practical Training (CPT) </div>
                                                                 <div className="col2 fw500 fs15">Yes</div>
                                                            </div>
                                                       </Col>

                                                  </Row>

                                                  <div className="stepTitle fw500 fs30 col4 position-relative mb-4 pb-1">Financial Preferences</div>
                                                  <Row className="mb-4">
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">What is your Current Income ? </div>
                                                                 <div className="col2 fw500 fs15">Yes</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">What is the parents/household investment for  higher education?</div>
                                                                 <div className="col2 fw500 fs15">$30,000 - $40,000</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Do you need any kind of financial assistance/aid? (Select the suitable option one or more)</div>
                                                                 <div className="col2 fw500 fs15">Federal student aid, Aid for military families</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">What is your estimated cost of education or net price </div>
                                                                 <div className="col2 fw500 fs15">Min $15000</div>
                                                            </div>
                                                       </Col>
                                                  </Row>

                                                  <div className="col21 fw500 fs15 mb-3">How important are the following factors to you in college selection? </div>

                                                  <Row>
                                                       <Col md={12}>
                                                            <Table bordered className="tableType2 myProfileTable planTable br10 mb-4">
                                                                 <thead>
                                                                      <tr>
                                                                           <th></th>
                                                                           <th>Not at all important</th>
                                                                           <th>Low importance</th>
                                                                           <th>Slightly important</th>
                                                                           <th>Neutral</th>
                                                                           <th>Moderately important</th>
                                                                           <th>Very important</th>
                                                                           <th>Extremely important</th>
                                                                      </tr>
                                                                 </thead>
                                                                 <tbody>
                                                                      <tr>
                                                                           <td><span className="fw600">
                                                                                Demographics (ehtnicity, gender)</span></td>
                                                                           <td>
                                                                                <Form.Group controlId="formBasicRadio9">
                                                                                     <Form.Check type="radio" aria-label="radio 9" className="checkboxTyp3" label="" />
                                                                                </Form.Group>
                                                                           </td>
                                                                           <td>
                                                                                <Form.Group controlId="formBasicRadio6">
                                                                                     <Form.Check type="radio" aria-label="radio 5" className="checkboxTyp3" label="" />
                                                                                </Form.Group>
                                                                           </td>
                                                                           <td>
                                                                                <Form.Group controlId="formBasicRadio7">
                                                                                     <Form.Check type="radio" aria-label="radio 7" className="checkboxTyp3" label="" />
                                                                                </Form.Group>
                                                                           </td>
                                                                           <td>
                                                                                <Form.Group controlId="formBasicRadio8">
                                                                                     <Form.Check type="radio" aria-label="radio 8" className="checkboxTyp3" label="" />
                                                                                </Form.Group>
                                                                           </td>
                                                                      </tr>
                                                                 </tbody>
                                                            </Table>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="fw500 col21 mb-2">
                                                                 Would you like to add extra information that is critical to your Institution search
                                                            </div>
                                                            <div className="fw500 col2 mb-4 pb-3">
                                                                 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sollicitudin enim tellus turpis eget. Lacus eros, posuere ante scelerisque et, vel duis at.
                                                            </div>
                                                       </Col>
                                                  </Row>
                                                  <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1 pr-3">  
                                                       <div>Plan</div>
                                                       <div>
                                                            <Image src={DownOne} alt="Icon" className="pointer rotateArrow" /> 
                                                       </div>
                                                  </div>
                                                  <Row>
                                                       <Col md={5}>
                                                            <div className="PlanList1 planList2 active w-100 bgCol44 p-3">
                                                                 <Row className="align-items-center">   
                                                                      <Col md={3}>
                                                                           <div className="text-center">
                                                                                <div className="circleType3">
                                                                                     <Image src={rocketUp} alt="Icon" className="mw30" />
                                                                                </div>
                                                                           </div>
                                                                      </Col>
                                                                      <Col md={6}>
                                                                           <div className="mt-2"> 
                                                                                <div className="col2 fs30 fw500 mb-1 elippse1">demo</div>
                                                                                <div className="mb-3 col5 fw500 fs14 elippse2">lorem ipsum</div>
                                                                           </div>
                                                                      </Col>
                                                                      <Col md={3} className="text-right"> 
                                                                           <div className="col30 fw600">$300</div>
                                                                      </Col>
                                                                 </Row>
                                                            </div>
                                                       </Col>
                                                  </Row>
                                             </div>
                                        </div>
                                   </div>
                              
                         
                         </Col>

                         <Col md={4} className="topHeaderBox bgCol28 pl-0 pr-0">
                              <div className="rightSidebar fixedSidebar pl-4 pr-4 pt-4 mb-5">
                                   <div className="d-flex justify-content-start mb-3">
                                        <Row className="w-100">
                                             <Col md={3} className="pr-0">
                                                  <Image src={userTwo} alt="User" />
                                             </Col>
                                             <Col md={9}>
                                                  <div className="col2 fw600 fs20 mt-1 mb-1">Kate Wilson</div>
                                                  <div className="col5 fw500">kate.wilson20@gmail.com</div>
                                                  <div className="col5 fw500 mb-3">Alva, Oklahoma</div>
                                             </Col>
                                        </Row>
                                   </div>

                                   <div className="profileBox1 bgCol3 p-4 mb-4">      
                                        <Image src={ProImg} alt="Profile" className="w-100 mb-4" />     
                                        <div className="col2 fs20 fw600 mb-4">
                                             If you would like to edit your profile, contact us at</div>
                                        <div className="col8 fs20 fw600">info@hiedsuccess.com</div>
                                   </div>

                              </div>
                         </Col>
                    </Row>
               </Container>
          </div>
     )
}
export default MyProfile;        






