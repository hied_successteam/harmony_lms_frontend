import React, { useState } from 'react';
import { Container, Col, Row, Image, Button, Pagination, Dropdown, Form, Nav, Tab, Modal, Accordion, Card, Table } from 'react-bootstrap';
import Logoadmin from '../../../assets/images/svg/logoadmin.svg';
import SearchIcon from '../../../assets/images/svg/MagnifyingGlass.svg';
import footerLogo from '../../../assets/images/footerlogo.png';  
import { Slider, RangeSlider } from 'rsuite';
import 'rsuite/dist/styles/rsuite-default.css';

const UserDetail = () => {
     return (
          <div className="wizardLayout userDashboard">
               <div className="container-fluid pl-0">
                    <Row>
                         <Col sm={3}>
                              <div className="sidebarBox">
                                   <div className="pl-4 pt-4 mt-2 mb-5 d-flex align-items-center">
                                        <Image src={Logoadmin} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" />
                                        <span className="fs18 col3 fw600 ml-2">HiEd Harmony</span>
                                   </div>
                                   <Nav variant="pills" className="flex-column innerBox">
                                        <Nav.Item>
                                             <Nav.Link className="listBox active">
                                                  <i className="fa fa-user-o" aria-hidden="true">
                                                  </i><span className="titles text-uppercase fs15 d-block">Student</span>
                                             </Nav.Link>
                                        </Nav.Item>
                                        <Nav.Item>
                                             <Nav.Link className="listBox">
                                                  <i className="fa fa-university" aria-hidden="true"></i><span className="titles text-uppercase fs15 d-block">College</span>
                                             </Nav.Link>
                                        </Nav.Item>
                                        <Nav.Item>
                                             <Nav.Link className="listBox">
                                                  <i className="fa fa-sign-out" aria-hidden="true"></i><span className="titles text-uppercase fs15 d-block">Logout</span>
                                             </Nav.Link>
                                        </Nav.Item>
                                   </Nav>
                              </div>
                         </Col>

                         <Col sm={9}>
                              <div className="userrightBox pr-4">
                                   <div className="rightUser">
                                        <div className="d-flex justify-content-end mt-4">
                                             <Dropdown className="DropdownType2">
                                                  <Dropdown.Toggle id="dropdown-basic" className="DropdownType1 col2 fs14 fw600">
                                                       Jone Aly   <span className="circleType3 d-inline-block br50 text-center"><i className="fa fa-chevron-down col1" aria-hidden="true"></i> </span>
                                                  </Dropdown.Toggle>
                                                  <Dropdown.Menu>
                                                       <Dropdown.Item href="#">Profile</Dropdown.Item>
                                                       <Dropdown.Item href="#">Setting</Dropdown.Item>
                                                       <Dropdown.Item href="#">Logout</Dropdown.Item>
                                                  </Dropdown.Menu>
                                             </Dropdown>
                                        </div>

                                        <div className="filterType1 mt-3">
                                             <Accordion>
                                                  <Card className="shadow4">
                                                       <Card.Header>
                                                            <Accordion.Toggle variant="link" eventKey="0">
                                                                 Filters <i className="fa fa-chevron-down arrowA" aria-hidden="true"></i>
                                                            </Accordion.Toggle>
                                                       </Card.Header>
                                                       <Accordion.Collapse eventKey="0">
                                                            <Card.Body className="bgCol3">
                                                                 <div>
                                                                      <Row>
                                                                           <Col md={3}>
                                                                                <Form.Group controlId="exampleOne" className="mb-4">
                                                                                     <Form.Control as="select" className="selectTyp1 select2 pointer">
                                                                                          <option>In State</option>
                                                                                          <option>UP</option>
                                                                                          <option>MP</option>
                                                                                          <option>MH</option>
                                                                                          <option>RJ</option>
                                                                                     </Form.Control>
                                                                                </Form.Group>
                                                                           </Col>
                                                                           <Col md={3}>
                                                                                <Form.Group controlId="exampleOne" className="mb-4">
                                                                                     <Form.Control as="select" className="selectTyp1 select2 pointer">
                                                                                          <option>Term</option>
                                                                                          <option>Fall 2021</option>
                                                                                          <option>Spring 2022</option>
                                                                                          <option>Fall 2022</option>
                                                                                     </Form.Control>
                                                                                </Form.Group>
                                                                           </Col>
                                                                           <Col md={3}>
                                                                                <Form.Group controlId="exampleOne" className="mb-4">
                                                                                     <Form.Control as="select" className="selectTyp1 select2 pointer">
                                                                                          <option>Bachelors</option>
                                                                                          <option>1</option>
                                                                                          <option>2</option>
                                                                                          <option>3</option>
                                                                                          <option>4</option>
                                                                                     </Form.Control>
                                                                                </Form.Group>
                                                                           </Col>
                                                                           <Col md={3}>
                                                                                <Form.Group controlId="exampleOne" className="mb-4">
                                                                                     <Form.Control as="select" className="selectTyp1 select2 pointer">
                                                                                          <option>Major</option>
                                                                                          <option>1</option>
                                                                                          <option>2</option>
                                                                                          <option>3</option>
                                                                                          <option>4</option>
                                                                                     </Form.Control>
                                                                                </Form.Group>
                                                                           </Col>
                                                                           <Col md={3}>
                                                                                <Form.Group controlId="exampleOne" className="mb-4">
                                                                                     <Form.Control as="select" className="selectTyp1 select2 pointer">
                                                                                          <option>Applicant Type</option>
                                                                                          <option>App</option>
                                                                                          <option>App</option>
                                                                                          <option>App</option>
                                                                                          <option>App</option>
                                                                                     </Form.Control>
                                                                                </Form.Group>
                                                                           </Col>
                                                                           <Col md={3}>
                                                                                <Form.Group controlId="exampleOne" className="mb-4">
                                                                                     <Form.Control as="select" className="selectTyp1 select2 pointer">
                                                                                          <option>Entrance Exams</option>
                                                                                          <option>1</option>
                                                                                          <option>2</option>
                                                                                          <option>3</option>
                                                                                          <option>4</option>
                                                                                     </Form.Control>
                                                                                </Form.Group>
                                                                           </Col>
                                                                      </Row>
                                                                      <div className="mt-2 d-flex">
                                                                           <div className="rangeSlider w-100">
                                                                                <Row className="justify-content-center">
                                                                                     <Col md={4}>
                                                                                          <Form.Group controlId="formBasicRange">
                                                                                               <Form.Label className="fw600 col2 mb-3">
                                                                                                    <span className="mr-3">SAT</span> 400+ to 1300+
                                                                                           </Form.Label>
                                                                                               <RangeSlider defaultValue={[0, 60]} />
                                                                                          </Form.Group>
                                                                                     </Col>
                                                                                     <Col md={4}>
                                                                                          <Form.Group controlId="formBasicRange">
                                                                                               <Form.Label className="fw600 col2 mb-3">
                                                                                                    <span className="mr-3">ACT</span> 22+ to 28+
                                                                                          </Form.Label>
                                                                                               <RangeSlider defaultValue={[0, 60]} />
                                                                                          </Form.Group>
                                                                                     </Col>
                                                                                </Row>
                                                                           </div>
                                                                      </div>
                                                                 </div>
                                                            </Card.Body>
                                                       </Accordion.Collapse>
                                                  </Card>
                                             </Accordion>
                                        </div>

                                   </div>
                                   <Row>
                                        <Col md={5}>
                                             <div className="rightBox pt-4 pb-2 pl-2">
                                                  <div className="searchBox d-flex justify-content-center">
                                                       <Form.Group className="position-relative w-100">
                                                            <Form.Control type="text" placeholder="Search..." className="searchType1 shadow3 w-100" />
                                                            <Image src={SearchIcon} alt="Search" className="pointer searchOne" />
                                                       </Form.Group>
                                                  </div>
                                             </div>
                                        </Col>
                                   </Row>
                                   <Row>
                                        <Col md={12}>
                                             {/* start from here  */}
                                             <div className="table-responsive pl-2">
                                                  <Table bordered className="studentTable br10 mb-4">
                                                       <thead>
                                                            <tr>
                                                                 <th>FirstName</th>
                                                                 <th>Last Name</th>
                                                                 <th>Mobile Number</th>
                                                                 <th>Email</th>
                                                                 <th>City</th>
                                                                 <th>State</th>
                                                                 <th>Country Plan</th>
                                                                 <th>UserType</th>
                                                                 <th>InstituteId</th>
                                                                 <th>InstituteName</th>
                                                                 <th>CreatedAt</th>
                                                                 <th>IsProfileCompleted</th>
                                                                 <th>ProfileStepCompleted</th>
                                                                 <th>Status</th>
                                                                 <th>UpdatedAt</th>
                                                                 <th>ZipCode</th>
                                                            </tr>
                                                       </thead>
                                                       <tbody>

                                                            <tr>
                                                                 <td>
                                                                      John
                                                                 </td>
                                                                 <td>
                                                                      Deo
                                                                 </td>
                                                                 <td>
                                                                      8745968574
                                                                 </td>
                                                                 <td>
                                                                      john@gmail.com
                                                                 </td>
                                                                 <td>
                                                                      Indore
                                                                 </td>
                                                                 <td>
                                                                      MP
                                                                 </td>
                                                                 <td>India</td>
                                                                 <td>1</td>
                                                                 <td>1234567</td>
                                                                 <td>RGPV</td>
                                                                 <td>User1</td>
                                                                 <td>Done</td>
                                                                 <td>Pending</td>
                                                                 <td>Pending</td>
                                                                 <td>1</td>
                                                                 <td>452980</td>
                                                            </tr>


                                                            <tr>
                                                                 <td>
                                                                      John
                                                                                </td>
                                                                 <td>
                                                                      Deo
                                                                                </td>
                                                                 <td>
                                                                      8745968574
                                                                                </td>
                                                                 <td>
                                                                      john@gmail.com
                                                                                </td>
                                                                 <td>
                                                                      Indore
                                                                                </td>
                                                                 <td>
                                                                      MP
                                                                                </td>
                                                                 <td>India</td>
                                                                 <td>1</td>
                                                                 <td>1234567</td>
                                                                 <td>RGPV</td>
                                                                 <td>User1</td>
                                                                 <td>Done</td>
                                                                 <td>Pending</td>
                                                                 <td>Pending</td>
                                                                 <td>1</td>
                                                                 <td>452980</td>
                                                            </tr>


                                                            <tr>
                                                                 <td>
                                                                      John
                                                                                </td>
                                                                 <td>
                                                                      Deo
                                                                                </td>
                                                                 <td>
                                                                      8745968574
                                                                                </td>
                                                                 <td>
                                                                      john@gmail.com
                                                                                </td>
                                                                 <td>
                                                                      Indore
                                                                                </td>
                                                                 <td>
                                                                      MP
                                                                                </td>
                                                                 <td>India</td>
                                                                 <td>1</td>
                                                                 <td>1234567</td>
                                                                 <td>RGPV</td>
                                                                 <td>User1</td>
                                                                 <td>Done</td>
                                                                 <td>Pending</td>
                                                                 <td>Pending</td>
                                                                 <td>1</td>
                                                                 <td>452980</td>
                                                            </tr>


                                                            <tr>
                                                                 <td>
                                                                      John
                                                                                </td>
                                                                 <td>
                                                                      Deo
                                                                                </td>
                                                                 <td>
                                                                      8745968574
                                                                                </td>
                                                                 <td>
                                                                      john@gmail.com
                                                                                </td>
                                                                 <td>
                                                                      Indore
                                                                                </td>
                                                                 <td>
                                                                      MP
                                                                                </td>
                                                                 <td>India</td>
                                                                 <td>1</td>
                                                                 <td>1234567</td>
                                                                 <td>RGPV</td>
                                                                 <td>User1</td>
                                                                 <td>Done</td>
                                                                 <td>Pending</td>
                                                                 <td>Pending</td>
                                                                 <td>1</td>
                                                                 <td>452980</td>
                                                            </tr>


                                                            <tr>
                                                                 <td>
                                                                      John
                                                                                </td>
                                                                 <td>
                                                                      Deo
                                                                                </td>
                                                                 <td>
                                                                      8745968574
                                                                                </td>
                                                                 <td>
                                                                      john@gmail.com
                                                                                </td>
                                                                 <td>
                                                                      Indore
                                                                                </td>
                                                                 <td>
                                                                      MP
                                                                                </td>
                                                                 <td>India</td>
                                                                 <td>1</td>
                                                                 <td>1234567</td>
                                                                 <td>RGPV</td>
                                                                 <td>User1</td>
                                                                 <td>Done</td>
                                                                 <td>Pending</td>
                                                                 <td>Pending</td>
                                                                 <td>1</td>
                                                                 <td>452980</td>
                                                            </tr>

                                                            <tr>
                                                                 <td>
                                                                      John
                                                                                </td>
                                                                 <td>
                                                                      Deo
                                                                                </td>
                                                                 <td>
                                                                      8745968574
                                                                                </td>
                                                                 <td>
                                                                      john@gmail.com
                                                                                </td>
                                                                 <td>
                                                                      Indore
                                                                                </td>
                                                                 <td>
                                                                      MP
                                                                                </td>
                                                                 <td>India</td>
                                                                 <td>1</td>
                                                                 <td>1234567</td>
                                                                 <td>RGPV</td>
                                                                 <td>User1</td>
                                                                 <td>Done</td>
                                                                 <td>Pending</td>
                                                                 <td>Pending</td>
                                                                 <td>1</td>
                                                                 <td>452980</td>
                                                            </tr>

                                                            <tr>
                                                                 <td>
                                                                      John
                                                                                </td>
                                                                 <td>
                                                                      Deo
                                                                                </td>
                                                                 <td>
                                                                      8745968574
                                                                                </td>
                                                                 <td>
                                                                      john@gmail.com
                                                                                </td>
                                                                 <td>
                                                                      Indore
                                                                                </td>
                                                                 <td>
                                                                      MP
                                                                                </td>
                                                                 <td>India</td>
                                                                 <td>1</td>
                                                                 <td>1234567</td>
                                                                 <td>RGPV</td>
                                                                 <td>User1</td>
                                                                 <td>Done</td>
                                                                 <td>Pending</td>
                                                                 <td>Pending</td>
                                                                 <td>1</td>
                                                                 <td>452980</td>
                                                            </tr>

                                                            <tr>
                                                                 <td>
                                                                      John
                                                                                </td>
                                                                 <td>
                                                                      Deo
                                                                                </td>
                                                                 <td>
                                                                      8745968574
                                                                                </td>
                                                                 <td>
                                                                      john@gmail.com
                                                                                </td>
                                                                 <td>
                                                                      Indore
                                                                                </td>
                                                                 <td>
                                                                      MP
                                                                                </td>
                                                                 <td>India</td>
                                                                 <td>1</td>
                                                                 <td>1234567</td>
                                                                 <td>RGPV</td>
                                                                 <td>User1</td>
                                                                 <td>Done</td>
                                                                 <td>Pending</td>
                                                                 <td>Pending</td>
                                                                 <td>1</td>
                                                                 <td>452980</td>
                                                            </tr>

                                                       </tbody>
                                                  </Table>
                                             </div>
                                        </Col>
                                   </Row>

                                   <div className="d-flex mt-5 mb-5 justify-content-between">
                                        <div className="d-flex align-items-center">
                                             <div className="mr-2">Show</div>
                                             {/* <Form.Select>  
                                                  <option>Large select</option>
                                              </Form.Select> */}
                                             <Form.Control as="select" className="selectTyp1 selectNumber1">
                                                  <option>8</option>
                                                  <option>10</option>
                                                  <option>20</option>
                                             </Form.Control>
                                        </div>
                                        <div className="paginationType4">         
                                             <Pagination>
                                                  <Pagination.First />
                                                  <Pagination.Prev />
                                                  <Pagination.Item>{1}</Pagination.Item>
                                                  <Pagination.Item>{2}</Pagination.Item>
                                                  <Pagination.Item active>{3}</Pagination.Item>
                                                  <Pagination.Item>{4}</Pagination.Item>
                                                  <Pagination.Last />
                                             </Pagination>
                                        </div>
                                   </div>  
                              </div>
                         </Col> 
                    </Row>  
                    <div className="bottomFooter d-flex align-items-center bgCol43">
                         <div className="container-fluid">  
                              <Row>
                                   <Col md={12}>
                                        <div className="d-flex align-items-center">
                                             <Image src={footerLogo} alt="footer-logo" className="fLogo" />
                                             <div className="col42 fs13 ml-4">Copyright © {new Date().getFullYear()} HiEd Success | Powered by HiEd Success</div>
                                        </div>
                                   </Col>
                              </Row>
                         </div>
                    </div>
               </div>
          </div>
     )
}

export default UserDetail; 





