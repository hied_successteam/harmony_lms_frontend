import React, { useState } from 'react';
import { Container, Col, Row, Image, Button, Dropdown, Form, Nav, Tab, Modal, Accordion, Card, Table } from 'react-bootstrap';
import Logoadmin from '../../../assets/images/svg/logoadmin.svg';
import rocketUp from "../../../assets/images/svg/rocket-up.svg";
import DownOne from "../../../assets/images/svg/downOne.svg";
import footerLogo from '../../../assets/images/footerlogo.png';  

const StudentDetail = () => {
     return (
          <div className="wizardLayout userDashboard"> 
               <div className="container-fluid pl-0">
                    <Row>
                         <Col sm={3}>
                              <div className="sidebarBox">
                                   <div className="pl-4 pt-4 mt-2 mb-5 d-flex align-items-center">
                                        <Image src={Logoadmin} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" />
                                        <span className="fs18 col3 fw600 ml-2">HiEd Harmony</span>
                                   </div>
                                   <Nav variant="pills" className="flex-column innerBox">
                                        <Nav.Item>
                                             <Nav.Link className="listBox active">
                                                  <i className="fa fa-user-o" aria-hidden="true">
                                                  </i><span className="titles text-uppercase fs15 d-block">Student Profile</span>
                                             </Nav.Link>
                                        </Nav.Item>
                                        <Nav.Item>
                                             <Nav.Link className="listBox">
                                                  <i className="fa fa-university" aria-hidden="true"></i><span className="titles text-uppercase fs15 d-block">College</span>
                                             </Nav.Link>
                                        </Nav.Item>
                                        <Nav.Item>
                                             <Nav.Link className="listBox">
                                                  <i className="fa fa-sign-out" aria-hidden="true"></i><span className="titles text-uppercase fs15 d-block">Logout</span>
                                             </Nav.Link>
                                        </Nav.Item>
                                   </Nav>
                              </div>
                         </Col>

                         <Col sm={9}>
                              <div className="userrightBox pr-4">
                                   <div className="rightUser">
                                        <div className="d-flex justify-content-end mt-4">
                                             <Dropdown className="DropdownType2">
                                                  <Dropdown.Toggle id="dropdown-basic" className="DropdownType1 col2 fs14 fw600">
                                                       Jone Aly   <span className="circleType3 d-inline-block br50 text-center"><i className="fa fa-chevron-down col1" aria-hidden="true"></i> </span>
                                                  </Dropdown.Toggle>
                                                  <Dropdown.Menu>
                                                       <Dropdown.Item href="#">Profile</Dropdown.Item>
                                                       <Dropdown.Item href="#">Setting</Dropdown.Item>
                                                       <Dropdown.Item href="#">Logout</Dropdown.Item>
                                                  </Dropdown.Menu>
                                             </Dropdown>
                                        </div>
                                   </div>

                                   <div className="pl-4 pr-4 pt-4">

                                        <div className="middileAccordian mb-5">
                                             <div className="myProfile">
                                                  
                                                  <div className="d-flex justify-content-between">
                                                       <div className="fs20 fw500 col2 mb-4 pointer"><i className="fs20 mr-2 fa fa-chevron-left" aria-hidden="true"></i> Back</div>
                                                       <Button className="btnType3 updateBtn">Update</Button>
                                                  </div>

                                                  <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1">
                                                       <div>Basic</div>
                                                       <div>
                                                            <Image src={DownOne} alt="Icon" className="pointer rotateArrow" />
                                                       </div>
                                                  </div>

                                                  <Row className="mb-4">
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Name of Student</div>
                                                                 <div className="col2 fw500 fs15">Shanaya Fernandes</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Email ID</div>
                                                                 <div className="col2 fw500 fs15">shanayafernandes@gmail.com</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Contact Number</div>
                                                                 <div className="col2 fw500 fs15">(704) 555-0127</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Date of Birth (DD/MM/YYYY)</div>
                                                                 <div className="col2 fw500 fs15">03/12/2001</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Country</div>
                                                                 <div className="col2 fw500 fs15">USA</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">State</div>
                                                                 <div className="col2 fw500 fs15">Florida</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">City</div>
                                                                 <div className="col2 fw500 fs15">Miami</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Zipcode</div>
                                                                 <div className="col2 fw500 fs15">452007</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Religious affiliation</div>
                                                                 <div className="col2 fw500 fs15">Catholics</div>
                                                            </div>
                                                       </Col>

                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Visa Status</div>
                                                                 <div className="col2 fw500 fs15">Contact the National Visa Center (NVC)</div>
                                                            </div>
                                                       </Col>
                                                  </Row>

                                                  <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1">
                                                       <div>Academic Info</div>
                                                       <div>
                                                            <Image src={DownOne} alt="Icon" className="pointer rotateArrow" />
                                                       </div>
                                                  </div>
                                                  <Row className="mb-4">
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">What is your current progress in finding the suitable course or college ?</div>
                                                                 <div className="col2 fw500 fs15">Seeking out info. about College/Course</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Which batch (fall/spring) would you like to apply for?</div>
                                                                 <div className="col2 fw500 fs15">Spring 2022</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">What is the highest degree or the level of education you have completed? (If you are currently enrolled in school/college, indicate the highest degree you have  achieved)</div>
                                                                 <div className="col2 fw500 fs15">Less than a high school diploma</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Which entrance exams have you appeared recently?  (Select all that you have appeared)</div>
                                                                 <div className="col2 fw500 fs15">SAT, TOEFL</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Which educational degree would you like to pursue next?</div>
                                                                 <div className="col2 fw500 fs15">Professional Certificate</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Which field of study would you like to pursue?</div>
                                                                 <div className="col2 fw500 fs15">Computer & information sciences, Parks, recreation, leisure, & fitness studies, Engineering</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Write a short essay describing your passion for the field of study and how you will contribute to the college community.</div>
                                                                 <div className="col2 fw500 fs15">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vitae id eget sem ornare. At nisi vel a, nullam et. Ipsum scelerisque sed nunc, ut accumsan, molestie at ac volutpat.</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">How strong are your letters of recommendation?</div>
                                                                 <div className="col2 fw500 fs15">Very Strong</div>
                                                            </div>
                                                       </Col>
                                                  </Row>

                                                  <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1">
                                                       <div>College Preferences</div>
                                                       <div>
                                                            <Image src={DownOne} alt="Icon" className="pointer rotateArrow" />
                                                       </div>
                                                  </div>
                                                  <Row className="mb-4">
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Select top priorities when considering admission in a particular college (Choose 1 or more)</div>
                                                                 <div className="col2 fw500 fs15">First generation grants, Career guidance</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Which entrance exams have you appeared recently?  (Select all that you have appeared)</div>
                                                                 <div className="col2 fw500 fs15">Top100,  Top50</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Any specific target colleges you would like to mention</div>
                                                                 <div className="col2 fw500 fs15">Northwestern Oklahoma State University</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Preferred college size (student population, Choose 1 or more)</div>
                                                                 <div className="col2 fw500 fs15">5,000 - 9,999, 20,000 and above</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Which mode of education is preferred for you?</div>
                                                                 <div className="col2 fw500 fs15">Online</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Which college clubs would you be most interested in joining? (Choose 1 or more)</div>
                                                                 <div className="col2 fw500 fs15">Academic (math, literature, language, history), Media & publication</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Which of the following sports facilities would you like to use? (Choose 1 or more)</div>
                                                                 <div className="col2 fw500 fs15">Football</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Please share your preference of college (dorm) roommates</div>
                                                                 <div className="col2 fw500 fs15">
                                                                      24-hour emergency helpline
                                                                 </div>
                                                            </div>
                                                       </Col>
                                                  </Row>

                                                  <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1">
                                                       <div>Demographic Preferences</div>
                                                       <div>
                                                            <Image src={DownOne} alt="Icon" className="pointer rotateArrow" />
                                                       </div>
                                                  </div>
                                                  <Row className="mb-4">
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Will  you be the first generation student graduating ?</div>
                                                                 <div className="col2 fw500 fs15">Yes</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">
                                                                      What is your ethnicity?
                                                                 </div>
                                                                 <div className="col2 fw500 fs15">American Indian or Alaska Native</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Do you prefer colleges which follow a Specialized Mission?</div>
                                                                 <div className="col2 fw500 fs15">Yes, Single-sex: Men</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Do you have any seasonal allergy?(E.g. Pollen)</div>
                                                                 <div className="col2 fw500 fs15">Yes</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="col2 fw600 fs22 mb-4">Climate Preferences</div>
                                                       </Col>
                                                       <Col md={6}></Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">What are your preferred weather conditions (Choose 1 or more,  Approx. Temperature)</div>
                                                                 <div className="col2 fw500 fs15">Moderate (15C to 35C / 59F to 59F)</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Please share your preference of college (dorm) roommates</div>
                                                                 <div className="col2 fw500 fs15">24-hour emergency helpline</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Which of the following sports facilities would you like to use? (Choose 1 or more)</div>
                                                                 <div className="col2 fw500 fs15">Football</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}></Col>
                                                       <Col md={6}><div className="col2 fw600 fs22 mb-4">Location Preferences</div></Col>
                                                       <Col md={6}></Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Choose preferred college setting</div>
                                                                 <div className="col2 fw500 fs15">City</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Do you prefer colleges that are closer to home? If yes, please select the preferred proximity from home (miles)</div>
                                                                 <div className="col2 fw500 fs15">10 miles</div>
                                                            </div>
                                                       </Col>
                                                  </Row>

                                                  <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1">
                                                       <div>Employment Info</div>
                                                       <div>
                                                            <Image src={DownOne} alt="Icon" className="pointer rotateArrow" />
                                                       </div>
                                                  </div>
                                                  <Row className="mb-4">
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Which of the following personality traits do you possess or have experienced?</div>
                                                                 <div className="col2 fw500 fs15">Effective decision making</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Share an experience or story where you have utilized or demonstrated the personality traits</div>
                                                                 <div className="col2 fw500 fs15">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean aliquam ullamcorper mauris risus mi ut morbi nulla. Consectetur egestas habitasse elementum nullam sed enim.</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">How many hours have you dedicated towards community service or volunteer work in the last 3 years?</div>
                                                                 <div className="col2 fw500 fs15">More than 50 hours</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">What volunteering activities did you participate in? List down those activities.</div>
                                                                 <div className="col2 fw500 fs15">Basketball</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">What is your current employment status?</div>
                                                                 <div className="col2 fw500 fs15">Employed Full - time(40+ hours a week)</div>
                                                            </div>
                                                       </Col>

                                                  </Row>

                                                  <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1">
                                                       <div>Placement Preferences</div>
                                                       <div>
                                                            <Image src={DownOne} alt="Icon" className="pointer rotateArrow" />
                                                       </div>
                                                  </div>
                                                  <Row className="mb-4">
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Is it required for the institute to have partnerships with certain companies for training/job opportunities?</div>
                                                                 <div className="col2 fw500 fs15">Yes</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Is it required for the institute to provide job assistance or employment opportunities for graduating students?</div>
                                                                 <div className="col2 fw500 fs15">Yes</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">How many hours have you dedicated towards community service or volunteer work in the last 3 years?</div>
                                                                 <div className="col2 fw500 fs15">More than 50 hours</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Is it important for the institute to offer Optional Practical Training (OPT) or Curricular Practical Training (CPT) </div>
                                                                 <div className="col2 fw500 fs15">Yes</div>
                                                            </div>
                                                       </Col>

                                                  </Row>

                                                  <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1">
                                                       <div>Financial Preferences</div>
                                                       <div>
                                                            <Image src={DownOne} alt="Icon" className="pointer rotateArrow" />
                                                       </div>
                                                  </div>
                                                  <Row className="mb-4">
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">What is your Current Income ? </div>
                                                                 <div className="col2 fw500 fs15">Yes</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">What is the parents/household investment for  higher education?</div>
                                                                 <div className="col2 fw500 fs15">$30,000 - $40,000</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">Do you need any kind of financial assistance/aid? (Select the suitable option one or more)</div>
                                                                 <div className="col2 fw500 fs15">Federal student aid, Aid for military families</div>
                                                            </div>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="mb-4">
                                                                 <div className="col21 fw500 fs15 mb-2">What is your estimated cost of education or net price </div>
                                                                 <div className="col2 fw500 fs15">Min $15000</div>
                                                            </div>
                                                       </Col>
                                                  </Row>

                                                  <div className="col21 fw500 fs15 mb-3">How important are the following factors to you in college selection? </div>

                                                  <Row>
                                                       <Col md={12}>
                                                            <Table bordered className="tableType2 myProfileTable planTable br10 mb-4">
                                                                 <thead>
                                                                      <tr>
                                                                           <th></th>
                                                                           <th>Not at all important</th>
                                                                           <th>Low importance</th>
                                                                           <th>Slightly important</th>
                                                                           <th>Neutral</th>
                                                                           <th>Moderately important</th>
                                                                           <th>Very important</th>
                                                                           <th>Extremely important</th>
                                                                      </tr>
                                                                 </thead>
                                                                 <tbody>
                                                                      <tr>
                                                                           <td><span className="fw600">
                                                                                Demographics (ehtnicity, gender)</span></td>
                                                                           <td>
                                                                                <Form.Group controlId="formBasicRadio9">
                                                                                     <Form.Check type="radio" aria-label="radio 9" className="checkboxTyp3" label="" />
                                                                                </Form.Group>
                                                                           </td>
                                                                           <td>
                                                                                <Form.Group controlId="formBasicRadio6">
                                                                                     <Form.Check type="radio" aria-label="radio 5" className="checkboxTyp3" label="" />
                                                                                </Form.Group>
                                                                           </td>
                                                                           <td>
                                                                                <Form.Group controlId="formBasicRadio7">
                                                                                     <Form.Check type="radio" aria-label="radio 7" className="checkboxTyp3" label="" />
                                                                                </Form.Group>
                                                                           </td>
                                                                           <td>
                                                                                <Form.Group controlId="formBasicRadio8">
                                                                                     <Form.Check type="radio" aria-label="radio 8" className="checkboxTyp3" label="" />
                                                                                </Form.Group>
                                                                           </td>
                                                                      </tr>
                                                                 </tbody>
                                                            </Table>
                                                       </Col>
                                                       <Col md={6}>
                                                            <div className="fw500 col21 mb-2">
                                                                 Would you like to add extra information that is critical to your Institution search
                                                            </div>
                                                            <div className="fw500 col2 mb-4 pb-3">
                                                                 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sollicitudin enim tellus turpis eget. Lacus eros, posuere ante scelerisque et, vel duis at.
                                                            </div>
                                                       </Col>
                                                  </Row>
                                                  <div className="stepTitle d-flex justify-content-between fw500 fs30 col4 position-relative mb-4 pb-1 pr-3">
                                                       <div>Plan</div>
                                                       <div>
                                                            <Image src={DownOne} alt="Icon" className="pointer rotateArrow" />
                                                       </div>
                                                  </div>
                                                  <Row>
                                                       <Col md={5}>
                                                            <div className="PlanList1 planList2 active w-100 bgCol44 p-3">
                                                                 <Row className="align-items-center">
                                                                      <Col md={3}>
                                                                           <div className="text-center">
                                                                                <div className="circleType3">
                                                                                     <Image src={rocketUp} alt="Icon" className="mw30" />
                                                                                </div>
                                                                           </div>
                                                                      </Col>
                                                                      <Col md={6}>
                                                                           <div className="mt-2">
                                                                                <div className="col2 fs30 fw500 mb-1 elippse1">demo</div>
                                                                                <div className="mb-3 col5 fw500 fs14 elippse2">lorem ipsum</div>
                                                                           </div>
                                                                      </Col>
                                                                      <Col md={3} className="text-right">
                                                                           <div className="col30 fw600">$300</div>
                                                                      </Col>
                                                                 </Row>
                                                            </div>
                                                       </Col>
                                                  </Row>
                                             </div>
                                        </div>
                                   </div>

                              </div>
                         </Col>

                    </Row>
                    <div className="bottomFooter d-flex align-items-center bgCol43">
                         <div className="container-fluid">
                              <Row>
                                   <Col md={12}>
                                        <div className="d-flex align-items-center">
                                             <Image src={footerLogo} alt="footer-logo" className="fLogo" />
                                             <div className="col42 fs13 ml-4">Copyright © {new Date().getFullYear()} HiEd Success | Powered by HiEd Success</div>
                                        </div>
                                   </Col>
                              </Row>
                         </div>
                    </div>
               </div>
          </div>
     )
}

export default StudentDetail;





