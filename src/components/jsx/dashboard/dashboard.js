import React from 'react';
import { Col, Row, Container, Image, Form } from 'react-bootstrap';  
import Logos from '../../../assets/images/logos.png'; 
import Graduate from '../../../assets/images/svg/graduated.svg';  
import Colleges from '../../../assets/images/svg/colleges.svg';   

const Dashboard = (props) => {
     const loginPage = (status) =>{
          var pageType = {
               pathname: status == "student" ? '/student/login' : '/college/login'
             }
             props.history.push(pageType);
     }
    return (  
          <div className="bgBanner d-flex align-items-center justify-content-center w-100">   
               <Container>
                    <Col lg={8} md={9} className="m-auto">     
                         <div className="studentBox text-center bgCol3 shadow1 br10">       
                              <Image src={Logos} alt="HiEd Success" title="Welecome to HiEd Success" className="pointer logos" />
                              <div className="studentInfo d-flex justify-content-center"> 
                                   <div className="boxLeft mr-5" onClick={() => loginPage("student")}>  
                                        <div className="boxInner bgCol3 shadow2 br8 pt-1 d-flex align-items-center justify-content-center"> 
                                             <Image src={Graduate} alt="Graduate" className="pointer" />
                                        </div>
                                        <div className="fw600 col2 mt-3">Student</div> 
                                   </div>
                                   <div className="boxRight" onClick={() => loginPage("college")}>  
                                        <div className="boxInner bgCol3 shadow2 br8 pt-1 d-flex align-items-center justify-content-center">   
                                             <Image src={Colleges} alt="Graduate" className="pointer" />
                                        </div>  
                                        <div className="fw600 col2 mt-3">College</div>  
                                   </div>
                              </div>
                         </div> 
                    </Col>
               </Container>
          </div> 
    )
}



export default Dashboard; 

