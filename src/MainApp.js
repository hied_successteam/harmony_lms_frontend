import './App.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Route, Switch, Redirect} from 'react-router-dom'
import React from 'react'; 
import CollegeModule from './components/development/college'
import StudentModule from './components/development/student'
import AdminModule from './components/development/admin'
import Landing from './components/development/dashboard'
import ForgetPassword from './components/forgetpassword/ForgetPassword'
import ResetPassword from './components/forgetpassword/ResetPassword'
import Success from './components/forgetpassword/Success'
import PublicRoute from './routes/PublicRoute';


import { LinkedInPopUp } from 'react-linkedin-login-oauth2';


const MainApp = (props) => {

    const {location } = props;
    const isRoot = location && location.pathname == '/' ? true : false;
    if (isRoot) {
      return ( <Redirect  to={`/landing/dashboard`}/> );
    }

    const isRootAdmin = location && location.pathname == '/admin' ? true : false;
    if (isRootAdmin) {
      return ( <Redirect  to={`/admin/login`}/> );
    }

    return (
      <div className="App">
        <Switch>
            <Route path="/linkedin" component={LinkedInPopUp} />
            <Route path={`/college`} component={CollegeModule} />
            <Route path={`/student`} component={StudentModule} />     
            <Route path={`/admin`} component={AdminModule} />         
            <Route path={`/forgotpassword`} component={ForgetPassword} />
            <Route path={`/reset-password/`} component={ResetPassword} />   
            <PublicRoute path={`/landing`} component={Landing} />
            <PublicRoute path={`/success`} component={Success} />            

      </Switch>
    </div>
    )
  
}
export default MainApp;