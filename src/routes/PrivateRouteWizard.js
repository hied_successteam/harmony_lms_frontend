import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { getLocalStorage } from '../Lib/Utils';
import CONSTANTS from '../Lib/Constants';

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      getLocalStorage('user') ? ( 

        getLocalStorage('user').isProfileCompleted ?
        <Redirect
          to={{ pathname: '/student/dashboard'}}
        />
        :
          <Component {...props} />
      ) : (
        <Redirect
          to={{ pathname: '/landing/dashboard'}}
        />
      )
    }
  />
);

export default PrivateRoute;
