import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { getLocalStorage } from '../Lib/Utils';
import CONSTANTS from '../Lib/Constants';

const AdminPrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      getLocalStorage('admin') ? ( 
          <Component {...props} />
      ) : (
        <Redirect
          to={{ pathname: '/admin/login'}}
        />
      )
    }
  />
);

export default AdminPrivateRoute;
