import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { getLocalStorage } from '../Lib/Utils';
import CONSTANTS from '../Lib/Constants';

const PublicRoute = ({
  component: Component,
  isAutoLogin,
  restricted,
  ...rest
}) => {
  return (    
    <Route
      {...rest}
      render={props =>
        (
          <>           

              { isAutoLogin == true ? (
                <> 
                <Component {...props} />
                </>
              ) : (getLocalStorage('user') && getLocalStorage('user').userType == CONSTANTS.ROLES.STUDENT) ? (
                <> 
                {
                  getLocalStorage('user') && getLocalStorage('user').isProfileCompleted &&
                    <Redirect to="/student/dashboard" /> 
                }
                </>
              ) : (getLocalStorage('user') && getLocalStorage('user').userType == CONSTANTS.ROLES.COLLEGE ) ? (
                <> 
                {
                  getLocalStorage('user') && getLocalStorage('user').isProfileCompleted &&
                    <Redirect to="/college/dashboard" /> 
                } 
                </>
              ) : (
                <>                              
                <Component {...props} />
                </> 
              )
            }

          </>
        )
      }
    />
  );
};

export default PublicRoute;
