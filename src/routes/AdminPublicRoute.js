import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { getLocalStorage } from '../Lib/Utils';
import CONSTANTS from '../Lib/Constants';

const AdminPublicRoute = ({
    component: Component,
    isAutoLogin,
    restricted,
    ...rest
}) => {
    return (
        
        <Route
            {...rest}
            render={props =>
            (
                <>
                    <Component {...props} />
                </>
            )
            }
        />
    );
};

export default AdminPublicRoute;
