import './App.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
// import Dashboard from './components/jsx/dashboard/dashboard'; 
// import QuestionWizard from './components/jsx/student/questionwizard/questionwizard';
// import StudentRegister from './components/jsx/student/Register/register'; 
// import CollegeRegister from './components/jsx/college/Register/register'; 
// import CollegeLogin from './components/jsx/college/Login/login';  
// import StudentLogin from './components/jsx/student/Login/login';
//import ResetLink from './components/jsx/student/Resetlink/resetlink'; 
import CollegeDashboard from './components/jsx/college/dashboard/dashboard';
import StudentDashboard from './components/jsx/student/dashboard/dashboard'; 
import Help from './components/jsx/student/dashboard/help'; 
import CollegeDetail from './components/jsx/student/collegeDetail/detail'; 
// import Notification from './components/jsx/college/notification/notification';  
//import StudentComparison from './components/jsx/student/comparison/comparison';   
import CollegeComparison from './components/jsx/college/comparison/comparison';   
//import StudentRegister from './components/jsx/student/Register/register'; 
//import CollegeRegister from './components/jsx/college/Register/register'; 
//import CollegeLogin from './components/jsx/college/Login/login';    
//import StudentLogin from './components/jsx/student/Login/login';
import Wizard from './components/jsx/student/wizard/wizard';  
import MyProfile from './components/jsx/myprofile/myprofile'; 
import Payment from './components/jsx/Payment/payment';                
import UserDetail from './components/jsx/userdashboard/userdetail';  
import StudentDetail from './components/jsx/userdashboard/studentdetail';   

//import ForgotPassword from './components/jsx/student/Forgotpassword/forgot';  

/* jsx code end */ 
// import ForgotPassword from './components/jsx/student/Forgotpassword/forgot'
// import ResetPassword from './components/jsx/student/Resetpassword/reset'

//Devlopment code
import { Route, BrowserRouter } from 'react-router-dom';     
import React, {useEffect} from 'react'; 
import MainApp from './MainApp'
import {Provider} from 'react-redux'  
import store from './Redux/store'  
import {logout } from './Actions/Student/register';
import { getLocalStorage, clearLocalStorage, setLocalStorage } from './Lib/Utils';

function App() {    
  // console.warn = () => {};
  // console.disableYellowBox = true;


  // useEffect(() => {
  //   return () => {
  //     window.addEventListener('beforeunload',keepOnPage());
  //   }
  // }, [])

  // const keepOnPage = () =>{
  //   alert('ygy')
  //   var user = getLocalStorage('user');

  //       if (user && user.id) {
  //         logout({
  //           userId: user.id
  //         }).then(res => {
  //           if (res.data.success == true) {
  //             localStorage.clear();
  //             clearLocalStorage();
  //           }
  //         }).catch(err => {
  //         });
  //       }
  // }



  return (
    <>  
      <Provider store={store}>
        <BrowserRouter>

        {/* <Route path={`/jsx/ResetLink`} component={ResetLink} />
        <Route path={`/jsx/CollegeRegister`} component={CollegeRegister} />
        <Route path={`/jsx/StudentRegister`} component={StudentRegister} /> */}
        <Route path={`/jsx/MyProfile`} component={MyProfile} /> 
        
        <Route path={`/jsx/Payment`} component={Payment} /> 
        <Route path={`/jsx/UserDetail`} component={UserDetail} /> 
        <Route path={`/jsx/StudentDetail`} component={StudentDetail} />   
        <Route path={`/jsx/CollegeDashboard`} component={CollegeDashboard} /> 
        {/* <Route path={`/jsx/CollegeRegister`} component={CollegeRegister} /> */}
        {/* <Route path={`/jsx/dashboard`} component={Dashboard} />
        <Route path={`/jsx/QuestionWizard`} component={QuestionWizard} />
        <Route path={`/jsx/StudentRegister`} component={StudentRegister} />
        <Route path={`/jsx/StudentLogin`} component={StudentLogin} />
        <Route path={`/jsx/CollegeRegister`} component={CollegeRegister} />
        <Route path={`/jsx/CollegeLogin`} component={CollegeLogin} />
      <Route path={`/jsx/CollegeDashboard`} component={CollegeDashboard} />
        <Route path={`/jsx/Notification`} component={Notification} />
        <Route path={`/jsx/StudentComparison`} component={StudentComparison} />
        <Route path={`/jsx/ForgotPassword`} component={ForgotPassword} />
        <Route path={`/jsx/ResetPassword`} component={ResetPassword} />*/}
        {/* <Route path={`/jsx/Wizard`} component={Wizard} />   
        <Route path={`/jsx/CollegeDetail`} component={CollegeDetail} />      
        <Route path={`/jsx/studentDashboard`} component={StudentDashboard} />   */}   
        <Route path={`/jsx/collegeComparison`} component={CollegeComparison} />     

        <Route path={`/jsx/Wizard`} component={Wizard} />
        <Route path={`/jsx/studentDashboard`} component={StudentDashboard} />   
        <Route path={`/jsx/CollegeDetail`} component={CollegeDetail} />     
        <Route path={`/jsx/Help`} component={Help} />     

      {/* <Route path={`/jsx/StudentRegister`} component={StudentRegister} />   */}
       {/* <Route path={`/jsx/ForgotPassword`} component={ForgotPassword} />
        <Route path={`/jsx/ResetPassword`} component={ResetPassword} />
        <Route path={`/jsx/ResetLink`} component={ResetLink} /> */}
          <Route path={`/`} component={MainApp} />      
        </BrowserRouter>  
      </Provider>

      {/* <Dashboard />   */}  
      {/* <QuestionWizard />          */}
      {/* <StudentRegister />       */} 
      {/* <CollegeRegister />         */} 
      {/* <CollegeLogin />       */}
      {/* <StudentLogin />   */}
      {/* <CollegeDashboard />         */}
      {/* <StudentDashboard />     */}
      {/* <Notification />  */}  
      {/* <CollegeComparison />        */}
      {/* <StudentComparison />                */}
      {/* <CollegeDetail /> */}    
      {/* <Wizard />                                           */}    
    </>
  );
}         

export default App;  














